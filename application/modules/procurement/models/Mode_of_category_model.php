<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Mode_of_category_model extends CI_Model
{
    function categoryModeList()
    {
        $this->db->select('*');
        $this->db->from('mode_of_category');
         $query = $this->db->get();
         $result = $query->result();
         //print_r($result);exit();     
         return $result;
    }

    function procurementCategoryListSearch($search)
    {
        $this->db->select('*');
        $this->db->from('mode_of_category');
        if (!empty($search))
        {
            $likeCriteria = "(code  LIKE '%" . $search . "%' or description  LIKE '%" . $search . "%')";
            $this->db->where($likeCriteria);
        }
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function getCategoryModeDetails($id)
    {
        $this->db->select('*');
        $this->db->from('mode_of_category');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewCategoryMode($data)
    {
        $this->db->trans_start();
        $this->db->insert('mode_of_category', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editCategoryMode($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('mode_of_category', $data);
        return TRUE;
    }
}

