<?php $this->load->helper("form"); ?>


<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
            <div class="page-title clearfix">
                <h3>Add NON-PO Entry</h3>
            </div>

        <form id="form_nonpo_entry" action="" method="post">
        <div class="form-container">
            <h4 class="form-group-title">Non-PO Entry</h4>


            <div class="row">
                <div class="col-sm-4">
                        <div class="form-group">
                            <label>PR Number <span class='error-text'>*</span></label>
                            <select name='type_of_pr' id='type_of_pr' class='form-control' onChange="getDetails(this.value)">
                                <option value=''>Select</option>

                                <?php for($i=0;$i<count($prEntryList);$i++)
                                {
                                    ?>
                                    <option value="<?php echo $prEntryList[$i]->id;?>">
                                    <?php echo $prEntryList[$i]->pr_number . " - " . $prEntryList[$i]->description;?>  
                                    </option>
                                    <?php
                                } ?> 
                            </select>
                        </div>
                </div>
            </div>

        </div>


        <div class="form-container" style="display: none;" id="view_display">
            <h4 class="form-group-title">PO Details</h4>

            <div  id="view">
            </div>

        </div>

            

        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="list" class="btn btn-link">Cancel</a>
            </div>
        </div>

        </form>

        
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>

<script>

    $(document).ready(function() {
        $("#form_nonpo_entry").validate({
            rules: {
                type_of_pr: {
                    required: true
                },
                 pr_number: {
                    required: true
                }
                ,
                 description: {
                    required: true
                },
                 pr_entry_date: {
                    required: true
                },
                 id_financial_year: {
                    required: true
                },
                 amount: {
                    required: true
                },
                 id_vendor: {
                    required: true
                }
            },
            messages: {
                type_of_pr: {
                    required: "<p class='error-text'>Select Type Of PR",
                },
                pr_number: {
                    required: "<p class='error-text'>Enter Description",
                },
                description: {
                    required: "<p class='error-text'>Enter NON-PO Description</p>",
                },
                pr_entry_date: {
                    required: "<p class='error-text'>Select Vendor</p>",
                },
                id_financial_year: {
                    required: "<p class='error-text'>Select Department</p>",
                },
                amount: {
                    required: "<p class='error-text'>Select Financial Year</p>",
                },
                id_vendor: {
                    required: "<p class='error-text'>Enter Amount</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


  
    function getDetails(id) {       
            $.ajax(
            {
               url: '/procurement/NonPoEntry/getData/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {    
                    $("#view_display").show();
                    $("#view").html(result);
               }
            });
        
    }   
</script>
<script type="text/javascript">
    $('select').select2();
</script>