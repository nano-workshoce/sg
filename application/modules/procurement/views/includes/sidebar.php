            <div class="sidebar">
                <div class="user-profile clearfix">
                    <a href="#" class="user-profile-link">
                        <span><img src="<?php echo BASE_PATH; ?>assets/img/user_profile.jpg"></span> <?php echo $name; ?>
                    </a>
                    <div class="dropdown">
                        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
                            <li><a href="/setup/user/profile">Edit Profile</a></li>
                            <li><a href="/setup/user/logout">Logout</a></li>
                        </ul>
                    </div>
                </div>
                <div class="sidebar-nav">
                    <h4>Setup</h4>
                    <ul>
                        <!-- <li><a href="/procurement/modeOfCategory/list">Mode Of Category</a></li> -->
                        <li><a href="/procurement/procurementCategory/list">Procurement Category</a></li>
                        <li><a href="/procurement/procurementSubCategory/list">Procurement Sub-Category</a></li>
                        <li><a href="/procurement/procurementItem/list">Procurement Item</a></li>
                        <!-- <li><a href="/procurement/procurementLimit/list">Procurement Limit</a></li> -->
                    </ul>
                     <h4>Vendor</h4>
                    <ul>
                        <li><a href="/procurement/vendor/list">Vendor Registration</a></li>
                        <li><a href="/procurement/vendor/approvalList">Approve Vendor</a></li>
                        <li><a href="/procurement/vendor/blockList">Block Vendor</a></li>
                    </ul>
                    <h4>Purchase Requisition</h4>
                    <ul>
                        <li><a href="/procurement/prEntry/list">PR Entry</a></li>
                        <li><a href="/procurement/prEntry/approvalList">PR Approval</a></li>
                    </ul>
                    <h4>Purchase Order</h4>
                    <ul>
                        <li><a href="/procurement/poEntry/list">PO Entry</a></li>
                        <li><a href="/procurement/poEntry/approvalList">PO Approval</a></li>
                    </ul>
                    <h4>NON-PO</h4>
                    <ul>
                        <li><a href="/procurement/nonPoEntry/list">NON PO Entry</a></li>
                        <li><a href="/procurement/nonPoEntry/approvalList">NON PO Approval</a></li>
                    </ul>
                    <!-- <h4>Warrant</h4>
                    <ul>
                        <li><a href="/procurement/warrant/list">Warrant Entry</a></li>
                        <li><a href="/procurement/warrant/approvalList">Warrant Approval</a></li>
                    </ul> -->
                    <h4>Tender</h4>
                    <ul>
                        <li><a href="/procurement/tenderQuotation/list">Tender Quotation</a></li>
                        <li><a href="/procurement/tenderSubmission/list">Tender Submission</a></li>
                        <li><a href="/procurement/tenderShortlist/list">Tender Shortlist</a></li>
                        <li><a href="/procurement/tenderAward/list">Tender Award</a></li>
                        <!-- <li><a href="/procurement/tenderAward/list">Tender Agreement</a></li>
                        <li><a href="#">Tender contract for payment</a></li> -->
                    </ul>
                </div>
            </div>