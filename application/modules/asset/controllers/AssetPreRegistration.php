<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class AssetPreRegistration extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Asset_pre_registration_model');
        $this->load->model('Asset_order_model');

        $this->isLoggedIn();
    }

    function list()
    {

        if ($this->checkAccess('asset_pre_registration.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['brand'] = $this->security->xss_clean($this->input->post('brand'));
            $formData['company'] = $this->security->xss_clean($this->input->post('company'));
            $formData['depriciation_code'] = $this->security->xss_clean($this->input->post('depriciation_code'));
            $formData['asset_order_number'] = $this->security->xss_clean($this->input->post('asset_order_number'));
            $formData['status'] = '';
 
            $data['searchParam'] = $formData;

            $data['assetPreRegistrationList'] = $this->Asset_pre_registration_model->assetPreRegistrationListSearch($formData);

            $this->global['pageTitle'] = 'FIMS : List Asset Pre-Registration';
            // echo "<Pre>";print_r($data['assetPreRegistrationList']);exit;
            $this->loadViews("asset_pre_registration/list", $this->global, $data, NULL);
        }
    }

     function add()
    {

        if ($this->checkAccess('asset_pre_registration.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {


                $id_grn = $this->security->xss_clean($this->input->post('id_grn'));
                $id_session = $this->session->my_session_id;
                $user_id = $this->session->userId;
                
                $id_grn = $this->security->xss_clean($this->input->post('id_grn'));
                $id_item = $this->security->xss_clean($this->input->post('id_item'));
                $id_category = $this->security->xss_clean($this->input->post('id_category'));
                $id_sub_category = $this->security->xss_clean($this->input->post('id_sub_category'));
                $formData = $this->input->post();

                // echo "<Pre>"; print_r($formData);exit;
                for($i=0;$i<count($formData['name']);$i++)
                 {

                    $id_ao = $formData['id_ao'][$i];
                    $name = $formData['name'][$i];
                    $brand = $formData['brand'][$i];
                    $company = $formData['company'][$i];
                    $price = $formData['price'][$i];
                    $tax = $formData['tax'][$i];
                    $depriciation_code = $formData['depriciation_code'][$i];
                    $depriciation_value = $formData['depriciation_value'][$i];

                    if($price > 0)
	                {

	                        $detailsData = array(
                                'id_asset_order' => $id_ao,
                                'id_category' => $id_category,
                                'id_sub_category' => $id_sub_category,
                                'id_item' => $id_item,
		                        'id_grn' => $id_grn,
		                        'name' => $name,
		                        'brand' => $brand,
		                        'company' => $company,
		                        'price' => $price,
                                'tax' => $tax,
		                        'total_amount' => $price + $tax,
		                        'depriciation_code' => $depriciation_code,                 
		                        'depriciation_value' => $depriciation_value,
		                        'status' => '0',
		                        'created_by' => $user_id
		                        );
		                        $pre_register_id = $this->Asset_pre_registration_model->addNewAssetPreRegistration($detailsData);
		                        // print_r($inserted_id);exit;

                                if($pre_register_id)
                                {
                                    $updated = $this->Asset_pre_registration_model->updatePreRegisterIdToAssetOrder($id_ao,$id_grn);
        		                }
	                }
                }
                redirect('/asset/assetPreRegistration/list');
            }

            $data['assetOrderList'] = $this->Asset_pre_registration_model->assetOrderListFprPreRegistration();
            $this->global['pageTitle'] = 'FIMS : List Asset Pre-Registration';
            // echo "<Pre>";print_r($data['assetOrderList']);exit;
            $this->loadViews("asset_pre_registration/add", $this->global, $data, NULL);
        }
        
    }

    function getData($id)
    {
        
        $assetOrderDetails = $this->Asset_pre_registration_model->getAssetOrderByOPID($id);
	// echo "<Pre>";print_r($assetOrderDetails);exit;

		$table = "

            <h3> &nbsp;&nbsp;&nbsp;GRN Details </h3>
        

        <div class='custom-table'>
		<table  class='table' id='list-table'>
              <tr>
                <th>Sl. No</th>
                <th>Asset Order Number</th>
                <th>Item Name</th>
                <th>Category</th>
                <th>Sub Category</th>
                
                <th>Received Qty</th>
                <th class='text-center'>Pre Register</th>
            </tr>";

            // <th>Amount / Qty</th>
            //     <th>Total Amount</th>

            $total_amount = 0;
            for($i=0;$i<count($assetOrderDetails);$i++)
            {

	            $id = $assetOrderDetails[$i]->id;
                $asset_order_number = $assetOrderDetails[$i]->asset_order_number;
	            $id_item = $assetOrderDetails[$i]->id_item;
	            $item_code = $assetOrderDetails[$i]->item_code;
	            $item_name = $assetOrderDetails[$i]->item_name;
	            $id_category = $assetOrderDetails[$i]->id_category;
	            $sub_category_code = $assetOrderDetails[$i]->sub_category_code;
	            $sub_category_name = $assetOrderDetails[$i]->sub_category_name;
	            $id_sub_category = $assetOrderDetails[$i]->id_sub_category;
	            $category_code = $assetOrderDetails[$i]->category_code;
	            $category_name = $assetOrderDetails[$i]->category_name;
	            $ordered_qty = $assetOrderDetails[$i]->ordered_qty;
                $balance_qty = $assetOrderDetails[$i]->balance_qty;
                $amount = $assetOrderDetails[$i]->amount;
	            $amount_per_item = $assetOrderDetails[$i]->amount_per_item;
	            $received_qty = $assetOrderDetails[$i]->received_qty;

            	$j = $i+1;
                
                $table .= "
                <tr>
                    <td>$j. <input type='number' hidden='hidden' readonly='readonly' id='id_grn_detail[]' name='id_grn_detail[]' value='$id'>
                    </td>
                    <td>
                        
                        $asset_order_number
                    </td>
                    <td>
	                    <input type='number' hidden='hidden' readonly='readonly' id='id_item[]' name='id_item[]' value='$id_item'>
	                    $item_code - item_name
                    </td>
                    <td>
                    	<input type='number' hidden='hidden' readonly='readonly' id='id_category[]' name='id_category[]' value='$id_category'>
                    	$category_code - $category_name
                    </td>
                    <td>
                    	<input type='number' hidden='hidden' readonly='readonly' id='id_sub_category[]' name='id_sub_category[]' value='$id_sub_category'>
                    	$sub_category_code - $sub_category_name
                    </td>                   
                   
                    <td>
                        $received_qty
                    </td>
                   
                    <td class='text-center'>
                        <a class='btn btn-sm btn-edit' onclick='showPreRegister($id)'>Pre Register</a>
                    </td>
                </tr>";

                        // <a onclick='showPreRegister($id)' title='Pre Register'>Pre Register</a>
                 // <td>
                 //        $amount_per_item
                 //    </td>

                 //     <td>
                 //        $amount
                 //    </td>


			}

        $table.= "</table>
        </div>";

       echo $table;
       // exit();

    }

    function showPreRegister($id)
    {
        
        $assetOrderDetail = $this->Asset_order_model->getAssetOrderById($id);
    // echo "<Pre>";print_r($assetOrderDetail);exit;


        $qty = $assetOrderDetail->received_qty;
        $id_ao = $assetOrderDetail->id;
        $item_name = $assetOrderDetail->item_name;
        $item_code = $assetOrderDetail->item_code;
        $id_category = $assetOrderDetail->id_category;
        $id_sub_category = $assetOrderDetail->id_sub_category;
        $id_item = $assetOrderDetail->id_item;

        $table = "

        <hr>

        <div class='page-title clearfix'>
            <h3> Pre Register Asset For Item -> $item_code - $item_name And Quantity -> $qty 
            <input type='number' hidden='hidden' readonly='readonly' id='id_category' name='id_category' value='$id_category'>
            <input type='number' hidden='hidden' readonly='readonly' id='id_sub_category' name='id_sub_category' value='$id_sub_category'>
            <input type='number' hidden='hidden' readonly='readonly' id='id_item' name='id_item' value='$id_item'>
            </h3>
        </div>


        <table width='100%' class='table' id='list-table'>
              <tr>
                <th>Sl. No</th>
                <th>Asset Name</th>
                <th>Brand</th>
                <th>Company</th>
                <th>Price</th>
                <th>Tax</th>
                <th>Depriciation code</th>
                <th>Depriciation Value</th>
            </tr>";

            for($i=0;$i<$qty;$i++)
            {

                $j = $i+1;
                
                $table .= "
                <tr>
                    <td>$j. <input type='number' hidden='hidden' readonly='readonly' id='id_ao[]' name='id_ao[]' value='$id_ao'></td>
                    <td>
                        <input type='text' id='name[]' name='name[]'>
                    </td>
                    <td>
                        <input type='text' id='brand[]' name='brand[]'>
                    </td>
                    <td>
                        <input type='text' id='company[]' name='company[]'>
                    </td>                   
                    <td>
                        <input type='number' id='price[]' name='price[]'>
                    </td>
                    <td>
                        <input type='number' id='tax[]' name='tax[]'>
                    </td>
                    <td>
                        <input type='number' id='depriciation_code[]' name='depriciation_code[]'>
                    </td>
                    <td>
                        <input type='number' id='depriciation_value[]' name='depriciation_value[]'>
                    </td>
                </tr>";
            }

        $table.= "</table>";

       echo $table;
       // exit();

    }

    function approvalList()
    {

        if ($this->checkAccess('asset_order.approval') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $resultprint = $this->input->post();

           if($resultprint)
            {
             switch ($resultprint['button'])
             {
                case 'approve':

                     for($i=0;$i<count($resultprint['checkvalue']);$i++)
                        {

                         $id = $resultprint['checkvalue'][$i];
                         // echo "<Pre>";print_r($id);exit();

                         $data = array('status' => 1);
                         

                         $result = $this->Asset_pre_registration_model->editAssetPreRegisterList($data,$id,'1');
                        }
                        redirect($_SERVER['HTTP_REFERER']);
                     break;


                case 'reject':

                     for($i=0;$i<count($resultprint['checkvalue']);$i++)
                        {

                         $id = $resultprint['checkvalue'][$i];
                         // echo "<Pre>";print_r($id);exit();

                         $data = array('status' => 2);
                         
                         $result = $this->Asset_pre_registration_model->approveInvestmentWithdraw($data,$id,'2');
                        }
                        redirect($_SERVER['HTTP_REFERER']);
                     break;


                case 'search':

                  
                     
                     break;
                 
                default:
                     break;
             }
                
            }



           $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['brand'] = $this->security->xss_clean($this->input->post('brand'));
            $formData['company'] = $this->security->xss_clean($this->input->post('company'));
            $formData['depriciation_code'] = $this->security->xss_clean($this->input->post('depriciation_code'));
            $formData['asset_order_number'] = $this->security->xss_clean($this->input->post('asset_order_number'));
            $formData['status'] = '0';
 
            $data['searchParam'] = $formData;

            $data['assetPreRegistrationList'] = $this->Asset_pre_registration_model->assetPreRegistrationListSearch($formData);

            // $array = $this->security->xss_clean($this->input->post('checkvalue'));
            // if (!empty($array))
            // {

            //     $result = $this->Asset_pre_registration_model->editAssetPreRegisterList($array);
            //     redirect($_SERVER['HTTP_REFERER']);
            // }

            $this->global['pageTitle'] = 'FIMS : Approve Asset Pre-Registration';
            // echo "<Pre>";print_r($data['assetPreRegistrationList']);exit;
            $this->loadViews("asset_pre_registration/approval_list", $this->global, $data, NULL);
        }
    }

    function view($id = NULL)
    {
        if ($this->checkAccess('receipt.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/asset/assetPreRegistration/approvalList');
            }
            if($this->input->post())
            {
                $status = $this->security->xss_clean($this->input->post('status'));
                $reason = $this->security->xss_clean($this->input->post('reason'));


                $data = array(
                    'status' => $status,
                    'reason' => $reason
                );

                
                 $result = $this->Asset_pre_registration_model->updateAssetPreRegister($data,$id);
                redirect('/asset/assetPreRegistration/approvalList');

            }
            // $data['studentList'] = $this->receipt_model->studentList();
            $data['assetPreRegistration'] = $this->Asset_pre_registration_model->assetPreRegistrationById($id);

            // echo "<Pre>";print_r($data['assetPreRegistration']);exit;
            $this->global['pageTitle'] = 'FIMS : View Asset Pre-Registraton';
            $this->loadViews("asset_pre_registration/view", $this->global, $data, NULL);
        }
    }

    function edit($id = NULL)
    {
        if ($this->checkAccess('receipt.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/finance/receipt/list');
            }
            if($this->input->post())
            {
                redirect('/finance/receipt/list');
            }
            // $data['studentList'] = $this->receipt_model->studentList();
            $data['assetPreRegistration'] = $this->Asset_pre_registration_model->assetPreRegistrationById($id);

            // echo "<Pre>";print_r($data['assetPreRegistration']);exit;
            $this->global['pageTitle'] = 'FIMS : View Asset Pre-Registraton';
            $this->loadViews("asset_pre_registration/edit", $this->global, $data, NULL);
        }
    }
}