<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class DisposalType extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('disposal_type_model');
        $this->isLoggedIn();
    }

    function list()
    {

        if ($this->checkAccess('disposal_type.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;

            $data['disposalTypeList'] = $this->disposal_type_model->disposalTypeListSearch($name);
            $this->global['pageTitle'] = 'FIMS : List Disposal Type';
            //print_r($subjectDetails);exit;
            $this->loadViews("disposal_type/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('disposal_type.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $code = $this->security->xss_clean($this->input->post('code'));
                $name = $this->security->xss_clean($this->input->post('name'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'code' => $code,
                    'name' => $name,
                    'status' => $status
                );
            
                $result = $this->disposal_type_model->addNewDisposalType($data);
                redirect('/asset/disposalType/list');
            }
            
            $this->global['pageTitle'] = 'FIMS : Add Disposal Type';
            $this->loadViews("disposal_type/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('disposal_type.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/asset/disposalType/list');
            }
            if($this->input->post())
            {
                $code = $this->security->xss_clean($this->input->post('code'));
                $name = $this->security->xss_clean($this->input->post('name'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'code' => $code,
                    'name' => $name,
                    'status' => $status
                );
                
                $result = $this->disposal_type_model->editDisposalType($data,$id);
                redirect('/asset/disposalType/list');
            }
            $data['disposalType'] = $this->disposal_type_model->getDisposalType($id);
            $this->global['pageTitle'] = 'FIMS : Edit Disposal Type';
            $this->loadViews("disposal_type/edit", $this->global, $data, NULL);
        }
    }
}
