<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Asset_sub_category_model extends CI_Model
{
    function assetSubCategoryList()
    {
        $this->db->select('assc.*, ac.code as asset_category_code, ac.description as asset_category_description');
        $this->db->from('asset_sub_category as assc');
        $this->db->join('asset_category as ac', 'assc.id_asset_category = ac.id');
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function assetSubCategoryListByAssetId($id)
    {
        $this->db->select('assc.*');
        $this->db->from('asset_sub_category as assc');
        $this->db->where('assc.id_asset_category='.$id);
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function assetSubCategoryListSearch($formData)
    {
        $this->db->select('assc.*, ac.code as asset_category_code, ac.description as asset_category_description');
        $this->db->from('asset_sub_category as assc');
        $this->db->join('asset_category as ac', 'assc.id_asset_category = ac.id');
        if($formData['id_asset_category']) {
            $likeCriteria = "(assc.id_asset_category  LIKE '%" . $formData['id_asset_category'] . "%')";
            $this->db->where($likeCriteria);
        }

        if($formData['name']) {
            $likeCriteria = "(assc.description  LIKE '%" . $formData['name'] . "%' or assc.code  LIKE '%" . $formData['name'] . "%')";
            $this->db->where($likeCriteria);
        }
         $query = $this->db->get();
         $result = $query->result();   
         // print_r($formData);exit();     
         return $result;
    }

    function getAssetSubCategory($id)
    {
        $this->db->select('*');
        $this->db->from('asset_sub_category');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewAssetSubCategory($data)
    {
        $this->db->trans_start();
        $this->db->insert('asset_sub_category', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editAssetSubCategory($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('asset_sub_category', $data);
        return TRUE;
    }
}

