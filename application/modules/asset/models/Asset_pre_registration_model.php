<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Asset_pre_registration_model extends CI_Model
{
    function assetPreRegistrationListSearch($data)
    {
        // echo "<Pre>";print_r($data);exit();
        $this->db->select('DISTINCT (apr.id) as id, apr.*, pi.description as item_name, pi.code as item_code, ao.asset_order_number, grn.reference_number');
        $this->db->from('asset_pre_registration as apr');
        $this->db->join('grn as grn', 'apr.id_grn = grn.id');
        $this->db->join('asset_item as pi', 'apr.id_item = pi.id');
        $this->db->join('asset_order as ao', 'apr.id_asset_order = ao.id');
        if ($data['brand']!='')
        {
            $likeCriteria = "(apr.brand  LIKE '%" . $data['brand'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['name']!='')
        {
            $likeCriteria = "(apr.name  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['company']!='')
        {
            $likeCriteria = "(apr.company  LIKE '%" . $data['company'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['depriciation_code']!='')
        {
            $likeCriteria = "(apr.depriciation_code  LIKE '%" . $data['depriciation_code'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['asset_order_number']!='')
        {
            $likeCriteria = "(ao.asset_order_number  LIKE '%" . $data['asset_order_number'] . "%' or grn.reference_number  LIKE '%" . $data['asset_order_number'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['status'] !='')
        {
            $this->db->where('apr.status', $data['status']);
        }
        $this->db->order_by("id", "ASC");
        $query = $this->db->get();
        return $query->result();

    }

    function assetOrderListFprPreRegistration()
    {
        $pre_register = '0';
        $this->db->select('DISTINCT (po.id) as id, po.*');
        $this->db->from('grn as po');
        $this->db->where('po.id_pre_register', $pre_register);
        $query = $this->db->get();
        return $query->result();
    }

    function getAssetOrderByOPID($id_po)
    {
        $pre_register = '0';
        $this->db->select('DISTINCT (ao.id) as id, ao.*, pc.description as category_name, pc.code as category_code, psc.description as sub_category_name, psc.code as sub_category_code, pi.description as item_name, pi.code as item_code');
        $this->db->from('asset_order as ao');
        $this->db->join('po_entry as po','ao.id_po = po.id');
        $this->db->join('po_detail as pod','ao.id_po_detail = pod.id');
        $this->db->join('asset_category as pc', 'ao.id_category = pc.id');
        $this->db->join('asset_sub_category as psc', 'ao.id_sub_category = psc.id');
        $this->db->join('asset_item as pi', 'ao.id_item = pi.id');
        $this->db->where('ao.id_pre_register', $pre_register);
        $this->db->where('ao.id_grn', $id_po);
        $query = $this->db->get();
        return $query->result();

    }

    function addNewAssetPreRegistration($data)
    {
        $this->db->trans_start();
        $this->db->insert('asset_pre_registration', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;

    }

    function updatePreRegisterIdToAssetOrder($id_ao,$id_grn)
    {
        $data['id_pre_register'] = '1';
        $this->db->where('id', $id_ao);
        $this->db->update('asset_order', $data);


        $this->db->select('ao.*');
        $this->db->from('asset_order as ao');
        $this->db->where('ao.id_pre_register', '0');
        $this->db->where('ao.id_grn', $id_grn);
        $query = $this->db->get();
        $grn_detail = $query->row();



        if($grn_detail)
        {

        // echo "<Pre>";print_r($grn_detail);exit();
        }
        else
        {
            $this->db->where('id', $id_grn);
            $this->db->update('grn', $data);
        }

        return TRUE;
    }

    function assetPreRegistrationById($id)
    {
        $this->db->select('DISTINCT (apr.id) as id, apr.*, pc.description as category_name, pc.code as category_code, psc.description as sub_category_name, psc.code as sub_category_code, pi.description as item_name, pi.code as item_code, grn.reference_number, po.po_number');
        $this->db->from('asset_pre_registration as apr');
        $this->db->join('asset_category as pc', 'apr.id_category = pc.id');
        $this->db->join('grn as grn', 'apr.id_grn = grn.id');
        $this->db->join('po_entry as po','grn.id_po = po.id');
        $this->db->join('asset_sub_category as psc', 'apr.id_sub_category = psc.id');
        $this->db->join('asset_item as pi', 'apr.id_item = pi.id');
        $this->db->where('apr.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function editAssetPreRegisterList($data,$id)
    {
        
      $this->db->where_in('id', $id);
      $this->db->update('asset_pre_registration', $data);
        // foreach ($array as $id)
        // {
        // echo "<Pre>";print_r($id);exit();
           $insert_id = $this->addNewAssetRegistration($id);
        // }

        return TRUE;
    }

    function updateAssetPreRegister($data,$id)
    {
        
        $this->db->where_in('id', $id);
        $this->db->update('asset_pre_registration', $data);
        $insert_id = $this->addNewAssetRegistration($id);
        return TRUE;
    }

    function addNewAssetRegistration($id)
    {
        $query = $this->db->select('*')->from('asset_pre_registration')->where('id',$id)->get();
        $row = $query->row();
        
            unset($row->id);
            unset($row->reason);
            $generated_number = $this->generateAssetRegistrationCode();
            $row->code = $generated_number;
            $row->id_pre_registration = $id;
            // echo "<Pre>";print_r($row);exit();
            $this->db->insert('asset_registration',$row);
            $insert_id = $this->db->insert_id();

        $this->db->trans_complete();
        return $insert_id;
    }

    function generateAssetRegistrationCode()
    {
        $year = date('y');
        $Year = date('Y');
            $this->db->select('*');
            $this->db->from('asset_registration');
            $this->db->order_by("id", "desc");
            $query = $this->db->get();
            $result = $query->num_rows();

     
            $count= $result + 1;
           $generate_number = $number = "ASSET" .(sprintf("%'06d", $count)). "/" . $Year;
           return $generate_number;        
    }
}
