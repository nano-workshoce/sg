<?php $this->load->helper("form"); ?>
<form id="form_pr_entry" action="" method="post">
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
            <div class="page-title clearfix">
                <h3>View Asset Disposal</h3>
            </div>

        <div class="form-container">
            <h4 class="form-group-title">Asset Disposal</h4>


            <div class="row"> 

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Department <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $assetDisposal->department_code . " - " . $assetDisposal->department_name;?>" readonly="readonly">
                    </div>
                </div>



                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Staff <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $assetDisposal->ic_no . " - " . $assetDisposal->staff_name;?>" readonly="readonly">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Disposal Type <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $assetDisposal->disposal_type_code . " - " . $assetDisposal->disposal_type_name;?>" readonly="readonly">
                    </div>
                </div>
            </div>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Reason <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="reason" name="description" value="<?php echo $assetDisposal->reason;?>" readonly="readonly">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Disposal Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="pr_entry_date" name="pr_entry_date" value="<?php echo date('d-m-Y', strtotime($assetDisposal->date_time));?>" readonly="readonly">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Status <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php 
                        if($assetDisposal->status == '0')
                        {
                            echo "Pending";
                        }
                        elseif($assetDisposal->status == '1')
                        {
                            echo "Approved";
                        } 
                        elseif($assetDisposal->status == '2')
                        {
                            echo "Rejected";
                        } 
                        ?>
                        " readonly="readonly">
                    </div>
                </div>
            </div>

        </div>


        <h3>Asset Disposal Details</h3>

    <div class="form-container">
            <h4 class="form-group-title">Asset Disposal Details</h4>

        <div class="custom-table">
            <table class="table">
                <thead>
                     <tr>
                        <th>Sl. No</th>
                        <th>Asset Name</th>
                        <th>Asset Code</th>
                        <th>Comapny</th>
                        <th>Brand</th>
                        <th>Price</th>
                        <th>Depriciation Code</th>
                        <th>Depriciation Value</th>
                        <th>Status</th>
                     </tr>
                </thead>
                <tbody>

                     <?php for($i=0;$i<count($assetDisposalDetails);$i++) { ?>
                        <tr>
                        <td><?php echo $i+1;?></td>
                        <td><?php echo $assetDisposalDetails[$i]->name;?></td>
                        <td><?php echo $assetDisposalDetails[$i]->asset_code;?></td>
                        <td><?php echo $assetDisposalDetails[$i]->company;?></td>
                        <td><?php echo $assetDisposalDetails[$i]->brand;?></td>
                        <td><?php echo $assetDisposalDetails[$i]->price;?></td>
                        <td><?php echo $assetDisposalDetails[$i]->depriciation_value;?></td>
                        <td><?php echo $assetDisposalDetails[$i]->depriciation_code;?></td>
                        <td><?php 
                        if($assetDisposalDetails[$i]->status == '0')
                            {
                                echo "Pending";
                            }
                            elseif($assetDisposal->status == '1')
                            {
                                echo "Approved";
                            } 
                            elseif($assetDisposal->status == '2')
                            {
                                echo "Rejected";
                            } 
                        ?></td>
                         </tr>
                      <?php } ?>

                </tbody>
            </table>
        </div>
    </div>


        <div class="button-block clearfix">
            <div class="bttn-group">
                <!-- <button type="submit" class="btn btn-primary btn-lg">Save</button> -->
                <a href="../list" class="btn btn-link">Back</a>
            </div>
        </div>
        
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>