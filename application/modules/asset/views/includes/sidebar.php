            <div class="sidebar">
                <div class="user-profile clearfix">
                    <a href="#" class="user-profile-link">
                        <span><img src="<?php echo BASE_PATH; ?>assets/img/user_profile.jpg"></span> <?php echo $name; ?>
                    </a>
                    <div class="dropdown">
                        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
                            <li><a href="/setup/user/profile">Edit Profile</a></li>
                            <li><a href="/setup/user/logout">Logout</a></li>
                        </ul>
                    </div>
                </div>
                <div class="sidebar-nav">
                    <h4>Setup</h4>
                    <ul>
                        <li><a href="/asset/assetCategory/list">Asset Category</a></li>
                        <li><a href="/asset/assetSubCategory/list">Asset Sub-Category</a></li>
                        <li><a href="/asset/assetItem/list">Asset Item</a></li>
                    </ul>
                     <h4>Pre Registration</h4>
                    <ul>
                        <li><a href="/asset/assetOrder/list">GRN</a></li>
                        <li><a href="/asset/assetOrder/approvalList">GRN Approval</a></li>
                        <li><a href="/asset/assetPreRegistration/list">Asset Pre-Registration</a></li>
                        <li><a href="/asset/assetPreRegistration/approvalList">Pre-Registration Approval</a></li>
                        <li><a href="/asset/assetRegistration/list">Asset Registration</a></li>
                    </ul>
                    <!-- <h4>Asset Details</h4>
                    <ul>
                        <li><a href="">Asset Information</a></li>
                        <li><a href="#">Asset Update</a></li>
                        <li><a href="#">Asset Approval</a></li>
                    </ul>
                    <h4>Stock</h4>
                    <ul>
                        <li><a href="#">Apply Srock</a></li>
                        <li><a href="#">Apply Srock Approval</a></li>
                    </ul> -->
                 <h4>Disposal</h4>
                    <ul>
                       
                        <li><a href="/asset/disposalType/list">Disposal Type</a></li>
                        <li><a href="/asset/assetDisposal/list">Asset Disposal</a></li>
                        <li><a href="/asset/assetDisposal/approvalList">Asset Disposal Approval</a></li>
                     </ul> 
                    <!-- <h4>Others</h4>
                    <ul>
                        <li><a href="#">Maintenance</a></li>
                        <li><a href="#">Maintenance Approval</a></li>
                    </ul> -->
                </div>
            </div>