<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class AccountCode extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('account_code_model');
        $this->isLoggedIn();
    }

    function list()
    {

        if ($this->checkAccess('account_code.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;

            $data['accountCodeList'] = $this->account_code_model->accountCodeListSearch($name);
            $this->global['pageTitle'] = 'FIMS : List Account Code';
            //print_r($subjectDetails);exit;
            $this->loadViews("account_code/list", $this->global, $data, NULL);
        }
    }


    function add()
    {
        if ($this->checkAccess('account_code.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $code = $this->security->xss_clean($this->input->post('code'));
                $name = $this->security->xss_clean($this->input->post('name'));

            
                $data = array(
                    'code' => $code,
                    'short_code' => $code,
                    'level' => 1,
                    'name' => $name
                );
                // ,
                    // 'status' => $status

                // $duplicate_row = $this->account_code_model->checkAccountCodeDuplication($data);

                // if($duplicate_row)
                // {
                //     echo "Duplicate Account Code Not Allowed";exit();
                // }
            
                $result = $this->account_code_model->addNewAccountCode($data);
                // redirect(['HTTP_REFERER']);
                redirect('/glsetup/accountCode/add');
            }
            $activity['level'] = '1';
            $activity['parent'] = '';

            $data['accountCodeList'] = $this->account_code_model->accountCodeListByLevel($activity);   
            $this->global['pageTitle'] = 'FIMS : Add Account Code';
            $this->loadViews("account_code/add", $this->global, $data, NULL);
        }
    }

    function level2($id = NULL)
    {
        if ($this->checkAccess('account_code.level2') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $code1 = $this->security->xss_clean($this->input->post('code1'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $name = $this->security->xss_clean($this->input->post('name'));
                $status = $this->security->xss_clean($this->input->post('status'));

                $complete_code = $code1 . $code;

            
                $data = array(
                    'code' => $complete_code,
                    'short_code' => $code,
                    'level' => 2,
                    'id_parent' => $id,
                    'name' => $name
                );
                // ,
                    // 'status' => $status

                // $duplicate_row = $this->account_code_model->checkAccountCodeDuplication($data);

                // if($duplicate_row)
                // {
                //     echo "Duplicate Account Code Not Allowed";exit();
                // }
            
                $result = $this->account_code_model->addNewAccountCode($data);
                // redirect(['HTTP_REFERER']);
                redirect('/glsetup/accountCode/level2/'.$id);
            }
            $activity['level'] = '2';
            $activity['parent'] = $id;

            $data['accountCode'] = $this->account_code_model->getAccountCode($id);   
            $data['accountCodeList'] = $this->account_code_model->accountCodeListByLevel($activity);   
            $this->global['pageTitle'] = 'FIMS : Add Account Code';
            $this->loadViews("account_code/level2", $this->global, $data, NULL);
        }
    }

    function level3($id,$id_parent)
    {
        if ($this->checkAccess('account_code.level3') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $code1 = $this->security->xss_clean($this->input->post('code1'));
                $code2 = $this->security->xss_clean($this->input->post('code2'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $name = $this->security->xss_clean($this->input->post('name'));
                $status = $this->security->xss_clean($this->input->post('status'));

                $complete_code = $code2 . $code;

                $data = array(
                    'code' => $complete_code,
                    'short_code' => $code,
                    'level' => 3,
                    'id_parent' => $id,
                    'name' => $name
                );
                // ,
                    // 'status' => $status

                // $duplicate_row = $this->account_code_model->checkAccountCodeDuplication($data);

                // if($duplicate_row)
                // {
                //     echo "Duplicate Account Code Not Allowed";exit();
                // }
            
                $result = $this->account_code_model->addNewAccountCode($data);
                // redirect(['HTTP_REFERER']);
                redirect('/glsetup/accountCode/level3/'.$id.'/'.$id_parent);
            }
            $activity['level'] = '3';
            $activity['parent'] = $id;

            $data['accountCode1'] = $this->account_code_model->getAccountCode($id_parent);
            $data['accountCode2'] = $this->account_code_model->getAccountCode($id);
            $data['accountCodeList'] = $this->account_code_model->accountCodeListByLevel($activity);   
            $data['id'] = $id;
            $data['id_parent'] = $id_parent;
            // echo "<Pre>";print_r($data);exit();
            $this->global['pageTitle'] = 'FIMS : Add Account Code';
            $this->loadViews("account_code/level3", $this->global, $data, NULL);
        }
    }

    function level1Edit($id = NULL)
    {
        if ($this->checkAccess('account_code.level1_edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/glsetup/accountCode/add');
            }
            if($this->input->post())
            {
                $code = $this->security->xss_clean($this->input->post('code'));
                $name = $this->security->xss_clean($this->input->post('name'));

            
                $data = array(
                    'code' => $code,
                    'short_code' => $code,
                    'name' => $name
                );
                
                $result = $this->account_code_model->editAccountCode($data,$id);
                redirect('/glsetup/accountCode/add');
            }
            $data['accountCode'] = $this->account_code_model->getAccountCode($id);
            $this->global['pageTitle'] = 'FIMS : Edit Account Level 1 Code';
            $this->loadViews("account_code/level1_edit", $this->global, $data, NULL);
        }
    }


    function level2Edit($id = NULL,$id_parent = NULL)
    {
        if ($this->checkAccess('account_code.level2_edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/glsetup/accountCode/level2/'.$id_parent);
            }
            if($this->input->post())
            {
                $code1 = $this->security->xss_clean($this->input->post('code1'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $name = $this->security->xss_clean($this->input->post('name'));

                $complete_code = $code1 . $code;
            
                $data = array(
                    'short_code' => $code,
                    'code' => $complete_code,
                    'name' => $name
                );
                
                $result = $this->account_code_model->editAccountCode($data,$id);
                redirect('/glsetup/accountCode/level2/'.$id_parent);
            }
            $data['accountCode'] = $this->account_code_model->getAccountCode($id);
            $data['accountCodeParent'] = $this->account_code_model->getAccountCode($id_parent);
            $this->global['pageTitle'] = 'FIMS : Edit Account Level 2 Code';
            $this->loadViews("account_code/level2_edit", $this->global, $data, NULL);
        }
    }

    function level3Edit($id = NULL,$id_parent = NULL,$id_level1 = NULL)
    {
        if ($this->checkAccess('account_code.level3_edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/glsetup/accountCode/level3/'.$id_parent.'/'.$id_level1);
            }
            if($this->input->post())
            {
                $code1 = $this->security->xss_clean($this->input->post('code1'));
                $code2 = $this->security->xss_clean($this->input->post('code2'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $name = $this->security->xss_clean($this->input->post('name'));
            
                $complete_code = $code2 . $code;

                $data = array(
                    'short_code' => $code,
                    'code' => $complete_code,
                    'name' => $name
                );
                
                $result = $this->account_code_model->editAccountCode($data,$id);
                redirect('/glsetup/accountCode/level3/'.$id_parent.'/'.$id_level1);
            }
            $data['accountCode'] = $this->account_code_model->getAccountCode($id);
            $data['accountCodeParent'] = $this->account_code_model->getAccountCode($id_parent);
            $data['accountCodeParentLevel1'] = $this->account_code_model->getAccountCode($id_level1);
            $data['id_level1'] = $id_level1;

            // echo "<Pre>";print_r($data);exit();

            $this->global['pageTitle'] = 'FIMS : Edit Account Level 3 Code';
            $this->loadViews("account_code/level3_edit", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('account_code.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/glsetup/account_code/list');
            }
            if($this->input->post())
            {
                $code = $this->security->xss_clean($this->input->post('code'));
                $name = $this->security->xss_clean($this->input->post('name'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'code' => $code,
                    'name' => $name,
                    'status' => $status
                );

                // $duplicate_row = $this->account_code_model->checkAccountCodeDuplication($data);

                // if($duplicate_row)
                // {
                //     echo "Duplicate Account Code Not Allowed";exit();
                // }
                
                $result = $this->account_code_model->editAccountCode($data,$id);
                redirect('/glsetup/accountCode/list');
            }
            $data['accountCode'] = $this->account_code_model->getAccountCode($id);
            $this->global['pageTitle'] = 'FIMS : Edit Account Code';
            $this->loadViews("account_code/edit", $this->global, $data, NULL);
        }
    }
}
