<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Ledger_model extends CI_Model
{

    function ledgerListSearch($data)
    {
        $this->db->select('l.*, fy.name as financial_year, bty.name as budget_year');
        $this->db->from('ledger as l');
        // $this->db->join('journal as j', 'l.id_journal = j.id');
        $this->db->join('financial_year as fy', 'l.id_financial_year = fy.id');
        $this->db->join('budget_year as bty', 'l.id_budget_year = bty.id');
        if ($data['name'] != '')
        {
            // $likeCriteria = "(l.ledger_number  LIKE '%" . $data['name'] . "%' or l.reason  LIKE '%" . $data['name'] . "%')";
            $likeCriteria = "(l.reason  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['id_financial_year'] != '')
        {
            $this->db->where('l.id_financial_year', $data['id_financial_year']);
        }
        if ($data['id_budget_year'] != '')
        {
            $this->db->where('l.id_budget_year', $data['id_budget_year']);
        }
        if ($data['status'] != '')
        {
            $this->db->where('l.status', $data['status']);
        }
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getLedger($id)
    {
        $this->db->select('l.*, fy.name as financial_year, bty.name as budget_year');
        $this->db->from('ledger as l');
        $this->db->join('financial_year as fy', 'l.id_financial_year = fy.id');
        $this->db->join('budget_year as bty', 'l.id_budget_year = bty.id');
        // $this->db->join('journal as j', 'l.id_journal = j.id');
        $this->db->where('l.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getLedgerDetails($id_ledger)
    {
        $this->db->select('j.*');
        $this->db->from('ledger_details as j');
        $this->db->where('j.id_ledger', $id_ledger);
        $query = $this->db->get();
        return $query->result();
    }
    
    function financialYearListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('financial_year');
        $this->db->where('status', $status);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function budgetYearListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('budget_year');
        $this->db->where('status', $status);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }
}

