<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Journal_model extends CI_Model
{
    function journalList()
    {
        $this->db->select('j.*, fy.year as financial_year');
        $this->db->from('journal as j');
        $this->db->join('financial_year as fy', 'j.id_financial_year = fy.id');
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function journalListSearch($data)
    {
        $this->db->select('j.*, fy.name as financial_year, bty.name as budget_year');
        $this->db->from('journal as j');
        $this->db->join('financial_year as fy', 'j.id_financial_year = fy.id');
        $this->db->join('budget_year as bty', 'j.id_budget_year = bty.id');
        if ($data['name'] != '')
        {
            $likeCriteria = "(j.journal_number  LIKE '%" . $data['name'] . "%' or j.reason  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['id_financial_year'] != '')
        {
            $this->db->where('j.id_financial_year', $data['id_financial_year']);
        }
        if ($data['id_budget_year'] != '')
        {
            $this->db->where('j.id_budget_year', $data['id_budget_year']);
        }
        if ($data['status'] != '')
        {
            $this->db->where('j.status', $data['status']);
        }
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function journalApprovalListSearch($formData)
    {
        $this->db->select('j.*, fy.name as financial_year, bty.name as budget_year');
        $this->db->from('journal as j');
        $this->db->join('financial_year as fy', 'j.id_financial_year = fy.id');
        $this->db->join('budget_year as bty', 'j.id_budget_year = bty.id');
        $this->db->where('j.status=0');

         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getJournal($id)
    {
        $this->db->select('j.*');
        $this->db->from('journal as j');
        $this->db->join('financial_year as fy', 'j.id_financial_year = fy.id');
        $this->db->where('j.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getJournalDetails($id_journal)
    {
        $this->db->select('j.*');
        $this->db->from('journal_details as j');
        $this->db->where('j.id_journal', $id_journal);
        $query = $this->db->get();
        return $query->result();
    }
    
    function addNewJournal($data)
    {
        $this->db->trans_start();
        $this->db->insert('journal', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function addNewLedger($data)
    {
        $this->db->trans_start();
        $this->db->insert('ledger', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }


    function addNewLedgerDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('ledger_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function addNewJournalDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('journal_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function editJournal($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('journal', $data);
        return TRUE;
    }

    function addLedger($id_journal)
    {
        $this->db->select('*');
        $this->db->from('journal');
        $this->db->where('id', $id_journal);
         $query = $this->db->get();
         $journal_data = $query->row();


         $this->db->select('*');
        $this->db->from('journal_details');
        $this->db->where('id_journal', $id_journal);
         $query = $this->db->get();
         $journal_detail_data = $query->result();


         unset($journal_data->journal_number);
         unset($journal_data->id);
         $ledger_number = $this->generateLedgerNumber();
         $journal_data->ledger_number = $ledger_number;
         $journal_data->id_journal = $id_journal;

         $id_ledger = $this->addNewLedger($journal_data);

         if($id_ledger)
         {

             foreach ($journal_detail_data as $value)
             {
                unset($value->id_journal);
                unset($value->id);
                $value->id_ledger = $id_ledger;

                $id_ledger_detail = $this->addNewLedgerDetails($value);
             }

         }



    }

    function studentList()
    {
        $this->db->select('*');
        $this->db->from('student');
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function financialYearListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('financial_year');
        $this->db->where('status', $status);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function budgetYearListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('budget_year');
        $this->db->where('status', $status);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function addNewTempJournalDetails($data)
    {
         // echo "<Pre>";  print_r($data);exit;
        $this->db->trans_start();
        $this->db->insert('temp_journal_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    // function getTempJournalDetails($id_session)
    // {
    //      $this->db->select('tjd.*, dep.name as department_name, dep.code as department_code, acc.name as account_name, acc.code as account_code, actc.name as activity_name, actc.code as activity_code, fc.name as fund_name, fc.code as fund_code');
    //     $this->db->from('temp_journal_details as tjd');
    //     $this->db->join('department_code as dep', 'tjd.department_code = dep.code');        
    //     $this->db->join('account_code as acc', 'tjd.account_code = acc.code');        
    //     $this->db->join('activity_code as actc', 'tjd.activity_code = actc.code');        
    //     $this->db->join('fund_code as fc', 'tjd.fund_code = fc.code');        
    //     $this->db->where('tjd.id_session', $id_session);

    //     $query = $this->db->get();
    //     return $query->result();

    // }

    function getTempJournalDetailsData($id_session)
    {
         $this->db->select('tjd.*');
        $this->db->from('temp_journal_details as tjd');      
        $this->db->where('tjd.id_session', $id_session);

        $query = $this->db->get();
        $journal_details = $query->result();
        return $journal_details;
    }

    function getTempJournalDetailsDataBySession($id_session,$id_journal)
    {
         $this->db->select('tjd.*');
        $this->db->from('temp_journal_details as tjd');      
        $this->db->where('tjd.id_session', $id_session);

        $query = $this->db->get();
        $journal_details = $query->result();
        // return $journal_details;
        foreach ($journal_details as $value)
        {
            unset($value->id_session);
            unset($value->id);
            $value->id_journal = $id_journal;
            $detail_add = $this->addNewJournalDetails($value);
        }

    }

    function deleteTempDataBySession($id_session)
    {
       $this->db->where('id_session', $id_session);
       $this->db->delete('temp_journal_details');
    }

    function deleteTempData($id)
    {
       $this->db->where('id', $id);
       $this->db->delete('temp_journal_details');
    }

    function approveJouranl($array) {
        
        $status = ['status'=>'1'];

        $this->db->where_in('id', $array);
      $this->db->update('journal', $status);

    }
 

    function generateJRNumber($data)
    {

        $year = date('y');
        $Year = date('Y');
     



            $this->db->select('j.*');
            $this->db->from('journal as j');
            $this->db->order_by("id", "desc");
            $query = $this->db->get();
            $result = $query->num_rows();

     
            $count= $result + 1;
           $jrnumber = $number = "JR" .(sprintf("%'06d", $count)). "/" . $Year;
           return $jrnumber;        
    }


    function generateLedgerNumber()
    {

        $year = date('y');
        $Year = date('Y');
     



            $this->db->select('j.*');
            $this->db->from('ledger as j');
            $this->db->order_by("id", "desc");
            $query = $this->db->get();
            $result = $query->num_rows();

     
            $count= $result + 1;
           $jrnumber = $number = "LD" .(sprintf("%'06d", $count)). "/" . $Year;
           return $jrnumber;        
    }

    function updateTempDetails($data,$id) {
         $this->db->where('id', $id);
        $this->db->update('temp_journal_details', $data);
        return TRUE;
    }

    function getTempDetails($id){
        $this->db->select('*');
        $this->db->from('temp_journal_details');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getTempBasedOnSession($sessionid)
    {
        $this->db->select('*');
        $this->db->from('temp_pr_entry');
        $this->db->where('sessionid', $sessionid);
        $query = $this->db->get();
        return $query->result();
    }

   
}

