<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Budget_allocation_model extends CI_Model
{

    function budgetAmountListSearch($data)
    {
        $this->db->select('ba.*, fy.name as financial_year, bty.name as budget_year, d.name as department_name, d.code as department');
        $this->db->from('budget_amount as ba');
        $this->db->join('budget_year as bty','ba.id_budget_year = bty.id');
        $this->db->join('department_code as d','ba.department_code = d.code');
        $this->db->join('financial_year as fy','ba.id_financial_year = fy.id');
        if ($data['id_budget_year']!='')
        {
            $this->db->where('ba.id_budget_year', $data['id_budget_year']);
        }
        if ($data['id_financial_year']!='')
        {
            $this->db->where('ba.id_financial_year', $data['id_financial_year']);
        }
        if ($data['department_code']!='')
        {
            $this->db->where('ba.department_code', $data['department_code']);
        }
        if ($data['status'] == '')
        {
            $this->db->where('ba.status', '1');
        }
        if ($data['status']!='')
        {
            $this->db->where('ba.allocation_status', $data['status']);
            if($data['status'] == '0')
            {
                $this->db->where('ba.status', '1');
                $this->db->where('ba.is_detail_added', '1');
            }
        }
        $this->db->order_by("id", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function checkAllocation($id_budget_amount)
    {
        $this->db->select('ba.*');
        $this->db->from('budget_allocation as ba');
        $this->db->where('ba.id_budget_amount', $id_budget_amount);
        $query = $this->db->get();
        return $query->row();
    }


    function budgetAllocationListSearch($data)
    {
        $this->db->select('ba.*, bty.name as budget_year, d.name as department_name, d.code as department');
        $this->db->from('budget_allocation as ba');
        $this->db->join('budget_year as bty','ba.id_budget_year = bty.id');
        $this->db->join('department as d','ba.id_department = d.id');
        if ($data['name']!='')
        {
            $likeCriteria = "(po.po_number  LIKE '%" . $data['name'] . "%' or ao.reference_number  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['id_budget_year']!='')
        {
            $this->db->where('ba.id_budget_year', $data['id_budget_year']);
        }
        if ($data['id_department']!='')
        {
            $this->db->where('ao.id_department', $data['id_sub_category']);
        }
        $query = $this->db->get();
        return $query->result();
    }

    function financialYearListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('financial_year');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function departmentListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('department');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function budgetYearListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('budget_year');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function fundCodeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('fund_code');
        $this->db->where('status', $status);
        $this->db->order_by("code", "ASC");
        $query = $this->db->get();
        return $query->result();
    }    


    function departmentCodeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('department_code');
        $this->db->where('status', $status);
        $this->db->order_by("code", "ASC");
        $query = $this->db->get();
        return $query->result();

    }    

    function activityCodeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('activity_code');
        $this->db->where('level', '3');
        $this->db->order_by("code", "ASC");
        $query = $this->db->get();
        return $query->result();
    }    

    function accountCodeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('account_code');
        $this->db->where('level', '3');
        $this->db->order_by("code", "ASC");
        $query = $this->db->get();
        return $query->result();
    }    

    function addNewBudgetAllocation($id_session,$id_budget_amount)
    {
        $temp_datas = $this->getTempBudgetAllocationDetailsBySession($id_session);
        foreach ($temp_datas as $temp_data)
        {
           
        unset($temp_data->id_session);
        unset($temp_data->id);
        $temp_data->id_budget_amount = $id_budget_amount;

        $this->addBudgetAllocation($temp_data);        
        }
        return TRUE;
    }


    function getBudgetAllocationByBudgetAmount($id_budget_amount)
    {
        $this->db->select('ba.*');
        $this->db->from('budget_allocation as ba');
        $this->db->where('ba.id_budget_amount', $id_budget_amount);
        $query = $this->db->get();
        return $query->result();
    }



    function getBudgetAllocation($id)
    {
        $this->db->select('ba.*, bty.name as budget_year, d.name as department_name, d.code as department');
        $this->db->from('budget_allocation as ba');
        $this->db->join('budget_year as bty','ba.id_budget_year = bty.id');
        $this->db->join('department as d','ba.id_department = d.id');
        $this->db->where('ba.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function checkDuplicateBudgetAllocation($data)
    {
        $this->db->select('ba.*, bty.name as budget_year, d.name as department_name, d.code as department');
        $this->db->from('budget_allocation as ba');
        $this->db->join('budget_year as bty','ba.id_budget_year = bty.id');
        $this->db->join('department as d','ba.id_department = d.id');
        $this->db->where('ba.id_budget_year', $data['id_budget_year']);
        $this->db->where('ba.id_department', $data['id_department']);
        $this->db->where('ba.fund_code', $data['fund_code']);
        $query = $this->db->get();
        return $query->row();
    }

    function checkTempDuplicateBudgetAllocation($data)
    {
        $this->db->select('ba.*');
        $this->db->from('temp_budget_allocation as ba');
        $this->db->where('ba.id_session', $data['id_session']);
        $this->db->where('ba.fund_code', $data['fund_code']);
        $this->db->where('ba.account_code', $data['account_code']);
        $this->db->where('ba.activity_code', $data['activity_code']);
        $query = $this->db->get();
        return $query->row();
    }

    function addTempBudgetAllocationDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('temp_budget_allocation', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function getTempBudgetAllocationDetailsBySession($id_session)
    {
        $this->db->select('ba.*');
        $this->db->from('temp_budget_allocation as ba');
        $this->db->where('ba.id_session', $id_session);
        $query = $this->db->get();
        return $query->result();
    }

    function deleteTempData($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('temp_budget_allocation');
        return TRUE;
    }

    function deleteBudgetAllocationByMaster($id_budget_amount)
    {
        $this->db->where('id_budget_amount', $id_budget_amount);
        $this->db->delete('budget_allocation');
        return TRUE;
    }

    function deleteData($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('budget_allocation');
        return TRUE;
    }

    function deleteTempDetailsBySession($id_session)
    {
        $this->db->where('id_session', $id_session);
        $this->db->delete('temp_budget_allocation');
        return TRUE;
    }

    function addBudgetAllocation($data)
    {
        $this->db->trans_start();
        $this->db->insert('budget_allocation', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        // return $insert_id;
    }

    function editBudgetAllocationByMaster($data,$id_budget_amount)
    {
        $this->db->where('id_budget_amount', $id_budget_amount);
        $this->db->update('budget_allocation', $data);
        return TRUE;
    }

    function moveDetailToTemp($id_budget_amount)
    {
        $id_session = $this->session->my_session_id;

        $this->db->select('ba.*');
        $this->db->from('budget_allocation as ba');
        $this->db->where('ba.id_budget_amount', $id_budget_amount);
        $query = $this->db->get();
        $detail_data = $query->result();
        // print_r($detail_data);exit();

        foreach ($detail_data as $value)
        {
            unset($value->id_budget_amount);
            unset($value->id);
            $value->id_session = $id_session;
            $added = $this->addTempBudgetAllocationDetails($value);
        }
    }

    function updateBudgetAmount($data,$id_budget_amount)
    {
        $this->db->where('id', $id_budget_amount);
        $this->db->update('budget_amount', $data);
        return TRUE;
    }
























    // function budgetAmountListSearch($data)
    // {
    //     $this->db->select('ba.*, fy.name as financial_year, bty.name as budget_year, d.name as department_name, d.code as department');
    //     $this->db->from('budget_amount as ba');
    //     $this->db->join('budget_year as bty','ba.id_budget_year = bty.id');
    //     $this->db->join('department_code as d','ba.department_code = d.code');
    //     $this->db->join('financial_year as fy','ba.id_financial_year = fy.id');
    //     if ($data['id_budget_year']!='')
    //     {
    //         $this->db->where('ba.id_budget_year', $data['id_budget_year']);
    //     }
    //     if ($data['id_financial_year']!='')
    //     {
    //         $this->db->where('ba.id_financial_year', $data['id_financial_year']);
    //     }
    //     if ($data['department_code']!='')
    //     {
    //         $this->db->where('ba.department_code', $data['department_code']);
    //     }
    //     if ($data['status']!='')
    //     {
    //         $this->db->where('ba.status', $data['status']);
    //     }
    //     $query = $this->db->get();
    //     $result = $query->result();

    //     $result_data = array();
    //     foreach ($result as $value)
    //     {
    //         $value->check_allocation = 0;
    //         $id = $value->id;
    //         $check_allocation = $this->checkAllocation($id);
    //         if($check_allocation)
    //         {
    //             $value->check_allocation = 1;
    //         }
    //         array_push($result_data, $value);
    //     }

    //     return $result_data;
    // }
}
