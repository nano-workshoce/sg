<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Budget_transaction_model extends CI_Model
{

    function budgetTransactionListSearch($data)
    {
        $this->db->select('ba.*, bty.name as budget_year, fy.name as financial_year, d.name as department_name, d.code as department_code');
        $this->db->from('budget_transaction as ba');
       $this->db->join('budget_year as bty','ba.id_budget_year = bty.id');
        $this->db->join('financial_year as fy','ba.id_financial_year = fy.id');
        $this->db->join('department_code as d','ba.department_code = d.code');
        if ($data['id_budget_year']!='')
        {
            $this->db->where('ba.id_budget_year', $data['id_budget_year']);
        }
        if ($data['department_code']!='')
        {
            $this->db->where('ba.department_code', $data['department_code']);
        }
        if ($data['id_financial_year']!='')
        {
            $this->db->where('ba.id_financial_year', $data['id_financial_year']);
        }
        if ($data['status']!='')
        {
            $this->db->where('ba.status', $data['status']);
        }
        $this->db->order_by("ba.id", "ASC");
        $query = $this->db->get();
        $result = $query->result();
        return $result;


        $array_data = array();
        foreach ($result as $value)
        {
            $id_credit_budget = $value->id_credit_budget;
            $id_debit_budget = $value->id_debit_budget;

            $credit_budget = $this->getBudgetAmountById($id_credit_budget);
            $debit_budget = $this->getBudgetAmountById($id_debit_budget);

            if($credit_budget && $debit_budget)
            {



            $transaction_data = new \stdClass();

            $transaction_data->credit_financial_year = $credit_budget->financial_year;
            $transaction_data->credit_budget_year = $credit_budget->budget_year;
            $transaction_data->credit_department_name = $credit_budget->department_name;
            $transaction_data->credit_department_code = $credit_budget->department_code;


            $transaction_data->debit_financial_year = $debit_budget->financial_year;
            $transaction_data->debit_budget_year = $debit_budget->budget_year;
            $transaction_data->debit_department_name = $debit_budget->department_name;
            $transaction_data->debit_department_code = $debit_budget->department_code;
            $transaction_data->amount = $value->total_amount;
            $transaction_data->status = $value->status;
            $transaction_data->id = $value->id;
            $transaction_data->reason = $value->reason;

            array_push($array_data, $transaction_data);
            }
        }
        return $array_data;
            // echo "<Pre>";print_r($array_data);exit();
    }

    function getBudgetAmountById($id)
    {
        $this->db->select('ba.*, bty.name as budget_year, fy.name as financial_year, d.name as department_name, d.code as department_code');
        $this->db->from('budget_amount as ba');
        $this->db->join('budget_year as bty','ba.id_budget_year = bty.id');
        $this->db->join('financial_year as fy','ba.id_financial_year = fy.id');
        $this->db->join('department_code as d','ba.department_code = d.code');
        $this->db->where('ba.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function financialYearListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('financial_year');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function departmentListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('department');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function budgetYearListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('budget_year');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function fundCodeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('fund_code');
        $this->db->where('status', $status);
        $this->db->order_by("code", "ASC");
        $query = $this->db->get();
        return $query->result();
    }    


    function departmentCodeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('department_code');
        $this->db->where('status', $status);
        $this->db->order_by("code", "ASC");
        $query = $this->db->get();
        return $query->result();

    }    

    function activityCodeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('activity_code');
        $this->db->where('level', '3');
        $this->db->order_by("code", "ASC");
        $query = $this->db->get();
        return $query->result();
    }    

    function accountCodeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('account_code');
        $this->db->where('level', '3');
        $this->db->order_by("code", "ASC");
        $query = $this->db->get();
        return $query->result();
    }    

    function addNewBudgetAllocation($data)
    {
        $this->db->trans_start();
        $this->db->insert('budget_allocation', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }


    function getBudgetAllocation($id)
    {
        $this->db->select('ba.*');
        $this->db->from('budget_allocation as ba');
        $this->db->where('ba.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getDebitBudget($data)
    {
        $this->db->select('ba.*, bty.name as budget_year, d.name as department_name, d.code as department');
        $this->db->from('budget_amount as ba');
        $this->db->join('budget_year as bty','ba.id_budget_year = bty.id');
        $this->db->join('department_code as d','ba.department_code = d.code');
        $this->db->where('ba.id_financial_year', $data['id_dt_financial_year']);
        $this->db->where('ba.department_code', $data['id_dt_department']);
        $this->db->where('ba.id_budget_year', $data['id_dt_budget_year']);
        $this->db->where('ba.status', '1');
        $query = $this->db->get();
        return $query->row();
    }

    function getCreditBudget($data)
    {
        $this->db->select('ba.*, bty.name as budget_year, d.name as department_name, d.code as department');
        $this->db->from('budget_amount as ba');
        $this->db->join('budget_year as bty','ba.id_budget_year = bty.id');
        $this->db->join('department_code as d','ba.department_code = d.code');
        $this->db->where('ba.id_financial_year', $data['id_cr_financial_year']);
        $this->db->where('ba.id_budget_year', $data['id_cr_budget_year']);
        $this->db->where('ba.department_code', $data['id_cr_department']);
        $this->db->where('ba.status', '1');
        $query = $this->db->get();
        return $query->row();
    }


     function getBudgetAllocationByBudgetAmount($id_budget_amount)
    {
        $this->db->select('DISTINCT(ba.fund_code) as fund_code');
        $this->db->from('budget_allocation as ba');
        $this->db->join('fund_code as fc','ba.fund_code = fc.code');
        $this->db->where('ba.id_budget_amount', $id_budget_amount);
        $query = $this->db->get();
        return $query->result();
    }

    function getFundNameByCode($code)
    {
        $this->db->select('ba.name as fund_name');
        $this->db->from('fund_code as ba');
        $this->db->where('ba.code', $code);
        $query = $this->db->get();
        return $query->row();
    }

    function getBudgetAllocationByBudgetAmounNFund($id_budget_amount,$fund_code)
    {
        $this->db->select('DISTINCT(ba.activity_code) as activity_code');
        $this->db->from('budget_allocation as ba');
        $this->db->join('fund_code as fc','ba.fund_code = fc.code');
        $this->db->where('ba.id_budget_amount', $id_budget_amount);
        $this->db->where('ba.fund_code', $fund_code);
        $query = $this->db->get();
        return $query->result();
    }

     function getActivityNameByCode($code)
    {
        $this->db->select('ba.name as activity_name');
        $this->db->from('activity_code as ba');
        $this->db->where('ba.code', $code);
        $query = $this->db->get();
        return $query->row();
    }
    

    function getBudgetAllocationByBudgetAmounNFundNActivity($id_budget_amount,$fund_code,$activity_code)
    {
        $this->db->select('DISTINCT(ba.account_code) as account_code');
        $this->db->from('budget_allocation as ba');
        $this->db->join('fund_code as fc','ba.fund_code = fc.code');
        $this->db->where('ba.id_budget_amount', $id_budget_amount);
        $this->db->where('ba.fund_code', $fund_code);
        $this->db->where('ba.activity_code', $activity_code);
        $query = $this->db->get();
        return $query->result();
    }

     function getAccountNameByCode($code)
    {
        $this->db->select('ba.name as account_name');
        $this->db->from('account_code as ba');
        $this->db->where('ba.code', $code);
        $query = $this->db->get();
        return $query->row();
    }

    function getBudgetAllocationByCodes($data)
    {
         $this->db->select('ba.*, fc.name as fund_name, ac.name as account_name, actc.name as activity_name');
        $this->db->from('budget_allocation as ba');
        $this->db->join('fund_code as fc','ba.fund_code = fc.code');
        $this->db->join('account_code as ac','ba.account_code = ac.code');
        $this->db->join('activity_code as actc','ba.activity_code = actc.code');
        $this->db->where('ba.id_budget_amount', $data['dt_id_budget_amount']);
        $this->db->where('ba.fund_code', $data['dt_fund_code']);
        $this->db->where('ba.activity_code', $data['dt_activity_code']);
        $this->db->where('ba.account_code', $data['dt_account_code']);
        $query = $this->db->get();
        return $query->row();
    }

    function getBudgetAmount($id_budget_year, $id_department, $id_financial_year)
    {
         $this->db->select('ba.*');
        $this->db->from('budget_amount as ba');
        $this->db->where('ba.id_financial_year', $id_financial_year);
        $this->db->where('ba.id_budget_year', $id_budget_year);
        $this->db->where('ba.department_code', $id_department);
        $this->db->where('ba.status', '1');
        $query = $this->db->get();
        return $query->row();
    }

    function addNewBudgetTransaction($data)
    {
        $this->db->trans_start();
        $this->db->insert('budget_transaction', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;        
    }

    function addNewBudgetAllocationByTransaction($data)
    {
        $this->db->trans_start();
        $this->db->insert('budget_allocation', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;        
    }


    function addNewBudgetTransactionDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('budget_transaction_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;        
    }


    function updateBudgetTransaction($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('budget_transaction', $data);
        return TRUE;
    }

    function updateBudgetAllocation($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('budget_allocation', $data);
        return TRUE;
    }

    function updateBudgetAmount($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('budget_amount', $data);
        return TRUE;
    }

    function getBudgetTransaction($id)
    {
         $this->db->select('ba.*, bty.name as budget_year, fy.name as financial_year, d.name as department_name, d.code as department_code*');
         $this->db->from('budget_transaction as ba');
        $this->db->join('budget_year as bty','ba.id_budget_year = bty.id');
        $this->db->join('financial_year as fy','ba.id_financial_year = fy.id');
        $this->db->join('department_code as d','ba.department_code = d.code');
        $this->db->where('ba.id', $id);
        $query = $this->db->get();
        $value = $query->row();

        return $value;






        $id_credit_budget = $value->id_credit_budget;
        $id_debit_budget = $value->id_debit_budget;

        $credit_budget = $this->getBudgetAmountById($id_credit_budget);
        $debit_budget = $this->getBudgetAmountById($id_debit_budget);

        // echo "<Pre>";print_r($credit_budget);exit();

        $transaction_data = new \stdClass();

        $transaction_data->credit_financial_year = $credit_budget->financial_year;
        $transaction_data->credit_budget_year = $credit_budget->budget_year;
        $transaction_data->credit_department_name = $credit_budget->department_name;
        $transaction_data->credit_department_code = $credit_budget->department_code;


        $transaction_data->debit_financial_year = $debit_budget->financial_year;
        $transaction_data->debit_budget_year = $debit_budget->budget_year;
        $transaction_data->debit_department_name = $debit_budget->department_name;
        $transaction_data->debit_department_code = $debit_budget->department_code;
        $transaction_data->amount = $value->total_amount;
        $transaction_data->status = $value->status;
        $transaction_data->id = $value->id;
        $transaction_data->reason = $value->reason;

        return $transaction_data;
    }


    function getBudgetTransactionDetails($id_budget_transaction)
    {
        $this->db->select('bts.*');
        $this->db->from('budget_transaction_details as bts');
        $this->db->where('bts.id_budget_transaction', $id_budget_transaction);
        $query = $this->db->get();
        return $query->result();
    }

    function updateBudgetAllocationAmount($id_transaction)
    {
        $transaction_data = $this->getBudgetTransaction($id_transaction);


        $id_budget_amount = $transaction_data->id_budget_amount;
        $id_budget_year = $transaction_data->id_budget_year;
        $id_financial_year = $transaction_data->id_financial_year;
        // echo "<Pre>";print_r($id_budget_amount);exit();
       
        $id_budget_allocation = $transaction_data->id_budget_allocation;
        $amount = $transaction_data->amount;


        $data_allocation = $this->getBudgetAllocation($id_budget_allocation);
        // echo "<Pre>";print_r($data_allocation);exit();

        
        $old_allocation_balance_amount = $data_allocation->balance_amount;
        $old_allocation_increment_amount = $data_allocation->increment_amount;
        $old_allocation_balance_amount = $data_allocation->balance_amount;
        $old_allocation_decrement_amount = $data_allocation->decrement_amount;

        $new_allocation_balance_amount = $old_allocation_balance_amount - $amount;
        $new_allocation_decrement_amount = $old_allocation_decrement_amount + $amount;

        $updated_budget_allocation_data = array(
            'decrement_amount' => $new_allocation_decrement_amount,
            'balance_amount' => $new_allocation_balance_amount
        );

        $update_allocation = $this->updateBudgetAllocation($updated_budget_allocation_data,$id_budget_allocation);

        if($update_allocation)
        {


             $details = $this->getBudgetTransactionDetails($id_transaction);
            foreach ($details as $detail)
            {
            // echo "<Pre>";print_r($detail);exit();

                $transaction_detail_amount = $detail->balance_amount;


                $get_data['id_dt_financial_year'] = $id_financial_year;
                $get_data['id_dt_department'] = $detail->department_code;
                $get_data['id_dt_budget_year'] = $id_budget_year;
            // echo "<Pre>";print_r($get_data);exit();

                $budget_amount_data = $this->getDebitBudget($get_data);


                $id_budget_amount = $budget_amount_data->id;

                $old_budget_allocation_amount = $budget_amount_data->amount;
                $old_budget_balance_amount = $budget_amount_data->balance_amount;
                $old_budget_increment_amount = $budget_amount_data->increment_amount;
                $old_budget_decrement_amount = $budget_amount_data->decrement_amount;


                $new_budget_balance_amount = $old_budget_balance_amount + $transaction_detail_amount;
                $new_budget_increment_amount = $old_budget_increment_amount + $transaction_detail_amount;

                $updated_budget_amount_data = array(
                        'increment_amount' => $new_budget_increment_amount,
                        'balance_amount' => $new_budget_balance_amount
                    );

                $updated_budget_amount = $this->updateBudgetAmount($updated_budget_amount_data,$id_budget_amount);


                if($updated_budget_amount)
                {


                $detail->id_transaction_detail = $detail->id;
                $detail->id_budget_amount = $id_budget_amount;
                $detail->budget_amount = $old_budget_allocation_amount;
                $detail->increment_amount =  $detail->balance_amount;


                unset($detail->id);
                unset($detail->id_budget_transaction);
                unset($detail->created_dt_tm);
                
                $details = $this->addNewBudgetAllocationByTransaction($detail);

                }
                // echo "<Pre>";print_r($data_allocation);exit();
            }

            $allocation_details_data_by_amount_id = $this->getBudgetAllocationDetailsByAmountIdForBalanceTotal($id_budget_amount);

            // echo "<Pre>";print_r($allocation_details_data_by_amount_id);exit();


            $update_allocation_data = array(
                'balance_amount' => $allocation_details_data_by_amount_id->ba
            );

            $update_allocation = $this->updateBudgetAmount($update_allocation_data,$id_budget_amount);
           


            // $transaction_amount = $transaction_data->amount;



            // $allocation_amount = $data_allocation->allocated_amount;
            // $allocation_balance = $data_allocation->balance_amount;
            // $allocation_used = $data_allocation->used_amount;
            // $allocation_decrement = $data_allocation->decrement_amount;
            // $allocation_increment = $data_allocation->increment_amount;


            // $new_allocation_balance = $allocation_balance - $transaction_amount;
            // $new_allocation_decrement = $allocation_decrement + $transaction_amount;
            // $new_allocation_used = $allocation_used + $transaction_amount;
        }
    }

    function getBudgetAllocationDetailsByAmountIdForBalanceTotal($id_budget_amount)
    {
        $this->db->select('SUM(ba.balance_amount) as ba');
        $this->db->from('budget_allocation as ba');
        $this->db->where('ba.id_budget_amount', $id_budget_amount);
        $query = $this->db->get();
        return $query->row();
    }


    function addTempBudgetTransactionDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('temp_budget_transaction_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;    
    }

    function getTempBudgetTransactionDetailsBySession($id_session)
    {
        $this->db->select(' bts.*');
        $this->db->from('temp_budget_transaction_details as bts');
        $this->db->where('bts.id_session', $id_session);
        $query = $this->db->get();
        return $query->result();
    }

    function deleteTempBudgetTransactionDetailsById($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('temp_budget_transaction_details');
        return TRUE;
    }

    function deleteTempBudgetTransactionDetailsBySession($id_session)
    {
        $this->db->where('id_session', $id_session);
        $this->db->delete('temp_budget_transaction_details');
        return TRUE;
    }

    function checkDepartmentAvailable($data)
    {
        $this->db->select('ba.*, fy.name as financial_year, bty.name as budget_year, d.name as department_name, d.code as department');
        $this->db->from('budget_amount as ba');
        $this->db->join('budget_year as bty','ba.id_budget_year = bty.id');
        $this->db->join('financial_year as fy','ba.id_financial_year = fy.id');
        $this->db->join('department_code as d','ba.department_code = d.code');
        $this->db->where('ba.id_financial_year', $data['id_dt_financial_year']);
        $this->db->where('ba.department_code', $data['department_code']);
        $this->db->where('ba.id_budget_year', $data['id_dt_budget_year']);
        $this->db->where('ba.status', '1');
        $query = $this->db->get();
        return $query->row();
    }

    function moveBudgetTransactionDetailsTempTDetail($id_transaction)
    {
        $id_session = $this->session->my_session_id;
        $temp_details_data = $this->getTempBudgetTransactionDetailsBySession($id_session);
        foreach ($temp_details_data as $value)
        {
            unset($value->id_session);
            unset($value->id);
            unset($value->id_session);
            $value->id_budget_transaction = $id_transaction;

            $added_detail = $this->addNewBudgetTransactionDetails($value);
        }

        $deleted_temp = $this->deleteTempBudgetTransactionDetailsBySession($id_session);
    }

    function departmentCodeForTransfer($data)
    {
        $this->db->select('ba.*,d.name as department_name, d.code as department');
        $this->db->from('budget_amount as ba');        
        $this->db->join('department_code as d','ba.department_code = d.code');
        // $this->db->where('ba.id_financial_year', $data['id_dt_financial_year']);
        // $this->db->where('ba.id_budget_year', $data['id_dt_budget_year']);
        $query = $this->db->get();
        return $query->result();
    }

    function getBudgetTransactionReport($data)
    {
        // echo "<Pre>";print_r($data['department_code']);exit;

        $this->db->select('ba.*');
        $this->db->from('budget_allocation as ba');
        $this->db->join('budget_amount as bua','ba.id_budget_amount = bua.id');
        $this->db->join('budget_year as bty','bua.id_budget_year = bty.id');
        $this->db->join('financial_year as fy','bua.id_financial_year = fy.id');
        $this->db->where('bua.id_financial_year', $data['id_financial_year']);
        $this->db->where('bua.id_budget_year', $data['id_budget_year']);
        $this->db->where('ba.department_code', $data['department_code']);
         $query = $this->db->get();
        return $query->result();
        // echo "<Pre>";print_r($query->result());exit;
        
    }
}
