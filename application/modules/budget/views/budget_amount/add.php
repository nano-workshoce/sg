<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Budget Amount</h3>
        </div>
        <form id="form_subject" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Budget Amount Details</h4>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Financial Year <span class='error-text'>*</span></label>
                        <select name="id_financial_year" id="id_financial_year" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($financialYearList))
                            {
                                foreach ($financialYearList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>  

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Budget Year <span class='error-text'>*</span></label>
                        <select name="id_budget_year" id="id_budget_year" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($budgetYearList))
                            {
                                foreach ($budgetYearList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>  


                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Department Code <span class='error-text'>*</span></label>
                        <select name="department_code" id="department_code" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($departmentCodeList))
                            {
                                foreach ($departmentCodeList as $record)
                                {?>
                             <option value="<?php echo $record->code;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>   

            </div>

            <div class="row">  

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Total Amount <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="amount" name="amount">
                    </div>
                </div>
               
            </div>

        </div>
            
            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="list" class="btn btn-link">Cancel</a>
                </div>
            </div>
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
    </div>
</div>



<script>

    $('select').select2();


    $(document).ready(function()
    {
        $("#form_subject").validate(
        {
            rules:
            {
                id_budget_year:
                {
                    required: true
                },
                department_code:
                {
                    required: true
                },
                fund_code:
                {
                    required: true
                },
                department_code:
                {
                    required: true
                },
                activity_code:
                {
                    required: true
                },
                account_code:
                {
                    required: true
                },
                budget_amount:
                {
                    required: true
                },
                allocated_amount:
                {
                    required: true
                }
            },
            messages:
            {
                id_budget_year:
                {
                    required: "<p class='error-text'>Select Budget Year</p>",
                },
                department_code:
                {
                    required: "<p class='error-text'>Select Department Code</p>",
                },
                fund_code:
                {
                    required: "<p class='error-text'>Select Fund Code</p>",
                },
                department_code:
                {
                    required: "<p class='error-text'>Select Department Code</p>",
                },
                activity_code:
                {
                    required: "<p class='error-text'>Select Activity Code</p>",
                },
                account_code:
                {
                    required: "<p class='error-text'>Select Account Code</p>",
                },
                budget_amount:
                {
                    required: "<p class='error-text'>Enter Budget Amount</p>",
                },
                allocated_amount:
                {
                    required: "<p class='error-text'>Enter Allocated Amount</p>",
                }

            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>