<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>View Budget Amount</h3>
        </div>
        <form id="form_subject" action="" method="post">


         <div class="form-container">
            <h4 class="form-group-title">Budget Amount Details</h4>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Financial Year <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name" value="<?php echo $budgetAmount->financial_year;?>" readonly>
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Budget Year <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name" value="<?php echo $budgetAmount->budget_year;?>" readonly>
                    </div>
                </div>



                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Department Code<span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name" value="<?php echo $budgetAmount->department . " - " . $budgetAmount->department_name;?>" readonly>
                    </div>
                </div>
            </div>


             <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Allocated Amount <span class='error-text'>*</span></label>
                       <input type="text" class="form-control" id="name" name="name" value="<?php echo $budgetAmount->amount; ?>" readonly>
                          <!-- . " - " . $budgetAmount->name;?>"> -->
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Balance Amount <span class='error-text'>*</span></label>
                       <input type="text" class="form-control" id="name" name="name" value="<?php echo $budgetAmount->balance_amount; ?>" readonly>
                          <!-- . " - " . $budgetAmount->name;?>"> -->
                    </div>
                </div>


               <!--  <div class="col-sm-4">
                    <div class="form-group">
                        <label>Increment Amount <span class='error-text'>*</span></label>
                       <input type="text" class="form-control" id="name" name="name" value="<?php echo $budgetAmount->increment_amount; ?>" readonly>
                    </div>
                </div> -->

            </div>


             <div class="row">
                
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Status</label>
                       <input type="text" class="form-control" id="name" name="name" value="<?php 
                       if( $budgetAmount->status == '0')
                      {
                        echo "Pending";
                      }
                      elseif($budgetAmount->status == '1')
                      {
                        echo "Approved";
                      } elseif($budgetAmount->status == '2')
                      {
                        echo "Rejected";
                      } 

                     ?>" readonly>
                    </div>
                </div>


                  <?php
            if($budgetAmount->status == '2')
            {
             ?>

                <div class="col-sm-4" id="view_reject">
                    <div class="form-group">
                        <label>Reject Reason <span class='error-text'>*</span></label>
                        <input type="text" id="reason" name="reason" class="form-control" value="<?php echo $budgetAmount->reason; ?>" readonly>
                    </div>
                </div>
           

            <?php
            }?>

            </div>

        </div>



        <div class="button-block clearfix">
            <div class="bttn-group">
                <!-- <button type="submit" class="btn btn-primary btn-lg">Save</button> -->
                <a href="../list" class="btn btn-link">Back</a>
            </div>
        </div>

        </form>
        
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
    </div>
</div>

<script>
    $(document).ready(function()
    {
        $("#form_subject").validate(
        {
            rules:
            {
                name:
                {
                    required: true
                }
            },
            messages:
            {
                name:
                {
                    required: "Subject Name Required",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>