<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add budget Year</h3>
        </div>
        <form id="form_academic_year" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Budget Year Details</h4>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Budget Year <span class='error-text'>*</span></label>
                        <br>
                        <input type="number" pattern="\d*" class="form-control"  id="name" name="name" autocomplete="off" min="<?php echo date('Y'); ?>" max="<?php echo date('Y')+4; ?>"  style="width: 360px; height: 30px">
                    </div>
                </div>

                <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" checked="checked"><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0"><span class="check-radio"></span> Inactive
                            </label>                              
                        </div>                         
                </div>

            </div>

        </div>
         

        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="list" class="btn btn-link">Cancel</a>
            </div>
        </div>
        
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>
    $(document).ready(function() {
        $("#form_academic_year").validate({
            rules: {
                name: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>Budget Year required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
