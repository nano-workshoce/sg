<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>View Budget Transaction</h3>
        </div>
        <form id="form_subject" action="" method="post">



            <!-- <h3>From Budget Allocation</h3> -->
            <h3>From Budget Allocation</h3>

          

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Budget Year <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name" value="<?php echo $budgetTransaction->budget_year;?>" readonly>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Financial Year <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name" value="<?php echo $budgetTransaction->financial_year;?>" readonly>
                    </div>
                </div>

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Department <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name" value="<?php echo $budgetTransaction->department_code . " - " . $budgetTransaction->department_name;?>" readonly>
                    </div>
                </div>
               
            </div>     




            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Fund Code <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name" value="<?php echo $budgetAllocation->fund_code;?>" readonly>
                    </div>
                </div>


                  <div class="col-sm-4">
                    <div class="form-group">
                        <label>Account Code <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name" value="<?php echo $budgetAllocation->account_code;?>" readonly>
                    </div>
                </div>              


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Activity Code <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name" value="<?php echo $budgetAllocation->activity_code;?>" readonly>
                    </div>
                </div>

               
            </div>            

            
             <div class="row">

             


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Total Transaction Amount <span class='error-text'>*</span></label>
                       <input type="text" class="form-control" id="name" name="name" value="<?php echo $budgetTransaction->amount; ?>" readonly>
                          <!-- . " - " . $budgetTransaction->name;?>"> -->
                    </div>
                </div>

                

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Status</label>
                       <input type="text" class="form-control" id="name" name="name" value="<?php 
                       if( $budgetTransaction->status == '0')
                      {
                        echo "Pending";
                      }
                      elseif($budgetTransaction->status == '1')
                      {
                        echo "Approved";
                      } 
                      elseif($budgetTransaction->status == '2')
                      {
                        echo "Rejected";
                      } 

                     ?>" readonly>
                          <!-- . " - " . $budgetTransaction->name;?>"> -->
                    </div>
                </div>

            </div>


             <?php
            if($budgetTransaction->status == '2')
            {
             ?>

            <div class="row">

                <div class="col-sm-4" id="view_reject">
                    <div class="form-group">
                        <label>Reject Reason <span class='error-text'>*</span></label>
                        <input type="text" id="reason" name="reason" class="form-control" value="<?php echo $budgetTransaction->reason; ?>" readonly>
                    </div>
                </div>
            </div>

            <?php
            }
            ?>






       <h3>Budget Transaction Details</h3>



          <div class="custom-table">
            <table class="table">
                <thead>
                    <tr>
                    <th>Sl. No</th>
                     <th>GL CODE</th>
                     <!-- <th>Used Amount</th> -->
                     <th>Balance Amount</th>
                     <th>Allocated Amount</th>
                    </tr>
                </thead>
                <tbody>
                     <?php
                 $total = 0;
                  for($i=0;$i<count($budgetTransactionDetailsList);$i++)
                 { ?>
                    <tr>
                    <td><?php echo $i+1;?></td>
                    <td><?php echo $budgetTransactionDetailsList[$i]->fund_code . " - " . $budgetTransactionDetailsList[$i]->department_code . " - " . $budgetTransactionDetailsList[$i]->activity_code . " - " . $budgetTransactionDetailsList[$i]->account_code;?></td>
                    <!-- <td><?php echo $budgetTransactionDetailsList[$i]->used_amount;?></td> -->
                    <td><?php echo $budgetTransactionDetailsList[$i]->balance_amount;?></td>
                    <td><?php echo $budgetTransactionDetailsList[$i]->allocated_amount;?></td>

                     </tr>
                  <?php 
                  $total = $total + $budgetTransactionDetailsList[$i]->allocated_amount;
              } 
              $total = number_format($total, 2, '.', '');
              ?>
                <tr>
                    <td bgcolor="" colspan="2"></td>
                    <td bgcolor=""><b> Total : </b></td>
                    <td bgcolor=""><b><?php echo $total; ?></b></td>
                </tr>

                </tbody>
            </table>
        </div>






            <div class="button-block clearfix">
                <div class="bttn-group">
                    <!-- <button type="submit" class="btn btn-primary btn-lg">Save</button> -->
                    <a href="../list" class="btn btn-link">Back</a>
                </div>
            </div>
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
    </div>
</div>

<script>
    $(document).ready(function()
    {
        $("#form_subject").validate(
        {
            rules:
            {
                name:
                {
                    required: true
                }
            },
            messages:
            {
                name:
                {
                    required: "Subject Name Required",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>