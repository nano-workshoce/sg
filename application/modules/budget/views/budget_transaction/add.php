<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Budget Transaction</h3>
        </div>




        <form id="form_subject" action="" method="post">

        <h3>From Budget GL</h3>


        <div class="form-container">
        <h4 class="form-group-title">From Budget GL Details</h4>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Debit Budget Year <span class='error-text'>*</span></label>
                        <select name="id_dt_budget_year" id="id_dt_budget_year" class="form-control" onchange="getDebitBudget()">
                            <option value="">Select</option>
                            <?php
                            if (!empty($budgetYearList))
                            {
                                foreach ($budgetYearList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>  

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Debit Financial Year <span class='error-text'>*</span></label>
                        <select name="id_dt_financial_year" id="id_dt_financial_year" class="form-control" onchange="getDebitBudget()">
                            <option value="">Select</option>
                            <?php
                            if (!empty($financialYearList))
                            {
                                foreach ($financialYearList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>  


                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Debit Department Code<span class='error-text'>*</span></label>
                        <select name="id_dt_department" id="id_dt_department" class="form-control" onchange="getDebitBudget()">
                            <option value="">Select</option>
                            <?php
                            if (!empty($departmentCodeList))
                            {
                                foreach ($departmentCodeList as $record)
                                {?>
                             <option value="<?php echo $record->code;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>   

            </div>

            <div class="row">

                <div class="col-sm-4" id="view_debit_budget_fund_span" style="display: none">
                    <div class="form-group">
                        <label> Fund Code <span class='error-text'>*</span></label>
                        <span id="view_debit_budget_fund"></span>
                    </div>
                </div>

                <div class="col-sm-4" id="view_debit_budget_activity_span" style="display: none">
                    <div class="form-group">
                        <label> Activity Code <span class='error-text'>*</span></label>
                        <span id="view_debit_budget_activity"></span>
                    </div>
                </div>

                <div class="col-sm-4" id="view_debit_budget_account_span" style="display: none">
                    <div class="form-group">
                        <label> Account Code <span class='error-text'>*</span></label>
                        <span id="view_debit_budget_account"></span>
                    </div>
                </div>

            </div>


             <div id="view_debit_span" style="display: none">

            </div>

        </div>

        </form>

            <h3>To Budget GL</h3>

             <button type="button" class="btn btn-info btn-lg" onclick="opendialog()">Add</button>

            <br>
            <br>

            <div class="row">
                <div  id="view_credit_budget">
                </div>
            </div>



    <div id="myModal" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Budget Allocation</h4>
          </div>
          <div class="modal-body">

      <form id="form_transaction_details" action="" method="post">

                 <div class="row">
                    

                        <div class="col-sm-3">
                        <div class="form-group">
                            <label>Fund Code <span class='error-text'>*</span></label>
                            <select name="fund_code" id="fund_code" class="form-control" style="width: 196px">
                                <option value="">Select</option>
                                <?php
                                if (!empty($fundCodeList))
                                {
                                    foreach ($fundCodeList as $record)
                                    {?>
                                 <option value="<?php echo $record->code;  ?>">
                                    <?php echo $record->code . " - " . $record->name;?>
                                 </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div> 

                     <div class="col-sm-3">
                            <div class="form-group">
                                <label>Department Code <span class='error-text'>*</span></label>
                                <select name="department_code" id="department_code" class="form-control" style="width: 196px" onchange="checkDepartmentAvailable()">
                                    <!-- onchange="checkDepartmentAvailable()" -->
                                    <option value="">Select</option>
                                    <?php
                                    if (!empty($departmentCodeList))
                                    {
                                        foreach ($departmentCodeList as $record)
                                        {?>
                                     <option value="<?php echo $record->code;  ?>">
                                        <?php echo $record->code . " - " . $record->name;?>
                                     </option>
                                    <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div> 

                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>Activity Code <span class='error-text'>*</span></label>
                            <select name="activity_code" id="activity_code" class="form-control" style="width: 196px">
                                <option value="">Select</option>
                                <?php
                                if (!empty($activityCodeList))
                                {
                                    foreach ($activityCodeList as $record)
                                    {?>
                                 <option value="<?php echo $record->code;  ?>">
                                    <?php echo $record->code . " - " . $record->name;?>
                                 </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>  

                    
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>Account Code <span class='error-text'>*</span></label>
                            <select name="account_code" id="account_code" class="form-control" style="width: 196px">
                                <option value="">Select</option>
                                <?php
                                if (!empty($accountCodeList))
                                {
                                    foreach ($accountCodeList as $record)
                                    {?>
                                 <option value="<?php echo $record->code;  ?>">
                                    <?php echo $record->code . " - " . $record->name;?>
                                 </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>  

                </div>

                <div class="row">  


                     <div class="col-sm-3">
                        <div class="form-group">
                            <label>Allocated Amount <span class='error-text'>*</span></label>
                            <input type="number" class="form-control" id="amount" name="amount">
                        </div>
                    </div>


                </div>

        </form>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" onclick="saveData()">Add</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>

      </div>
    </div>


            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="button" class="btn btn-primary btn-lg" onclick="validateData()">Save</button>
                    <a href="list" class="btn btn-link">Cancel</a>
                </div>
            </div>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
    </div>
</div>



<script>

    function opendialog()
    {
        if($('#form_subject').valid())
        {
            $('#myModal').modal('show');
        }
    }



    function getDebitBudget()
    {
        var id_dt_financial_year = $("#id_dt_financial_year").val();
        var id_dt_budget_year = $("#id_dt_budget_year").val();
        var id_dt_department = $("#id_dt_department").val();

        if(id_dt_budget_year != '' && id_dt_department != '' && id_dt_financial_year != '')
        {

        var tempPR = {};
        tempPR['id_dt_financial_year'] = id_dt_financial_year;
        tempPR['id_dt_budget_year'] = id_dt_budget_year;
        tempPR['id_dt_department'] = id_dt_department;

            $.ajax(
            {
               url: '/budget/budgetTransaction/getDebitBudget',
               type: 'POST',
               data:
               {
                data: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                if(result == '')
                {
                    alert('No Debit Budget GL Found, Select Another Department Cpde')
                }
                else
                {
                    $("#view_debit_budget_fund").html(result);
                    $("#view_debit_budget_fund_span").show();

                }
                // $('#myModal').modal('hide');
                // var ta = $("#total_detail").val();
                // alert(ta);
                // $("#amount").val(ta);
               }
            });
        }
    }

    function getActivityDebitCodes()
    {
        var id_dt_financial_year = $("#id_dt_financial_year").val();
        var id_dt_budget_year = $("#id_dt_budget_year").val();
        var id_dt_department = $("#id_dt_department").val();
        var dt_fund_code = $("#dt_fund_code").val();

        if(id_dt_budget_year != '' && id_dt_department != '' && id_dt_financial_year != '' && dt_fund_code != '')
        {

        var tempPR = {};
        tempPR['id_dt_financial_year'] = id_dt_financial_year;
        tempPR['id_dt_budget_year'] = id_dt_budget_year;
        tempPR['id_dt_department'] = id_dt_department;
        tempPR['dt_fund_code'] = dt_fund_code;

            $.ajax(
            {
               url: '/budget/budgetTransaction/getActivityDebitCodes',
               type: 'POST',
               data:
               {
                data: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                if(result == '')
                {
                    alert('No Debit Budget GL Found For Entered Data')
                }
                else
                {
                    
                    $("#view_debit_budget_activity").html(result);
                    $("#view_debit_budget_activity_span").show();
                }
               }
            });
        }
    }


    function getAccountDebitCodes()
    {
        var id_dt_financial_year = $("#id_dt_financial_year").val();
        var id_dt_budget_year = $("#id_dt_budget_year").val();
        var id_dt_department = $("#id_dt_department").val();
        var dt_fund_code = $("#dt_fund_code").val();
        var dt_activity_code = $("#dt_activity_code").val();

        if(id_dt_budget_year != '' && id_dt_department != '' && id_dt_financial_year != '' && dt_fund_code != '' && dt_activity_code != '')
        {

        var tempPR = {};
        tempPR['id_dt_financial_year'] = id_dt_financial_year;
        tempPR['id_dt_budget_year'] = id_dt_budget_year;
        tempPR['id_dt_department'] = id_dt_department;
        tempPR['dt_fund_code'] = dt_fund_code;
        tempPR['dt_activity_code'] = dt_activity_code;

            $.ajax(
            {
               url: '/budget/budgetTransaction/getAccountDebitCodes',
               type: 'POST',
               data:
               {
                data: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                if(result == '')
                {
                    alert('No Debit Budget GL Found For Entered Data')
                }
                else
                {
                    $("#view_debit_budget_account").html(result);
                    $("#view_debit_budget_account_span").show();
                }
               }
            });
        }
    }

    function getBudgetAllocationByCodes()
    {
        var id_dt_financial_year = $("#id_dt_financial_year").val();
        var id_dt_budget_year = $("#id_dt_budget_year").val();
        var id_dt_department = $("#id_dt_department").val();
        var dt_fund_code = $("#dt_fund_code").val();
        var dt_activity_code = $("#dt_activity_code").val();
        var dt_account_code = $("#dt_account_code").val();

        if(id_dt_budget_year != '' && id_dt_department != '' && id_dt_financial_year != '' && dt_fund_code != '' && dt_activity_code != '' && dt_account_code != '')
        {

        var tempPR = {};
        tempPR['id_dt_financial_year'] = id_dt_financial_year;
        tempPR['id_dt_budget_year'] = id_dt_budget_year;
        tempPR['id_dt_department'] = id_dt_department;
        tempPR['dt_fund_code'] = dt_fund_code;
        tempPR['dt_activity_code'] = dt_activity_code;
        tempPR['dt_account_code'] = dt_account_code;

            $.ajax(
            {
               url: '/budget/budgetTransaction/getBudgetAllocationByCodes',
               type: 'POST',
               data:
               {
                data: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                if(result == '')
                {
                    alert('No Debit Budget Allocation Found For Entered Data, Select Another Department For Transaction')
                }
                else
                {
                    $("#view_debit_span").html(result);
                    $("#view_debit_span").show();
                }
               }
            });
        }
    }

    function checkDepartmentAvailable()
    {

        var id_dt_financial_year = $("#id_dt_financial_year").val();
        var id_dt_budget_year = $("#id_dt_budget_year").val();
        var department_code = $("#department_code").val();
        var id_dt_department = $("#id_dt_department").val();


        // alert(id_dt_department+ ' - '+department_code);
        // if($department_code != id_dt_department)
        // if(department_code.equals(id_dt_department) == true)
        // {
        //     alert("Debit and Credit Department Shouldn't be Same");
        //     $("#department_code").val('');
        // }
        // else
        // {
             if(id_dt_budget_year != '' && department_code != '' && id_dt_financial_year != '')
            {
             var tempPR = {};
            tempPR['id_dt_financial_year'] = id_dt_financial_year;
            tempPR['id_dt_budget_year'] = id_dt_budget_year;
            tempPR['department_code'] = department_code;

                 $.ajax(
                {
                   url: '/budget/budgetTransaction/checkDepartmentAvailable',
                   type: 'POST',
                   data:
                   {
                    data: tempPR
                   },
                   error: function()
                   {
                    alert('Something is wrong');
                   },
                   success: function(result)
                   {
                    if(result == '')
                    {
                        alert('Selected Department Not Found in Budget Allocation For Selected Budget & Financial Year, Select Another Department For Transaction')
                    }
                    else
                    {

                    }
                   }
                });
            }
        // }

    }


    function saveData()
    {
        
            if($('#form_transaction_details').valid())
            {
                $('#myModal').modal('hide');
                var tempPR = {};
                tempPR['fund_code'] = $("#fund_code").val();
                tempPR['activity_code'] = $("#activity_code").val();
                tempPR['account_code'] = $("#account_code").val();
                tempPR['department_code'] = $("#department_code").val();
                tempPR['allocated_amount'] = $("#amount").val();
            
                $.ajax(
                {
                   url: '/budget/budgetTransaction/tempadd',
                   type: 'POST',
                   data:
                   {
                    tempData: tempPR
                   },
                   error: function()
                   {
                    alert('Something is wrong');
                   },
                   success: function(result)
                   {
                    $("#view_credit_budget").html(result);
                   }
                });
            }
    }


    function deleteTemp(id)
    {
        // alert(id);

         $.ajax(
            {
               url: '/budget/budgetTransaction/tempDelete/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    $("#view_credit_budget").html(result);
               }
            });
    }


    function getCreditBudget()
    {
        var id_cr_financial_year = $("#id_cr_financial_year").val();
        var id_cr_budget_year = $("#id_cr_budget_year").val();
        var id_cr_department = $("#id_cr_department").val();

        if(id_cr_financial_year != '' && id_cr_budget_year != '' && id_cr_department != '')
        {

        var tempPR = {};
        tempPR['id_cr_financial_year'] = id_cr_financial_year;
        tempPR['id_cr_budget_year'] = id_cr_budget_year;
        tempPR['id_cr_department'] = id_cr_department;
            $.ajax(
            {
               url: '/budget/budgetTransaction/getCreditBudget',
               type: 'POST',
               data:
               {
                data: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                 if(result == '')
                {
                    alert('No Credit Budget GL Found For Entered Data')
                }
                else
                {
                    $("#view_credit_budget").html(result);
                    $("#view_credit_budget").show();
                }
                // $('#myModal').modal('hide');
                // var ta = $("#total_detail").val();
                // alert(ta);
                // $("#amount").val(ta);
               }
            });
        }
    }

    function validateData()
    {
        if($('#form_subject').valid())
        {
            console.log($("#view_credit_budget").html());
            var addedProgam = $("#view_credit_budget").html();


            if(addedProgam == undefined)
            {

                alert("Add Transaction Details");
            }
            else
            {
                var entered_amount = $("#entered_amount").val()+'.00';
                var total_detail = $("#total_detail").val();
                // $total_detail = number_format($total_detail, 2, '.', '');
                // var e = parseFloat(entered_amount);
                // alert(entered_amount);

                // alert(total_detail+' - '+e);


                if(entered_amount != total_detail)
                {
                    if(total_detail == undefined)
                    {
                        alert('Enter Transaction Details');
                    }
                    else
                    {
                        alert('Sum Of All Budget Transaction Amount '+total_detail+', Should Be Equal To Entered Transaction Amount '+entered_amount);
                    }
                }
                else
                {
                    $('#form_subject').submit();
                }
            }
        }    
    }



    function validateDetailsData()
    {
        if($('#form_subject').valid())
        {
            console.log($("#view").html());
            var addedProgam = $("#view").html();
            if(addedProgam=='')
            {
                alert("Add Budget Allocation");
            }
            else
            {
                var total_detail = $("#total_detail").val();
                var budget_amount = $("#budget_amount").val();

                // alert(budget_amount);

                    // $('#form_subject').submit();

                if(total_detail != budget_amount)
                {
                    alert('Sum Of All Budget Allocated Amount '+total_detail+' Should Be Equal To Budget Amount '+budget_amount)
                }
                else
                {
                    $('#form_subject').submit();
                }

            }
        }    
    }







    $('select').select2();


    


    $(document).ready(function()
    {
        $("#form_transaction_details").validate(
        {
            rules:
            {
                fund_code:
                {
                    required: true
                },
                department_code:
                {
                    required: true
                },activity_code:
                {
                    required: true
                },
                account_code:
                {
                    required: true
                },
                amount:
                {
                    required: true
                }
            },
            messages:
            {
                fund_code:
                {
                    required: "<p class='error-text'>Select Fund Code</p>",
                },
                department_code:
                {
                    required: "<p class='error-text'>Select Department Code</p>",
                },
                activity_code:
                {
                    required: "<p class='error-text'>Select Activity Code</p>",
                },
                account_code:
                {
                    required: "<p class='error-text'>Select Account COde</p>",
                },
                amount:
                {
                    required: "<p class='error-text'>Enter Amount</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });





    $(document).ready(function()
    {
        $("#form_subject").validate(
        {
            rules:
            {
                id_dt_financial_year:
                {
                    required: true
                },
                id_dt_budget_year:
                {
                    required: true
                },
                id_dt_department:
                {
                    required: true
                },
                dt_fund_code:
                {
                    required: true
                },
                dt_activity_code:
                {
                    required: true
                },
                dt_account_code:
                {
                    required: true
                },
                entered_amount:
                {
                    required: true
                }
            },
            messages:
            {
                id_dt_financial_year:
                {
                    required: "<p class='error-text'>Select Debit Financial Year</p>",
                },
                id_dt_budget_year:
                {
                    required: "<p class='error-text'>Select Debit Budget Year</p>",
                },
                id_dt_department:
                {
                    required: "<p class='error-text'>Select Debit Department</p>",
                },
                dt_fund_code:
                {
                    required: "<p class='error-text'>Select Debit Fund Code</p>",
                },
                dt_activity_code:
                {
                    required: "<p class='error-text'>Select Debit Activity Code</p>",
                },
                dt_account_code:
                {
                    required: "<p class='error-text'>Select Debit Account Code</p>",
                },
                entered_amount:
                {
                    required: "<p class='error-text'>Enter Transaction Amoont</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>