            <div class="sidebar">
                <div class="user-profile clearfix">
                    <a href="#" class="user-profile-link">
                        <span><img src="<?php echo BASE_PATH; ?>assets/img/user_profile.jpg"></span> <?php echo $name; ?>
                    </a>
                    <div class="dropdown">
                        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
                            <li><a href="/setup/user/profile">Edit Profile</a></li>
                            <li><a href="/setup/user/logout">Logout</a></li>
                        </ul>
                    </div>
                </div>
                <div class="sidebar-nav">
                    <h4>Setup</h4>
                    <ul>
                        <li><a href="/budget/budgetYear/list">Budget Year</a></li>
                       </ul>
                    <h4>Budget</h4>
                    <ul>
                        <li><a href="/budget/budgetAmount/list">Budget Amount</a></li>
                        <li><a href="/budget/budgetAmount/approvalList">Budget Amount Approval</a></li>
                        <li><a href="/budget/budgetAllocation/list">Budget Allocation</a></li>
                        <li><a href="/budget/budgetAllocation/approvalList">Budget Allocation Approval</a></li>
                        <li><a href="/budget/budgetTransaction/list">Budget Transaction</a></li>
                        <li><a href="/budget/budgetTransaction/approvalList">Budget Transaction Approval</a></li>
                    </ul>

                     <h4>Reports</h4>
                    <ul>
                        <li><a href="/budget/budgetTransaction/budgetTransactionReportByDepartment">Budget Transaction By Department Report</a></li>
                    </ul>

                </div>
            </div>