<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class BudgetYear extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('budget_year_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('budget_year.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchParam'] = $name;
            $data['budgetYearList'] = $this->budget_year_model->budgetYearList($name);
            $this->global['pageTitle'] = 'Campus Management System : Budget Year';
            //print_r($subjectDetails);exit;
            $this->loadViews("budget_year/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('budget_year.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;

            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'status' => $status,
                    'created_by' => $user_id
                );

                $duplicate_row = $this->budget_year_model->checkBudgetYearDuplication($data);

                if($duplicate_row)
                {
                    echo "Duplicate Budget Year Not Allowed";exit();
                }
            
                $result = $this->budget_year_model->addNewBudgetYear($data);
                redirect('/budget/budgetYear/list');
            }
            $this->load->model('budget_year_model');
            $this->global['pageTitle'] = 'Campus Management System : Add Budget Year';
            $this->loadViews("budget_year/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('budget_year.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;

            if ($id == null)
            {
                redirect('/budget/budgetYear/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'status' => $status,
                    'created_by' => $user_id
                );

                 $duplicate_row = $this->budget_year_model->checkBudgetYearDuplicationEdit($data,$id);

                if($duplicate_row)
                {
                    echo "Duplicate Budget Year Not Allowed";exit();
                }

                $result = $this->budget_year_model->editBudgetYear($data,$id);
                redirect('/budget/budgetYear/list');
            }
            $data['budgetYearDetails'] = $this->budget_year_model->getBudgetYear($id);
            $this->global['pageTitle'] = 'Campus Management System : Edit Budget Year';
            $this->loadViews("budget_year/edit", $this->global, $data, NULL);
        }
    }
}
