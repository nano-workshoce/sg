<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';


class TestBooking extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('test_booking_model');
    }

    public function index()
    {   
        $uniqueId = rand(0000000000,9999999999);
        $this->session->set_userdata("id_session_patient", md5($uniqueId));
        $this->booking();
    }

    public function booking()
    {
        $formData = $this->input->post();
        $id_session_patient = $this->session->id_session_patient;
        // echo "<Pre>"; print_r($id_session_patient);exit;
        $domain = $this->getDomainName();

        if($formData)
        {

            echo "<Pre>"; print_r($_POST);exit;

            $this->load->library('form_validation');
            
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email|max_length[128]|trim');
            $this->form_validation->set_rules('password', 'Password', 'required|max_length[32]');
            
            if($this->form_validation->run() == FALSE)
            {
                $this->index();
            }
            else
            {
                $email = strtolower($this->security->xss_clean($this->input->post('email')));
                $password = $this->input->post('password');
                
                $result = $this->alumni_login_model->loginAlumni($email, $password);
                
                
                if(!empty($result))
                {
                    if($result->applicant_status == 'Graduated')
                    {
                        
                    $lastLogin = $this->alumni_login_model->alumniLastLoginInfo($result->id_student);

                    if($lastLogin == '')
                    {
                        $alumni_login = date('Y-m-d h:i:s');
                    }
                    else
                    {
                        $alumni_login = $lastLogin->created_dt_tm;
                    }

                    $sessionArray = array('id_alumni'=>$result->id_student,
                                            'alumni_name'=>$result->student_name,
                                            'alumni_email_id'=>$result->email_id,
                                            'alumni_nric'=>$result->nric,
                                            'alumni_id_intake'=>$result->id_intake,
                                            'alumni_id_program'=>$result->id_program,
                                            'alumni_id_qualification'=>$result->id_degree_type,
                                            'alumni_last_login'=> $alumni_login,
                                            'isAlumniLoggedIn' => TRUE
                                    );

                    $this->session->set_userdata($sessionArray);

                    unset($sessionArray['id_alumni'], $sessionArray['isAlumniLoggedIn'], $sessionArray['alumni_last_login']);

                    $loginInfo = array("id_student"=>$result->id_student, "session_data" => json_encode($sessionArray), "machine_ip"=>$_SERVER['REMOTE_ADDR'], "user_agent"=>getBrowserAgent(), "agent_string"=>$this->agent->agent_string(), "platform"=>$this->agent->platform());

                    $uniqueId = rand(0000000000,9999999999);
                    $this->session->set_userdata("my_alumni_session_id", md5($uniqueId));


                    $this->alumni_login_model->addAlumniLastLogin($loginInfo);

                    // echo "<Pre>"; print_r($this->session->userdata());exit();
                    // echo md5($uniqueId);exit();
                    redirect('/alumni/profile');
                    }
                }
                else
                {
                    $this->session->set_flashdata('error', 'Email or password mismatch');
                    
                    $this->index();
                }
            }
        }

        $data['id_session_patient'] = $id_session_patient;
        $data['testList'] = $this->test_booking_model->getTestListByStatus('1');
        $data['salutationList'] = $this->test_booking_model->salutationListByStatus('1');
        // echo "<Pre>"; print_r($data['testList']);exit;

        $this->global['pageTitle'] = 'Cure Test : Book a lab Test Online';
        $this->loadOnlyView("events/tb",$data);
    }

    function getAgeCalculation($dob)
    {
        $date1 = date('Y-m-d', strtotime($dob));
        $date2 = date("Y-m-d");

        $diff = $this->getDatesDifference($date1,$date2);
        if($diff)
        {
            echo $diff;exit;
        }
        else
        {
            echo $date1;
        }

        // $diff=date_diff($dob,$date);

        // $date1=date_create(date('Y-m-d', strtotime($dob)));
        // $date2=date_create($date = date("Y-m-d"));
        // $diff=date_diff($date1,$date2);
        // $diff_days = $diff->format("%R%a days");
        // echo $diff_days;exit;
    }


    function addTempBookingPatientDetails()
    {        
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        echo "<Pre>"; print_r($tempData);exit();
        $inserted_id = $this->mark_distribution_model->saveTempDetailData($tempData);

        $data = $this->displayTempData();

        print_r($data);exit();
    }

    function displayTempData()
    {
        $id_session = $this->session->my_session_id;

        $temp_details = $this->mark_distribution_model->getTempMarkDistributionDetailsBySessionId($id_session);

        // echo "<Pre>";print_r($temp_details);exit;



        if(!empty($temp_details))
        {
            
        $table ="
        <div class='custom-table'>
        <table  class='table' id='list-table'>
                <thead>
                  <tr>
                    <th>Sl. No</th>
                    <th>Exam Components</th>
                    <th>Pass Compulsary</th>
                    <th>Pass Marks</th>
                    <th>Max. Marks</th>
                    <th>Attendance Status</th>
                    <th class='text-center'>Action</th>
                    </tr>
                </thead>";
                    $total_detail = 0;
                    for($i=0;$i<count($temp_details);$i++)
                    {
                        $id = $temp_details[$i]->id;
                        $is_pass_compulsary = $temp_details[$i]->is_pass_compulsary;
                        $pass_marks = $temp_details[$i]->pass_marks;
                        $max_marks = $temp_details[$i]->max_marks;
                        $attendance_status = $temp_details[$i]->attendance_status;
                        $component_code = $temp_details[$i]->component_code;
                        $component_name = $temp_details[$i]->component_name;

                        if($is_pass_compulsary == 1)
                        {
                            $is_pass_compulsary = 'Yes';
                        }else
                        {
                            $is_pass_compulsary = 'No';
                        }

                        if($attendance_status == 1)
                        {
                            $attendance_status = 'Yes';
                        }else
                        {
                            $attendance_status = 'No';
                        }


                        $j=$i+1;

                        $table .= "
                        <tbody>
                        <tr>
                            <td>$j</td>
                            <td>$component_code - $component_name</td>
                            <td>$is_pass_compulsary</td>
                            <td>$pass_marks</td>
                            <td>$max_marks</td>
                            <td>$attendance_status</td>
                            <td class='text-center'>
                                <a class='btn btn-sm btn-edit' onclick='deleteTempData($id)'>Delete</a>
                            <td>
                        </tr>";
                                // <span onclick='deleteTempData($id)'>Delete</a>
                    }

                     $table .= "
                    </tbody>
                    </table>
        </div>
        <br>";
        }
        else
        {
            $table = "";

        }


        return $table;
    }

    function deleteTempData($id)
    {
        $id_session = $this->session->my_session_id;

        $deleted = $this->mark_distribution_model->deleteTempMarkDistribution($id);

        $data = $this->displayTempData();

        print_r($data);exit();
    }
}

?>