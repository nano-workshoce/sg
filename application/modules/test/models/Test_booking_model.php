<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Test_booking_model extends CI_Model
{
    function getTestListByStatus($status)
    {
        $this->db->select('t.*');
        $this->db->from('test as t');
        $this->db->where('t.status', $status);
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function salutationListByStatus($status)
    {
    	$this->db->select('s.*');
        $this->db->from('salutation_setup as s');
        $this->db->where('s.status', $status);
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }
}

?>