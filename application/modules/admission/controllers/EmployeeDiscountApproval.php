<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class EmployeeDiscountApproval extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('employee_discount_approval_model');
        $this->load->model('setup/country_model');
        $this->load->model('setup/state_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('employee_discount_approval.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $data['intakeList'] = $this->employee_discount_approval_model->intakeList();
            $data['programList'] = $this->employee_discount_approval_model->programList();


            $formData['first_name'] = $this->security->xss_clean($this->input->post('first_name'));
            $formData['email_id'] = $this->security->xss_clean($this->input->post('email_id'));
            $formData['nric'] = $this->security->xss_clean($this->input->post('nric'));
            $formData['id_program'] = $this->security->xss_clean($this->input->post('id_program'));
            $formData['id_intake'] = $this->security->xss_clean($this->input->post('id_intake'));
            $formData['applicant_status'] = $this->security->xss_clean($this->input->post('applicant_status'));


            $data['searchParam'] = $formData;

            $data['employeeList'] = $this->employee_discount_approval_model->employeeList($formData);

            $this->global['pageTitle'] = 'Campus Management System : Employee Discount Approval';
            // echo "<Pre>";print_r($data['employeeList']);exit;
            $this->loadViews("employee_discount_approval/list", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('employee_discount_approval.approve') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/admission/employeeDiscountApproval/list');
            }

            $data['employeeDiscountDetails'] = $this->employee_discount_approval_model->getEmployeeDiscountDetails($id);

            $id_applicant = $data['employeeDiscountDetails']->id_applicant;
            // echo "<Pre>";print_r($id_applicant);exit();


            if($this->input->post())
            {
            $id_user = $this->session->userId;
            $id_session = $this->session->session_id;


            
            $employee_status = $this->security->xss_clean($this->input->post('employee_status'));
            $reason = $this->security->xss_clean($this->input->post('reason'));
            $data = array(
                    'employee_status' => $employee_status,
                    'rejected_by' => $id_user,
                    'rejected_on' => date('Y-m-d H:i:s'),
                    'reason' => $reason
                );
                $result = $this->employee_discount_approval_model->editEmployeeDiscountDetails($data,$id);
                if($result)
                {
                    if($employee_status == 'Approved')
                    {
                        $master_data = array('is_employee_discount' => 1);
                    }
                    elseif($employee_status == 'Reject')
                    {
                        $master_data = array('is_employee_discount' => 2);
                    }
                    $updated_applicant = $this->employee_discount_approval_model->editApplicantDetails($master_data,$id_applicant);
                }
                redirect('/admission/employeeDiscountApproval/list');
            }
            
            $data['raceList'] = $this->employee_discount_approval_model->raceList();
            $data['religionList'] = $this->employee_discount_approval_model->religionList();
            $data['salutationList'] = $this->employee_discount_approval_model->salutationListByStatus('1');
            $data['degreeTypeList'] = $this->employee_discount_approval_model->qualificationList();
            $data['countryList'] = $this->country_model->countryList();
            $data['stateList'] = $this->state_model->stateList();
            $data['programList'] = $this->employee_discount_approval_model->programList();
            $data['intakeList'] = $this->employee_discount_approval_model->intakeList();
            $data['branchList'] = $this->employee_discount_approval_model->branchListByStatus();
            $data['requiremntListList'] = $this->employee_discount_approval_model->programRequiremntListList();
            $data['partnerUniversityList'] = $this->employee_discount_approval_model->getUniversityListByStatus('1');
            $data['schemeList'] = $this->employee_discount_approval_model->schemeListByStatus('1');
            $data['programStructureTypeList'] = $this->employee_discount_approval_model->programStructureTypeListByStatus('1');
            
            $data['nationalityList'] = $this->employee_discount_approval_model->nationalityListByStatus('1');

            $data['programEntryRequirementList'] = $this->employee_discount_approval_model->programEntryRequirementList($data['employeeDiscountDetails']->id_program);
            $data['programDetails'] = $this->employee_discount_approval_model->getProgramDetails($data['employeeDiscountDetails']->id_program);
            $data['applicantUploadedFiles'] = $this->employee_discount_approval_model->getApplicantUploadedFiles($id_applicant);
            

            // echo "<Pre>";print_r($data['employeeDiscountDetails']);exit();
            $this->global['pageTitle'] = 'Campus Management System : Edit Employee Discount Approval';
            $this->loadViews("employee_discount_approval/edit", $this->global, $data, NULL);
        }
    }
}

