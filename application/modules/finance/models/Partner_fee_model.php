<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Partner_fee_model extends CI_Model
{

	function intakeListByStatus($status)
	{
		$this->db->select('fc.*');
        $this->db->from('intake as fc');
        $this->db->order_by("fc.status", $status);
        $this->db->order_by("fc.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
	}

	function programListByStatus($status)
	{
		$this->db->select('fc.*');
        $this->db->from('programme as fc');
        $this->db->order_by("fc.status", $status);
        $this->db->order_by("fc.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
	}

	function partnerUniversityListByStatus($status)
	{
		$this->db->select('fc.*');
        $this->db->from('partner_university as fc');
        $this->db->order_by("fc.status", $status);
        $this->db->order_by("fc.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
	}

	function partnerFeeList()
    {
        $this->db->select('fc.*, pu.code as university_code, pu.name as university_name, p.code as program_code, p.name as program_code, i.name as intake');
        $this->db->from('partner_university_fee as fc');
        $this->db->join('partner_university as pu', 'fc.id_partner_university = pu.id');
        $this->db->join('programme as p', 'fc.id_program = p.id');
        $this->db->join('intake as i', 'fc.id_intake = i.id');
        // $this->db->order_by("fc.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }


    function partnerFeeListSearch($data)
    {
        $this->db->select('fc.*, pu.code as university_code, pu.name as university_name, p.code as program_code, p.name as program_name, i.name as intake');
        $this->db->from('partner_university_fee as fc');
        $this->db->join('partner_university as pu', 'fc.id_partner_university = pu.id');
        $this->db->join('programme as p', 'fc.id_program = p.id');
        $this->db->join('intake as i', 'fc.id_intake = i.id');
        if($data['id_partner_university'] != '')
        {
        	$this->db->where("fc.id_partner_university", $data['id_partner_university']);
        }
         if($data['id_program'] != '')
        {
        	$this->db->where("fc.id_program", $data['id_program']);
        }
         if($data['id_intake'] != '')
        {
        	$this->db->where("fc.id_intake", $data['id_intake']);
        }
         if($data['fee_type'] != '')
        {
        	$this->db->where("fc.fee_type", $data['fee_type']);
        }
        
        // $this->db->order_by("fc.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function partnerFeeListByStatus($status)
    {
        $this->db->select('fc.*');
        $this->db->from('partner_university_fee as fc');
        // $this->db->join('country as c', 'sp.id_country = c.id');
        $this->db->where('fc.status', $status);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getPartnerUniversityFee($id)
    {
        $this->db->select('fc.*');
        $this->db->from('partner_university_fee as fc');
        $this->db->where('fc.id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewPartnerFee($data)
    {
        $this->db->trans_start();
        $this->db->insert('partner_university_fee', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function addNewPartnerUniversityFee($data)
    {
        $this->db->trans_start();
        $this->db->insert('partner_university_fee', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function editPartnerUniversityFee($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('partner_university_fee', $data);
        return TRUE;
    }

    function getIntakeListByProgramme($id_programme)
    {
        $this->db->select('DISTINCT(ihp.id_intake) as id_intake, ihp.*, in.name as intake_name, in.year as intake_year');
        $this->db->from('intake_has_programme as ihp');
        $this->db->join('intake as in', 'ihp.id_intake = in.id');
        $this->db->where('ihp.id_programme', $id_programme);
        $query = $this->db->get();
         return $query->result();
    }


    function getAgreement() {

         $this->db->select('fc.*');
        $this->db->from('scholarship_partner_university_has_aggrement as fc');
        // $this->db->join('country as c', 'sp.id_country = c.id');
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getPartnerUniversityAggrementList($id_partner_university)
    {
        $this->db->select('aggr.*, cs.code as currency_code, cs.name as currency_name');
        $this->db->from('scholarship_partner_university_has_aggrement as aggr');
        $this->db->join('currency_setup as cs','aggr.id_currency = cs.id');
        $this->db->where('aggr.id_partner_university', $id_partner_university);
        $query = $this->db->get();
        return $query->result();
    }
}

