<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Department_model extends CI_Model
{
    function departmentList()
    {
        $this->db->select('d.*');
        $this->db->from('scholarship_department as d');
        $this->db->order_by("d.name", "DESC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function departmentListSearch($search)
    {
        $this->db->select('d.*');
        $this->db->from('scholarship_department as d');
        if (!empty($search))
        {
            $likeCriteria = "(d.name  LIKE '%" . $search . "%' or d.description  LIKE '%" . $search . "%' or d.code  LIKE '%" . $search . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("d.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function getDepartment($id)
    {
        $this->db->select('d.*');
        $this->db->from('scholarship_department as d');
        $this->db->where('d.id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewDepartment($data)
    {
        $this->db->trans_start();
        $this->db->insert('scholarship_department', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editDepartment($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('scholarship_department', $data);
        return TRUE;
    }
    
    function deleteActivityDetails($countryId, $countryInfo)
    {
        $this->db->where('id', $countryId);
        $this->db->update('scholarship_department', $countryInfo);
        return $this->db->affected_rows();
    }

    function getDepartmentHasStaffList($id_department)
    {
        $this->db->select('a.*, s.ic_no, s.name as staff_name, s.email');
        $this->db->from('scholarship_department_has_staff as a');
        $this->db->join('staff as s', 'a.id_staff = s.id');
        $this->db->where('a.id_department', $id_department);
        // $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function addNewStaffToDepartment($data)
    {
        $this->db->trans_start();
        $this->db->insert('scholarship_department_has_staff', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function deleteDepartmentHasStaff($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('scholarship_department_has_staff');
        return TRUE;
    }

    function staffListByStatus($status)
    {
        $this->db->select('a.*');
        $this->db->from('staff as a');
        $this->db->where('a.status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }
}