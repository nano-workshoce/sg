<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Documents_model extends CI_Model
{
    function documentsList()
    {
        $this->db->select('a.*');
        $this->db->from('scholarship_documents as a');
        $this->db->order_by("id", "DESC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function documentsListSearch($formData)
    {
        $this->db->select('a.*');
        $this->db->from('scholarship_documents as a');
        if (!empty($formData['name']))
        {
            $likeCriteria = "(a.name  LIKE '%" . $formData['name'] . "%' or a.code  LIKE '%" . $formData['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("a.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getDocuments($id)
    {
        $this->db->select('*');
        $this->db->from('scholarship_documents');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewDocuments($data)
    {
        $this->db->trans_start();
        $this->db->insert('scholarship_documents', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editDocuments($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('scholarship_documents', $data);
        return TRUE;
    }
}

