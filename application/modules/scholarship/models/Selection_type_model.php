<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Selection_type_model extends CI_Model
{
    function selectionTypeList()
    {
        $this->db->select('*');
        $this->db->from('scholarship_selection_type');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function selectionTypeListSearch($search)
    {
        $this->db->select('*');
        $this->db->from('scholarship_selection_type');
        if (!empty($search))
        {
            $likeCriteria = "(name  LIKE '%" . $search . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getSelectionType($id)
    {
        $this->db->select('*');
        $this->db->from('scholarship_selection_type');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewSelectionType($data)
    {
        $this->db->trans_start();
        $this->db->insert('scholarship_selection_type', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editSelectionType($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('scholarship_selection_type', $data);
        return TRUE;
    }
}