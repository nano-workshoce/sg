<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Role_model extends CI_Model
{
    function roleListingCount()
    {
        $this->db->select('BaseTbl.*');
        $this->db->from('scholar_roles as BaseTbl');
         $query = $this->db->get();
        
        return $query->num_rows();
    }
    
    function latestRoleListing()
    {
        $this->db->select('BaseTbl.*');
        $this->db->from('scholar_roles as BaseTbl');
        $this->db->limit(10, 0);
        $this->db->order_by("id", "desc");

         $query = $this->db->get();
         $result = $query->result();        
         return $result;
    }
    
    function roleListSearch($data)
    {
        $this->db->select('BaseTbl.*');
        $this->db->from('scholar_roles as BaseTbl');
        if($data['name'] != '')
        {
            $likeCriteria = "(BaseTbl.role  LIKE '%".$data['name']."%')";
            $this->db->where($likeCriteria);
        }
        if($data['status'] != '')
        {
            $this->db->where('BaseTbl.status', $data['status']);
        }
         $query = $this->db->get();
         $result = $query->result();        
         return $result;
    }

    function rolePermissions($id)
    {
        $this->db->select('BaseTbl.id');
        $this->db->from('scholar_role_permissions as BaseTbl');
        $this->db->where('id', $id);
        $query = $this->db->get();
         $result = $query->result();        
         return $result;
    }

    function addNewRole($roleInfo)
    {
        $this->db->trans_start();
        $this->db->insert('scholar_roles', $roleInfo);
        
        $insert_id = $this->db->insert_id();
        
        $this->db->trans_complete();
        
        return $insert_id;
    }

    function updateRolePermissions($permissions,$id)
    {
        $this->db->trans_start();
        $SQL = "delete from scholar_role_permissions where id = ".$id;
        $query = $this->db->query($SQL);
        foreach($permissions as $permission){
            $info = array('id'=>$id,'id'=>$permission);
            $this->db->insert('scholar_role_permissions', $info);
        }
        
        $this->db->trans_complete();
        
        return ;
    }
    
    function getRole($id)
    {
        $this->db->select('*');
        $this->db->from('scholar_roles');
        $this->db->where('id', $id);
        $query = $this->db->get();
            // echo "<pre>";print_r($query);die;
        
        return $query->row();
    }

    function checkScholarAccess($id,$code)
    {
        $this->db->select('srp.id');
        $this->db->from('scholar_role_permissions as srp');
        $this->db->join('scholar_permissions as sp', 'srp.id_permission = sp.id');
        $this->db->where('sp.id', $id);
        $this->db->where('sp.code', $code);
        $query = $this->db->get();
        if(empty($query->row())){
            return 1;
        }
        else{
            return 0;
        }
    }
    
    function editRole($roleInfo, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('scholar_roles', $roleInfo);
        
        return TRUE;
    }
    
    function deleteRole($id, $roleInfo)
    {
        $this->db->where('id', $id);
        $this->db->update('scholar_roles', $roleInfo);
        
        return $this->db->affected_rows();
    }

}

  