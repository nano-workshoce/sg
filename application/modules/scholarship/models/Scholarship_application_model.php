<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Scholarship_application_model extends CI_Model
{
    function getStudentByStudentId($id_student)
    {
        $this->db->select('s.*, p.name as programme_name, i.name as intake_name, qs.name as qualification_name, qs.code as qualification_code');
        $this->db->from('student as s');
        $this->db->join('programme as p', 's.id_program = p.id'); 
        $this->db->join('intake as i', 's.id_intake = i.id');
        $this->db->join('qualification_setup as qs', 's.id_degree_type = qs.id');
        $this->db->where('s.id', $id_student);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

	function getScholarshipApplicationListSearch($data)
	{
        // echo "<Pre>";print_r($data);exit();
        $this->db->select('ia.*, ict.name as scholarship_name, ict.code as scholarship_code, p.code as program_code, p.name as program_name, s.nric, s.full_name as student_name');
        $this->db->from('scholarship_application as ia');
        $this->db->join('scholarship_scheme as ict', 'ia.id_scholarship_scheme = ict.id');
        $this->db->join('scholarship_programme as p', 'ia.id_program = p.id');
        $this->db->join('scholar_applicant as s', 'ia.id_student = s.id');
        if ($data['application_number'] != '')
        {
            $likeCriteria = "(ia.application_number LIKE '%" . $data['application_number'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['name'] != '')
        {
            $likeCriteria = "(s.full_name LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['nric'] != '')
        {
            $likeCriteria = "(s.nric LIKE '%" . $data['nric'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['id_scholarship_scheme'] != '')
        {
            $this->db->where('ia.id_scholarship_scheme', $data['id_scholarship_scheme']);
        }
        if ($data['id_program'] != '')
        {
            $this->db->where('ia.id_program', $data['id_program']);
        }
        if ($data['status'] != '')
        {
            $this->db->where('ia.status', $data['status']);
        }
        $this->db->order_by('ia.id', "ASC");
        $query = $this->db->get();
         return $query->result();
    }

    function schemeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('scholarship_scheme');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result();      
        return $result;
    }

    function schemeList()
    {
        $this->db->select('*');
        $this->db->from('scholarship_scheme');
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result();      
        return $result;
    }

    function getScholarshipApplication($id)
    {
        $this->db->select('ia.*, ss.name as scholarship_name, ss.code as scholarship_code');
        $this->db->from('scholarship_application as ia');
        $this->db->join('scholarship_scheme as ss', 'ia.id_scholarship_scheme = ss.id');
         $this->db->where('ia.id', $id);
        $query = $this->db->get();
          $result = $query->row();
         return $result; 
    }

    function getSchemeById($id)
    {
        $this->db->select('ia.*');
        $this->db->from('scholarship_scheme as ia');
         $this->db->where('ia.id', $id);
        $query = $this->db->get();
          $result = $query->row();
         return $result; 
    }


    // function getScholarshipApplication($id)
    // {
    //     $this->db->select('ia.*, ict.name as company_type_name, ict.code as company_type_code, icr.name as company_name, icr.registration_no, p.code as program_code, p.name as program_name, i.name as intake, s.nric, s.full_name as student_name');
    //     $this->db->from('scholarship_application as ia');
    //     $this->db->join('scholarship_company_type as ict', 'ia.id_company_type = ict.id');
    //     $this->db->join('scholarship_company_registration as icr', 'ia.id_company = icr.id');
    //     $this->db->join('programme as p', 'ia.id_program = p.id');
    //     $this->db->join('intake as i', 'ia.id_intake = i.id');
    //     $this->db->join('student as s', 'ia.id_student = s.id');
    //      $this->db->where('ia.id', $id);
    //     $query = $this->db->get();
    //       $result = $query->row();
    //      return $result; 
    // }



    function getApplicationPersonalDetailsByApplicationId($id_scholar_applicant,$id_scheme,$id_application)
    {
        $this->db->select('ia.*');
        $this->db->from('scholarship_applicant as ia');
        $this->db->where('ia.id_scholar_applicant', $id_scholar_applicant);
        $this->db->where('id_application', $id_application);
        $this->db->where('ia.id_scholarship_scheme', $id_scheme);
        $this->db->order_by("ia.id", "desc");
        $query = $this->db->get();
        $result = $query->row();

        return $result;
    }



    function getExamDetailsByApplicationId($id_scholar_applicant,$id_scholarship_scheme,$id_application)
    {
        $this->db->select('*');
        $this->db->from('scholarship_examination_details');
        $this->db->where('id_scholar_applicant', $id_scholar_applicant);
        $this->db->where('id_scholarship_scheme', $id_scholarship_scheme);
        $this->db->where('id_application', $id_application);
        $query = $this->db->get();
         $result = $query->result();
        return $result;
    }
    
    function getFamilyDetailsByApplicationId($id_scholar_applicant,$id_scholarship_scheme,$id_application)
    {
        $this->db->select('*');
        $this->db->from('scholar_applicant_family_details');
        $this->db->where('id_scholar_applicant', $id_scholar_applicant);
        $this->db->where('id_scholarship_scheme', $id_scholarship_scheme);
        $this->db->where('id_application', $id_application);
        $query = $this->db->get();
         $result = $query->row();
        return $result;
    }
    
    function getEmploymentDetailsByApplicationId($id_scholar_applicant,$id_scholarship_scheme,$id_application)
    {
        $this->db->select('*');
        $this->db->from('scholar_applicant_employment_status');
        $this->db->where('id_scholar_applicant', $id_scholar_applicant);
        $this->db->where('id_scholarship_scheme', $id_scholarship_scheme);
        $this->db->where('id_application', $id_application);
        $query = $this->db->get();
         $result = $query->result();
        return $result;
    }


    function editScholarshipApplication($data,$id)
    {
        $this->db->where_in('id', $id);
      $this->db->update('scholarship_application', $data);
      return TRUE;
    }


    function programListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('scholarship_programme');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    
    function nationalityList()
    {
        $this->db->select('*');
        $this->db->from('nationality');
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function programList()
    {
        $this->db->select('*');
        $this->db->from('scholarship_programme');
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    

    function countryList()
    {
        $this->db->select('*');
        $this->db->from('scholarship_country');
        $this->db->where('status', '1');
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function stateList()
    {
        $this->db->select('*');
        $this->db->from('scholarship_state');
        $this->db->where('status', '1');
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function getStudentDetails($id)
    {
        $this->db->select('ia.*');
        $this->db->from('scholar_applicant as ia');
        $this->db->where('ia.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function raceListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('scholarship_race_setup');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }


    function religionListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('scholarship_religion_setup');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }




}