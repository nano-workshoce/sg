<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class AccreditationCategory extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('accreditation_category_model');
        $this->isScholarLoggedIn();
    }

    function list()
    {
        if ($this->checkScholarAccess('accreditation_category.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;
            $data['accreditationCategoryList'] = $this->accreditation_category_model->accreditationCategoryListSearch($name);
            $this->global['pageTitle'] = 'Scholarship Management System : Accreditation Category List';
            $this->loadViews("accreditation_category/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkScholarAccess('accreditation_category.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'status' => $status
                );
                //echo "<Pre>"; print_r($subjectDetails);exit;

                $result = $this->accreditation_category_model->addNewAccreditationCategory($data);
                redirect('/scholarship/accreditationCategory/list');
            }
            $this->global['pageTitle'] = 'Scholarship Management System : Add Accreditation Category';
            $this->loadViews("accreditation_category/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkScholarAccess('accreditation_category.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/scholarship/accreditationCategory/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'status' => $status
                );

                $result = $this->accreditation_category_model->editAccreditationCategory($data,$id);
                redirect('/scholarship/accreditationCategory/list');
            }
            $data['accreditationCategory'] = $this->accreditation_category_model->getAccreditationCategory($id);
            $this->global['pageTitle'] = 'Scholarship Management System : Edit Accreditation Category';
            $this->loadViews("accreditation_category/edit", $this->global, $data, NULL);
        }
    }
}
