<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Role extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('role_model');
        $this->load->model('permission_model');
        $this->isScholarLoggedIn();
    }

    function list()
    {
        if ($this->checkScholarAccess('role.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['status'] = $this->security->xss_clean($this->input->post('status'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
 
            $data['searchParam'] = $formData;
            $data['roleRecords'] = $this->role_model->roleListSearch($formData);
            $this->global['pageTitle'] = 'Scholarship Management System : Role List';
            $this->loadViews("role/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkScholarAccess('role.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $role = $this->security->xss_clean($this->input->post('role'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'role' => $role,
                    'status' => $status
                );
                //echo "<Pre>"; print_r($subjectDetails);exit;

                $result = $this->role_model->addNewRole($data);
                redirect('/scholarship/role/list');
            }
            $this->global['pageTitle'] = 'Scholarship Management System : Add Role';
            $this->loadViews("role/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkScholarAccess('role.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/scholarship/role/list');
            }
            if($this->input->post())
            {
                $role = $this->security->xss_clean($this->input->post('role'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'role' => $role,
                    'status' => $status
                );

                $result = $this->role_model->editRole($data,$id);
                redirect('/scholarship/role/list');
            }
            $data['role'] = $this->role_model->getRole($id);
            $this->global['pageTitle'] = 'Scholarship Management System : Edit Role';
            $this->loadViews("role/edit", $this->global, $data, NULL);
        }
    }


    function logout()
    {
        // echo "string";exit();
        $sessionArray = array('id_scholar'=> '',                    
                    'scholar_name'=> '',
                    'scholar_role'=> '',
                    'scholarship_last_login'=> '',
                    'isScholarLoggedIn'=> FALSE
            );

     $this->session->set_userdata($sessionArray);
     // $this->session->sess_destroy();
     // redirect($_SERVER['HTTP_REFERER']);
     $this->isScholarLoggedIn();
    }
}
