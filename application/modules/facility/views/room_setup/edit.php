<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit Room</h3>
        </div>
        <form id="form_bank" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Room Details</h4>



            <div class="row">

                

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Code <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="code" name="code" value="<?php echo $roomSetup->code;?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name" value="<?php echo $roomSetup->name;?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Building<span class='error-text'>*</span></label>
                        <select name="id_facility_building_registration" id="id_facility_building_registration" class="form-control" >
                            <option value="">Select</option>
                            <?php
                            if (!empty($hostelList))
                            {
                                foreach ($hostelList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>" 
                                 <?php 
                                        if($record->id == $roomSetup->id_facility_building_registration)
                                        {
                                            echo "selected=selected";
                                        } ?>
                                >

                                >
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
            
          

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Room Type <span class='error-text'>*</span></label>
                        <select name="id_facility_room_type" id="id_facility_room_type" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($roomTypeList))
                            {
                                foreach ($roomTypeList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>"
                                 <?php 
                                        if($record->id == $roomSetup->id_facility_room_type)
                                        {
                                            echo "selected=selected";
                                        } ?>
                                >
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>


     <div class="col-sm-4">
                    <div class="form-group">
                        <label>Cost per Day <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="cost" name="cost"  value="<?php echo $roomSetup->cost; ?>" >
                    </div>
                </div>

                  <div class="col-sm-4">
                    <div class="form-group">
                        <label>Cost per Hour <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="cost_hour" name="cost_hour" value="<?php echo $roomSetup->cost_hour; ?>" >
                    </div>
                </div>

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Equipment <span class='error-text'>*</span></label>
                        <select name="id_equipment[]" id="id_equipment" class="form-control" multiple="true">
                            <?php
                            if (!empty($equipmentList))
                            {
                                foreach ($equipmentList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                

               
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Max. Capacity <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="max_capacity" name="max_capacity" value="<?php echo $roomSetup->max_capacity; ?>" min="1" >
                    </div>
                </div>


               <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status *</p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" <?php if($roomSetup->status=='1') {
                                 echo "checked=checked";
                              };?>><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0" <?php if($roomSetup->status=='0') {
                                 echo "checked=checked";
                              };?>>
                              <span class="check-radio"></span> In-Active
                            </label>                              
                        </div>                         
                </div>

            </div>

        </div>

        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="../list" class="btn btn-link">Cancel</a>
            </div>
        </div>
            
        </form>



      








    </div>

        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
</div>
<script>

    $('select').select2();

</script>
