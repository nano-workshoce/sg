<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Mark_distribution_model extends CI_Model
{
    function courseListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('course');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function intakeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('intake');
        $this->db->where('status', $status);
        $this->db->order_by("year", "DESC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }


    function programListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function examComponentListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('examination_components');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function markDistributionListSearch($data)
    {
        // $date = 
        $this->db->select('a.*, p.code as program_code, p.name as program_name, i.year as intake_year, i.name as intake_name, c.code as course_code, c.name as course_name');
        $this->db->from('mark_distribution as a');
        $this->db->join('intake as i', 'a.id_intake = i.id');
        $this->db->join('programme as p', 'a.id_program = p.id');
        $this->db->join('course as c', 'a.id_course = c.id');
        if ($data['id_program'] != '')
        {
            $this->db->where('a.id_program', $data['id_program']);
        }
        if ($data['id_intake'] != '')
        {
            $this->db->where('a.id_intake', $data['id_intake']);
        }
        if ($data['id_course'] != '')
        {
            $this->db->where('a.id_course', $data['id_course']);
        }
        if ($data['status'] != '')
        {
            $this->db->where('a.status', $data['status']);
        }
         $query = $this->db->get();
         $result = $query->result();  

         // $list = array();
         // foreach ($result as $value)
         // {
         //    $data = $this->getExamCenterNLocationByCenterId($value->id_exam_center);
         //    $value->exam_center = $data['exam_center'];
         //    $value->location = $data['location'];
            
         //    array_push($list, $value);
         // }
         return $result;
    }

    function getIntakeByProgrammeId($id_programme)
    {
        $this->db->select('DISTINCT(i.id) as id, i.*');
        $this->db->from('programme_landscape as ihs');
        $this->db->join('intake as i', 'ihs.id_intake = i.id');
        $this->db->where('ihs.id_programme', $id_programme);
        $query = $this->db->get();
        return $query->result();
    }

    function getMarkDistribution($id)
    {
        $this->db->select('*');
        $this->db->from('mark_distribution');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addMarkDistribution($data)
    {
        $this->db->trans_start();
        $this->db->insert('mark_distribution', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editMarkDistribution($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('mark_distribution', $data);
        return TRUE;
    }

    function saveTempDetailData($data)
    {
        $this->db->trans_start();
        $this->db->insert('temp_mark_distribution_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getTempMarkDistributionDetailsBySessionId($id_session)
    {
        $this->db->select('tctd.*, ec.name as component_name, ec.code as component_code');
        $this->db->from('temp_mark_distribution_details as tctd');
        $this->db->join('examination_components as ec', 'tctd.id_component = ec.id');
        $this->db->where('tctd.id_session', $id_session);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function deleteTempMarkDistribution($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('temp_mark_distribution_details');
        return TRUE;
    }


    function deleteTempMarkDistributionBySession($id_session)
    {
        $this->db->where('id_session', $id_session);
        $this->db->delete('temp_mark_distribution_details');
        return TRUE;
    }

    function getCourseCreditHours($id)
    {
        $this->db->select('*');
        $this->db->from('course');
        $this->db->where('id', $id);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->row();

         if($result)
         {
            return $result->credit_hours;
         }
        return 0;
    }




    function moveDetailDataFromTempToMain($id_mark_distribution)
    {
        $id_session = $this->session->my_session_id;
        $details = $this->getTempMarkDistributionDetailsBySessionIdForMove($id_session);
        foreach ($details as $detail)
        {
            $detail->id_mark_distribution = $id_mark_distribution;
            unset($detail->id_session);
            unset($detail->id);

            $added = $this->addMarkDistributionDetails($detail);
            # code...
        }
        
        $added = $this->deleteTempMarkDistributionBySession($id_session);
    }

    function addMarkDistributionDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('mark_distribution_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }


    function getTempMarkDistributionDetailsBySessionIdForMove($id_session)
    {
        $this->db->select('tctd.*');
        $this->db->from('temp_mark_distribution_details as tctd');
        $this->db->where('tctd.id_session', $id_session);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getStudentByStudentId($id_student)
    {
        $this->db->select('s.*, p.name as programme_name, i.name as intake_name, qs.name as qualification_name, qs.code as qualification_code');
        $this->db->from('student as s');
        $this->db->join('programme as p', 's.id_program = p.id'); 
        $this->db->join('intake as i', 's.id_intake = i.id'); 
        $this->db->join('qualification_setup as qs', 's.id_degree_type = qs.id');
        $this->db->where('s.id', $id_student);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    

    function getExamCenterNLocationByCenterId($id)
    {
        $this->db->select('*');
        $this->db->from('exam_center');
        $this->db->where('id', $id);
        $query = $this->db->get();
        $data['exam_center'] = $query->row()->name;



        $id_location = $query->row()->id_location;

        $this->db->select('*');
        $this->db->from('exam_center_location');
        $this->db->where('id', $id_location);
        $query = $this->db->get();
        $data['location'] = $query->row()->name;

        return $data;
    }

    function generateMarkDistributionNumber()
    {

        $year = date('y');
        $Year = date('Y');
            $this->db->select('*');
            $this->db->from('mark_distribution');
            $this->db->order_by("id", "desc");
            $query = $this->db->get();
            $result = $query->num_rows();

     
            $count= $result + 1;

           $generated_number = "CT" .(sprintf("%'06d", $count)). "/" . $Year;
           // echo "<Pre>";print_r($generated_number);exit();
           return $generated_number;
    }

    function getMarkDistributionDetailsByIdMarkDistribution($id_mark_distribution)
    {
        $this->db->select('tctd.*, ec.name as component_name, ec.code as component_code,');
        $this->db->from('mark_distribution_details as tctd');
        $this->db->join('examination_components as ec', 'tctd.id_component = ec.id');
        $this->db->where('tctd.id_mark_distribution', $id_mark_distribution);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function deleteDetailData($id)
    {
         $this->db->where('id', $id);
        $this->db->delete('mark_distribution_details');
        return TRUE;
    }

    function getProgramLandscapeCourses($data)
    {
        $this->db->select('DISTINCT(c.id) as id, c.name , c.code, a.id as id_course_registered');
        $this->db->from('add_course_to_program_landscape as a');
        $this->db->join('intake as in', 'a.id_intake = in.id');
        $this->db->join('course as c', 'a.id_course = c.id');
        $this->db->join('programme_landscape as pl', 'a.id_program_landscape = pl.id');
        $this->db->where('a.id_intake', $data['id_intake']);
        $this->db->where('pl.status', '1');
         $this->db->where('pl.id_programme', $data['id_programme']);
         $this->db->where('a.id_program_landscape', $data['id_programme_landscape']);
        $query = $this->db->get();
         $result = $query->result();
         return $result; 
    }

    function getCourseForLandscape($id_course_registered)
    {
        $this->db->select('a.*, c.code, c.name');
        $this->db->from('add_course_to_program_landscape as a');
        $this->db->join('course as c', 'a.id_course = c.id', 'left');
        $this->db->where('a.id', $id_course_registered);
         $query = $this->db->get();
         $result = $query->row();
         return $result; 
    }

    function getLandscapeListByProgramIdNIntakeId($data)
    {
        $this->db->select('a.*');
        $this->db->from('programme_landscape as a');
        $this->db->where('a.id_programme', $data['id_programme']);
        $this->db->where('a.id_intake', $data['id_intake']);
         $query = $this->db->get();
         $result = $query->result();
         return $result; 
    }

    function programmeLandscapeListByStatus($status)
    {
        $this->db->select('a.*');
        $this->db->from('programme_landscape as a');
        $this->db->where('a.status', $status);
         $query = $this->db->get();
         $result = $query->result();
         return $result; 
    }
}