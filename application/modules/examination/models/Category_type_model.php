<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Category_type_model extends CI_Model
{
    function categoryTypeList()
    {
        $this->db->select('*');
        $this->db->from('category_type');
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }


    function getCategoryTypeDetails($id)
    {
        $this->db->select('*');
        $this->db->from('category_type');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewCategoryType($data)
    {
        $this->db->trans_start();
        $this->db->insert('category_type', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editCategoryTypeDetails($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('category_type', $data);
        return TRUE;
    }
    
    // function deleteActivityDetails($id, $date)
    // {
    //     $this->db->where('id', $countryId);
    //     $this->db->update('academic_year', $countryInfo);
    //     return $this->db->affected_rows();
    // }
}

