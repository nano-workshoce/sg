<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class StudentMarksEntry extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('student_marks_entry_model');
        // $this->load->model('main_invoice_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('student_marks_entry.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            // echo "<Pre>";print_r("ss");exit();
            $formData['id_programme'] = $this->security->xss_clean($this->input->post('id_programme'));
            $formData['id_intake'] = $this->security->xss_clean($this->input->post('id_intake'));
            $formData['id_semester'] = $this->security->xss_clean($this->input->post('id_semester'));
            $formData['id_student'] = $this->security->xss_clean($this->input->post('id_studentent'));
            $formData['status'] = '';

            $data['searchParam'] = $formData;


            $data['programmeList'] = $this->student_marks_entry_model->getProgrammeListByStatus('1');
            $data['intakeList'] = $this->student_marks_entry_model->getIntakeListByStatus('1');
            $data['semesterList'] = $this->student_marks_entry_model->getSemesterListByStatus('1');


            $data['semesterResultList'] = $this->student_marks_entry_model->semesterResultList($formData);
            // echo "<Pre>";print_r($data['semesterResultList']);exit;
            $this->global['pageTitle'] = 'Campus Management System : Receipt List';
            $this->loadViews("student_marks_entry/list", $this->global, $data, NULL);
        }
    }

    
    
    function add()
    {
        $user_id = $this->session->userId;
        if ($this->checkAccess('student_marks_entry.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {

                $formData = $this->input->post();

                $id_session = $this->session->my_session_id;
                $user_id = $this->session->userId;

                $id_programme = $this->security->xss_clean($this->input->post('id_programme'));
                $id_intake = $this->security->xss_clean($this->input->post('id_intake'));
                $main_grade = $this->security->xss_clean($this->input->post('main_grade'));
                $main_marks = $this->security->xss_clean($this->input->post('main_marks'));
                $main_result = $this->security->xss_clean($this->input->post('main_result'));
                $id_student = $this->security->xss_clean($this->input->post('id_student'));
                $id_semester = $this->security->xss_clean($this->input->post('id_semester'));

                // $student_marks_entry_number = $this->student_marks_entry_model->generateReceiptNumber();

                $data = array(
                    'id_semester' => $id_semester,
                    'id_student' => $id_student,
                    'id_program' => $id_programme,
                    'id_intake' => $id_intake,
                    'grade' => $main_grade,
                    'marks' => $main_marks,
                    'result' => $main_result,
                    'status' => '0',
                    'created_by' => $user_id
                );
                // echo "<Pre>";print_r($data);exit;
                $inserted_id = $this->student_marks_entry_model->addNewMarksEntry($data);


                // $id_main_invoice = $this->security->xss_clean($this->input->post('id_main_invoice'));
                // $invoice_amount = $this->security->xss_clean($this->input->post('invoice_amount'));
                // $paid_amount = $this->security->xss_clean($this->input->post('paid_amount'));



                 for($i=0;$i<count($formData['id_course']);$i++)
                 {
                // echo "<Pre>";print_r($formData);exit;

                    $id_course = $formData['id_course'][$i];
                    $grade = $formData['grade'][$i];
                    $marks = $formData['marks'][$i];
                    $result = $formData['result'][$i];
                    $id_exam_registration = $formData['id_exam_registration'][$i];

                    if($marks > 0)
                    {
                        $detailsData = array(
                        'id_student_semester_result' => $inserted_id,
                        'id_exam_registration' => $id_exam_registration,
                        'id_course' => $id_course,
                        'grade' => $grade,
                        'marks' => $marks,                        
                        'result' => $result,
                        'status' => '1',
                        'created_by' => $user_id
                    );
                    $inserted_detail = $this->student_marks_entry_model->addNewMarksEntryByCourse($detailsData);

                        if($inserted_detail)
                        {
                            $exam_reg_data = array(
                                'is_semester_result' => $inserted_detail
                            );
                            $updated_exam_reg = $this->student_marks_entry_model->updateExamRegistration($exam_reg_data,$id_exam_registration);
                        }
                    }
                 }


                // $this->student_marks_entry_model->deleteTempDataBySession($id_session);
                // $this->student_marks_entry_model->deleteTempAmountDataBySession($id_session);

                redirect('/examination/studentMarksEntry/list');
            }

            

            $data['gradeList'] = $this->student_marks_entry_model->gradeListByStatus('1');
            $data['programmeList'] = $this->student_marks_entry_model->getProgrammeListByStatus('1');
            $data['gradeList'] = $this->student_marks_entry_model->getGradeListByStatus('1');
            $data['intakeList'] = $this->student_marks_entry_model->getIntakeListByStatus('1');
            $this->global['pageTitle'] = 'Campus Management System : Add Receipt';
            $this->loadViews("student_marks_entry/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('student_marks_entry.view') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/examination/studentMarksEntry/list');
            }
            if($this->input->post())
            {
                redirect('/examination/studentMarksEntry/list');
            }
            $data['marksEntry'] = $this->student_marks_entry_model->getMarksEntry($id);
            $data['marksEntryDetails'] = $this->student_marks_entry_model->getMarksEntryDetailsByMasterId($id);

            // echo "<Pre>";print_r($data);exit();

            $this->global['pageTitle'] = 'Campus Management System : View Marks Entry';
            $this->loadViews("student_marks_entry/edit", $this->global, $data, NULL);
        }
    }





    function approvalList()
    {
        // if ($this->checkAccess('pr_entry_approval.list') == 1)
        if ($this->checkAccess('student_marks_entry_approval.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        { 
            
            // echo "<Pre>";print_r("ss");exit();
            $formData['id_programme'] = $this->security->xss_clean($this->input->post('id_programme'));
            $formData['id_intake'] = $this->security->xss_clean($this->input->post('id_intake'));
            $formData['id_semester'] = $this->security->xss_clean($this->input->post('id_semester'));
            $formData['id_student'] = $this->security->xss_clean($this->input->post('id_studentent'));
            $formData['status'] = '0';

            $data['searchParam'] = $formData;


            $data['programmeList'] = $this->student_marks_entry_model->getProgrammeListByStatus('1');
            $data['intakeList'] = $this->student_marks_entry_model->getIntakeListByStatus('1');
            $data['semesterList'] = $this->student_marks_entry_model->getSemesterListByStatus('1');


            $data['semesterResultList'] = $this->student_marks_entry_model->semesterResultList($formData);
            // echo "<Pre>";print_r($data['semesterResultList']);exit;


            $this->global['pageTitle'] = 'Campus Management System : Approve Student Marks Entry';
            $this->loadViews("student_marks_entry/approval_list", $this->global, $data, NULL);
        }
    }





    function view($id = NULL)
    {
        if ($this->checkAccess('student_marks_entry.approve') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/examination/studentMarksEntry/list');
            }

             if($this->input->post())
            {
                
                $status = $this->security->xss_clean($this->input->post('status'));
                $reason = $this->security->xss_clean($this->input->post('reason'));


                $data = array(
                    'status' => $status,
                    'reason' => $reason
                );
                $result = $this->student_marks_entry_model->editStudentMarksEntry($data,$id);
                redirect('/examination/studentMarksEntry/approvalList');
            }



            // if($this->input->post())
            // {
            //     redirect('/examination/studentMarksEntry/list');
            // }
            $data['marksEntry'] = $this->student_marks_entry_model->getMarksEntry($id);
            $data['marksEntryDetails'] = $this->student_marks_entry_model->getMarksEntryDetailsByMasterId($id);

            // echo "<Pre>";print_r($data);exit();

            $this->global['pageTitle'] = 'Campus Management System : View Marks Entry';
            $this->loadViews("student_marks_entry/view", $this->global, $data, NULL);
        }
    }

























































































    function studentList()
    {
        if ($this->checkAccess('student_marks_entry.student_list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
        
                $formData['id_program'] = $this->security->xss_clean($this->input->post('id_program'));
                $formData['id_intake'] = $this->security->xss_clean($this->input->post('id_intake'));
                $formData['id_programme_landscape'] = $this->security->xss_clean($this->input->post('id_programme_landscape'));
                $formData['id_course'] = $this->security->xss_clean($this->input->post('id_course'));
                
                $data['searchParam'] = $formData;
                $data['applicantList'] = $this->student_marks_entry_model->getStudentListForCourseRegisteredStudent($formData);

            }

            $data['intakeList'] = $this->student_marks_entry_model->intakeListByStatus('1');
            $data['programList'] = $this->student_marks_entry_model->programListByStatus('1');

            // echo "<Pre>";print_r($data['applicantList']);exit;

            $this->global['pageTitle'] = 'Campus Management System : Receipt List';
            $this->loadViews("student_marks_entry/student_list", $this->global, $data, NULL);
        }
    }

    function addMarksEntry($id_program, $id_intake, $id_course_registered_landscape, $id_course_registration, $id_student)
    {
        // echo "<Pre>";print_r($id_student);exit;

        $user_id = $this->session->userId;
        if ($this->checkAccess('student_marks_entry.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id_student == null)
            {
                redirect('/examination/studentMarksEntry/studentList');
            }

            if($this->input->post())
            {

                $formData = $this->input->post();
                // echo "<Pre>";print_r($formData);exit;

                $id_session = $this->session->my_session_id;
                $user_id = $this->session->userId;

                $id_student = $this->security->xss_clean($this->input->post('id_student'));
                $id_program = $this->security->xss_clean($this->input->post('id_program'));
                $id_intake = $this->security->xss_clean($this->input->post('id_intake'));
                $id_course_registration = $this->security->xss_clean($this->input->post('id_course_registration'));
                $id_course_registered_landscape = $this->security->xss_clean($this->input->post('id_course_registered_landscape'));
                $id_mark_distribution = $this->security->xss_clean($this->input->post('id_mark_distribution'));
                $grade = $this->security->xss_clean($this->input->post('grade'));

                // $student_marks_entry_number = $this->student_marks_entry_model->generateReceiptNumber();

                $data = array(
                    'id_student' => $id_student,
                    'id_program' => $id_program,
                    'id_intake' => $id_intake,
                    'id_course_registration' => $id_course_registration,
                    'id_course_registered_landscape' => $id_course_registered_landscape,
                    'id_mark_distribution' => $id_mark_distribution,
                    'grade' => $grade,
                    'status' => '0',
                    'created_by' => $user_id
                );
                // echo "<Pre>";print_r($data['studentDetails']);exit;
                $inserted_id = $this->student_marks_entry_model->addStudentMarksEntry($data);

                // echo "<Pre>";print_r($inserted_id);exit;

                $total_course_marks = 0;
                $total_obtained_marks = 0;
                $total_min_pass_marks = 0;

                if($inserted_id)
                {

                     for($i=0;$i<count($formData['obtained_marks']);$i++)
                     {
                        $obtained_marks = $formData['obtained_marks'][$i];
                    
                        // echo "<Pre>";print_r($obtained_marks);exit;

                        if($obtained_marks > 0)
                        {


                            $id_marks_distribution_details = $formData['id_marks_distribution_details'][$i];
                            $max_marks = $formData['max_marks'][$i];
                            $pass_marks = $formData['pass_marks'][$i];
                            $id_component = $formData['id_component'][$i];

                            $result = 'Pass';

                            if($obtained_marks < $pass_marks)
                            {
                                $result = 'Fail';
                            }
                            
                                $detailsData = array(
                                'id_student_marks_entry' => $inserted_id,
                                'id_marks_distribution_details' => $id_marks_distribution_details,
                                'max_marks' => $max_marks,
                                'obtained_marks' => $obtained_marks,
                                'pass_marks' => $pass_marks,
                                'id_component' => $id_component,
                                'result' => $result,
                                'status' => '1',
                                'created_by' => $user_id
                            );

                            $inserted_detail = $this->student_marks_entry_model->addStudentMarksEntryDetails($detailsData);
                            // echo "<Pre>";print_r($inserted_detail);exit;

                            $total_course_marks = $total_course_marks + $max_marks;
                            $total_obtained_marks = $total_obtained_marks + $obtained_marks;
                            $total_min_pass_marks = $total_min_pass_marks + $pass_marks;
                             

                        }

                    }

                    $total_result = 'Pass';
                    if($total_obtained_marks < $total_min_pass_marks)
                    {
                        $total_result = 'Fail';
                    }

                    $student_marks_data = array(
                        'total_course_marks' => $total_course_marks,
                        'total_obtained_marks' => $total_obtained_marks,
                        'total_min_pass_marks' => $total_min_pass_marks,
                        'result' => $total_result
                    );

                    $updated_student_marks_data = $this->student_marks_entry_model->updateStudentMarksEntry($student_marks_data,$inserted_id);

                    
                    if($updated_student_marks_data)
                    {

                        

                        $exam_marks_data = array(
                            'total_course_marks' => $total_course_marks,
                            'total_obtained_marks' => $total_obtained_marks,
                            'total_min_pass_marks' => $total_min_pass_marks,
                            'total_result' => $total_result,
                            'is_result_announced' => $inserted_id,
                            'grade' => $grade,
                            'updated_by' => $user_id
                        );

                        $add_course_marks_history = $this->student_marks_entry_model->combineDataAddMarkHistoryOfStudent($exam_marks_data,$id_course_registration,$id_student,$inserted_id);
                        
                        
                        if($add_course_marks_history)
                        {

                        $updated_exam_reg = $this->student_marks_entry_model->updateCourseRegistration($exam_marks_data,$id_course_registration);
                        }
                        // echo "<Pre>";print_r($updated_exam_reg);exit;
                    }

                }


                // $this->student_marks_entry_model->deleteTempDataBySession($id_session);
                // $this->student_marks_entry_model->deleteTempAmountDataBySession($id_session);

                redirect('/examination/studentMarksEntry/studentList');
            }

            $data['gradeList'] = $this->student_marks_entry_model->gradeListByStatus('1');

            $data['programmeList'] = $this->student_marks_entry_model->getProgrammeListByStatus('1');
            $data['gradeList'] = $this->student_marks_entry_model->getGradeListByStatus('1');
            $data['intakeList'] = $this->student_marks_entry_model->getIntakeListByStatus('1');
            $data['studentDetails'] = $this->student_marks_entry_model->getStudentByStudent($id_student);
            
            $data['markDistributionDetails'] = $this->student_marks_entry_model->getMarkDistributionByProgNIntakeNIdCourseRegLandscape($id_program, $id_intake, $id_course_registered_landscape);

            $data['courseRegisteredDetails'] = $this->student_marks_entry_model->getCourseRegisteredDetails($id_course_registered_landscape);

            $data['id_program'] = $id_program;
            $data['id_intake'] = $id_intake;
            $data['id_course_registered_landscape'] = $id_course_registered_landscape;
            $data['id_student'] = $id_student;
            $data['id_course_registration'] = $id_course_registration;
            
                // echo "<Pre>";print_r($data['courseRegisteredDetails']);exit;

            $this->global['pageTitle'] = 'Campus Management System : Add Receipt';
            $this->loadViews("student_marks_entry/add_marks", $this->global, $data, NULL);
        }
    }


    function marksEntryApprovalList()
    {
        // if ($this->checkAccess('pr_entry_approval.list') == 1)
        if ($this->checkAccess('student_marks_entry.approval_list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        { 
            
            // echo "<Pre>";print_r("ss");exit();
            $formData['id_programme'] = $this->security->xss_clean($this->input->post('id_programme'));
            $formData['id_intake'] = $this->security->xss_clean($this->input->post('id_intake'));
            $formData['id_student'] = $this->security->xss_clean($this->input->post('id_studentent'));
            $formData['status'] = '0';

            $data['searchParam'] = $formData;

            $data['marksEntryList'] = $this->student_marks_entry_model->marksEntryListSearch($formData);



            // echo "<Pre>";print_r($data['semesterResultList']);exit;


            $data['programmeList'] = $this->student_marks_entry_model->getProgrammeListByStatus('1');
            $data['intakeList'] = $this->student_marks_entry_model->getIntakeListByStatus('1');
            $data['studentList'] = $this->student_marks_entry_model->getStudentList();



            $this->global['pageTitle'] = 'Campus Management System : Approve Student Marks Entry';
            $this->loadViews("student_marks_entry/marks_approval_list", $this->global, $data, NULL);
        }

    }




    function approve($id = NULL)
    {
        if ($this->checkAccess('student_marks_entry.approve') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $user_id = $this->session->userId;

            if ($id == null)
            {
                redirect('/examination/studentMarksEntry/marksEntryApprovalList');
            }

             if($this->input->post())
            {
                // $formData = $this->input->post();
                // echo "<Pre>";print_r($formData);exit;
                
                $status = $this->security->xss_clean($this->input->post('status'));
                $reason = $this->security->xss_clean($this->input->post('reason'));


                $data = array(
                    'status' => $status,
                    'reason' => $reason,
                    'updated_by' => $user_id
                );
                $result = $this->student_marks_entry_model->updateStudentMarksEntry($data,$id);
                redirect('/examination/studentMarksEntry/marksEntryApprovalList');
            }


            $data['marksEntry'] = $this->student_marks_entry_model->getStudentMarksEntry($id);
            $data['marksEntryDetails'] = $this->student_marks_entry_model->getStudentMarksEntryDetailsByMasterId($id);

            $data['courseRegisteredDetails'] = $this->student_marks_entry_model->getCourseRegisteredDetails($data['marksEntry']->id_course_registered_landscape);
            $data['studentDetails'] = $this->student_marks_entry_model->getStudentByStudent($data['marksEntry']->id_student);

            // echo "<Pre>";print_r($data['marksEntryDetails']);exit();

            $this->global['pageTitle'] = 'Campus Management System : Approve Marks Entry';
            $this->loadViews("student_marks_entry/approve", $this->global, $data, NULL);
        }
    }


    function summaryList()
    {
        // if ($this->checkAccess('pr_entry_approval.list') == 1)
        if ($this->checkAccess('student_marks_entry_approval.summary_list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        { 
            
            // echo "<Pre>";print_r("ss");exit();
            $formData['id_programme'] = $this->security->xss_clean($this->input->post('id_programme'));
            $formData['id_intake'] = $this->security->xss_clean($this->input->post('id_intake'));
            $formData['id_student'] = $this->security->xss_clean($this->input->post('id_studentent'));
            $formData['status'] = '';

            $data['searchParam'] = $formData;

            $data['marksEntryList'] = $this->student_marks_entry_model->marksEntryListSearch($formData);



            // echo "<Pre>";print_r($data['semesterResultList']);exit;


            $data['programmeList'] = $this->student_marks_entry_model->getProgrammeListByStatus('1');
            $data['intakeList'] = $this->student_marks_entry_model->getIntakeListByStatus('1');
            $data['studentList'] = $this->student_marks_entry_model->getStudentList();



            $this->global['pageTitle'] = 'Campus Management System : Approve Student Marks Entry';
            $this->loadViews("student_marks_entry/summary_list", $this->global, $data, NULL);
        }

    }


    function viewSummary($id = NULL)
    {
        if ($this->checkAccess('student_marks_entry.summary_view') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $user_id = $this->session->userId;

            if ($id == null)
            {
                redirect('/examination/studentMarksEntry/marksEntryApprovalList');
            }

             if($this->input->post())
            {

            }


            $data['marksEntry'] = $this->student_marks_entry_model->getStudentMarksEntry($id);
            $data['marksEntryDetails'] = $this->student_marks_entry_model->getStudentMarksEntryDetailsByMasterId($id);

            $data['courseRegisteredDetails'] = $this->student_marks_entry_model->getCourseRegisteredDetails($data['marksEntry']->id_course_registered_landscape);
            $data['studentDetails'] = $this->student_marks_entry_model->getStudentByStudent($data['marksEntry']->id_student);

            // echo "<Pre>";print_r($data['marksEntryDetails']);exit();

            $this->global['pageTitle'] = 'Campus Management System : View Marks Entry';
            $this->loadViews("student_marks_entry/view_summary", $this->global, $data, NULL);
        }
    }




     function searchStudentList()
    {
        $id_session = $this->session->my_session_id;
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        // $tempData['id_semester'] = '';
        // $tempData['status'] = '';

        $student_list_data = $this->student_marks_entry_model->searchStudentList($tempData);
        // echo "<Pre>";print_r($student_list_data);exit;


        $table="<select name='id_student' id='id_student' class='form-control' onchange='getSemesterByStudentId(this.value)'>";
        $table.="<option value=''>Select</option> 

        <script type='text/javascript'>
            $('select').select2();
        </script>
                ";

        for($i=0;$i<count($student_list_data);$i++)
        {

        // $id = $results[$i]->id_procurement_category;
        $id = $student_list_data[$i]->id;
        $full_name = $student_list_data[$i]->full_name;
        $nric = $student_list_data[$i]->nric;
        $table.="<option value=".$id.">".$nric . " - " .$full_name.
                "</option>";

        }
        $table.="</select>";
        
        echo $table;
        exit();
    }

    function getSemesterByStudentId($id_student)
    {

        $semester_list_data = $this->student_marks_entry_model->getSemesterByStudentId($id_student);
        // echo "<Pre>";print_r($semester_list_data);exit;


        $table="<select name='id_semester' id='id_semester' class='form-control' onchange='getCourse()'>";
        $table.="<option value=''>Select</option>


        <script type='text/javascript'>
            $('select').select2();
        </script>";

        for($i=0;$i<count($semester_list_data);$i++)
        {



        // $id = $results[$i]->id_procurement_category;
        $id = $semester_list_data[$i]->id;
        $semester_name = $semester_list_data[$i]->semester_name;
        $semester_code = $semester_list_data[$i]->semester_code;
        $table.="<option value=".$id.">".$semester_code . " - " . $semester_name.
                "</option>";

        }
        $table.="</select>";
        
        echo $table;
        exit();

    }

    function getCources()
    {
        $id_session = $this->session->my_session_id;
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        // $tempData['id_semester'] = '';
        // $tempData['status'] = '';
        // echo "<Pre>";print_r($tempData);exit;

        if(empty($tempData['id_programme']) || empty($tempData['id_intake']) || empty($tempData['id_semester']) || empty($tempData['id_student']))
        {
            echo "Select The Proper Data Of The Student";exit();
        }

        if($tempData['id_programme'] == '' || $tempData['id_intake'] == '' || $tempData['id_semester'] == '' || $tempData['id_student'] == '')
        {
            echo "Select The Proper Data Of The Student";exit();
        }

        // echo "<Pre>";print_r($tempData);exit;
        $student_data = $this->student_marks_entry_model->getStudentInfo($tempData);
        // echo "<Pre>";print_r($student_data);exit;

        if(empty($student_data))
        {
            echo "Courses Not Available For This Student";exit();
        }

        $student_name = $student_data->full_name;
        $nric = $student_data->nric;
        $semester_name = $student_data->semester_name;
        $semester_code = $student_data->semester_code;
        // echo "<Pre>";print_r($student_name);exit;

        $table  = "

        <div class='form-container'>
            <h4 class='form-group-title'>Student Details</h4>

                <div class='data-list'>
                    <div class='row'>
    
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Student Name :</dt>
                                <dd>$student_name</dd>
                            </dl>
                            <dl>
                                <dt>Student NRIC :</dt>
                                <dd>$nric</dd>
                            </dl>
                        </div>        
                        
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Semester Code :</dt>
                                <dd>
                                    $semester_code
                                </dd>
                            </dl>
                            <dl>
                                <dt>Semester Name :</dt>
                                <dd>$semester_name</dd>
                            </dl>
                        </div>
    
                    </div>
                </div>
        </div>
                <br>
            ";

        // echo "<Pre>";print_r($table);exit;


            

            // $invoice_data = $this->student_marks_entry_model->getInvoicesByStudentId($id);
            // echo "<Pre>";print_r($tempData);exit;
            $course_list_data = $this->student_marks_entry_model->getCourseForMarksEntry($tempData);


            if(!empty($course_list_data))
            {

                $table .= "
            <div class='form-container'>
            <h4 class='form-group-title'>Course Details</h4>

            <div class='custom-table'>
                <table class='table' id='list-table'>
                  <tr>
                    <th>Sl. No</th>
                    <th >Course Name</th>
                    <th style='text-align: center;'>Grade</th>
                    <th style='text-align: center;'>Marks</th>
                    <th style='text-align: center;'>Result</th>
                </tr>

                ";


                // echo "<Pre>";print_r($course_list_data);exit;

            for($i=0;$i<count($course_list_data);$i++)
                {
                    $id_exam_registration = $course_list_data[$i]->id_exam_registration;
                    $id_course = $course_list_data[$i]->id;
                    $course_name = $course_list_data[$i]->name;
                    $course_code = $course_list_data[$i]->code;
                    $course = $course_code . " - " . $course_name;
                    $j=$i+1;
                    $table .= "

                    <script type='text/javascript'>
                        $('select').select2();
                    </script>
                <tr>
                    <td>
                    $j
                    <input type='hidden' class='form-control' id='id_course[]' name='id_course[]' value='$id_course' readonly='readonly'/>
                    <input type='hidden' class='form-control' id='id_exam_registration[]' name='id_exam_registration[]' value='$id_exam_registration' readonly='readonly'/>
                    </td>

                    <td >
                    $course
                    </td>

                    <td style='text-align: center;'>

                        <div class='form-group'>

                            <select name='grade[]' id='grade[]' class='form-control' style='width:300px'>
                                <option value=''>Select</option>";

            $gradeList = $this->student_marks_entry_model->getGradeListByStatus('1');

                        for($j=0;$j<count($gradeList);$j++)
                        {
                            $id = $gradeList[$j]->id;
                            $name = $gradeList[$j]->name;
                            $table.="<option value=".$id.">".$name.
                                    "</option>";
                        }
                        $table.="</select>

                        </div>
                    </td>

                    <td style='text-align: center;'>
                        <div class='form-group'>
                            <input type='number' class='form-control' id='marks[]' name='marks[]' >
                        </div>
                    </td>

                    <td style='text-align: center;'>
                        <div class='form-group'>
                            <select name='result[]' id='result[]' class='form-control' style='width:300px'>
                                <option value=''>Select</option>
                                <option value='Pass'>Pass</option>
                                <option value='Fail'>Fail</option>
                            </select>
                        </div>
                    </td>

                </tr>";
                }
                        
            $table .= "
            </table>
            </div>
            </div>";
                
            }
            else
            {
                $table .= "
            <h4 style='text-align: center;'>Exam Registered Courses Not Available For This Student</h4>";

                // echo "Courses Not Available For This Student";exit();
            }


            echo $table;
            exit;
    }

    function getIntakeByProgramme($id_programme)
    {
            $intake_data = $this->student_marks_entry_model->getIntakeByProgrammeId($id_programme);

            // echo "<Pre>"; print_r($intake_data);exit;

            $table="
            <script type='text/javascript'>
                $('select').select2();
            </script>


            <select name='id_intake' id='id_intake' class='form-control' onchange='getLandscapeListByProgramIdNIntakeId()'>";
            $table.="<option value=''>Select</option>";

            for($i=0;$i<count($intake_data);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $intake_data[$i]->id;
            $name = $intake_data[$i]->name;
            $year = $intake_data[$i]->year;

            $table.="<option value=".$id.">".$year . " - " . $name.
                    "</option>";

            }
            $table.="</select>";

            echo $table;
    }


    function getLandscapeListByProgramIdNIntakeId()
    {
        
        $data = $this->security->xss_clean($this->input->post('data'));
         // echo "<Pre>"; print_r($data);exit();
        $id_intake = $data['id_intake'];
        $id_programme = $data['id_programme'];

        $temp_details = $this->student_marks_entry_model->getLandscapeListByProgramIdNIntakeId($data);

        // echo "<Pre>"; print_r($temp_details);exit();
        
        $table="
            <script type='text/javascript'>
                $('select').select2();
            </script>


            <select name='id_programme_landscape' id='id_programme_landscape' class='form-control' onchange='getCoursesByProgramIdNIntakeId()'>";
            $table.="<option value=''>Select</option>";

            for($i=0;$i<count($temp_details);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $temp_details[$i]->id;
            $name = $temp_details[$i]->name;
            $program_landscape_type = $temp_details[$i]->program_landscape_type;
            // $code = $temp_details[$i]->code;

            $table.="<option value=".$id.">".$program_landscape_type . " - " . $name.
                    "</option>";

            }
            $table.="</select>";

            echo $table;
    }



    function getCoursesByProgramIdNIntakeId()
    {
        
        $data = $this->security->xss_clean($this->input->post('data'));

        $id_intake = $data['id_intake'];
        $id_programme = $data['id_programme'];
        $id_programme_landscape = $data['id_programme_landscape'];
         // echo "<Pre>"; print_r($id_student);exit();

        $temp_details = $this->student_marks_entry_model->getProgramLandscapeCourses($data);

        // echo "<Pre>"; print_r($temp_details);exit();
        
        $table="
            <script type='text/javascript'>
                $('select').select2();
            </script>


            <select name='id_course' id='id_course' class='form-control'>";
            $table.="<option value=''>Select</option>";

            for($i=0;$i<count($temp_details);$i++)
            {

            // Id Landscape Course Id Taken As id_course_registered
            // $id = $results[$i]->id_procurement_category;
            $id = $temp_details[$i]->id;
            $id_course_registered = $temp_details[$i]->id_course_registered;
            $name = $temp_details[$i]->name;
            $code = $temp_details[$i]->code;

            $table.="<option value=".$id_course_registered.">".$code . " - " . $name.
                    "</option>";

            }
            $table.="</select>";

            echo $table;
    }
}
