<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>List Exam Calender</h3>
      <a href="add" class="btn btn-primary">+ Add Exam Calender</a>
    </div>

    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Advanced Search
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">
                <div class="row">

                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Semester</label>
                      <div class="col-sm-8">
                        <select name="id_semester" id="id_semester" class="form-control selitemIcon">
                            <option value="">Select</option>
                              <?php
                            if (!empty($semesterList)) {
                              foreach ($semesterList as $record)
                              {
                                $selected = '';
                                if ($record->id == $searchParam['id_semester']) {
                                  $selected = 'selected';
                                }
                            ?>
                                <option value="<?php echo $record->id;  ?>"
                                  <?php echo $selected;  ?>>
                                  <?php echo  $record->code  . " - " . $record->name;  ?>
                                  </option>
                            <?php
                            }
                            }
                            ?>
                        </select>
                      </div>
                    </div>
                  </div>

                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Activity</label>
                      <div class="col-sm-8">
                        <select name="activity" id="activity" class="form-control">
                          <option value=''>Select</option>
                            <option value='Update Exam Center' <?php if($searchParam['activity'] =='Update Exam Center')
                            { echo "selected=selected";} ?> >Update Exam Center
                            </option>
                        </select>
                      </div>
                    </div>
                  </div>

                </div>
              </div>
              <div class="app-btn-group">
                <button type="submit" class="btn btn-primary">Search</button>
                <button type="reset" class="btn btn-link" onclick="clearSearchForm()">Clear All Fields</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>

    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
             <th>Sl. No</th>
            <th>Semester</th>
            <th>Activity</th>
            <th>Start Date</th>
            <th>End Date</th>
            <th class="text-center">Status</th>
            <th class="text-center">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($examCalenderList))
          {
            $i=1;
            foreach ($examCalenderList as $record) {
          ?>
              <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $record->activity ?></td>
                <td><?php echo $record->semester_code . " - " . $record->semester_name ?></td>
                <td><?php echo date('d-m-Y', strtotime($record->start_date)) ?></td>
                <td><?php echo date('d-m-Y', strtotime($record->end_date)) ?></td>
                <td class="text-center"><?php if( $record->status == '1')
                {
                  echo "Active";
                }
                else
                {
                  echo "In-Active";
                } 
                ?></td>
                <td class="text-center">
                  <a href="<?php echo 'edit/' . $record->id; ?>" title="Edit">Edit</a>
                  
                </td>
              </tr>
          <?php
          $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
<script>
    function clearSearchForm()
    {
      window.location.reload();
    }
    $('select').select2();
</script>