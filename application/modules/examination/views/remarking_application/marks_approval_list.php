<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>Approval Remark Entry List</h3>
      <!-- <a href="add" class="btn btn-primary">+ Add Student Marks</a> -->
    </div>

    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
               Search
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">
                <div class="row">

                  <div class="col-sm-6">
                    
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Program <span class='error-text'>*</span></label>
                      <div class="col-sm-8">
                        <select name="id_programme" id="id_programme" class="form-control selitemIcon"
                         >
                            <option value="">Select</option>
                            <?php
                            if (!empty($programmeList))
                            {
                                foreach ($programmeList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>"
                              <?php
                              if($record->id == $searchParam['id_programme'])
                              {
                                echo 'selected';
                              }
                              ?>
                              >
                                <?php echo $record->code ."-".$record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                    </div>


                    <div class="form-group">
                      <label class="col-sm-4 control-label">Intake <span class='error-text'>*</span></label>
                      <div class="col-sm-8">
                        <select name="id_intake" id="id_intake" class="form-control selitemIcon"
                         >
                            <option value="">Select</option>
                            <?php
                            if (!empty($intakeList))
                            {
                                foreach ($intakeList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>"
                              <?php
                              if($record->id == $searchParam['id_intake'])
                              {
                                echo 'selected';
                              }
                              ?>
                              >
                                <?php echo $record->year ."-".$record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                    </div>


                    <!-- <div class="form-group">
                      <label class="col-sm-4 control-label">Intake <span class='error-text'>*</span></label>
                      <div class="col-sm-8">
                        <span id="view_intake">
                            <select class="form-control" id='id_intake' name='id_intake'>
                              <option value=''></option>
                            </select>
                          </span>
                      </div>
                    </div> -->


                      <div class="form-group">
                        <label class="col-sm-4 control-label">Student Name / NRIC</label>
                        <div class="col-sm-8">
                          <input type="text" class="form-control" name="name" id="name" value="<?php echo $searchParam['name']; ?>">
                        </div>
                      </div>

    
                  </div>
                </div>
                
              </div>
              <div class="app-btn-group">
                <button type="submit" class="btn btn-primary">Search</button>
                <a href="marksEntryApprovalList" class="btn btn-link">Clear All Fields</a>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>

    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl. No</th>
            <th>Student</th>
            <th>Intake</th>
            <th>Programme</th>
            <th>Result</th>
            <th style="text-align: center;">Status</th>
            <th style="text-align: center;">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($marksEntryList))
          {
            $i=1;
            foreach ($marksEntryList as $record)
            {
             ?>
              <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $record->nric . " - " . $record->student_name; ?></td>
                <td><?php echo $record->intake_year . " - " .$record->intake_name; ?></td>
                <td><?php echo $record->programme_code . " - " .$record->programme_name; ?></td>
                <td><?php echo $record->result; ?></td>
                <td style="text-align: center;"><?php if( $record->status == '1')
                {
                  echo "Re-Mark Added";
                }
                elseif( $record->status == '2')
                {
                  echo "Rejected";
                }
                else
                {
                  echo "Pending";
                } 
                ?></td>
                <td class="text-center">
                  <?php if( $record->status == '0')
                  {
                    ?>
                  <a href="<?php echo 'approve/' . $record->id; ?>" title="Approve">Approve</a>

                  <?php
                  }
                  ?>
                </td>
              </tr>
          <?php
          $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
<script type="text/javascript">
  
  $('select').select2();

  function clearSearchForm()
      {
        window.location.reload();
      }
</script>