<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>List Student Apply Remarking Remarks</h3>
      <a href="add" class="btn btn-primary">+ Apply For Student Remarks </a>
    </div>

  <form action="" method="post" id="searchForm">
    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
               Search
            </a>
          </h4>
        </div>
        
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">



                
                <div class="row">

                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Course</label>
                      <div class="col-sm-8">
                        <select name="id_course_registered_landscape" id="id_course_registered_landscape" class="form-control">
                          <option value="">Select</option>
                          <?php
                          if (!empty($courseRegisteredLandscapeList)) {
                            foreach ($courseRegisteredLandscapeList as $record)
                            {
                              $selected = '';
                              if ($record->id == $searchParam['id_course_registered_landscape']) {
                                $selected = 'selected';
                              }
                          ?>
                              <option value="<?php echo $record->id;  ?>"
                                <?php echo $selected;  ?>>
                                <?php echo  $record->code ."-".$record->name;  ?></option>
                          <?php
                            }
                          }
                          ?>
                        </select>
                      </div>
                    </div>

                    <!-- <div class="form-group">
                      <label class="col-sm-4 control-label">Semester</label>
                      <div class="col-sm-8">
                        <select name="id_semester" id="id_semester" class="form-control">
                          <option value="">Select</option>
                          <?php
                          if (!empty($semesterList)) {
                            foreach ($semesterList as $record)
                            {
                              $selected = '';
                              if ($record->id == $searchParam['id_semester']) {
                                $selected = 'selected';
                              }
                          ?>
                              <option value="<?php echo $record->id;  ?>"
                                <?php echo $selected;  ?>>
                                <?php echo  $record->code ."-".$record->name;  ?></option>
                          <?php
                            }
                          }
                          ?>
                        </select>
                      </div>
                    </div> -->
    
                  </div>
                </div>
                
              </div>
              <div class="app-btn-group">
                <button type="submit" name='btn_submit' value='1' class="btn btn-primary">Search</button>
                <a href='list' class="btn btn-link">Clear All Fields</a>
              </div>
            </div>
          </div>
        
      </div>
    </div>

    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl. No</th>
            <th>Student</th>
            <th>Intake</th>
            <th>Programme</th>
            <!-- <th>Semester</th> -->
            <th>Grade</th>
            <th>Result</th>
            <th>Status</th>
            <th style="text-align: center;">Action</th>
            <!-- <th style="text-align: center;"><input type='checkbox' id='checkAll' name='checkAll'> Check All</th> -->
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($studentMarksEntryList))
          {
            $i=1;
            foreach ($studentMarksEntryList as $record)
            {
             ?>
              <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $record->nric . " - " . $record->full_name; ?></td>
                <td><?php echo $record->intake_year . " - " .$record->intake_name; ?></td>
                <td><?php echo $record->programme_code . " - " .$record->programme_name; ?></td>
                <!-- <td><?php echo $record->semester_code . " - " .$record->semester_name; ?></td> -->
                <td><?php echo $record->grade; ?></td>
                <td><?php echo $record->result; ?></td>
                <td><?php if( $record->status == '1')
                {
                  echo "Approved";
                }
                elseif( $record->status == '2')
                {
                  echo "Rejected";
                }
                else
                {
                  echo "Pending";
                } 
                ?></td>
                
                 <!-- <?php

                  if($record->is_remarking >= $record->max_count)
                  {
                    echo 'Max Recount Limit Is '. $record->max_count;
                  }else
                  {
                  ?>
                  <input type='checkbox' id='id_mark_entry[]' name='id_mark_entry[]' class='check' value='<?php echo $record->id_mark_entry; ?>'>
                  <?php
                  }
                ?> -->
                <td class="text-center">
                   <a href="<?php echo 'edit/' . $record->id; ?>" title="Add Marks">Add Marks</a>
                </td>
              </tr>
          <?php
          $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>



    

   <!--  <div class="app-btn-group">
      <button type="submit" name='btn_submit' value='2' class="btn btn-primary">Save</button>
    </div> -->


  </form>

  </div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
<script type="text/javascript">
    $('select').select2();
    
  function clearSearchForm()
      {
        window.location.reload();
      }
</script>