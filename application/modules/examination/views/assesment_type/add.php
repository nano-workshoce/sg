<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Assesment Type</h3>
        </div>
        <form id="form_grade" action="" method="post">

        <div class="form-container">
                <h4 class="form-group-title">Assesment Type Details</h4>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>ID <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="type_id" name="type_id">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Type <span class='error-text'>*</span></label>
                        <select name="type" id="type" class="form-control">
                            <option value="">Select</option>
                            <option value="Standard">Standard</option>
                            <option value="Comprehensive Exam">Comprehensive Exam</option>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                        <div class="form-group">
                            <p>Exam <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="exam" id="exam" value="No" checked="checked"><span class="check-radio"></span> No
                            </label>                  
                            <label class="radio-inline">
                              <input type="radio" name="exam" id="exam" value="Yes"><span class="check-radio"></span> Yes
                            </label>            
                        </div>                         
                </div>

            </div>

            <div class="row">

                

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Effective Intake <span class='error-text'>*</span></label>
                        <select name="id_intake" id="id_intake" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($intakeList))
                            {
                                foreach ($intakeList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->name; ?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>


                <!-- <div class="col-sm-4">
                    <div class="form-group">
                        <label>Effective Semester <span class='error-text'>*</span></label>
                        <select name="id_semester" id="id_semester" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($semesterList))
                            {
                                foreach ($semesterList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->code . " - " . $record->name; ?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div> -->



                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Description <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Description Optional Language </label>
                        <input type="text" class="form-control" id="description_optional_language" name="description_optional_language">
                    </div>
                </div>

            </div>

            <div class="row">


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Order <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="order" name="order">
                    </div>
                </div>

                <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" checked="checked"><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0"><span class="check-radio"></span> Inactive
                            </label>                              
                        </div>                         
                </div>
                
            </div>

        </div>


        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="list" class="btn btn-link">Cancel</a>
            </div>
        </div>

        
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>

    $('select').select2();

    $(document).ready(function() {
        $("#form_grade").validate({
            rules: {
                type_id: {
                    required: true
                },
                 type: {
                    required: true
                },
                exam: {
                    required: true
                },
                 id_semester: {
                    required: true
                },
                description: {
                    required: true
                },
                 order: {
                    required: true
                },
                 id_intake: {
                    required: true
                }                
            },
            messages: {
                type_id: {
                    required: "<p class='error-text'>ID Required</p>",
                },
                type: {
                    required: "<p class='error-text'>Select Type</p>",
                },
                exam: {
                    required: "<p class='error-text'>Select Exam</p>",
                },
                id_semester: {
                    required: "<p class='error-text'>Select Effective Semester</p>",
                },
                description: {
                    required: "<p class='error-text'>Description Required</p>",
                },
                order: {
                    required: "<p class='error-text'>Order Required</p>",
                },
                id_intake: {
                    required: "<p class='error-text'>Select Intake</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
