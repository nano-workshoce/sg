<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit Grade</h3>
        </div>
        <form id="form_grade" action="" method="post">

        <div class="form-container">
                <h4 class="form-group-title">Grade Details</h4>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Updation Type <span class='error-text'>*</span></label>
                        <select name="name" id="name" class="form-control selitemIcon">
                            <option value="">Select</option>
                            <option value="Latest"
                            <?php 
                            if($examConfiguration->name == 'Latest')
                            {
                                echo 'selected';
                            }
                            ?>>Latest
                            </option>

                            <option value="Highest"
                            <?php 
                            if($examConfiguration->name == 'Highest')
                            {
                                echo 'selected';
                            }
                            ?>>Highest
                            </option>
                            
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Max. Remark Limit <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="max_count" name="max_count" value="<?php echo $examConfiguration->max_count; ?>">
                        <input type="hidden" class="form-control" id="id" name="id" value="<?php echo $examConfiguration->id; ?>">
                    </div>
                </div>               
                
            </div>

        </div>


        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <!-- <a href="../list" class="btn btn-link">Cancel</a> -->
            </div>
        </div>

            
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>

    $('select').select2();


    $(document).ready(function() {
        $("#form_grade").validate({
            rules: {
                name: {
                    required: true
                },
                max_count: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>Updation Type Required</p>",
                },
                max_count: {
                    required: "<p class='error-text'>Max. Count Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>