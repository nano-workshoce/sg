<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>List Exam Attendence</h3>
      <a href="add" class="btn btn-primary">+ Add Exam Attendence</a>
    </div>

    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Advanced Search
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">
                

                <div class="row">

                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Location</label>
                      <div class="col-sm-8">
                        <select name="id_location" id="id_location" class="form-control selitemIcon">
                            <option value="">Select</option>
                              <?php
                            if (!empty($examLocationList)) {
                              foreach ($examLocationList as $record)
                              {
                                $selected = '';
                                if ($record->id == $searchParam['id_location']) {
                                  $selected = 'selected';
                                }
                            ?>
                                <option value="<?php echo $record->id;  ?>"
                                  <?php echo $selected;  ?>>
                                  <?php echo  $record->name;  ?>
                                  </option>
                            <?php
                            }
                            }
                            ?>
                        </select>
                      </div>
                    </div>                    
                     <!-- <div class="form-group">
                      <label class="col-sm-4 control-label">Applicant Status</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" name="applicant_status" value="<?php echo $searchParam['applicant_status']; ?>">
                      </div>
                    </div> -->

                  </div>
                </div>


              </div>
              <div class="app-btn-group">
                <button type="submit" class="btn btn-primary">Search</button>
                <button type="reset" class="btn btn-link" onclick="clearSearchForm()">Clear All Fields</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>

    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl. No</th>
            <th>Location</th>
            <!-- <th>Room / Hall</th> -->
            <th>Date & Time</th>
            <!-- <th>Total Styudents</th>
            <th>Present</th>
            <th>Absent</th> -->
            <th class="text-center">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($examAttendenceList))
          {
            $i=1;
            foreach ($examAttendenceList as $record) {
          ?>
              <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $record->location ?></td>
                <!-- <td><?php echo $record->room ?></td> -->
                <td><?php echo date('d-m-Y - g:i A', strtotime($record->created_dt_tm)) ?></td>
               <!--  <td><?php echo $record->total ?></td>
                <td><?php echo $record->present ?></td>
                <td><?php echo $record->absent ?></td>
                <td><?php echo $record->semester_code . " - " . $record->semester_name ?></td> -->
                <td class="text-center">
                   <a href="<?php echo 'edit/' . $record->id; ?>" title="View">View</a>
                  
                </td>
              </tr>
          <?php
          $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
<script type='text/javascript'>
    $('select').select2();
    
    function clearSearchForm()
    {
      window.location.reload();
    }
</script>