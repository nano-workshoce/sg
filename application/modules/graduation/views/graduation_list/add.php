<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Graduation Students</h3>
        </div>
        <form id="form_award" action="" method="post">


        <div class="form-container">
                <h4 class="form-group-title">Graduation Details</h4> 


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Convocation Session <span class='error-text'>*</span></label>
                        <select name="id_convocation" id="id_convocation" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($convocationList))
                            {
                                foreach ($convocationList as $record)
                                {?>
                                    <option value="<?php echo $record->id;?>"
                                    ><?php echo  $record->convocation_session;?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Message For Students</label>
                        <input type="text" class="form-control" id="message" name="message">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Convocation Date</label>
                        <input type="text" class="form-control datepicker" id="date_tm" name="date_tm" autocomplete="off">
                    </div>
                </div>

            </div>

        </div>

        <!-- </form> -->

        <div class="form-container">
            <h4 class="form-group-title">Search Student For Graduation</h4>
            <!-- <h4 >Search Asset For Disposal</h4> -->

        <!-- <form id="form_award" action="" method="post"> -->


            <div class="row">

                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Program <span class='error-text'>*</span></label>
                        <select name="id_program" id="id_program" class="form-control" >
                            <option value="">Select</option>
                            <?php
                            if (!empty($programList))
                            {
                                foreach ($programList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>  


                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Intake <span class='error-text'>*</span></label>
                        <select name="id_intake" id="id_intake" class="form-control" >
                            <option value="">Select</option>
                            <?php
                            if (!empty($intakeList))
                            {
                                foreach ($intakeList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->year . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>  

                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Qualification Level <span class='error-text'>*</span></label>
                        <select name="id_qualification" id="id_qualification" class="form-control" >
                            <option value="">Select</option>
                            <?php
                            if (!empty($qualificationList))
                            {
                                foreach ($qualificationList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>  


                 <div class="col-sm-3">
                    <div class="form-group">
                        <label>Student NRIC</label>
                        <input type="text" class="form-control" id="nric" name="nric">
                    </div>
               </div>
                
            </div>


            <div class="modal-footer">
                <button type="button" class="btn btn-default" onclick="searchStudent()">Search</button>
              </div>



        </div>

         <div class="form-container" style="display: none;" id="view_student_display">
            <h4 class="form-group-title">Students For Graduation</h4>


            <div  id='view_student'>
            </div>

        </div>


        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="list" class="btn btn-link">Cancel</a>
            </div>
        </div>

        </form>
        

        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>

    function searchStudent()
    {
        var tempPR = {};
        tempPR['id_program'] = $("#id_program").val();
        tempPR['id_intake'] = $("#id_intake").val();
        tempPR['id_qualification'] = $("#id_qualification").val();
        tempPR['nric'] = $("#nric").val();
            $.ajax(
            {
               url: '/graduation/graduationList/searchStudent',
               type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view_student_display").show();
                $("#view_student").html(result);
                // $('#myModal').modal('hide');
                // var ta = $("#total_detail").val();
                // alert(ta);
                // $("#amount").val(ta);
               }
            });
    }

    $('select').select2();

    $(document).ready(function() {
        $("#form_award").validate({
            rules: {
                id_convocation: {
                    required: true
                },
                message: {
                    required: true
                },
                date_tm: {
                    required: true
                }
            },
            messages: {
                id_convocation: {
                    required: "<p class='error-text'>Convocation Session Name Required</p>",
                },
                message: {
                    required: " <p class='error-text'>Capacity Required</p>",
                },
                date_tm: {
                    required: " <p class='error-text'>Select Convocation Date</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

    $( function() {
    $( ".datepicker" ).datepicker(
        {
            changeYear: true,
            changeMonth: true,
        });
  } );
</script>