<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Convocation</h3>
        </div>
        <form id="form_convocation" action="" method="post">
            <div class="form-container">
                <h4 class="form-group-title">Convocation Details</h4> 


                <div class="row">
                
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Convocation Session <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="convocation_session" name="convocation_session">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Session Capacity <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="session_capacity" name="session_capacity">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Convocation Year <span class='error-text'>*</span></label>
                            <select name="year" id="year" class="form-control">
                                <option value="">Select</option>
                                <?php
                                for ($i=date('Y'); $i < date('Y') + 5; $i++)
                                { 
                                    ?>
                                    <option value="<?php echo $i;?>"
                                        ><?php echo  $i;?>
                                    </option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                </div>



                <div class="row">


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Number Of Guests <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="number_of_guest" name="number_of_guest">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Website <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="weblink" name="weblink">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Start Date <span class='error-text'>*</span></label>
                            <input type="text" class="form-control datepicker" id="start_date" name="start_date" autocomplete="off">
                        </div>
                    </div>

                
                </div>


                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>End Date <span class='error-text'>*</span></label>
                            <input type="text" class="form-control datepicker" id="end_date" name="end_date" autocomplete="off">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Attendence End Date <span class='error-text'>*</span></label>
                            <input type="text" class="form-control datepicker" id="confirm_attendence_end_date" name="confirm_attendence_end_date" autocomplete="off">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Record Verification End Date <span class='error-text'>*</span></label>
                            <input type="text" class="form-control datepicker" id="record_verification_end_date" name="record_verification_end_date" autocomplete="off">
                        </div>
                    </div>

                
                </div>


                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Guest Application End Date <span class='error-text'>*</span></label>
                            <input type="text" class="form-control datepicker" id="graduation_guest_application_end_date" name="graduation_guest_application_end_date" autocomplete="off">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Student Portal End Date <span class='error-text'>*</span></label>
                            <input type="text" class="form-control datepicker" id="student_portal_end_date" name="student_portal_end_date" autocomplete="off">
                        </div>
                    </div>

                    <div class="col-sm-4">
                            <div class="form-group">
                                <p>Status <span class='error-text'>*</span></p>
                                <label class="radio-inline">
                                <input type="radio" name="status" id="status" value="1" checked="checked"><span class="check-radio"></span> Active
                                </label>
                                <label class="radio-inline">
                                <input type="radio" name="status" id="status" value="0"><span class="check-radio"></span> Inactive
                                </label>                              
                            </div>                         
                    </div>
                
                </div>
            </div>

            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="list" class="btn btn-link">Cancel</a>
                </div>
            </div>
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>
    $(document).ready(function() {
        $("#form_convocation").validate({
            rules: {
                convocation_session: {
                    required: true
                },
                session_capacity: {
                    required: true
                },
                number_of_guest: {
                    required: true
                },
                weblink: {
                    required: true
                },
                start_date: {
                    required: true
                },
                end_date: {
                    required: true
                },
                confirm_attendence_end_date: {
                    required: true
                },
                record_verification_end_date: {
                    required: true
                },
                graduation_guest_application_end_date: {
                    required: true
                },
                student_portal_end_date: {
                    required: true
                },
                year: {
                    required: true
                }
            },
            messages: {
                convocation_session: {
                    required: "<p class='error-text'>Convocation Session Name Required</p>",
                },
                session_capacity: {
                    required: "<p class='error-text'>Capacity Required</p>",
                },
                number_of_guest: {
                    required: "<p class='error-text'>No. Of Guests Required</p>",
                },
                weblink: {
                    required: "<p class='error-text'>Weblink Required</p>",
                },
                start_date: {
                    required: "<p class='error-text'>Start Date Required</p>",
                },
                end_date: {
                    required: "<p class='error-text'>End Date Required</p>",
                },
                confirm_attendence_end_date: {
                    required: "<p class='error-text'>Attendence End Date Required</p>",
                },
                record_verification_end_date: {
                    required: "<p class='error-text'>Record Verification End Date Required</p>",
                },
                graduation_guest_application_end_date: {
                    required: "<p class='error-text'>Guest Application End Date Required</p>",
                },
                student_portal_end_date: {
                    required: "<p class='error-text'>Student Portal End Date Required</p>",
                },
                year: {
                    required: "<p class='error-text'>Select Convocation Year</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

    $('select').select2();

</script>

<!-- <script>
    <?php

    $current_year = date('Y');
    $max_year = $current_year + 3;
    $min_year = $current_year;
    $ran = $min_year . ":" . $max_year;

    ?>
  $( function() {
    // var rang = <?php echo $ran ?>;
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
        yearRange: "2020:20202"
    });
  } );

</script> -->

<script>
  $( function() {
    // var currentTime = new Date()

    // // returns the month (from 0 to 11)
    // var month = currentTime.getMonth() + 1

    // // returns the day of the month (from 1 to 31)
    // var day = currentTime.getDate()

    // // returns the year (four digits)
    // var year = currentTime.getFullYear()


    // var min_year = year;
    // var max_year = year + 2;
    // var range = str.concat(min_year,':',max_year); 


    $( ".datepicker" ).datepicker(
        {
            changeYear: true,
            changeMonth: true,
        });
  } );
</script>