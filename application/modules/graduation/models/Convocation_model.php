<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Convocation_model extends CI_Model
{
    function convocationList()
    {
        $this->db->select('*');
        $this->db->from('convocation');
        $this->db->order_by("id", "DESC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function convocationListSearch($search)
    {
        $this->db->select('*');
        $this->db->from('convocation');
        if (!empty($search))
        {
            $likeCriteria = "(convocation_session  LIKE '%" . $search . "%' or weblink  LIKE '%" . $search . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("id", "DESC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getConvocation($id)
    {
        $this->db->select('*');
        $this->db->from('convocation');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewConvocation($data)
    {
        $this->db->trans_start();
        $this->db->insert('convocation', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editConvocation($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('convocation', $data);
        return TRUE;
    }

    function addConvocationDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('convocation_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function deleteConvocationDetails($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('convocation_details');
        return TRUE;
    }

    function getConvocationDetails($id_convocation)
    {
        $this->db->select('*');
        $this->db->from('convocation_details');
        $this->db->where('id_convocation', $id_convocation);
        $query = $this->db->get();
        return $query->rEsult();
    }
}