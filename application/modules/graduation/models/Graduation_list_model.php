<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Graduation_list_model extends CI_Model
{
    function graduationList()
    {
        $this->db->select('*');
        $this->db->from('graduation_list');
        $this->db->order_by("id", "DESC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function graduationListSearch($data)
    {
        // $date = 
        $this->db->select('g.*, c.award, c.convocation_session');
        $this->db->from('graduation_list as g');
        $this->db->join('convocation as c', 'g.id_convocation = c.id');
        // $this->db->join('programme as p', 's.id_program = p.id');
        // $this->db->join('qualification_setup as qs', 's.id_degree_type = qs.id');
        // if ($data['name'] != '')
        // {
        //     $likeCriteria = "(s.first_name  LIKE '%" . $data['name'] . "%' or s.nric  LIKE '%" . $data['name'] . "%')";
        //     $this->db->where($likeCriteria);

        //     // $likeCriteria = "(first_name  LIKE '%" . $search . "%')";
        // }
        if ($data['id_convocation'] != '')
        {
            $this->db->where('g.id_convocation', $data['id_convocation']);
        }
        // if ($data['id_intake'] != '')
        // {
        //     $this->db->where('s.id_intake', $data['id_intake']);
        // }
        // if($data['id_qualification'] != '')
        // {
        //     $this->db->where('s.id_qualification', $data['id_qualification']);
        // }
        if($data['status'] != '')
        {
            $this->db->where('g.status', $data['status']);
        }
        $this->db->order_by("g.id", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function studentListSearchForGraduation($data)
    {
        // echo "<Pre>";print_r($tempData);exit();


        $this->db->select('s.*, p.name as program_name, p.code as program_code, i.name as intake_name, i.year as intake_year, qs.code as qualification_code, qs.name as qualification_name, branc.name as branch_name, branc.code as branch_code');
        $this->db->from('student as s');
        $this->db->join('intake as i', 's.id_intake = i.id');
        $this->db->join('programme as p', 's.id_program = p.id');
        $this->db->join('organisation_has_training_center as branc', 's.id_branch = branc.id','left');
        $this->db->join('qualification_setup as qs', 's.id_degree_type = qs.id');

        if ($data['nric'] != '')
        {
            $likeCriteria = "(s.first_name  LIKE '%" . $data['nric'] . "%' or s.nric  LIKE '%" . $data['nric'] . "%')";
            $this->db->where($likeCriteria);

            // $likeCriteria = "(first_name  LIKE '%" . $search . "%')";
        }
        if ($data['id_program'] != '')
        {
            $this->db->where('s.id_program', $data['id_program']);
        }
        if ($data['id_intake'] != '')
        {
            $this->db->where('s.id_intake', $data['id_intake']);
        }
        if($data['id_qualification'] != '')
        {
            $this->db->where('s.id_degree_type', $data['id_qualification']);
        }
       
        $this->db->where('s.applicant_status', 'Approved');
        $this->db->order_by("s.id", "ASC");
        $query = $this->db->get();
        $results = $query->result();

        $details = array();
        foreach ($results as $result)
        {
           $cleared_credit_hours = $this->getCourseRegistrationByStudentId($result);
           $total_credit_hours = $this->getCourseLandscape($result);

           $invoice_data = $this->getInvoiceBalanceByStudentData($result);

           // echo "<Pre>";print_r($invoice_data);exit();
           $result->course_credit_hours = $total_credit_hours;
           $result->cleared_credit_hours = $cleared_credit_hours;
           $result->is_invoice_pending = $invoice_data;


           array_push($details, $result);
        }

         return $details;
    }

    function getInvoiceBalanceByStudentData($data)
    {
        $student = "Student";
        $applicant = "Applicant";

        $this->db->select('mi.*');
        $this->db->from('main_invoice as mi');
        $likeCriteria = "mi.balance_amount >" . 0.00 . " and (id_student  = '" . $data->id . "' and type  ='" . $student . "') or (id_student  ='" . $data->id_applicant . "' and type  ='" . $applicant . "')";
        $this->db->where($likeCriteria);
        $query = $this->db->get();
        $result = $query->row();

        // if($data->id == 13)
        // {
        //     echo "<Pre>";print_r($result);exit();
        // }


        if($result)
        {
            return 'Pending';
        }else
        {
            return 'Cleared';
        }

    }

    function getCourseRegistrationByStudentId($data)
    {
        $this->db->select('DISTINCT(id_course_registered_landscape) as id_course_registered_landscape');
        $this->db->from('course_registration');
        $this->db->where('id_student', $data->id);
        $this->db->where('id_programme', $data->id_program);
        $this->db->where('id_intake', $data->id_intake);
        $query = $this->db->get();
        $results = $query->result();  
        
        // $details = array();
        $course_credit_hours = 0;
        $cleared_credit_hours = 0;
        
        foreach ($results as $result)
        {
            $id_course_registered_landscape = $result->id_course_registered_landscape;
            

            $course = $this->getCourseByRegistredLandscape($data,$id_course_registered_landscape);
            if($course)
            {
                $cleared_credit_hours = $cleared_credit_hours + $course->credit_hours;
            }


            // $course_completed = $this->getCourseCreditHoursCompletedByStudent($data,$id_course_registered_landscape);
            // if($course_completed)
            // {
            //     $cleared_credit_hours = $cleared_credit_hours + $course_completed->credit_hours;
            // }
        }

        // $course_credit_data['cleared_credit_hours'] = $cleared_credit_hours;
        // $course_credit_data['course_credit_hours'] = 0;
        
        return $cleared_credit_hours;
    }

    function getCourseLandscape($data)
    {
        $this->db->select('c.*');
        $this->db->from('course as c');
        $this->db->join('add_course_to_program_landscape as acpl', 'c.id = acpl.id_course');
        $this->db->where('acpl.id_program', $data->id_program);
        $this->db->where('acpl.id_intake', $data->id_intake);
        $this->db->where('acpl.id_program_scheme', $data->id_program_scheme);
        $query = $this->db->get();
        $results = $query->result();

        $total_credit_hours = 0;
        foreach ($results as $result)
        {

            $total_credit_hours = $total_credit_hours + $result->credit_hours;
        }

        return $total_credit_hours;
    }

    function getCourseByRegistredLandscape($data,$id_course_registered_landscape)
    {
        $this->db->select('c.*');
        $this->db->from('course as c');
        $this->db->join('add_course_to_program_landscape as acpl', 'c.id = acpl.id_course');
        $this->db->where('acpl.id_program', $data->id_program);
        $this->db->where('acpl.id_intake', $data->id_intake);
        $this->db->where('acpl.id_program_scheme', $data->id_program_scheme);
        $this->db->where('acpl.id', $id_course_registered_landscape);
        $query = $this->db->get();
        $result = $query->row();
        return $result;
    }

    function getCourseCreditHoursCompletedByStudent($data,$id_course_registered_landscape)
    {
        $this->db->select('c.*');
        $this->db->from('course as c');
        $this->db->join('course_registration as cr', 'c.id = cr.id_course');
        $this->db->join('add_course_to_program_landscape as acpl', 'cr.id_course_registered_landscape = acpl.id');
        $this->db->where('acpl.id', $id_course_registered_landscape);
        $this->db->where('cr.id_student', $data->id);
        $this->db->where('cr.id_programme', $data->id_program);
        $this->db->where('cr.id_intake', $data->id_intake);
        $query = $this->db->get();
        $result = $query->row();
        return $result;
    }


    function getOrganisation()
    {
        $this->db->select('*');
        $this->db->from('organisation');
        $this->db->where('status', 1);
        $this->db->order_by("id", "DESC");
         $query = $this->db->get();
         $result = $query->row();  
         return $result;
    }


    function convocationListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('convocation');
        $this->db->where('status', $status);
        $this->db->order_by("id", "DESC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function programList()
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result();      
        return $result;
    }

    function programListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result();      
        return $result;
    }

    function intakeList()
    {

        $this->db->select('*');
        $this->db->from('intake');
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result();      
        return $result;
    }

    function intakeListByStatus($status)
    {

        $this->db->select('*');
        $this->db->from('intake');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result();      
        return $result;
    }

    function qualificationList()
    {

        $this->db->select('*');
        $this->db->from('qualification_setup');
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result();      
        return $result;
    }

    function qualificationListByStatus($status)
    {

        $this->db->select('*');
        $this->db->from('qualification_setup');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result();      
        return $result;
    }

    function addNewGraduation($data)
    {
        $this->db->trans_start();
        $this->db->insert('graduation_list', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function addNewGraduationDetail($data)
    {
        $this->db->trans_start();
        $this->db->insert('graduation_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getGraduationList($id)
    {
        $this->db->select('gl.*, c.convocation_session');
        $this->db->from('graduation_list as gl');
        $this->db->join('convocation as c', 'gl.id_convocation = c.id');
        $this->db->where('gl.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getGraduationDetailsByMasterId($id)
    {
        $this->db->select('s.*, p.name as program_name, p.code as program_code, i.name as intake_name, i.year as intake_year, qs.code as qualification_code, qs.name as qualification_name, branc.name as branch_name, branc.code as branch_code, gd.id as id_graduation_detail, gd.status');
        $this->db->from('graduation_details as gd');
        $this->db->join('student as s', 'gd.id_student = s.id');
        $this->db->join('intake as i', 's.id_intake = i.id');
        $this->db->join('programme as p', 's.id_program = p.id');
        $this->db->join('organisation_has_training_center as branc', 's.id_branch = branc.id','left');
        $this->db->join('qualification_setup as qs', 's.id_degree_type = qs.id');
        $this->db->where('gd.id_graduation', $id);
        $query = $this->db->get();
        $results = $query->result();

        $details = array();
        foreach ($results as $result)
        {
            // $student_data = $result;
            // $student_data->id = $student_data->id_student;

           $cleared_credit_hours = $this->getCourseRegistrationByStudentId($result);
           $total_credit_hours = $this->getCourseLandscape($result);

           $invoice_data = $this->getInvoiceBalanceByStudentData($result);

           // echo "<Pre>";print_r($invoice_data);exit();
           $result->course_credit_hours = $total_credit_hours;
           $result->cleared_credit_hours = $cleared_credit_hours;
           $result->is_invoice_pending = $invoice_data;


           array_push($details, $result);
        }

         return $details;
    }

    function updateGraduationDetails($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('graduation_details', $data);
        return TRUE;
    }
    

    function updateStudent($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('student', $data);
        return TRUE;
    }

    function updateGraduation($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('graduation_list', $data);
        return TRUE;
    }

    function updateGraduationDetailsByMasterId($data,$id)
    {
        $this->db->where('id_graduation', $id);
        $this->db->update('graduation_details', $data);
        return TRUE;
    }

    function getGraduationDetails($id)
    {
        $this->db->select('gd.*');
        $this->db->from('graduation_details as gd');
        $this->db->where('gd.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function deleteGraduationTaging($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('graduation_details');
        return TRUE;
    }

    function getPendingGraduationDetailsByMasterId($id_graduation)
    {
        $this->db->select('gl.*');
        $this->db->from('graduation_details as gl');
        $this->db->where('gl.id_graduation', $id_graduation);
        $this->db->where('gl.status =', 0);
        $query = $this->db->get();
        return $query->row();
    }
}

