<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Award extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('award_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('award.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $data['programmeList'] = $this->award_model->programmeList();

            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_programme'] = $this->security->xss_clean($this->input->post('id_programme'));

            $data['searchParameters'] = $formData;
            $data['awardList'] = $this->award_model->awardListSearch($formData);
            $this->global['pageTitle'] = 'Campus Management System : Award List';
            $this->loadViews("award/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('award.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $id_user = $this->session->userId;
                $id_session = $this->session->my_session_id;

                $name = $this->security->xss_clean($this->input->post('name'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $id_programme = $this->security->xss_clean($this->input->post('id_programme'));
                $id_intake = $this->security->xss_clean($this->input->post('id_intake'));
                $type = $this->security->xss_clean($this->input->post('type'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $from_cgpa = $this->security->xss_clean($this->input->post('from_cgpa'));
                $to_cgpa = $this->security->xss_clean($this->input->post('to_cgpa'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'description' => $description,
                    'code' => $code,
                    'id_intake' => $id_intake,
                    'id_programme' => $id_programme,
                    'type' => $type,
                    'from_cgpa' => $from_cgpa,
                    'to_cgpa' => $to_cgpa,
                    'status' => $status,
                    'created_by' => $id_user
                );

                $result = $this->award_model->addNewAward($data);
                redirect('/graduation/award/list');
            }
            $data['programmeList'] = $this->award_model->programmeList();
            $data['intakeList'] = $this->award_model->intakeListByStatus('1');


            $this->global['pageTitle'] = 'Campus Management System : Add Award';
            $this->loadViews("award/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('award.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/graduation/award/list');
            }
            if($this->input->post())
            {
                $id_user = $this->session->userId;
                $id_session = $this->session->my_session_id;

                $name = $this->security->xss_clean($this->input->post('name'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $id_intake = $this->security->xss_clean($this->input->post('id_intake'));
                $id_programme = $this->security->xss_clean($this->input->post('id_programme'));
                $type = $this->security->xss_clean($this->input->post('type'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $from_cgpa = $this->security->xss_clean($this->input->post('from_cgpa'));
                $to_cgpa = $this->security->xss_clean($this->input->post('to_cgpa'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'description' => $description,
                    'code' => $code,
                    'id_intake' => $id_intake,                    
                    'id_programme' => $id_programme,
                    'type' => $type,
                    'from_cgpa' => $from_cgpa,
                    'to_cgpa' => $to_cgpa,
                    'status' => $status,
                    'updated_by' => $id_user
                );

                $result = $this->award_model->editAward($data,$id);
                redirect('/graduation/award/list');
            }
            $data['programmeList'] = $this->award_model->programmeList();
            $data['intakeList'] = $this->award_model->intakeListByStatus('1');

            $data['awardDetails'] = $this->award_model->getAward($id);
            $this->global['pageTitle'] = 'Campus Management System : Edit Award';
            $this->loadViews("award/edit", $this->global, $data, NULL);
        }
    }
}
