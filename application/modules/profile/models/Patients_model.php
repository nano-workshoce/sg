<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Patients_model extends CI_Model
{
    
    function patientList()
    {
        $this->db->select('sp.*');
        $this->db->from('patient as sp');
        $this->db->order_by("sp.id", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         //echo "<Pre>"; print_r($result);exit;
         return $result;
    }

    function salutationListByStatus($status)
    {
        $this->db->select('s.*');
        $this->db->from('salutation_setup as s');
        $this->db->where('s.status', $status);
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function saveTempBookingPatientsDetail($data)
    {
        $this->db->trans_start();
        $this->db->insert('temp_test_booking_patients', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getTempBookingPatientDetailsBySessionId($id_session)
    {
        $this->db->select('*');
        $this->db->from('temp_test_booking_patients');
        $this->db->where('id_session', $id_session);
        $query = $this->db->get();
        return $query->result();
    }

    function deleteTempBookingPatientDetailsBySessionId($id_session)
    {
       $this->db->where('id_session', $id_session);
       $this->db->delete('temp_test_booking_patients');
       return TRUE;
    }

    function deleteTempBookingPatientDetailsById($id)
    {
       $this->db->where('id', $id);
       $this->db->delete('temp_test_booking_patients');
       return TRUE;
    }

    function addNewPatient($data)
    {
        $this->db->trans_start();
        $this->db->insert('patient', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }
} 