<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Welcome extends BaseController
{
    public function __construct()
    {
        // echo 'Logged In';exit();
        parent::__construct();
        $this->load->model('dashboard_model');
        $this->isAdminProfileLoggedIn =  $this->session->userdata['isAdminProfileLoggedIn'];
        $this->index();
        error_reporting(0);
    }

    function index()
    {
    	$id_session = session_id();

        $isAdminProfileLoggedIn = $this->isAdminProfileLoggedIn;

        if(!$this->isAdminProfileLoggedIn)
        {
            redirect('/adminLogin');
        }
        $this->load->view('includes/welcome');
    }
}
?>