<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Patients extends BaseController
{
    // echo 'Logged In';exit();

    public function __construct()
    {
        // echo 'Logged In';exit();
        // $this->index();
        parent::__construct();
        $this->load->model('patients_model');
        $this->isAdminProfileLoggedIn =  $this->session->userdata['isAdminProfileLoggedIn'];
        error_reporting(0);
        $this->index();
    }

    function index()
    {
        $id_session = session_id();

        $isAdminProfileLoggedIn = $this->isAdminProfileLoggedIn;

        if(!$this->isAdminProfileLoggedIn)
        {
            redirect('/adminLogin');
        }

        if($this->input->post())
        {
        
            // echo "<Pre>";print_r($this->input->post());exit;

            $salutation = $this->security->xss_clean($this->input->post('salutation'));
            $first_name = $this->security->xss_clean($this->input->post('first_name'));
            $last_name = $this->security->xss_clean($this->input->post('last_name'));
            $gender = $this->security->xss_clean($this->input->post('gender'));
            $date_of_birth = $this->security->xss_clean($this->input->post('date_of_birth'));
            $phone = $this->security->xss_clean($this->input->post('phone'));
            $alternative_phone = $this->security->xss_clean($this->input->post('alternative_phone'));
            $email = $this->security->xss_clean($this->input->post('email'));
            $mail_address1 = $this->security->xss_clean($this->input->post('mail_address1'));
            $mail_address2 = $this->security->xss_clean($this->input->post('mail_address2'));
            $mailing_city = $this->security->xss_clean($this->input->post('mailing_city'));

            $martial_status = $this->security->xss_clean($this->input->post('martial_status'));
            $mailing_country = $this->security->xss_clean($this->input->post('mailing_country'));
            $mailing_state = $this->security->xss_clean($this->input->post('mailing_state'));
            $mailing_zipcode = $this->security->xss_clean($this->input->post('mailing_zipcode'));
            $present_address_same_as_mailing_address = $this->security->xss_clean($this->input->post('present_address_same_as_mailing_address'));
            $permanent_address1 = $this->security->xss_clean($this->input->post('permanent_address1'));
            $permanent_address2 = $this->security->xss_clean($this->input->post('permanent_address2'));
            $permanent_country = $this->security->xss_clean($this->input->post('permanent_country'));
            $permanent_state = $this->security->xss_clean($this->input->post('permanent_state'));
            $permanent_city = $this->security->xss_clean($this->input->post('permanent_city'));
            $permanent_zipcode = $this->security->xss_clean($this->input->post('permanent_zipcode'));

            $duplicate_check['email'] = $email;
            $duplicate_check['phone'] = $phone;

            $patient = $this->patients_model->getPatientByEmailRPhone($duplicate_check);
            if($patient)
            {
                echo "<script>alert('Patient Already Exist.');</script>";
            }
            else
            {
                $generated_number = $this->patients_model->generatePatientCode();
                $salutationInfo = $this->patients_model->getSalutation($salutation);

                $data = array(
                    'salutation' => $salutation,
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'full_name' => $salutationInfo->name . ". " . $first_name." ".$last_name,
                    'phone' => $phone,
                    'alternative_phone' => $alternative_phone,
                    'email' => $email,
                    'password' => md5($phone),
                    'register_no' => $generated_number,
                    'gender' => $gender,
                    'date_of_birth' => date("Y-m-d", strtotime($date_of_birth)),
                    'martial_status' => $martial_status,
                    'present_address_same_as_mailing_address' => $present_address_same_as_mailing_address,
                    'mail_address1' => $mail_address1,
                    'mail_address2' => $mail_address2,
                    'mailing_country' => $mailing_country,
                    'mailing_state' => $mailing_state,
                    'mailing_city' => $mailing_city,
                    'mailing_zipcode' => $mailing_zipcode,
                    'permanent_address1' => $permanent_address1,
                    'permanent_address2' => $permanent_address2,
                    'permanent_country' => $permanent_country,
                    'permanent_state' => $permanent_state,
                    'permanent_city' => $permanent_city,
                    'permanent_zipcode' => $permanent_zipcode,
                    'status' => 1,
                    'created_by' => 0
                );

                // echo "<Pre>"; print_r($data);exit;
                $id_patient = $this->patients_model->addNewPatient($data);

                echo "<script>alert('Patient Added Succesfully.');</script>";


            }
            redirect('/profile/dashboard/index');
        }    
        $data['patientList'] = $this->patients_model->patientList();
        // echo "<Pre>";print_r($data['pendingTestBookingsList']);exit;
    
        $this->global['pageTitle'] = 'Sangameshwara laboratory : Test Booking Approval List';
        $this->loadViews("patients/list", $this->global, $data, NULL);
    }

    function course()
    {

        // $data['courseList'] = $this->patients_model->getCoursesByStudent($this->studentId);
        $data['courseList'] = array();
        $this->loadViews("dashboard/course", $this->global, $data, NULL);
    }

    function soa()
    {
        // $data['invoiceList'] = $this->patients_model->getInvoices($this->studentId);       
        // $data['receiptList'] = $this->patients_model->getReceipt($this->studentId);
        $data['courseList'] = array();

        $this->global['pageTitle'] = 'Speed Management System : Exam Has Question List';
        $this->loadViews("dashboard/soa", $this->global, $data, NULL);
    }

    function checkout()
    {
        // $id_session = session_id();
        // $data['listOfCourses'] = $this->patients_model->getCourses($id_session);
        $data['courseList'] = array();
        $this->loadViews('dashboard/checkout',$this->global,$data,NULL);
    }

    public function payment()
    {
        $this->global = '';
        $data = '';
        $this->loadViews('dashboard/payment',$this->global,$data,NULL);
    }

    public function success()
    {
        $id_session = session_id();
        $listOfCourses = $this->patients_model->getCourses($id_session);
        for($k=0;$k<count($listOfCourses);$k++)
        {
            $getCateogryId = $this->patients_model->getCoursesById($listOfCourses[$k]->id_course);
            $data['id_course'] = $listOfCourses[$k]->id_course;
            $data['id_student'] = $this->studentId;
            $data['id_category'] = $getCateogryId->id_category;
            $idinvoiceNumber = $this->generateInvoice($data);

            $receipt['invoice_id'] = $idinvoiceNumber;
            $this->generateReceipt($receipt);

            $this->patients_model->deletefromtempsuccess($id_session);
        }

        $this->global = '';
        $data = '';
        $this->loadViews('dashboard/success',$this->global,$data,NULL);
    }

    function deletetemp($id)
    {
         $student_list_data = $this->patients_model->deletefromtemp($id);
        if($student_list_data)
        {
           $return =  "1";            
        }
        else
        {
            $return =  "0";
        }
        
        echo $return;exit;
    }


    function getStateByCountry($id_country)
    {
        $results = $this->patients_model->getStateByCountryId($id_country);

        // echo "<Pre>"; print_r($programme_data);exit;
        $table="
            <script type='text/javascript'>
                 $('select').select2();
            </script>";

        $table.="
        <select name='state' id='state' class='custom-select d-block w-100' required=''>
            <option value=''>Choose...</option>
            ";

        for($i=0;$i<count($results);$i++)
        {
            // $id = $results[$i]->id_procurement_category;
            $id = $results[$i]->id;
            $name = $results[$i]->name;
            $table.="<option value=".$id.">".$name.
                    "</option>";
        }

        $table.="

        </select>";

        echo $table;
        exit;
    }

    function invoice()
    {
        $data = $this->security->xss_clean($this->input->post('tempData'));
        $this->generateInvoice($data);
    }

    function generateInvoice($data)
    {
        // echo "<Pre>";print_r($data);exit;
        if($data == '')
        {
            echo 'No Parameters Passed To Generate the Invoice';
        }
        else
        {
            $id_category = $data['id_category'];
            $id_course = $data['id_course'];
            $id_student = $data['id_student'];

            $fee_structure = $this->patients_model->getFeestructureByData($data);

            $course = $this->patients_model->getCourseByData($data);

            if(empty($course))
            {
                echo 'No Module Found';exit;
            }

            if($fee_structure)
            {
                $fee_structure_details = $this->patients_model->getFeestructureDetailsByIdFeeStructureMain($fee_structure->id);

                if($fee_structure_details)
                {
                    $invoice_number = $this->patients_model->generateMainInvoiceNumber();

                    $invoice_data = array(
                        'id_category' => $id_category,
                        'id_course' => $id_course,
                        'id_student' => $id_student,
                        'type' => 'Student',
                        'currency' => 1,
                        'status' => 1,
                        'invoice_number' => $invoice_number
                    );

                    $id_invoice = $this->patients_model->addInvoice($invoice_data);

                    if($id_invoice)
                    {
                        $total_amount = 0;

                        foreach ($fee_structure_details as $detail)
                        {
                            $detail_data = array(
                                'id_main_invoice' => $id_invoice,
                                'id_fee_item' => $detail->id_fee_item,
                                'amount' => $detail->amount,
                                'price' => $detail->amount,
                                'quantity' => 1,
                            );
                        
                            $id_invoice_details = $this->patients_model->addInvoiceDetails($detail_data);
                            $total_amount = $total_amount + $detail->amount;
                        }

                        $months = "+". $course->months ." months";
                        $expiry_date = date("Y-m-d", strtotime($months));

                        // $expiry_date = date(("Y-m-d") . $months);


                        $data['id_invoice'] = $id_invoice;
                        $data['status'] = 1;
                        $data['amount'] = $total_amount;
                        $data['expiry_date'] = $expiry_date;

                        // echo "<Pre>"; print_r($id_invoice);exit();

                        $added_student_has_course = $this->patients_model->addStudentHasCourse($data);

                        $update_invoice['total_amount'] = $total_amount;
                        $update_invoice['invoice_total'] = $total_amount;
                        $update_invoice['balance_amount'] = $total_amount;

                        $id_updated_invoice = $this->patients_model->editInvoice($update_invoice,$id_invoice);
                    
                        // echo "<Pre>"; print_r($id_updated_invoice);exit();

                        return $id_invoice ;

                    }

                }
                else
                {
                    echo 'No Fee Structure Is Defined For Entered Data';exit;
                }


            }
            else
            {
                echo 'No Fee Structure Is Defined For Entered Data';exit;
            }


        }
    }


    function generateReceipt($data)
    {
        // echo "<Pre>";print_r($data);exit;
        if($data == '')
        {
            echo 'No Parameters Passed To Generate the Receipt';
        }
        else
        {
            $id_invoice = $data['invoice_id'];

            $main_invoice = $this->patients_model->getInvoice($id_invoice);

            if($main_invoice)
            {
               

                    // $fee_structure_details = $this->patients_model->getFeestructureDetailsByIdFeeStructureMain($fee_structure->id);

                    $receipt_number = $this->patients_model->generateReceiptNumber();

                    $invoice_data = array(
                        'id_category' => $main_invoice->id_category,
                        'id_course' => $main_invoice->id_course,
                        'id_student' => $main_invoice->id_student,
                        'type' => $main_invoice->type,
                        'currency' => $main_invoice->currency,
                        'status' => 1,
                        'receipt_number' => $receipt_number,
                        'receipt_amount' => $main_invoice->invoice_total
                    );

                    $id_receipt = $this->patients_model->addReceipt($invoice_data);

                    if($id_receipt)
                    {
                        $total_amount = 0;

                        $detail_data = array(
                            'id_receipt' => $id_receipt,
                            'id_main_invoice' => $id_invoice,
                            'invoice_amount' => $main_invoice->invoice_total,
                            'paid_amount' => $main_invoice->invoice_total,
                            'status' => 1,
                        );
                    
                        $id_invoice_details = $this->patients_model->addReceiptDetails($detail_data);
                        


                        $update_invoice['balance_amount'] = 0;
                        $update_invoice['paid_amount'] = $main_invoice->invoice_total;

                        $id_updated_invoice = $this->patients_model->editInvoice($update_invoice,$id_invoice);

                        echo "Receipt Generated : " . $receipt_number;
                    }
                

            }
            else
            {
                echo 'No Main Invoice Found For The Entered Invoice';
            }

            return 1;


        }

    }

    function getAgeCalculation($dob)
    {
        $date1 = date('Y-m-d', strtotime($dob));
        $date2 = date("Y-m-d");

        $diff = $this->getDatesDifference($date1,$date2);
        if($diff)
        {
            echo $diff;exit;
        }
        else
        {
            echo $date1;
        }

        // $diff=date_diff($dob,$date);

        // $date1=date_create(date('Y-m-d', strtotime($dob)));
        // $date2=date_create($date = date("Y-m-d"));
        // $diff=date_diff($date1,$date2);
        // $diff_days = $diff->format("%R%a days");
        // echo $diff_days;exit;
    }
}