
  
<div class="py-5 bg-light-v2">
  <div class="container">
   <div class="row align-items-center">
     <div class="col-md-6">
       <h2>Registered Course Details</h2>
     </div>
     <div class="col-md-6">
      <ol class="breadcrumb justify-content-md-end bg-transparent">  
        <li class="breadcrumb-item">
          <a href="#">Home</a>
        </li> 
        <li class="breadcrumb-item">
          <a href="../dashboard/course"> Course Details</a>
        </li>
      </ol>
     </div>
   </div>
  </div> 
</div>
<section class="padding-y-10 border-bottom border-light">
  <div class="container">
       <div class="row">
                <div class="col-lg-10 mx-auto">
                  <div class="table-responsive my-4">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th scope="col" class="font-weight-semiBold">Sl No</th>
                          <th scope="col">Category Name</th>
                          <th scope="col">Course Name</th>
                          <th scope="col">Invoice Number</th>
                          <th scope="col">Invoice Amount (RM)</th>
                          <th scope="col">Registered Date</th>

                          <th scope="col">Expriy Date</th>
                          <th scope="col">Access LMS</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php for($i=0;$i<count($courseList);$i++) {?>
                        <tr>
                                                    <td><?php echo $i+1;?></td>

                          <td><?php echo $courseList[$i]->categoryname;?></td>
                          <td><?php echo $courseList[$i]->coursename;?></td>
                          <td><?php echo $courseList[$i]->invoice_number;?></td>
                          <td><?php echo $courseList[$i]->total_amount;?></td>
                          <td><?php echo date('d-m-Y',strtotime($courseList[$i]->date_time));?></td>
                          <td><?php echo date('d-m-Y',strtotime($courseList[$i]->expiry_date));?></td>

                          <td><a href="#" style="color:#2f3984;">Click Here</a></td>


                        </tr>
                      <?php } ?>
                        
                      </tbody>
                    </table>
                  </div>        
                </div>      
              </div> <!-- END row-->
    </div> <!-- END row-->
  </div> <!-- END container-->
</section> <!-- END section-->
    