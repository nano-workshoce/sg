  
<div class="py-5 bg-light-v2">
  <div class="container">
   <div class="row align-items-center">
     <div class="col-md-6">
       <h2>Add Patient Details</h2>
     </div>
     <div class="col-md-6">
      <ol class="breadcrumb justify-content-md-end bg-transparent">  
        <li class="breadcrumb-item">
          <a href="#">Home</a>
        </li> 
        <li class="breadcrumb-item">
          <a href="../dashboard/index"> Add Patient</a>
        </li>
      </ol>
     </div>
   </div>
  </div> 
</div>
<form id="form_main" method="POST">

<section class="padding-y-10">
  <div class="container">

   <div class="row">
        <div class="col-md-12 order-md-1">


            <div class="row">

              <div class="col-md-6 mb-3">
                <label for="salutation">Salutation <span class="text-muted">*</span></label>
                <select class="custom-select d-block w-100" id="salutation" name="salutation" required="">
                  <option value="">Choose...</option>
                  <?php
                  if (!empty($salutationList))
                  {
                      foreach ($salutationList as $record)
                      {?>
                   <option value="<?php echo $record->id;  ?>"
                    <?php
                    if($student->salutation == $record->id)
                    {
                      echo "selected";
                    }
                    ?>
                    >
                      <?php echo $record->name;?>
                   </option>
                  <?php
                      }
                  }
                  ?>
                </select>
                <div class="invalid-feedback">
                  Select a valid Salutation.
                </div>
              </div>



              <div class="col-md-6 mb-3">
                <label for="first_name">First name <span class="text-muted">*</span></label>
                <input type="text" class="form-control" id="first_name" name='first_name' placeholder="" required="required">
                <div class="invalid-feedback">
                  Valid first name is required.
                </div>
              </div>


            </div>

            <div class="row">


              <div class="col-md-6 mb-3">
                <label for="last_name">Last name <span class="text-muted">*</span></label>
                <input type="text" class="form-control" id="last_name" name="last_name" placeholder="" required="">
                <div class="invalid-feedback">
                  Valid last name is required.
                </div>
              </div>

            


              <div class="col-md-6 mb-3">
                <label for="gender">Gender <span class="text-muted">*</span></label>
                <select class="custom-select d-block w-100" id="gender" name="gender" required="">
                  <option value="">Choose...</option>
                  <option value="Male"
                  <?php
                  if($student->gender == 'Male')
                  {
                    echo "selected";
                  }
                  ?>
                  >Male</option>
                  <option value="Female"
                  <?php
                  if($student->gender == 'Female')
                  {
                    echo "selected";
                  }
                  ?>
                  >Female</option>
                  <option value="Others"
                  <?php
                  if($student->gender == 'Others')
                  {
                    echo "selected";
                  }
                  ?>
                  >Others</option>
                </select>
                <div class="invalid-feedback">
                  Please select a valid Gender.
                </div>
              </div>


            </div>

            <div class="row">

              <div class="col-md-6 mb-3">
                <label for="last_name">Date Of Birth <span class="text-muted">*</span></label>
                <input type="date" class="form-control" id="date_of_birth" name="date_of_birth" placeholder="" required="" onblur="getAge()" >
                <div class="invalid-feedback">
                  Valid Date Of Birth Required.
                </div>
              </div>


              <div class="col-md-6 mb-3">
                <label for="last_name">Age </label>
                <input type="text" class="form-control" id="age" name="age" readonly>
                <div class="invalid-feedback">
                  Valid name is required.
                </div>
              </div>


            </div>

            <div class="row">

              <div class="col-md-6 mb-3">
                <label for="last_name">Phone <span class="text-muted">*</span></label>
                <input type="number" class="form-control" id="phone" name="phone" placeholder="" required="">
                <div class="invalid-feedback">
                  Valid Phone is required.
                </div>
              </div>

              <div class="col-md-6 mb-3">
                <label for="last_name">Alternative Mobile </label>
                <input type="number" class="form-control" id="alternative_phone" name="alternative_phone" placeholder="" required="">
                <div class="invalid-feedback">
                  Valid Phone is required.
                </div>
              </div>


            </div>

            <div class="row">

              <div class="col-md-6 mb-3">
                <label for="last_name">Email </label>
                <input type="email" class="form-control" id="age" name="age" placeholder="" required="">
                <div class="invalid-feedback">
                  Valid last Email is required.
                </div>
              </div>

            </div>



            <div class="mb-3">
              
            </div>

            <div class="mb-3">
              <label for="address">Address <span class="text-muted">*</span></label>
              <input type="text" class="form-control" id="address" name="address" placeholder="1234 Main St" required="" value="<?php echo $student->mail_address1 ?>">
              <div class="invalid-feedback">
                Please enter your shipping address.
              </div>
            </div>

            <div class="mb-3">
              <label for="mail_address2">Address 2 / Landmark</label>
              <input type="text" class="form-control" id="mail_address2" name="mail_address2" placeholder="Apartment or suite" value="<?php echo $student->mail_address2 ?>">
            </div>

            <div class="row">


              <div class="col-md-6 mb-3">
                <label for="gender">City <span class="text-muted">*</span></label>
                <select class="custom-select d-block w-100" id="mailing_city" name="mailing_city" required="">
                  <option value="Bengaluru">Bengaluru</option>
                  <option value="Dhoddabalapura">Dhoddabalapura</option>
                  <option value="Tumakuru">Tumakuru</option>
                </select>
                <div class="invalid-feedback">
                  Please select a valid City.
                </div>
              </div>

              <div class="col-md-6 mb-3">
                <label for="zip">Pincode <span class="text-muted">*</span></label>
                <input type="number" class="form-control" id="mailing_zipcode"  name="mailing_zipcode" placeholder="" required="" value="<?php echo $student->mailing_zipcode ?>">
                <div class="invalid-feedback">
                  City required.
                </div>
              </div>

            </div>
            
            <hr class="mb-4">
            <button class="btn btn-primary btn-lg btn-block" type="submit">Add Patient</button>
        </div>
      </div>
  </div> <!-- END container-->
</section>

</form>




<footer class="site-footer">
   <div class="footer-bottom bg-black-0_9 py-5 text-center">
    <div class="container">
      <p class="text-white-0_5 mb-0">&copy; 2020. All rights reserved</p>
    </div>
  </div>  <!-- END footer-bottom-->
</footer> <!-- END site-footer -->


<div class="scroll-top">
  <i class="ti-angle-up"></i>
</div>
     
    <script src="<?php echo BASE_PATH;?>website/js/vendors.bundle.js"></script>
    <script src="<?php echo BASE_PATH;?>website/js/scripts.js"></script>
  </body>
</html>

<script>

  function getAge()
  {
    var date_of_birth = $("#date_of_birth").val();
    // alert(date_of_birth);

        if(date_of_birth != '')
        {
            $.ajax(
            {
               url: '/profile/dashboard/getAgeCalculation/'+date_of_birth,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                // alert(result);
                  $("#age").val(result);
               }
            });
        }
  }

  $(document).ready(function()
  {

    var id_country = "";

    // alert(id_country);

    if(id_country > 0)
    {
       $.get("/profile/dashboard/getStateByCountry/"+id_country, function(data, status){
       
            $("#view_state").html(data);

            var state = "<?php echo $student->mailing_state;?>";

            // alert(state);

            $("#state").find('option[value="'+state+'"]').attr('selected',true);
            $('select').select2();


        });
    }

  });



  function getStateByCountry(id)
    {
        $.get("/profile/dashboard/getStateByCountry/"+id, function(data, status){
       
            $("#view_state").html(data);
        });
    }


  function buynow(id)
  {
      var id = 1;
      $.get("/coursedetails/tempbuynow/"+id, function(data, status){
           console.log(data);
           parent.location='../login';
      });
  }

  </script>