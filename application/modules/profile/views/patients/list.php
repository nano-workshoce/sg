  
<div class="py-5 bg-light-v2">
  <div class="container">
   <div class="row align-items-center">
     <div class="col-md-6">
       <h2>Patients Details</h2>
     </div>
     <div class="col-md-6">
      <ol class="breadcrumb justify-content-md-end bg-transparent">  
        <li class="breadcrumb-item">
          <a href="#">Home</a>
        </li> 
        <li class="breadcrumb-item">
          <a href="../appointments/index"> Confirm Test Booking</a>
        </li>
      </ol>
     </div>
   </div>
  </div> 
</div>

<form id="form_main" method="POST">

<section class="padding-y-10">
  <div class="container">

   <div class="row">
        <div class="col-md-12 order-md-1">



        <div class="custom-table">
          <table class="table" id="list-table">
            <thead style="background-color: pink;">
              <tr>
                <th>Sl. No</th>
                <th>Register No.</th>
                <th>Patient Name</th>
                <th>Gender</th>
                <th>Phone No</th>
                <th>Email ID</th>
                <th>DOB</th>
                <th class="text-center">Action</th>
              </tr>
            </thead>
            <tbody>
              <?php
              if (!empty($patientList)) {
                $i=1;
                foreach ($patientList as $record) {
              ?>
                  <tr>
                    <td><?php echo $i ?></td>
                    <td><?php echo $record->register_no ?></td>
                    <td><?php echo $record->full_name ?></td>
                    <td><?php echo $record->gender ?></td>
                    <td><?php echo $record->phone ?></td>
                    <td><?php echo $record->email ?></td>
                    <td><?php echo date("d-m-Y", strtotime($record->date_of_birth)) ?></td>
                    <td class="text-center">
                      <a href="<?php echo 'edit/' . $record->id; ?>" title="Edit">Edit</a>
                      <!--  -->
                    </td>
                  </tr>
              <?php
              $i++;
                }
              }
              ?>
            </tbody>
          </table>
        </div>
        

        </div>
      </div>
  </div> <!-- END container-->
</section>

</form>




<footer class="site-footer">
   <div class="footer-bottom bg-black-0_9 py-5 text-center">
    <div class="container">
      <p class="text-white-0_5 mb-0">&copy; 2020. All rights reserved</p>
    </div>
  </div>  <!-- END footer-bottom-->
</footer> <!-- END site-footer -->


<div class="scroll-top">
  <i class="ti-angle-up"></i>
</div>
     
    <script src="<?php echo BASE_PATH;?>website/js/vendors.bundle.js"></script>
    <script src="<?php echo BASE_PATH;?>website/js/scripts.js"></script>
  </body>
</html>

<script>

</script>