  
<div class="py-5 bg-light-v2">
  <div class="container">
   <div class="row align-items-center">
     <div class="col-md-6">
       <h2>Patient Test Booking Details</h2>
     </div>
     <div class="col-md-6">
      <ol class="breadcrumb justify-content-md-end bg-transparent">  
        <li class="breadcrumb-item">
          <a href="#">Home</a>
        </li> 
        <li class="breadcrumb-item">
          <a href="../appointments/index"> Confirm Test Booking</a>
        </li>
      </ol>
     </div>
   </div>
  </div> 
</div>
<form id="form_main" method="POST">


    <div class="custom-table">
      <table class="table" id="list-table">
        <thead style="background-color: pink;">
          <tr>
            <th>Sl. No</th>
            <th>Reference No.</th>
            <th>Patient Register No.</th>
            <th>Patient Name</th>
            <th>Gender</th>
            <th>Phone No</th>
            <th>Email ID</th>
            <!-- <th>DOB</th>
            <th>Collection By</th> -->
            <th>Date</th>
            <th>Time Slot</th>
            <th class="text-center">Status</th>
            <th class="text-center">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($pendingTestBookingsList)) {
            $i=1;
            foreach ($pendingTestBookingsList as $record) {
            ?>
              <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $record->reference_number ?></td>
                <td><?php echo $record->register_no ?></td>
                <td><?php echo $record->full_name ?></td>
                <td><?php echo $record->gender ?></td>
                <td>
                  <a href="tel:<?php echo $record->phone ?>">
                    <?php echo $record->phone ?>
                  </a>
                </td>
                <td><?php echo $record->email ?></td>
                <!-- <td><?php echo date("d-m-Y", strtotime($record->date_of_birth)) ?></td>
                <td><?php echo $record->staff_id . " - " . $record->staff_name ?></td> -->
                <td><?php echo date('d-m-Y', strtotime($record->appointment_date)) ?></td>
                <td><?php echo $record->time_slot ?></td>
                <td>
                <?php if( $record->status == '0')
                {
                  echo "Pending";
                }
                elseif( $record->status == '1')
                {
                  echo "Completed";
                }
                elseif( $record->status == '2')
                {
                  echo "Cancelled";
                } 
                ?></td>
                <td class="text-center">
                  <?php
                  if( $record->status == '0')
                  {
                  ?>
                    <a href="<?php echo 'view/' . $record->id; ?>" title="Complete / View Appointment">Complete</a>
                  <?php
                  }
                  else
                  {
                    ?>
                    <a href="<?php echo 'edit/' . $record->id; ?>" title="View Appointment">View</a>
                    <?php
                  }
                ?>
                  <!--  -->
                </td>
              </tr>
          <?php
          $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>

</form>




<footer class="site-footer">
   <div class="footer-bottom bg-black-0_9 py-5 text-center">
    <div class="container">
      <p class="text-white-0_5 mb-0">&copy; 2020. All rights reserved</p>
    </div>
  </div>  <!-- END footer-bottom-->
</footer> <!-- END site-footer -->


<div class="scroll-top">
  <i class="ti-angle-up"></i>
</div>
     
    <script src="<?php echo BASE_PATH;?>website/js/vendors.bundle.js"></script>
    <script src="<?php echo BASE_PATH;?>website/js/scripts.js"></script>
  </body>
</html>

<script>

  function getAge()
  {
    var date_of_birth = $("#date_of_birth").val();
    // alert(date_of_birth);

        if(date_of_birth != '')
        {
            $.ajax(
            {
               url: '/profile/dashboard/getAgeCalculation/'+date_of_birth,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                // alert(result);
                  $("#age").val(result);
               }
            });
        }
  }

  $(document).ready(function()
  {

    var id_country = "";

    // alert(id_country);

    if(id_country > 0)
    {
       $.get("/profile/dashboard/getStateByCountry/"+id_country, function(data, status){
       
            $("#view_state").html(data);

            var state = "<?php echo $student->mailing_state;?>";

            // alert(state);

            $("#state").find('option[value="'+state+'"]').attr('selected',true);
            $('select').select2();


        });
    }

  });



  function getStateByCountry(id)
    {
        $.get("/profile/dashboard/getStateByCountry/"+id, function(data, status){
       
            $("#view_state").html(data);
        });
    }


  function buynow(id)
  {
      var id = 1;
      $.get("/coursedetails/tempbuynow/"+id, function(data, status){
           console.log(data);
           parent.location='../login';
      });
  }

  </script>