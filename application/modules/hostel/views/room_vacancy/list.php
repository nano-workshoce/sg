<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>List Room Vacancy</h3>
      <!-- <a href="add" class="btn btn-primary">+ Allot Hostel</a> -->
    </div>

    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Advanced Search
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">


                <div class="row">

                  <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Hostel </label>
                    <div class="col-sm-8">
                      <select name="id_hostel" id="id_hostel" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($hostelList)) {
                          foreach ($hostelList as $record)
                          {
                            $selected = '';
                            if ($record->id == $searchParam['id_hostel']) {
                              $selected = 'selected';
                            }
                        ?>
                            <option value="<?php echo $record->id;  ?>"
                              <?php echo $selected;  ?>>
                              <?php echo  $record->code ."-".$record->name;  ?>
                              </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>
                  
                  <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Apartment </label>
                    <div class="col-sm-8">
                      <select name="id_building" id="id_building" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($buildingList)) {
                          foreach ($buildingList as $record)
                          {
                            $selected = '';
                            if ($record->id == $searchParam['id_building']) {
                              $selected = 'selected';
                            }
                        ?>
                            <option value="<?php echo $record->id;  ?>"
                              <?php echo $selected;  ?>>
                              <?php echo  $record->code ."-".$record->name;  ?>
                              </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>

               

                </div>




                <div class="row">

                  
                  
                  <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Block </label>
                    <div class="col-sm-8">
                      <select name="id_block" id="id_block" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($blockList)) {
                          foreach ($blockList as $record)
                          {
                            $selected = '';
                            if ($record->id == $searchParam['id_block']) {
                              $selected = 'selected';
                            }
                        ?>
                            <option value="<?php echo $record->id;  ?>"
                              <?php echo $selected;  ?>>
                              <?php echo  $record->code ."-".$record->name;  ?>
                              </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Room Type </label>
                    <div class="col-sm-8">
                      <select name="id_room_type" id="id_room_type" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($roomTypeList)) {
                          foreach ($roomTypeList as $record)
                          {
                            $selected = '';
                            if ($record->id == $searchParam['id_room_type']) {
                              $selected = 'selected';
                            }
                        ?>
                            <option value="<?php echo $record->id;  ?>"
                              <?php echo $selected;  ?>>
                              <?php echo  $record->code ."-".$record->name;  ?>
                              </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>

               

                </div>





              </div>
              <div class="app-btn-group">
                <button type="submit" class="btn btn-primary">Search</button>
                <button type="reset" class="btn btn-link" onclick="clearSearchForm()">Clear All Fields</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>

    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl. No</th>
            <!-- <th>Student</th> -->
            <th>Room</th>
            <!-- <th>Hostel</th> -->
            <th class="text-center">Max Capacity</th>
            <!-- <th>Block</th> -->
            <th class="text-center">Filled / Vacant</th>
            <!-- <th>Status</th> -->
            <th style="text-align:center; ">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($roomVacancyList))
          {
            $i=1;
            foreach ($roomVacancyList as $record) {
          ?>
              <tr>
                <td><?php echo $i ?></td>
                <!-- <td><?php echo $record->nric . " - " . $record->full_name ?></td> -->
                <td><?php echo $record->room_code . " - " . $record->room_name ?></td>
                <!-- <td><?php echo $record->hostel_code . " - " . $record->hostel_name ?></td> -->
                <td class="text-center"><?php echo $record->max_capacity ?></td>
                <td class="text-center"><?php echo $record->filled_count . " / " . $record->vacant_count ?></td>
               <td class="text-center"><?php if( $record->vacant_count > 0)
                {
                  ?>
                  
                  <a href="<?php echo 'add/' . $record->id; ?>" title="Edit">Allocate</a>
                   | 
                  <a href="<?php echo 'view/' . $record->id . '/vacancy'; ?>" title="Edit">View</a>
                  
                  <?php
                }
                else
                {
                  ?>
                  <a href="<?php echo 'view/' . $record->id . '/vacancy'; ?>" title="Edit">View</a>
                  <?php
                } 
                ?></td>
                <!-- <td class="text-center">
                  <a href="<?php echo 'add/' . $record->id; ?>" title="Edit">Allocate</a>
                </td> -->
              </tr>
          <?php
          $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
<script>
  
  $('select').select2();

  function clearSearchForm()
      {
        window.location.reload();
      }
</script>