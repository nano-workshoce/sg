<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Toc extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('toc_model');
        $this->isSupervisorLoggedIn();
    }

    function list()
    {       
        $id_supervisor = $this->session->id_supervisor;
        $data['supervisor'] = $this->toc_model->getSupervisor($id_supervisor);
        $data['tocList'] = $this->toc_model->getTocListBySupervisorId($id_supervisor);

        // echo "<Pre>";print_r($data['tocList']);exit();

        $this->global['pageTitle'] = 'Supervisor Portal : List Toc Reporting';
        $this->loadViews("toc/list", $this->global, $data, NULL);
    }
    
    function edit($id = NULL)
    {
        $id_supervisor = $this->session->id_supervisor;

        if ($id == null)
        {
            redirect('/supervisor/toc/list');
        }

        if($this->input->post())
        {
            if($_FILES['upload_file'])
            {
            // echo "<Pre>"; print_r($_FILES['image']);exit;

                $certificate_name = $_FILES['upload_file']['name'];
                $certificate_size = $_FILES['upload_file']['size'];
                $certificate_tmp =$_FILES['upload_file']['tmp_name'];
                
                // echo "<Pre>"; print_r($certificate_tmp);exit();

                $certificate_ext=explode('.',$certificate_name);
                $certificate_ext=end($certificate_ext);
                $certificate_ext=strtolower($certificate_ext);


                $this->fileFormatNSizeValidation($certificate_ext,$certificate_size,'Image File');

                $upload_file = $this->uploadFile($certificate_name,$certificate_tmp,'Image File');
            }

            $comments = $this->security->xss_clean($this->input->post('comments'));

            $comments_data = array(
                'comments' => $comments,
                'id_supervisor' => $id_supervisor,
                'id_toc' => $id
            );

            if($upload_file)
            {
                $comments_data['upload_file'] = $upload_file;
            }
            $added_comments = $this->toc_model->addTocReportingComments($comments_data);
            
            redirect($_SERVER['HTTP_REFERER']);
         }

        $data['toc'] = $this->toc_model->getToc($id);
        $data['tocReportingComments'] = $this->toc_model->tocCommentsDetails($id);

        $data['studentDetails'] = $this->toc_model->getStudentByStudentId($data['toc']->id_student);
        $data['organisationDetails'] = $this->toc_model->getOrganisation();

            
        // echo "<Pre>"; print_r($data['researchStatusList']);exit;

        $this->global['pageTitle'] = 'Supervisor Portal : View Toc Reporting';
        $this->loadViews("toc/edit", $this->global, $data, NULL);
    }

    function view($id = NULL)
    {
        $id_supervisor = $this->session->id_supervisor;

        if ($id == null)
        {
            redirect('/supervisor/toc/list');
        }

        if($this->input->post())
        {
            if($_FILES['upload_file'])
            {
            // echo "<Pre>"; print_r($_FILES['image']);exit;

                $certificate_name = $_FILES['upload_file']['name'];
                $certificate_size = $_FILES['upload_file']['size'];
                $certificate_tmp =$_FILES['upload_file']['tmp_name'];
                
                // echo "<Pre>"; print_r($certificate_tmp);exit();

                $certificate_ext=explode('.',$certificate_name);
                $certificate_ext=end($certificate_ext);
                $certificate_ext=strtolower($certificate_ext);


                $this->fileFormatNSizeValidation($certificate_ext,$certificate_size,'Image File');

                $upload_file = $this->uploadFile($certificate_name,$certificate_tmp,'Image File');
            }

            $comments = $this->security->xss_clean($this->input->post('comments'));

            $comments_data = array(
                'comments' => $comments,
                'id_supervisor' => $id_supervisor,
                'id_toc' => $id
            );

            if($upload_file)
            {
                $comments_data['upload_file'] = $upload_file;
            }
            $added_comments = $this->toc_model->addTocReportingComments($comments_data);
            
            redirect($_SERVER['HTTP_REFERER']);
         }


        // $data['studentDetails'] = $this->toc_model->getStudentByStudentId($id_student);
        $data['toc'] = $this->toc_model->getToc($id);
        $data['tocReportingComments'] = $this->toc_model->tocCommentsDetails($id);

        $data['studentDetails'] = $this->toc_model->getStudentByStudentId($data['toc']->id_student);
        $data['organisationDetails'] = $this->toc_model->getOrganisation();

        // echo "<Pre>"; print_r($data);exit;

        $this->global['pageTitle'] = 'Supervisor Portal : View Toc Reporting';
        $this->loadViews("toc/edit", $this->global, $data, NULL);
    }
}