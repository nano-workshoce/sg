<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class ProposalReporting extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('proposal_reporting_model');
        $this->isSupervisorLoggedIn();
    }

    function list()
    {   
        $id_supervisor = $this->session->id_supervisor;

        $formData['name'] = $this->security->xss_clean($this->input->post('name'));
        $formData['id_phd_duration'] = $this->security->xss_clean($this->input->post('id_phd_duration'));
        $formData['stage'] = 1;
        // $formData['status'] = $this->security->xss_clean($this->input->post('status'));

        $data['searchParam'] = $formData;

        $formData['id_supervisor'] = $id_supervisor;


        $data['supervisor'] = $this->proposal_reporting_model->getSupervisor($id_supervisor);
        $data['durationList'] = $this->proposal_reporting_model->durationListByStatus('1');

        $data['deliverablesList'] = $this->proposal_reporting_model->getProposalReportingListBySupervisorId($id_supervisor,'1');
        $data['stage'] = 1;

        // echo "<Pre>";print_r($data['deliverablesList']);exit();

        $this->global['pageTitle'] = 'Supervisor Portal : List Proposal Reporting Application';
        $this->loadViews("proposal_reporting/list", $this->global, $data, NULL);
    }
    


    function view($id = NULL)
    {
        $id_supervisor = $this->session->id_supervisor;

        if ($id == null)
        {
            redirect('/student/proposalReporting/list');
        }

        if($this->input->post())
        {
            if($_FILES['upload_file'])
            {
            // echo "<Pre>"; print_r($_FILES['image']);exit;

                $certificate_name = $_FILES['upload_file']['name'];
                $certificate_size = $_FILES['upload_file']['size'];
                $certificate_tmp =$_FILES['upload_file']['tmp_name'];
                
                // echo "<Pre>"; print_r($certificate_tmp);exit();

                $certificate_ext=explode('.',$certificate_name);
                $certificate_ext=end($certificate_ext);
                $certificate_ext=strtolower($certificate_ext);


                $this->fileFormatNSizeValidation($certificate_ext,$certificate_size,'Image File');

                $upload_file = $this->uploadFile($certificate_name,$certificate_tmp,'Image File');
            }

            $comments = $this->security->xss_clean($this->input->post('comments'));

            $comments_data = array(
                'comments' => $comments,
                'id_supervisor' => $id_supervisor,
                'id_proposal_reporting' => $id
            );

            if($upload_file)
            {
                $comments_data['upload_file'] = $upload_file;
            }


            $added_comments = $this->proposal_reporting_model->addProposalReportingComments($comments_data);
            redirect($_SERVER['HTTP_REFERER']);
            // echo "<Pre>"; print_r($this->input->post());exit;
        }

        $data['proposalReporting'] = $this->proposal_reporting_model->getProposalReport($id);
        $data['proposalReportingComments'] = $this->proposal_reporting_model->getProposalReportComments($id);

        $data['studentDetails'] = $this->proposal_reporting_model->getStudentByStudentId($data['proposalReporting']->id_student);
        $data['organisationDetails'] = $this->proposal_reporting_model->getOrganisation();

        $data['durationList'] = $this->proposal_reporting_model->durationListByStatus('1');
        $data['chapterList'] = $this->proposal_reporting_model->chapterListByStatus('1');
        $data['deliverableList'] = $this->proposal_reporting_model->deliverableListByStatus('1');
        $data['stage'] = 1;
            
        // echo "<Pre>"; print_r($data['proposalReportingComments']);exit;

        $this->global['pageTitle'] = 'Supervisor Portal : View Proposal Reporting Comments';
        $this->loadViews("proposal_reporting/view", $this->global, $data, NULL);
    }

    function list2()
    {   
        $id_supervisor = $this->session->id_supervisor;

        $formData['name'] = $this->security->xss_clean($this->input->post('name'));
        $formData['id_phd_duration'] = $this->security->xss_clean($this->input->post('id_phd_duration'));
        $formData['stage'] = 2;
        // $formData['status'] = $this->security->xss_clean($this->input->post('status'));

        $data['searchParam'] = $formData;

        $formData['id_supervisor'] = $id_supervisor;


        $data['supervisor'] = $this->proposal_reporting_model->getSupervisor($id_supervisor);
        $data['durationList'] = $this->proposal_reporting_model->durationListByStatus('1');

        $data['deliverablesList'] = $this->proposal_reporting_model->getProposalReportingListBySupervisorId($id_supervisor,'2');
        $data['stage'] = 2;

        // echo "<Pre>";print_r($data['deliverablesList']);exit();

        $this->global['pageTitle'] = 'Supervisor Portal : List Proposal Reporting Application';
        $this->loadViews("proposal_reporting/list", $this->global, $data, NULL);
    }
    


    function view2($id = NULL)
    {
        $id_supervisor = $this->session->id_supervisor;

        if ($id == null)
        {
            redirect('/student/proposalReporting/list2');
        }

        if($this->input->post())
        {
            if($_FILES['upload_file'])
            {
            // echo "<Pre>"; print_r($_FILES['image']);exit;

                $certificate_name = $_FILES['upload_file']['name'];
                $certificate_size = $_FILES['upload_file']['size'];
                $certificate_tmp =$_FILES['upload_file']['tmp_name'];
                
                // echo "<Pre>"; print_r($certificate_tmp);exit();

                $certificate_ext=explode('.',$certificate_name);
                $certificate_ext=end($certificate_ext);
                $certificate_ext=strtolower($certificate_ext);


                $this->fileFormatNSizeValidation($certificate_ext,$certificate_size,'Image File');

                $upload_file = $this->uploadFile($certificate_name,$certificate_tmp,'Image File');
            }

            $comments = $this->security->xss_clean($this->input->post('comments'));

            $comments_data = array(
                'comments' => $comments,
                'id_supervisor' => $id_supervisor,
                'id_proposal_reporting' => $id
            );

            if($upload_file)
            {
                $comments_data['upload_file'] = $upload_file;
            }


            $added_comments = $this->proposal_reporting_model->addProposalReportingComments($comments_data);
            redirect($_SERVER['HTTP_REFERER']);
            // echo "<Pre>"; print_r($this->input->post());exit;
        }

        $data['proposalReporting'] = $this->proposal_reporting_model->getProposalReport($id);
        $data['proposalReportingComments'] = $this->proposal_reporting_model->getProposalReportComments($id);

        $data['studentDetails'] = $this->proposal_reporting_model->getStudentByStudentId($data['proposalReporting']->id_student);
        $data['organisationDetails'] = $this->proposal_reporting_model->getOrganisation();

        $data['durationList'] = $this->proposal_reporting_model->durationListByStatus('1');
        $data['chapterList'] = $this->proposal_reporting_model->chapterListByStatus('1');
        $data['deliverableList'] = $this->proposal_reporting_model->deliverableListByStatus('1');
        $data['stage'] = 2;
            
        // echo "<Pre>"; print_r($data['proposalReportingComments']);exit;

        $this->global['pageTitle'] = 'Supervisor Portal : View Proposal Reporting Comments';
        $this->loadViews("proposal_reporting/view", $this->global, $data, NULL);
    }

    function list3()
    {   
        $id_supervisor = $this->session->id_supervisor;

        $formData['name'] = $this->security->xss_clean($this->input->post('name'));
        $formData['id_phd_duration'] = $this->security->xss_clean($this->input->post('id_phd_duration'));
        $formData['stage'] = 3;
        // $formData['status'] = $this->security->xss_clean($this->input->post('status'));

        $data['searchParam'] = $formData;

        $formData['id_supervisor'] = $id_supervisor;


        $data['supervisor'] = $this->proposal_reporting_model->getSupervisor($id_supervisor);
        $data['durationList'] = $this->proposal_reporting_model->durationListByStatus('1');

        $data['deliverablesList'] = $this->proposal_reporting_model->getProposalReportingListBySupervisorId($id_supervisor,'3');
        $data['stage'] = 3;

        // echo "<Pre>";print_r($data['deliverablesList']);exit();

        $this->global['pageTitle'] = 'Supervisor Portal : List Proposal Reporting Application';
        $this->loadViews("proposal_reporting/list", $this->global, $data, NULL);
    }
    


    function view3($id = NULL)
    {
        $id_supervisor = $this->session->id_supervisor;

        if ($id == null)
        {
            redirect('/student/proposalReporting/list3');
        }

        if($this->input->post())
        {
            if($_FILES['upload_file'])
            {
            // echo "<Pre>"; print_r($_FILES['image']);exit;

                $certificate_name = $_FILES['upload_file']['name'];
                $certificate_size = $_FILES['upload_file']['size'];
                $certificate_tmp =$_FILES['upload_file']['tmp_name'];
                
                // echo "<Pre>"; print_r($certificate_tmp);exit();

                $certificate_ext=explode('.',$certificate_name);
                $certificate_ext=end($certificate_ext);
                $certificate_ext=strtolower($certificate_ext);


                $this->fileFormatNSizeValidation($certificate_ext,$certificate_size,'Image File');

                $upload_file = $this->uploadFile($certificate_name,$certificate_tmp,'Image File');
            }

            $comments = $this->security->xss_clean($this->input->post('comments'));

            $comments_data = array(
                'comments' => $comments,
                'id_supervisor' => $id_supervisor,
                'id_proposal_reporting' => $id
            );

            if($upload_file)
            {
                $comments_data['upload_file'] = $upload_file;
            }


            $added_comments = $this->proposal_reporting_model->addProposalReportingComments($comments_data);
            redirect($_SERVER['HTTP_REFERER']);
            // echo "<Pre>"; print_r($this->input->post());exit;
        }

        $data['proposalReporting'] = $this->proposal_reporting_model->getProposalReport($id);
        $data['proposalReportingComments'] = $this->proposal_reporting_model->getProposalReportComments($id);

        $data['studentDetails'] = $this->proposal_reporting_model->getStudentByStudentId($data['proposalReporting']->id_student);
        $data['organisationDetails'] = $this->proposal_reporting_model->getOrganisation();

        $data['durationList'] = $this->proposal_reporting_model->durationListByStatus('1');
        $data['chapterList'] = $this->proposal_reporting_model->chapterListByStatus('1');
        $data['deliverableList'] = $this->proposal_reporting_model->deliverableListByStatus('1');
        $data['stage'] = 3;
            
        // echo "<Pre>"; print_r($data['proposalReportingComments']);exit;

        $this->global['pageTitle'] = 'Supervisor Portal : View Proposal Reporting Comments';
        $this->loadViews("proposal_reporting/view", $this->global, $data, NULL);
    }






    function add()
    {

        $id_student = $this->session->id_student;
        $id_intake = $this->session->id_intake;
        $id_program = $this->session->id_program;
        $id_qualification = $this->session->id_qualification;

        if($this->input->post())
        {
            // echo "<Pre>"; print_r($this->input->post());exit;

            $stage = 1;
            $phd_duration = $this->security->xss_clean($this->input->post('phd_duration'));
            $id_chapter = $this->security->xss_clean($this->input->post('id_chapter'));
            $id_deliverable = $this->security->xss_clean($this->input->post('id_deliverable'));
            $from_dt = $this->security->xss_clean($this->input->post('from_dt'));
            $to_dt = $this->security->xss_clean($this->input->post('to_dt'));
            $target_dt = $this->security->xss_clean($this->input->post('target_dt'));
            $description = $this->security->xss_clean($this->input->post('description'));
            // $status = $this->security->xss_clean($this->input->post('status'));

            $student = $this->proposal_reporting_model->getStudent($id_student);
            $generated_number = $this->proposal_reporting_model->generateProposalReportingApplicationNumber();



            $data = array(
                'application_number' => $generated_number,
                'id_student' => $id_student,
                'id_supervisor' => $student->id_supervisor,
                'phd_duration' => $phd_duration,
                'id_chapter' => $id_chapter,
                'id_deliverable' => $id_deliverable,
                'from_dt' => date('Y-m-d', strtotime($from_dt)),
                'to_dt' => date('Y-m-d', strtotime($to_dt)),
                'target_dt' => date('Y-m-d', strtotime($target_dt)),
                'description' => $description,
                'stage' => $stage,
                'status' => 1
            );

            // $check_limit = $this->proposal_reporting_model->checkStudentDeliverables($data);
            // if($check_limit == 1)
            // {
            //      echo "<Pre>";print_r("Max Application Limit Reached For Active & Pending Deliverables Application");exit();
            // }            
                 // echo "<Pre>";print_r($data);exit();
            $insert_id = $this->proposal_reporting_model->addNewProposalReporting($data);
            redirect('/student/proposalReporting/list');
        }

        $data['studentDetails'] = $this->proposal_reporting_model->getStudentByStudentId($id_student);
        $data['durationList'] = $this->proposal_reporting_model->durationListByStatus('1');
        $data['researchTopicList'] = $this->proposal_reporting_model->researchTopicListByStatus('1');
        $data['researchStatusList'] = $this->proposal_reporting_model->researchStatusListByStatus('1');

        $this->global['pageTitle'] = 'Supervisor Portal : Add Deliverables Form';
        $this->loadViews("proposal_reporting/add", $this->global, $data, NULL);
    }

    function getChapterByDuration($id_duration)
    {
        $results = $this->proposal_reporting_model->getChapterByDuration($id_duration);

        // echo "<Pre>"; print_r($results);exit;

        $table="

         <script type='text/javascript'>
                 $('select').select2();
         </script>


        <select name='id_chapter' id='id_chapter' class='form-control' onchange='getTopicByData()'>
            <option value=''>Select</option>
            ";

        for($i=0;$i<count($results);$i++)
        {

        // $id = $results[$i]->id_procurement_category;
        $id = $results[$i]->id;
        $name = $results[$i]->name;
        $table.="<option value=".$id.">".$name.
                "</option>";

        }
        $table.="

        </select>";

        echo $table;
        exit;
    }

    function getTopicByData()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        $results = $this->proposal_reporting_model->getTopicByData($tempData);

        // echo "<Pre>"; print_r($results);exit;

        $table="

         <script type='text/javascript'>
                 $('select').select2();
         </script>


        <select name='id_deliverable' id='id_deliverable' class='form-control'>
            <option value=''>Select</option>
            ";

        for($i=0;$i<count($results);$i++)
        {

        // $id = $results[$i]->id_procurement_category;
        $id = $results[$i]->id;
        $name = $results[$i]->topic;
        $table.="<option value=".$id.">".$name.
                "</option>";

        }
        $table.="

        </select>";

        echo $table;
        exit;
    }
}

