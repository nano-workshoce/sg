<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Profile extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('profile_model');
        $this->isSupervisorLoggedIn();
    }

    public function index()
    {
        $this->view();
    }

    function pageNotFound()
    {
        $this->global['pageTitle'] = 'Supervisor Portal : 404 - Page Not Found';
        $this->loadViews("404", $this->global, NULL, NULL);
    }

    function view()
    {
        $id_supervisor = $this->session->id_supervisor;
        
        $data['supervisor'] = $this->profile_model->getSupervisor($id_supervisor);
        
        $this->global['pageTitle'] = 'Supervisor Portal : Supervisor Profile';
        $this->loadViews("profile/edit", $this->global, $data, NULL);
    }

    function logout()
    {
        // echo "string";exit();
        // echo $isSupervisorAdminLoggedIn;exit();
            
            $sessionArray = array('id_supervisor'=> '',                    
                    'supervisor_name'=> '',
                    'supervisor_last_login'=> '',
                    'my_supervisor_session_id' => '',
                    'isSupervisorLoggedIn' => FALSE
            );

            $this->session->set_userdata($sessionArray);

            // $this->session->set_userdata("id_admin_student",$id_student);


     // $this->session->sess_destroy();
     // redirect($_SERVER['HTTP_REFERER']);
     $this->isSupervisorLoggedIn();
    }
}
