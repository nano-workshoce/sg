<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>Profile</h3>
      <!-- <a href="add" class="btn btn-primary">+ Add Topic</a> -->
    </div>



        <div class="form-container">
                <h4 class="form-group-title">Supervisor Details</h4>
                <div class='data-list'>
                    <div class='row'> 
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Supervisor Name :</dt>
                                <dd><?php echo ucwords($supervisor->full_name);?></dd>
                            </dl>
                            <dl>
                                <dt>Supervisor Email :</dt>
                                <dd><?php echo $supervisor->email ?></dd>
                            </dl>                     
                        </div>        
                        
                        <div class='col-sm-6'>                           
                            <dl>
                                <dt>Supervisor Type :</dt>
                                <dd><?php 
                                if($supervisor->type == 0)
                                {
                                    echo 'External';
                                }elseif($supervisor->type == 1)
                                {
                                    echo 'Internal';
                                } ?></dd>
                            </dl>
                        </div>
                    </div>
                </div>
            </div>


        <br>
  </div>
</div>
<script>
    $('select').select2();
</script>