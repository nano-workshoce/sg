<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Investment Registration</h3>
        </div>
        <form id="form_investment_registration" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Investment Withdraw</h4>



            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select Investment Registration <span class='error-text'>*</span></label>
                        <select name="id_investment_registration" id="id_investment_registration" class="form-control" onchange="getRegistrationDetails(this.value)">
                            <option value="">Select</option>
                            <?php
                            if (!empty($investmentRegistrationList))
                            {
                                foreach ($investmentRegistrationList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->registration_number;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

            </div>

        </div>


        <br>

        <div class="form-container" style="display: none;" id="view_registration_data_display">
            <h4 class="form-group-title">Investment Registration Details</h4>



            <div id="view_registration_data">
            </div>

        </div>


        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="list" class="btn btn-link">Cancel</a>
            </div>
        </div>

            
        </form>

    </div>

        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
</div>
<script>

    function getRegistrationDetails(id)
    {
       
        $.get("/investment/investmentWithdraw/getRegistrationDetails/"+id, function(data, status)
        {
            $("#view_registration_data_display").show();
            $("#view_registration_data").html(data);
        });
    }


    function getApplicationData(id)
    {
       
        $.get("/investment/investmentRegistration/getApplicationData/"+id, function(data, status)
        {
            // alert(data);
            $("#view_application_details_data").html(data);
        });

    }



    $(document).ready(function() {
        $("#form_investment_registration").validate({
            rules: {
                id_investment_registration: {
                    required: true
                }
            },
            messages: {
                id_investment_registration: {
                    required: "<p class='error-text'>Select Investment Registration</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>

<script type='text/javascript'>
     $('select').select2();
 </script>