            <div class="sidebar">
                <div class="user-profile clearfix">
                    <a href="#" class="user-profile-link">
                        <span><img src="<?php echo BASE_PATH; ?>assets/img/user_profile.jpg"></span> <?php echo $name; ?>
                    </a>
                    <div class="dropdown">
                        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
                            <li><a href="/setup/user/profile">Edit Profile</a></li>
                            <li><a href="/setup/user/logout">Logout</a></li>
                        </ul>
                    </div>
                </div>
                <div class="sidebar-nav">
                    <h4>Setup</h4>
                    <ul>
                        <li><a href="/investment/investmentType/list">Investment Type</a></li>
                        <li><a href="/investment/investmentInstitution/list">Investment Institution</a></li>
                    </ul>

                    <h4>Investment Application</h4>
                    <ul>
                       
                        <li><a href="/investment/investmentApplication/list">Application</a></li>
                        <li><a href="/investment/investmentApplication/approvalList">Application Approval</a></li>
                       
                    </ul>

                    <h4>Investment Registration</h4>
                    <ul>
                       
                        <li><a href="/investment/investmentRegistration/list">Investment Registration</a></li>
                       
                    </ul>

                    <h4>Investment Withdraw</h4>
                    <ul>
                       
                        <li><a href="/investment/investmentWithdraw/list">Investment Registration Withdraw</a></li>
                        <li><a href="/investment/investmentWithdraw/approvalList">Registration Withdraw Approval</a></li>
                        <li><a href="/investment/investmentRegistration/reinvestmentList">Re-Investment</a></li>
                       
                    </ul>

                   

                </div>
            </div>