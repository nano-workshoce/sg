<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Investment Registration</h3>
        </div>
        <form id="form_investment_registration" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Investment Application Details</h4>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select Investment Application <span class='error-text'>*</span></label>
                        <select name="id_application" id="id_application" class="form-control" onchange="getApplicationDetails(this.value)">
                            <option value="">Select</option>
                            <?php
                            if (!empty($investmentApplicationList))
                            {
                                foreach ($investmentApplicationList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->application_number;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select Application Details <span class='error-text'>*</span></label>
                        <span id='view_application_detail'></span>
                    </div>
                </div>

            </div>
        </div>


           <!--  <div class="page-title clearfix">
                <h4>Application Details</h4>
            </div> -->

        <div class="form-container" style="display: none;" id="view_application_details_data_display">
            <h4 class="form-group-title">Investment Registration Details </h4>

            <div id="view_application_details_data">
            </div>

        </div>

            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="list" class="btn btn-link">Cancel</a>
                </div>
            </div>
            
        </form>

    </div>

        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
</div>
<script>

    function getApplicationDetails(id)
    {
       
        $.get("/investment/investmentRegistration/getApplicationDetails/"+id, function(data, status){
            $("#view_application_detail").html(data);
        });
    }


    function getApplicationData(id)
    {
       
        $.get("/investment/investmentRegistration/getApplicationData/"+id, function(data, status)
        {
            // alert(data);
            $("#view_application_details_data_display").show();
            $("#view_application_details_data").html(data);
        });

    }



    $(document).ready(function() {
        $("#form_investment_registration").validate({
            rules: {
                id_application: {
                    required: true
                },
                id_application_detail: {
                    required: true
                },
                interest_to_bank: {
                    required: true
                },
                name: {
                    required: true
                },
                contact_person_one: {
                    required: true
                },
                contact_email_one: {
                    required: true
                },
                rate_of_interest: {
                    required: true
                },
                contact_person_two: {
                    required: true
                },
                contact_email_two: {
                    required: true
                },
                interest_day: {
                    required: true
                },
                profit_amount: {
                    required: true
                },
                account_code: {
                    required: true
                },
                activity_code: {
                    required: true
                },
                department_code: {
                    required: true
                },
                fund_code: {
                    required: true
                },
                effective_date: {
                    required: true
                }
            },
            messages: {
                id_application: {
                    required: "<p class='error-text'>Select Investment Application</p>",
                },
                id_application_detail: {
                    required: "<p class='error-text'>Select Investment Application Details</p>",
                },
                interest_to_bank: {
                    required: "<p class='error-text'>Enter Interest To Bank</p>",
                },
                name: {
                    required: "<p class='error-text'>Enter Name For The Registration</p>",
                },
                contact_person_one: {
                    required: "<p class='error-text'>Enter Contact Person Name</p>",
                },
                contact_email_one: {
                    required: "<p class='error-text'>Enter Contact E-mail</p>",
                },
                rate_of_interest: {
                    required: "<p class='error-text'>Enter Rate Of Interest</p>",
                },
                contact_person_two: {
                    required: "<p class='error-text'>Enter Contact Person 2 Name</p>",
                },
                contact_email_two: {
                    required: "<p class='error-text'>Enter Contact 2 E-mail</p>",
                },
                interest_day: {
                    required: "<p class='error-text'>Enter Interest Day Of the Month</p>",
                },
                profit_amount: {
                    required: "<p class='error-text'>Enter Profit Amount</p>",
                },
                account_code: {
                    required: "<p class='error-text'>Select Account Code</p>",
                },
                activity_code: {
                    required: "<p class='error-text'>Select Activity Code</p>",
                },
                department_code: {
                    required: "<p class='error-text'>Select Department Code</p>",
                },
                fund_code: {
                    required: "<p class='error-text'>Select Fund Code</p>",
                },
                effective_date: {
                    required: "<p class='error-text'>Select Effective Date</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>

<script type='text/javascript'>
     $('select').select2();
 </script>