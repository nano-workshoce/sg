<?php $this->load->helper("form"); ?>
<form id="form_pr_entry" action="" method="post">
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
            <div class="page-title clearfix">
                <h3>View Investment Application</h3>
            </div>

        <div class="form-container">
            <h4 class="form-group-title">Investment Application</h4>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Description <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $investmentApplication->description;?>" readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Total Investment Amount <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="amount" name="amount" value="<?php echo $investmentApplication->amount;?>" readonly="readonly">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Application Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="date_time" name="date_time" value="<?php echo date("d-m-Y", strtotime($investmentApplication->date_time));?>" readonly="readonly">
                    </div>
                </div>
            </div>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Invested Bank <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="date_time" name="date_time" value="<?php echo $investmentApplication->bank_code . " - " . $investmentApplication->bank_name;?>" readonly="readonly">
                    </div>
                </div>

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Status <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="date_time" name="date_time" value="<?php
                        if($investmentApplication->status == '0')
                        {
                            echo "Pending";
                        }
                        elseif($investmentApplication->status == '1')
                        {
                            echo "Approved";
                        }
                        elseif($investmentApplication->status == '2')
                        {
                            echo "Rejected";
                        }
                         ?>" readonly="readonly">
                    </div>
                </div>
                
            </div>

        </div>

        <h3>Investment Application Details</h3>


    <div class="form-container">
            <h4 class="form-group-title">Investment Application Details</h4>


        <div class="custom-table">
            <table class="table">
                <thead>
                     <tr>
                        <th>Sl. No</th>
                        <th>Investment Type</th>
                        <th>Duration</th>
                        <th>Investment Institution</th>
                        <th>Bank</th>
                        <th>Profit rate</th>
                        <th>Maturity Date</th>
                        <th>Investment Amount</th>
                        <th>Status</th>
                     </tr>
                </thead>
                <tbody>

                     <?php 
                     $total=0;
                     for($i=0;$i<count($iADetails);$i++)
                     {
                      ?>
                        <tr>
                        <td><?php echo $i+1; ?></td>
                        <td><?php echo $iADetails[$i]->investment_type;?></td>
                        <td><?php echo $iADetails[$i]->duration . " " . $iADetails[$i]->duration_type;?></td>
                        <td><?php echo $iADetails[$i]->investment_institution_code . " - " . $iADetails[$i]->investment_institution_name;?></td>
                        <td><?php echo $iADetails[$i]->bank_code . " - " . $iADetails[$i]->bank_name;?></td>
                        <td><?php echo $iADetails[$i]->profit_rate;?></td>
                        <td><?php echo $iADetails[$i]->maturity_date;?></td>
                        <td><?php echo $iADetails[$i]->total_amount;?></td>
                        <td><?php if( $iADetails[$i]->status == '1')
                        {
                          echo "Approved";
                        }
                        else
                        {
                          echo "Pending";
                        } 
                        ?></td>
                         </tr>
                      <?php 
                      $total = $total + $iADetails[$i]->total_amount;
                  }
                   $total = number_format($total, 2, '.', ',');
                   ?>

                    <tr>
                        <td bgcolor="" colspan="6"></td>
                        <td bgcolor=""><b> Total : </b></td>
                        <td bgcolor=""><b><?php echo $total; ?></b></td>
                        <td bgcolor=""></td>
                    </tr>

                </tbody>
            </table>
        </div>

    </div>


        <div class="button-block clearfix">
            <div class="bttn-group">
                <!-- <button type="submit" class="btn btn-primary btn-lg">Save</button> -->
                <a href="../list" class="btn btn-link">Back</a>
            </div>
        </div>
        
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>