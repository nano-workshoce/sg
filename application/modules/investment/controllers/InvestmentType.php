<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class InvestmentType extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('investment_type_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('investment_type.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $formData['account_code'] = $this->security->xss_clean($this->input->post('account_code'));
            $formData['activity_code'] = $this->security->xss_clean($this->input->post('activity_code'));
            $formData['department_code'] = $this->security->xss_clean($this->input->post('department_code'));
            $formData['fund_code'] = $this->security->xss_clean($this->input->post('fund_code'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['contact_number'] = $this->security->xss_clean($this->input->post('contact_number'));
            $formData['status'] = '';
 
            $data['searchParam'] = $formData;

            $data['investmentTypeList'] = $this->investment_type_model->investmentTypeListSearch($formData);
            
            $data['fundCodeList'] = $this->investment_type_model->getFundCodeList();
            $data['departmentCodeList'] = $this->investment_type_model->getDepartmentCodeList();
            $data['accountCodeList'] = $this->investment_type_model->getAccountCodeList();
            $data['activityCodeList'] = $this->investment_type_model->getActivityCodeList();
            // echo "<Pre>";
            // print_r($formData);exit();


            $this->global['pageTitle'] = 'FIMS : List Investment Type';
            $this->loadViews("investment_type/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('investment_type.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $id_session = $this->session->my_session_id;
                $user_id = $this->session->userId;

                $type = $this->security->xss_clean($this->input->post('type'));
                $contact_name = $this->security->xss_clean($this->input->post('contact_name'));
                $contact_number = $this->security->xss_clean($this->input->post('contact_number'));
                $account_code = $this->security->xss_clean($this->input->post('account_code'));
                $activity_code = $this->security->xss_clean($this->input->post('activity_code'));
                $department_code = $this->security->xss_clean($this->input->post('department_code'));
                $fund_code = $this->security->xss_clean($this->input->post('fund_code'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'type' => $type,
                    'contact_name' => $contact_name,
                    'contact_number' => $contact_number,
                    'account_code' => $account_code,
                    'activity_code' => $activity_code,
                    'department_code' => $department_code,
                    'fund_code' => $fund_code,
                    'status' => $status,
                    'created_by' => $user_id
                );
                //echo "<Pre>"; print_r($subjectDetails);exit;

                $result = $this->investment_type_model->addNewInvestmentType($data);
                redirect('/investment/investmentType/list');
            }

            $data['fundCodeList'] = $this->investment_type_model->getFundCodeList();
            $data['departmentCodeList'] = $this->investment_type_model->getDepartmentCodeList();
            $data['accountCodeList'] = $this->investment_type_model->getAccountCodeList();
            $data['activityCodeList'] = $this->investment_type_model->getActivityCodeList();



            $this->global['pageTitle'] = 'FIMS : Add Investment Type';
            $this->loadViews("investment_type/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('investment_type.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/investment/investmentType/list');
            }
            if($this->input->post())
            {
               $id_session = $this->session->my_session_id;
                $user_id = $this->session->userId;

                $type = $this->security->xss_clean($this->input->post('type'));
                $contact_name = $this->security->xss_clean($this->input->post('contact_name'));
                $contact_number = $this->security->xss_clean($this->input->post('contact_number'));
                $account_code = $this->security->xss_clean($this->input->post('account_code'));
                $activity_code = $this->security->xss_clean($this->input->post('activity_code'));
                $department_code = $this->security->xss_clean($this->input->post('department_code'));
                $fund_code = $this->security->xss_clean($this->input->post('fund_code'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'type' => $type,
                    'contact_name' => $contact_name,
                    'contact_number' => $contact_number,
                    'account_code' => $account_code,
                    'activity_code' => $activity_code,
                    'department_code' => $department_code,
                    'fund_code' => $fund_code,
                    'status' => $status,
                    'updated_by' => $user_id
                );

                $result = $this->investment_type_model->editInvestmentType($data,$id);
                redirect('/investment/investmentType/list');
            }

            $data['fundCodeList'] = $this->investment_type_model->getFundCodeList();
            $data['departmentCodeList'] = $this->investment_type_model->getDepartmentCodeList();
            $data['accountCodeList'] = $this->investment_type_model->getAccountCodeList();
            $data['activityCodeList'] = $this->investment_type_model->getActivityCodeList();

            $data['investmentType'] = $this->investment_type_model->getInvestmentType($id);

            $this->global['pageTitle'] = 'FIMS : Edit Investment Type';
            $this->loadViews("investment_type/edit", $this->global, $data, NULL);
        }
    }
}
