<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class SchoolDetails extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('school_details_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('school_details.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $data['schoolDetails'] = $this->school_details_model->schoolDetailsList();
            $this->global['pageTitle'] = 'School : School Listing';
            $this->loadViews("school_details/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('school_details.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $reg_no = $this->security->xss_clean($this->input->post('reg_no'));
                $phone_number = $this->security->xss_clean($this->input->post('phone_number'));
                $mobile_number = $this->security->xss_clean($this->input->post('mobile_number'));
                //$email = $this->security->xss_clean($this->input->post('email'));
                $address_one = $this->security->xss_clean($this->input->post('address_one'));
                $address_two = $this->security->xss_clean($this->input->post('address_two'));
                $address_three = $this->security->xss_clean($this->input->post('address_three'));
                $id_state = $this->security->xss_clean($this->input->post('id_state'));
                $id_city = $this->security->xss_clean($this->input->post('id_city'));
                $pincode = $this->security->xss_clean($this->input->post('pincode'));
                $fax = $this->security->xss_clean($this->input->post('fax'));
                $status = $this->security->xss_clean($this->input->post('status'));
                $schoolInfo = array(
                    'name' => $name,
                    'reg_no' => $reg_no,
                    'phone_number' => $phone_number,
                    'mobile_number' => $mobile_number,
                    'id_state' => $id_state,
                    'id_city' => $id_city,
                    'address_one' => $address_one,
                    'address_two' => $address_two,
                    'address_three' => $address_three,
                    'pincode' => $pincode,
                    'status' => $status,
                    'fax' => $fax
                );
                $result = $this->school_details_model->addNewSchoolDetails($schoolInfo);
                redirect('/setup/schoolDetails/list');
            }
            $data['stateList'] = $this->school_details_model->selectStateListList();
            $data['cityList'] = $this->school_details_model->selectCityList();
            //print_r($data['stateList']);exit;
            $this->load->model('school_details_model');
            $this->global['pageTitle'] = 'School : Add New School';
            $this->loadViews("school_details/add", $this->global, $data, NULL);
        }
    }


    function edit($schoolId = NULL)
    {
        if ($this->checkAccess('school_details.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($schoolId == null)
            {
                redirect('/setup/subjectDetails/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $reg_no = $this->security->xss_clean($this->input->post('reg_no'));
                $phone_number = $this->security->xss_clean($this->input->post('phone_number'));
                $mobile_number = $this->security->xss_clean($this->input->post('mobile_number'));
                //$email = $this->security->xss_clean($this->input->post('email'));
                $address_one = $this->security->xss_clean($this->input->post('address_one'));
                $address_two = $this->security->xss_clean($this->input->post('address_two'));
                $address_three = $this->security->xss_clean($this->input->post('address_three'));
                $id_state = $this->security->xss_clean($this->input->post('id_state'));
                $id_city = $this->security->xss_clean($this->input->post('id_city'));
                $pincode = $this->security->xss_clean($this->input->post('pincode'));
                $fax = $this->security->xss_clean($this->input->post('fax'));
                $status = $this->security->xss_clean($this->input->post('status'));
                $schoolInfo = array(
                    'name' => $name,
                    'reg_no' => $reg_no,
                    'phone_number' => $phone_number,
                    'mobile_number' => $mobile_number,
                    'id_state' => $id_state,
                    'id_city' => $id_city,
                    'address_one' => $address_one,
                    'address_two' => $address_two,
                    'address_three' => $address_three,
                    'pincode' => $pincode,
                    'status' => $status,
                    'fax' => $fax
                );
                $result = $this->school_details_model->editSchoolDetails($schoolInfo,$schoolId);
                redirect('/setup/schoolDetails/list');
            }
            $data['schoolDetails'] = $this->school_details_model->getSchoolDetails($schoolId);
            $this->global['pageTitle'] = 'Subject : Edit Subject';
            $this->loadViews("school_details/edit", $this->global, $data, NULL);
        }
    }

    function delete()
    {
        if ($this->checkAccess('school_details.delete') == 1)
        {
            echo (json_encode(array('status' => 'access')));
        }
        else
        {
            $countryId = $this->input->post('countryId');
            $countryInfo = array('isDeleted' => 1, 'updatedBy' => $this->vendorId, 'updatedDtm' => date('Y-m-d H:i:s'));
            $result = $this->school_details_model->deleteCountry($countryId, $countryInfo);
            if ($result > 0)
            {
                echo (json_encode(array('status' => TRUE)));
            }
            else
            {
                echo (json_encode(array('status' => FALSE)));
            }
        }
    }
}
