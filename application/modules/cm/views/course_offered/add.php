<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Course Offered</h3>
        </div>

        
        <form id="form_semester" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Search Course</h4>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Semester <span class='error-text'>*</span></label>
                        <select name="id_sem" id="id_sem" class="form-control" disabled>
                            <option value="">Select</option>
                            <?php
                            if (!empty($semesterList))
                            {
                                foreach ($semesterList as $record)
                                {?>
                                 <option value="<?php echo $record->id;  ?>"
                                    <?php
                                    if ($record->id == $id_semester)
                                    {
                                      echo 'selected';
                                    } ?>
                                    >
                                    <?php echo $record->code . " - " . $record->name;?>
                                 </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>


                <input type='hidden' class='form-control' id='id_semester' name='id_semester' value="<?php echo $id_semester; ?>" >



                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Course Description <span class='error-text'>*</span></label>
                        <select name="id_faculty_program" id="id_faculty_program" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($facultyProgramList))
                            {
                                foreach ($facultyProgramList as $record)
                                {?>
                                 <option value="<?php echo $record->id;  ?>">
                                    <?php echo $record->code . " - " . $record->name;?>
                                 </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

            </div>

            <div class="row">


                <div class="modal-footer">
                    <button type="button" class="btn btn-default" onclick="searchCourseData()">Search</button>
                </div>



            </div>

        </div>



        <div class="form-container" id="display_course_details" style="display: none">
            <h4 class="form-group-title">Course Details</h4>

            <div class="row" id="view_course_details">
            </div>

        </div>



        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="button" class="btn btn-primary btn-lg" onclick="validateSearchData()">Save</button>
                <a href="<?php echo '../edit/'.$id_semester; ?>" class="btn btn-link">Back</a>
            </div>
        </div>



    </form>






        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
    </div>
</div>
<script type="text/javascript">

    $('select').select2();


    $(function ()
    {
        $("#checkAll").click(function ()
        {
            if ($("#checkAll").is(':checked'))
            {
                $(".check").prop("checked", true);
            } else {
                $(".check").prop("checked", false);
            }
        });
    });




    function searchCourseData()
    {
        if($('#form_semester').valid())
        {

        var tempPR = {};
        tempPR['id_semester'] = <?php echo $id_semester; ?>;
        tempPR['id_faculty_program'] = $("#id_faculty_program").val();

            $.ajax(
            {
               url: '/setup/courseOffered/searchCourseData',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                // alert(result);
                if(result != '')
                {    
                    $("#display_course_details").show();
                    $("#view_course_details").html(result);
                }
                else
                {
                    alert('Courses Not Available For Selected Search');
                }
               }
            });
        }

    }


    function validateSearchData()
    {
        if($('#form_semester').valid())
          {
            $('#form_semester').submit();

          }
    }

    $(document).ready(function()
    {
        $("#form_semester").validate(
        {
            rules:
            {
                id_faculty_program:
                {
                    required: true
                }
            },
            messages:
            {
                id_faculty_program:
                {
                    required: "<p class='error-text'>Select Faculty Program</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>