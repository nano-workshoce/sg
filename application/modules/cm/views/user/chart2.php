<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/series-label.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>
<style>
	.highcharts-figure, .highcharts-data-table table {
    min-width: 360px; 
    max-width: 800px;
    margin: 1em auto;
}

.highcharts-data-table table {
	font-family: Verdana, sans-serif;
	border-collapse: collapse;
	border: 1px solid #EBEBEB;
	margin: 10px auto;
	text-align: center;
	width: 100%;
	max-width: 500px;
}
.highcharts-data-table caption {
    padding: 1em 0;
    font-size: 1.2em;
    color: #555;
}
.highcharts-data-table th {
	font-weight: 600;
    padding: 0.5em;
}
.highcharts-data-table td, .highcharts-data-table th, .highcharts-data-table caption {
    padding: 0.5em;
}
.highcharts-data-table thead tr, .highcharts-data-table tr:nth-child(even) {
    background: #f8f8f8;
}
.highcharts-data-table tr:hover {
    background: #f1f7ff;
}

</style>
<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Students Count based on Intake </h3>
        </div>
        <form id="form_award" action="" method="post">

        <div class="form-container">

			<figure class="highcharts-figure">
			    <div id="container"></div>
			    <p class="highcharts-description">
			    </p>
			</figure>
		</div>
	</form>
</div>
</div>
<script type="text/javascript">
Highcharts.chart('container', {
    title: {
        text: 'Combination chart Applicant V/S Lead V/S Student By Intake'
    },
    xAxis: {
        categories: ['SEP-2019', 'JAN-2020', 'JUNE-2020', 'SEP-2020']
    },
    labels: {
        items: [{
            html: 'Total Applicant V/S Lead V/S Student',
            style: {
                left: '50px',
                top: '18px',
                color: ( // theme
                    Highcharts.defaultOptions.title.style &&
                    Highcharts.defaultOptions.title.style.color
                ) || 'black'
            }
        }]
    },
    series: [{
        type: 'column',
        name: 'Applicant',
        data: [100, 160, 210, 230]
    }, {
        type: 'column',
        name: 'Lead',
        data: [78,105,190,176]
    }, {
        type: 'column',
        name: 'Student',
        data: [70,90,130,165]
    }, {
        type: 'pie',
        name: 'Total consumption',
        data: [{
            name: 'Applicant',
            y: 13,
            color: Highcharts.getOptions().colors[0] // Jane's color
        }, {
            name: 'Lead',
            y: 23,
            color: Highcharts.getOptions().colors[1] // John's color
        }, {
            name: 'Students',
            y: 23,
            color: Highcharts.getOptions().colors[2] // John's color
        }],
        center: [100, 80],
        size: 100,
        showInLegend: false,
        dataLabels: {
            enabled: false
        }
    }]
});

</script>