<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Allumini_model extends CI_Model
{
    function applicantList($applicantList)
    {
        $this->db->select('a.*, i.name as intake, p.code as program_code, p.name as program');
        $this->db->from('student as a');
        $this->db->join('intake as i', 'a.id_intake = i.id');
        $this->db->join('programme as p', 'a.id_program = p.id');
        if($applicantList['first_name']) {
            $likeCriteria = "(a.full_name  LIKE '%" . $applicantList['first_name'] . "%')";
            $this->db->where($likeCriteria);
        }
        //  if($applicantList['last_name']) {
        //     $likeCriteria = "(a.last_name  LIKE '%" . $applicantList['last_name'] . "%')";
        //     $this->db->where($likeCriteria);
        // }
        if($applicantList['email_id'])
        {
            $likeCriteria = "(a.email_id  LIKE '%" . $applicantList['email_id'] . "%')";
            $this->db->where($likeCriteria);
        }
        if($applicantList['nric'])
        {
            $likeCriteria = "(a.nric  LIKE '%" . $applicantList['nric'] . "%')";
            $this->db->where($likeCriteria);
        }
         if($applicantList['id_program'])
        {
            $this->db->where('a.id_program', $applicantList['id_program']);
        }
        if($applicantList['id_intake'])
        {
            $this->db->where('a.id_intake', $applicantList['id_intake']);
        }
        if($applicantList['applicant_status'])
        {
            $this->db->where('a.applicant_status', $applicantList['applicant_status']);
        }
        if($applicantList['id_award'])
        {
            $this->db->where('p.id_award', $applicantList['id_award']);
        }
        $this->db->where('a.applicant_status =', 'Graduated');
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }


    function getOrganisation()
    {
        $this->db->select('*');
        $this->db->from('organisation');
        $this->db->order_by("id", "ASC");
        $query = $this->db->get();
        return $query->row();
    }

    function countryListByActivity($status)
    {
    	$this->db->select('a.*');
        $this->db->from('country as a');
		$this->db->where('a.status', $status);
        $this->db->order_by("name", "ASC");
		$query = $this->db->get();
		$result = $query->result();
		return $result;
    }


    function staffListByActivity($status)
    {
        $this->db->select('a.*');
        $this->db->from('staff as a');
		$this->db->where('a.status', $status);
        $this->db->order_by("name", "ASC");
		$query = $this->db->get();
		$result = $query->result();
		return $result;
    }

    function awardListByStatus($status)
    {
        $this->db->select('a.*');
        $this->db->from('award as a');
        $this->db->where('a.status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function editOrganisation($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('organisation', $data);
        return TRUE;
    }
}

