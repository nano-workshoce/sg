<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Course_registration_model extends CI_Model
{

    function getPerogramLandscape($id_intake,$id_programme,$id_program_scheme,$id_program_landscape,$student_semester)
    {
        $id_student = $this->session->id_student;
        // $student_semester = $this->session->student_semester;

        $this->db->select('DISTINCT(a.id) as id, a.id_semester, c.name , c.code');
        $this->db->from('add_course_to_program_landscape as a');
        $this->db->join('intake as in', 'a.id_intake = in.id');
        $this->db->join('course as c', 'a.id_course = c.id');
        $this->db->join('programme_landscape as pl', 'a.id_program_landscape = pl.id');
        $this->db->where('a.id_intake', $id_intake);
        $this->db->where('pl.status', '1');
        $this->db->where('pl.id_programme', $id_programme);
        // $this->db->where('a.id_program_scheme', $id_program_scheme);
        $this->db->where('a.id_program_landscape', $id_program_landscape);
        $this->db->where('a.id_semester <=', $student_semester);
        
        $likeCriteria = " a.id NOT IN (SELECT cr.id_course_registered_landscape FROM course_registration cr where cr.id_student = " . $id_student . ")";
        $this->db->where($likeCriteria);                

        $query = $this->db->get();
         $result = $query->result();
         return $result; 
    }

    function getCoursePerogramLandscape($id)
    {
        $this->db->select('a.id, b.pre_requisite, c.name as courseName, pl.name as plName, pl.min_total_cr_hrs');

        $this->db->from('courses_from_programme_landscape as a');
        $this->db->join('add_course_to_program_landscape as b', 'a.id_course_programme = b.id');
        $this->db->join('course as c', 'b.id_course = c.id');
        $this->db->join('programme_landscape as pl', 'b.id_program_landscape = pl.id');
        $this->db->where('a.id_course_registration', $id);
        $query = $this->db->get();
         $result = $query->result();
         return $result; 
    }

    function getCourseRegisteredList($id_student,$id_intake,$id_program,$id_qualification,$id_program_scheme)
    {
        $this->db->select('DISTINCT(cr.id_course) as id_course, cr.*, rl.name as role, sem.code as semester_code, sem.name as semester_name, lct.name as course_type');
        $this->db->from('course_registration as cr');
        $this->db->join('course_register as creg', 'cr.id_course_register = creg.id');
        // $this->db->join('course as c', 'cr.id_course = c.id');
        $this->db->join('semester as sem', 'cr.id_semester = sem.id');
        $this->db->join('add_course_to_program_landscape as acpl', 'cr.id_course_registered_landscape = acpl.id');
        $this->db->join('landscape_course_type as lct', 'acpl.course_type = lct.id','left');
        // $this->db->join('programme_landscape as pl', 'acpl.id_program_landscape = pl.id');
        $this->db->join('users as rl', 'cr.created_by = rl.id','left');
        $this->db->where('cr.id_student', $id_student);
        // $this->db->where('cr.id_program_scheme', $id_program_scheme);
        $this->db->where('creg.id_intake', $id_intake);
        $this->db->where('creg.id_programme', $id_program);
        // $this->db->where('acpl.id_qualification', $id_qualification);
        $this->db->where('cr.is_exam_registered', '0');
        $this->db->where('cr.is_bulk_withdraw', '0');
        $query = $this->db->get();
        
        $results = $query->result();
        
         // echo "<Pre>";print_r($results);exit();
         
        $details = array();

        foreach ($results as $result)
        {
            $id_course = $result->id_course;

            $course = $this->getCourse($id_course);

            if($course)
            {
                $result->course_code = $course->code;
                $result->course_name = $course->name;
                $result->credit_hours = $course->credit_hours;

                array_push($details, $result);
            }
        }

        return $details;
    }

    function getCourse($id)
    {
        $this->db->select('*');
        $this->db->from('course');
        $this->db->where('id', $id);
        $query = $this->db->get();
        $result = $query->row();
        return $result;
    }

    // function getCoursesByProgramNIntakeNStudent($id_intake,$id_programme,$id_student)
    // {
    //      // print_r($id_intake);exit();
    //     $this->db->select('cou.name, cou.name_in_malay, cou.code, acpl.pre_requisite, pl.min_total_cr_hrs');
    //     $this->db->from('course_registration as a');
    //     $this->db->join('course as cou', 'a.id_course = cou.id');
    //     $this->db->join('add_course_to_program_landscape as acpl', 'acpl.id_course = cou.id');
    //     $this->db->join('programme_landscape as pl', 'acpl.id_program_landscape = pl.id');
    //     $this->db->where('pl.id_programme', $id_programme);
    //     $this->db->where('pl.id_intake', $id_intake);
    //     $this->db->where('a.id_programme', $id_programme);
    //     $this->db->where('a.id_intake', $id_intake);
    //     $this->db->where('a.id_student', $id_student);
    //     $this->db->where('a.is_exam_registered', '0');
    //     $this->db->where('a.is_bulk_withdraw', '0');
    //      $query = $this->db->get();
    //      $result = $query->result();
    //      return $result;
    // }

    function deleteCourseRegistration($id_course_registration)
    {
         $this->db->where('id', $id_course_registration);
        $this->db->delete('course_registration');
        return TRUE;
    }

    function addCoureRegistration($data)
    {
        $this->db->trans_start();
        $this->db->insert('course_registration', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getCourseFromExamRegister($id_intake,$id_programme,$id_student)
    {
      $status = '0';
        $this->db->select('DISTINCT(c.id) as id_course, cr.id, in.name as intake_name, c.code as course_code, c.name as course_name, exc.name as exam_center_name, exc.address as exam_center_address');
        $this->db->from('exam_registration as cr');
        $this->db->join('intake as in', 'cr.id_intake = in.id');
        $this->db->join('course as c', 'cr.id_course = c.id');
        $this->db->join('exam_center as exc', 'cr.id_exam_center = exc.id');
        $this->db->where('cr.id_intake', $id_intake);
        $this->db->where('cr.id_student', $id_student);
        $this->db->where('cr.id_programme', $id_programme);
        $this->db->where('cr.is_bulk_withdraw', $status);
        // $this->db->where('cr.is_exam_registered', $status);
        $query = $this->db->get();
          $result = $query->result();
         return $result; 
    }

    function bulkWithdrawList($id_student)
    {
        $this->db->select('DISTINCT(cr.id_course) as id_course, cr.id, cr.reason, cr.created_dt_tm, in.id as id_intake,  in.name as intake_name, stu.full_name, stu.nric, stu.email_id, p.name as programme_name, p.code as programme_code, p.id as id_programme, c.code as course_code, c.name as course_name');
        $this->db->from('bulk_withdraw as cr');
        $this->db->join('intake as in', 'cr.id_intake = in.id');
        $this->db->join('course as c', 'cr.id_course = c.id');
        $this->db->join('student as stu', 'cr.id_student = stu.id');
        $this->db->join('programme as p', 'cr.id_programme = p.id');
        // $this->db->join('exam_center as exc', 'cr.id_exam_center = exc.id','left');
         $this->db->where('cr.id_student', $id_student);
        $query = $this->db->get();
          $result = $query->result();
         return $result; 
    }

    function addBulkWithdraw($data)
    {
        $this->db->trans_start();
        $this->db->insert('bulk_withdraw', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getExamRegistration($id)
    {
        $this->db->select('*');
        $this->db->from('exam_registration');
         $this->db->where('id', $id);
        $query = $this->db->get();
          $result = $query->row();
         return $result; 
    }

    function updateExamRegistration($data,$id)
    {
      $this->db->where_in('id', $id);
      $this->db->update('exam_registration', $data);
    }

    function getRole($id)
    {
        $this->db->select('*');
        $this->db->from('roles');
        $this->db->where('id', $id);
        $query = $this->db->get();
            // echo "<pre>";print_r($query);die;
        
        return $query->row();
    }

    function addCoureRegister($data)
    {
        $this->db->trans_start();
        $this->db->insert('course_register', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getCoursesIdByLandscapeIdForDetailsAdd($id)
    {
        $this->db->select('s.*');
        $this->db->from('add_course_to_program_landscape as s');
        $this->db->where('s.id', $id);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function semesterListByStatus($status)
    {
        $this->db->select('s.*');
        $this->db->from('semester as s');
        $this->db->where('s.status', $status);
        $query = $this->db->get();
        $result = $query->result(); 

        return$result;
    }

    function getCourseRegisteredByStudentIdForWithdraw($id_intake,$id_programme,$id_student)
    {
        $this->db->select('c.*,cou.code as course_code, cou.name as course_name, sem.code as semester_code, sem.name as semester_name');
        $this->db->from('course_registration as c');
        $this->db->join('course as cou', 'c.id_course = cou.id'); 
        $this->db->join('semester as sem', 'c.id_semester = sem.id');
        $this->db->where('c.id_intake', $id_intake);
        $this->db->where('c.id_programme', $id_programme);
        $this->db->where('c.id_student', $id_student);
        $this->db->where('c.is_bulk_withdraw', 0);
        $this->db->where('c.is_exam_registered', 0);
        $this->db->where('c.total_result', '');
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();
         return $result;
    }

    function getCourseRegisteredByIdRegistration($id)
    {
        $this->db->select('c.*, s.course_type');
        $this->db->from('course_registration as s');
        $this->db->join('course as c', 's.id_course = c.id'); 
        $this->db->where('s.id', $id);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function semesterListByIdStudentFromCourseRegister($id_student)
    {
        $this->db->select('DISTINCT(s.id_semester) as id_semester');
        $this->db->from('course_register as s');
        // $this->db->join('course as c', 's.id_course = c.id'); 
        $this->db->where('s.id_student', $id_student);
        $query = $this->db->get();
        $results = $query->result();


        // echo "<Pre>";print_r($semesters);exit();

        $details = array();
        foreach ($results as $result)
        {
            $id_semester = $result->id_semester;
            $semester = $this->getSemester($id_semester);
            if($semester)
            {
                array_push($details, $semester);
            }
        }

        return $details;
    }

    function getSemester($id)
    {
        $this->db->select('s.*');
        $this->db->from('semester as s');
        $this->db->where('s.id', $id);
        $query = $this->db->get();
        $results = $query->row();
        return $results;
    }

    function getCourseRegisteredBySemesterNStudentId($id_student,$id_semester)
    {
        $this->db->select('c.*,cou.code as course_code, cou.name as course_name');
        $this->db->from('course_registration as c');
        $this->db->join('course as cou', 'c.id_course = cou.id'); 
        $this->db->where('c.id_semester', $id_semester);
        $this->db->where('c.id_student', $id_student);
        $this->db->where('c.is_bulk_withdraw', 0);
        $this->db->where('c.is_exam_registered', 0);
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();
         return $result;
    }

    function getCourseRegisteredLandscape($id)
    {
        $this->db->select('c.*, s.course_type');
        $this->db->from('add_course_to_program_landscape as s');
        $this->db->join('course as c', 's.id_course = c.id'); 
        $this->db->where('s.id', $id);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function getCourseRegistration($id)
    {
        $this->db->select('s.*');
        $this->db->from('course_registration as s'); 
        $this->db->where('s.id', $id);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function addBulkWithdrawMaster($data)
    {
        $this->db->trans_start();
        $this->db->insert('bulk_withdraw_master', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getFeeStructureActivityType($type,$trigger,$id_program)
    {
        $this->db->select('s.*');
        $this->db->from('fee_structure_activity as s');
        $this->db->join('activity_details as a', 's.id_activity = a.id');
        $this->db->where('a.name', $type);
        $this->db->where('s.trigger', $trigger);
        $this->db->where('s.id_program', $id_program);
        $this->db->where('s.status', 1);
        $this->db->order_by('s.id', 'DESC');
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }


    function generateMainInvoice($data,$id_apply_change_status)
    {
        // $user_id = $this->session->userId;


        $id_student = $data['id_student'];
        $add = $data['add'];

        $student_data = $this->getStudent($id_student);

        $nationality = $student_data->nationality;
        $id_program = $student_data->id_program;
        $id_intake = $student_data->id_intake;

        if($add == 1)
        {
            $fee_structure_data = $this->getFeeStructureActivityType('COURSE WITHDRAW','Application Level',$id_program);
        }
        elseif($add == 0)
        {
            $fee_structure_data = $this->getFeeStructureActivityType('COURSE WITHDRAW','Approval Level',$id_program);
        }

        if($fee_structure_data)
        {

            if($nationality == 'Malaysian')
            {
                $currency = 'MYR';
                $invoice_amount = $fee_structure_data->amount_local;
            }
            elseif($nationality == 'Other')
            {
                $currency = 'USD';
                $invoice_amount = $fee_structure_data->amount_international;
            }


        

            $invoice_number = $this->generateMainInvoiceNumber();


            $invoice['invoice_number'] = $invoice_number;
            $invoice['type'] = 'Student';
            $invoice['remarks'] = 'Student Course Withdraw';
            $invoice['id_application'] = '0';
            $invoice['id_program'] = $id_program;
            $invoice['id_intake'] = $id_intake;
            $invoice['id_student'] = $id_student;
            $invoice['currency'] = $currency;
            $invoice['total_amount'] = $invoice_amount;
            $invoice['invoice_total'] = $invoice_amount;
            $invoice['balance_amount'] = $invoice_amount;
            $invoice['paid_amount'] = '0';
            $invoice['status'] = '1';
            $invoice['created_by'] = 0;

            // $fee_structure_data = $this->getFeeStructure($id_program,$id_intake,$id_program_scheme);

            
            // $update = $this->editStudentData($id_program_scheme,$id_program,$id_intake,$id_student);
            
            $inserted_id = $this->addNewMainInvoice($invoice);

            if($inserted_id)
            {
                $data = array(
                        'id_main_invoice' => $inserted_id,
                        'id_fee_item' => $fee_structure_data->id_fee_setup,
                        'amount' => $invoice_amount,
                        'status' => 1,
                        'quantity' => 1,
                        'price' => $invoice_amount,
                        'id_reference' => $id_apply_change_status,
                        'description' => 'COURSE WITHDRAW',
                        'created_by' => 0
                    );

                $this->addNewMainInvoiceDetails($data);
            }

        }
        return TRUE;
    }

    function generateMainInvoiceNumber()
    {
        $year = date('y');
        $Year = date('Y');
            $this->db->select('j.*');
            $this->db->from('main_invoice as j');
            $this->db->order_by("id", "desc");
            $query = $this->db->get();
            $result = $query->num_rows();

     
            $count= $result + 1;
           $jrnumber = $number = "INV" .(sprintf("%'06d", $count)). "/" . $Year;
           return $jrnumber;        
    }

    function getStudent($id_student)
    {
        $this->db->select('s.*');
        $this->db->from('student as s');
        $this->db->where('s.id', $id_student);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function getPartnerUniversity($id)
    {
        $this->db->select('s.*');
        $this->db->from('partner_university as s');
        $this->db->where('s.id', $id);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function updateCourseRegistration($data,$id)
    {
      $this->db->where_in('id', $id);
      $this->db->update('course_registration', $data);
      return TRUE;
    }

    function generateNewMainInvoiceForCourseRegistration($datas,$id_student,$id_semester)
    {

        // echo "<Pre>";print_r($id_semester);exit();
        
        $user_id = $this->session->userId;
        $student_data = $this->getStudent($id_student);

        // echo "<Pre>";print_r($student_data);exit();

        $id_student = $student_data->id;
        $id_program = $student_data->id_program;
        $id_intake = $student_data->id_intake;
        $nationality = $student_data->nationality;
        $id_program_scheme = $student_data->id_program_scheme;
        $id_program_has_scheme = $student_data->id_program_has_scheme;
        $id_university = $student_data->id_university;
        $id_branch = $student_data->id_branch;
        $current_semester = $student_data->current_semester;
        $id_program_landscape = $student_data->id_program_landscape;
        $id_fee_structure = $student_data->id_fee_structure;
        $student_current_semester = $student_data->current_semester;

        // echo "<Pre>";print_r($student_data);exit();

        $get_data['id_intake'] = $id_intake;
        $get_data['id_programme'] = $id_program;
        $get_data['id_programme_scheme'] = $id_program_scheme;
        $get_data['id_program_has_scheme'] = $id_program_has_scheme;

        // $programme_landscape = $this->getProgramLandscapeByData($get_data);


        if($id_fee_structure != 0)
        {
            // echo "<Pre>";print_r($id_branch);exit();

            $trigger = 'COURSE REGISTRATION';
            $is_installment = 0;
            $installments = 0;

            // Hided university == 1 && For Per Semester Based Fee Generation 
                // $id_university == 1 && 
            if($id_university == 1)
            {


                if($nationality == 'Malaysian')
                {
                    $currency = 'MYR';

                    // $fix_amount_data = $this->getFeeStructure($id_program,$id_intake,$id_program_scheme,'MYR','FIX AMOUNT');
                    $fix_amount_data = $this->getFeeStructure($id_fee_structure,'MYR','FIX AMOUNT',$id_university,$trigger);
                    $subject_multiplier_amount_data = $this->getFeeStructure($id_fee_structure,'MYR','SUBJECT MULTIPLICATION',$id_university,$trigger);
                    $cr_hr_multiflier_amount_data = $this->getFeeStructure($id_fee_structure,'MYR','CREDIT HOUR MULTIPLICATION',$id_university,$trigger);
                }
                elseif($nationality == 'Other')
                {
                    $currency = 'USD';
                    $fix_amount_data = $this->getFeeStructure($id_fee_structure,'USD','FIX AMOUNT',$id_university,$trigger);
                    $subject_multiplier_amount_data = $this->getFeeStructure($id_fee_structure,'USD','SUBJECT MULTIPLICATION',$id_university,$trigger);
                    $cr_hr_multiflier_amount_data = $this->getFeeStructure($id_fee_structure,'USD','CREDIT HOUR MULTIPLICATION',$id_university,$trigger);
                }



                // echo "<Pre>";print_r($fix_amount_data);exit();


                if(isset($fix_amount_data) || isset($subject_multiplier_amount_data) || isset($cr_hr_multiflier_amount_data))
                {

                    // echo "<Pre>";print_r($cr_hr_multiflier_amount_data);exit;

                
                    $invoice_number = $this->generateMainInvoiceNumber();

                    $invoice['invoice_number'] = $invoice_number;
                    $invoice['type'] = 'Student';
                    $invoice['remarks'] = 'Student Course Registration';
                    $invoice['id_application'] = '';
                    $invoice['id_student'] = $id_student;
                    $invoice['id_program'] = $id_program;
                    $invoice['id_intake'] = $id_intake;
                    $invoice['currency'] = $currency;
                    $invoice['invoice_total'] = 0;
                    $invoice['total_amount'] = 0;
                    $invoice['balance_amount'] = 0;
                    $invoice['paid_amount'] = 0;
                    $invoice['status'] = '1';
                    $invoice['created_by'] = $user_id;
                    
                    // echo "<Pre>";print_r($detail_data);exit;
                    $inserted_id = $this->addNewMainInvoice($invoice);



                    $total = 0;


                    if($fix_amount_data)
                    {
                        $detail_data = array(
                            'id_main_invoice' => $inserted_id,
                            'id_fee_item' => $fix_amount_data->id_fee_item,
                            'amount' => $fix_amount_data->amount,
                            'price' => $fix_amount_data->amount,
                            'quantity' => 1,
                            'description' => 'FIX AMOUNT',
                            'status' => '1',
                            'created_by' => $user_id
                        );

                        $this->addNewMainInvoiceDetails($detail_data);

                        $total = $total + $fix_amount_data->amount;
                    }

                    if($subject_multiplier_amount_data)
                    {
                        $count = count($datas);

                        $amount = $count * $subject_multiplier_amount_data->amount;

                        $detail_data = array(
                            'id_main_invoice' => $inserted_id,
                            'id_fee_item' => $subject_multiplier_amount_data->id_fee_item,
                            'amount' => $amount,
                            'price' => $subject_multiplier_amount_data->amount,
                            // 'quantity' => 1,
                            'quantity' => $count,
                            'description' => 'SUBJECT MULTIPLICATION',
                            'status' => '1',
                            'created_by' => $user_id
                        );

                        $this->addNewMainInvoiceDetails($detail_data);
                        $total = $total + $amount;

                    }


                    if($cr_hr_multiflier_amount_data)
                    {
                        foreach ($datas as $data)
                        {
                           
                        
                            $id_course = $data['id_course'];
                            $id_course_registered = $data['id_course_registered'];

                            $course_data = $this->getCourse($id_course);

                            $credit_hours = $course_data->credit_hours;

                            $detail_total = $credit_hours * $cr_hr_multiflier_amount_data->amount;
                       
                            $data = array(
                                    'id_main_invoice' => $inserted_id,
                                    'id_fee_item' => $cr_hr_multiflier_amount_data->id_fee_item,
                                    'amount' => $detail_total,
                                    'price' => $cr_hr_multiflier_amount_data->amount,
                                    'quantity' => 1,
                                    'id_reference' => $id_course_registered,
                                    'description' => 'CREDIT HOUR MULTIPLICATION',
                                    'status' => '1',
                                    'created_by' => $user_id
                                );

                            $total = $total + $detail_total;

                            $this->addNewMainInvoiceDetails($data);

                        }
                    }

                    $invoice_update['total_amount'] = $total;
                    $invoice_update['balance_amount'] = $total;
                    $invoice_update['invoice_total'] = $total;
                    $invoice_update['total_discount'] = 0;
                    $invoice_update['paid_amount'] = 0;
                    // $invoice_update['inserted_id'] = $inserted_id;
                    // echo "<Pre>";print_r($invoice_update);exit;
                    $this->editMainInvoice($invoice_update,$inserted_id);

                }


            }



            // echo "<Pre>";print_r($installments);exit();

            if($id_university > 1)
            {
                $currency = 'USD';

                $fee_structure_data = $this->getFeeStructureByTrainingCenterForInvoiceGeneration($id_program,$id_intake,$id_fee_structure,$id_university,'COURSE REGISTRATION');

                // $fee_structure_training_data = $this->getFeeStructureInstallmentByDataNSemester($id_programme_landscape,$id_university,$current_semester);

                // $count_fee_struture = count($fee_structure_data);

                // echo "<Pre>";print_r($count_fee_struture);exit();

                $count_installment_fee = 0;
                $count_per_semester_fee = 0;

                foreach ($fee_structure_data as $fee_structure)
                {
                    $currency = $fee_structure->currency;
                    $is_installment = $fee_structure->is_installment;
                    $id_training_center = $fee_structure->id_training_center;
                    $trigger_name = $fee_structure->trigger_name;
                    $installments = $fee_structure->installments;

                    $instllment_data['id_fee_structure'] = $fee_structure->id;
                    $instllment_data['trigger_code'] = 'COURSE REGISTRATION';
                    $instllment_data['id_fee_structure_master'] = $fee_structure->id_program_landscape;


                    if($is_installment == 1)
                    {
                        $installment_details = $this->getTrainingCenterInstallmentDetails($instllment_data);

                        if($installment_details)
                        {
                            foreach ($installment_details as $installment_detail)
                            {
                                
                                $installment_trigger_name = $installment_detail->trigger_name;
                                $trigger_semester = $installment_detail->id_semester;


                                if($installment_trigger_name == 'COURSE REGISTRATION' && $trigger_semester == $student_current_semester)
                                {
                                    $count_installment_fee ++;   
                                }
                            }
                        }
                    }
                    else
                    {
                        if($trigger_name == 'COURSE REGISTRATION')
                        {
                            $count_per_semester_fee ++;    
                        }
                    }
                }

                    // echo "<Pre>";print_r($count_per_semester_fee . '1' . $count_installment_fee);exit();
                    // echo "<Pre>";print_r('1');exit();

                if(!empty($fee_structure_data) && ($count_per_semester_fee > 0 || $count_installment_fee > 0))
                {

                    $amount = 0;

                    $invoice_number = $this->generateMainInvoiceNumber();

                    $invoice['invoice_number'] = $invoice_number;
                    $invoice['type'] = 'Student';
                    $invoice['remarks'] = 'Student Course Registration Fee';
                    $invoice['id_application'] = '';
                    $invoice['id_student'] = $id_student;
                    $invoice['id_program'] = $id_program;
                    $invoice['id_intake'] = $id_intake;
                    $invoice['currency'] = $currency;
                    $invoice['invoice_total'] = $amount;
                    $invoice['total_amount'] = $amount;
                    $invoice['balance_amount'] = $amount;
                    $invoice['paid_amount'] = 0;
                    $invoice['status'] = '1';
                    $invoice['created_by'] = $user_id;
                    
                    // echo "<Pre>";print_r($detail_data);exit;
                    $inserted_id = $this->addNewMainInvoice($invoice);


                    if($inserted_id)
                    {
                        $invoice_total_amount = 0;

                        foreach ($fee_structure_data as $fee_structure)
                        {
                            $is_installment = $fee_structure->is_installment;
                            $id_training_center = $fee_structure->id_training_center;
                            $trigger_name = $fee_structure->trigger_name;
                            $installments = $fee_structure->installments;

                            $instllment_data['id_fee_structure'] = $fee_structure->id;
                            $instllment_data['trigger_code'] = 'COURSE REGISTRATION';
                            $instllment_data['id_fee_structure_master'] = $fee_structure->id_program_landscape;



                            $total_amount = 0;

                            if($is_installment == 1)
                            {

                                $installment_details = $this->getTrainingCenterInstallmentDetails($instllment_data);

                                if($installment_details)
                                {
                                    foreach ($installment_details as $installment_detail)
                                    {
                                        
                                        $installment_trigger_name = $installment_detail->trigger_name;
                                        $trigger_semester = $installment_detail->id_semester;


                                        if($installment_trigger_name == 'COURSE REGISTRATION' && $trigger_semester == $student_current_semester)
                                        {

                                            $data = array(
                                                'id_main_invoice' => $inserted_id,
                                                'id_fee_item' => $installment_detail->id_fee_item,
                                                'amount' => $installment_detail->amount,
                                                'price' => $installment_detail->amount,
                                                'quantity' => 1,
                                                'id_reference' => $installment_detail->id,
                                                'description' => 'Student Course Registration Installment Trigger Fee',
                                                'status' => 1,
                                                'created_by' => $user_id
                                            );

                                            $total_amount = $total_amount + $installment_detail->amount;
                            
                                            $this->addNewMainInvoiceDetails($data);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if($trigger_name == 'COURSE REGISTRATION')
                                {
                                    $data = array(
                                        'id_main_invoice' => $inserted_id,
                                        'id_fee_item' => $fee_structure->id_fee_item,
                                        'amount' => $fee_structure->amount,
                                        'price' => $fee_structure->amount,
                                        'quantity' => 1,
                                        'id_reference' => $fee_structure->id,
                                        'description' => 'Student Course Registration Per Semester Trigger Fee',
                                        'status' => 1,
                                        'created_by' => $user_id
                                    );

                                    $total_amount = $total_amount + $fee_structure->amount;

                                    $this->addNewMainInvoiceDetails($data);
                                }
                            }

                            $invoice_total_amount = $invoice_total_amount + $total_amount;
                        }



                        $invoice_update['total_amount'] = $invoice_total_amount;
                        $invoice_update['balance_amount'] = $invoice_total_amount;
                        $invoice_update['invoice_total'] = $invoice_total_amount;
                        $invoice_update['total_discount'] = 0;
                        $invoice_update['paid_amount'] = 0;

                        $updated_invoice = $this->editMainInvoice($invoice_update,$inserted_id);
                    }

                }

            }

        }
        return TRUE;
    }

    function getFeeStructure($id_program_landscape,$currency,$code,$id_training_center,$trigger_name)
    {
       $this->db->select('p.*, fstp.name as trigger_name');
        $this->db->from('fee_structure as p');
        $this->db->join('fee_setup as fs', 'p.id_fee_item = fs.id'); 
        $this->db->join('frequency_mode as fm', 'fs.id_frequency_mode = fm.id'); 
        $this->db->join('amount_calculation_type as amt', 'fs.id_amount_calculation_type = amt.id'); 
        $this->db->join('fee_structure_triggering_point as fstp', 'p.id_fee_structure_trigger = fstp.id'); 
        // $this->db->where('p.id_programme', $id_programme);
        // $this->db->where('p.id_intake', $id_intake);
        // $this->db->where('p.id_program_scheme', $id_program_scheme);
        $this->db->where('p.id_program_landscape', $id_program_landscape);
        $this->db->where('p.currency', $currency);
        $this->db->where('p.id_training_center', $id_training_center);
        $this->db->where('fstp.name', $trigger_name);
        $this->db->where('fm.code', 'PER SEMESTER');
        $this->db->where('amt.code', $code);
        $this->db->order_by('p.id', 'DESC');
        $query = $this->db->get();
        $fee_structure = $query->row();
        return $fee_structure;
    }

    function addNewMainInvoice($data)
    {
        $this->db->trans_start();
        $this->db->insert('main_invoice', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function addNewMainInvoiceDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('main_invoice_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function editMainInvoice($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('main_invoice', $data);
        return TRUE;
    }

    function getFeeStructureByTrainingCenterForInvoiceGeneration($id_programme,$id_intake,$id_fee_structure,$id_training_center,$trigger_code)
    {
        $this->db->select('p.*, fstp.name as trigger_name');
        $this->db->from('fee_structure as p');
        $this->db->join('fee_setup as fs', 'p.id_fee_item = fs.id','left'); 
        $this->db->join('frequency_mode as fm', 'fs.id_frequency_mode = fm.id','left'); 
        $this->db->join('fee_structure_triggering_point as fstp', 'p.id_fee_structure_trigger = fstp.id','left');
        // $this->db->where('p.id_programme', $id_programme);
        // $this->db->where('p.id_intake', $id_intake);
        $this->db->where('p.id_program_landscape', $id_fee_structure);
        $this->db->where('p.id_training_center', $id_training_center);
        $query = $this->db->get();
        $fee_structures = $query->result();

        $details = array();

        foreach ($fee_structures as $fee_structure)
        {
            $is_installment = $fee_structure->is_installment;
            $trigger_name = $fee_structure->trigger_name;

            if($is_installment == 0)
            {
                if($trigger_name == $trigger_code)
                {
                    array_push($details, $fee_structure);
                }
            }
            else
            {
                array_push($details, $fee_structure);
            }
        }

        return $details;
    }

    function getTrainingCenterInstallmentDetails($data)
    {
        $this->db->select('p.*, sem.name as fee_name, sem.code as fee_code, fm.name as frequency_mode, fstp.name as trigger_name');
        $this->db->from('fee_structure_has_training_center as p');
        $this->db->join('fee_setup as sem', 'p.id_fee_item = sem.id');
        $this->db->join('frequency_mode as fm', 'sem.id_frequency_mode = fm.id');
        $this->db->join('fee_structure_triggering_point as fstp', 'p.id_fee_structure_trigger = fstp.id');
        $this->db->where('p.id_fee_structure', $data['id_fee_structure']);
        $this->db->where('p.id_program_landscape', $data['id_fee_structure_master']);
        $query = $this->db->get(); 
        $results = $query->result();  
        
        $details = array();

        foreach ($results as $result)
        {
            $trigger_name = $result->trigger_name;

            if($trigger_name == $data['trigger_code'])
            {
                array_push($details, $result);
            }
            
        }
        return $details;
    }
}