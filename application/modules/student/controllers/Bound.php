<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Bound extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('bound_model');
        $this->isStudentLoggedIn();
    }

    function list()
    {       
        $id_student = $this->session->id_student;
        $id_intake = $this->session->id_intake;
        $id_program = $this->session->id_program;
        $id_qualification = $this->session->id_qualification;

        
        $data['studentDetails'] = $this->bound_model->getStudentByStudentId($id_student);
        $supervisor = $this->bound_model->getSupervisor($data['studentDetails']->id_supervisor);

        if($supervisor)
        {
            $data['supervisor'] = $supervisor;
        }
        else
        {
            $data['supervisor'] = array();
        }

        $data['boundList'] = $this->bound_model->getBoundByStudentId($id_student);

        // echo "<Pre>";print_r($data['boundList']);exit();

        $this->global['studentPageCode'] = 'bound.list';
        $this->global['pageTitle'] = 'Student Portal : List Bound';
        $this->loadViews("bound/list", $this->global, $data, NULL);
    }
    
    function add()
    {

        $id_student = $this->session->id_student;
        $id_intake = $this->session->id_intake;
        $id_program = $this->session->id_program;
        $id_qualification = $this->session->id_qualification;

        if($this->input->post())
        {
            // echo "<Pre>"; print_r($this->input->post());exit;
            $description = $this->security->xss_clean($this->input->post('description'));
            $status = $this->security->xss_clean($this->input->post('status'));

            $student = $this->bound_model->getStudent($id_student);


            if($_FILES['upload_file'])
            {
            // echo "<Pre>"; print_r($_FILES['image']);exit;

                $certificate_name = $_FILES['upload_file']['name'];
                $certificate_size = $_FILES['upload_file']['size'];
                $certificate_tmp =$_FILES['upload_file']['tmp_name'];
                
                // echo "<Pre>"; print_r($certificate_tmp);exit();

                $certificate_ext=explode('.',$certificate_name);
                $certificate_ext=end($certificate_ext);
                $certificate_ext=strtolower($certificate_ext);


                $this->fileFormatNSizeValidation($certificate_ext,$certificate_size,'Image File');

                $upload_file = $this->uploadFile($certificate_name,$certificate_tmp,'Image File');
            }


            if($_FILES['upload_file2'])
            {
            // echo "<Pre>"; print_r($_FILES['image']);exit;

                $certificate_name = $_FILES['upload_file2']['name'];
                $certificate_size = $_FILES['upload_file2']['size'];
                $certificate_tmp =$_FILES['upload_file2']['tmp_name'];
                
                // echo "<Pre>"; print_r($certificate_tmp);exit();

                $certificate_ext=explode('.',$certificate_name);
                $certificate_ext=end($certificate_ext);
                $certificate_ext=strtolower($certificate_ext);


                $this->fileFormatNSizeValidation($certificate_ext,$certificate_size,'Image File');

                $upload_file2 = $this->uploadFile($certificate_name,$certificate_tmp,'Image File');
            }


            if($_FILES['upload_file3'])
            {
            // echo "<Pre>"; print_r($_FILES['image']);exit;

                $certificate_name = $_FILES['upload_file3']['name'];
                $certificate_size = $_FILES['upload_file3']['size'];
                $certificate_tmp =$_FILES['upload_file3']['tmp_name'];
                
                // echo "<Pre>"; print_r($certificate_tmp);exit();

                $certificate_ext=explode('.',$certificate_name);
                $certificate_ext=end($certificate_ext);
                $certificate_ext=strtolower($certificate_ext);


                $this->fileFormatNSizeValidation($certificate_ext,$certificate_size,'Image File');

                $upload_file3 = $this->uploadFile($certificate_name,$certificate_tmp,'Image File');
            }


            if($_FILES['upload_file4'])
            {
            // echo "<Pre>"; print_r($_FILES['image']);exit;

                $certificate_name = $_FILES['upload_file4']['name'];
                $certificate_size = $_FILES['upload_file4']['size'];
                $certificate_tmp =$_FILES['upload_file4']['tmp_name'];
                
                // echo "<Pre>"; print_r($certificate_tmp);exit();

                $certificate_ext=explode('.',$certificate_name);
                $certificate_ext=end($certificate_ext);
                $certificate_ext=strtolower($certificate_ext);


                $this->fileFormatNSizeValidation($certificate_ext,$certificate_size,'Image File');

                $upload_file4 = $this->uploadFile($certificate_name,$certificate_tmp,'Image File');
            }


            if($_FILES['upload_file5'])
            {
            // echo "<Pre>"; print_r($_FILES['image']);exit;

                $certificate_name = $_FILES['upload_file5']['name'];
                $certificate_size = $_FILES['upload_file5']['size'];
                $certificate_tmp =$_FILES['upload_file5']['tmp_name'];
                
                // echo "<Pre>"; print_r($certificate_tmp);exit();

                $certificate_ext=explode('.',$certificate_name);
                $certificate_ext=end($certificate_ext);
                $certificate_ext=strtolower($certificate_ext);


                $this->fileFormatNSizeValidation($certificate_ext,$certificate_size,'Image File');

                $upload_file5 = $this->uploadFile($certificate_name,$certificate_tmp,'Image File');
            }
            

            $data = array(
                'id_student' => $id_student,
                'id_supervisor' => $student->id_supervisor,
                'description' => $description,
                'status' => 1
            );

            if($upload_file)
            {
                $data['upload_file'] = $upload_file;
            }

            if($upload_file2)
            {
                $data['upload_file2'] = $upload_file2;
            }

            if($upload_file3)
            {
                $data['upload_file3'] = $upload_file3;
            }

            if($upload_file4)
            {
                $data['upload_file4'] = $upload_file4;
            }

            if($upload_file5)
            {
                $data['upload_file5'] = $upload_file5;
            }
            // echo "<Pre>";print_r($check_limit);exit();
            $insert_id = $this->bound_model->addBound($data);
            redirect('/student/bound/list');
        }

        $data['studentDetails'] = $this->bound_model->getStudentByStudentId($id_student);

        $this->global['studentPageCode'] = 'bound.add';
        $this->global['pageTitle'] = 'Student Portal : Add Deliverables Form';
        $this->loadViews("bound/add", $this->global, $data, NULL);
    }


    function view($id = NULL)
    {
        $id_student = $this->session->id_student;

        if ($id == null)
        {
            redirect('/student/bound/list');
        }
        $data['bound'] = $this->bound_model->getBound($id);
        $data['supervisor'] = $this->bound_model->getSupervisor($data['bound']->id_supervisor);
        $data['studentDetails'] = $this->bound_model->getStudentByStudentId($id_student);
        $data['boundCommentsDetails'] = $this->bound_model->boundCommentsDetails($id);
            
        // echo "<Pre>"; print_r($data);exit;

        $this->global['studentPageCode'] = 'bound.view';
        $this->global['pageTitle'] = 'Student Portal : View Bound Reporting';
        $this->loadViews("bound/view", $this->global, $data, NULL);
    }
}