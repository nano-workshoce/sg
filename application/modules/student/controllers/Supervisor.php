<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Supervisor extends BaseController
{
    public function __construct()
    {
        // $test = new BaseController();
        // $test->isStudentLoggedIn();
        parent::__construct();
        $this->load->model('supervisor_model');
        $this->isStudentLoggedIn();
    }

    function list()
    {       
        $id = $this->session->id_student;

        if($this->input->post())
        {
            // echo "<Pre>"; print_r($this->input->post());exit;

            $reason = $this->security->xss_clean($this->input->post('reason'));

            $student = $this->supervisor_model->getStudent($id);

            $data = array(
                'id_student' => $id,
                'id_supervisor_old' => $student->id_supervisor,
                'reason' => $reason,
                'status' => 0
            );

            $insert_id = $this->supervisor_model->addNewChangeSupervisorApplication($data);
            redirect('/student/supervisor/list');
        }


        
        $data['studentDetails'] = $this->supervisor_model->getStudentByStudentId($id);

        $supervisor = $this->supervisor_model->getSupervisor($data['studentDetails']->id_supervisor);

        if($supervisor)
        {
            $data['supervisor'] = $supervisor;
        }
        else
        {
            $data['supervisor'] = array();
        }

        $data['supervisorList'] = $this->supervisor_model->getSupervisorHistoryListByStudentId($id);
        
        $data['getSupervisorChangeApplicationListByStudentId'] = $this->supervisor_model->getSupervisorChangeApplicationListByStudentId($id);



        // echo "<Pre>";print_r($data['getSupervisorChangeApplicationListByStudentId']);exit();

        $this->global['studentPageCode'] = 'suoervisor.list';
        $this->global['pageTitle'] = 'Student Portal : Supervisor Change Application';
        $this->loadViews("supervisor/list", $this->global, $data, NULL);
    }
    
    function add()
    {

        $id_student = $this->session->id_student;
        $id_intake = $this->session->id_intake;
        $id_program = $this->session->id_program;
        $id_qualification = $this->session->id_qualification;

        if($this->input->post())
        {
            // echo "<Pre>"; print_r($this->input->post());exit;

            $id_phd_duration = $this->security->xss_clean($this->input->post('id_phd_duration'));
            $id_chapter = $this->security->xss_clean($this->input->post('id_chapter'));
            $id_topic = $this->security->xss_clean($this->input->post('id_topic'));
            $description = $this->security->xss_clean($this->input->post('description'));

            $student = $this->supervisor_model->getStudent($id_student);
            $generated_number = $this->supervisor_model->generateDeliverableApplicationNumber();



            $data = array(
                'application_number' => $generated_number,
                'id_student' => $id_student,
                'id_supervisor' => $student->id_supervisor,
                'id_phd_duration' => $id_phd_duration,
                'id_phd_duration' => $id_phd_duration,
                'id_chapter' => $id_chapter,
                'id_topic' => $id_topic,
                'description' => $description,
                'status' => 0
            );

            // $check_limit = $this->supervisor_model->checkStudentSupervisor($data);
            // if($check_limit == 1)
            // {
            //      echo "<Pre>";print_r("Max Application Limit Reached For Active & Pending Supervisor Application");exit();
            // }            
                 // echo "<Pre>";print_r($check_limit);exit();
            $insert_id = $this->supervisor_model->addNewSupervisor($data);
            redirect('/student/supervisor/list');
        }

        $data['durationList'] = $this->supervisor_model->durationListByStatus('1');

        $this->global['studentPageCode'] = 'suoervisor.add';
        $this->global['pageTitle'] = 'Student Portal : Add Supervisor Form';
        $this->loadViews("supervisor/add", $this->global, $data, NULL);
    }


    function view($id = NULL)
    {
        $id_student = $this->session->id_student;

        if ($id == null)
        {
            redirect('/student/supervisorApplication/list');
        }
        $data['supervisor'] = $this->supervisor_model->getSupervisor($id);
        $data['supervisor'] = $this->supervisor_model->getSupervisor($data['supervisor']->id_supervisor);
        $data['studentDetails'] = $this->supervisor_model->getStudentByStudentId($id_student);

        $data['durationList'] = $this->supervisor_model->durationListByStatus('1');
        $data['chapterList'] = $this->supervisor_model->chapterListByStatus('1');
        $data['topicList'] = $this->supervisor_model->topicListByStatus('1');
            
        // echo "<Pre>"; print_r($data);exit;

        $this->global['studentPageCode'] = 'suoervisor.view';
        $this->global['pageTitle'] = 'Student Portal : View Supervisor Form';
        $this->loadViews("supervisor/view", $this->global, $data, NULL);
    }

    function getChapterByDuration($id_duration)
    {
        $results = $this->supervisor_model->getChapterByDuration($id_duration);

        // echo "<Pre>"; print_r($results);exit;

        $table="

         <script type='text/javascript'>
                 $('select').select2();
         </script>


        <select name='id_chapter' id='id_chapter' class='form-control' onchange='getTopicByData()'>
            <option value=''>Select</option>
            ";

        for($i=0;$i<count($results);$i++)
        {

        // $id = $results[$i]->id_procurement_category;
        $id = $results[$i]->id;
        $name = $results[$i]->name;
        $table.="<option value=".$id.">".$name.
                "</option>";

        }
        $table.="

        </select>";

        echo $table;
        exit;
    }

    function getTopicByData()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        $results = $this->supervisor_model->getTopicByData($tempData);

        // echo "<Pre>"; print_r($results);exit;

        $table="

         <script type='text/javascript'>
                 $('select').select2();
         </script>


        <select name='id_topic' id='id_topic' class='form-control'>
            <option value=''>Select</option>
            ";

        for($i=0;$i<count($results);$i++)
        {

        // $id = $results[$i]->id_procurement_category;
        $id = $results[$i]->id;
        $name = $results[$i]->topic;
        $table.="<option value=".$id.">".$name.
                "</option>";

        }
        $table.="

        </select>";

        echo $table;
        exit;
    }
}

