<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Profile extends BaseController
{
    public function __construct()
    {
        // echo "string";exit();
        // $test = new BaseController();
        // $test->isStudentLoggedIn();
        parent::__construct();
        $this->load->model('profile_model');
        $this->isStudentLoggedIn();
    }

    public function index()
    {
        $this->list();
    }

    function demoView()
    {
        $this->load->view('vie');
    }

    function pageNotFound()
    {
        $this->global['pageTitle'] = 'Student Portal : 404 - Page Not Found';
        $this->loadViews("404", $this->global, NULL, NULL);
    }

    function list()
    {

        $id_student = $this->session->id_student;
        $id_intake = $this->session->id_intake;
        $id_program = $this->session->id_program;
        $id_qualification = $this->session->id_qualification;
        $id_program_scheme = $this->session->id_program_scheme;
        $id_program_landscape = $this->session->id_program_landscape;
        $student_semester = $this->session->student_semester;


                 // echo "<Pre>";print_r($id_program_scheme);exit();
        // $data['courseList'] = $this->profile_model->getPerogramLandscape($id_intake,$id_program);
        $data['getStudentData'] = $this->profile_model->getStudentData($id_student);
        $data['courseRegisteredList'] = $this->profile_model->getCourseRegisteredList($id_student,$id_intake,$id_program,$id_qualification,$id_program_scheme);
        $data['semesterList'] = $this->profile_model->semesterListByStatus('1');
        $data['courseList'] = $this->profile_model->getPerogramLandscape($id_intake,$id_program,$id_program_scheme,$id_program_landscape,$data['getStudentData']->current_semester);


        // if(!empty($data['courseRegisteredList']))
        // {
        //     $$data['courseRegisteredList']->
        // }

        // echo "<Pre>";print_r($data['courseRegisteredList']);exit();

        $data['profileList'] = $this->profile_model->profileList();
        $this->global['pageTitle'] = 'Student Portal : Payment Type List';
        $this->loadViews("profile/list", $this->global, $data, NULL);
    }


    function dashboard()
    {

        $id_student = $this->session->id_student;
        $id_intake = $this->session->id_intake;
        $id_program = $this->session->id_program;
        $id_qualification = $this->session->id_qualification;
        $id_program_scheme = $this->session->id_program_scheme;
        $id_program_landscape = $this->session->id_program_landscape;
        $student_semester = $this->session->student_semester;

        $data['getStudentData'] = $this->profile_model->getStudentData($id_student);
        $data['getInvoiceByStudentId'] = $this->profile_model->getInvoiceByStudentId($id_student,$data['getStudentData']->id_applicant);
        $data['notificationList'] = $this->profile_model->notificationList('Student');
        $data['eventsList'] = $this->profile_model->eventsList('Student');

        $data['getReceiptByStudentId'] = $this->profile_model->getReceiptByStudentId($id_student,$data['getStudentData']->id_applicant);

        $data['courseRegisteredList'] = $this->profile_model->getCourseRegisteredList($id_student,$id_intake,$id_program,$id_qualification,$id_program_scheme);


        // echo "<Pre>";print_r($data['courseRegisteredList']);exit();

        $data['profileList'] = $this->profile_model->profileList();
        $this->global['pageTitle'] = 'Student Portal : Student Dashboard';
        $this->loadViews("profile/dashboard", $this->global, $data, NULL);
    }

    
    function add()
    {
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $status = $this->security->xss_clean($this->input->post('status'));
                            
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'status' => $status
                );

                $result = $this->profile_model->addNewPaymentType($data);
                redirect('/student/profile/list');
            }
            //print_r($data['stateList']);exit;
            $this->global['pageTitle'] = 'Student Portal : Add Sponser';
            $this->loadViews("profile/add", $this->global, NULL, NULL);
    }


    function edit($id = NULL)
    {
            if ($id == null)
            {
                redirect('/student/profile/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $status = $this->security->xss_clean($this->input->post('status'));
                            
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'status' => $status
                );
                
                $result = $this->profile_model->editPaymentType($data,$id);
                redirect('/student/profile/list');
            }

            $data['profileDetails'] = $this->profile_model->getPaymentType($id);
            
            $this->global['pageTitle'] = 'Student Portal : Edit Sponser';
            $this->loadViews("profile/edit", $this->global, $data, NULL);
    }

    // function isStudentLoggedIn()
    // {
    //     // echo "string";exit();
    //     $this->load->library('session');
    //     $isStudentLoggedIn = $this->session->userdata('isStudentLoggedIn');
        
    //     if (! isset ( $isStudentLoggedIn ) || $isStudentLoggedIn != TRUE)
    //     {
    //         redirect ( 'studentLogin/checkStudentLoggedIn');
    //     }
    //     else
    //     {
    //         $this->student_name = $this->session->userdata ( 'student_name' );
    //         $this->id_student = $this->session->userdata ( 'id_student' );
    //         $this->lastLogin = $this->session->userdata ( 'last_login' );
            
    //         $this->global ['name'] = $this->student_name;
    //         $this->global ['id_student'] = $this->id_student;
    //         $this->global ['last_login'] = $this->lastLogin;
    //     }
    // }

    function logout()
    {
        // echo "string";exit();
        

        $isStudentAdminLoggedIn = $this->session->isStudentAdminLoggedIn;
        $id_student = $this->session->id_student;
        // echo $isStudentAdminLoggedIn;exit();


        if($isStudentAdminLoggedIn == TRUE)
        {

        // echo $isStudentAdminLoggedIn;exit();
            
            $sessionArray = array('id_student'=> '',                    
                    'student_name'=> '',
                    'email_id'=> '',
                    'nric'=> '',
                    'id_intake'=> '',
                    'id_program'=> '',
                    'id_program_landscape' => '',
                    'id_qualification' => '',
                    'id_program_scheme' => '',
                    'student_education_level'=>  '',
                    'student_semester'=>  '',
                    'student_last_login'=>  '',
                    'student_profile_pic' => '',
                    'isStudentLoggedIn' => FALSE,
                    'isStudentAdminLoggedIn' => FALSE
            );
            $this->session->set_userdata($sessionArray);

            // $this->session->set_userdata("id_admin_student",$id_student);

            redirect('studentAdminLogin/checkStudentLoggedIn/'.$id_student);

        }
        else
        {
            $sessionArray = array('id_student'=> '',                    
                    'student_name'=> '',
                    'email_id'=> '',
                    'nric'=> '',
                    'id_intake'=> '',
                    'id_program'=> '',
                    'id_program_landscape' => '',
                    'id_qualification' => '',
                    'id_program_scheme' => '',
                    'student_education_level'=>  '',
                    'student_semester'=>  '',
                    'student_last_login'=>  '',
                    'student_profile_pic' => '',
                    'isStudentLoggedIn' => FALSE
            );

            $this->session->set_userdata($sessionArray);
            $this->isStudentLoggedIn();

        }
     // $this->session->sess_destroy();
     // redirect($_SERVER['HTTP_REFERER']);
     $this->isStudentLoggedIn();
    }
}
