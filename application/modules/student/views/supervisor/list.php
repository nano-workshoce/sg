<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Change Supervisor Application</h3>
        </div>    
            <div class="form-container">
                <h4 class="form-group-title">Student Details</h4>
                <div class='data-list'>
                    <div class='row'> 
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Student Name :</dt>
                                <dd><?php echo ucwords($studentDetails->full_name);?></dd>
                            </dl>
                            <dl>
                                <dt>Student NRIC :</dt>
                                <dd><?php echo $studentDetails->nric ?></dd>
                            </dl>
                            <dl>
                                <dt>Student Email :</dt>
                                <dd><?php echo $studentDetails->email_id; ?></dd>
                            </dl>
                             <dl>
                                <dt>Nationality :</dt>
                                <dd><?php echo $studentDetails->nationality ?></dd>
                            </dl>                            
                        </div>        
                        
                        <div class='col-sm-6'>                           
                            <dl>
                                <dt>Intake :</dt>
                                <dd><?php echo $studentDetails->intake_name ?></dd>
                            </dl>
                            <dl>
                                <dt>Program :</dt>
                                <dd><?php echo $studentDetails->programme_name; ?></dd>
                            </dl>
                            <dl>
                                <dt>Education Level :</dt>
                                <dd><?php echo $studentDetails->qualification_name; ?></dd>
                            </dl>
                            <dl>
                                <dt>Program Scheme :</dt>
                                <dd><?php echo $studentDetails->program_scheme; ?></dd>
                            </dl>
                        </div>
                    </div>
                </div>
            </div>


        <?php
        if($supervisor)
        {
            ?>

            <br>

             <div class="form-container">
                <h4 class="form-group-title">Supervisor Details</h4>
                <div class='data-list'>
                    <div class='row'> 
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Supervisor Name :</dt>
                                <dd><?php echo ucwords($supervisor->full_name);?></dd>
                            </dl>
                            <dl>
                                <dt>Supervisor Email :</dt>
                                <dd><?php echo $supervisor->email ?></dd>
                            </dl>                     
                        </div>        
                        
                        <div class='col-sm-6'>                           
                            <dl>
                                <dt>Supervisor Type :</dt>
                                <dd><?php 
                                if($supervisor->type == 0)
                                {
                                    echo 'External';
                                }elseif($supervisor->type == 1)
                                {
                                    echo 'Internal';
                                } ?></dd>
                            </dl>
                        </div>
                    </div>
                </div>
            </div>



        <form id="form_unit" action="" method="post">

            <div class="form-container">
                <h4 class="form-group-title">Change Supervisor Application Details</h4>

                <div class="row">
                    
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Reason <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="reason" name="reason">
                        </div>
                    </div>


                  <!--   <div class="col-sm-4">
                        <div class="form-group">
                            <label>Email <span class='error-text'>*</span></label>
                            <input type="email" class="form-control" id="email" name="email">
                        </div>
                    </div> -->


                    <!-- <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" checked="checked"><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0"><span class="check-radio"></span> Inactive
                            </label>                              
                        </div>                         
                    </div> -->
                </div>

            </div>


            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="list" class="btn btn-link">Clear All Fields</a>
                </div>
            </div>


        </form>

        <?php
        }
        else
        {
            ?>

                <a class="btn btn-link">Supervisor Not Alloted Wait till the Supervisor Assigning</a>

            <?php

        }
        ?>



        <div class="form-container">
            <h4 class="form-group-title">Change Supervisor Applications</h4>          
            <div class="m-auto text-center">
                <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
            </div>
            <div class="clearfix">
                <ul class="nav nav-tabs offers-tab sub-tabs text-center" role="tablist" >
                    <li role="presentation" class="active" ><a href="#invoice" class="nav-link border rounded text-center"
                            aria-controls="invoice" aria-selected="true"
                            role="tab" data-toggle="tab">Application</a>
                    </li>
                    <li role="presentation"><a href="#history" class="nav-link border rounded text-center"
                            aria-controls="history" role="tab" data-toggle="tab">Change History</a>
                    </li>
                    
                </ul>
                <br>

                
                <div class="tab-content offers-tab-content">



                    <div role="tabpanel" class="tab-pane active" id="invoice">
                        <div class="mt-4">
                            <div class="custom-table" id="printInvoice">
                                <table class="table" id="list-table">
                                    <thead>
                                    <tr>
                                        <th>Sl. No</th>
                                        <th>Reason</th>
                                        <th>Old Supervisor</th>
                                        <th>New Supervisor</th>
                                        <th>Applied On</th>
                                        <th>Status</th>
                                        <th>Approved / Rejected By</th>
                                        <!-- <th class="text-center">Action</th> -->
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    if (!empty($getSupervisorChangeApplicationListByStudentId))
                                    {
                                        $i=1;
                                        foreach ($getSupervisorChangeApplicationListByStudentId as $record) {
                                    ?>
                                        <tr>
                                            <td><?php echo $i ?></td>
                                            <td><?php echo $record->reason ?></td>
                                            <td><?php
                                            if($record->old_supervisor_type != '')
                                            {
                                                if($record->old_supervisor_type == 0)
                                                {
                                                    echo 'External';
                                                }elseif($record->old_supervisor_type == 1)
                                                {
                                                    echo 'Internal';
                                                }
                                            }
                                                echo " - " . $record->old_supervisor_name;
                                            ?></td>
                                            <td><?php
                                            if($record->new_supervisor_type != '')
                                            {

                                                if($record->new_supervisor_type == 0)
                                                {
                                                    echo 'External';
                                                }elseif($record->new_supervisor_type == 1)
                                                {
                                                    echo 'Internal';
                                                }
                                            }
                                                echo " - " . $record->new_supervisor_name;
                                            ?></td>
                                            <td>
                                            <?php 
                                            if($record->created_dt_tm)
                                            {
                                                echo date('d-m-Y',strtotime($record->created_dt_tm)); 
                                            }
                                            ?>
                                            </td>
                                            <td><?php if( $record->status == 0)
                                            {
                                              echo "Pending";
                                            }
                                            elseif( $record->status == 1)
                                            {
                                              echo "Approved";
                                            }
                                            elseif( $record->status == 2)
                                            {
                                              echo "Rejected";
                                            }
                                            ?></td>
                                            <td><?php echo $record->updated_by ?></td>
                                            <!-- <td class="text-center">
                                                <a href="<?php echo 'view/' . $record->id; ?>" title="Preview">View</a>
                                            </td> -->
                                        </tr>
                                    <?php
                                    $i++;
                                        }
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div> 
                    </div>












                    <div role="tabpanel" class="tab-pane" id="history">
                        <div class="mt-4">
                            <div class="custom-table" id="printInvoice">
                                <table class="table" id="list-table">
                                    <thead>
                                    <tr>
                                        <th>Sl. No</th>
                                        <th>Supervisor</th>
                                        <th>Supervisor Type</th>
                                        <th>Changed On</th>
                                        <th>Changed By</th>
                                        <th>Requested By Student</th>
                                        <!-- <th class="text-center">Action</th> -->
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    if (!empty($supervisorList))
                                    {
                                        $i=1;
                                        foreach ($supervisorList as $record) {
                                    ?>
                                        <tr>
                                            <td><?php echo $i ?></td>
                                            <td><?php echo $record->supervisor_name;
                                            ?></td>
                                            <td><?php 
                                            if($record->supervisor_type == 0)
                                            {
                                                echo 'External';
                                            }elseif($record->supervisor_type == 1)
                                            {
                                                echo 'Internal';
                                            } ?></td>
                                            <td><?php echo date('d-m-Y',strtotime($record->created_dt_tm)) ?></td>
                                            <td><?php echo $record->created_by; ?></td>
                                            <td><?php 
                                            if($record->is_change_application == 0)
                                            {
                                                echo 'No';
                                            }elseif($record->is_change_application <= 1)
                                            {
                                                echo 'Yes';
                                            } ?>
                                                
                                            </td>
                                            <!-- <td class="text-center">
                                                <a href="<?php echo 'view/' . $record->id; ?>" title="Preview">View</a>
                                            </td> -->
                                        </tr>
                                    <?php
                                    $i++;
                                        }
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div> 
                    </div>


                </div>
            </div>

        </div>
            

    </div>

        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
</div>


</form>

<script>

    $('select').select2();

    $(document).ready(function() {
        $("#form_unit").validate({
            rules: {
                reason: {
                    required: true
                },
                email: {
                    required: true
                }
            },
            messages: {
                reason: {
                    required: "<p class='error-text'>Reason Required</p>",
                },
                email: {
                    required: "<p class='error-text'>Email Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>

</script>