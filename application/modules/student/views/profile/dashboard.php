<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <!-- <h3>Student Profile</h3> -->
    </div>

     <h2 align="center">Welcome <?php echo $student_name; ?></h2>






      <div class="form-container">
        <h4 class="form-group-title">Peding payment for invoice</h4>
         
            <div class="custom-table" id="printInvoice">
                <table class="table" id="list-table">
                    <thead>
                    <tr>
                        <th>Sl. No</th>
                        <th>Invoice Number</th>
                        <th>Invoice Type</th>
                        <th>Invoice Total</th>
                        <th>Total Payable</th>
                        <th>Total Discount</th>
                        <th>Balance </th>
                        <th>Remarks</th>
                        <th>Currency</th>
                        <th>Status</th>
                        <!-- <th>Action</th> -->
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if (!empty($getInvoiceByStudentId))
                    {
                         $i=1;
                        foreach ($getInvoiceByStudentId as $record)
                        {
                    ?>
                        <tr>
                            <td><?php echo $i ?></td>
                            <td><?php echo $record->invoice_number ?></td>
                            <td><?php echo $record->type ?></td>
                            <td><?php echo $record->invoice_total ?></td>
                            <td><?php echo $record->total_amount ?></td>
                            <td><?php echo $record->total_discount ?></td>
                            <td><?php if($record->balance_amount == ""){
                                echo "0.00";
                            } else { echo $record->balance_amount; } ?></td>
                            <td><?php echo $record->remarks ?></td>
                            <td><?php 
                            if($record->currency_name == '')
                            {
                              echo $record->currency;
                            }else
                            {
                              echo $record->currency_name;
                            }
                             ?></td>
                            <td><?php 
                            if($record->status == "0")
                            {
                                echo "Pending";
                            }
                            elseif($record->status == "1")
                            { 
                                echo "Approved"; 
                            }elseif($record->status == "2")
                            { 
                                echo "Rejected"; 
                            } ?></td>

                            <!-- <td class="">
                            <a href="#" title="" onclick="printDiv('printInvoice')">Print</a> | <a href="<?php echo 'showInvoice/' . $record->id . '/invoice'; ?>" title="">View</a>
                            </td> -->
                        </tr>
                    <?php
                        $i++;
                        }
                    }
                    ?>
                    </tbody>
                </table>
            </div>


      </div>




    <br>




    <div class="form-container">
        <h4 class="form-group-title">Receipt Details</h4>



        <div class="custom-table" id="printInvoice">


            <table class="table" id="list-table">
                <thead>
                <tr>
                    <th>Sl. No</th>
                    <th>Receipt Number</th>
                    <th>Receipt Amount</th>
                    <th>Currency</th>
                    <th>Remarks</th>
                    <th>Status</th>
                    <!-- <th>Action</th> -->
                </tr>
                </thead>
                <tbody>
                <?php
                if (!empty($getReceiptByStudentId)) {
                    $i=1;
                    foreach ($getReceiptByStudentId as $record) {
                ?>
                    <tr>
                        <td><?php echo $i ?></td>
                        <td><?php echo $record->receipt_number ?></td>
                        <td><?php if($record->receipt_amount == ""){
                            echo "0.00";
                        } else { echo $record->receipt_amount; } ?></td>
                        <td ><?php
                         if($record->currency_name == '')
                          {
                            echo $record->currency;
                          }
                          else
                          {
                            echo $record->currency_name;
                          }
                          ?>
                        </td>
                        <td><?php echo $record->remarks ?></td>
                        <td><?php 
                        if($record->status == "0")
                        {
                            echo "Pending";
                        }
                        elseif($record->status == "1")
                        { 
                            echo "Approved"; 
                        }elseif($record->status == "2")
                        { 
                            echo "Rejected"; 
                        } ?></td>

                        <!-- <td class="">
                            <a href="#" title="" onclick="printDiv('printReceipt')">Print</a> | <a href="<?php echo 'showReceipt/' . $record->id. '/receipt'; ?>" title="">View</a>
                        </td> -->
                    </tr>
                <?php
                    $i++;
                    }
                }
                ?>
                </tbody>

            </table>


        </div>



    </div>




      <br>




      <div class="form-container">
        <h4 class="form-group-title">Notifications</h4>
         
            <div class="custom-table" id="printInvoice">
                <table class="table" id="list-table">
                    <thead>
                    <tr>
                        <!-- <th>Sl. No</th> -->
                        <th>Notification</th>
                        <!-- <th class="text-center">Status</th> -->
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if (!empty($notificationList))
                    {
                         $i=1;
                        foreach ($notificationList as $record)
                        {
                    ?>
                        <tr>
                            <!-- <td><?php echo $i ?></td> -->
                            <td><b><?php echo $record->name ?></b></td>
                            <!-- <td class="text-center"><?php if( $record->status == '1')
                            {
                              echo "Active";
                            }
                            else
                            {
                              echo "In-Active";
                            } 
                            ?></td> -->
                            <!-- <td class="text-center">
                              <a href="<?php echo 'edit/' . $record->id; ?>" title="Edit">Edit</a>
                            </td> -->
                        </tr>
                    <?php
                        $i++;
                        }
                    }
                    ?>
                    </tbody>
                </table>
            </div>

      </div>

      
      <br>


      <div class="form-container">
        <h4 class="form-group-title">Events</h4>
         
            <div class="custom-table" id="printInvoice">
                <table class="table" id="list-table">
                    <thead>
                    <tr>
                        <th>Sl. No</th>
                        <th>Event Title</th>
                        <th>Start Date</th>
                        <th>End Date</th>
                        <th class="text-center">Status</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if (!empty($eventsList))
                    {
                         $i=1;
                        foreach ($eventsList as $record)
                        {
                    ?>
                        <tr>
                            <td><?php echo $i ?></td>
                            <td><?php echo $record->name ?></td>
                            <td><?php echo date('d-m-Y', strtotime($record->start_date)) ?></td>
                            <td><?php echo date('d-m-Y', strtotime($record->end_date)) ?></td>
                            <td class="text-center"><?php if( $record->status == '1')
                            {
                              echo "Active";
                            }
                            else
                            {
                              echo "In-Active";
                            } 
                            ?></td>
                            <!-- <td class="text-center">
                              <a href="<?php echo 'edit/' . $record->id; ?>" title="Edit">Edit</a>
                            </td> -->
                        </tr>
                    <?php
                        $i++;
                        }
                    }
                    ?>
                    </tbody>
                </table>
            </div>


      </div>



      <br>



      <div class="form-container">
                <h4 class="form-group-title">Course Registered List</h4>

               
            <div class="custom-table">
                <table class="table">
                    <thead>
                        <tr>
                        <th>Sl. No</th>
                         <th>Course </th>
                         <th>Credit Hours </th>
                         <th>Semester </th>
                         <th>Course Registered By </th>
                         <th class="text-center">Registered Semester </th>
                         <th class="text-center">Registered On </th>
                        </tr>
                    </thead>
                    <tbody>
                         <?php
                     $total = 0;
                      for($i=0;$i<count($courseRegisteredList);$i++)
                     { ?>
                        <tr>
                        <td><?php echo $i+1;?></td>
                        <td><?php echo $courseRegisteredList[$i]->course_code . " - " . $courseRegisteredList[$i]->course_name;?></td>
                        <td><?php echo $courseRegisteredList[$i]->credit_hours;?></td>
                        <td><?php echo $courseRegisteredList[$i]->semester_code . " - " . $courseRegisteredList[$i]->semester_name;?></td>
                        <td>
                            <?php if($courseRegisteredList[$i]->by_student > 0)
                            {
                                echo $student_name . " ( SELF )";
                            }
                            else
                            {
                                echo $courseRegisteredList[$i]->role; 
                            } 
                        ?>
                                
                        </td>

                        <td class="text-center">
                            <?php echo $courseRegisteredList[$i]->student_current_semester;  ?>
                        </td>


                        <td class="text-center">
                            <?php if($courseRegisteredList[$i]->created_dt_tm) echo date('d-m-Y', strtotime($courseRegisteredList[$i]->created_dt_tm));  ?>        
                        </td>
                       
                      <?php 
                     
                  }
                  ?>
                    </tbody>
                </table>
            </div>
        </div>


  </div>



  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
<script type="text/javascript">
    $('select').select2();
</script>