<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Nationality_model extends CI_Model
{
    function nationalityList()
    {
        $this->db->select('*');
        $this->db->from('nationality');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function nationalityListSearch($search)
    {
        $this->db->select('*');
        $this->db->from('nationality');
        if (!empty($search))
        {
            $likeCriteria = "(name  LIKE '%" . $search . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function getNationality($id)
    {
        $this->db->select('*');
        $this->db->from('nationality');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewNationality($data)
    {
        $this->db->trans_start();
        $this->db->insert('nationality', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editNationality($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('nationality', $data);
        return TRUE;
    }
}

