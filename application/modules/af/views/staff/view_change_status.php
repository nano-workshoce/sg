<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>View Change Of Status (AF)</h3>
        </div>





        <div class="form-container">
            <h4 class="form-group-title">Change Status Details</h4>


            <form id="form_staff" action="" method="post">

            <div class="row">


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select Staff <span class='error-text'>*</span></label>
                         <select name="id_staff" id="id_staff" class="form-control" disabled>
                            <option value="">Select</option>
                            <?php
                            if (!empty($staffList)) {
                                foreach ($staffList as $record) {
                            ?>
                                    <option value="<?php echo $record->id;  ?>"
                                    <?php
                                    if($staffChangeStatus->id_staff == $record->id)
                                    {
                                        echo "selected";
                                    }
                                    ?>
                                     >
                                        <?php echo $record->ic_no . " - " . $record->name;  ?>        
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Status <span class='error-text'>*</span></label>
                         <select name="id_change_status" id="id_change_status" class="form-control" onchange="showLeaveFields(this.value)" disabled>
                            <option value="">Select</option>
                            <?php
                            if (!empty($changeStatusList)) {
                                foreach ($changeStatusList as $record) {
                            ?>
                                    <option value="<?php echo $record->id;  ?>" 
                                    <?php
                                    if($staffChangeStatus->id_change_status == $record->id)
                                    {
                                        echo "selected";
                                    }
                                    ?>
                                    >
                                        <?php echo $record->name;  ?>        
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>




                <div class="col-sm-4">
                    <div class="form-group">
                        <label>From Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="change_from_dt" name="change_from_dt" value="<?php if($staffChangeStatus->from_dt){ echo date('d-m-Y',strtotime($staffChangeStatus->from_dt)); }
                        ?>
                        " readonly>
                    </div>
                </div>



                <div class="col-sm-4">
                    <div class="form-group">
                        <label>To Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="change_to_dt" name="change_to_dt"  value="<?php if($staffChangeStatus->to_dt){ echo date('d-m-Y',strtotime($staffChangeStatus->to_dt)); }
                        ?>
                        " readonly>
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Created On <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="created_dt_tm" name="created_dt_tm"  value="<?php if($staffChangeStatus->created_dt_tm){ echo date('d-m-Y',strtotime($staffChangeStatus->created_dt_tm)); }
                        ?>
                        " readonly>
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Reason <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="change_status_reason" name="change_status_reason"  value="<?php echo $staffChangeStatus->reason;
                        ?>" readonly>
                    </div>
                </div>



            </div>

        </div>


        <div class="button-block clearfix">
            <div class="bttn-group">
                    <!-- <button type="submit" class="btn btn-primary btn-lg" name="btn_submit" value="5">Save</button> -->
                <a href="../changeStatusList" class="btn btn-link">Back</a>
            </div>
        </div>

          


        </form>


        

            
        
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Intake Has Programme</h4>
      </div>
      <div class="modal-body">
         <h4></h4>
             <div class="row">
                        <input type="text" class="form-control" id="id" name="id">

                
            </div>


      </div>
      <div class="modal-footer">
                

        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

</form>

<script>

    $('select').select2();


    $(function()
    {
        $(".datepicker").datepicker(
        {
            changeYear: true,
            changeMonth: true,
        });
    });


    function showLeaveFields(id_status)
    {
        if(id_status > 6)
        {
            $("#view_change_from_date").show();
            $("#view_change_to_date").show();
        }
        else
        {
            $("#view_change_from_date").hide();
            $("#view_change_to_date").hide();
        }

    }



    $(document).ready(function()
    {
        $("#form_staff").validate(
        {
            rules:
            {
                id_staff:
                {
                    required: true
                },
                id_change_status:
                {
                    required: true
                },
                change_from_dt:
                {
                    required: true
                },
                change_to_dt:
                {
                    required: true
                },
                change_status_reason:
                {
                    required: true
                }
            },
            messages:
            {
                id_staff:
                {
                    required: "<p class='error-text'>Select Staff</p>",
                },
                id_change_status:
                {
                    required: "<p class='error-text'>Select Change Status</p>",
                },
                change_from_dt:
                {
                    required: "<p class='error-text'>Select From Date</p>",
                },
                change_to_dt:
                {
                    required: "<p class='error-text'>Select To Date</p>",
                },
                change_status_reason:
                {
                    required: "<p class='error-text'>Reason Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>