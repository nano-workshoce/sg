<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        

        <div class="page-title clearfix">
            <h3>View Patient Appointment</h3>
        </div>



    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Patient Details
            </a>
          </h4>
        </div>


        
          <div id="collapseOne" class="panel-collapse collapse out" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">


              	<div class="form-container">
				    
				    <!-- <h4 class="form-group-title">Patient Details</h4>  -->


                	<div class="row">
                  
                        <div class='data-list'>
                            <div class='row'> 
                                <div class='col-sm-6'>
                                    <dl>
                                        <dt>Patient Name :</dt>
                                        <dd><?php echo ucwords($patient->full_name);?></dd>
                                    </dl>
                                    <dl>
                                        <dt>Patient Phone :</dt>
                                        <dd><?php echo $patient->phone ?></dd>
                                    </dl>
                                    <dl>
                                        <dt>Patient Email :</dt>
                                        <dd><?php echo $patient->email; ?></dd>
                                    </dl>
                                    <dl>
                                        <dt>Patient Gender :</dt>
                                        <dd><?php echo $patient->gender; ?></dd>
                                    </dl>                        
                                </div>        
                                
                                <div class='col-sm-6'>                           
                                    <dl>
                                        <dt>Patient Register No. :</dt>
                                        <dd><?php echo $patient->register_no ?></dd>
                                    </dl>
                                    <dl>
                                        <dt>Address :</dt>
                                        <dd><?php echo $patient->mail_address1; ?></dd>
                                    </dl>
                                    <dl>
                                        <dt>City :</dt>
                                        <dd><?php echo $patient->mailing_city; ?></dd>
                                    </dl>
                                    <dl>
                                        <dt>Patient DOB :</dt>
                                        <dd><?php echo date('d-m-Y', strtotime($patient->date_of_birth)); ?></dd>
                                    </dl>  
                                </div>
                            </div>
                        </div>

                	</div>

                </div>





              </div>

            </div>
          </div>
        
      

      </div>
   
    </div>







        
    <div class="panel-group advanced-search" id="accordion_two" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingTwo">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion_two" href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
              Test Booking Details
            </a>
          </h4>
        </div>



          <div id="collapseTwo" class="panel-collapse collapse out" role="tabpanel" aria-labelledby="headingTwo">
            <div class="panel-body">
              <div class="form-horizontal">

                
                    <?php  
				        if(!empty($patientBookingTestDetails))
				        {
				         ?>



				        <!-- <div class="form-container">
				                <h4 class="form-group-title">Test Details</h4> --> 


				            

				            <div class="custom-table">
				              <table class="table" id="list-table">
				                <thead>
				                  <tr>
				                    <th>Sl. No</th>
				                    <th>Booking Type</th>
				                    <th>Package</th>
				                    <th>Test</th>
				                    <th>Amount</th>
				                  </tr>
				                </thead>
				                <tbody>
				                  <?php
				                  if (!empty($patientBookingTestDetails))
				                  {
				                    $i = 1;
				                    $total = 0;
				                    $total_amount = 0;
				                    foreach ($patientBookingTestDetails as $record)
				                    {
				                  ?>
				                      <tr>
				                        <td><?php echo $i ?></td>
				                        <td><?php echo $record->booking_type ?></td>
				                        <td><?php echo $record->test_code . " - " . $record->test_name ?></td>
				                        <td><?php echo $record->package_code . " - " . $record->package_name ?></td>
				                        <td><?php echo $record->amount ?></td>
				                      </tr>
				                  <?php
				                  $total = $total + $record->amount;
				                  $i++;
				                    }
				                     $total = number_format($total, 2, '.', ',');
				                    ?>

				                    <tr >
				                        <td bgcolor="" colspan="3"></td>
				                        <td bgcolor="" style="text-align: center;"><b>Total Amount :</b></td>
				                        <td bgcolor="" ><b> <?php echo $total ?></b></td>
				                      </tr>
				                    <?php
				                  }
				                  ?>
				                </tbody>
				              </table>
				            </div>

				        <!-- </div> -->


			        <?php  
			       
			        }
			       
			        ?>




              </div>

            </div>
          </div>
        


      </div>
    </div>







    <div class="panel-group advanced-search" id="accordion_three" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingThree">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion_three" href="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
              Sample Collection Details
            </a>
          </h4>
        </div>

                  
          <div id="collapseThree" class="panel-collapse collapse out" role="tabpanel" aria-labelledby="headingThree">
            <div class="panel-body">
              <div class="form-horizontal">

                
	                <?php  
				        if(!empty($patientBookingSampleDetails))
				        {
				         ?>



				        <!-- <div class="form-container">
				                <h4 class="form-group-title">Sample Collection Details</h4>  -->


				            

				            <div class="custom-table">
				              <table class="table" id="list-table">
				                <thead>
				                  <tr>
				                    <th>Sl. No</th>
				                    <th>Sample</th>
				                  </tr>
				                </thead>
				                <tbody>
				                  <?php
				                  if (!empty($patientBookingSampleDetails))
				                  {
				                    $i = 1;
				                    $total = 0;
				                    $total_amount = 0;
				                    foreach ($patientBookingSampleDetails as $record)
				                    {
				                  ?>
				                      <tr>
				                        <td><?php echo $i ?></td>
				                        <td><?php echo $record->sample_code . " - " . $record->sample_name ?></td>
				                      </tr>
				                  <?php
				                  $i++;
				                    }
				                    ?>
				                    <?php
				                  }
				                  ?>
				                </tbody>
				              </table>
				            </div>

				        <!-- </div> -->


			        <?php  
			       
			        }
			       
			        ?>


                <div id="view_temp_sample">
                  
                </div>


              </div>

            </div>
          </div>


      
      </div>
    </div>
        







    <div class="panel-group advanced-search" id="accordion_four" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingFour">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion_four" href="#collapseFour" aria-expanded="true" aria-controls="collapseFour">
              Appointment Details
            </a>
          </h4>
        </div>

          <div id="collapseFour" class="panel-collapse collapse out" role="tabpanel" aria-labelledby="headingFour">
            <div class="panel-body">
              <div class="form-horizontal">




                <div class="form-container">
            
            <!-- <h4 class="form-group-title">Patient Details</h4>  -->


                  <div class="row">
                  
                        <div class='data-list'>
                            <div class='row'> 
                                <div class='col-sm-6'>
                                    <dl>
                                        <dt>Booking Reference No :</dt>
                                        <dd><?php echo ucwords($patientBooking->reference_number);?></dd>
                                    </dl>
                                    <dl>
                                        <dt>Sample Collection By :</dt>
                                        <dd><?php echo $patientBooking->staff_id . " - " . $patientBooking->staff_name ?></dd>
                                    </dl>
                                    <dl>
                                        <dt>Total Billing Amount  :</dt>
                                        <dd><?php echo $patientBooking->amount; ?></dd>
                                    </dl>
                                    <?php 
                                    if($patientBooking->status == 1)
                                    {
                                      ?>

                                    <dl>
                                        <dt>Paid Amount :</dt>
                                        <dd><?php echo $patientBooking->paid_amount; ?></dd>
                                    </dl>

                                    <dl>
                                          <dt>Comments :</dt>
                                          <dd><?php echo $patientBooking->comments; ?></dd>
                                      </dl>                        

                                    <?php
                                    }
                                    elseif($patientBooking->status == 2)
                                    {
                                      ?>

                                      <dl>
                                          <dt>Reason :</dt>
                                          <dd><?php echo $patientBooking->reason; ?></dd>
                                      </dl>                        

                                      <?php

                                    }
                                    ?>
                                </div>        
                                
                                <div class='col-sm-6'>                           
                                    <dl>
                                        <dt>Appointment Date :</dt>
                                        <dd><?php echo date('d-m-Y', strtotime($patientBooking->appointment_date)) ?></dd>
                                    </dl>
                                    <dl>
                                        <dt>Appointment Time :</dt>
                                        <dd><?php echo $patientBooking->time_slot; ?></dd>
                                    </dl>
                                    <dl>
                                        <dt>Status :</dt>
                                        <dd><?php 
                                          if($patientBooking->status == 0)
                                          {
                                            echo 'Pending';
                                          }
                                          elseif($patientBooking->status == 1)
                                          {
                                            echo 'Completed';
                                          }
                                          elseif($patientBooking->status == 2)
                                          {
                                            echo 'Cancelled';
                                          }
                                          ?>    
                                        </dd>
                                    </dl>
                                    <?php 
                                    if($patientBooking->status == 1)
                                    {
                                      ?>

                                    <dl>
                                        <dt>Payment Mode :</dt>
                                        <dd><?php echo $patientBooking->payment_mode; ?></dd>
                                    </dl>  

                                    <dl>
                                        <dt>Completed On :</dt>
                                        <dd><?php echo date('d-m-Y H:i:s', strtotime($patientBooking->completed_date)) ?></dd>
                                    </dl>                      

                                    <?php
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>

                  </div>

                </div>




              </div>

            </div>
          </div>

      </div>
   
    </div>



      <br>


      <form id="form_main" action="" method="post" enctype="multipart/form-data">



          <div class="form-container">
            
            <h4 class="form-group-title">Upload File Details</h4> 



              <div class="row">

                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>Upload File <span class='error-text'>*</span></label>
                          <input type="file" class="form-control" id="upload_file" name="upload_file">
                      </div>
                  </div>

                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>Comments </label>
                          <input type="text" class="form-control" id="comments" name="comments">
                      </div>
                  </div>

              </div>


          </div>

          <div class="button-block clearfix">
              <div class="bttn-group">
                  <button type="submit" class="btn btn-primary btn-lg">Save</button>
                  <a href="../list" class="btn btn-link">Back</a>
              </div>
          </div>

        </form>



        <?php

                if(!empty($getUploadFilesByTestBooking))
                {
            ?>
            <br>

            <div class="form-container">
                    <h4 class="form-group-title">File Uploaded</h4>

                

                  <div class="custom-table">
                    <table class="table">
                        <thead>
                            <tr>
                            <th>Sl. No</th>
                             <th>Comments</th>
                             <th class="text-center">File</th>
                             <th>Date Time</th>
                             <!-- <th class="text-center">Action</th> -->
                            </tr>
                        </thead>
                        <tbody>
                             <?php
                         $total = 0;
                          for($i=0;$i<count($getUploadFilesByTestBooking);$i++)
                         { ?>
                            <tr>
                            <td><?php echo $i+1;?></td>
                            <td><?php echo $getUploadFilesByTestBooking[$i]->comments;?></td> 
                            <td class="text-center">

                                <a href="<?php echo '/assets/images/' . $getUploadFilesByTestBooking[$i]->upload_file; ?>" target="popup" onclick="window.open(<?php echo '/assets/images/' . $getUploadFilesByTestBooking[$i]->upload_file; ?>)" title="<?php echo $getUploadFilesByTestBooking[$i]->upload_file; ?>">View</a>
                            </td>
                            <td><?php echo date('d-m-Y H:i:s', strtotime($getUploadFilesByTestBooking[$i]->created_dt_tm));?></td>

                            </tr>
                          <?php
                      } 
                      ?>
                        </tbody>
                    </table>
                  </div>

                </div>




        <?php
        
        }
         ?>







            <!-- <div class="button-block clearfix">
                        <div class="bttn-group">
                            <button type="button" onclick="validateDetailsData()" class="btn btn-primary btn-lg">Save</button>
                            <a href="../list" class="btn btn-link">Back</a>
                        </div>
            </div> -->

        



        


	 



        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>


<script>
    $('select').select2();

    $(function(){
    $(".datepicker").datepicker(
    {
        changeYear: true,
        changeMonth: true,
    });
    });



    $(document).ready(function()
    {
        $("#form_main").validate({
            rules: {
                upload_file: {
                    required: true
                }
            },
            messages: {
                upload_file: {
                    required: "<p class='error-text'>Select File</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

</script>