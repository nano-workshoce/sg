<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>Test Booking Summary</h3>
      <!-- <a href="add" class="btn btn-primary">+ Add Patient</a> -->
    </div>

    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Advanced Search
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">

                <div class="row">

                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Name</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" name="name" id="name" value="<?php echo $searchParam['name']; ?>">
                      </div>
                    </div>
                  </div>

                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Register No</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" name="register_no" id="register_no" value="<?php echo $searchParam['register_no']; ?>">
                      </div>
                    </div>
                  </div>


                </div>


                <div class="row">


                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Phone</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" name="phone" id="phone" value="<?php echo $searchParam['phone']; ?>">
                      </div>
                    </div>
                  </div>



                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Email Id</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" name="email" id="email" value="<?php echo $searchParam['email']; ?>">
                      </div>
                    </div>
                  </div>


                </div>


                <div class="row">


                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Collection By</label>
                      <div class="col-sm-8">
                        <select name="id_staff" id="id_staff" class="form-control selitemIcon">
                            <option value="">Select</option>
                            <?php
                            if (!empty($staffList))
                            {
                                foreach ($staffList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>"
                              <?php
                              if($record->id == $searchParam['id_staff'])
                              {
                                echo "selected";
                              }
                              ?>
                              >
                                <?php echo $record->name . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                      </div>
                    </div>
                  </div>



                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Status</label>
                      <div class="col-sm-8">
                        <select name="status" id="status" class="form-control selitemIcon">
                           <option value="">Select</option>  
                           <option value="0"
                              <?php
                              if(0 == $searchParam['status'])
                              {
                                echo "selected";
                              }
                              ?>
                              >Pending                                
                             </option>
                            <option value="1"
                              <?php
                              if(1 == $searchParam['status'])
                              {
                                echo "selected";
                              }
                              ?>
                              >Completed                                
                             </option>
                            <option value="2"
                              <?php
                              if(2 == $searchParam['status'])
                              {
                                echo "selected";
                              }
                              ?>
                              >Cancelled                                
                             </option>
                        </select>
                      </div>
                    </div>
                  </div>


                </div>



              </div>
              <div class="app-btn-group">
                <button type="submit" class="btn btn-primary">Search</button>
                <a href="list" class="btn btn-link">Clear All Fields</a>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>

    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl. No</th>
            <th>Reference No.</th>
            <th>Patient Register No.</th>
            <th>Patient Name</th>
            <th>Gender</th>
            <th>Phone No</th>
            <th>Email ID</th>
            <th>DOB</th>
            <th>Collection By</th>
            <th>Date</th>
            <th>Time Slot</th>
            <th class="text-center">Status</th>
            <th class="text-center">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($patientList)) {
            $i=1;
            foreach ($patientList as $record) {
            ?>
              <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $record->reference_number ?></td>
                <td><?php echo $record->register_no ?></td>
                <td><?php echo $record->full_name ?></td>
                <td><?php echo $record->gender ?></td>
                <td><?php echo $record->phone ?></td>
                <td><?php echo $record->email ?></td>
                <td><?php echo date("d-m-Y", strtotime($record->date_of_birth)) ?></td>
                <td><?php echo $record->staff_id . " - " . $record->staff_name ?></td>
                <td><?php echo date('d-m-Y', strtotime($record->appointment_date)) ?></td>
                <td><?php echo $record->time_slot ?></td>
                <td>
                <?php if( $record->status == '0')
                {
                  echo "Pending";
                }
                elseif( $record->status == '1')
                {
                  echo "Completed";
                }
                elseif( $record->status == '2')
                {
                  echo "Cancelled";
                } 
                ?></td>
                <td class="text-center">
                  <?php
                  if( $record->status == '0')
                  {
                  ?>
                    <a href="<?php echo 'view/' . $record->id; ?>" title="Complete / View Appointment">Complete</a>
                  <?php
                  }
                  else
                  {
                    ?>
                    <a href="<?php echo 'edit/' . $record->id; ?>" title="View Appointment">View</a>
                    <?php
                  }
                ?>
                  <!--  -->
                </td>
              </tr>
          <?php
          $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
<script>
    $('select').select2();
</script>