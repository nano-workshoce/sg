<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        

        <div class="page-title clearfix">
            <h3>Complete Patient Appointment</h3>
        </div>



    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Patient Details
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">
          <div id="collapseOne" class="panel-collapse collapse out" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">


              	<div class="form-container">
				    
				    <!-- <h4 class="form-group-title">Patient Details</h4>  -->


                	<div class="row">
                  
                        <div class='data-list'>
                            <div class='row'> 
                                <div class='col-sm-6'>
                                    <dl>
                                        <dt>Patient Name :</dt>
                                        <dd><?php echo ucwords($patient->full_name);?></dd>
                                    </dl>
                                    <dl>
                                        <dt>Patient Phone :</dt>
                                        <dd><?php echo $patient->phone ?></dd>
                                    </dl>
                                    <dl>
                                        <dt>Patient Email :</dt>
                                        <dd><?php echo $patient->email; ?></dd>
                                    </dl>
                                    <dl>
                                        <dt>Patient Gender :</dt>
                                        <dd><?php echo $patient->gender; ?></dd>
                                    </dl>                        
                                </div>        
                                
                                <div class='col-sm-6'>                           
                                    <dl>
                                        <dt>Patient Register No. :</dt>
                                        <dd><?php echo $patient->register_no ?></dd>
                                    </dl>
                                    <dl>
                                        <dt>Address :</dt>
                                        <dd><?php echo $patient->mail_address1; ?></dd>
                                    </dl>
                                    <dl>
                                        <dt>City :</dt>
                                        <dd><?php echo $patient->mailing_city; ?></dd>
                                    </dl>
                                    <dl>
                                        <dt>Patient DOB :</dt>
                                        <dd><?php echo date('d-m-Y', strtotime($patient->date_of_birth)); ?></dd>
                                    </dl>  
                                </div>
                            </div>
                        </div>

                	</div>

                </div>





              </div>

            </div>
          </div>
        </form>
      </div>
   
    </div>







        
    <div class="panel-group advanced-search" id="accordion_two" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingTwo">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion_two" href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
              Test Booking Details
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">
          <div id="collapseTwo" class="panel-collapse collapse out" role="tabpanel" aria-labelledby="headingTwo">
            <div class="panel-body">
              <div class="form-horizontal">

                
                    <?php  
				        if(!empty($patientBookingTestDetails))
				        {
				         ?>



				        <!-- <div class="form-container">
				                <h4 class="form-group-title">Test Details</h4> --> 


				            

				            <div class="custom-table">
				              <table class="table" id="list-table">
				                <thead>
				                  <tr>
				                    <th>Sl. No</th>
				                    <th>Booking Type</th>
				                    <th>Package</th>
				                    <th>Test</th>
				                    <th>Amount</th>
				                  </tr>
				                </thead>
				                <tbody>
				                  <?php
				                  if (!empty($patientBookingTestDetails))
				                  {
				                    $i = 1;
				                    $total = 0;
				                    $total_amount = 0;
				                    foreach ($patientBookingTestDetails as $record)
				                    {
				                  ?>
				                      <tr>
				                        <td><?php echo $i ?></td>
				                        <td><?php echo $record->booking_type ?></td>
				                        <td><?php echo $record->test_code . " - " . $record->test_name ?></td>
				                        <td><?php echo $record->package_code . " - " . $record->package_name ?></td>
				                        <td><?php echo $record->amount ?></td>
				                      </tr>
				                  <?php
				                  $total = $total + $record->amount;
				                  $i++;
				                    }
				                     $total = number_format($total, 2, '.', ',');
				                    ?>

				                    <tr >
				                        <td bgcolor="" colspan="3"></td>
				                        <td bgcolor="" style="text-align: center;"><b>Total Amount :</b></td>
				                        <td bgcolor="" ><b> <?php echo $total ?></b></td>
				                      </tr>
				                    <?php
				                  }
				                  ?>
				                </tbody>
				              </table>
				            </div>

				        <!-- </div> -->


			        <?php  
			       
			        }
			       
			        ?>




              </div>

            </div>
          </div>
        </form>
      </div>
    </div>







    <div class="panel-group advanced-search" id="accordion_three" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingThree">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion_three" href="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
              Sample Collection Details
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">
          <div id="collapseThree" class="panel-collapse collapse out" role="tabpanel" aria-labelledby="headingThree">
            <div class="panel-body">
              <div class="form-horizontal">

                
	                <?php  
				        if(!empty($patientBookingSampleDetails))
				        {
				         ?>



				        <!-- <div class="form-container">
				                <h4 class="form-group-title">Sample Collection Details</h4>  -->


				            

				            <div class="custom-table">
				              <table class="table" id="list-table">
				                <thead>
				                  <tr>
				                    <th>Sl. No</th>
				                    <th>Sample</th>
				                  </tr>
				                </thead>
				                <tbody>
				                  <?php
				                  if (!empty($patientBookingSampleDetails))
				                  {
				                    $i = 1;
				                    $total = 0;
				                    $total_amount = 0;
				                    foreach ($patientBookingSampleDetails as $record)
				                    {
				                  ?>
				                      <tr>
				                        <td><?php echo $i ?></td>
				                        <td><?php echo $record->sample_code . " - " . $record->sample_name ?></td>
				                      </tr>
				                  <?php
				                  $i++;
				                    }
				                    ?>
				                    <?php
				                  }
				                  ?>
				                </tbody>
				              </table>
				            </div>

				        <!-- </div> -->


			        <?php  
			       
			        }
			       
			        ?>


                <div id="view_temp_sample">
                  
                </div>


              </div>

            </div>
          </div>
        </form>
      </div>
    </div>
        





    <br>
        



        <form id="form_sponser" action="" method="post">


            <div class="form-container">

                <h4 class="form-group-title">Appointment Details</h4> 

                <div class="row">


                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>Booking Reference No. <span class='error-text'>*</span></label>
                          <input type="text" class="form-control" id="reference_number" name="reference_number" autocomplete="off" value="<?php echo $patientBooking->reference_number; ?>" readonly>
                      </div>
                  </div>


                  
                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>Appointment Date <span class='error-text'>*</span></label>
                          <input type="text" class="form-control datepicker" id="appointment_date" name="appointment_date" autocomplete="off" value="<?php echo date('d-m-Y', strtotime($patientBooking->appointment_date)) ?>" readonly>
                      </div>
                  </div>


                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>Appointment Time <span class='error-text'>*</span></label>
                          <input type="text" class="form-control datepicker" id="appointment_date" name="appointment_date" autocomplete="off" value="<?php echo $patientBooking->time_slot ?>" readonly>
                      </div>
                  </div>


                </div>


                <div class="row">


                  <div class="col-sm-4">
                    <div class="form-group">
                        <label>Sample Collection By <span class='error-text'>*</span></label>
                        <select name="id_staff" id="id_staff" class="form-control" disabled>
                            <option value="">Select</option>
                            <?php
                            if (!empty($staffList))
                            {
                                foreach ($staffList as $record)
                                {?>
                                 <option value="<?php echo $record->id;  ?>"
                                 	<?php
                                 	if($patientBooking->id_staff == $record->id)
                                 	{
                                 		echo 'selected';
                                 	}
                                 	?>>
                                    <?php echo $record->staff_id . " - " . $record->name;?>
                                 </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                  </div>


                  
                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>Total Amount <span class='error-text'>*</span></label>
                          <input type="text" class="form-control" id="amount" name="amount" value="<?php echo $patientBooking->amount ?>" readonly>
                      </div>
                  </div>


                  <div class="col-sm-4" id="view_payable_amount">
                      <div class="form-group">
                          <label>Payment Amount <span class='error-text'>*</span></label>
                          <input type="text" class="form-control" id="paid_amount" name="paid_amount" value="<?php echo $patientBooking->amount ?>">
                      </div>
                  </div>




                </div>


                <div class="row">



                  <div class="col-sm-4" id="view_reason" style="display: none;">
                      <div class="form-group">
                          <label>Reason <span class='error-text'>*</span></label>
                          <input type="text" class="form-control" id="reason" name="reason" value="">
                      </div>
                  </div>



                  <div class="col-sm-4">
                    <div class="form-group">
                        <label>Status <span class='error-text'>*</span></label>
                        <select name="status" id="status" class="form-control" onchange="showComplete(this.value)">
                            <option value="">Select</option>
                            <option value="1">Complete</option>
                            <option value="2">Cancel</option>                           
                        </select>
                    </div>
                  </div>






                  <div class="col-sm-4" id="view_payment_type">
                    <div class="form-group">
                        <label>Payment Type <span class='error-text'>*</span></label>
                        <select name="payment_mode" id="payment_mode" class="form-control">
                            <option value="">Select</option>
                            <option value="Cash">Cash</option>
                            <option value="UPI">UPI</option>                           
                        </select>
                    </div>
                  </div>

                  
                  <div class="col-sm-4" id="view_comments">
                      <div class="form-group">
                          <label>Comments <span class='error-text'>*</span></label>
                          <input type="text" class="form-control" id="comments" name="comments" value="">
                      </div>
                  </div>


              </div>



	            <div class="row">

                  
                  
	            </div>


            </div>


            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="button" onclick="validateDetailsData()" class="btn btn-primary btn-lg">Save</button>
                    <a href="../list" class="btn btn-link">Back</a>
                </div>
            </div>


        </form>


	      <div id="myModal" class="modal fade" role="dialog">
	          <div class="modal-dialog modal-lg">

	            <!-- Modal content-->
	            <div class="modal-content">
	              <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal">&times;</button>
	                <h4 class="modal-title">Complete Appointment Payment Confirmation</h4>
	              </div>

	              <div class="modal-body">

	                <br>

	                <form id="form_four" action="" method="post">


	                  <div class="form-container">
	                    <h4 class="form-group-title">Test Booking Details</h4>

	                    
	                    

	                    <div class="row">
	                        
	                        <div class='data-list'>

	                            <div class='row'> 

	                                <div class='col-sm-6'>
	                                    <dl>
	                                        <dt>Patient Name :</dt>
	                                        <dd><?php echo ucwords($patient->full_name);?></dd>
	                                    </dl>
	                                    <dl>
	                                        <dt>Booking Reference No. :</dt>
	                                        <dd><?php echo $patientBooking->reference_number ?></dd>
	                                    </dl>
	                                    <dl>
	                                        <dt>Test Booking Date :</dt>
	                                        <dd><?php echo date('d-m-Y', strtotime($patientBooking->appointment_date)); ?></dd>
	                                    </dl> 

	                                    <dl>
	                                        <dt>Test Total Amount :</dt>
	                                        <dd><?php echo $patientBooking->amount; ?></dd>
	                                    </dl> 
	                                        
	                                    <dl>
	                                        <dt>Patient Phone :</dt>
	                                        <dd><?php echo $patient->phone ?></dd>
	                                    </dl>
	                                    <dl>
	                                        <dt>Patient Email :</dt>
	                                        <dd><?php echo $patient->email; ?></dd>
	                                    </dl>
	                                    <dl>
	                                        <dt>Patient Gender :</dt>
	                                        <dd><?php echo $patient->gender; ?></dd>
	                                    </dl>	                                   
	                                    <dl>
	                                        <dt>Patient DOB :</dt>
	                                        <dd><?php echo date('d-m-Y', strtotime($patient->date_of_birth)); ?></dd>
	                                    </dl>	                                                     
	                                </div> 

	                                
	                                <div class='col-sm-6'>                           
	                                    <dl>
	                                        <dt>Patient Register No. :</dt>
	                                        <dd><?php echo $patient->register_no ?></dd>
	                                    </dl>
	                                    <dl>
	                                        <dt>Payment Mode :</dt>
	                                        <dd><input type='text' name='test_booking_payment_mode' id='test_booking_payment_mode' readonly>
	                                    </dl>
	                                    <dl>
	                                        <dt>Collection By :</dt>
	                                        <dd><?php echo $patientBooking->staff_id . " - " . $patientBooking->staff_name ?></dd>
	                                    </dl>
	                                    <dl>
	                                        <dt>Payable Amount :</dt>
	                                        <dd><input type='text' name='test_booking_paid_amount' id='test_booking_paid_amount' readonly>
	                                    </dl> 
	                                    <dl>
	                                        <dt>Test Time Slot :</dt>
	                                        <dd><?php echo $patientBooking->time_slot; ?></dd>
	                                    </dl> 
	                                    <dl>
	                                        <dt>Address :</dt>
	                                        <dd><?php echo $patient->mail_address1; ?></dd>
	                                    </dl>
	                                    <dl>
	                                        <dt>City :</dt>
	                                        <dd><?php echo $patient->mailing_city; ?></dd>
	                                    </dl>	                                    
	                                </div>
	                            </div>

	                        </div>

	                    </div>


	                    <div class="row">
	                          <div id='view_model'>
	                          </div>
	                    </div>




	                  </div>
	              
	              </form>

	              <div class="modal-footer">
	                  <button type="button" class="btn btn-default" onclick="submitForm()">Save</button>
	                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	              </div>








	                <div class="form-container">
	                        
	               



	                </div>

	            </div>
	            </div>

	          </div>
	      
	      </div>



        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>


<script>
    $('select').select2();

    $(function(){
    $(".datepicker").datepicker(
    {
        changeYear: true,
        changeMonth: true,
    });
    });



    function showComplete(status)
    {
      if(status == '1')
      {
            $("#view_payable_amount").show();
            $("#view_payment_type").show();
            $("#view_comments").show();
            $("#view_reason").hide();
      }
      else if(status == '2')
      {
            $("#view_payable_amount").hide();
            $("#view_payment_type").hide();
            $("#view_comments").hide();
            $("#view_reason").show();
      }
    }


    function validateDetailsData()
    {
      if($('#form_sponser').valid())
        {    
            showConfirmation();   
        }    
    }


    function showConfirmation()
    {
        // alert(result);
        $('#myModal').modal('show');
        // $("#view_model").html(result);

        var paid_amount = $("#paid_amount").val();
        var payment_mode = $("#payment_mode").val();
        // alert(appointment_date);
        // alert(time_slot);
        $("#test_booking_payment_mode").val(payment_mode);
        $("#test_booking_paid_amount").val(paid_amount);
    }

    function submitForm()
    {
      $('#form_sponser').submit();
    }



    $(document).ready(function()
    {
        $("#form_sponser").validate({
            rules: {
                status: {
                    required: true
                },
                paid_amount: {
                    required: true
                },
                payment_mode: {
                  required: true
                },
                reason: {
                    required: true
                },
                amount: {
                  required: true
                }
            },
            messages: {
                status: {
                    required: "<p class='error-text'>Select Status</p>",
                },
                paid_amount: {
                    required: "<p class='error-text'>Paid Amount Required</p>",
                },
                payment_mode: {
                    required: "<p class='error-text'>Select Payment Mode</p>",
                },
                reason: {
                    required: "<p class='error-text'>Reason Required</p>",
                },
                amount: {
                    required: "<p class='error-text'>Amount Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });




    $(document).ready(function()
    {
        $("#form_samples").validate({
            rules: {
                id_sample: {
                    required: true
                }
            },
            messages: {
                id_sample: {
                    required: "<p class='error-text'>Select Sample</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });




    $(document).ready(function()
    {
        $("#form_tests").validate({
            rules: {
                booking_type: {
                    required: true
                },
                id_package: {
                  required: true
                },
                id_test: {
                  required: true
                }
            },
            messages: {
                booking_type: {
                    required: "<p class='error-text'>Select Booking Type</p>",
                },
                id_package: {
                    required: "<p class='error-text'>Select Package</p>",
                },
                id_test: {
                    required: "<p class='error-text'>Select Tests</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });




</script>