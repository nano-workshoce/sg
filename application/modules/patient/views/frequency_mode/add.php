<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Frequency Mode</h3>
        </div>
        <form id="form_frequency_mode" action="" method="post">

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Frequency Mode Code *</label>
                        <input type="text" class="form-control" id="code" name="code">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name *</label>
                        <input type="text" class="form-control" id="name" name="name">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name (Optional Language) *</label>
                        <input type="text" class="form-control" id="name_optional_language" name="name_optional_language">
                    </div>
                </div>
            </div>


            <div class="row">

                <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status *</p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" checked="checked"><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0"><span class="check-radio"></span> Inactive
                            </label>                              
                        </div>                         
                </div>
            </div>

            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="list" class="btn btn-link">Cancel</a>
                </div>
            </div>
            
        </form>

    </div>

        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
</div>
<script>
    $(document).ready(function() {
        $("#form_frequency_mode").validate({
            rules: {
                code: {
                    required: true
                },
                name: {
                    required: true
                },
                name_optional_language: {
                    required: true
                }
            },
            messages: {
                code: {
                    required: "Code Required",
                },
                name: {
                    required: "Name Required",
                },
                name_optional_language: {
                    required: "Name Optional Required",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
