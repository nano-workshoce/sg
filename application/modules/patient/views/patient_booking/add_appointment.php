<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Patient Appointment</h3>
        </div>



  <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Patient Details
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">

                <div class="row">
                  
                        <div class='data-list'>
                            <div class='row'> 
                                <div class='col-sm-6'>
                                    <dl>
                                        <dt>Patient Name :</dt>
                                        <dd><?php echo ucwords($patient->full_name);?></dd>
                                    </dl>
                                    <dl>
                                        <dt>Patient Phone :</dt>
                                        <dd><?php echo $patient->phone ?></dd>
                                    </dl>
                                    <dl>
                                        <dt>Patient Email :</dt>
                                        <dd><?php echo $patient->email; ?></dd>
                                    </dl>
                                    <dl>
                                        <dt>Patient Gender :</dt>
                                        <dd><?php echo $patient->gender; ?></dd>
                                    </dl>                        
                                </div>        
                                
                                <div class='col-sm-6'>                           
                                    <dl>
                                        <dt>Patient Register No. :</dt>
                                        <dd><?php echo $patient->register_no ?></dd>
                                    </dl>
                                    <dl>
                                        <dt>Address :</dt>
                                        <dd><?php echo $patient->mail_address1; ?></dd>
                                    </dl>
                                    <dl>
                                        <dt>City :</dt>
                                        <dd><?php echo $patient->mailing_city; ?></dd>
                                    </dl>
                                    <dl>
                                        <dt>Patient DOB :</dt>
                                        <dd><?php echo date('d-m-Y', strtotime($patient->date_of_birth)); ?></dd>
                                    </dl>  
                                </div>
                            </div>
                        </div>

                </div>

              </div>

            </div>
          </div>
        </form>
      </div>
    </div>
















        
    <div class="panel-group advanced-search" id="accordion_two" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingTwo">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion_two" href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
              Test Booking Details
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">
          <div id="collapseTwo" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">
            <div class="panel-body">
              <div class="form-horizontal">

                
                    <form id="form_tests" action="" method="post">


                      <div class="form-container">

                          <h4 class="form-group-title">Tests Details</h4> 


                          <div class="row">


                            <div class="col-sm-4">
                              <div class="form-group">
                                <label>Booking Type <span class='error-text'>*</span></label>
                                <br>
                                <div class="col-sm-8">
                                  <select name="booking_type" id="booking_type" class="form-control" onchange="showBookingType(this.value)">
                                     <option value="">Select</option>
                                      <option value="Package">Package</option>
                                      <option value="Individual">Individual</option>
                                  </select>
                                </div>
                              </div>
                            </div>





                            <div class="col-sm-4" id="view_package">
                              <div class="form-group">
                                <label>Package <span class='error-text'>*</span></label>
                                <br>
                                <div class="col-sm-8">
                                  <select name="id_package" id="id_package" class="form-control">
                                      <option value="">Select</option>
                                      <?php
                                      if (!empty($packageList))
                                      {
                                          foreach ($packageList as $record)
                                          {?>
                                           <option value="<?php echo $record->id;  ?>">
                                              <?php echo $record->code . " - " . $record->name;?>
                                           </option>
                                      <?php
                                          }
                                      }
                                      ?>
                                  </select>
                                </div>
                              </div>
                            </div>



                            <div class="col-sm-4" id="view_test">
                              <div class="form-group">
                                  <label>Tests <span class='error-text'>*</span></label>
                                <br>
                                <div class="col-sm-8">
                                  <select name="id_test" id="id_test" class="form-control">
                                      <option value="">Select</option>
                                      <?php
                                      if (!empty($testList))
                                      {
                                          foreach ($testList as $record)
                                          {?>
                                           <option value="<?php echo $record->id;  ?>">
                                              <?php echo $record->code . " - " . $record->name;?>
                                           </option>
                                      <?php
                                          }
                                      }
                                      ?>
                                  </select>
                                </div>
                              </div>
                            </div>



                          </div>


                      </div>

                      <div class="button-block clearfix">
                          <div class="bttn-group">
                              <button type="button" onclick="saveTests()" class="btn btn-primary btn-lg">Add</button>
                          </div>
                      </div>


                  </form>


                  <div id="view_temp_test">
                  
                  </div>


              </div>

            </div>
          </div>
        </form>
      </div>
    </div>





    <br>






    <div class="panel-group advanced-search" id="accordion_three" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingThree">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion_three" href="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
              Sample Collection Details
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">
          <div id="collapseThree" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingThree">
            <div class="panel-body">
              <div class="form-horizontal">

                
                <form id="form_samples" action="" method="post">


                    <div class="form-container">

                        <h4 class="form-group-title">Sample Collection Details</h4> 

                        <div class="row">




                          <div class="col-sm-4" id="view_package">
                              <div class="form-group">
                                <label>Samples <span class='error-text'>*</span></label>
                                <br>
                                <div class="col-sm-8">
                                  <select name="id_sample" id="id_sample" class="form-control">
                                    <option value="">Select</option>
                                    <?php
                                    if (!empty($samplesList))
                                    {
                                        foreach ($samplesList as $record)
                                        {?>
                                         <option value="<?php echo $record->id;  ?>">
                                            <?php echo $record->code . " - " . $record->name;?>
                                         </option>
                                    <?php
                                        }
                                    }
                                    ?>
                                </select>
                                </div>
                              </div>
                            </div>
                        

                        </div>


                    </div>


                    <div class="button-block clearfix">
                        <div class="bttn-group">
                            <button type="button" onclick="saveSamples()" class="btn btn-primary btn-lg">Add</button>
                        </div>
                    </div>


                </form>


                <div id="view_temp_sample">
                  
                </div>


              </div>

            </div>
          </div>
        </form>
      </div>
    </div>
        





    <br>
        



        <form id="form_sponser" action="" method="post">


            <div class="form-container">

                <h4 class="form-group-title">Appointment Details</h4> 

                <div class="row">
                  
                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>Appointment Date <span class='error-text'>*</span></label>
                          <input type="text" class="form-control datepicker" id="appointment_date" name="appointment_date" autocomplete="off">
                          <input type="hidden" class="form-control" id="id_patient" name="id_patient" value="<?php echo $patient->id; ?>">
                          <input type='hidden' name='total_test_amount' id='total_test_amount'/>
                      </div>
                  </div>


                  <div class="col-sm-4">
                     <div class="form-group">
                        <label>Time Slot <span class='error-text'>*</span></label>
                        <select name="time_slot" id="time_slot" class="form-control">
                           <option value="">Select</option>
                            <!-- <option value="6:00 - 6:30">6:00 - 6:30 (AM)</option> -->
                            <option value="6:30 - 7:00">6:30 - 7:00 (AM)</option>
                            <option value="7:00 - 7:30">7:00 - 7:30 (AM)</option>
                            <option value="7:30 - 8:00">7:30 - 8:00 (AM)</option>
                            <option value="8:00 - 8:30">8:00 - 8:30 (AM)</option>
                            <option value="8:30 - 9:00">8:30 - 9:00 (AM)</option>
                            <option value="9:30 - 10:00">9:30 - 10:00 (AM)</option>
                            <option value="10:00 - 10:30">10:00 - 10:30 (AM)</option>
                            <option value="10:30 - 11:00">10:30 - 11:00 (AM)</option>
                            <option value="11:00 - 11:30">11:00 - 11:30 (AM)</option>
                        </select>
                     </div>
                  </div>


                  <div class="col-sm-4">
                    <div class="form-group">
                        <label>Sample Collection By <span class='error-text'>*</span></label>
                        <select name="id_staff" id="id_staff" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($staffList))
                            {
                                foreach ($staffList as $record)
                                {?>
                                 <option value="<?php echo $record->id;  ?>">
                                    <?php echo $record->staff_id . " - " . $record->name;?>
                                 </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                  </div>


                </div>


            </div>




                <div class="button-block clearfix">
                    <div class="bttn-group">
                        <button type="button" onclick="validateDetailsData()" class="btn btn-primary btn-lg">Save</button>
                        <a href="../list" class="btn btn-link">Back</a>
                    </div>
                </div>


            </div>


        </form>


      <div id="myModal" class="modal fade" role="dialog">
          <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <!-- <h4 class="modal-title">Program Landscape</h4> -->
              </div>

              <div class="modal-body">

                <br>

                <form id="form_four" action="" method="post">


                  <div class="form-container">
                    <h4 class="form-group-title">Test Booking Details</h4>

                    
                    

                    <div class="row">
                        
                        <div class='data-list'>
                            <div class='row'> 
                                <div class='col-sm-6'>
                                    <dl>
                                        <dt>Patient Name :</dt>
                                        <dd><?php echo ucwords($patient->full_name);?></dd>
                                    </dl>
                                    <dl>
                                        <dt>Patient Phone :</dt>
                                        <dd><?php echo $patient->phone ?></dd>
                                    </dl>
                                    <dl>
                                        <dt>Patient Email :</dt>
                                        <dd><?php echo $patient->email; ?></dd>
                                    </dl>
                                    <dl>
                                        <dt>Patient Gender :</dt>
                                        <dd><?php echo $patient->gender; ?></dd>
                                    </dl>  

                                    <dl>
                                        <dt>Test Booking Date :</dt>
                                        <dd><input type='text' name='test_booking_date' id='test_booking_date' readonly>
                                        </dd>
                                    </dl> 
                                    <dl>
                                        <dt>Collection By :</dt>
                                        <dd><input type='text' name='test_sample_collection_by' id='test_sample_collection_by' readonly>
                                        </dd>
                                    </dl>                        
                                </div>        
                                
                                <div class='col-sm-6'>                           
                                    <dl>
                                        <dt>Patient Register No. :</dt>
                                        <dd><?php echo $patient->register_no ?></dd>
                                    </dl>
                                    <dl>
                                        <dt>Address :</dt>
                                        <dd><?php echo $patient->mail_address1; ?></dd>
                                    </dl>
                                    <dl>
                                        <dt>City :</dt>
                                        <dd><?php echo $patient->mailing_city; ?></dd>
                                    </dl>
                                    <dl>
                                        <dt>Patient DOB :</dt>
                                        <dd><?php echo date('d-m-Y', strtotime($patient->date_of_birth)); ?></dd>
                                    </dl>  
                                    <dl>
                                        <dt>Test Time Slot :</dt>
                                        <dd><input type='text' name='test_booking_time_slot' id='test_booking_time_slot' readonly>
                                    </dl> 
                                    <dl>
                                        <dt>Test Total Amount :</dt>
                                        <dd><input type='text' name='test_booking_amount' id='test_booking_amount' readonly>
                                    </dl> 
                                </div>
                            </div>
                        </div>

                    </div>


                    <div class="row">
                          <div id='view_model'>
                          </div>
                    </div>




                  </div>
              
              </form>

              <div class="modal-footer">
                  <button type="button" class="btn btn-default" onclick="submitForm()">Add</button>
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>








                <div class="form-container">
                        
               



                </div>

            </div>
            </div>

          </div>
      
      </div>



        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>
    $('select').select2();

    $(function(){
    $(".datepicker").datepicker(
    {
        changeYear: true,
        changeMonth: true,
    });
    });



    function showBookingType(booking_type)
    {
      if(booking_type == 'Package')
      {
            $("#view_package").show();
            $("#view_test").hide();
      }
      else if(booking_type == 'Individual')
      {
            $("#view_package").hide();
            $("#view_test").show();
      }
    }


    function saveTests()
    {
      // if($('#form_tests').valid())
      // {
        var booking_type = $("#booking_type").val();
        var id_package = $("#id_package").val();
        var id_test = $("#id_test").val();
        // alert(booking_type);

        if(booking_type != '')
        {
          
          // var id_selected =0;
          // if(booking_type == 'Package')
          // {
          //   $("#id_test").find('option[value="'+id_selected+'"]').attr('selected',true);
          //   $('select').select2();
          // }

          // if(booking_type == 'Individual')
          // {
          //   $("#id_package").find('option[value="'+id_selected+'"]').attr('selected',true);
          //   $('select').select2();
          // }

          

        var tempPR = {};
        tempPR['booking_type'] = booking_type;
        tempPR['id_package'] = id_package;
        tempPR['id_test'] = id_test;

            $.ajax(
            {
               url: '/patient/patientBooking/addTempTestBooking',
               type: 'POST',
               data:
               {
                data: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                if(result == '')
                {
                    alert('No Debit Budget GL Found For Entered Data')
                }
                else
                {
                    $("#view_temp_test").html(result);
                    var total_test_amount = $("#total_detail").val();
                    $("#total_test_amount").val(total_test_amount);

                }
               }
            });
        }
      // }
    }

    function deleteTempTestBookingDetails(id)
    {

        $.get("/patient/patientBooking/deleteTempTestBookingDetails/"+id, function(data, status)
        {
            $("#view_temp_test").html(data);
            var total_test_amount = $("#total_detail").val();
            $("#total_test_amount").val(total_test_amount);
        });
    }





    function saveSamples()
    {
      // if($('#form_samples').valid())
      // {

        var id_sample = $("#id_sample").val();

        if(id_sample != '')
        {

        var tempPR = {};
        tempPR['id_sample'] = id_sample;

            $.ajax(
            {
               url: '/patient/patientBooking/addTempSampleBooking',
               type: 'POST',
               data:
               {
                data: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                if(result == '')
                {
                    alert('No Debit Budget GL Found For Entered Data')
                }
                else
                {
                    $("#view_temp_sample").html(result);
                }
               }
            });
        }
      // }
    }
    
    function deleteTempSampleBookingDetails(id)
    {

        $.get("/patient/patientBooking/deleteTempSampleBookingDetails/"+id, function(data, status)
        {
            $("#view_temp_sample").html(data);
        });
    }


    function validateDetailsData()
    {
      if($('#form_sponser').valid())
        {
            var addedTest = $("#view_temp_test").html();
            var addedSample = $("#view_temp_sample").html();
            console.log(addedTest);
            console.log(addedSample);
            if(addedTest=='' || addedTest == undefined)
            {
                alert("Add Test Details");
            }
            else if(addedSample == '' || addedSample == undefined)
            {
                alert("Add Sample Collection Details");
            }
            else
            {
                showConfirmation();
            }
        }    
    }


    function showConfirmation()
    {
      var id_staff = $("#id_staff").val();
      var tempPR = {};
      tempPR['id_staff'] = id_staff;
      $.ajax(
            {
               url: '/patient/patientBooking/showConfirmation',
               type: 'POST',
               data:
               {
                data: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    // alert(result);
                    $('#myModal').modal('show');
                    $("#view_model").html(result);

                    var time_slot = $("#time_slot").val();
                    var appointment_date = $("#appointment_date").val();
                    var total_amount = $("#total_detail").val();
                    var staff = $("#staff").val();

                    // alert(appointment_date);
                    // alert(time_slot);

                    $("#test_booking_date").val(appointment_date);
                    $("#test_booking_time_slot").val(time_slot);
                    $("#test_sample_collection_by").val(staff);
                    $("#test_booking_amount").val(total_amount);

               }
            });

    }

    function submitForm()
    {
      $('#form_sponser').submit();
    }



    $(document).ready(function()
    {
        $("#form_sponser").validate({
            rules: {
                appointment_date: {
                    required: true
                },
                time_slot: {
                    required: true
                },
                id_staff: {
                  required: true
                }
            },
            messages: {
                appointment_date: {
                    required: "<p class='error-text'>Select Appointment Date</p>",
                },
                time_slot: {
                    required: "<p class='error-text'>Select Time Slot</p>",
                },
                id_staff: {
                    required: "<p class='error-text'>Select Sample Collection Staff</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });




    $(document).ready(function()
    {
        $("#form_samples").validate({
            rules: {
                id_sample: {
                    required: true
                }
            },
            messages: {
                id_sample: {
                    required: "<p class='error-text'>Select Sample</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });




    $(document).ready(function()
    {
        $("#form_tests").validate({
            rules: {
                booking_type: {
                    required: true
                },
                id_package: {
                  required: true
                },
                id_test: {
                  required: true
                }
            },
            messages: {
                booking_type: {
                    required: "<p class='error-text'>Select Booking Type</p>",
                },
                id_package: {
                    required: "<p class='error-text'>Select Package</p>",
                },
                id_test: {
                    required: "<p class='error-text'>Select Tests</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });




</script>