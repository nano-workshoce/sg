<?php $this->load->helper("form"); ?>
<form id="form_performa_invoice" action="" method="post">
    <div class="container-fluid page-wrapper">
        <div class="main-container clearfix">
            <div class="page-title clearfix">
                <h3>Add Performa Invoice</h3>
            </div>

                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Type Of Invoice *</label>
                            <select name="type_of_invoice" id="type_of_invoice" class="form-control">
                                <option value="">Select</option>
                                <option value="Student">Student</option>
                                <option value="Applicant">Applicant</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Performa Number *</label>
                            <input type="text" class="form-control" id="invoice_number" name="invoice_number">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Total Amount *</label>
                            <input type="number" class="form-control" id="total_amount" name="total_amount">
                        </div>
                    </div>
                </div>

                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Select Student *</label>
                            <select name="id_student" id="id_student" class="form-control">
                                <option value="">Select</option>
                                <?php
                                if (!empty($studentList))
                                {
                                    foreach ($studentList as $record)
                                    {?>
                                        <option value="<?php echo $record->id;?>"
                                        ><?php echo $record->name;?>
                                        </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Select Application *</label>
                            <select name="id_application" id="id_application" class="form-control">
                                <option value="1">Application1</option>
                                <option value="2">Application2</option>
                                <!-- <?php
                                if (!empty($applicationList))
                                {
                                    foreach ($applicationList as $record)
                                    {?>
                                        <option value="<?php echo $record->id;?>"
                                        ><?php echo $record->name;?>
                                        </option>
                                <?php
                                    }
                                }
                                ?> -->
                            </select>
                        </div>
                    </div>

                     <div class="col-sm-4">
                        <div class="form-group">
                            <label>Date Time *</label>
                            <input type="text" class="form-control" id="date_time" name="date_time">
                        </div>
                    </div>
                </div>


                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Remarks *</label>
                            <input type="text" class="form-control" id="remarks" name="remarks">
                        </div>
                    </div>

                    <div class="col-sm-4">
                            <div class="form-group">
                                <p>Status *</p>
                                <label class="radio-inline">
                                  <input type="radio" name="status" id="status" value="1" checked="checked"><span class="check-radio"></span> Active
                                </label>
                                <label class="radio-inline">
                                  <input type="radio" name="status" id="status" value="0"><span class="check-radio"></span> Inactive
                                </label>                              
                            </div>                         
                    </div>
                </div>

                <div class="button-block clearfix">
                    <div class="bttn-group">
                        <button type="submit" class="btn btn-primary btn-lg">Save</button>
                        <a href="list" class="btn btn-link">Cancel</a>
                    </div>
                </div>
        </div>

        <div class="main-container clearfix">
            <div class="page-title clearfix">
                <h4>Performa Invoice Details</h4>
            </div>
                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Fee Item *</label>
                            <select name="id_fee_item" id="id_fee_item" class="form-control">
                                <option value="">Select</option>
                                <?php
                                if (!empty($feeSetupList))
                                {
                                    foreach ($feeSetupList as $record)
                                    {?>
                                        <option value="<?php echo $record->id;?>"
                                        ><?php echo $record->name;?>
                                        </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Amount *</label>
                            <input type="text" class="form-control" id="amount" name="amount">
                        </div>
                    </div>


                    <div class="col-sm-4">
                        <div class="form-group">
                            <div class="button-block clearfix">
                                <div class="bttn-group">
                                    <button type="button" class="btn btn-primary btn-lg">Add</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="view">     
                </div>

               
                <footer class="footer-wrapper">
                    <p>&copy; 2019 All rights, reserved</p>
                </footer>

        </div>
    </div>
</form>

<script>
    $(document).ready(function() {
        $("#form_performa_invoice").validate({
            rules: {
                type_of_invoice: {
                    required: true
                },
                performa_number: {
                    required: true
                },
                total_amount: {
                    required: true
                },
                id_student: {
                    required: true
                },
                id_application: {
                    required: true
                },
                date_time:{
                    required: true
                },
                remarks: {
                    required: true
                },
                status: {
                    required: true
                }
            },
            messages: {
                type_of_invoice: {
                    required: "Select Type Of Invoice",
                },
                performa_number: {
                    required: "Enter Performa Number",
                },
                total_amount: {
                    required: "Enter Total Amount",
                },
                id_student: {
                    required: "Select Student",
                },
                id_application: {
                    required: "Select Application",
                },
                date_time: {
                    required: "Enter Remarks",
                },
                remarks: {
                    required: "Enter Remarks",
                },
                status: {
                    required: "Status required",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
<script type="text/javascript">
    $("button").click(function()
    {

       var id_fee_item = $("select[name='id_fee_item']").val();
       var amount = $("input[name='amount']").val();

        $.ajax(
        {
           url: '/finance/performaInvoice/tempadd',
           type: 'POST',
           data:
           {
            id_fee_item: id_fee_item,
            amount: amount
           },
           error: function()
           {
            alert(data);
            alert('Something is wrong');
           },
           success: function(result)
           {
            $("#view").html(result);
            //alert("Record added successfully");  
           }
        });
    });
</script>
