<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class BookingSummary extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('booking_summary_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('patient.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['email'] = $this->security->xss_clean($this->input->post('email'));
            $formData['phone'] = $this->security->xss_clean($this->input->post('phone'));
            $formData['register_no'] = $this->security->xss_clean($this->input->post('register_no'));
            $formData['id_staff'] = $this->security->xss_clean($this->input->post('id_staff'));
            $formData['status'] = $this->security->xss_clean($this->input->post('status'));
            if($formData['status'] =='')
            {
                $formData['status'] = 0;
            }
            $data['searchParam'] = $formData;


            $data['patientList'] = $this->booking_summary_model->patientListSearch($formData);
            $data['staffList'] = $this->booking_summary_model->staffListByStatus('1');

            // echo "<Pre>"; print_r($formData);exit;

            $this->global['pageTitle'] = 'Campus Management System : Patient List';
            $this->loadViews("booking_summary/list", $this->global, $data, NULL);
        }
    }

    function view($id = NULL)
    {
        if ($this->checkAccess('patient.view_appoimntment') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            
            if ($id == null)
            {
                redirect('/patient/bookingSummary/list');
            }

            $id_user = $this->session->userId;
            $id_session = $this->session->my_session_id;

            if($this->input->post())
            {

                // echo "<Pre>";print_r($this->input->post());exit();


                $status = $this->security->xss_clean($this->input->post('status'));
                $reason = $this->security->xss_clean($this->input->post('reason'));
                $paid_amount = $this->security->xss_clean($this->input->post('paid_amount'));
                $payment_mode = $this->security->xss_clean($this->input->post('payment_mode'));
                $comments = $this->security->xss_clean($this->input->post('comments'));

                if($status == 2)
                {
                    $paid_amount = '0';
                    $payment_mode = '';
                    $comments = '';

                }
                elseif($status == 1)
                {
                    $reason = '';
                }

                // $current_time = rand(10,99) .  date('hisdmY');

                $data = array(
                    'paid_amount' => $paid_amount,
                    'completed_date' => date('Y-m-d H:i:s'),
                    'payment_mode' => $payment_mode,
                    'comments' => $comments,
                    'reason' => $reason,
                    'status' => $status,
                    'updated_by' => $id_user
                );
                // echo "<Pre>";print_r($this->input->post());exit();
                
                $id_test_booking = $this->booking_summary_model->editPatientBooking($data,$id);
                
                redirect('/patient/bookingSummary/list');
            }

            $data['staffList'] = $this->booking_summary_model->staffListByStatus('1');
            $data['patientBooking'] = $this->booking_summary_model->getPatientBooking($id);
            $data['patientBookingTestDetails'] = $this->booking_summary_model->getPatientBookingTestDetails($id);
            $data['patientBookingSampleDetails'] = $this->booking_summary_model->getPatientBookingSampleDetails($id);
            $data['patient'] = $this->booking_summary_model->getPatient($data['patientBooking']->id_patient);

            // echo "<Pre>"; print_r($data['patient']);exit;

            $this->global['pageTitle'] = 'Campus Management System : Edit Patient';
            $this->loadViews("booking_summary/view", $this->global, $data, NULL);
        }
    }



    function edit($id = NULL)
    {
        if ($this->checkAccess('patient.view_appoimntment') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            
            if ($id == null)
            {
                redirect('/patient/bookingSummary/list');
            }

            $id_user = $this->session->userId;
            $id_session = $this->session->my_session_id;

            if($this->input->post())
            {

                // echo "<Pre>";print_r($this->input->post());exit();


                if($_FILES['upload_file'])
                {  


                $certificate_name = $_FILES['upload_file']['name'];
                $certificate_size = $_FILES['upload_file']['size'];
                $certificate_tmp =$_FILES['upload_file']['tmp_name'];
                
                // echo "<Pre>"; print_r($certificate_tmp);exit();

                $certificate_ext=explode('.',$certificate_name);
                $certificate_ext=end($certificate_ext);
                $certificate_ext=strtolower($certificate_ext);


                $this->fileFormatNSizeValidation($certificate_ext,$certificate_size,'Upload File');

                $upload_file = $this->uploadFile($certificate_name,$certificate_tmp,'File');

                }

                // echo "<Pre>"; print_r($upload_file);exit();

                $comments = $this->security->xss_clean($this->input->post('comments'));


                if($upload_file != '')
                {
                    $data['id_test_booking'] = $id;
                    $data['upload_file'] = $upload_file;
                    $data['comments'] = $comments;
                    $data['status'] = 1;
                    $data['created_by'] = $id_user;
                    
                // echo "<Pre>"; print_r($data);exit();

                    $result = $this->booking_summary_model->addNewTestBookingFiles($data);
                }

                // echo "<Pre>"; print_r($moa_file);exit();

                
                redirect($_SERVER['HTTP_REFERER']);;


            }

            $data['staffList'] = $this->booking_summary_model->staffListByStatus('1');
            $data['patientBooking'] = $this->booking_summary_model->getPatientBooking($id);
            $data['patientBookingTestDetails'] = $this->booking_summary_model->getPatientBookingTestDetails($id);
            $data['patientBookingSampleDetails'] = $this->booking_summary_model->getPatientBookingSampleDetails($id);
            $data['getUploadFilesByTestBooking'] = $this->booking_summary_model->getUploadFilesByTestBooking($id);

            $data['patient'] = $this->booking_summary_model->getPatient($data['patientBooking']->id_patient);

            // echo "<Pre>"; print_r($data['getUploadFilesByTestBooking']);exit;

            $this->global['pageTitle'] = 'Campus Management System : Edit Patient';
            $this->loadViews("booking_summary/edit", $this->global, $data, NULL);
        }
    }



    function getStateByCountry($id_country)
    {
            $results = $this->booking_summary_model->getStateByCountryId($id_country);

            
                 
            $table="  


            <script type='text/javascript'>
                     $('select').select2();
                 </script> 
                
         ";

            $table.="
            <select name='mailing_state' id='mailing_state' class='form-control'>
                <option value=''>Select</option>
                ";

            for($i=0;$i<count($results);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $results[$i]->id;
            $name = $results[$i]->name;
            $table.="<option value=".$id.">".$name.
                    "</option>";

            }
            $table.="

            </select>";

            echo $table;
            exit;
    }

    function addTempTestBooking()
    {
        $tempData = $this->security->xss_clean($this->input->post('data'));
        // echo "<Pre>";print_r($tempData);exit;
        $id_session = $this->session->my_session_id;
        $tempData['id_session'] = $id_session;
        $booking_type = $tempData['booking_type'];
        
        $amount = 0;

        if($booking_type == 'Package')
        {
            $tempData['id_test'] = 0;
            $id_package = $tempData['id_package'];
            $package = $this->booking_summary_model->getPackage($id_package);
            if($package)
            {
                $amount = $package->amount;
            }
        }
        elseif($booking_type == 'Individual')
        {
            $tempData['id_package'] = 0;
            $id_test = $tempData['id_test'];
            $test = $this->booking_summary_model->getTest($id_test);
            if($test)
            {
                $amount = $test->amount;
            }
        }
        
        $tempData['amount'] = $amount;

        $inserted_id = $this->booking_summary_model->addTempTestBooking($tempData);

        $data = array();

        if($inserted_id)
        {
            $data = $this->displayTempTestBookingData();
        
        }
        
        echo $data;
        exit();
    }



    function displayTempTestBookingData()
    {
        $id_session = $this->session->my_session_id;
        
        $details = $this->booking_summary_model->getTempTestBookingDataBySessionId($id_session); 
        // echo "<Pre>";print_r($details);exit;
        if(!empty($details))
        {
            
        $table = "
        <div class='custom-table'>
        <table  class='table' id='list-table'>
                <thead>
                  <tr>
                    <th>Sl. No</th>
                    <th>Booking Type</th>
                    <th>Package</th>
                    <th>Test</th>
                    <th>Amount</th>
                    <th>Action</th>
                    </tr>
                </thead>";
                    $total_detail = 0;
                    for($i=0;$i<count($details);$i++)
                    {
                        $id = $details[$i]->id;
                        $booking_type = $details[$i]->booking_type;
                        $test_name = $details[$i]->test_name;
                        $test_code = $details[$i]->test_code;
                        $package_code = $details[$i]->package_code;
                        $package_name = $details[$i]->package_name;
                        $amount = $details[$i]->amount;

                        $j=$i+1;

                        $table .= "
                        <tbody>
                        <tr>
                            <td>$j</td>
                            <td>$booking_type</td>
                            <td>$test_code - $test_name</td>
                            <td>$package_code - $package_name</td>
                            <td>$amount</td>
                            <td>
                                <a class='btn btn-sm btn-edit' onclick='deleteTempTestBookingDetails($id)'>Delete</a>
                            <td>
                        </tr>";
                                // <span onclick='deleteTempData($id)'>Delete</a>
                        $total_detail = $total_detail + $amount;
                    }

                     $table .= "

                    <tr>
                            <td bgcolor='' colspan='3'></td>
                            <td bgcolor=''><b> Total : </b></td>
                            <td bgcolor=''>
                            <input type='hidden' name='total_detail' id='total_detail' value='$total_detail' />
                            <b>$total_detail</b></td>
                            <td bgcolor=''></td>
                        </tr>
                    </tbody>";
        $table.= "</table>
        </div>";
        }
        else
        {
            $total_detail='';

            $table = "
            <input type='hidden' name='total_detail' id='total_detail' value='$total_detail' />
            ";
        }
        return $table;
    }

    function deleteTempTestBookingDetails($id)
    {
        $deleted = $this->booking_summary_model->deleteTempTestBookingDetails($id);

        $data = array();

        if($deleted)
        {
            $data = $this->displayTempTestBookingData();
        }
        echo $data;
        exit();
    }




    function addTempSampleBooking()
    {
        $tempData = $this->security->xss_clean($this->input->post('data'));
        $id_session = $this->session->my_session_id;
        $tempData['id_session'] = $id_session;
        $inserted_id = $this->booking_summary_model->addTempSampleBooking($tempData);

        $data = array();

        if($inserted_id)
        {
            $data = $this->displayTempSampleBookingData($id_session);
        }
        
        echo $data;
        exit();
    }

    function displayTempSampleBookingData()
    {
        $id_session = $this->session->my_session_id;
        
        $details = $this->booking_summary_model->getTempSampleBookingDataBySessionId($id_session); 
        // echo "<Pre>";print_r($details);exit;
        if(!empty($details))
        {
            
        $table = "
        <div class='custom-table'>
        <table  class='table' id='list-table'>
                <thead>
                  <tr>
                    <th>Sl. No</th>
                    <th>Sample</th>
                    <th>Action</th>
                    </tr>
                </thead>";

                    for($i=0;$i<count($details);$i++)
                    {
                        $id = $details[$i]->id;
                        $sample_code = $details[$i]->sample_code;
                        $sample_name = $details[$i]->sample_name;

                        $j=$i+1;

                        $table .= "
                        <tbody>
                        <tr>
                            <td>$j</td>
                            <td>$sample_code - $sample_name</td>
                            <td>
                                <a class='btn btn-sm btn-edit' onclick='deleteTempSampleBookingDetails($id)'>Delete</a>
                            <td>
                        </tr>";
                    }

                     $table .= "
                    </tbody>";
        $table.= "</table>
        </div>";
        }
        else
        {
            $total_detail='';

            $table = "
            <input type='hidden' name='total_detail' id='total_detail' value='$total_detail' />
            ";
        }
        return $table;
    }

    function deleteTempSampleBookingDetails($id)
    {
        $deleted = $this->booking_summary_model->deleteTempSampleBookingDetails($id);

        $data = array();

        if($deleted)
        {
            $data = $this->displayTempSampleBookingData();
        }
        echo $data;
        exit();
    }

    function showConfirmation()
    {
        $tempData = $this->security->xss_clean($this->input->post('data'));
        $id_staff = $tempData['id_staff'];
        $id_session = $this->session->my_session_id;

        $details_test = $this->booking_summary_model->getTempTestBookingDataBySessionId($id_session); 
        $staff = $this->booking_summary_model->getStaff($id_staff);
        // echo "<Pre>";print_r($details_test);exit;
        $table = "";
        if(!empty($details_test))
        {
        
        $staff_name = $staff->name;
        $staff_id = $staff->staff_id;

        $staff = $staff_id . " " . $staff_name;
        $table = "
            <input type='hidden' name='staff' id='staff' value='$staff' />
            ";

        $table .= "
        <div class='custom-table'>
        <table  class='table' id='list-table'>
                <thead>
                  <tr>
                    <th>Sl. No</th>
                    <th>Booking Type</th>
                    <th>Package</th>
                    <th>Test</th>
                    <th>Amount</th>
                    </tr>
                </thead>";
                    $total_detail = 0;
                    for($i=0;$i<count($details_test);$i++)
                    {
                        $id = $details_test[$i]->id;
                        $booking_type = $details_test[$i]->booking_type;
                        $test_name = $details_test[$i]->test_name;
                        $test_code = $details_test[$i]->test_code;
                        $package_code = $details_test[$i]->package_code;
                        $package_name = $details_test[$i]->package_name;
                        $amount = $details_test[$i]->amount;

                        $j=$i+1;

                        $table .= "
                        <tbody>
                        <tr>
                            <td>$j</td>
                            <td>$booking_type</td>
                            <td>$test_code - $test_name</td>
                            <td>$package_code - $package_name</td>
                            <td>$amount</td>
                        </tr>";
                                // <span onclick='deleteTempData($id)'>Delete</a>
                        $total_detail = $total_detail + $amount;
                    }

                     $table .= "

                    <tr>
                            <td bgcolor='' colspan='3'></td>
                            <td bgcolor=''><b> Total : </b></td>
                            <td bgcolor=''>
                            <b>$total_detail</b></td>
                            <td bgcolor=''></td>
                        </tr>
                    </tbody>";
        $table.= "</table>
        </div>";
        }

        
        $details = $this->booking_summary_model->getTempSampleBookingDataBySessionId($id_session); 
        // echo "<Pre>";print_r($details);exit;
        if(!empty($details))
        {
            
        $table .= "
        <br>

        <div class='custom-table'>
        <table  class='table' id='list-table'>
                <thead>
                  <tr>
                    <th>Sl. No</th>
                    <th>Sample</th>
                    </tr>
                </thead>";

                    for($i=0;$i<count($details);$i++)
                    {
                        $id = $details[$i]->id;
                        $sample_code = $details[$i]->sample_code;
                        $sample_name = $details[$i]->sample_name;

                        $j=$i+1;

                        $table .= "
                        <tbody>
                        <tr>
                            <td>$j</td>
                            <td>$sample_code - $sample_name</td>
                        </tr>";
                    }

                     $table .= "
                    </tbody>";
        $table.= "</table>
        </div>";
        }

        echo $table;exit;

    }
}