<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Credit_note_model extends CI_Model
{
    function creditNoteList()
    {
        $this->db->select('cn.*');
        $this->db->from('sponser_credit_note as cn');
        // $this->db->join('receipt as r', 'rpd.id_receipt = r.id');
        // $this->db->join('payment_type as pt', 'rpd.id_payment_type = pt.id');
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getCreditNote($id)
    {
        $this->db->select('cn.*');
        $this->db->from('sponser_credit_note as cn');
        // $this->db->join('receipt as r', 'rpd.id_receipt = r.id');
        // $this->db->join('payment_type as pt', 'rpd.id_payment_type = pt.id');
        $this->db->where('cn.id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewCreditNote($data)
    {
        $this->db->trans_start();
        $this->db->insert('sponser_credit_note', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function addNewCreditNoteDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('sponser_credit_note_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function editCreditNote($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('sponser_credit_note', $data);
        return TRUE;
    }
    function addNewTempCreditNoteDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('temp_sponser_credit_note_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
// echo "<Pre>";  print_r($db);exit;
        return $insert_id;
    }

    function getTempCreditNoteDetails($id_session)
    {
        $this->db->select('tcnd.*, mi.invoice_number as sponser_main_invoice');
        $this->db->from('temp_sponser_credit_note_details as tcnd');
        $this->db->join('sponser_main_invoice as mi', 'tcnd.id_sponser_main_invoice = mi.id');        
        $this->db->where('tcnd.id_session', $id_session);
        $query = $this->db->get();
        return $query->result();
    }

    function deleteTempDataBySession($id_session)
    {
        $this->db->where('id_session', $id_session);
       $this->db->delete('temp_sponser_credit_note_details');
    }

     function deleteTempData($id)
    { 
       $this->db->where('id', $id);
       $this->db->delete('temp_sponser_credit_note_details');
    }

    function addTempDetails($data)
    {
        // echo "<Pre>";  print_r($data);exit;

        $this->db->trans_start();
        $this->db->insert('temp_sponser_credit_note_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function updateTempDetails($data,$id) {
        $this->db->where('id', $id);
        $this->db->update('temp_sponser_credit_note_details', $data);
        return TRUE;
    }
}

