<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Patient_booking_model extends CI_Model
{
    function patientList()
    {
        $this->db->select('sp.*, s.name as state, c.name as country');
        $this->db->from('patient as sp');
        $this->db->join('state as s', 'sp.id_state = s.id');
        $this->db->join('country as c', 'sp.id_country = c.id');
        $this->db->order_by("sp.id", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         //echo "<Pre>"; print_r($result);exit;
         return $result;
    }

    function patientListSearch($data)
    {
        $this->db->select('sp.*');
        $this->db->from('patient as sp');
        // $this->db->join('state as s', 'sp.id_state = s.id');
        // $this->db->join('country as c', 'sp.id_country = c.id');
        if ($data['name'] != '')
        {
            $likeCriteria = "(sp.full_name  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['email'] != '')
        {
            $likeCriteria = "(sp.email  LIKE '%" . $data['email'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['phone'] != '')
        {
            $likeCriteria = "(sp.phone  LIKE '%" . $data['phone'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['register_no'] != '')
        {
            $likeCriteria = "(sp.register_no  LIKE '%" . $data['register_no'] . "%')";
            $this->db->where($likeCriteria);
        }
        // $this->db->limit(1,5);
        $this->db->order_by("sp.full_name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         //echo "<Pre>"; print_r($result);exit;
         return $result;
    }

    function getPatient($id)
    {
        $this->db->select('*');
        $this->db->from('patient');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getStaff($id)
    {
        $this->db->select('*');
        $this->db->from('staff');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewPatient($data)
    {
        $this->db->trans_start();
        $this->db->insert('patient', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editPatient($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('patient', $data);
        return TRUE;
    }

    function getSalutation($id)
    {
        $this->db->select('*');
        $this->db->from('salutation_setup');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function stateList()
    {
        $this->db->select('*');
        $this->db->from('state');
        $this->db->where('status', '1');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function countryListByStatus($status)
    {
        $this->db->select('c.id, c.name');
        $this->db->from('country as c');
        $this->db->where('c.status', $status);
        $this->db->order_by("c.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function staffListByStatus($status)
    {
        $this->db->select('c.*');
        $this->db->from('staff as c');
        $this->db->where('c.status', $status);
        $this->db->order_by("c.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function packageListByStatus($status)
    {
        $this->db->select('c.*');
        $this->db->from('package as c');
        $this->db->where('c.status', $status);
        $this->db->order_by("c.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }


    function testListByStatus($status)
    {
        $this->db->select('c.*');
        $this->db->from('test as c');
        $this->db->where('c.status', $status);
        $this->db->order_by("c.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function samplesListByStatus($status)
    {
        $this->db->select('c.*');
        $this->db->from('sample as c');
        $this->db->where('c.status', $status);
        $this->db->order_by("c.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function salutationListByStatus($status)
    {
        $this->db->select('c.id, c.name');
        $this->db->from('salutation_setup as c');
        $this->db->where('c.status', $status);
        $this->db->order_by("c.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getStateByCountryId($id_country)
    {
        $this->db->select('*');
        $this->db->from('state');
        $this->db->where('id_country', $id_country);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function generatePatientCode()
    {
        $year = date('y');
        $Year = date('Y');

            $this->db->select('*');
            $this->db->from('patient');
            $this->db->order_by("id", "desc");
            $query = $this->db->get();
            $result = $query->num_rows();

     
            $count= $result + 1;

           $generated_number = "SG" .(sprintf("%'06d", $count)) . $Year;
           // echo "<Pre>";print_r($generated_number);exit();
           return $generated_number;
    }

    function feeListByStatus($status)
    {
        $this->db->select('c.*');
        $this->db->from('fee_setup as c');
        $this->db->where('c.status', $status);
        $this->db->order_by("c.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function calculationModeListByStatus($status)
    {
        $this->db->select('c.*');
        $this->db->from('amount_calculation_type as c');
        $this->db->where('c.status', $status);
        $this->db->order_by("c.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function frequencyModeListByStatus($status)
    {
        $this->db->select('c.*');
        $this->db->from('frequency_mode as c');
        $this->db->where('c.status', $status);
        $this->db->order_by("c.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getPatientFeeInfoDetails($id)
    {
        $this->db->select('sp.*, fs.name as fee_name, fs.code as fee_code, fm.name as frequency_name, fm.code as frequency_code');
        $this->db->from('patient_fee_info_details as sp');
        $this->db->join('fee_setup as fs', 'sp.id_fee_item = fs.id');
        $this->db->join('frequency_mode as fm', 'sp.id_frequency_mode = fm.id');
        $this->db->where('sp.id_patient', $id);
         $query = $this->db->get();
         $result = $query->result();  
         //echo "<Pre>"; print_r($result);exit;
         return $result;
    }

    function getPatientCoordinatorDetails($id)
    {
        $this->db->select('sp.*');
        $this->db->from('patient_coordinator_details as sp');
        $this->db->where('sp.id_patient', $id);
         $query = $this->db->get();
         $result = $query->result();  
         //echo "<Pre>"; print_r($result);exit;
         return $result;
    }

    function saveFeeDetailData($data)
    {
        $this->db->trans_start();
        $this->db->insert('patient_fee_info_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function saveCoordinatorDetailData($data)
    {
        $this->db->trans_start();
        $this->db->insert('patient_coordinator_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function deleteFeeDetailData($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('patient_fee_info_details');
        return TRUE;
    }

    function deleteCoordinatorDetailData($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('patient_coordinator_details');
        return TRUE;
    }

    function addTempTestBooking($data)
    {
        $this->db->trans_start();
        $this->db->insert('temp_test_booking_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getTempTestBookingDataBySessionId($id_session)
    {
        $this->db->select('tbd.*, t.name as test_name, t.code as test_code, p.name as package_name, p.code as package_code');
        $this->db->from('temp_test_booking_details as tbd');
        $this->db->join('test as t', 'tbd.id_test = t.id','left');
        $this->db->join('package as p', 'tbd.id_package = p.id','left');
        $this->db->where('tbd.id_session', $id_session);
         $query = $this->db->get();
         $result = $query->result();  
         //echo "<Pre>"; print_r($result);exit;
         return $result;
    }

    function getPackage($id)
    {
        $this->db->select('sp.*');
        $this->db->from('package as sp');
        $this->db->where('sp.id', $id);
         $query = $this->db->get();
         $result = $query->row();
         return $result;
    }

    function getTest($id)
    {
        $this->db->select('sp.*');
        $this->db->from('test as sp');
        $this->db->where('sp.id', $id);
         $query = $this->db->get();
         $result = $query->row();
         return $result;
    }

    function deleteTempTestBookingDetails($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('temp_test_booking_details');
        return TRUE;
    }


    function addTempSampleBooking($data)
    {
        $this->db->trans_start();
        $this->db->insert('temp_sample_booking_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getTempSampleBookingDataBySessionId($id_session)
    {
        $this->db->select('tbd.*, s.name as sample_name, s.code as sample_code');
        $this->db->from('temp_sample_booking_details as tbd');
        $this->db->join('sample as s', 'tbd.id_sample = s.id');
        $this->db->where('tbd.id_session', $id_session);
         $query = $this->db->get();
         $result = $query->result();  
         //echo "<Pre>"; print_r($result);exit;
         return $result;
    }

    function deleteTempSampleBookingDetails($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('temp_sample_booking_details');
        return TRUE;
    }

    function addNewPatientBooking($data)
    {
        $this->db->trans_start();
        $this->db->insert('test_booking', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function moveTempTestsToDetail($id_test_booking)
    {
        $id_session = $this->session->my_session_id;

        $temp_req_details = $this->getTempTestBookingDataBySessionIdToMove($id_session);

        foreach ($temp_req_details as $temp_test_detail)
        {
           unset($temp_test_detail->id);
           unset($temp_test_detail->id_session);
           $temp_test_detail->id_test_booking = $id_test_booking;

           $inserted_req = $this->addTestBookingDetails($temp_test_detail);
        }

        $deleted_temp_req = $this->deleteTempTestBookingDetailsBySession($id_session);
    }


    function getTempTestBookingDataBySessionIdToMove($id_session)
    {
        $this->db->select('tbd.*');
        $this->db->from('temp_test_booking_details as tbd');
        $this->db->join('test as t', 'tbd.id_test = t.id','left');
        $this->db->join('package as p', 'tbd.id_package = p.id','left');
        $this->db->where('tbd.id_session', $id_session);
         $query = $this->db->get();
         $result = $query->result();  
         //echo "<Pre>"; print_r($result);exit;
         return $result;
    }

    function addTestBookingDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('test_booking_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function deleteTempTestBookingDetailsBySession($id_session)
    {
        $this->db->where('id_session', $id_session);
        $this->db->delete('temp_test_booking_details');
        return TRUE;
    }

    function moveTempSamplesToDetail($id_test_booking)
    {
        $id_session = $this->session->my_session_id;

        $temp_req_details = $this->getTempSampleBookingDataBySessionIdToMove($id_session);

        foreach ($temp_req_details as $temp_test_detail)
        {
           unset($temp_test_detail->id);
           unset($temp_test_detail->id_session);
           $temp_test_detail->id_test_booking = $id_test_booking;

           $inserted_req = $this->addSampleBooking($temp_test_detail);
        }

        $deleted_temp_req = $this->deleteTempSampleBookingDetailsBySessionId($id_session);
    }

    function getTempSampleBookingDataBySessionIdToMove($id_session)
    {
        $this->db->select('tbd.*');
        $this->db->from('temp_sample_booking_details as tbd');
        $this->db->join('sample as s', 'tbd.id_sample = s.id');
        $this->db->where('tbd.id_session', $id_session);
         $query = $this->db->get();
         $result = $query->result();  
         //echo "<Pre>"; print_r($result);exit;
         return $result;
    }

    function addSampleBooking($data)
    {
        $this->db->trans_start();
        $this->db->insert('sample_booking_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function deleteTempSampleBookingDetailsBySessionId($id_session)
    {
        $this->db->where('id_session', $id_session);
        $this->db->delete('temp_sample_booking_details');
        return TRUE;
    }
}