<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Sponser_has_students_model extends CI_Model
{
    function sponserHasStudentsList()
    {
        $this->db->select('shs.*, s.name as sponser, stu.full_name as student');
        $this->db->from('sponser_has_students as shs');
        $this->db->join('sponser as s', 'shs.id_sponser = s.id');
        $this->db->join('student as stu', 'shs.id_student = stu.id');
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getSponserHasStudents($id_sponser)
    {

        $this->db->select('DISTINCT(stu.id) as id_student, sp.*, stu.full_name as student, stu.nric, fs.name as fee_name, fs.code as fee_code');
        $this->db->from('sponser_has_students as sp');
        $this->db->join('student as stu', 'sp.id_student = stu.id');
        $this->db->join('fee_setup as fs', 'sp.id_fee_item = fs.id');
        $this->db->where('sp.id_sponser', $id_sponser);
         $query = $this->db->get();
         $result = $query->result();  
         //echo "<Pre>"; print_r($result);exit;
         return $result;
    }
    
    function addNewSponserHasStudents($data)
    {
        $this->db->trans_start();
        $this->db->insert('sponser_has_students', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editSponserHasStudents($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('sponser_has_students', $data);
        return TRUE;
    }

    function studentList()
    {
        $this->db->select('*');
        $this->db->from('student');
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function programmeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result(); 

        return$result;
    }

    function intakeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('intake');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result(); 

        return$result;
    }

    function getStudentByProgrammeId($id_programme)
    {
        $this->db->select('*');
        $this->db->from('student');
        $this->db->where('id_program', $id_programme);
        $this->db->order_by("full_name", "ASC");
        $query = $this->db->get();
        $result = $query->result(); 

        return$result;
    }

    function getProgrammeById($id_programme)
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->where('id', $id_programme);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function getStudentByStudentId($id_student)
    {
        $this->db->select('s.*, p.name as programme_name, i.name as intake_name');
        $this->db->from('student as s');
        $this->db->join('programme as p', 's.id_program = p.id'); 
        $this->db->join('intake as i', 's.id_intake = i.id'); 
        $this->db->where('s.id', $id_student);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function getStudentsSearch($data)
    {
        $id_programme = $data['id_programme'];
        $id_intake = $data['id_intake'];
        $student = $data['student'];

        $this->db->select('s.*, p.name as programme_name, i.name as intake_name');
        $this->db->from('student as s');
        $this->db->join('programme as p', 's.id_program = p.id'); 
        $this->db->join('intake as i', 's.id_intake = i.id');
        if ($id_programme != '')
        {
            $this->db->where('s.id_program', $id_programme);
            
        }
        if ($id_intake != '')
        {
            $this->db->where('s.id_intake', $id_intake);
            
        }
        if ($student != '')
        {
            $likeCriteria = "(s.full_name  LIKE '%" . $student . "%' or s.nric  LIKE '%" . $student . "%')";
            $this->db->where($likeCriteria);
            
        }
       
        $query = $this->db->get();
        $result = $query->result(); 

        return$result;

    }

    function deleteTempData($id)
    { 
        // echo "<Pre>";  print_r($id);exit;
       $this->db->where('id', $id);
       $this->db->delete('sponser_has_students');
    }

    function feeListByStatus($status)
    {
        $this->db->select('c.*');
        $this->db->from('fee_setup as c');
        $this->db->where('c.status', $status);
        $this->db->order_by("c.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function deleteStudentFromSponser($id)
    {
         $this->db->where('id', $id);
       $this->db->delete('sponser_has_students');
       return TRUE;
    }


}