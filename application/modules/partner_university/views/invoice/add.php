<?php $this->load->helper("form"); ?>

<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        
        <div class="page-title clearfix">
            <h3>Partner University Applicants Billing Generation</h3>
        </div>


        <?php
            if($partnerUniversity)
            {
            ?>


            <div class="form-container">
                <h4 class="form-group-title">Partner University Details</h4>
                <div class='data-list'>
                    <div class='row'> 
                        <div class='col-sm-6'>
                            <dl>
                                <dt>University Name :</dt>
                                <dd><?php echo ucwords($partnerUniversity->name);?></dd>
                            </dl>
                            <dl>
                                <dt>Short Name :</dt>
                                <dd><?php echo $partnerUniversity->short_name ?></dd>
                            </dl>                                                     
                        </div>        
                        
                        <div class='col-sm-6'>                           
                            <dl>
                                <dt>University Code :</dt>
                                <dd><?php echo $partnerUniversity->code ?></dd>
                            </dl>
                            <dl>
                                <dt>Name (Optional Language) :</dt>
                                <dd><?php echo $partnerUniversity->name_in_malay; ?></dd>
                            </dl>

                        </div>
                    </div>
                </div>
            </div>

            <br>



            <?php
            }
            ?>



    <form id="form_pr_entry" action="" method="post">


        <div class="form-container">
            <h4 class="form-group-title">Search Applicants For Billing</h4>
            <!-- <h4 >Search Student For Academic Advisor Tagging</h4> -->


            <div class="row">

                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Applicant Name</label>
                        <input type="text" class="form-control" id="full_name" name="full_name">
                    </div>
                </div>
                

                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Applicant Email</label>
                        <input type="text" class="form-control" id="email_id" name="email_id">
                    </div>
                </div>   


                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Applicantion Submission From Date</label>
                        <input type="text" class="form-control datepicker" id="from_date" name="from_date">
                    </div>
                </div>   


                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Applicantion Submission To Date</label>
                        <input type="text" class="form-control datepicker" id="to_date" name="to_date">
                    </div>
                </div>   


            </div>

            <div class="row">    

                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Program </label>
                        <select name="id_program" id="id_program" class="form-control" >
                            <option value="">-- All --</option>
                            <?php
                            if (!empty($programList))
                            {
                                foreach ($programList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>  

                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Intake </label>
                        <select name="id_intake" id="id_intake" class="form-control" >
                            <option value="">-- All --</option>
                            <?php
                            if (!empty($intakeList))
                            {
                                foreach ($intakeList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->year . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>  



            </div>

        </div>

        

        <div class="modal-footer">
            <button type="button" class="btn btn-default" onclick="searchStudents()">Search</button>
        </div>


        


        <div class="form-container" style="display: none;" id="view_student_display">
            <h4 class="form-group-title">Applicant For Partner University Billing</h4>

            <div  id='view_student'>
            </div>

        </div>

        <?php
        if($partnerUniversity->billing_to == 'Student')
        {
            ?>
                <h4>Billing Is Defined For Student, Partner University Billing Not Granted</h4>

            <?php
        }
        else
        {
            ?>


        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <!-- <a href="list" class="btn btn-link">Cancel</a> -->
            </div>
        </div>


        <?php
        }
        ?>

    </form>



    <div id="myModal" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <!-- <h4 class="modal-title">Program Landscape</h4> -->
          </div>

          <div class="modal-body">

            <br>



              <div class="form-container">
                <h4 class="form-group-title"> Fee Structure Details</h4>

                
                <div class="row">
                    <div id='view_model'>
                    </div>
                </div>


              </div>
          

          <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>

        </div>
        </div>

      </div>
    </div>



        
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>



<script>

    $('select').select2();


    $(function(){
    $(".datepicker").datepicker({
        changeYear: true,
        changeMonth: true,
    });
    });



    function searchStudents()
    {
        var tempPR = {};
        tempPR['id_program'] = $("#id_program").val();
        tempPR['id_intake'] = $("#id_intake").val();
        tempPR['full_name'] = $("#full_name").val();
        tempPR['email_id'] = $("#email_id").val();
        tempPR['from_date'] = $("#from_date").val();
        tempPR['to_date'] = $("#to_date").val();
            $.ajax(
            {
               url: '/partner_university/invoice/searchStudents',
               type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view_student_display").show();
                $("#view_student").html(result);
                // alert(ta);
               }
            });
    }



    function viewFeeStructureByApplicant(id_applicant)
    {
        $.ajax(
            {
               url: '/partner_university/invoice/viewFeeStructureByApplicant/'+id_applicant,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view_model").html(result);
                $('#myModal').modal('show');
               }
            });

        // var tempPR = {};
        // tempPR['id_applicant'] = id_applicant;

        //     $.ajax(
        //     {
        //        url: '/partner_university/invoice/viewFeeStructureByApplicant',
        //         type: 'POST',
        //        data:
        //        {
        //         tempData: tempPR
        //        },
        //        error: function()
        //        {
        //         alert('Something is wrong');
        //        },
        //        success: function(result)
        //        {
        //         // alert(result);
        //         $("#view_model").html(result);
        //         $('#myModal').modal('show');
        //         // window.location.reload();
        //        }
        //     });
    }




    $(document).ready(function() {
        $("#form_pr_entry").validate({
            rules: {
                id_advisor_for_tagging: {
                    required: true
                },
                 id_student: {
                    required: true
                }
            },
            messages: {
                id_advisor_for_tagging: {
                    required: "<p class='error-text'>Select Supervisor</p>",
                },
                id_student: {
                    required: "<p class='error-text'>Select Students</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

</script>