<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Max. Internship Limit Per Student</h3>
        </div>
        <form id="form_award" action="" method="post">

         <div class="form-container">
            <h4 class="form-group-title">Edit Internship Limit Details</h4>


            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Max No. Of Internship <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="max_limit" name="max_limit" value="<?php echo $internshipLimit->max_limit; ?>">
                        <input type="hidden" class="form-control" id="id" name="id" value="<?php echo $internshipLimit->id; ?>">
                    </div>
                </div>
                
            </div>

        </div>        

        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <!-- <a href="../list" class="btn btn-link">Cancel</a> -->
            </div>
        </div>


        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>
    $(document).ready(function() {
        $("#form_award").validate({
            rules: {
                max_limit: {
                    required: true
                }
            },
            messages: {
                max_limit: {
                    required: "<p class='error-text'>Max. Limit Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

    $('select').select2();


</script>
<script>
  $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true
    });
  } );
</script>