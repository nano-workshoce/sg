<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Company_registration_model extends CI_Model
{
    function countryListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('country');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result();      
        return $result;
    }

    function programListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result();      
        return $result;
    }

    function stateListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('state');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result();      
        return $result;
    }

    function getStateByCountryId($id_country)
    {
        $this->db->select('*');
        $this->db->from('state');
        $this->db->where('id_country', $id_country);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function companyTypeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('internship_company_type');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result();      
        return $result;
    }

    function companyTypeList()
    {
        $this->db->select('*');
        $this->db->from('internship_company_type');
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result();      
        return $result;
    }

    function companyRegistrationListSearch($data)
    {
        $this->db->select('invt.*, ict.name as company_type');
        $this->db->from('internship_company_registration as invt');
        $this->db->join('internship_company_type as ict', 'invt.id_company_type = ict.id');
        if ($data['name'] != '')
        {
            $likeCriteria = "(invt.name LIKE '%" . $data['name'] . "%' or invt.registration_no  LIKE '%" . $data['name'] . "%' or invt.code  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }if ($data['id_company_type'] != '')
        {
            $this->db->where('invt.id_company_type', $data['id_company_type']);
        }
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }


    function getCompanyRegistration($id)
    {
        $this->db->select('*');
        $this->db->from('internship_company_registration');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewCompanyRegistration($data)
    {
        $this->db->trans_start();
        $this->db->insert('internship_company_registration', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editCompanyRegistration($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('internship_company_registration', $data);
        return TRUE;
    }

    function addTempDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('temp_internship_company_has_program', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function addCompanyProgramDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('internship_company_has_program', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getTempCompanyHasProgram()
    {
        $id_session = $this->session->my_session_id;

        $this->db->select('tihp.*, p.name as program_name, p.code as program_code');
        $this->db->from('temp_internship_company_has_program as tihp');
        $this->db->join('programme as p', 'tihp.id_program = p.id');
        $this->db->where('id_session', $id_session);
        $query = $this->db->get();
        return $query->result();
    }

    function deleteTempData($id)
    {
        $this->db->where('id', $id);
       $this->db->delete('temp_internship_company_has_program');
       return TRUE;
    }

    function deleteTempDataBySession($id_session)
    {
        // echo "<Pre>";print_r($id_session);exit();
        $this->db->where('id_session', $id_session);
       $this->db->delete('temp_internship_company_has_program');
       return TRUE;
    }

    function getTempProgram()
    {
        $id_session = $this->session->my_session_id;

        $this->db->select('tihp.*');
        $this->db->from('temp_internship_company_has_program as tihp');
        $this->db->where('id_session', $id_session);
        $query = $this->db->get();
        return $query->result();
    }

    function moveTempToDetails($id_company)
    {
        $id_session = $this->session->my_session_id;

        $temp_details = $this->getTempProgram();
        foreach ($temp_details as $detail)
        {
            unset($detail->id_session);
            unset($detail->id);
            $detail->id_company = $id_company;

            $inserted = $this->addCompanyProgramDetails($detail);
        }

        $deleted = $this->deleteTempDataBySession($id_session);
    }

    function getCompanyHasProgramme($id_company)
    {
        $this->db->select('tihp.*, p.name as program_name, p.code as program_code');
        $this->db->from('internship_company_has_program as tihp');
        $this->db->join('programme as p', 'tihp.id_program = p.id');
        $this->db->where('id_company', $id_company);
        $query = $this->db->get();
        return $query->result();
    }

    function deleteCompanyHasProgramme($id)
    {
        $this->db->where('id', $id);
       $this->db->delete('internship_company_has_program');
       return TRUE;
    }
}