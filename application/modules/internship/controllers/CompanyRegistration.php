<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class CompanyRegistration extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('company_registration_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('internship_company_registration.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_company_type'] = $this->security->xss_clean($this->input->post('id_company_type'));
            $formData['status'] = '';
 
            $data['searchParam'] = $formData;

            $data['companyRegistrationList'] = $this->company_registration_model->companyRegistrationListSearch($formData);
            $data['companyTypeList'] = $this->company_registration_model->companyTypeList();
            
            // echo "<Pre>";print_r($data);exit();


            $this->global['pageTitle'] = 'Campus Management System : List Company Type';
            $this->loadViews("company_registration/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('internship_company_registration.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;

            if($this->input->post())
            {

                $name = $this->security->xss_clean($this->input->post('name'));
                $registration_no = $this->security->xss_clean($this->input->post('registration_no'));
                $person_in_charge = $this->security->xss_clean($this->input->post('person_in_charge'));
                $email = $this->security->xss_clean($this->input->post('email'));
                $address = $this->security->xss_clean($this->input->post('address'));
                $contact_number = $this->security->xss_clean($this->input->post('contact_number'));
                $id_company_type = $this->security->xss_clean($this->input->post('id_company_type'));
                $id_country = $this->security->xss_clean($this->input->post('id_country'));
                $id_state = $this->security->xss_clean($this->input->post('id_state'));
                $city = $this->security->xss_clean($this->input->post('city'));
                $zipcode = $this->security->xss_clean($this->input->post('zipcode'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'registration_no' => $registration_no,
                    'person_in_charge' => $person_in_charge,
                    'email' => $email,
                    'address' => $address,
                    'contact_number' => $contact_number,
                    'id_company_type' => $id_company_type,
                    'id_country' => $id_country,
                    'id_state' => $id_state,
                    'city' => $city,
                    'zipcode' => $zipcode,
                    'status' => $status,
                    'created_by' => $user_id
                );
                //echo "<Pre>"; print_r($subjectDetails);exit;

                $inserted_id = $this->company_registration_model->addNewCompanyRegistration($data);
                if($inserted_id)
                {
                    $result = $this->company_registration_model->moveTempToDetails($inserted_id);
                }
                redirect('/internship/companyRegistration/list');
            }
            else
            {
                $result = $this->company_registration_model->deleteTempDataBySession($id_session);
            }

            $data['countryList'] = $this->company_registration_model->countryListByStatus('1');
            $data['companyTypeList'] = $this->company_registration_model->companyTypeListByStatus('1');
            $data['programList'] = $this->company_registration_model->programListByStatus('1');

            $this->global['pageTitle'] = 'Campus Management System : Add Company Type';
            $this->loadViews("company_registration/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('internship_company_registration.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;

            if ($id == null)
            {
                redirect('/internship/companyRegistration/list');
            }

            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $registration_no = $this->security->xss_clean($this->input->post('registration_no'));
                $person_in_charge = $this->security->xss_clean($this->input->post('person_in_charge'));
                $email = $this->security->xss_clean($this->input->post('email'));
                $address = $this->security->xss_clean($this->input->post('address'));
                $contact_number = $this->security->xss_clean($this->input->post('contact_number'));
                $id_company_type = $this->security->xss_clean($this->input->post('id_company_type'));
                $id_country = $this->security->xss_clean($this->input->post('id_country'));
                $id_state = $this->security->xss_clean($this->input->post('id_state'));
                $city = $this->security->xss_clean($this->input->post('city'));
                $zipcode = $this->security->xss_clean($this->input->post('zipcode'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'registration_no' => $registration_no,
                    'email' => $email,
                    'person_in_charge' => $person_in_charge,
                    'address' => $address,
                    'contact_number' => $contact_number,
                    'id_company_type' => $id_company_type,
                    'id_country' => $id_country,
                    'id_state' => $id_state,
                    'city' => $city,
                    'zipcode' => $zipcode,
                    'status' => $status,
                    'updated_by' => $user_id,
                    'updated_dt_tm' => date('Y-m-d h:i:s')
                );

                // echo "<Pre>"; print_r($data);exit;


                $inserted_id = $this->company_registration_model->editCompanyRegistration($data,$id);
                redirect('/internship/companyRegistration/list');
            }
            // else
            // {
            //     $result = $this->company_registration_model->deleteTempDataBySession($id_session);
            // }
            $data['idCompany'] = $id;
            $data['countryList'] = $this->company_registration_model->countryListByStatus('1');
            $data['stateList'] = $this->company_registration_model->stateListByStatus('1');
            $data['companyRegistration'] = $this->company_registration_model->getCompanyRegistration($id);
            $data['companyTypeList'] = $this->company_registration_model->companyTypeList();
            $data['programList'] = $this->company_registration_model->programListByStatus('1');

            $this->global['pageTitle'] = 'Campus Management System : Edit Company Type';
            $this->loadViews("company_registration/edit", $this->global, $data, NULL);
        }
    }

    function getStateByCountry($id_country)
    {
            $results = $this->company_registration_model->getStateByCountryId($id_country);

            // echo "<Pre>"; print_r($programme_data);exit;
            $table="   
                <script type='text/javascript'>
                     $('select').select2();
                 </script>
         ";

            $table.="
            <select name='id_state' id='id_state' class='form-control'>
                <option value=''>Select</option>
                ";

            for($i=0;$i<count($results);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $results[$i]->id;
            $name = $results[$i]->name;
            $table.="<option value=".$id.">".$name.
                    "</option>";

            }
            $table.="

            </select>";

            echo $table;
            exit;
    }



    function tempadd()
    {
        $id_session = $this->session->my_session_id;
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        $tempData['id_session'] = $id_session;
        // echo "<Pre>";print_r($tempData);exit;
        
        $inserted_id = $this->company_registration_model->addTempDetails($tempData);

        $data = $this->displaytempdata();
        
        echo $data;        
    }

    function displaytempdata()
    {
        $temp_details = $this->company_registration_model->getTempCompanyHasProgram(); 
        // echo "<Pre>";print_r($temp_details);exit;

        if(!empty($temp_details))
        {

        $table = "
        <div class='custom-table'>
        <table  class='table' id='list-table'>
                <thead>
                  <tr>
                    <th>Sl. No</th>
                    <th>Program</th>
                    <th>Action</th>
                </tr>
                </thead>";
                    for($i=0;$i<count($temp_details);$i++)
                    {
                    $id = $temp_details[$i]->id;
                    $program_name = $temp_details[$i]->program_name;
                    $program_code = $temp_details[$i]->program_code;

                    $j = $i+1;
                        $table .= "
                        <tbody>
                        <tr>
                            <td>$j</td>
                            <td>$program_code - $program_name</td>                         
                            <td>
                                <a class='btn btn-sm btn-edit' onclick='deleteTempCompanyHasProgramme($id)'>Delete</a>
                            <td>
                        </tr>";
                    }


                                // <span onclick='deleteTempIntakeHasProgramme($id)'>Delete</a>
                            

            $table.= "
            </tbody>
            </table></div>";
        }
        else
        {
            $table="";
        }
        return $table;
    }

    function deleteTempCompanyHasProgramme($id)
    {
        $inserted_id = $this->company_registration_model->deleteTempData($id);
        // echo "<Pre>";print_r($inserted_id);exit;
        if($inserted_id)
        {
            $data = $this->displaytempdata();
            echo $data;exit();
        }
        
        // echo "Success"; 
    }


    function directadd()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        $data['id_program'] =  $tempData['id_program'];
        $data['id_company'] =  $tempData['id'];
        $inserted_id = $this->company_registration_model->addCompanyProgramDetails($data);
        
         $temp_details = $this->company_registration_model->getCompanyHasProgramme($tempData['id']);
         if(!empty($temp_details))
        {
            
        // echo "<Pre>";print_r($temp_details);exit;
        $table = " <div class='custom-table'>
                    <table  class='table'>
                    <thead>
                  <tr>
                    <th>Sl. No</th>
                    <th>Program</th>
                    <th>Action</th>
                </tr></thead>
                <tbody>";
                    for($i=0;$i<count($temp_details);$i++)
                    {
                    $id = $temp_details[$i]->id;
                    $program_name = $temp_details[$i]->program_name;
                    $program_code = $temp_details[$i]->program_code;
                    $j = $i+1;
                        $table .= "
                        <tr>
                            <td>$j</td>
                            <td>$program_code - $program_name</td>                         
                            <td>
                                <a class='btn btn-sm btn-edit' onclick='deleteCompanyHasProgramme($id)'>Delete</a>
                            <td>
                        </tr>";
                    }
                    // <a onclick='deleteIntakeHasProgramme($id)'>Delete</a>

        $table.= "</tbody>
        </table>
        </div>";
        }
        else
        {
            $table="";
        }
        echo $table;           
    }

    function deleteCompanyHasProgramme($id)
    {
        $inserted_id = $this->company_registration_model->deleteCompanyHasProgramme($id);
        // if($inserted_id)
        // {
        //     $data = $this->displaytempdata();
        //     echo $data;  
        // }
        
        echo "Success"; 
    }

    function tempDelete($id)
    {
        $inserted_id = $this->company_registration_model->deleteTempData($id);
        $data = $this->displaytempdata();
        echo $data; 
    } 
}
