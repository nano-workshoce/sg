<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class CompanyType extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('company_type_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('company_type.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['status'] = '';
 
            $data['searchParam'] = $formData;

            $data['companyTypeList'] = $this->company_type_model->companyTypeListSearch($formData);
            
            // echo "<Pre>";
            // print_r($formData);exit();


            $this->global['pageTitle'] = 'Campus Management System : List Company Type';
            $this->loadViews("company_type/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('company_type.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $id_session = $this->session->my_session_id;
                $user_id = $this->session->userId;

                $code = $this->security->xss_clean($this->input->post('code'));
                $name = $this->security->xss_clean($this->input->post('name'));
                $name_in_malay = $this->security->xss_clean($this->input->post('name_in_malay'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'code' => $code,
                    'name' => $name,
                    'name_in_malay' => $name_in_malay,
                    'status' => $status,
                    'created_by' => $user_id
                );
                //echo "<Pre>"; print_r($subjectDetails);exit;

                $result = $this->company_type_model->addNewCompanyType($data);
                redirect('/internship/companyType/list');
            }

            $this->global['pageTitle'] = 'Campus Management System : Add Company Type';
            $this->loadViews("company_type/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('company_type.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/internship/companyType/list');
            }
            if($this->input->post())
            {
               $id_session = $this->session->my_session_id;
                $user_id = $this->session->userId;

                $code = $this->security->xss_clean($this->input->post('code'));
                $name = $this->security->xss_clean($this->input->post('name'));
                $name_in_malay = $this->security->xss_clean($this->input->post('name_in_malay'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'code' => $code,
                    'name' => $name,
                    'name_in_malay' => $name_in_malay,
                    'status' => $status,
                    'created_by' => $user_id
                );

                $result = $this->company_type_model->editCompanyType($data,$id);
                redirect('/internship/companyType/list');
            }
            
            $data['companyType'] = $this->company_type_model->getCompanyType($id);

            $this->global['pageTitle'] = 'Campus Management System : Edit Company Type';
            $this->loadViews("company_type/edit", $this->global, $data, NULL);
        }
    }
}
