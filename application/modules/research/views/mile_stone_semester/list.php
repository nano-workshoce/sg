<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>List Mile Stone Tag Semester</h3>
      <a href="add" class="btn btn-primary">+ Add Mile Stone Tag Semester</a>
    </div>

    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Advanced Search
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">
                <div class="row">

                  <!-- <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Mile Stone</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" name="name" value="<?php echo $searchParam['name']; ?>">
                      </div>
                    </div>
                  </div> -->

                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Type</label>
                      <div class="col-sm-8">
                        <select name="type" id="type" class="form-control selitemIcon">
                          <option value="">Select</option>
                          <option value="Part Time"
                            <?php
                                if('Part Time' == $searchParam['type'])
                                {
                                    echo "selected";
                                } ?>>
                                Part Time
                          </option>

                          <option value="Full Time"
                            <?php
                                if('Full Time' == $searchParam['type'])
                                {
                                    echo "selected";
                                } ?>>
                                Full Time
                          </option>
                           
                        </select>
                      </div>
                    </div>
                  </div>

                

                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Stage</label>
                      <div class="col-sm-8">
                        <select name="id_stage" id="id_stage" class="form-control">
                          <option value="">Select</option>
                          <?php
                          if (!empty($stageList)) {
                            foreach ($stageList as $record)
                            {
                              
                          ?>
                              <option value="<?php echo $record->id;  ?>"
                                <?php
                                if ($record->id == $searchParam['id_stage'])
                                {
                                  echo 'selected';
                                } ?>
                                >
                                <?php echo  $record->name;  ?>
                                </option>
                          <?php
                            }
                          }
                          ?>
                        </select>
                      </div>
                    </div>
                  </div>

                  <!-- <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Semester</label>
                      <div class="col-sm-8">
                        <select name="id_semester" id="id_semester" class="form-control">
                          <option value="">Select</option>
                          <?php
                          if (!empty($semesterList)) {
                            foreach ($semesterList as $record)
                            {
                              
                          ?>
                              <option value="<?php echo $record->id;  ?>"
                                <?php
                                if ($record->id == $searchParam['id_semester'])
                                {
                                  echo 'selected';
                                } ?>
                                >
                                <?php echo  $record->name;  ?>
                                </option>
                          <?php
                            }
                          }
                          ?>
                        </select>
                      </div>
                    </div>
                  </div> -->

                </div>

                <div class="row">


                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Mile Stone</label>
                      <div class="col-sm-8">
                        <select name="id_mile_stone" id="id_mile_stone" class="form-control">
                          <option value="">Select</option>
                          <?php
                          if (!empty($mileStoneList)) {
                            foreach ($mileStoneList as $record)
                            {
                              
                          ?>
                              <option value="<?php echo $record->id;  ?>"
                                <?php
                                if ($record->id == $searchParam['id_mile_stone'])
                                {
                                  echo 'selected';
                                } ?>
                                >
                                <?php echo  $record->name;  ?>
                                </option>
                          <?php
                            }
                          }
                          ?>
                        </select>
                      </div>
                    </div>
                  </div>

                </div>
              </div>
              <div class="app-btn-group">
                <button type="submit" class="btn btn-primary">Search</button>
                <a href="list" class="btn btn-link">Clear All Fields</a>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>

    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl. No</th>
            <th>Type</th>
            <th>Stage</th>
            <th>Semester</th>
            <th>Mile Stone</th>
            <th>Status</th>
            <th style="text-align: center;">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($mileStoneSemesterList)) {
            $i=1;
            foreach ($mileStoneSemesterList as $record) {
          ?>
              <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $record->type ?></td>
                <td><?php echo $record->stage ?></td>
                <td><?php echo $record->semester ?></td>
                <td><?php echo $record->mile_stone ?></td>
                <td><?php if( $record->status == '1')
                {
                  echo "Active";
                }
                else
                {
                  echo "In-Active";
                } 
                ?></td>
                <td class="text-center">
                  <a href="<?php echo 'edit/' . $record->id; ?>" title="Edit">Edit</a>
                </td>
              </tr>
          <?php
          $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
<script type="text/javascript">
  
  $('select').select2();

  function clearSearchForm()
      {
        window.location.reload();
      }
</script>