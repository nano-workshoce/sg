<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit Research Category</h3>
        </div>
        <form id="form_programme" action="" method="post">
        <div class="form-container">
            <h4 class="form-group-title">Research Category Details</h4>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name" value="<?php echo $researchCategory->name; ?>">
                    </div>
                </div>

                
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Program <span class='error-text'>*</span></label>
                        <select class="form-control" id="id_program" name="id_program">
                            <option value="">Select</option>
                            <?php
                            if (!empty($programList))
                            {
                                foreach ($programList as $record)
                                {?>
                                 <option value="<?php echo $record->id;  ?>"
                                    <?php 
                                    if($record->id == $researchCategory->id_program)
                                    {
                                        echo 'selected';
                                    }
                                    ?>
                                    >
                                    <?php echo $record->code . " - " . $record->name;?>
                                 </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
                
                
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Max No Of Candidates <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="max_candidates" name="max_candidates" value="<?php echo $researchCategory->max_candidates; ?>">
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" <?php if($researchCategory->status=='1') {
                                 echo "checked=checked";
                              };?>><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0" <?php if($researchCategory->status=='0') {
                                 echo "checked=checked";
                              };?>>
                              <span class="check-radio"></span> In-Active
                            </label>                              
                        </div>                         
                </div>

            </div>

        </div>
    

        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="../list" class="btn btn-link">Back</a>
            </div>
        </div>


    </form>



        



    <form id="form_programme_intake" action="" method="post">
        <div class="form-container">
        <h4 class="form-group-title">Research Category Has Supervisor Details</h4>

            <div class="row">


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Supervisor <span class='error-text'>*</span></label>
                        <select class="form-control" id="id_staff" name="id_staff">
                            <option value="">Select</option>
                            <?php
                            if (!empty($staffList))
                            {
                                foreach ($staffList as $record)
                                {?>
                                 <option value="<?php echo $record->id;  ?>">
                                    <?php echo $record->ic_no . " - " . $record->name;?>
                                 </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
              
                <div class="col-sm-4">
                    <button type="button" class="btn btn-primary btn-lg form-row-btn" onclick="addResearchCategoryHasSupervisor()">Add</button>
                </div>
            </div>





            <?php

            if(!empty($researchCategoryHasSupervisor))
            {
                ?>

                <div class="form-container">
                        <h4 class="form-group-title">Supervisor Details</h4>

                    

                      <div class="custom-table">
                        <table class="table">
                            <thead>
                                <tr>
                                <th>Sl. No</th>
                                <th>Supervisor</th>
                                <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                 <?php
                             $total = 0;
                              for($i=0;$i<count($researchCategoryHasSupervisor);$i++)
                             { ?>
                                <tr>
                                <td><?php echo $i+1;?></td>
                                <td><?php echo $researchCategoryHasSupervisor[$i]->ic_no . " - " . $researchCategoryHasSupervisor[$i]->staff_name;?></td>
                                <td>
                                <a onclick="deleteResearchCategoryHasSupervisor(<?php echo $researchCategoryHasSupervisor[$i]->id; ?>)">Delete</a>
                                </td>

                                 </tr>
                              <?php 
                          } 
                          ?>
                            </tbody>
                        </table>
                      </div>

                    </div>

            <?php
            
            }
             ?>
            



        </div>
    </form>








           

      </div>
    </div>

   </div> <!-- END row-->

           
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>

    $('select').select2();


    function addResearchCategoryHasSupervisor()
    {

        if($('#form_programme_intake').valid())
        {


        var tempPR = {};
        tempPR['id_staff'] = $("#id_staff").val();
        tempPR['id_category'] = <?php echo $researchCategory->id;?>;
            $.ajax(
            {
               url: '/research/researchCategory/addResearchCategoryHasSupervisor',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                window.location.reload();
                // $("#view").html(result);
               }
            });
        }
    }

    function deleteResearchCategoryHasSupervisor(id) {
                    // alert(id);

            $.ajax(
            {
               url: '/research/researchCategory/deleteResearchCategoryHasSupervisor/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    window.location.reload();
               }
            });
    }

    



    $(document).ready(function() {
        $("#form_programme_intake").validate({
            rules: {
                id_staff: {
                    required: true
                }
            },
            id_staff: {
                id_programme: {
                    required: "<p class='error-text'>Select Staff</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });



    $(document).ready(function() {
        $("#form_programme").validate({
            rules: {
                name: {
                    required: true
                },
                id_researchCategory_category: {
                    required: true
                },
                status: {
                    required: true
                }
                // ,
                // max_candidates: {
                //     required: true
                // },
                // assignment_requirement: {
                //     required: true
                // }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>Name Required</p>",
                },
                id_researchCategory_category: {
                    required: "<p class='error-text'>Select Topic Category</p>",
                },
                status: {
                    required: "<p class='error-text'>Select Status</p>",
                }
                // ,
                // max_candidates: {
                //     required: "<p class='error-text'>Max. No Of Candidates Required</p>",
                // },
                // assignment_requirement: {
                //     required: "<p class='error-text'>Assignment Required</p>",
                // }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

</script>