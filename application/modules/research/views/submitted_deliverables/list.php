<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>List Submitted Delivarables</h3>
      <!-- <a href="add" class="btn btn-primary">+ Add Topic</a> -->
    </div>

    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Advanced Search
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">
                <div class="row">

                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Student Name</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" name="name" value="<?php echo $searchParam['name']; ?>">
                      </div>
                    </div>
                  </div>

                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Topic</label>
                      <div class="col-sm-8">
                        <select name="id_topic" id="id_topic" class="form-control">
                          <option value="">Select</option>
                          <?php
                          if (!empty($topicList)) {
                            foreach ($topicList as $record)
                            {
                              
                          ?>
                              <option value="<?php echo $record->id;  ?>"
                                <?php
                                if ($record->id == $searchParam['id_topic'])
                                {
                                  echo 'selected';
                                } ?>
                                >
                                <?php echo  $record->name;  ?>
                                </option>
                          <?php
                            }
                          }
                          ?>
                        </select>
                      </div>
                    </div>
                  </div> 

                </div>

                <div class="row">

                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Status</label>
                      <div class="col-sm-8">
                        <select name="status" id="status" class="form-control">
                          <option value="">Select</option>
                          <?php
                          if (!empty($statusList)) {
                            foreach ($statusList as $record)
                            {
                              
                          ?>
                              <option value="<?php echo $record->id;  ?>"
                                <?php
                                if ($record->id == $searchParam['status'])
                                {
                                  echo 'selected';
                                } ?>
                                >
                                <?php echo  $record->code;  ?>
                                </option>
                          <?php
                            }
                          }
                          ?>
                        </select>
                      </div>
                    </div>
                  </div>


                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Supervisor</label>
                      <div class="col-sm-8">
                        <select name="id_supervisor" id="id_supervisor" class="form-control">
                          <option value="">Select</option>
                          <?php
                          if (!empty($supervisorList)) {
                            foreach ($supervisorList as $record)
                            {
                              
                          ?>
                              <option value="<?php echo $record->id;  ?>"
                                <?php
                                if ($record->id == $searchParam['id_supervisor'])
                                {
                                  echo 'selected';
                                } ?>
                                >
                                <?php echo  $record->email . " - " . $record->full_name;  ?>
                                </option>
                          <?php
                            }
                          }
                          ?>
                        </select>
                      </div>
                    </div>
                  </div>



                  <!-- <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Status</label>
                      <div class="col-sm-8">
                        <select name="status" id="status" class="form-control">
                          <option value="">-- All --</option>
                          <option value="0"
                          <?php
                            if ('0' == $searchParam['status'])
                            {
                              echo 'selected';
                            } ?>
                            >Pending</option>
                          <option value="1"
                          <?php
                            if ('1' == $searchParam['status'])
                            {
                              echo 'selected';
                            } ?>
                            >Approved</option>
                          <option value="2"
                          <?php
                            if ('2' == $searchParam['status'])
                            {
                              echo 'selected';
                            } ?>
                            >Rejected</option>
                        </select>
                      </div>
                    </div>
                  </div> -->

                </div>


              </div>
              <div class="app-btn-group">
                <button type="submit" class="btn btn-primary">Search</button>
                <a href='list' class="btn btn-link" >Clear All Fields</a>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>

    <div class="custom-table">
      <table class="table">
        <thead>
          <tr>
            <th>Sl. No</th>
            <th>Application Number</th>
            <th>Student Name</th>
            <th>Student Email</th>
            <th>Supervisor</th>
            <!-- <th>Duration</th>
            <th>Chapter</th> -->
            <th>Topic</th>
            <th>Supervisor</th>
            <th>Submitted On</th>
            <!-- <th>Approved / Rejected On</th> -->
            <th>Status</th>
            <th class="text-center">Action</th>
        </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($deliverablesList)) {
            $i=1;
            foreach ($deliverablesList as $record) {
          ?>
              <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $record->application_number ?></td>
                <td><?php echo $record->student_name ?></td>
                <td><?php echo $record->email_id ?></td>
                <td><?php echo $record->supervisor ?></td>
                <!-- <td><?php echo $record->duration; ?></td>
                <td><?php echo $record->chapter ?></td> -->
                <td><?php echo $record->topic ?></td>
                <td><?php echo $record->supervisor ?></td>
                <td><?php echo date('d-m-Y',strtotime($record->created_dt_tm)) ?></td>
                <!-- <td>
                <?php 
                if($record->approved_on)
                {
                    echo date('d-m-Y',strtotime($record->approved_on)); 
                }
                ?>
                </td> -->
                <td><?php 
                if( $record->status == 0)
                {
                  echo "Pending";
                }
                else
                {
                  echo $record->status_code;
                }
                // if( $record->status == 0)
                // {
                //   echo "Pending";
                // }
                // elseif( $record->status == 1)
                // {
                //   echo "Approved";
                // }
                // elseif( $record->status == 2)
                // {
                //   echo "Rejected";
                // }
                ?></td>
                <td class="text-center">      
                  <a href="<?php echo 'view/' . $record->id; ?>" title="Preview">View</a>
                </td>
            </tr>
          <?php
          $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
<script>
    $('select').select2();
</script>