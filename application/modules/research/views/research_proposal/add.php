<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
            <div class="page-title clearfix">
                <h3>Add Research Proposal</h3>
            </div>

    <form id="form_programme" action="" method="post">
        <div class="form-container">
            <h4 class="form-group-title">Research Proposal Details</h4>


            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select Program <span class='error-text'>*</span></label>
                        <select name="id_programme" id="id_programme" class="form-control" onchange="getPreviousProgramSchemeByProgramId(this.value)">
                            <option value="">Select</option>
                            <?php
                            if (!empty($programList))
                            {
                                foreach ($programList as $record)
                                {?>
                            <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                            </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div> 

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select Intake <span class='error-text'>*</span></label>
                        <span id='view_program_intake'></span>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select Student <span class='error-text'>*</span></label>
                        <span id='view_students_list'></span>
                    </div>
                </div>     

            </div>

            <br>
            
            <div id="view_student_details">
            </div>

            <br>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Title <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name">
                    </div>
                </div>

                
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Research Category <span class='error-text'>*</span></label>
                        <select class="form-control" id="id_research_category" name="id_research_category">
                            <option value="">Select</option>
                            <?php
                            if (!empty($researchCategoryList))
                            {
                                foreach ($researchCategoryList as $record)
                                {?>
                                 <option value="<?php echo $record->id;  ?>">
                                    <?php echo $record->name;?>
                                 </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Research Topic <span class='error-text'>*</span></label>
                        <select class="form-control" id="id_research_topic" name="id_research_topic">
                            <option value="">Select</option>
                            <?php
                            if (!empty($researchTopicList))
                            {
                                foreach ($researchTopicList as $record)
                                {?>
                                 <option value="<?php echo $record->id;  ?>">
                                    <?php echo $record->name;?>
                                 </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

            </div>

            <div class="row">

                
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Synopsis <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="synopsis" name="synopsis">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Thesis Committee Meeting <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="meeting_date" name="meeting_date" autocomplete="off">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Upload</label>
                        <input type="file" class="form-control" id="document" name="document">
                    </div>
                </div>


            </div>

            <div class="row">


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Completion Date </label>
                        <input type="text" class="form-control datepicker" id="completion_date" name="completion_date" autocomplete="off">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Semester Start </label>
                        <input type="text" class="form-control" id="semester_start_date" name="semester_start_date" readonly>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Semester End </label>
                        <input type="text" class="form-control" id="semester_end_date" name="semester_end_date" readonly>
                    </div>
                </div>

            </div>

        </div>
    </form>

    <div class="button-block clearfix">
        <div class="bttn-group">
            <button type="button" class="btn btn-primary btn-lg" onclick="validateProgram()">Save</button>
            <a href="list" class="btn btn-link">Cancel</a>
        </div>
    </div>






    <div class="form-container">
            <h4 class="form-group-title"> Proposal Details</h4>          
            <div class="m-auto text-center">
                <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
            </div>
            <div class="clearfix">
                <ul class="nav nav-tabs offers-tab sub-tabs text-center" role="tablist" >
                    <li role="presentation" class="active" ><a href="#invoice" class="nav-link border rounded text-center"
                            aria-controls="invoice" aria-selected="true"
                            role="tab" data-toggle="tab">Supervisor Details</a>
                    </li>
                    <li role="presentation"><a href="#tab_two" class="nav-link border rounded text-center"
                            aria-controls="tab_two" role="tab" data-toggle="tab">Examiner Details</a>
                    </li>

                      
                </ul>

                
                <div class="tab-content offers-tab-content">

                    <div role="tabpanel" class="tab-pane active" id="invoice">
                        <div class="col-12 mt-4">




                        <form id="form_supervisor" action="" method="post">

                        <div class="form-container">
                        <h4 class="form-group-title">Supervisor Details</h4>

                            <div class="row">


                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Supervisor <span class='error-text'>*</span></label>
                                        <select class="form-control" id="id_staff" name="id_staff">
                                            <option value="">Select</option>
                                            <?php
                                            if (!empty($staffList))
                                            {
                                                foreach ($staffList as $record)
                                                {?>
                                                 <option value="<?php echo $record->id;  ?>">
                                                    <?php echo $record->ic_no . " - " . $record->name;?>
                                                 </option>
                                            <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>


                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Supervisor Role <span class='error-text'>*</span></label>
                                        <select class="form-control" id="id_staff_role" name="id_staff_role">
                                            <option value="">Select</option>
                                            <?php
                                            if (!empty($supervisorRoleList))
                                            {
                                                foreach ($supervisorRoleList as $record)
                                                {?>
                                                 <option value="<?php echo $record->id;  ?>">
                                                    <?php echo $record->code;?>
                                                 </option>
                                            <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>

                                <!-- <div class="col-sm-4">
                                    <div class="form-group">
                                        <p>Status <span class='error-text'>*</span></p>
                                        <label class="radio-inline">
                                          <input type="radio" name="supervisor_status" id="supervisor_status" value="1" checked="checked"><span class="check-radio"></span> Active
                                        </label>
                                        <label class="radio-inline">
                                          <input type="radio" name="supervisor_status" id="supervisor_status" value="0"><span class="check-radio"></span> In-Active
                                        </label>                              
                                    </div>                         
                                </div> -->


                            </div>


                            <div class="row">
                              
                                <div class="col-sm-4">
                                    <button type="button" class="btn btn-primary btn-lg form-row-btn" onclick="tempAddResearchProposalHasSupervisor()">Add</button>
                                </div>
                            </div>

                        </div>


                    </form>


                    <div class="row">
                        <div id="view_supervisor"></div>
                    </div>


                        </div> 
                    </div>





                    <div role="tabpanel" class="tab-pane" id="tab_two">
                        <div class="col-12">




                        <form id="form_examiner" action="" method="post">

                        <div class="form-container">
                        <h4 class="form-group-title">Examiner Details</h4>

                            <div class="row">


                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Examiner <span class='error-text'>*</span></label>
                                        <select class="form-control" id="id_examiner" name="id_examiner">
                                            <option value="">Select</option>
                                            <?php
                                            if (!empty($examinerList))
                                            {
                                                foreach ($examinerList as $record)
                                                {?>
                                                 <option value="<?php echo $record->id;  ?>">
                                                    <?php
                                                    if($record->type == 1)
                                                    {
                                                        echo $record->ic_no . " - " . $record->staff_name;
                                                    }
                                                    else
                                                    {
                                                        echo "External - " . $record->full_name;
                                                    }
                                                    ?>
                                                 </option>
                                            <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>


                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Examiner Role <span class='error-text'>*</span></label>
                                        <select class="form-control" id="id_examiner_role" name="id_examiner_role">
                                            <option value="">Select</option>
                                            <?php
                                            if (!empty($examinerRoleList))
                                            {
                                                foreach ($examinerRoleList as $record)
                                                {?>
                                                 <option value="<?php echo $record->id;  ?>">
                                                    <?php echo $record->code;?>
                                                 </option>
                                            <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>

                                <!-- <div class="col-sm-4">
                                    <div class="form-group">
                                        <p>Status <span class='error-text'>*</span></p>
                                        <label class="radio-inline">
                                          <input type="radio" name="examiner_status" id="examiner_status" value="1" checked="checked"><span class="check-radio"></span> Active
                                        </label>
                                        <label class="radio-inline">
                                          <input type="radio" name="examiner_status" id="examiner_status" value="0"><span class="check-radio"></span> Inactive
                                        </label>                              
                                    </div>                         
                                </div> -->

                            </div>


                            <div class="row">

                              
                                <div class="col-sm-4">
                                    <button type="button" class="btn btn-primary btn-lg form-row-btn" onclick="tempAddResearchProposalHasExaminer()">Add</button>
                                </div>
                            </div>

                        </div>


                    </form>


                    <div class="row">
                        <div id="view_examiner"></div>
                    </div>




                        </div> 
                    </div>




                </div>

            </div>
        

    </div>









    

         
        
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>

<script>

    function getPreviousProgramSchemeByProgramId(id)
    {
        $.get("/research/proposal/getIntakeByProgramme/"+id, function(data, status)
        {
            $("#view_program_intake").html(data);
            $("#view_program_intake").show();
        });
    }


    function getStudentByData()
    {
        // alert('sas');
        var id_programme = $("#id_programme").val();
        var id_intake = $("#id_intake").val();

         

        if(id_programme != '' && id_intake != '')
        {

        var tempPR = {};
       
        tempPR['id_programme'] = $("#id_programme").val();
        tempPR['id_intake'] = $("#id_intake").val();


            $.ajax(
            {
                url: '/research/proposal/getStudentByData',
                type: 'POST',
                data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view_students_list").html(result);
               }
            });
        }
    }



     function getStudentByStudentId(id)
     {

         $.get("/research/proposal/getStudentByStudentId/"+id, function(data, status){
       
            $("#view_student_details").html(data);
            $("#view_student_details").show();
        });
     }






    function tempAddResearchProposalHasSupervisor()
    {
        if($('#form_supervisor').valid())
        {

        var tempPR = {};
        tempPR['id_staff'] = $("#id_staff").val();
        tempPR['id_staff_role'] = $("#id_staff_role").val();
        // tempPR['status'] = $("#supervisor_status").val();
        tempPR['status'] = 1;

            $.ajax(
            {
               url: '/research/proposal/tempAddResearchProposalHasSupervisor',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view_supervisor").html(result);
               }
            });
        }
    }

    function deleteTempResearchProposalHasSupervisor(id) {
        // alert(id);
         $.ajax(
            {
               url: '/research/proposal/deleteTempResearchProposalHasSupervisor/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    $("#view_supervisor").html(result);
               }
            });
    }



    function tempAddResearchProposalHasExaminer()
    {
        if($('#form_examiner').valid())
        {

        var tempPR = {};
        tempPR['id_examiner'] = $("#id_examiner").val();
        tempPR['id_examiner_role'] = $("#id_examiner_role").val();
        // tempPR['status'] = $("#examiner_status").val();
        tempPR['status'] = 1;

            $.ajax(
            {
               url: '/research/proposal/tempAddResearchProposalHasExaminer',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view_examiner").html(result);
               }
            });
        }
    }

    function deleteTempResearchProposalHasExaminer(id) {
        // alert(id);
         $.ajax(
            {
               url: '/research/proposal/deleteTempResearchProposalHasExaminer/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    $("#view_examiner").html(result);
               }
            });
    }
    

    $(document).ready(function() {
        $("#form_supervisor").validate({
            rules: {
                id_staff: {
                    required: true
                },
                id_staff_role: {
                    required: true
                }
            },
            messages: {
                id_staff: {
                    required: "<p class='error-text'>Select Supervisor</p>",
                },
                id_staff_role: {
                    required: "<p class='error-text'>Select Supervisor Role</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


    $(document).ready(function() {
        $("#form_examiner").validate({
            rules: {
                id_examiner: {
                    required: true
                },
                id_examiner_role: {
                    required: true
                }
            },
            messages: {
                id_examiner: {
                    required: "<p class='error-text'>Select Examiner</p>",
                },
                id_examiner_role: {
                    required: "<p class='error-text'>Select Examiner Role</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

    $(document).ready(function() {
        $("#form_programme").validate({
            rules: {
                id_intake: {
                    required: true
                },
                id_programme: {
                    required: true
                },
                id_student: {
                    required: true
                },
                name: {
                    required: true
                },
                id_research_topic: {
                    required: true
                },
                id_research_category: {
                    required: true
                },
                synopsis: {
                    required: true
                },
                meeting_date: {
                    required: true
                }
            },
            messages: {
                id_intake: {
                    required: "<p class='error-text'>Select Intake</p>",
                },
                id_programme: {
                    required: "<p class='error-text'>Select Program</p>",
                },
                id_student: {
                    required: "<p class='error-text'>Select Student</p>",
                },
                name: {
                    required: "<p class='error-text'>Title Required</p>",
                },
                id_research_topic: {
                    required: "<p class='error-text'>Select Research Topic</p>",
                },
                id_research_category: {
                    required: "<p class='error-text'>Select Research Category</p>",
                },
                synopsis: {
                    required: "<p class='error-text'>Synopsis Required</p>",
                },
                meeting_date: {
                    required: "<p class='error-text'>Meeting Date Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

    function validateProgram() {

    if($('#form_programme').valid())
      {
        console.log($("#view_supervisor").html());
        var dataSupervisor = $("#view_supervisor").html();
        var dataExaminer = $("#view_examiner").html();
        if(dataSupervisor=='')
        {
            alert("Add Supervisor To Research Proposal");
        }
        else if(dataExaminer=='')
        {
            alert("Add Examiner To Research Proposal");
        }else
        {
            $('#form_programme').submit();
        }
      }     
    }

    $('select').select2();


    $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
        yearRange: "2019:2020"
    });
  } );
</script>