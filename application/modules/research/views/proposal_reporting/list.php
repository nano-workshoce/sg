<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>List Research Proposal Reorting(Stage <?php echo $stage; ?>)</h3>
      <!-- <a href="add" class="btn btn-primary">+ Add Topic</a> -->
    </div>

    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Advanced Search
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">
                <div class="row">

                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Student Name</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" name="name" value="<?php echo $searchParam['name']; ?>">
                      </div>
                    </div>
                  </div>

                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Chapter</label>
                      <div class="col-sm-8">
                        <select name="id_chapter" id="id_chapter" class="form-control">
                          <option value="">Select</option>
                          <?php
                          if (!empty($chapterList)) {
                            foreach ($chapterList as $record)
                            {
                              
                          ?>
                              <option value="<?php echo $record->id;  ?>"
                                <?php
                                if ($record->id == $searchParam['id_chapter'])
                                {
                                  echo 'selected';
                                } ?>
                                >
                                <?php echo  $record->name;  ?>
                                </option>
                          <?php
                            }
                          }
                          ?>
                        </select>
                      </div>
                    </div>
                  </div> 

                </div>

                <div class="row">

                  
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Supervisor</label>
                      <div class="col-sm-8">
                        <select name="id_supervisor" id="id_supervisor" class="form-control">
                          <option value="">Select</option>
                          <?php
                          if (!empty($supervisorList)) {
                            foreach ($supervisorList as $record)
                            {
                              
                          ?>
                              <option value="<?php echo $record->id;  ?>"
                                <?php
                                if ($record->id == $searchParam['id_supervisor'])
                                {
                                  echo 'selected';
                                } ?>
                                >
                                <?php echo  $record->email . " - " . $record->full_name;  ?>
                                </option>
                          <?php
                            }
                          }
                          ?>
                        </select>
                      </div>
                    </div>
                  </div>

                </div>


              </div>
              <div class="app-btn-group">
                <button type="submit" class="btn btn-primary">Search</button>
                <a href="<?php $st = $stage; if($stage == 1) { $st = ''; } echo 'list' . $st ?>" class="btn btn-link" >Clear All Fields</a>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>

    <div class="custom-table">
      <table class="table">
        <thead>
          <tr>
            <th>Sl. No</th>
            <th>Application Number</th>
            <th>Chapter</th>
            <th>Duration</th>
            <th>Deliverable</th>
            <th>Start Date</th>
            <th>End Date</th>
            <th>Targeted Date</th>
            <th>Supervisor</th>
            <th>Student</th>
            <th>Submitted On</th>
            <!-- <th>Approved / Rejected On</th> -->
            <th>Status</th>
            <th class="text-center">Action</th>
        </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($deliverablesList)) {
            $i=1;
            foreach ($deliverablesList as $record) {
          ?>
              <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $record->application_number ?></td>
                <td><?php echo $record->chapter ?></td>
                <td><?php echo $record->phd_duration; ?></td>
                <td><?php echo $record->deliverable ?></td>
                <td><?php echo date('d-m-Y',strtotime($record->from_dt)) ?></td>
                <td><?php echo date('d-m-Y',strtotime($record->to_dt)) ?></td>
                <td><?php echo date('d-m-Y',strtotime($record->target_dt)) ?></td>
                <td><?php echo $record->supervisor_email . " - " . $record->supervisor_name ?></td>
                <td><?php echo $record->nric . " - " . $record->student_name ?></td>
                <td><?php echo date('d-m-Y',strtotime($record->created_dt_tm)) ?></td>
                <!-- <td>
                <?php 
                if($record->approved_on)
                {
                    echo date('d-m-Y',strtotime($record->approved_on)); 
                }
                ?>
                </td> -->
                <td><?php 
                if( $record->status == 0)
                {
                  echo "Pending";
                }
                elseif( $record->status == 1)
                {
                  echo "Approved";
                }
                elseif( $record->status == 2)
                {
                  echo "Rejected";
                }
                ?></td>
                <td class="text-center">
                    <a href="<?php echo 'view' . $st . '/' . $record->id; ?>" title="View Comments">Comments</a>
                </td>
            </tr>
          <?php
          $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
<script>
    $('select').select2();
</script>