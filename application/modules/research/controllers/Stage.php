<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Stage extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('stage_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('research_stage.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['type'] = $this->security->xss_clean($this->input->post('type'));

            $data['searchParameters'] = $formData;
            $data['stageList'] = $this->stage_model->stageListSearch($formData);
            
            $this->global['pageTitle'] = 'Campus Management System : Stage List';
            $this->loadViews("stage/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('research_stage.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $id_user = $this->session->userId;
                $id_session = $this->session->my_session_id;

                $name = $this->security->xss_clean($this->input->post('name'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $type = $this->security->xss_clean($this->input->post('type'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'description' => $description,
                    'type' => $type,
                    'status' => $status,
                    'created_by' => $id_user
                );

                $result = $this->stage_model->addNewStage($data);
                redirect('/research/stage/list');
            }



            $this->global['pageTitle'] = 'Campus Management System : Add Stage';
            $this->loadViews("stage/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('research_stage.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/research/stage/list');
            }
            if($this->input->post())
            {
                $id_user = $this->session->userId;
                $id_session = $this->session->my_session_id;

                $name = $this->security->xss_clean($this->input->post('name'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $type = $this->security->xss_clean($this->input->post('type'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'description' => $description,
                    'type' => $type,
                    'status' => $status,
                    'updated_by' => $id_user
                );

                $result = $this->stage_model->editStage($data,$id);
                redirect('/research/stage/list');
            }

            $data['stage'] = $this->stage_model->getStage($id);

            $this->global['pageTitle'] = 'Campus Management System : Edit Stage';
            $this->loadViews("stage/edit", $this->global, $data, NULL);
        }
    }

    function addSemester($id = NULL)
    {
        if ($this->checkAccess('research_stage.add_semester') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/research/stage/list');
            }
            if($this->input->post())
            {
                // echo "<Pre>"; print_r($this->input->post());exit;

                $id_user = $this->session->userId;
                $id_session = $this->session->my_session_id;

                // $name = $this->security->xss_clean($this->input->post('name'));
                $semester_type = $this->security->xss_clean($this->input->post('semester_type'));
                $from_semester = $this->security->xss_clean($this->input->post('from_semester'));
                $to_semester = $this->security->xss_clean($this->input->post('to_semester'));
                $status = $this->security->xss_clean($this->input->post('status'));

                if($semester_type == 'Regular')
                {
                    $name = "Semester ". $from_semester;
                }
                elseif($semester_type == 'Range')
                {
                    $name = "Semester ". $from_semester . " - " . $to_semester;
                }
            
                $data = array(
                    'id_stage' => $id,
                    'name' => $name,
                    'type' => $semester_type,
                    'from' => $from_semester,
                    'to' => $to_semester,
                    'status' => 1,
                    'created_by' => $id_user
                );
                // echo "<Pre>"; print_r($data);exit;

                $result = $this->stage_model->addSemesterStage($data);

                redirect('/research/stage/addSemester/'.$id);
            }

            $data['stage'] = $this->stage_model->getStage($id);
            $data['stageSemesterList'] = $this->stage_model->getSemesterStage($id);

            $this->global['pageTitle'] = 'Campus Management System : Edit Stage';
            $this->loadViews("stage/add_semester", $this->global, $data, NULL);
        }
    }

    function deleteSemsterStage($id)
    {
        $deleted = $this->stage_model->deleteSemsterStage($id);
        echo 'success';exit;
    }

    function overview()
    {
        if ($this->checkAccess('research_stage.overview') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            // if($this->input->post())
            // {
            //     $id_user = $this->session->userId;
            //     $id_session = $this->session->my_session_id;

            //     $name = $this->security->xss_clean($this->input->post('name'));
            //     $description = $this->security->xss_clean($this->input->post('description'));
            //     $type = $this->security->xss_clean($this->input->post('type'));
            //     $status = $this->security->xss_clean($this->input->post('status'));
            
            //     $data = array(
            //         'name' => $name,
            //         'description' => $description,
            //         'type' => $type,
            //         'status' => $status,
            //         'updated_by' => $id_user
            //     );

            //     $result = $this->stage_model->editStage($data,$id);
            //     redirect('/research/stage/list');
            // }

            $data['stageOverview'] = $this->stage_model->getStageOverview('');


            // $data['stageOverview'] = $this->stage_model->getStageOverviewList();

            // echo "<Pre>"; print_r($data['stageOverviewList']);exit;


            $this->global['pageTitle'] = 'Campus Management System : Overview Stage';
            $this->loadViews("stage/overview", $this->global, $data, NULL);
            // $this->loadViews("stage/overview_new", $this->global, $data, NULL);
        }
    }
}
