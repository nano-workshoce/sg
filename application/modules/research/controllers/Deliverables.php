<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Deliverables extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('deliverables_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('research_deliverables.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $data['searchParam'] = $formData;

            $data['deliverablesList'] = $this->deliverables_model->deliverablesListSearch($formData);
            
            // echo "<Pre>"; print_r($data['deliverablesList']);exit;
            
            $this->global['pageTitle'] = 'College Management System : Deliverables List';
            $this->loadViews("deliverables/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('research_deliverables.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $id_phd_duration = $this->security->xss_clean($this->input->post('id_phd_duration'));
                $id_chapter = $this->security->xss_clean($this->input->post('id_chapter'));
                $topic = $this->security->xss_clean($this->input->post('topic'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'id_phd_duration' => $id_phd_duration,
                    'id_chapter' => $id_chapter,
                    'topic' => $topic,
                    'status' => $status
                );
                // echo "<Pre>"; print_r($data);exit;

                $result = $this->deliverables_model->addNewDeliverables($data);
                redirect('/research/deliverables/list');
            }

            $data['durationList'] = $this->deliverables_model->durationListByStatus('1');
            $data['chapterList'] = $this->deliverables_model->chapterListByStatus('1');

            $this->global['pageTitle'] = 'College Management System : Add Deliverables';
            $this->loadViews("deliverables/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('research_deliverables.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/research/deliverables/list');
            }
            if($this->input->post())
            {
                $id_phd_duration = $this->security->xss_clean($this->input->post('id_phd_duration'));
                $id_chapter = $this->security->xss_clean($this->input->post('id_chapter'));
                $topic = $this->security->xss_clean($this->input->post('topic'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'id_phd_duration' => $id_phd_duration,
                    'id_chapter' => $id_chapter,
                    'topic' => $topic,
                    'status' => $status
                );

                $result = $this->deliverables_model->editDeliverables($data,$id);
                redirect('/research/deliverables/list');
            }
            
            $data['durationList'] = $this->deliverables_model->durationListByStatus('1');
            $data['chapterList'] = $this->deliverables_model->chapterListByStatus('1');

            $data['deliverables'] = $this->deliverables_model->getDeliverables($id);
            
            $this->global['pageTitle'] = 'College Management System : Edit Deliverables';
            $this->loadViews("deliverables/edit", $this->global, $data, NULL);
        }
    }
}