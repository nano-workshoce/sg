<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class ProposalReporting extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('proposal_reporting_model');
        $this->isLoggedIn();
    }

    function list()
    {   
        if ($this->checkAccess('proposal_reporting.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_chapter'] = $this->security->xss_clean($this->input->post('id_chapter'));
            $formData['id_supervisor'] = $this->security->xss_clean($this->input->post('id_supervisor'));
            $formData['status'] = $this->security->xss_clean($this->input->post('status'));

            $data['searchParam'] = $formData;

            $data['chapterList'] = $this->proposal_reporting_model->chapterListByStatus('1');
            $data['supervisorList'] = $this->proposal_reporting_model->supervisorListByStatus('1');

            $data['deliverablesList'] = $this->proposal_reporting_model->getProposalReportingLisSearch($formData,'1');
            $data['stage'] = 1;

            // echo "<Pre>";print_r($data['deliverablesList']);exit();

            $this->global['pageTitle'] = 'College management System : List Proposal Reporting Application';
            $this->loadViews("proposal_reporting/list", $this->global, $data, NULL);
        }
    }

    function view($id = NULL)
    {
        if ($this->checkAccess('proposal_reporting.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            if ($id == null)
            {
                redirect('/student/proposalReporting/list');
            }

            if($this->input->post())
            {
                if($_FILES['upload_file'])
                {
                // echo "<Pre>"; print_r($_FILES['image']);exit;

                    $certificate_name = $_FILES['upload_file']['name'];
                    $certificate_size = $_FILES['upload_file']['size'];
                    $certificate_tmp =$_FILES['upload_file']['tmp_name'];
                    
                    // echo "<Pre>"; print_r($certificate_tmp);exit();

                    $certificate_ext=explode('.',$certificate_name);
                    $certificate_ext=end($certificate_ext);
                    $certificate_ext=strtolower($certificate_ext);


                    $this->fileFormatNSizeValidation($certificate_ext,$certificate_size,'Image File');

                    $upload_file = $this->uploadFile($certificate_name,$certificate_tmp,'Image File');
                }

                $comments = $this->security->xss_clean($this->input->post('comments'));

                $comments_data = array(
                    'comments' => $comments,
                    'id_supervisor' => $id_supervisor,
                    'id_proposal_reporting' => $id
                );

                if($upload_file)
                {
                    $comments_data['upload_file'] = $upload_file;
                }


                $added_comments = $this->proposal_reporting_model->addProposalReportingComments($comments_data);
                redirect($_SERVER['HTTP_REFERER']);
                // echo "<Pre>"; print_r($this->input->post());exit;
            }

            $data['proposalReporting'] = $this->proposal_reporting_model->getProposalReport($id);
            
            $id_supervisor = $data['proposalReporting']->id_supervisor; 
            $id_student = $data['proposalReporting']->id_student; 
            
            $data['proposalReportingComments'] = $this->proposal_reporting_model->getProposalReportComments($id);

            $data['studentDetails'] = $this->proposal_reporting_model->getStudentByStudentId($id_student);
            $data['supervisor'] = $this->proposal_reporting_model->getSupervisor($id_supervisor);
            
            $data['organisationDetails'] = $this->proposal_reporting_model->getOrganisation();

            $data['durationList'] = $this->proposal_reporting_model->durationListByStatus('1');
            $data['chapterList'] = $this->proposal_reporting_model->chapterListByStatus('1');
            $data['deliverableList'] = $this->proposal_reporting_model->deliverableListByStatus('1');
            $data['stage'] = 1;
                
            // echo "<Pre>"; print_r($data['proposalReportingComments']);exit;

            $this->global['pageTitle'] = 'College management System : View Proposal Reporting Comments';
            $this->loadViews("proposal_reporting/view", $this->global, $data, NULL);
        }
    }



    function list2()
    {   
        if ($this->checkAccess('proposal_reporting.list_stage2') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_chapter'] = $this->security->xss_clean($this->input->post('id_chapter'));
            $formData['id_supervisor'] = $this->security->xss_clean($this->input->post('id_supervisor'));
            $formData['status'] = $this->security->xss_clean($this->input->post('status'));

            $data['searchParam'] = $formData;

            $data['chapterList'] = $this->proposal_reporting_model->chapterListByStatus('1');
            $data['supervisorList'] = $this->proposal_reporting_model->supervisorListByStatus('1');

            $data['deliverablesList'] = $this->proposal_reporting_model->getProposalReportingLisSearch($formData,'2');
            $data['stage'] = 2;

            // echo "<Pre>";print_r($data['deliverablesList']);exit();

            $this->global['pageTitle'] = 'College management System : List Proposal Reporting Application';
            $this->loadViews("proposal_reporting/list", $this->global, $data, NULL);
        }
    }

    function view2($id = NULL)
    {
        if ($this->checkAccess('proposal_reporting.view_stage2') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            if ($id == null)
            {
                redirect('/student/proposalReporting/list2');
            }

            if($this->input->post())
            {
                if($_FILES['upload_file'])
                {
                // echo "<Pre>"; print_r($_FILES['image']);exit;

                    $certificate_name = $_FILES['upload_file']['name'];
                    $certificate_size = $_FILES['upload_file']['size'];
                    $certificate_tmp =$_FILES['upload_file']['tmp_name'];
                    
                    // echo "<Pre>"; print_r($certificate_tmp);exit();

                    $certificate_ext=explode('.',$certificate_name);
                    $certificate_ext=end($certificate_ext);
                    $certificate_ext=strtolower($certificate_ext);


                    $this->fileFormatNSizeValidation($certificate_ext,$certificate_size,'Image File');

                    $upload_file = $this->uploadFile($certificate_name,$certificate_tmp,'Image File');
                }

                $comments = $this->security->xss_clean($this->input->post('comments'));

                $comments_data = array(
                    'comments' => $comments,
                    'id_supervisor' => $id_supervisor,
                    'id_proposal_reporting' => $id
                );

                if($upload_file)
                {
                    $comments_data['upload_file'] = $upload_file;
                }


                $added_comments = $this->proposal_reporting_model->addProposalReportingComments($comments_data);
                redirect($_SERVER['HTTP_REFERER']);
                // echo "<Pre>"; print_r($this->input->post());exit;
            }

            $data['proposalReporting'] = $this->proposal_reporting_model->getProposalReport($id);
            
            $id_supervisor = $data['proposalReporting']->id_supervisor; 
            $id_student = $data['proposalReporting']->id_student; 
            
            $data['proposalReportingComments'] = $this->proposal_reporting_model->getProposalReportComments($id);

            $data['studentDetails'] = $this->proposal_reporting_model->getStudentByStudentId($id_student);
            $data['supervisor'] = $this->proposal_reporting_model->getSupervisor($id_supervisor);
            
            $data['organisationDetails'] = $this->proposal_reporting_model->getOrganisation();

            $data['durationList'] = $this->proposal_reporting_model->durationListByStatus('1');
            $data['chapterList'] = $this->proposal_reporting_model->chapterListByStatus('1');
            $data['deliverableList'] = $this->proposal_reporting_model->deliverableListByStatus('1');
            $data['stage'] = 2;
                
            // echo "<Pre>"; print_r($data['proposalReportingComments']);exit;

            $this->global['pageTitle'] = 'College management System : View Proposal Reporting Comments';
            $this->loadViews("proposal_reporting/view", $this->global, $data, NULL);
        }
    }

    function list3()
    {   
        if ($this->checkAccess('proposal_reporting.list_stage3') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_chapter'] = $this->security->xss_clean($this->input->post('id_chapter'));
            $formData['id_supervisor'] = $this->security->xss_clean($this->input->post('id_supervisor'));
            $formData['status'] = $this->security->xss_clean($this->input->post('status'));

            $data['searchParam'] = $formData;

            $data['chapterList'] = $this->proposal_reporting_model->chapterListByStatus('1');
            $data['supervisorList'] = $this->proposal_reporting_model->supervisorListByStatus('1');

            $data['deliverablesList'] = $this->proposal_reporting_model->getProposalReportingLisSearch($formData,'3');
            $data['stage'] = 3;

            // echo "<Pre>";print_r($data['deliverablesList']);exit();

            $this->global['pageTitle'] = 'College management System : List Proposal Reporting Application';
            $this->loadViews("proposal_reporting/list", $this->global, $data, NULL);
        }
    }

    function view3($id = NULL)
    {
        if ($this->checkAccess('proposal_reporting.view_stage3') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            if ($id == null)
            {
                redirect('/student/proposalReporting/list3');
            }

            if($this->input->post())
            {
                if($_FILES['upload_file'])
                {
                // echo "<Pre>"; print_r($_FILES['image']);exit;

                    $certificate_name = $_FILES['upload_file']['name'];
                    $certificate_size = $_FILES['upload_file']['size'];
                    $certificate_tmp =$_FILES['upload_file']['tmp_name'];
                    
                    // echo "<Pre>"; print_r($certificate_tmp);exit();

                    $certificate_ext=explode('.',$certificate_name);
                    $certificate_ext=end($certificate_ext);
                    $certificate_ext=strtolower($certificate_ext);


                    $this->fileFormatNSizeValidation($certificate_ext,$certificate_size,'Image File');

                    $upload_file = $this->uploadFile($certificate_name,$certificate_tmp,'Image File');
                }

                $comments = $this->security->xss_clean($this->input->post('comments'));

                $comments_data = array(
                    'comments' => $comments,
                    'id_supervisor' => $id_supervisor,
                    'id_proposal_reporting' => $id
                );

                if($upload_file)
                {
                    $comments_data['upload_file'] = $upload_file;
                }


                $added_comments = $this->proposal_reporting_model->addProposalReportingComments($comments_data);
                redirect($_SERVER['HTTP_REFERER']);
                // echo "<Pre>"; print_r($this->input->post());exit;
            }

            $data['proposalReporting'] = $this->proposal_reporting_model->getProposalReport($id);
            
            $id_supervisor = $data['proposalReporting']->id_supervisor; 
            $id_student = $data['proposalReporting']->id_student; 
            
            $data['proposalReportingComments'] = $this->proposal_reporting_model->getProposalReportComments($id);

            $data['studentDetails'] = $this->proposal_reporting_model->getStudentByStudentId($id_student);
            $data['supervisor'] = $this->proposal_reporting_model->getSupervisor($id_supervisor);
            
            $data['organisationDetails'] = $this->proposal_reporting_model->getOrganisation();

            $data['durationList'] = $this->proposal_reporting_model->durationListByStatus('1');
            $data['chapterList'] = $this->proposal_reporting_model->chapterListByStatus('1');
            $data['deliverableList'] = $this->proposal_reporting_model->deliverableListByStatus('1');
            $data['stage'] = 3;
                
            // echo "<Pre>"; print_r($data['proposalReportingComments']);exit;

            $this->global['pageTitle'] = 'College management System : View Proposal Reporting Comments';
            $this->loadViews("proposal_reporting/view", $this->global, $data, NULL);
        }
    }

    function getChapterByDuration($id_duration)
    {
        $results = $this->proposal_reporting_model->getChapterByDuration($id_duration);

        // echo "<Pre>"; print_r($results);exit;

        $table="

         <script type='text/javascript'>
                 $('select').select2();
         </script>


        <select name='id_chapter' id='id_chapter' class='form-control' onchange='getTopicByData()'>
            <option value=''>Select</option>
            ";

        for($i=0;$i<count($results);$i++)
        {

        // $id = $results[$i]->id_procurement_category;
        $id = $results[$i]->id;
        $name = $results[$i]->name;
        $table.="<option value=".$id.">".$name.
                "</option>";

        }
        $table.="

        </select>";

        echo $table;
        exit;
    }

    function getTopicByData()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        $results = $this->proposal_reporting_model->getTopicByData($tempData);

        // echo "<Pre>"; print_r($results);exit;

        $table="

         <script type='text/javascript'>
                 $('select').select2();
         </script>


        <select name='id_deliverable' id='id_deliverable' class='form-control'>
            <option value=''>Select</option>
            ";

        for($i=0;$i<count($results);$i++)
        {

        // $id = $results[$i]->id_procurement_category;
        $id = $results[$i]->id;
        $name = $results[$i]->topic;
        $table.="<option value=".$id.">".$name.
                "</option>";

        }
        $table.="

        </select>";

        echo $table;
        exit;
    }
}

