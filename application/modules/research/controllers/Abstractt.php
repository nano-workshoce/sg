<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Abstractt extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('abstract_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if($this->checkAccess('research_abstract.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_supervisor'] = $this->security->xss_clean($this->input->post('id_supervisor'));

            $data['searchParam'] = $formData;

            $data['abstractList'] = $this->abstract_model->abstractListSearch($formData);

            $data['supervisorList'] = $this->abstract_model->supervisorListByStatus('1');

            // echo "<Pre>";print_r($data['abstractList']);exit();

            $this->global['pageTitle'] = 'College Management System : List Abstract Reporting';
            $this->loadViews("abstract/list", $this->global, $data, NULL);
        }
    }

    function view($id = NULL)
    {
        if($this->checkAccess('research_abstract.view') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($id == null)
            {
                redirect('/research/abstractt/list');
            }
            $data['abstract'] = $this->abstract_model->getAbstract($id);
            $data['abstractReportingComments'] = $this->abstract_model->abstractCommentsDetails($id);

            $data['organisationDetails'] = $this->abstract_model->getOrganisation();
            $data['supervisor'] = $this->abstract_model->getSupervisor($data['abstract']->id_supervisor);
            $data['studentDetails'] = $this->abstract_model->getStudentByStudentId($data['abstract']->id_student);
                
            // echo "<Pre>"; print_r($data);exit;

            $this->global['pageTitle'] = 'College Management System : View Abstract Reporting';
            $this->loadViews("abstract/edit", $this->global, $data, NULL);
        }
    }
}