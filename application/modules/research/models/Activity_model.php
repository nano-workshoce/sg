<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Activity_model extends CI_Model
{
    function activityList()
    {
        $this->db->select('*');
        $this->db->from('research_activity');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function activityListSearch($search)
    {
        $this->db->select('*');
        $this->db->from('research_activity');
        if (!empty($search))
        {
            $likeCriteria = "(name  LIKE '%" . $search . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getActivity($id)
    {
        $this->db->select('*');
        $this->db->from('research_activity');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewActivity($data)
    {
        $this->db->trans_start();
        $this->db->insert('research_activity', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editActivity($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('research_activity', $data);
        return TRUE;
    }
}

