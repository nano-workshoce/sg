<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Deliverables_model extends CI_Model
{
    function deliverablesList()
    {
        $this->db->select('*');
        $this->db->from('research_deliverables');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function durationListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('research_phd_duration');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result();  
        
        return $result;
    }
    
    function chapterListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('research_chapter');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result();  
        
        return $result;
    }

    function deliverablesListSearch($data)
    {
        $this->db->select('rd.*, rc.name as chapter, rpd.name as duration');
        $this->db->from('research_deliverables as rd');
        $this->db->join('research_chapter as rc','rd.id_chapter = rc.id');
        $this->db->join('research_phd_duration as rpd','rd.id_phd_duration = rpd.id');
        if ($data['name'] != '')
        {
            $likeCriteria = "(rd.topic  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("topic", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getDeliverables($id)
    {
        $this->db->select('*');
        $this->db->from('research_deliverables');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewDeliverables($data)
    {
        $this->db->trans_start();
        $this->db->insert('research_deliverables', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editDeliverables($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('research_deliverables', $data);
        return TRUE;
    }
}

