<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Mile_stone_model extends CI_Model
{
    function mileStoneList()
    {
        $this->db->select('a.*');
        $this->db->from('mile_stone as a');
        $this->db->order_by("id", "DESC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function mileStoneListSearch($formData)
    {
        $this->db->select('a.*');
        $this->db->from('mile_stone as a');
        if (!empty($formData['name']))
        {
            $likeCriteria = "(a.name  LIKE '%" . $formData['name'] . "%' or a.description  LIKE '%" . $formData['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        // if (!empty($formData['type']))
        // {
        //     $this->db->where('a.type', $formData['type']);
        // }
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getMileStone($id)
    {
        $this->db->select('*');
        $this->db->from('mile_stone');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewMileStone($data)
    {
        $this->db->trans_start();
        $this->db->insert('mile_stone', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editMileStone($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('mile_stone', $data);
        return TRUE;
    }
}