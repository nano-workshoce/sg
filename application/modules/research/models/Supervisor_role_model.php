<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Supervisor_role_model extends CI_Model
{
    function supervisorRoleList()
    {
        $this->db->select('*');
        $this->db->from('supervisor_role');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function supervisorRoleListSearch($data)
    {
        $this->db->select('*');
        $this->db->from('supervisor_role');
        if ($data['name'])
        {
            $likeCriteria = "(code  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("code", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getSupervisorRole($id)
    {
        $this->db->select('*');
        $this->db->from('supervisor_role');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewSupervisorRole($data)
    {
        $this->db->trans_start();
        $this->db->insert('supervisor_role', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editSupervisorRole($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('supervisor_role', $data);
        return TRUE;
    }
}

