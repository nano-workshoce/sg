<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit Question Pool</h3>
        </div>
        <form id="form_grade" action="" method="post">


        <div class="form-container">
            <h4 class="form-group-title">Question Pool Details</h4>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Question Pool Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name" value="<?php echo $setForm->name; ?>" readonly>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Branch <span class='error-text'>*</span></label>
                        <select name="id_branch" id="id_branch" class="form-control selitemIcon" disabled="true">
                            <option value="">Select</option>
                            <?php
                            if (!empty($organisationList))
                            {
                                foreach ($organisationList as $record)
                                {?>
                            <option value="<?php echo $record->id;  ?>"
                                <?php if($setForm->id_branch==$record->id)
                                { 
                                    echo "selected";
                                } 
                                ?>>
                                <?php echo $record->name ." -" . $record->permanent_address;?>
                            </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Semester <span class='error-text'>*</span></label>
                        <select name="id_semester" id="id_semester" class="form-control selitemIcon" disabled="true">
                            <option value="">Select</option>
                            <?php
                            if (!empty($semesterList))
                            {
                                foreach ($semesterList as $record)
                                {?>
                            <option value="<?php echo $record->id;  ?>"
                                <?php if($setForm->id_semester==$record->id)
                                { 
                                    echo "selected";
                                } 
                                ?>>
                                <?php echo $record->code ." -" . $record->name;?>
                            </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>          
                
            </div>



            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Program <span class='error-text'>*</span></label>
                        <select name="id_program" id="id_program" class="form-control selitemIcon" disabled="true">
                            <option value="">Select</option>
                            <?php
                            if (!empty($programList))
                            {
                                foreach ($programList as $record)
                                {?>
                            <option value="<?php echo $record->id;  ?>"
                                <?php if($setForm->id_program==$record->id)
                                { 
                                    echo "selected";
                                } 
                                ?>>
                                <?php echo $record->code ." -" . $record->name;?>
                            </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
                

                <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" <?php if($setForm->status=='1') {
                                 echo "checked=checked";
                              };?> disabled="true"><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0" <?php if($setForm->status=='0') {
                                 echo "checked=checked";
                              };?> disabled="true">
                              <span class="check-radio"></span> In-Active
                            </label>                              
                        </div>                         
                </div>     

            </div>


            

            <?php
            if($setForm->is_publish == '0' && $setForm->status == '1')
            {
             ?>


             <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <p>Publish Evaluation <span class='error-text'>*</span></p>
                        <label class="radio-inline">
                            <input type="radio" id="ed1" name="is_publish" value="1" onclick="hideRejectField()"><span class="check-radio"></span> Approve
                        </label>
                    </div>
                </div>

            </div>

          <?php
            }
            ?>

        </div>



        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="../list" class="btn btn-link">Cancel</a>
            </div>
        </div>

       


        <!-- <div class="page-title clearfix">
            <h3 class="form-group-title">Question Pool List</h3>
        </div> -->

        <div class="form-container">
                <h4 class="form-group-title">Question Pool List</h4>

            <div class="custom-table">
              <table class="table" id="list-table">
                <thead>
                  <tr>  
                    <!-- <th>Sl. No</th> -->
                    <th ></th>
                    <th  style="text-align: left">Name</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  if (!empty($questionPoolList))
                  {
                    $j=1;
                    // foreach ($setFormQuestionPoolList as $setFormPool)
                    // {
                        foreach ($questionPoolList as $record)
                        {
                    ?>
                      <tr>
                        <td><?php echo $j ?></td>
                        <td style="text-align: left" value="<?php echo $record->id ?>">&emsp; &emsp; <?php echo $record->name ?></td>
                      </tr>
                  <?php
                  $j++;
                    }
                  }
                  ?>
                </tbody>
              </table>
            </div>

        </div>



        
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>

    $('select').select2();
    
    $(document).ready(function() {
        $("#form_grade").validate({
            rules: {
                is_publish: {
                    required: true
                }
            },
            messages: {
                is_publish: {
                    required: "<p class='error-text'>Select Publish Evaluation</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>