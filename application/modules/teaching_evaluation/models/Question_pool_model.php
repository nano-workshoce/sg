<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Question_pool_model extends CI_Model
{
    function questionPoolList()
    {
        $this->db->select('*');
        $this->db->from('question_pool');
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }


    function questionPoolListSearch($search)
    {
        $this->db->select('*');
        $this->db->from('question_pool');
        if (!empty($search))
        {
            $likeCriteria = "(name  LIKE '%" . $search . "%')";
            $this->db->where($likeCriteria);
        }
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function getQuestionPoolDetails($id)
    {
        $this->db->select('*');
        $this->db->from('question_pool');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewQuestionPool($data)
    {
        $this->db->trans_start();
        $this->db->insert('question_pool', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editQuestionPool($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('question_pool', $data);
        return TRUE;
    }
}

