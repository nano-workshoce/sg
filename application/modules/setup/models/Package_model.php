<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Package_model extends CI_Model
{
    function packageList()
    {
        $this->db->select('*');
        $this->db->from('package');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function packageListSearch($data)
    {
        $this->db->select('*');
        $this->db->from('package');
        if ($data['name'] != '')
        {
            $likeCriteria = "(name  LIKE '%" . $data['name'] . "%' or code  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getPackage($id)
    {
        $this->db->select('*');
        $this->db->from('package');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewPackage($data)
    {
        $this->db->trans_start();
        $this->db->insert('package', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editPackage($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('package', $data);
        return TRUE;
    }

    function getTest($id)
    {
        $this->db->select('*');
        $this->db->from('test');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function testListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('test');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }
    
    function addNewPackageHasTest($data)
    {
        $this->db->trans_start();
        $this->db->insert('package_has_test', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getPackageHasTestList($id)
    {
        $this->db->select('pht.*, t.code as test_code, t.name as test_name');
        $this->db->from('package_has_test as pht');
        $this->db->join('test as t','pht.id_test = t.id');
        $this->db->where('pht.id_package', $id);
        $query = $this->db->get();
        return $query->rEsult();
    }

    function getPackageHasTest($id)
    {
        $this->db->select('*');
        $this->db->from('package_has_test');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function editPackageHasTest($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('package_has_test', $data);
        return TRUE;
    }

    function deletePackageHasTest($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('package_has_test');
        return TRUE;
    }
}