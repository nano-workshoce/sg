<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Landscape_course_type_model extends CI_Model
{
    function landscapeCourseTypeList()
    {
        $this->db->select('*');
        $this->db->from('landscape_course_type');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function landscapeCourseTypeListSearch($search)
    {
        $this->db->select('*');
        $this->db->from('landscape_course_type');
        if (!empty($search))
        {
            $likeCriteria = "(name  LIKE '%" . $search . "%' or code  LIKE '%" . $search . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getLandscapeCourseType($id)
    {
        $this->db->select('*');
        $this->db->from('landscape_course_type');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewLandscapeCourseType($data)
    {
        $this->db->trans_start();
        $this->db->insert('landscape_course_type', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editLandscapeCourseType($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('landscape_course_type', $data);
        return TRUE;
    }
}

