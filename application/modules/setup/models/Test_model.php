<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Test_model extends CI_Model
{
    function testList()
    {
        $this->db->select('*');
        $this->db->from('test');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function testListSearch($data)
    {
        $this->db->select('*');
        $this->db->from('test');
        if ($data['name'] != '')
        {
            $likeCriteria = "(name  LIKE '%" . $data['name'] . "%' or code  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getTest($id)
    {
        $this->db->select('*');
        $this->db->from('test');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewTest($data)
    {
        $this->db->trans_start();
        $this->db->insert('test', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editTest($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('test', $data);
        return TRUE;
    }
}

