<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Sample_model extends CI_Model
{
    function sampleList()
    {
        $this->db->select('*');
        $this->db->from('sample');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function sampleListSearch($data)
    {
        $this->db->select('*');
        $this->db->from('sample');
        if ($data['name'] != '')
        {
            $likeCriteria = "(name  LIKE '%" . $data['name'] . "%' or code  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getSample($id)
    {
        $this->db->select('*');
        $this->db->from('sample');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewSample($data)
    {
        $this->db->trans_start();
        $this->db->insert('sample', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editSample($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('sample', $data);
        return TRUE;
    }
}

