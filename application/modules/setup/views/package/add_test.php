<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit Package</h3>
        </div>

        <div class="form-container">
            <h4 class="form-group-title">Package Details</h4>

            <div class="row">


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Code <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" value="<?php echo $package->code; ?>" name="code" id="code" readonly>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" value="<?php echo $package->name; ?>" name="name" id="name" readonly>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Amount <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" value="<?php echo $package->amount; ?>" name="amount" id="amount" readonly>
                    </div>
                </div>



            </div>


            <div class="row">

                <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status *</p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" <?php if($package->status=='1') {
                                 echo "checked=checked";
                              };?>><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0" <?php if($package->status=='0') {
                                 echo "checked=checked";
                              };?>>
                              <span class="check-radio"></span> In-Active
                            </label>                              
                        </div>                         
                </div>
                
            </div>

        </div>


        <div class="button-block clearfix">
            <div class="bttn-group">
                <!-- <button type="submit" class="btn btn-primary btn-lg">Save</button> -->
                <a onclick="updateMasterAmount();" class="btn btn-link">Update Amount</a>
            </div>
        </div>


        <form id="form_main" method="post">


            <div class="form-container">
            <h4 class="form-group-title">Test Details</h4>


                <div class="row">


                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Test <span class='error-text'>*</span></label>
                                <select name="id_test" id="id_test" class="form-control" onchange="getTestAmount(this.value)">
                                    <option value="">Select</option>
                                    <?php
                                    if (!empty($testList))
                                    {
                                        foreach ($testList as $record)
                                        {?>
                                            <option value="<?php echo $record->id;  ?>" >
                                                <?php echo $record->code . " - " . $record->name;  ?>
                                            </option>
                                    <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>



                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Amount <span class='error-text'>*</span></label>
                                <input type="text" class="form-control" name="test_amount" id="test_amount">
                            </div>
                        </div>



                        <div class="col-sm-4">
                            <div class="form-group">
                                <p>Status <span class='error-text'>*</span></p>
                                <label class="radio-inline">
                                  <input type="radio" name="test_status" id="test_status" value="1" checked="checked"><span class="check-radio"></span> Active
                                </label>
                                <label class="radio-inline">
                                  <input type="radio" name="test_status" id="test_status" value="0"><span class="check-radio"></span> Inactive
                                </label>                              
                            </div>                         
                        </div>

                </div>

            </div>


            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="../list" class="btn btn-link">Back</a>
                </div>
            </div>

        
        



        <?php  
        if(!empty($packageHasTestList))
        {
         ?>



        <div class="form-container">
                <h4 class="form-group-title">Test Details</h4> 


            

            <div class="custom-table">
              <table class="table" id="list-table">
                <thead>
                  <tr>
                    <th>Sl. No</th>
                    <th>Test Code</th>
                    <th>Test Name</th>
                    <th>Amount</th>
                    <th>Status</th>
                    <th class="test-center">Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  if (!empty($packageHasTestList))
                  {
                    $i = 1;
                    $total = 0;
                    $total_amount = 0;
                    foreach ($packageHasTestList as $record)
                    {
                  ?>
                      <tr>
                        <td><?php echo $i ?></td>
                        <td><?php echo $record->test_code ?></td>
                        <td><?php echo $record->test_name ?></td>
                        <td><?php echo $record->amount ?></td>
                        <td>
                            <?php if ($record->status == '1')
                            {
                                $total_amount = $total_amount + $record->amount;
                                echo "Active";
                            }
                            else
                            {
                                echo "In-Active";
                            }
                            ?>                                
                        </td>
                        <!-- <td class="text-center"> -->
                        <td>
                        <a onclick="changePackageTestStatus(<?php echo $record->id; ?>)" title="Change Status"> Change </a> | 
                        <a onclick="deletePackageHasTest(<?php echo $record->id; ?>)" title="Delete Test"> Delete </a>
                        </td>
                      </tr>
                  <?php
                  $total = $total + $record->amount;
                  $i++;
                    }
                     $total_amount = number_format($total_amount, 2, '.', ',');
                     $total = number_format($total, 2, '.', ',');
                    ?>

                    <tr >
                        <td bgcolor="" colspan="2"></td>
                        <td bgcolor="" style="text-align: center;"><b>Total Amount :</b></td>
                        <td bgcolor="" ><input type="hidden" name="total_amount" id="total_amount" value="<?php echo $total_amount ?>"><span class="check-radio"> <b><?php echo $total_amount ?> &emsp; &emsp; <?php echo $total ?></b></td>
                        <td bgcolor=""></td>
                      </tr>
                    <?php
                  }
                  ?>
                </tbody>
              </table>
            </div>

        </div>


        <?php  
       
        }
       
        ?>


        </form>







        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>

    $('select').select2();

    function getTestAmount(id)
    {
        $.ajax(
            {
               url: '/setup/package/getTestAmount/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    // $("#view").html(result);
                    // alert(id);
                    // location.reload();
                    $("#test_amount").val(result);
               }
            });
    }


    function changePackageTestStatus(id)
    {
        $.ajax(
            {
               url: '/setup/package/changePackageTestStatus/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    // $("#view").html(result);
                    // alert('Status Updated Sucessfully');
                    location.reload();
                    // updatePackageAmount();
               }
            });
    }


    function deletePackageHasTest(id)
    {
        $.ajax(
            {
               url: '/setup/package/deletePackageHasTest/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    // alert('Test Deleted Sucessfully');
                    location.reload();
                    updatePackageAmount();
               }
            });
    }

    function updatePackageAmount()
    {
        var total_amount = $("#total_amount").val();
        var id_package = <?php echo $package->id; ?>;
        // alert(booking_type);

        if(total_amount != '')
        {          

        var tempPR = {};
        tempPR['amount'] = total_amount;
        tempPR['id_package'] = id_package;

            $.ajax(
            {
               url: '/setup/package/updatePackageAmount',
               type: 'POST',
               data:
               {
                data: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
               }
            });
        }
    }


    function updateMasterAmount()
    {
        var total_amount = $("#total_amount").val();
        var id_package = <?php echo $package->id; ?>;
        // alert(booking_type);

        if(total_amount != '')
        {          

        var tempPR = {};
        tempPR['amount'] = total_amount;
        tempPR['id_package'] = id_package;

            $.ajax(
            {
               url: '/setup/package/updatePackageAmount',
               type: 'POST',
               data:
               {
                data: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                window.location.reload();
               }
            });
        }
    }





    $(document).ready(function()
    {

        $("#form_main").validate({
            rules: {
                id_test: {
                    required: true
                },
                amount: {
                    required: true
                }
            },
            messages: {
                id_test: {
                    required: "<p class='error-text'>Select Test</p>",
                },
                amount: {
                    required: "<p class='error-text'>Amount required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>