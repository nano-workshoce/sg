<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit Department</h3>
        </div>
        <form id="form_department" action="" method="post">

        <div class="form-container">
        <h4 class="form-group-title">Department Details</h4>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Code <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="code" name="code" value="<?php echo $departmentDetails->code;?>">
                    </div>
                </div>
                
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name" value="<?php echo $departmentDetails->name;?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Description <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $departmentDetails->description;?>">
                    </div>
                </div>

                 <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" <?php if($departmentDetails->status=='1') {
                                 echo "checked=checked";
                              };?>><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0" <?php if($departmentDetails->status=='0') {
                                 echo "checked=checked";
                              };?>>
                              <span class="check-radio"></span> In-Active
                            </label>                              
                        </div>                         
                </div>
                
            </div>

        </div>


            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="../list" class="btn btn-link">Back</a>
                </div>
            </div>
        </form>





        <br>



        <div class="form-container">
            <h4 class="form-group-title">Department Has DEAN / HOD Info</h4>          
            <div class="m-auto text-center">
                <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
            </div>
            <div class="clearfix">
                <ul class="nav nav-tabs offers-tab sub-tabs text-center" role="tablist" >
                    <li role="presentation" class="active" ><a href="#invoice" class="nav-link border rounded text-center"
                            aria-controls="invoice" aria-selected="true"
                            role="tab" data-toggle="tab">DEAN / HOD Details</a>
                    </li>                    
                </ul>

                
                <div class="tab-content offers-tab-content">

                    <div role="tabpanel" class="tab-pane active" id="invoice">
                        <div class="col-12 mt-4">




                        <form id="form_detail" action="" method="post">


                            <!-- <div class="form-container">
                                <h4 class="form-group-title">Head Of Department Details</h4> -->
                                <br>

                                <div class="row">

                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label>Role <span class='error-text'>*</span></label>
                                            <select name="role" id="role" class="form-control">
                                                <option value="">Select</option>
                                                <option value="DEAN">DEAN</option>
                                                <option value="HOD">HOD</option>
                                            </select>
                                        </div>
                                    </div>


                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label>Staff *</label>
                                            <select name="id_staff" id="id_staff" class="form-control">
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($staffList))
                                                {
                                                    foreach ($staffList as $record)
                                                    {?>
                                                 <option value="<?php echo $record->id;  ?>">
                                                    <?php echo $record->ic_no ." - ".$record->name;?>
                                                 </option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div> 


                                    <div class="col-sm-3">
                                        <div class="forintake_has_programmem-group">
                                            <label>Effective Date <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control datepicker" id="effective_date" name="effective_date" autocomplete="off">
                                        </div>
                                    </div>
                                
                                 
                                    <div class="col-sm-3">
                                        <button type="button" class="btn btn-primary btn-lg form-row-btn" onclick="saveData()">Add</button>
                                    </div>

                                </div>
<!-- 
                                <div class="row">
                                    <div id="view"></div>
                                </div> -->

                            <!-- </div> -->


                        </form>

                        <br>


                        <?php

                        if(!empty($departmentHasStaff))
                        {
                            ?>

                            <div class="form-container">
                                    <h4 class="form-group-title">DEAN / HOD Details</h4>                    

                              <div class="custom-table">
                                <table class="table">
                                    <thead>
                                        <tr>
                                        <th>Sl. No</th>
                                         <th>Role</th>
                                         <th>Staff</th>
                                         <th>Effective Date</th>
                                         <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                         <?php
                                     $total = 0;
                                      for($i=0;$i<count($departmentHasStaff);$i++)
                                     { ?>
                                        <tr>
                                        <td><?php echo $i+1;?></td>
                                        <td><?php echo $departmentHasStaff[$i]->role;?></td>
                                        <td><?php echo $departmentHasStaff[$i]->ic_no . " - " . $departmentHasStaff[$i]->staff_name;?></td>
                                        <td><?php echo date('d-m-Y', strtotime($departmentHasStaff[$i]->effective_date));?></td>
                                        <td>
                                        <a onclick="deleteStaffDetails(<?php echo $departmentHasStaff[$i]->id; ?>)">Delete</a>
                                        </td>

                                         </tr>
                                      <?php 
                                  } 
                                  ?>
                                    </tbody>
                                </table>
                                </div>

                            </div>

                        <?php
                        
                        }
                         ?>


                        </div> 
                    </div>


                </div>

            </div>
        </div> 







        



        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
    </div>
</div>

<script>

    $('select').select2();

    $(function()
    {
        $(".datepicker").datepicker({
            changeYear: true,
            changeMonth: true
        });
    });

    function saveData()
    {
        if($('#form_detail').valid())
        {

        var tempPR = {};
        tempPR['role'] = $("#role").val();
        tempPR['id_staff'] = $("#id_staff").val();
        tempPR['effective_date'] = $("#effective_date").val();
        tempPR['id_department'] = <?php echo $departmentDetails->id;?>;
            $.ajax(
            {
               url: '/setup/department/addStaffDetails',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                // alert(result);
                window.location.reload();
                // $("#view").html(result);
                // $('#myModal').modal('hide');
               }
            });
        }
    }


    function deleteStaffDetails(id)
    {
        $.ajax(
            {
               url: '/setup/department/deleteStaffDetails/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    // $("#view").html(result);
                    // alert(id);
                    window.location.reload();
               }
            });
    }



     $(document).ready(function(){
        $("#form_department").validate(
        {
            rules:
            {
                name:
                {
                    required: true
                },
                code:
                {
                    required: true
                },
                description:
                {
                    required: true
                }                
            },
            messages:
            {
                name:
                {
                    required: "<p class='error-text'>Name Required</p>",
                },
                code:
                {
                    required: "<p class='error-text'>Code Required</p>",
                },
                description:
                {
                    required: "<p class='error-text'>Description Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


      $(document).ready(function(){
        $("#form_detail").validate(
        {
            rules:
            {
                role:
                {
                    required: true
                },
                effective_date:
                {
                    required: true
                },
                id_staff:
                {
                    required: true
                }             
            },
            messages:
            {
                role:
                {
                    required: "<p class='error-text'>Select Role</p>",
                },
                effective_date:
                {
                    required: "<p class='error-text'>Select Effective Date</p>",
                },
                id_staff:
                {
                    required: "<p class='error-text'>Select Staff</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>