<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <!-- <div class="page-title clearfix">
            <h3>Welcome : Module SETUP</h3>
        </div> -->
        
        <div class="row">
            <div class="col-sm-4">
                <div class="stats-col">
                    <div class="text">
                        <img src="<?php echo BASE_PATH; ?>assets/img/pending_approval_icon.svg" />Today Total Collections
                    </div>
                    <div class="count">
                        03
                    </div>
                    <div class="text-center">
                        <a href="#">More Info</a>
                    </div>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="stats-col pending-acceptance">
                    <div class="text">
                        <img src="<?php echo BASE_PATH; ?>assets/img/pending_acceptance_icon.svg" />Total Completed Collections
                    </div>
                    <div class="count">
                        100
                    </div>
                    <div class="text-center">
                        <a href="#">More Info</a>
                    </div>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="stats-col active-students">
                    <div class="text">
                        <img src="<?php echo BASE_PATH; ?>assets/img/active_students_icon.svg" />Total Patients
                    </div>
                    <div class="count">
                        295
                    </div>
                    <div class="text-center">
                        <a href="#">More Info</a>
                    </div>
                </div>
            </div>

            <!-- <div class="col-sm-4">
                <div class="stats-col inactive-students">
                    <div class="text">
                        <img src="<?php echo BASE_PATH; ?>assets/img/inactive_students_icon.svg" />Inactive Patients
                    </div>
                    <div class="count">
                        734
                    </div>
                    <div class="text-center">
                        <a href="#">More Info</a>
                    </div>
                </div>
            </div> -->

        </div>

        <hr/>



        <div class="row">            
            <div class="col-sm-3 col-lg-4">
                <a href="/setup/welcome" class="dashboard-menu"><span class="icon"></span>System Setup</a>
            </div>
             
            <div class="col-sm-3 col-lg-4">
                <a href="/pm/welcome" class="dashboard-menu partners-management"><span class="icon"></span>Patient Management</a>
            </div>

            <div class="col-sm-3 col-lg-4">
                <a href="/cm/welcome" class="dashboard-menu curriculm-management"><span class="icon"></span>Records Management</a>
            </div>                                                                 
        </div>

        <footer class="footer-wrapper">
            <p>&copy; 2020 All rights, reserved</p>
        </footer>

    </div>
</div>