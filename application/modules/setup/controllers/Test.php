<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Test extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('test_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('test.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $data['searchParam'] = $formData;
            
            $data['testList'] = $this->test_model->testListSearch($formData);
            
            // echo "<Pre>"; print_r($data);exit;

            $this->global['pageTitle'] = 'Campus Management System : Test List';
            $this->loadViews("test/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('test.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $amount = $this->security->xss_clean($this->input->post('amount'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'amount' => $amount,
                    'status' => $status
                );
                //echo "<Pre>"; print_r($subjectDetails);exit;

                $result = $this->test_model->addNewTest($data);
                redirect('/setup/test/list');
            }
            $this->global['pageTitle'] = 'Campus Management System : Add Test';
            $this->loadViews("test/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('test.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/setup/test/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $amount = $this->security->xss_clean($this->input->post('amount'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'amount' => $amount,
                    'status' => $status
                );

                $result = $this->test_model->editTest($data,$id);
                redirect('/setup/test/list');
            }
            $data['test'] = $this->test_model->getTest($id);

            $this->global['pageTitle'] = 'Campus Management System : Edit Test';
            $this->loadViews("test/edit", $this->global, $data, NULL);
        }
    }
}
