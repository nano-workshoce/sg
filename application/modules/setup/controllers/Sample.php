<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Sample extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('sample_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('sample.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $data['searchParam'] = $formData;
            
            $data['sampleList'] = $this->sample_model->sampleListSearch($formData);

            $this->global['pageTitle'] = 'Campus Management System : Sample List';
            $this->loadViews("sample/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('sample.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'description' => $description,
                    'status' => $status
                );
                //echo "<Pre>"; print_r($subjectDetails);exit;

                $result = $this->sample_model->addNewSample($data);
                redirect('/setup/sample/list');
            }
            $this->global['pageTitle'] = 'Campus Management System : Add Sample';
            $this->loadViews("sample/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('sample.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/setup/sample/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'description' => $description,
                    'status' => $status
                );

                $result = $this->sample_model->editSample($data,$id);
                redirect('/setup/sample/list');
            }
            $data['sample'] = $this->sample_model->getSample($id);
            
            $this->global['pageTitle'] = 'Campus Management System : Edit Sample';
            $this->loadViews("sample/edit", $this->global, $data, NULL);
        }
    }
}