<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Package extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('package_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('package.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $data['searchParam'] = $formData;
            
            $data['packageList'] = $this->package_model->packageListSearch($formData);
            
            // echo "<Pre>"; print_r($data);exit;

            $this->global['pageTitle'] = 'Campus Management System : Package List';
            $this->loadViews("package/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('package.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $amount = $this->security->xss_clean($this->input->post('amount'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'amount' => $amount,
                    'status' => $status
                );
                //echo "<Pre>"; print_r($subjectDetails);exit;

                $result = $this->package_model->addNewPackage($data);
                redirect('/setup/package/edit/'.$result);
            }
            $this->global['pageTitle'] = 'Campus Management System : Add Package';
            $this->loadViews("package/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('package.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/setup/package/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $amount = $this->security->xss_clean($this->input->post('amount'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'amount' => $amount,
                    'status' => $status
                );

                $result = $this->package_model->editPackage($data,$id);
                redirect('/setup/package/list');
            }
            $data['package'] = $this->package_model->getPackage($id);

            $this->global['pageTitle'] = 'Campus Management System : Edit Package';
            $this->loadViews("package/edit", $this->global, $data, NULL);
        }
    }


    function addTest($id = NULL)
    {
        if ($this->checkAccess('package.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/setup/package/list');
            }
            if($this->input->post())
            {
                // echo "<Pre>"; print_r($this->input->post());exit;

                $id_test = $this->security->xss_clean($this->input->post('id_test'));
                $test_amount = $this->security->xss_clean($this->input->post('test_amount'));
                $total_amount = $this->security->xss_clean($this->input->post('total_amount'));
                $test_status = $this->security->xss_clean($this->input->post('test_status'));

                if($test_amount > 0)
                {
                    $total = $test_amount + $total_amount;
                    $update['amount'] = $total;
                    $result = $this->package_model->editPackage($update,$id);
                }
            
                $data = array(
                    'id_package' => $id,
                    'id_test' => $id_test,
                    'amount' => $test_amount,
                    'status' => $test_status
                );

                $result = $this->package_model->addNewPackageHasTest($data);
                redirect($_SERVER['HTTP_REFERER']);
            }

            $data['testList'] = $this->package_model->testListByStatus('1');
            
            $data['package'] = $this->package_model->getPackage($id);
            $data['packageHasTestList'] = $this->package_model->getPackageHasTestList($id);

            $this->global['pageTitle'] = 'Campus Management System : Add Test Package';
            $this->loadViews("package/add_test", $this->global, $data, NULL);
        }
    }

    function getTestAmount($id_test)
    {
            $test = $this->package_model->getTest($id_test);
            // echo "<Pre>"; print_r($results);exit;
            $amount = 0;
            if($test)
            {
                $amount = $test->amount;
            }
            echo $amount;
            exit;
    }

    function changePackageTestStatus($id)
    {
        $test = $this->package_model->getPackageHasTest($id);
        if($test)
        {
            $status = $test->status;
            if($status == 0)
            {
                $new_status = 1;
            }
            elseif($status == 1)
            {
                $new_status = 0;
            }

            $data['status'] = $new_status;

            $updated = $this->package_model->editPackageHasTest($data,$id);

            echo '1';exit;
        }
    }


    function deletePackageHasTest($id)
    {
        $test = $this->package_model->deletePackageHasTest($id);
        echo '1';exit;
    }

    function updatePackageAmount()
    {
        $tempData = $this->security->xss_clean($this->input->post('data'));
        $id_package = $tempData['id_package'];
        unset($tempData['id_package']);
        $result = $this->package_model->editPackage($tempData,$id_package);
        if($result)
        {
            echo 'Updated';exit;
        }
    }
}