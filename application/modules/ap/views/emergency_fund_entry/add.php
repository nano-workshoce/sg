<?php $this->load->helper("form"); ?>
<form id="form_pr_entry" action="" method="post">

<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
            <div class="page-title clearfix">
                <h3>Add Emergency Fund Entry</h3>
            </div>

        <div class="form-container">
            <h4 class="form-group-title">Emergency Fund Entry Details</h4>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Emergency Fund Type <span class='error-text'>*</span></label>
                        <select name='type' id='type' class='form-control' onchange="getEFADataByType()">
                            <option value=''>Select</option>
                            <option value='Staff'>Staff</option>
                            <!-- <option value='Student'>Student</option> -->
                        </select>
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Financial Year <span class='error-text'>*</span></label>
                        <select name="id_financial_year" id="id_financial_year" class="form-control" onchange="getEFADataByType()">
                            <option value="">Select</option>
                            <?php
                            if (!empty($financialYearList))
                            {
                                foreach ($financialYearList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>



                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Budget Year <span class='error-text'>*</span></label>
                        <select name="id_budget_year" id="id_budget_year" class="form-control" onchange="getEFADataByType()">
                            <option value="">Select</option>
                            <?php
                            if (!empty($budgetYearList))
                            {
                                foreach ($budgetYearList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

            </div>



            <div class="row">



                <div id="view_efa_dropdown">
                </div>




                <div id="view_efa_allocation_dropdown">
                </div>
               
            </div>




            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Fund Code <span class='error-text'>*</span></label>
                        <select name="fund_code" id="fund_code" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($fundCodeList))
                            {
                                foreach ($fundCodeList as $record)
                                {?>
                             <option value="<?php echo $record->code;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Department Code <span class='error-text'>*</span></label>
                        <select name="department_code" id="department_code" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($departmentCodeList))
                            {
                                foreach ($departmentCodeList as $record)
                                {?>
                             <option value="<?php echo $record->code;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Activity Code <span class='error-text'>*</span></label>
                        <select name="activity_code" id="activity_code" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($activityCodeList))
                            {
                                foreach ($activityCodeList as $record)
                                {?>
                             <option value="<?php echo $record->code;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

            </div>


            <div class="row">


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Account Code <span class='error-text'>*</span></label>
                        <select name="account_code" id="account_code" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($accountCodeList))
                            {
                                foreach ($accountCodeList as $record)
                                {?>
                             <option value="<?php echo $record->code;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>



                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Requested Amount <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="requested_amount" name="requested_amount">
                    </div>
                </div>


                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Description  <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description">
                    </div>
                </div>



               <!--  <div class="col-sm-4">
                    <div class="form-group">
                        <label>Transaction Type <span class='error-text'>*</span></label>
                        <select name='transation_type' id='transation_type' class='form-control'>
                            <option value=''>Select</option>
                            <option value='Cash'>Cash</option>
                            <option value='Checq'>Checq</option>
                            <option value='DD'>DD</option>
                        </select>
                    </div>
                </div> -->


            </div>


        </div>



        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="list" class="btn btn-link">Cancel</a>
            </div>
        </div>

           
        
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>

</form>
<script>


    function getEFADataByType()
    {

        var tempPR = {};
        tempPR['type'] = $("#type").val();
        tempPR['id_budget_year'] = $("#id_budget_year").val();
        tempPR['id_financial_year'] = $("#id_financial_year").val();
        if(tempPR['type'] != '' && tempPR['id_budget_year'] != '' && tempPR['id_financial_year'] != '' )
        {
            $.ajax(
            {
               url: '/ap/emergencyFundEntry/getEFADataByType',
               type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view_efa_dropdown").html(result);
               }
            });
        }
    }

    function getAllocationData()
    {
        var tempPR = {};
        tempPR['type'] = $("#type").val();
        tempPR['id_staff'] = $("#id_staff").val();
        tempPR['id_student'] = $("#id_student").val();
        tempPR['id_financial_year'] = $("#id_financial_year").val();
        tempPR['id_budget_year'] = $("#id_budget_year").val();



        if(tempPR['type'] != '' && tempPR['id_financial_year'] != '' && tempPR['id_budget_year'] != '')
        {
            $.ajax(
            {
               url: '/ap/emergencyFundEntry/getAllocationData',
               type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                if(result == '')
                {
                    alert('No Emergency Fund Allocated For Entered Data');
                }
                else
                {
                    $("#view_efa_allocation_dropdown").html(result);
                }
               }
            });
        }
    }



    $(document).ready(function() {
        $("#form_pr_entry").validate({
            rules: {
                type: {
                    required: true
                },
                 id_financial_year: {
                    required: true
                }
                ,
                 id_budget_year: {
                    required: true
                },
                 fund_code: {
                    required: true
                },
                 department_code: {
                    required: true
                },
                 activity_code: {
                    required: true
                },
                 account_code: {
                    required: true
                },
                 requested_amount: {
                    required: true
                },
                transation_type: 
                {
                    required: true
                },
                description: 
                {
                    required: true
                }
            },
            messages: {
                type: {
                    required: "<p class='error-text'>Select Type</p>",
                },
                id_financial_year: {
                    required: "<p class='error-text'>Select Financial Year</p>",
                },
                id_budget_year: {
                    required: "<p class='error-text'>Select Budget Year</p>",
                },
                fund_code: {
                    required: "<p class='error-text'>Select Fund Year</p>",
                },
                department_code: {
                    required: "<p class='error-text'>Select Department Code</p>",
                },
                activity_code: {
                    required: "<p class='error-text'>Select Activity Code</p>",
                },
                account_code: {
                    required: "<p class='error-text'>Select Account Code</p>",
                },
                requested_amount: {
                    required: "<p class='error-text'>Requested Amount Required</p>",
                },
                transation_type: {
                    required: "<p class='error-text'>Select Transaction Type</p>",
                },
                description: {
                    required: "<p class='error-text'>Description Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
<script type="text/javascript">
    $('select').select2();
</script>
<script>
  $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
    });
  } );
</script>