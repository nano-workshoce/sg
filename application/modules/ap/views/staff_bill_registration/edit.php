<?php $this->load->helper("form"); ?>
<form id="form_pr_entry" action="" method="post">
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
            <div class="page-title clearfix">
                <h3>View Staff / General Bill Registration</h3>
            </div>

     <div class="form-container">
        <h4 class="form-group-title">Staff / General Bill Registration Entry</h4>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Bill Type <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $billRegistration->type;?>" readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Financial Year <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="amount" name="amount" value="<?php echo $billRegistration->financial_year;?>" readonly="readonly">
                    </div>
                </div>

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Budget Year <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="amount" name="amount" value="<?php echo $billRegistration->budget_year;?>" readonly="readonly">
                    </div>
                </div>

            </div>



            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Department Code <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $billRegistration->department_code . " - " . $billRegistration->department_name;?>" readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Bill Registered Date Time <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $billRegistration->invoice_date;?>" readonly="readonly">
                    </div>
                </div>


                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Total Amount <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $billRegistration->total_amount;?>" readonly="readonly">
                    </div>
                </div>

            </div>



            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Description <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $billRegistration->description;?>" readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Bank Account Number <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="bank_acc_no" name="bank_acc_no" value="<?php echo $billRegistration->bank_acc_no;?>" readonly="readonly">
                    </div>
                </div>


                <?php
                if($billRegistration->type == 'Staff Claim')
                {
                  ?>

                  <div class="col-sm-4">
                    <div class="form-group">
                      <label>Staff <span class='error-text'>*</span></label>
                      <select name="id_staff" id="id_staff" class="form-control" disabled="true">
                          <option value="">Select</option>
                          
                           <?php
                          if (!empty($staffList)) {
                            foreach ($staffList as $record)
                            {
                              $selected = '';
                              if ($record->id == $billRegistration->id_staff) {
                                $selected = 'selected';
                              }
                          ?>
                              <option value="<?php echo $record->id;  ?>"
                                <?php echo $selected;  ?>>
                                <?php echo  $record->ic_no . " - " . $record->salutation . ". " . $record->name;  ?>
                                </option>
                          <?php
                            }
                          }
                          ?>

                      </select>
                  </div>
                </div>




                  <?php
                }
                elseif ($billRegistration->type == 'General Claim')
                {
                  ?>

                  <div class="col-sm-4">
                    <div class="form-group">
                        <label>Customer Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $billRegistration->customer_name;?>" readonly="readonly">
                    </div>
                </div>


                  <?php
                }

                ?>

            </div>



             <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Status <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="date_time" name="date_time" value="<?php
                        if($billRegistration->status == '0')
                        {
                            echo "Pending";
                        }
                        elseif($billRegistration->status == '1')
                        {
                            echo "Approved";
                        }
                        elseif($billRegistration->status == '2')
                        {
                            echo "Rejected";
                        }
                         ?>" readonly="readonly">
                    </div>
                </div>
            


             <?php
            if($billRegistration->status == '2')
            {
             ?>


                <div class="col-sm-4" id="view_reject">
                    <div class="form-group">
                        <label>Reject Reason <span class='error-text'>*</span></label>
                        <input type="text" id="reason" name="reason" class="form-control" value="<?php echo $billRegistration->reason; ?>" readonly>
                    </div>
                </div>

            <?php
            }
            ?>


            </div>


        </div>










            <br>
            <h3>Bill Claim Details</h3>

         <div class="form-container">
        <h4 class="form-group-title">Staff / General Bill Registration Details</h4>



            <div class="custom-table">
        <table class="table">
            <thead>
                 <tr>
                    <th>Sl. No</th>
                    <th>Debit GL Code</th>
                    <th>Credit GL Code</th>
                    <th>Category</th>
                    <th>Sub-Category</th>
                    <th>Item</th>
                    <th>Tax</th>
                    <th>Quantity</th>
                    <th>Tax Price</th>
                    <th>Amount</th>
                    <th>Total Amount</th>
                 </tr>
            </thead>
            <tbody>
                 <?php 
                  $total = 0;
                 for($i=0;$i<count($billRegistrationDetails);$i++)
                    { 
                    // echo "<Pre>";print_r($poDetails[$i]);exit();

                        ?>
                   <tr>
                    <td><?php echo $i+1; ?></td>
                    <td><?php echo $billRegistrationDetails[$i]->dt_fund . " - " . $billRegistrationDetails[$i]->dt_department . " - " . $billRegistrationDetails[$i]->dt_activity  . " - " . $billRegistrationDetails[$i]->dt_account;?></td>
                    <td><?php echo $billRegistrationDetails[$i]->cr_fund . " - " . $billRegistrationDetails[$i]->cr_department . " - " . $billRegistrationDetails[$i]->cr_activity  . " - " . $billRegistrationDetails[$i]->cr_account;?></td>
                    <td><?php echo $billRegistrationDetails[$i]->category_code . " - ". $billRegistrationDetails[$i]->category_name ;?></td>
                    <td><?php echo $billRegistrationDetails[$i]->sub_category_code . " - ". $billRegistrationDetails[$i]->sub_category_name ;?></td>
                    <td><?php echo $billRegistrationDetails[$i]->item_code . " - ". $billRegistrationDetails[$i]->item_name ;?></td>
                    <td><?php echo $billRegistrationDetails[$i]->tax_code . " - ". $billRegistrationDetails[$i]->tax_percentage . " %" ;?></td>
                    <td><?php echo $billRegistrationDetails[$i]->quantity;?></td>
                    <td><?php echo $billRegistrationDetails[$i]->tax_price;?></td>
                    <td><?php echo $billRegistrationDetails[$i]->price;?></td>
                    <td><?php echo $billRegistrationDetails[$i]->total_final;?></td>
                     </tr>
                  <?php 
                  // $total = $total + $poDetails[$i]->total_final;
                }
                // $total = number_format($total, 2, '.', ',');
                ?>

                <!-- <tr>
                    <td bgcolor="" colspan="9"></td>
                    <td bgcolor=""><b> Total : </b></td>
                    <td bgcolor=""><b><?php echo $total; ?></b></td>
                </tr> -->

                    </tbody>
                </table>
                </div>

            </div>

            <div class="button-block clearfix">
                <div class="bttn-group">
                    <!-- <button type="submit" class="btn btn-primary btn-lg">Save</button> -->
                    <a href="../list" class="btn btn-link">Back</a>
                </div>
            </div>
        
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>