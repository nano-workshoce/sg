<?php $this->load->helper("form"); ?>
<form id="form_master" action="" method="post">

<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
            <div class="page-title clearfix">
                <h3>Add Staff / General Bill Registration</h3>
            </div>

        <div class="form-container">
        <h4 class="form-group-title">Staff / General Bill Registration Entry</h4>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Type <span class='error-text'>*</span></label>
                        <select name='type' id='type' class='form-control' onchange="selectListShow()">
                            <option value=''>Select</option>
                            <option value='General Claim'>General Claim</option>
                            <option value='Staff Claim'>Staff Claim</option>
                        </select>
                    </div>
                </div>

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Financial Year <span class='error-text'>*</span></label>
                        <select name="id_financial_year" id="id_financial_year" class="form-control" onchange="getDebitBudget()">
                            <option value="">Select</option>
                            <?php
                            if (!empty($financialYearList))
                            {
                                foreach ($financialYearList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>    


                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Budget Year <span class='error-text'>*</span></label>
                        <select name="id_budget_year" id="id_budget_year" class="form-control" onchange="getDebitBudget()">
                            <option value="">Select</option>
                            <?php
                            if (!empty($budgetYearList))
                            {
                                foreach ($budgetYearList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>  

            </div>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Department Code<span class='error-text'>*</span></label>
                        <select name="department_code" id="department_code" class="form-control" onchange="getDebitBudget()">
                            <option value="">Select</option>
                            <?php
                            if (!empty($departmentCodeList))
                            {
                                foreach ($departmentCodeList as $record)
                                {?>
                             <option value="<?php echo $record->code;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>  



                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Bill Registration Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="date_time" name="date_time" value="<?php echo date('d-m-Y'); ?>" readonly="readonly">
                    </div>
                </div>


                

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Total Amount <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="amount" name="amount" readonly="readonly" value="0">
                    </div>
               </div>
            
            </div>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Description <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description">
                    </div>
                </div>      


                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Bank Account Number <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="bank_acc_no" name="bank_acc_no">
                    </div>
                </div> 



                <div class="col-sm-4" id="view_general_claim" style="display: none">
                    <div class="form-group">
                        <label>General Customer Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="customer_name" name="customer_name">
                    </div>
                </div>


                 <div class="col-sm-4" id="view_staff_claim_dropdown" style="display: none" >
                    <div class="form-group">
                        <label>Staff <span class='error-text'>*</span></label>
                        <select name="id_staff" id="id_staff" class="form-control" style="width: 360px">
                            <option value="">Select</option>
                            <?php
                            if (!empty($staffList))
                            {
                                foreach ($staffList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->ic_no . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

            </div>

        </div>




        </form>

        

            <h3>Claim Details</h3><button type="button" class="btn btn-info btn-lg" onclick="opendialog()">Add</button>
            <br>
            <br>

            <div class="row">
                <div id="view"></div>
            </div>



            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="button" class="btn btn-primary btn-lg" onclick="validateDetailsData()">Save</button>
                    <a href="list" class="btn btn-link">Cancel</a>
                </div>
            </div>
        
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>


<form id="form_master_details" action="" method="post">

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">PR Details</h4>
      </div>
      <div class="modal-body">
         <h4></h4>


             <div class="row">

                <input type="hidden" class="form-control" id="id" name="id">
                


                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Procurement Category<span class='error-text'>*</span></label>
                        <select name="id_category" id="id_category" class="form-control" onchange="getSubCategoryByCategoryId()" style="width: 196px">
                            <option value="">Select</option>
                            <?php
                            if (!empty($prCategoryList))
                            {
                                foreach ($prCategoryList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->code . " - " . $record->description;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>  


                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Sub Category <span class='error-text'>*</span></label>
                        <span id='view_sub_category'></span>
                    </div>
                </div>  

                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Item <span class='error-text'>*</span></label>
                        <span id='view_item'></span>
                    </div>
                </div>  

            </div>


            <h4>Debit Budget GL</h4>

             <div class="row">   


                <div class="col-sm-3" id="view_debit_budget_fund_span" style="display: none">
                    <div class="form-group">
                        <label>Dt Fund Code <span class='error-text'>*</span></label>
                        <span id="view_debit_budget_fund"></span>
                    </div>
                </div>

                 <div class="col-sm-3">
                    <div class="form-group">
                        <label>Dt Department Code <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="dt_department" name="dt_department" readonly>
                    </div>
                </div>

                <div class="col-sm-3" id="view_debit_budget_activity_span" style="display: none">
                    <div class="form-group">
                        <label>Dt Activity Code <span class='error-text'>*</span></label>
                        <span id="view_debit_budget_activity"></span>
                    </div>
                </div>

                <div class="col-sm-3" id="view_debit_budget_account_span" style="display: none">
                    <div class="form-group">
                        <label>Dt Account Code <span class='error-text'>*</span></label>
                        <span id="view_debit_budget_account"></span>
                    </div>
                </div>
            </div>


            <div class="row">   
                <div class="col-sm-3" id="view_debit_span" style="display: none">
                    <div class="form-group">
                        <label>GL Debit Balance Amount <span class='error-text'>*</span></label>
                        <span id="view_debit_amount"></span>
                    </div>
            </div>


        </div>





            <h4>Credit Budget GL</h4>


            <div class="row">


                
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Cr Fund <span class='error-text'>*</span></label>
                        <select name="cr_fund" id="cr_fund" class="form-control" style="width: 196px;">
                            <option value="">Select</option>
                            <?php
                            if (!empty($fundCodeList))
                            {
                                foreach ($fundCodeList as $record)
                                {?>
                             <option value="<?php echo $record->code;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>   
                

                
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Cr Department <span class='error-text'>*</span></label>
                        <select name="cr_department" id="cr_department" class="form-control" style="width: 196px;">
                            <option value="">Select</option>
                            <?php
                            if (!empty($departmentCodeList))
                            {
                                foreach ($departmentCodeList as $record)
                                {?>
                             <option value="<?php echo $record->code;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div> 



                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Cr Activity <span class='error-text'>*</span></label>
                        <select name="cr_activity" id="cr_activity" class="form-control" style="width: 196px;">
                            <option value="">Select</option>
                            <?php
                            if (!empty($activityCodeList))
                            {
                                foreach ($activityCodeList as $record)
                                {?>
                             <option value="<?php echo $record->code;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>  

                 <div class="col-sm-3">
                    <div class="form-group">
                        <label>Cr Account <span class='error-text'>*</span></label>
                        <select name="cr_account" id="cr_account" class="form-control" style="width: 196px;">
                            <option value="">Select</option>
                            <?php
                            if (!empty($accountCodeList))
                            {
                                foreach ($accountCodeList as $record)
                                {?>
                             <option value="<?php echo $record->code;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>  


            </div>

           

            <div class="row">

                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Quantity <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="quantity" name="quantity">
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Price <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="price" name="price">
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Tax Code <span class='error-text'>*</span></label>
                        <select name="id_tax" id="id_tax" class="form-control" style="width: 196px;" onchange="getTaxCalculated()">
                            <option value="">Select</option>
                            <?php
                            if (!empty($taxList))
                            {
                                foreach ($taxList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->code . " - " . $record->name . " - " . $record->percentage . " %";?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>  


                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Tax Amount <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="tax_price" name="tax_price" readonly="readonly">
                    </div>
               </div>
                
            </div>
            <div class="row">

                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Tax Percentage(%) <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="tax_percentage" name="tax_percentage" readonly="readonly">
                    </div>
               </div>


                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Total Amount <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="total_final" name="total_final" readonly="readonly">
                    </div>
               </div>

               <div class="col-sm-3">
                    <div class="form-group">
                        <span id='view_dummy' style="display: none;"></span>
                        <!-- <span id='view_dummy'></span> -->
                    </div>
                </div>  



            </div>


      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" onclick="saveData()">Add</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

</form>
<script>

    function opendialog()
    {
         if($('#form_master').valid())
        {
            $('#myModal').modal('show');
        }
    }

    function selectListShow()
    {
        var type = $("#type").val();
        if(type == 'Staff Claim')
        {
            $("#view_staff_claim_dropdown").show();
            $("#view_general_claim").hide();

        }
        else if(type == 'General Claim')
        {
            $("#view_general_claim").show();
            $("#view_staff_claim_dropdown").hide();
        }


    }

    function getDebitBudget()
    {
        var id_dt_financial_year = $("#id_financial_year").val();
        var id_dt_budget_year = $("#id_budget_year").val();
        var id_dt_department = $("#department_code").val();

        if(id_dt_budget_year != '' && id_dt_department != '' && id_dt_financial_year != '')
        {

        var tempPR = {};
        tempPR['id_dt_financial_year'] = id_dt_financial_year;
        tempPR['id_dt_budget_year'] = id_dt_budget_year;
        tempPR['id_dt_department'] = id_dt_department;

            $.ajax(
            {
               url: '/procurement/prEntry/getDebitBudget',
               type: 'POST',
               data:
               {
                data: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                // alert(result);
                if(result == '')
                {
                    alert('No Debit Budget GL Found For Entered Data')
                }
                else
                {
                    // alert(result);
                    $("#view_debit_budget_fund").html(result);
                    $("#view_debit_budget_fund_span").show();

                    $("#dt_department").val(id_dt_department);

                } 
               }
            });
        }
    }

    function getActivityDebitCodes()
    {
        var id_dt_financial_year = $("#id_financial_year").val();
        var id_dt_budget_year = $("#id_budget_year").val();
        var id_dt_department = $("#department_code").val();
        var dt_fund_code = $("#dt_fund").val();

        if(id_dt_budget_year != '' && id_dt_department != '' && id_dt_financial_year != '' && dt_fund_code != '')
        {

        var tempPR = {};
        tempPR['id_dt_financial_year'] = id_dt_financial_year;
        tempPR['id_dt_budget_year'] = id_dt_budget_year;
        tempPR['id_dt_department'] = id_dt_department;
        tempPR['dt_fund_code'] = dt_fund_code;

            $.ajax(
            {
               url: '/procurement/prEntry/getActivityDebitCodes',
               type: 'POST',
               data:
               {
                data: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                if(result == '')
                {
                    alert('No Debit Budget GL Found For Entered Data')
                }
                else
                {
                    
                    $("#view_debit_budget_activity").html(result);
                    $("#view_debit_budget_activity_span").show();
                }
               }
            });
        }
    }


    function getAccountDebitCodes()
    {
        var id_dt_financial_year = $("#id_financial_year").val();
        var id_dt_budget_year = $("#id_budget_year").val();
        var id_dt_department = $("#department_code").val();
        var dt_fund_code = $("#dt_fund").val();
        var dt_activity_code = $("#dt_activity").val();

        if(id_dt_budget_year != '' && id_dt_department != '' && id_dt_financial_year != '' && dt_fund_code != '' && dt_activity_code != '')
        {

        var tempPR = {};
        tempPR['id_dt_financial_year'] = id_dt_financial_year;
        tempPR['id_dt_budget_year'] = id_dt_budget_year;
        tempPR['id_dt_department'] = id_dt_department;
        tempPR['dt_fund_code'] = dt_fund_code;
        tempPR['dt_activity_code'] = dt_activity_code;

            $.ajax(
            {
               url: '/procurement/prEntry/getAccountDebitCodes',
               type: 'POST',
               data:
               {
                data: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                if(result == '')
                {
                    alert('No Debit Budget GL Found For Entered Data')
                }
                else
                {
                    $("#view_debit_budget_account").html(result);
                    $("#view_debit_budget_account_span").show();
                }
               }
            });
        }
    }

    function getBudgetAllocationByCodes()
    {
        // var id_dt_financial_year = $("#id_financial_year").val();
        // var id_dt_budget_year = $("#id_budget_year").val();
        // var id_dt_department = $("#department_code").val();
        // var dt_fund_code = $("#dt_fund").val();
        // var dt_activity_code = $("#dt_activity").val();
        // var dt_account_code = $("#dt_account").val();

        // if(id_dt_budget_year != '' && id_dt_department != '' && id_dt_financial_year != '' && dt_fund_code != '' && dt_activity_code != '' && dt_account_code != '')
        // {

        // var tempPR = {};
        // tempPR['id_dt_financial_year'] = id_dt_financial_year;
        // tempPR['id_dt_budget_year'] = id_dt_budget_year;
        // tempPR['id_dt_department'] = id_dt_department;
        // tempPR['dt_fund_code'] = dt_fund_code;
        // tempPR['dt_activity_code'] = dt_activity_code;
        // tempPR['dt_account_code'] = dt_account_code;

        //     $.ajax(
        //     {
        //        url: '/procurement/prEntry/getBudgetAllocationByCodes',
        //        type: 'POST',
        //        data:
        //        {
        //         data: tempPR
        //        },
        //        error: function()
        //        {
        //         alert('Something is wrong');
        //        },
        //        success: function(result)
        //        {
        //         if(result == '')
        //         {
        //             alert('No Debit Budget GL Found For Entered Data')
        //         }
        //         else
        //         {
        //             $("#view_debit_amount").html(result);
        //             $("#view_debit_span").show();
        //         }
        //        }
        //     });
        // }
    }



    function saveData()
    {
        if($('#form_master_details').valid())
        {
            $('#myModal').modal('hide');
            document.getElementById("type").disabled = false;

            var tempPR = {};
            tempPR['dt_fund'] = $("#dt_fund").val();
            tempPR['dt_department'] = $("#dt_department").val();
            tempPR['dt_activity'] = $("#dt_activity").val();
            tempPR['dt_account'] = $("#dt_account").val();
            tempPR['cr_fund'] = $("#cr_fund").val();
            tempPR['cr_department'] = $("#cr_department").val();
            tempPR['cr_activity'] = $("#cr_activity").val();
            tempPR['cr_account'] = $("#cr_account").val();
            tempPR['type'] = $("#type").val();
            tempPR['id_category'] = $("#id_category").val();
            tempPR['id_sub_category'] = $("#id_sub_category").val();
            tempPR['id_item'] = $("#id_item").val();
            tempPR['quantity'] = $("#quantity").val();
            tempPR['price'] = $("#price").val();
            tempPR['id_tax'] = $("#id_tax").val();
            tempPR['tax_price'] = $("#tax_price").val();
            tempPR['total_final'] = $("#total_final").val();
            tempPR['id_budget_allocation'] = $("#id_budget_allocation").val();
        
            $.ajax(
            {
               url: '/ap/staffBillRegistration/tempadd',
               type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                document.getElementById("type").disabled = true;

                // $('#myModal').modal('hide');
                $("#view").html(result);
                var ta = $("#total_detail").val();
                // alert(ta);
                $("#amount").val(ta);
                if($result== '')
                {
                    $("#amount").val('0');
                }
                if(ta == '')
                {
                    $("#amount").val('0');
                }
               }
            });
        }
    }



    function getSubCategoryByCategoryId()
    {
        var tempPR = {};
        tempPR['type'] = 'Procurement';
        tempPR['id_category'] = $("#id_category").val();
        $.ajax(
            {
               url: '/procurement/prEntry/getSubCategoryByCategoryId',
               type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view_sub_category").html(result);
                $("#view_item").html('');
                // var ta = $("#total_detail").val();
                // // alert(ta);
                // $("#amount").val(ta);
               }
            });
    }


    function getItemBySubCategoryId()
    {
        var tempPR = {};
        tempPR['type'] = 'Procurement';
        tempPR['id_category'] = $("#id_category").val();
        tempPR['id_sub_category'] = $("#id_sub_category").val();
        $.ajax(
            {
               url: '/procurement/prEntry/getItemBySubCategoryId',
               type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view_item").html(result);
               }
            });
    }


    function deleteTempData(id)
    {

        // document.getElementById("type").disabled = false;
        // var type = $("#type").val();

         $.ajax(
            {
               url: '/ap/staffBillRegistration/tempDelete/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    $("#view").html(result);
                    var ta = $("#total_detail").val();
                    // alert(ta);
                    if(ta == '')
                    {
                        // document.getElementById("type").disabled = false;
                        $("#amount").val('0');
                    }
                    else
                    {
                        // document.getElementById("type").disabled = true;
                        $("#amount").val(ta);
                    }
                    // if($result== '')
                    // {
                    // }
               }
            });
    }


    function getTaxCalculated()
    {
        var tempPR = {};
        tempPR['id_tax'] = $("#id_tax").val();
        tempPR['quantity'] = $("#quantity").val();
        tempPR['price'] = $("#price").val();

                // alert($tempPR['id_tax']);
        // if ($tempPR['id_tax'] != '' && $tempPR['quantity'] != '' && $tempPR['price'] != '')
        // {

            $.ajax(
            {
               url: '/procurement/prEntry/getTaxCalculated',
               type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                 $("#view_dummy").html(result);
                
                var tax_amount = $("#tax_amount").val();
                var tax_per = $("#tax_per").val();
                var total_amount = $("#total_amount").val();

                $("#tax_price").val(tax_amount);
                $("#tax_percentage").val(tax_per);
                $("#total_final").val(total_amount);
               
               }
            });
        // }        
    }

    function validateDetailsData()
    {
        if($('#form_master').valid())
        {
            console.log($("#view").html());
            var addedProgam = $("#view").html();
            if(addedProgam== undefined)
            {
                alert("Add Bill Claim Details");
            }
            else if(addedProgam== '')
            {
                alert("Add Bill Claim Details");
            }
            else
            {
                $('#type').prop('disabled', false);
                $('#form_master').submit();
                // checkBudget();
            }
        }    
    }


    // function checkBudget(id)
    // {
    //      $.ajax(
    //         {
    //            url: '/procurement/prEntry/checkBudgetAmount',
    //            type: 'GET',
    //            error: function()
    //            {
    //             alert('Something is wrong');
    //            },
    //            success: function(result)
    //            {
    //                 // $("#view").html(result);
    //                 var budget = $("#check_budget").val();
    //                 // alert(ta);
    //                 if(result == "1")
    //                 {
    //                     $('#type').prop('disabled', false);
    //                     $('#form_master').submit();
    //                 }
    //                 else
    //                 {
    //                    alert('One Of The Entered Budget Exceeds The Balance Amount Of The Budget Allocated')
    //                 }
    //            }
    //         });
    // }


     $(document).ready(function() {
        $("#form_master").validate({
            rules: {
                type: {
                    required: true
                },
                 id_financial_year: {
                    required: true
                },
                 id_budget_year: {
                    required: true
                },
                 department_code: {
                    required: true
                },
                 amount: {
                    required: true
                },
                 description: {
                    required: true
                },
                 customer_name: {
                    required: true
                },
                 id_staff: {
                    required: true
                },
                bank_acc_no:
                {
                    required: true
                }
            },
            messages: {
                type: {
                    required: "<p class='error-text'>Select Type</p>",
                },
                id_financial_year: {
                    required: "<p class='error-text'>Select Financial Year</p>",
                },
                id_budget_year: {
                    required: "<p class='error-text'>Select Budget Year</p>",
                },
                department_code: {
                    required: "<p class='error-text'>Select Department Code</p>",
                },
                amount: {
                    required: "<p class='error-text'>Enter Claim Details For Amount</p>",
                },
                description: {
                    required: "<p class='error-text'>Description Required</p>",
                },
                customer_name: {
                    required: "<p class='error-text'>Customer Name Required</p>",
                },
                id_staff: {
                    required: "<p class='error-text'>Select Staff</p>",
                },
                bank_acc_no: {
                    required: "<p class='error-text'>Bank Account Number Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


    $(document).ready(function() {
        $("#form_master_details").validate({
            rules: {
                id_category: {
                    required: true
                },
                 id_sub_category: {
                    required: true
                },
                 id_item: {
                    required: true
                },
                 dt_account: {
                    required: true
                },
                 dt_activity: {
                    required: true
                },
                 dt_department: {
                    required: true
                },
                 dt_fund: {
                    required: true
                },
                 cr_account: {
                    required: true
                },
                 cr_activity: {
                    required: true
                },
                 cr_department: {
                    required: true
                },
                 cr_fund: {
                    required: true
                },
                 quantity: {
                    required: true
                },
                 price: {
                    required: true
                },
                 id_tax: {
                    required: true
                },
                total_final: {
                    required: true
                }
            },
            messages: {
                id_category: {
                    required: "<p class='error-text'>Select Category</p>",
                },
                id_sub_category: {
                    required: "<p class='error-text'>Select Sub Category</p>",
                },
                id_item: {
                    required: "<p class='error-text'>Select Item</p>",
                },
                dt_account: {
                    required: "<p class='error-text'>Select Account Code</p>",
                },
                dt_activity: {
                    required: "<p class='error-text'>Select Activity Code</p>",
                },
                dt_department: {
                    required: "<p class='error-text'>Select Department Code</p>",
                },
                dt_fund: {
                    required: "<p class='error-text'>Select Fund Code</p>",
                },
                cr_account: {
                    required: "<p class='error-text'>Select Account Code</p>",
                },
                cr_activity: {
                    required: "<p class='error-text'>Select Activity Code</p>",
                },
                cr_department: {
                    required: "<p class='error-text'>Select Department Code</p>",
                },
                cr_fund: {
                    required: "<p class='error-text'>Select Fund Code</p>",
                },
                quantity: {
                    required: "<p class='error-text'>Enter Quantity</p>",
                },
                price: {
                    required: "<p class='error-text'>Enter Price Per Item</p>",
                },
                id_tax: {
                    required: "<p class='error-text'>Select Tax Code</p>",
                },
                total_final: {
                    required: "<p class='error-text'>Wait Till Total Calculation</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
<script type="text/javascript">
    $('select').select2();
</script>
<script>
  $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
        yearRange: "1960:2020"
    });
  } );
</script>