            <div class="sidebar">
                <div class="user-profile clearfix">
                    <a href="#" class="user-profile-link">
                        <span><img src="<?php echo BASE_PATH; ?>assets/img/user_profile.jpg"></span> <?php echo $name; ?>
                    </a>
                    <div class="dropdown">
                        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
                            <li><a href="/setup/user/profile">Edit Profile</a></li>
                            <li><a href="/setup/user/logout">Logout</a></li>
                        </ul>
                    </div>
                </div>
                <div class="sidebar-nav">
                    <!-- <h4>Setup</h4>
                    <ul>
                        <li><a href="/ap/billRegistration/list">General Profile Registration</a></li>
                    </ul> -->

                    <h4>Staff Bill Registration</h4>
                    <ul>
                        <li><a href="/ap/staffBillRegistration/list">General / Staff Bill Registration</a></li>
                        <li><a href="/ap/staffBillRegistration/approvalList">General / Staff Bill Registration Approval</a></li>
                    </ul>

                    <h4>Bill Registration</h4>
                    <ul>
                       
                        <li><a href="/ap/billRegistration/list">Bill Registration</a></li>
                        <li><a href="/ap/billRegistration/approvalList">Bill Registration Approval</a></li>
                        <!-- <li><a href="/ap/billRegistration/summaryList">Bill Registration Summary</a></li> -->
                       
                    </ul>

                    <h4>Payment Voucher</h4>
                    <ul>                       
                        <li><a href="/ap/paymentVoucher/list">Payment Voucher</a></li>
                        <li><a href="/ap/paymentVoucher/approvalList">Payment Voucher Approval</a></li>
                        <li><a href="/ap/paymentVoucher/cancellationList">Payment Voucher Cancellation</a></li>
                        <!-- <li><a href="/ap/billRegistration/list">Payment Voucher Summary</a></li> -->
                    </ul>    

                    <h4>SAB</h4>
                    <ul>                       
                        <li><a href="/ap/EtfVoucher/list">SAB Entry</a></li>
                        <li><a href="/ap/EtfVoucher/approvalList">SAB Approval</a></li>
                        <li><a href="/ap/EtfBatch/list">SAB Batch</a></li>
                    </ul>

                    <h4>Petty Cash</h4>
                    <ul>                       
                        <li><a href="/ap/pettyCashAllocation/list">Petty Cash Allocation</a></li>
                        <li><a href="/ap/pettyCashAllocation/approvalList">Petty Cash Allocation Approval</a></li>
                        <li><a href="/ap/pettyCashEntry/list">Petty Cash Entry</a></li>
                        <li><a href="/ap/pettyCashEntry/approvalList">Petty Cash Entry Approval</a></li>
                    </ul>

                     <h4>Emergency Fund</h4>
                    <ul>                       
                        <li><a href="/ap/emergencyFundAllocation/list">Emergency Fund Allocation</a></li>
                        <li><a href="/ap/emergencyFundAllocation/approvalList">Emergency Fund Allocation Approval</a></li>
                        <li><a href="/ap/emergencyFundEntry/list">Emergency Fund Entry</a></li>
                        <li><a href="/ap/emergencyFundEntry/approvalList">Emergency Fund Entry Approval</a></li>
                    </ul>

                </div>
            </div>