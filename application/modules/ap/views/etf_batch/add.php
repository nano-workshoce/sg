<?php $this->load->helper("form"); ?>
<form id="form_pr_entry" action="" method="post">

<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
            <div class="page-title clearfix">
                <h3>Add ETF Batch</h3>
            </div>

        <div class="form-container">
            <h4 class="form-group-title">ETF Batch Entry</h4>

                

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Bank Reference Number <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="bank_reference_number" name="bank_reference_number">
                    </div>
                </div>


                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Batch Id <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="batch_id" name="batch_id">
                    </div>
                </div>
              

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Description <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description">
                    </div>
                </div>

            </div>


        </div>

        <div class="form-container">
            <h4 class="form-group-title">Search ETF Voucher</h4>


            <div class="row">

                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Reference Number / Batch Id</label>
                        <input type="text" class="form-control" id="reference_number" name="reference_number">
                    </div>
                </div>

               <!--  <div class="col-sm-3">
                    <div class="form-group">
                        <label>Voucher Type <span class='error-text'>*</span></label>
                        <select name='voucher_type' id='voucher_type' class='form-control' >
                            <option value=''>Select</option>
                            <option value='Investment'>Investment</option>
                            <option value='Bill Registration'>Bill Registration</option>
                        </select>
                    </div>
                </div> -->


                <div class="col-sm-3">
                    <div class="form-group">
                        <label>From Date</label>
                        <input type="text" class="form-control datepicker" id="from_date" name="from_date">
                    </div>
               </div>

                 <div class="col-sm-3">
                    <div class="form-group">
                        <label>To Date</label>
                        <input type="text" class="form-control datepicker" id="to_date" name="to_date">
                    </div>
               </div>
                
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" onclick="searchEtfVoucher()">Search</button>
              </div>

          </div>


        <div class="form-container" style="display: none" id="view_etf_voucher">
            <h4 class="form-group-title">ETF Vouchers</h4>


            <div class="row">
               <span id='view_pv'></span>
            </div>


        </div>



            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="list" class="btn btn-link">Cancel</a>
                </div>
            </div>
        
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
</form>


<script>

     function searchEtfVoucher()
    {
                    // alert('No Vouchers Available For Selected Search')

        var tempPR = {};
        tempPR['reference_number'] = $("#reference_number").val();
        tempPR['from_date'] = $("#from_date").val();
        tempPR['to_date'] = $("#to_date").val();
        // tempPR['voucher_type'] = $("#voucher_type").val();
        // alert(tempPR);
            $.ajax(
            {
               url: '/ap/etfBatch/searchEtfVoucher',
               type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                // alert('assd');
                if(result)
                {
                    $("#view_etf_voucher").show();
                    $("#view_pv").html(result);
                }else
                {
                    alert('No ETF Vouchers Available For Selected Search')
                }
               }
            });
    }



    $(document).ready(function() {
        $("#form_pr_entry").validate({
            rules: {
                bank_reference_number: {
                    required: true
                },
                 batch_id: {
                    required: true
                },
                 description: {
                    required: true
                }
            },
            messages: {
                bank_reference_number: {
                    required: "<p class='error-text'>Bank Reference Number Required</p>",
                },
                batch_id: {
                    required: "<p class='error-text'>Batch Id Required</p>",
                },
                description: {
                    required: "<p class='error-text'>Description Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
<script type="text/javascript">
    $('select').select2();
</script>
<script>
  $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
    });
  } );

  $(function () {
        $("#checkAll").click(function () {
            if ($("#checkAll").is(':checked')) {
                $(".check").prop("checked", true);
            } else {
                $(".check").prop("checked", false);
            }
        });
    });


</script>