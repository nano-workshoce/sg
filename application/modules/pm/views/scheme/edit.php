<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit Scheme</h3>
        </div>
        <form id="form_scheme" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Scheme Details</h4>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Code <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="code" name="code" value="<?php echo $scheme->code; ?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $scheme->description; ?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name In Other Language </label>
                        <input type="text" class="form-control" id="description_in_optional_language" name="description_in_optional_language" value="<?php echo $scheme->description_in_optional_language; ?>">
                    </div>
                </div>

            </div>

             <div class="row">

                <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status *</p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" <?php if($scheme->status=='1') {
                                 echo "checked=checked";
                              };?>><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0" <?php if($scheme->status=='0') {
                                 echo "checked=checked";
                              };?>>
                              <span class="check-radio"></span> In-Active
                            </label>                              
                        </div>                         
                </div>
               
            </div>

        </div>



        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="../list" class="btn btn-link">Cancel</a>
            </div>
        </div>

        
        </form>





        <div class="form-container">
            <h4 class="form-group-title"> Scheme Details</h4>          
            <div class="m-auto text-center">
                <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
            </div>
            <div class="clearfix">
                <ul class="nav nav-tabs offers-tab sub-tabs text-center" role="tablist" >
                    <li role="presentation" class="active" ><a href="#invoice" class="nav-link border rounded text-center"
                            aria-controls="invoice" aria-selected="true"
                            role="tab" data-toggle="tab">Nationality Details</a>
                    </li>
                </ul>

                
                <div class="tab-content offers-tab-content">

                    <div role="tabpanel" class="tab-pane active" id="invoice">
                        <div class="col-12 mt-4">




                        <form id="form_scheme_nationality" action="" method="post">
                            <div class="form-container">
                                <h4 class="form-group-title">Scheme Has Nationality Details</h4>


                                <div class="row">

                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label>Nationality <span class='error-text'>*</span></label>
                                            <select name="id_nationality" id="id_nationality" class="form-control">
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($nationalityList))
                                                {
                                                    foreach ($nationalityList as $record)
                                                    {?>
                                                        <option value="<?php echo $record->id;?>"
                                                        ><?php echo $record->name; ?>
                                                        </option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                  
                                    <div class="col-sm-3">
                                        <button type="button" class="btn btn-primary btn-lg form-row-btn" onclick="addSchemeHasNationality()">Add</button>
                                    </div>

                                </div>

                                <div class="row">
                                    <div id="view"></div>
                                </div>

                            </div>
                        </form>


                        <?php

                            if(!empty($schemeHasNationalityList))
                            {
                                ?>

                                <div class="form-container">
                                        <h4 class="form-group-title">Nationality Details</h4>

                                    

                                      <div class="custom-table">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                <th>Sl. No</th>
                                                <th>Nationality</th>
                                                <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                 <?php
                                             $total = 0;
                                              for($i=0;$i<count($schemeHasNationalityList);$i++)
                                             { ?>
                                                <tr>
                                                <td><?php echo $i+1;?></td>
                                                <td><?php echo $schemeHasNationalityList[$i]->nationality;?></td>
                                                <td>
                                                <a onclick="deleteSchemeHasNationality(<?php echo $schemeHasNationalityList[$i]->id; ?>)">Delete</a>
                                                </td>

                                                 </tr>
                                              <?php 
                                          } 
                                          ?>
                                            </tbody>
                                        </table>
                                      </div>

                                    </div>




                            <?php
                            
                            }
                             ?>


                        </div> 
                    </div>







                </div>

            </div>
        </div>






        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>

    $('select').select2();

    function addSchemeHasNationality()
    {

        if($('#form_scheme_nationality').valid())
        {


        var tempPR = {};
        tempPR['id_nationality'] = $("#id_nationality").val();
        tempPR['id_scheme'] = <?php echo $scheme->id;?>;
            $.ajax(
            {
               url: '/setup/scheme/addSchemeHasNationality',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                window.location.reload();
                // $("#view").html(result);
               }
            });
        }
    }

    function deleteSchemeHasNationality(id) {
                    // alert(id);

            $.ajax(
            {
               url: '/setup/scheme/deleteSchemeHasNationality/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                // $("#view").html(result);
                    // var ta = $("#inv-total-amount").val();
                // $("#total_amount").val(ta);
                    // alert(result);
                    window.location.reload();
               }
            });
    }


    

    $(document).ready(function() {
        $("#form_scheme_nationality").validate({
            rules: {
                id_nationality: {
                    required: true
                }
            },
            messages: {
                id_nationality: {
                    required: "<p class='error-text'>Select Nationality</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });



    $(document).ready(function() {
        $("#form_scheme").validate({
            rules: {
                description: {
                    required: true
                },
                code: {
                    required: true
                }
            },
            messages: {
                description: {
                    required: "<p class='error-text'>Name Required</p>",
                },
                code: {
                    required: "<p class='error-text'>Code Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>