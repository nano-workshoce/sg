<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Patient Appointment History</h3>
            <a href="<?php echo '/records/bookingSummary/list'; ?>" class="btn btn-link">< Back</a>
        </div>



  <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Patient Details
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">
          <div id="collapseOne" class="panel-collapse collapse out" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">

                <div class="row">
                  
                        <div class='data-list'>
                            <div class='row'> 
                                <div class='col-sm-6'>
                                    <dl>
                                        <dt>Patient Name :</dt>
                                        <dd><?php echo ucwords($patient->full_name);?></dd>
                                    </dl>
                                    <dl>
                                        <dt>Patient Phone :</dt>
                                        <dd><?php echo $patient->phone ?></dd>
                                    </dl>
                                    <dl>
                                        <dt>Patient Email :</dt>
                                        <dd><?php echo $patient->email; ?></dd>
                                    </dl>
                                    <dl>
                                        <dt>Patient Gender :</dt>
                                        <dd><?php echo $patient->gender; ?></dd>
                                    </dl>                        
                                </div>        
                                
                                <div class='col-sm-6'>                           
                                    <dl>
                                        <dt>Patient Register No. :</dt>
                                        <dd><?php echo $patient->register_no ?></dd>
                                    </dl>
                                    <dl>
                                        <dt>Address :</dt>
                                        <dd><?php echo $patient->mail_address1; ?></dd>
                                    </dl>
                                    <dl>
                                        <dt>City :</dt>
                                        <dd><?php echo $patient->mailing_city; ?></dd>
                                    </dl>
                                    <dl>
                                        <dt>Patient DOB :</dt>
                                        <dd><?php echo date('d-m-Y', strtotime($patient->date_of_birth)); ?></dd>
                                    </dl>  
                                </div>
                            </div>
                        </div>

                </div>

              </div>

            </div>
          </div>
        </form>
      </div>
  
  </div>




        
    










  <div class="panel-group advanced-search" id="accordion_two" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingTwo">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion_two" href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
              Patient Appointments Search
            </a>
          </h4>
        </div>

        <form action="" method="post" id="searchForm">
          <div id="collapseTwo" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">
            <div class="panel-body">
              <div class="form-horizontal">

                
                <div class="row">

                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Name</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" name="name" id="name" value="<?php echo $searchParam['name']; ?>">
                      </div>
                    </div>
                  </div>

                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Register No</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" name="register_no" id="register_no" value="<?php echo $searchParam['register_no']; ?>">
                      </div>
                    </div>
                  </div>


                </div>


                <div class="row">


                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Phone</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" name="phone" id="phone" value="<?php echo $searchParam['phone']; ?>">
                      </div>
                    </div>
                  </div>



                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Email Id</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" name="email" id="email" value="<?php echo $searchParam['email']; ?>">
                      </div>
                    </div>
                  </div>


                </div>


                <div class="row">


                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Collection By</label>
                      <div class="col-sm-8">
                        <select name="id_staff" id="id_staff" class="form-control selitemIcon">
                            <option value="">Select</option>
                            <?php
                            if (!empty($staffList))
                            {
                                foreach ($staffList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>"
                              <?php
                              if($record->id == $searchParam['id_staff'])
                              {
                                echo "selected";
                              }
                              ?>
                              >
                                <?php echo $record->name . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                      </div>
                    </div>
                  </div>



                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Status</label>
                      <div class="col-sm-8">
                        <select name="status" id="status" class="form-control selitemIcon">
                           <option value="">Select</option>  
                           <option value="0"
                              <?php
                              if(0 == $searchParam['status'])
                              {
                                echo "selected";
                              }
                              ?>
                              >Pending                                
                             </option>
                            <option value="1"
                              <?php
                              if(1 == $searchParam['status'])
                              {
                                echo "selected";
                              }
                              ?>
                              >Completed                                
                             </option>
                            <option value="2"
                              <?php
                              if(2 == $searchParam['status'])
                              {
                                echo "selected";
                              }
                              ?>
                              >Cancelled                                
                             </option>
                        </select>
                      </div>
                    </div>
                  </div>


                </div>



              </div>
              <div class="app-btn-group">
                <button type="submit" class="btn btn-primary">Search</button>
                <a href="<?php echo '/records/bookingSummary/edit/' . $id_patient ?>" class="btn btn-link">Clear All Fields</a>
              </div>
            </div>
          </div>
        </form>
      
      </div>
    </div>

    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl. No</th>
            <th>Reference No.</th>
            <th>Patient Register No.</th>
            <th>Patient Name</th>
            <th>Gender</th>
            <th>Phone No</th>
            <th>Email ID</th>
            <th>DOB</th>
            <th>Collection By</th>
            <th>Date</th>
            <th>Time Slot</th>
            <th class="text-center">Status</th>
            <th class="text-center">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($patientBookingList)) {
            $i=1;
            foreach ($patientBookingList as $record) {
            ?>
              <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $record->reference_number ?></td>
                <td><?php echo $record->register_no ?></td>
                <td><?php echo $record->full_name ?></td>
                <td><?php echo $record->gender ?></td>
                <td><?php echo $record->phone ?></td>
                <td><?php echo $record->email ?></td>
                <td><?php echo date("d-m-Y", strtotime($record->date_of_birth)) ?></td>
                <td><?php echo $record->staff_id . " - " . $record->staff_name ?></td>
                <td><?php echo date('d-m-Y', strtotime($record->appointment_date)) ?></td>
                <td><?php echo $record->time_slot ?></td>
                <td>
                <?php if( $record->status == '0')
                {
                  echo "Pending";
                }
                elseif( $record->status == '1')
                {
                  echo "Completed";
                }
                elseif( $record->status == '2')
                {
                  echo "Cancelled";
                } 
                ?></td>
                <td class="text-center">
                    <a href="<?php echo '../view/' . $record->id; ?>" title="View Appointment">View</a>
                  <!--  -->
                </td>
              </tr>
          <?php
          $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
















        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>
    $('select').select2();

    $(function(){
    $(".datepicker").datepicker(
    {
        changeYear: true,
        changeMonth: true,
    });
    });



    function showBookingType(booking_type)
    {
      if(booking_type == 'Package')
      {
            $("#view_package").show();
            $("#view_test").hide();
      }
      else if(booking_type == 'Individual')
      {
            $("#view_package").hide();
            $("#view_test").show();
      }
    }


    function saveTests()
    {
      // if($('#form_tests').valid())
      // {
        var booking_type = $("#booking_type").val();
        var id_package = $("#id_package").val();
        var id_test = $("#id_test").val();
        // alert(booking_type);

        if(booking_type != '')
        {
          
          // var id_selected =0;
          // if(booking_type == 'Package')
          // {
          //   $("#id_test").find('option[value="'+id_selected+'"]').attr('selected',true);
          //   $('select').select2();
          // }

          // if(booking_type == 'Individual')
          // {
          //   $("#id_package").find('option[value="'+id_selected+'"]').attr('selected',true);
          //   $('select').select2();
          // }

          

        var tempPR = {};
        tempPR['booking_type'] = booking_type;
        tempPR['id_package'] = id_package;
        tempPR['id_test'] = id_test;

            $.ajax(
            {
               url: '/patient/patientBooking/addTempTestBooking',
               type: 'POST',
               data:
               {
                data: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                if(result == '')
                {
                    alert('No Debit Budget GL Found For Entered Data')
                }
                else
                {
                    $("#view_temp_test").html(result);
                    var total_test_amount = $("#total_detail").val();
                    $("#total_test_amount").val(total_test_amount);

                }
               }
            });
        }
      // }
    }

    function deleteTempTestBookingDetails(id)
    {

        $.get("/patient/patientBooking/deleteTempTestBookingDetails/"+id, function(data, status)
        {
            $("#view_temp_test").html(data);
            var total_test_amount = $("#total_detail").val();
            $("#total_test_amount").val(total_test_amount);
        });
    }





    function saveSamples()
    {
      // if($('#form_samples').valid())
      // {

        var id_sample = $("#id_sample").val();

        if(id_sample != '')
        {

        var tempPR = {};
        tempPR['id_sample'] = id_sample;

            $.ajax(
            {
               url: '/patient/patientBooking/addTempSampleBooking',
               type: 'POST',
               data:
               {
                data: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                if(result == '')
                {
                    alert('No Debit Budget GL Found For Entered Data')
                }
                else
                {
                    $("#view_temp_sample").html(result);
                }
               }
            });
        }
      // }
    }
    
    function deleteTempSampleBookingDetails(id)
    {

        $.get("/patient/patientBooking/deleteTempSampleBookingDetails/"+id, function(data, status)
        {
            $("#view_temp_sample").html(data);
        });
    }


    function validateDetailsData()
    {
      if($('#form_sponser').valid())
        {
            var addedTest = $("#view_temp_test").html();
            var addedSample = $("#view_temp_sample").html();
            console.log(addedTest);
            console.log(addedSample);
            if(addedTest=='' || addedTest == undefined)
            {
                alert("Add Test Details");
            }
            else if(addedSample == '' || addedSample == undefined)
            {
                alert("Add Sample Collection Details");
            }
            else
            {
                showConfirmation();
            }
        }    
    }


    function showConfirmation()
    {
      var id_staff = $("#id_staff").val();
      var tempPR = {};
      tempPR['id_staff'] = id_staff;
      $.ajax(
            {
               url: '/patient/patientBooking/showConfirmation',
               type: 'POST',
               data:
               {
                data: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    // alert(result);
                    $('#myModal').modal('show');
                    $("#view_model").html(result);

                    var appointment_date = $("#time_slot").val();
                    var time_slot = $("#appointment_date").val();
                    var total_amount = $("#total_detail").val();
                    var staff = $("#staff").val();

                    // alert(appointment_date);
                    // alert(time_slot);

                    $("#test_booking_date").val(appointment_date);
                    $("#test_sample_collection_by").val(staff);
                    $("#test_booking_time_slot").val(time_slot);
                    $("#test_booking_amount").val(total_amount);

               }
            });

    }

    function submitForm()
    {
      $('#form_sponser').submit();
    }



    $(document).ready(function()
    {
        $("#form_sponser").validate({
            rules: {
                appointment_date: {
                    required: true
                },
                time_slot: {
                    required: true
                },
                id_staff: {
                  required: true
                }
            },
            messages: {
                appointment_date: {
                    required: "<p class='error-text'>Select Appointment Date</p>",
                },
                time_slot: {
                    required: "<p class='error-text'>Select Time Slot</p>",
                },
                id_staff: {
                    required: "<p class='error-text'>Select Sample Collection Staff</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });




    $(document).ready(function()
    {
        $("#form_samples").validate({
            rules: {
                id_sample: {
                    required: true
                }
            },
            messages: {
                id_sample: {
                    required: "<p class='error-text'>Select Sample</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });




    $(document).ready(function()
    {
        $("#form_tests").validate({
            rules: {
                booking_type: {
                    required: true
                },
                id_package: {
                  required: true
                },
                id_test: {
                  required: true
                }
            },
            messages: {
                booking_type: {
                    required: "<p class='error-text'>Select Booking Type</p>",
                },
                id_package: {
                    required: "<p class='error-text'>Select Package</p>",
                },
                id_test: {
                    required: "<p class='error-text'>Select Tests</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });




</script>