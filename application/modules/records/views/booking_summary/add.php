<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Patient</h3>
        </div>
        <form id="form_sponser" action="" method="post">
            <div class="form-container">
                    <h4 class="form-group-title">Patient Details</h4> 


                <div class="row">

                    <!-- <div class="col-sm-4">
                        <div class="form-group">
                            <label>Registration No. <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="code" name="code" readonly onfocus="showCodeAfterSave()">
                        </div>
                    </div> -->

                    <div class="col-sm-4">
                       <div class="form-group">
                          <label>Salutation <span class='error-text'>*</span></label>
                          <select name="salutation" id="salutation" class="form-control">
                             <option value="">Select</option>
                             <?php
                                if (!empty($salutationList)) {
                                    foreach ($salutationList as $record) {
                                ?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->name;  ?>        
                             </option>
                             <?php
                                }
                                }
                                ?>
                          </select>
                       </div>
                    </div>




                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>First Name <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="first_name" name="first_name">
                        </div>
                    </div>


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Last Name <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="last_name" name="last_name">
                        </div>
                    </div>


                </div>


                <div class="row">


                    <div class="col-sm-4">
                       <div class="form-group">
                          <label>Date Of Birth <span class='error-text'>*</span></label>
                          <input type="text" class="form-control datepicker" id="date_of_birth" name="date_of_birth" autocomplete="off">
                       </div>
                    </div>


                    <!-- <div class="col-sm-4">
                       <div class="form-group">
                          <label>Martial Status <span class='error-text'>*</span></label>
                          <select class="form-control" id="martial_status" name="martial_status">
                             <option value="">SELECT</option>
                             <option value="Single">
                                SINGLE
                             </option>
                             <option value="Married">
                                MARRIED
                             </option>
                             <option value="Divorced">DIVORCED</option>
                          </select>
                       </div>
                    </div> -->


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Email <span class='error-text'>*</span></label>
                            <input type="email" class="form-control" id="email" name="email">
                        </div>
                    </div>



                    <div class="col-sm-4">
                         <div class="form-group">
                            <label>Gender <span class='error-text'>*</span></label>
                            <select class="form-control" id="gender" name="gender">
                               <option value="">SELECT</option>
                               <option value="Male">MALE</option>
                               <option value="Female">FEMALE</option>
                               <!-- <option value="Others"<?php if($getApplicantDetails->gender=='Others'){ echo "selected"; } ?> >OTHERS</option> -->
                            </select>
                         </div><br/><br/>
                      </div>


                </div>


                


            </div>

            <div class="form-container">
                 <h4 class="form-group-title">Contact Details</h4>

                 <div class="row">
                    
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Phone No. <span class='error-text'>*</span></label>
                            <input type="number" class="form-control" id="phone" name="phone">
                        </div>
                    </div>



                    <div class="col-sm-4">
                       <div class="form-group">
                          <label>Mailing Address 1 <span class='error-text'>*</span></label>
                          <input type="text" class="form-control" id="mail_address1" name="mail_address1" >
                       </div>
                    </div>

                    <div class="col-sm-4">
                       <div class="form-group">
                          <label>Mailing Address 2 </label>
                          <input type="text" class="form-control" id="mail_address2" name="mail_address2">
                       </div>
                    </div>

                    
                 </div>
                 <div class="row">

                    <div class="col-sm-4">
                       <div class="form-group">
                          <label>Mailing Country <span class='error-text'>*</span></label>
                          <select name="mailing_country" id="mailing_country" class="form-control" onchange="getStateByCountry(this.value)">
                             <option value="">Select</option>
                             <?php
                                if (!empty($countryList))
                                {
                                    foreach ($countryList as $record)
                                    {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->name;?>
                             </option>
                             <?php
                                }
                                }
                                ?>
                          </select>
                       </div>
                    </div>


                    <div class="col-sm-4">
                       <div class="form-group">
                          <label>Mailing State <span class='error-text'>*</span></label>
                          <span id='view_mailing_state'>
                               <select name="mailing_state" id="mailing_state" class="form-control">
                                <option value=''>Select</option>
                               </select>

                          </span>
                       </div>
                    </div>


                    <div class="col-sm-4">
                       <div class="form-group">
                          <label>Mailing City <span class='error-text'>*</span></label>
                          <input type="text" class="form-control" id="mailing_city" name="mailing_city">
                       </div>
                    </div>


                 </div>


                 <div class="row">


                    <div class="col-sm-4">
                       <div class="form-group">
                          <label>Mailing Zipcode <span class='error-text'>*</span></label>
                          <input type="number" class="form-control" id="mailing_zipcode">
                       </div>
                    </div>


                    <br>

                    <div class="col-sm-4">
                        <div class="form-group">

                 &emsp;<input type="checkbox" id="present_address_same_as_mailing_address" onclick="checkthecheckboxstatus()" name="present_address_same_as_mailing_address" value="1" checked="true">&emsp;
              Permanent Address Same as Mailing Address

                        </div>
                    </div>


                </div>






              
            </div>




              




              <!-- <div class="form-container">
                 <h4 class="form-group-title">Permanent Address</h4>
                 <div class="row">
                    <div class="col-sm-4">
                       <div class="form-group">
                          <label>Permanent Address 1 <span class='error-text'>*</span></label>
                          <input type="text" class="form-control" id="permanent_address1" name="permanent_address1" value="<?php echo $getApplicantDetails->permanent_address1 ?>">
                       </div>
                    </div>
                    <div class="col-sm-4">
                       <div class="form-group">
                          <label>Permanent Address 2 </label>
                          <input type="text" class="form-control" id="permanent_address2" name="permanent_address2" value="<?php echo $getApplicantDetails->permanent_address2 ?>">
                       </div>
                    </div>
                    <div class="col-sm-4">
                       <div class="form-group">
                          <label>Permanent Country <span class='error-text'>*</span></label>
                          <select name="permanent_country" id="permanent_country" class="form-control" onchange="getStateByCountryPermanent(this.value)">
                             <option value="">Select</option>
                             <?php
                                if (!empty($countryList))
                                {
                                    foreach ($countryList as $record)
                                    {?>
                             <option value="<?php echo $record->id;  ?>"
                                <?php if($getApplicantDetails->permanent_country==$record->id){ echo "selected"; } ?>
                                >
                                <?php echo $record->name;?>
                             </option>
                             <?php
                                }
                                }
                                ?>
                          </select>
                       </div>
                    </div>
                 </div>
                 <div class="row">
                    <div class="col-sm-4">
                       <div class="form-group">
                          <label>Permanent State <span class='error-text'>*</span></label>
                          <span id='view_permanent_state'>
                               <select name="permanent_state" id="permanent_state" class="form-control">
                                <option value=''>Select</option>
                               </select>

                          </span>
                       </div>
                    </div>
                    <div class="col-sm-4">
                       <div class="form-group">
                          <label>Permanent City <span class='error-text'>*</span></label>
                          <input type="text" class="form-control" id="permanent_city" name="permanent_city" value="<?php echo $getApplicantDetails->permanent_city ?>">
                       </div>
                    </div>
                    <div class="col-sm-4">
                       <div class="form-group">
                          <label>Permanent Zipcode <span class='error-text'>*</span></label>
                          <input type="number" class="form-control" id="permanent_zipcode" name="permanent_zipcode" value="<?php echo $getApplicantDetails->permanent_zipcode ?>">
                       </div>
                    </div>
                 </div>
              </div> -->





            <!-- <div class="form-container">
                    <h4 class="form-group-title">Contact Details</h4>  
                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Address <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="address" name="address">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Address 2 <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="address2" name="address2">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Phone / Mobile Number <span class='error-text'>*</span></label>
                            <input type="number" class="form-control" id="mobile_number" name="mobile_number">
                        </div>
                    </div>
                </div>

                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Select Country <span class='error-text'>*</span></label>
                            <select name="id_country" id="id_country" class="form-control" onchange="getStateByCountry(this.value)">
                                <option value="">Select</option>
                                <?php
                                if (!empty($countryList))
                                {
                                    foreach ($countryList as $record)
                                    {?>
                                <option value="<?php echo $record->id;  ?>">
                                    <?php echo $record->name;?>
                                </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Select State <span class='error-text'>*</span></label>
                            <span id="view_state">
                                  <select class="form-control" id='id_state' name='id_state'>
                                    <option value=''></option>
                                  </select>
                             </span> 
                        </div>
                    </div>

                    

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Location <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="location" name="location">
                        </div>
                    </div>

                </div>

                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>ZIP Code <span class='error-text'>*</span></label>
                            <input type="number" class="form-control" id="zip_code" name="zip_code">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Fax Number <span class='error-text'>*</span></label>
                            <input type="number" class="form-control" id="fax" name="fax">
                        </div>
                    </div>


                </div>

                <div class="row">

                    <div class="col-sm-4">
                            <div class="form-group">
                                <p>Status <span class='error-text'>*</span></p>
                                <label class="radio-inline">
                                <input type="radio" name="status" id="status" value="1" checked="checked"><span class="check-radio"></span> Active
                                </label>
                                <label class="radio-inline">
                                <input type="radio" name="status" id="status" value="0"><span class="check-radio"></span> Inactive
                                </label>                              
                            </div>                         
                    </div>

                </div>
            </div> -->

            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="list" class="btn btn-link">Cancel</a>
                </div>
            </div>
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>

    $('select').select2();

    $(function(){
    $(".datepicker").datepicker(
    {
        changeYear: true,
        changeMonth: true,
    });
    });

    function showCodeAfterSave()
    {
        $("#code").val("Register No will Be generated After Patient Saved ");
    }
    

    function getStateByCountry(id)
    {

        $.get("/records/patient/getStateByCountry/"+id, function(data, status){
       
            $("#view_mailing_state").html(data);
            // $("#view_programme_details").html(data);
            // $("#view_programme_details").show();
        });
    }


    $(document).ready(function() {
        $("#form_sponser").validate({
            rules: {
                salutation: {
                    required: true
                },
                first_name: {
                    required: true
                },
                last_name: {
                    required: true
                },
                date_of_birth: {
                    required: true
                },
                email: {
                    required: true
                },
                gender: {
                    required: true
                },
                mail_address1: {
                    required: true
                },
                mail_address2: {
                    required: true
                },
                mailing_country: {
                    required: true
                },
                mailing_state: {
                    required: true
                },
                mailing_city: {
                    required: true
                },
                mailing_zipcode: {
                    required: true
                },
                phone: {
                    required: true
                }
            },
            messages: {
                salutation: {
                    required: "<p class='error-text'>Select Salutation</p>",
                },
                first_name: {
                    required: "<p class='error-text'>First name Required</p>",
                },
                last_name: {
                    required: "<p class='error-text'>Last name Required</p>",
                },
                date_of_birth: {
                    required: "<p class='error-text'>Select DOB</p>",
                },
                email: {
                    required: "<p class='error-text'>Email Required</p>",
                },
                gender: {
                    required: "<p class='error-text'>Select Gender</p>",
                },
                mail_address1: {
                    required: "<p class='error-text'>Mailing Address 2 Required</p>",
                },
                mail_address2: {
                    required: "<p class='error-text'>Mailing Address 2 Required</p>",
                },
                mailing_country: {
                    required: "<p class='error-text'>Select Country</p>",
                },
                mailing_state: {
                    required: "<p class='error-text'>State Required</p>",
                },
                mailing_city: {
                    required: "<p class='error-text'>Mailing City Required</p>",
                },
                mailing_zipcode: {
                    required: "<p class='error-text'>Zipcode Required</p>",
                },
                phone: {
                    required: "<p class='error-text'>Phone No. Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>