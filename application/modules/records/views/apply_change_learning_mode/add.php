<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Apply Change Learning Mode</h3>
        </div>
        <form id="form_apply_change_programme" action="" method="post">


        <div class="form-container">
            <h4 class="form-group-title">Select Student For Apply Change Learning Mode</h4> 
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select Program <span class='error-text'>*</span></label>
                        <select name="id_programme" id="id_programme" class="form-control" onchange="getPreviousProgramSchemeByProgramId(this.value)">
                            <option value="">Select</option>
                            <?php
                            if (!empty($programmeList))
                            {
                                foreach ($programmeList as $record)
                                {?>
                            <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                            </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div> 

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select Program Learning Mode <span class='error-text'>*</span></label>
                        <span id="view_program_scheme">
                          <select class="form-control" id='id_program_scheme' name='id_program_scheme'>
                            <option value=''></option>
                          </select>
                        </span>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select Intake <span class='error-text'>*</span></label>
                        <span id="view_program_intake">
                          <select class="form-control" id='id_intake' name='id_intake'>
                            <option value=''></option>
                          </select>
                        </span>
                    </div>
                </div>


            </div>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select Student <span class='error-text'>*</span></label>
                        <span id="view_students_list">
                          <select class="form-control" id='id_student' name='id_student'>
                            <option value=''></option>
                          </select>
                        </span>
                    </div>
                </div>


            </div>   


        


            

            <div id="view_student_details"  style="display: none;">
            </div>

            <br>

            <div class="row">                    

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Select New Semester <span class='error-text'>*</span></label>
                            <select name="id_new_semester" id="id_new_semester" class="form-control">
                                <option value="">Select</option>
                                <?php
                                if (!empty($semesterList))
                                {
                                    foreach ($semesterList as $record)
                                    {?>
                                 <option value="<?php echo $record->id;  ?>">
                                    <?php echo $record->code . " - " . $record->name;?>
                                 </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                 

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Select New Program Learning Mode <span class='error-text'>*</span></label>
                            <span id="view_new_program_scheme">
                              <select class="form-control" id='id_new_program_scheme' name='id_new_program_scheme'>
                                <option value=''></option>
                              </select>
                            </span>
                        </div>
                    </div>



                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Fees <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="fee" name="fee" readonly="readonly" value="0">
                    </div>
                 </div>

            </div>
            <div class="row"> 

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Reason <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="reason" name="reason">
                    </div>
                 </div>  


            </div>
            </div>

            <div class="form-container">
                <h4 class="form-group-title">Fee Structure Details</h4> 
                <div id='view_fee'>
                </div>
            </div>


            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="list" class="btn btn-link">Cancel</a>
                </div>
            </div>
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
    </div>
</div>

<script>


    function getPreviousProgramSchemeByProgramId(id)
    {
        $.get("/records/applyChangeLearningMode/getPreviousProgramSchemeByProgramId/"+id, function(data, status){
   
        $("#view_program_scheme").html(data);
        $("#view_program_scheme").show();
    });

        $.get("/records/applyChangeLearningMode/getIntakeByProgramme/"+id, function(data, status){
   
        $("#view_program_intake").html(data);
        $("#view_program_intake").show();
    });

    }


     function getStudentByData()
     {
        var id_programme = $("#id_programme").val();
        var id_program_scheme = $("#id_program_scheme").val();
        var id_intake = $("#id_intake").val();


         var tempPR = {};
       
        tempPR['id_programme'] = $("#id_programme").val();
        tempPR['id_intake'] = $("#id_intake").val();
        tempPR['id_program_scheme'] = $("#id_program_scheme").val();

        if(id_programme != '' && id_program_scheme != '' && id_intake != '')
        {

            $.ajax(
            {
               url: '/records/applyChangeLearningMode/getStudentByData',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view_students_list").html(result);
               }
            });


            $.get("/records/applyChangeLearningMode/getProgramSchemeByProgramId/"+id_programme, function(data, status)
            {
   
                $("#view_new_program_scheme").html(data);
                $("#view_new_program_scheme").show();
            });



        }
     }









    function getStudentByProgramme(id)
    {

     $.get("/records/applyChangeLearningMode/getStudentByProgrammeId/"+id, function(data, status){
   
        $("#student").html(data);
        // $("#view_programme_details").show();
        // // $("#view_programme_details").html(data);
        // var programme_name = $("#programme_name").val();
        //  // alert(programme_name);
        // $("#programme_name").val(programme_name);

        //  var programme_code = $("#programme_code").val();
        //  alert(programme_code);
        // $("#programme_code").val(programme_code);

        //  var total_cr_hrs = $("#total_cr_hrs").val();
        // $("#total_cr_hrs").val(total_cr_hrs);

        //  var graduate_studies = $("#graduate_studies").val();
        // $("#graduate_studies").val(graduate_studies);

    });
 }



 function getStudentByStudentId(id)
 {

     $.get("/records/applyChangeLearningMode/getStudentByStudentId/"+id, function(data, status){
   
        $("#view_student_details").html(data);
        $("#view_student_details").show();
    });
 }

 function getIntakeByProgramme(id)
 {

    var id_programme = $("#id_programme").val();
    var id_new_programme = $("#id_new_programme").val();



    $.get("/records/applyChangeLearningMode/getIntakeByProgramme/"+id, function(data, status){
   
        $("#view_new_intake").html(data);
        $("#view_new_intake").show();
    });


    



 }



 function getFeeByProgrammeNIntake() {

    var id_programme = $("#id_programme").val();
    var id_intake = $("#id_intake").val();
    var currency = $("#currency").val();
    var id_program_scheme = $("#id_program_scheme").val();
    var id_new_program_scheme = $("#id_new_program_scheme").val();

    if(id_program_scheme == id_new_program_scheme)
    {
        alert('Same Programme Schemes Selection For Change Not Allowed Select Anothe Combination');

        $("#id_new_program_scheme").val('');

    }
    else
    {


        var tempPR = {};
        tempPR['id_programme'] = $("#id_programme").val();
        tempPR['id_intake'] = $("#id_intake").val();
        tempPR['id_program_scheme'] = $("#id_new_program_scheme").val();
        tempPR['currency'] = $("#currency").val();
        
            $.ajax(
            {
               url: '/records/applyChangeLearningMode/getFeeByProgrammeNIntake',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view_fee").html(result);
                var ta = $("#amount").val();
                if(ta == '0')
                {
                    alert('No Fee Structure Defined For Entered Data, Select Anothe Combination');
                    $("#fee").val('');

                }
                else
                {
                    $("#fee").val(ta);
                }
               }
            });

        }
        
    }




    $(document).ready(function()
    {
        $("#form_apply_change_programme").validate(
        {
            rules:
            {
                id_programme:
                {
                    required: true
                },
                id_student:
                {
                    required: true
                },
                id_intake:
                {
                    required: true
                },
                id_program_scheme:
                {
                    required: true
                },
                id_new_semester:
                {
                    required: true
                },
                fee:
                {
                    required: true
                },
                reason:
                {
                    required: true
                },
                id_new_program_scheme:
                {
                    required: true
                }
            },
            messages:
            {
                id_programme:
                {
                    required: "<p class='error-text'>Select Current Program</p>",
                },
                id_student:
                {
                    required: "<p class='error-text'>Select Student</p>",
                },
                id_intake:
                {
                    required: "<p class='error-text'>Select Student For Intake</p>",
                },
                id_program_scheme:
                {
                    required: "<p class='error-text'>Select New Program Scheme</p>",
                },
                id_new_intake:
                {
                    required: "<p class='error-text'>Select Changing Intake</p>",
                },
                id_new_semester:
                {
                    required: "<p class='error-text'>Select Changing Semester</p>",
                },
                fee:
                {
                    required: "<p class='error-text'>Select Changing Intake & Program For Fee Structure</p>",
                },
                reason:
                {
                    required: "<p class='error-text'>Enter Reason</p>",
                },
                id_new_program_scheme:
                {
                    required: "<p class='error-text'>Select New Program Scheme</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
<script type="text/javascript">
    $('select').select2();
</script>