<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Apply Change Branch</h3>
        </div>
        <form id="form_apply_change_programme" action="" method="post">


        <div class="form-container">
            <h4 class="form-group-title">Select Student For Change Branch</h4> 
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select Program <span class='error-text'>*</span></label>
                        <select name="id_programme" id="id_programme" class="form-control" onchange="getPreviousProgramSchemeByProgramId(this.value)">
                            <option value="">Select</option>
                            <?php
                            if (!empty($programmeList))
                            {
                                foreach ($programmeList as $record)
                                {?>
                            <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                            </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div> 


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select Current Branch <span class='error-text'>*</span></label>
                        <span id="view_previous_branch">
                          <select class="form-control" id='id_branch' name='id_branch'>
                            <option value=''></option>
                          </select>
                        </span>
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select Intake <span class='error-text'>*</span></label>
                        <span id="view_program_intake">
                          <select class="form-control" id='id_intake' name='id_intake'>
                            <option value=''></option>
                          </select>
                        </span>
                    </div>
                </div>


                


            </div>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select Student <span class='error-text'>*</span></label>
                        <span id="view_students_list">
                          <select class="form-control" id='id_student' name='id_student'>
                            <option value=''></option>
                          </select>
                        </span>
                    </div>
                </div>


            </div>   


        


            

            <div id="view_student_details"  style="display: none;">
            </div>

            <br>

            <div class="row">                    

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Select New Semester <span class='error-text'>*</span></label>
                            <select name="id_new_semester" id="id_new_semester" class="form-control">
                                <option value="">Select</option>
                                <?php
                                if (!empty($semesterList))
                                {
                                    foreach ($semesterList as $record)
                                    {?>
                                 <option value="<?php echo $record->id;  ?>">
                                    <?php echo $record->code . " - " . $record->name;?>
                                 </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Select New Branch <span class='error-text'>*</span></label>
                            <span id="view_new_branch">
                              <select class="form-control" id='id_new_branch' name='id_new_branch'>
                                <option value=''></option>
                              </select>
                            </span>
                        </div>
                    </div>




                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Reason <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="reason" name="reason">
                        </div>
                     </div>  


                    
                </div>  

                 

            



                <!-- <div class="col-sm-4">
                    <div class="form-group">
                        <label>Fees <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="fee" name="fee" readonly="readonly" value="0">
                    </div>
                 </div>

            </div>
            <div class="row">  -->

                 


            </div>


            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="list" class="btn btn-link">Cancel</a>
                </div>
            </div>
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
    </div>
</div>

<script>


    function getPreviousProgramSchemeByProgramId(id)
    {
        $.get("/records/changeBranch/getPreviousBranchByProgramId/"+id, function(data, status){
   
        $("#view_previous_branch").html(data);
        $("#view_previous_branch").show();
    });

        $.get("/records/changeBranch/getIntakeByProgramme/"+id, function(data, status){
   
        $("#view_program_intake").html(data);
        $("#view_program_intake").show();
    });

    }


     function getStudentByData()
     {
        var id_programme = $("#id_programme").val();
        var id_branch = $("#id_branch").val();
        var id_intake = $("#id_intake").val();


         var tempPR = {};
       
        tempPR['id_programme'] = $("#id_programme").val();
        tempPR['id_intake'] = $("#id_intake").val();
        tempPR['id_branch'] = $("#id_branch").val();

        if(id_programme != '' && id_branch != '' && id_intake != '')
        {

            $.ajax(
            {
               url: '/records/changeBranch/getStudentByData',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view_students_list").html(result);
               }
            });


            $.get("/records/changeBranch/getBranchesListByProgramId/"+id_programme, function(data, status)
            {
   
                $("#view_new_branch").html(data);
                $("#view_new_branch").show();
            });



        }
     }


 function getStudentByStudentId(id)
 {

     $.get("/records/changeBranch/getStudentByStudentId/"+id, function(data, status){
   
        $("#view_student_details").html(data);
        $("#view_student_details").show();
    });
 }

 function getIntakeByProgramme(id)
 {

    var id_programme = $("#id_programme").val();
    var id_new_programme = $("#id_new_programme").val();



    $.get("/records/changeBranch/getIntakeByProgramme/"+id, function(data, status){
   
        $("#view_new_intake").html(data);
        $("#view_new_intake").show();
    });


    



 }



 function getFeeByProgrammeNIntake() {

    var id_programme = $("#id_programme").val();
    var id_intake = $("#id_intake").val();
    var id_branch = $("#id_branch").val();
    var id_new_branch = $("#id_new_branch").val();

    if(id_branch == id_new_branch)
    {
        alert('Same Branch Selection For Change Not Allowed, Select Anothe Combination');

        $("#id_new_branch").val('');

    }
    else
    {


        // var tempPR = {};
        // tempPR['id_programme'] = $("#id_programme").val();
        // tempPR['id_intake'] = $("#id_intake").val();
        // tempPR['id_program_scheme'] = $("#id_new_program_scheme").val();
        //     $.ajax(
        //     {
        //        url: '/records/changeBranch/getFeeByProgrammeNIntake',
        //         type: 'POST',
        //        data:
        //        {
        //         tempData: tempPR
        //        },
        //        error: function()
        //        {
        //         alert('Something is wrong');
        //        },
        //        success: function(result)
        //        {
        //         $("#view_fee").html(result);
        //         var ta = $("#amount").val();
        //         if(ta == '0')
        //         {
        //             alert('No Fee Structure Defined For Entered Data, Select Anothe Combination');
        //             $("#fee").val('');

        //         }
        //         else
        //         {
        //             $("#fee").val(ta);
        //         }
        //        }
        //     });

        }
        
    }




    $(document).ready(function()
    {
        $("#form_apply_change_programme").validate(
        {
            rules:
            {
                id_programme:
                {
                    required: true
                },
                id_student:
                {
                    required: true
                },
                id_intake:
                {
                    required: true
                },
                id_branch:
                {
                    required: true
                },
                id_new_semester:
                {
                    required: true
                },
                fee:
                {
                    required: true
                },
                reason:
                {
                    required: true
                },
                id_new_branch:
                {
                    required: true
                }
            },
            messages:
            {
                id_programme:
                {
                    required: "<p class='error-text'>Select Current Program</p>",
                },
                id_student:
                {
                    required: "<p class='error-text'>Select Student</p>",
                },
                id_intake:
                {
                    required: "<p class='error-text'>Select Student For Intake</p>",
                },
                id_branch:
                {
                    required: "<p class='error-text'>Select Branch</p>",
                },
                id_new_intake:
                {
                    required: "<p class='error-text'>Select Changing Intake</p>",
                },
                id_new_semester:
                {
                    required: "<p class='error-text'>Select Changing Semester</p>",
                },
                fee:
                {
                    required: "<p class='error-text'>Select Changing Intake & Program For Fee Structure</p>",
                },
                reason:
                {
                    required: "<p class='error-text'>Enter Reason</p>",
                },
                id_new_branch:
                {
                    required: "<p class='error-text'>Select New Branch</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
<script type="text/javascript">
    $('select').select2();
</script>