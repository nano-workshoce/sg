<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Patient extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('patient_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('patient.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['email'] = $this->security->xss_clean($this->input->post('email'));
            $formData['phone'] = $this->security->xss_clean($this->input->post('phone'));
            $formData['register_no'] = $this->security->xss_clean($this->input->post('register_no'));
            $data['searchParam'] = $formData;

            $data['patientList'] = $this->patient_model->patientListSearch($formData);
            //echo "<Pre>"; print_r($data);exit;
            $this->global['pageTitle'] = 'Campus Management System : Patient List';
            $this->loadViews("patient_view/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('patient.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
        $id_user = $this->session->userId;
            
            if($this->input->post())
            {

            
            $salutation = $this->security->xss_clean($this->input->post('salutation'));
            $first_name = $this->security->xss_clean($this->input->post('first_name'));
            $last_name = $this->security->xss_clean($this->input->post('last_name'));
            $phone = $this->security->xss_clean($this->input->post('phone'));
            $email = $this->security->xss_clean($this->input->post('email'));
            $gender = $this->security->xss_clean($this->input->post('gender'));
            $date_of_birth = $this->security->xss_clean($this->input->post('date_of_birth'));
            $martial_status = $this->security->xss_clean($this->input->post('martial_status'));
            $mail_address1 = $this->security->xss_clean($this->input->post('mail_address1'));
            $mail_address2 = $this->security->xss_clean($this->input->post('mail_address2'));
            $mailing_country = $this->security->xss_clean($this->input->post('mailing_country'));
            $mailing_state = $this->security->xss_clean($this->input->post('mailing_state'));
            $mailing_city = $this->security->xss_clean($this->input->post('mailing_city'));
            $mailing_zipcode = $this->security->xss_clean($this->input->post('mailing_zipcode'));
            $present_address_same_as_mailing_address = $this->security->xss_clean($this->input->post('present_address_same_as_mailing_address'));
            $permanent_address1 = $this->security->xss_clean($this->input->post('permanent_address1'));
            $permanent_address2 = $this->security->xss_clean($this->input->post('permanent_address2'));
            $permanent_country = $this->security->xss_clean($this->input->post('permanent_country'));
            $permanent_state = $this->security->xss_clean($this->input->post('permanent_state'));
            $permanent_city = $this->security->xss_clean($this->input->post('permanent_city'));
            $permanent_zipcode = $this->security->xss_clean($this->input->post('permanent_zipcode'));
            // echo "p" . $id_program . "- I". $id_intake;exit();
            // echo "S" . $is_submitted;exit();


            $generated_number = $this->patient_model->generatePatientCode();
            $salutationInfo = $this->patient_model->getSalutation($salutation);


            $data = array(

                'salutation' => $salutation,
                'first_name' => $first_name,
                'last_name' => $last_name,
                'full_name' => $salutationInfo->name . ". " . $first_name." ".$last_name,
                'phone' => $phone,
                'email' => $email,
                'password' => md5($phone),
                'register_no' => $generated_number,
                'gender' => $gender,
                'date_of_birth' => date("Y-m-d", strtotime($date_of_birth)),
                'martial_status' => $martial_status,
                'present_address_same_as_mailing_address' => $present_address_same_as_mailing_address,
                'mail_address1' => $mail_address1,
                'mail_address2' => $mail_address2,
                'mailing_country' => $mailing_country,
                'mailing_state' => $mailing_state,
                'mailing_city' => $mailing_city,
                'mailing_zipcode' => $mailing_zipcode,
                'permanent_address1' => $permanent_address1,
                'permanent_address2' => $permanent_address2,
                'permanent_country' => $permanent_country,
                'permanent_state' => $permanent_state,
                'permanent_city' => $permanent_city,
                'permanent_zipcode' => $permanent_zipcode,
                'status' => 1,
                'created_by' => $id_user
            );


                // echo "<Pre>"; print_r($data);exit;
                $result = $this->patient_model->addNewPatient($data);
                redirect('/records/patient/list');
                // redirect('/patient/patient/edit/'.$result);
            }

            $data['countryList'] = $this->patient_model->countryListByStatus('1');
            $data['salutationList'] = $this->patient_model->salutationListByStatus('1');

            $this->global['pageTitle'] = 'Campus Management System : Add Patient';
            $this->loadViews("patient_view/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('patient.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/records/patient/list');
            }

            $id_user = $this->session->userId;
            
            if($this->input->post())
            {
            

            // echo "<Pre>";print_r($this->input->post());exit();


                $salutation = $this->security->xss_clean($this->input->post('salutation'));
            $first_name = $this->security->xss_clean($this->input->post('first_name'));
            $last_name = $this->security->xss_clean($this->input->post('last_name'));
            $phone = $this->security->xss_clean($this->input->post('phone'));
            $email = $this->security->xss_clean($this->input->post('email'));
            $gender = $this->security->xss_clean($this->input->post('gender'));
            $date_of_birth = $this->security->xss_clean($this->input->post('date_of_birth'));
            $martial_status = $this->security->xss_clean($this->input->post('martial_status'));
            $mail_address1 = $this->security->xss_clean($this->input->post('mail_address1'));
            $mail_address2 = $this->security->xss_clean($this->input->post('mail_address2'));
            $mailing_country = $this->security->xss_clean($this->input->post('mailing_country'));
            $mailing_state = $this->security->xss_clean($this->input->post('mailing_state'));
            $mailing_city = $this->security->xss_clean($this->input->post('mailing_city'));
            $mailing_zipcode = $this->security->xss_clean($this->input->post('mailing_zipcode'));
            $present_address_same_as_mailing_address = $this->security->xss_clean($this->input->post('present_address_same_as_mailing_address'));
            $permanent_address1 = $this->security->xss_clean($this->input->post('permanent_address1'));
            $permanent_address2 = $this->security->xss_clean($this->input->post('permanent_address2'));
            $permanent_country = $this->security->xss_clean($this->input->post('permanent_country'));
            $permanent_state = $this->security->xss_clean($this->input->post('permanent_state'));
            $permanent_city = $this->security->xss_clean($this->input->post('permanent_city'));
            $permanent_zipcode = $this->security->xss_clean($this->input->post('permanent_zipcode'));
            // echo "p" . $id_program . "- I". $id_intake;exit();
            // echo "S" . $is_submitted;exit();


            // $generated_number = $this->patient_model->generatePatientCode();
            $salutationInfo = $this->patient_model->getSalutation($salutation);


            $data = array(

                'salutation' => $salutation,
                'first_name' => $first_name,
                'last_name' => $last_name,
                'full_name' => $salutationInfo->name . ". " . $first_name." ".$last_name,
                'phone' => $phone,
                'email' => $email,
                'password' => md5($phone),
                // 'register_no' => $generated_number,
                'gender' => $gender,
                'date_of_birth' => date("Y-m-d", strtotime($date_of_birth)),
                'martial_status' => $martial_status,
                'present_address_same_as_mailing_address' => $present_address_same_as_mailing_address,
                'mail_address1' => $mail_address1,
                'mail_address2' => $mail_address2,
                'mailing_country' => $mailing_country,
                'mailing_state' => $mailing_state,
                'mailing_city' => $mailing_city,
                'mailing_zipcode' => $mailing_zipcode,
                'permanent_address1' => $permanent_address1,
                'permanent_address2' => $permanent_address2,
                'permanent_country' => $permanent_country,
                'permanent_state' => $permanent_state,
                'permanent_city' => $permanent_city,
                'permanent_zipcode' => $permanent_zipcode,
                'status' => 1,
                'created_by' => $id_user
            );
                
                $result = $this->patient_model->editPatient($data,$id);
                redirect('/records/patient/list');
            }

            $data['countryList'] = $this->patient_model->countryListByStatus('1');
            $data['salutationList'] = $this->patient_model->salutationListByStatus('1');
            $data['patient'] = $this->patient_model->getPatient($id);

            // echo "<Pre>"; print_r($data['patient']);exit;

            $this->global['pageTitle'] = 'Campus Management System : Edit Patient';
            $this->loadViews("patient_view/edit", $this->global, $data, NULL);
        }
    }



    function getStateByCountry($id_country)
    {
            $results = $this->patient_model->getStateByCountryId($id_country);

            
                 
            $table="  


            <script type='text/javascript'>
                     $('select').select2();
                 </script> 
                
         ";

            $table.="
            <select name='mailing_state' id='mailing_state' class='form-control'>
                <option value=''>Select</option>
                ";

            for($i=0;$i<count($results);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $results[$i]->id;
            $name = $results[$i]->name;
            $table.="<option value=".$id.">".$name.
                    "</option>";

            }
            $table.="

            </select>";

            echo $table;
            exit;
    }

    function saveFeeDetailData()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $inserted_id = $this->patient_model->saveFeeDetailData($tempData);
        echo "success";exit();
    }

    function saveCoordinatorDetailData()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $inserted_id = $this->patient_model->saveCoordinatorDetailData($tempData);
        echo "success";exit();
    }

    function deleteFeeDetailData($id)
    {
        $deleted = $this->patient_model->deleteFeeDetailData($id);
        echo 'success';exit;
    }

    function deleteCoordinatorDetailData($id)
    {
        $deleted = $this->patient_model->deleteCoordinatorDetailData($id);
        echo 'success';exit;
    }
}
