<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class BookingSummary extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('booking_summary_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('patient.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['email'] = $this->security->xss_clean($this->input->post('email'));
            $formData['phone'] = $this->security->xss_clean($this->input->post('phone'));
            $formData['register_no'] = $this->security->xss_clean($this->input->post('register_no'));
            $data['searchParam'] = $formData;

            $data['patientList'] = $this->booking_summary_model->patientListSearch($formData);
            //echo "<Pre>"; print_r($data);exit;
            $this->global['pageTitle'] = 'Campus Management System : Patient List';
            $this->loadViews("booking_summary/list", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('patient.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/records/patient/list');
            }

            $id_user = $this->session->userId;

            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['email'] = $this->security->xss_clean($this->input->post('email'));
            $formData['phone'] = $this->security->xss_clean($this->input->post('phone'));
            $formData['register_no'] = $this->security->xss_clean($this->input->post('register_no'));
            $formData['id_staff'] = $this->security->xss_clean($this->input->post('id_staff'));
            $formData['id_patient'] = $id;
            $formData['status'] = $this->security->xss_clean($this->input->post('status'));
            
            if($formData['status'] =='')
            {
                $formData['status'] = 1;
            }
            $data['searchParam'] = $formData;

            $data['patientBookingList'] = $this->booking_summary_model->patientBookingListSearch($formData);
            

            $data['staffList'] = $this->booking_summary_model->staffListByStatus('1');
            $data['patient'] = $this->booking_summary_model->getPatient($id);
            $data['id_patient'] = $id;

            // echo "<Pre>"; print_r($data['patient']);exit;

            $this->global['pageTitle'] = 'Campus Management System : Edit Patient';
            $this->loadViews("booking_summary/edit", $this->global, $data, NULL);
        }
    }


    function view($id = NULL)
    {
        if ($this->checkAccess('patient.view_appoimntment') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            
            if ($id == null)
            {
                redirect('/records/bookingSummary/list');
            }

            $id_user = $this->session->userId;
            $id_session = $this->session->my_session_id;

            if($this->input->post())
            {

                // echo "<Pre>";print_r($this->input->post());exit();


                $status = $this->security->xss_clean($this->input->post('status'));
                $reason = $this->security->xss_clean($this->input->post('reason'));
                $paid_amount = $this->security->xss_clean($this->input->post('paid_amount'));
                $payment_mode = $this->security->xss_clean($this->input->post('payment_mode'));
                $comments = $this->security->xss_clean($this->input->post('comments'));

                if($status == 2)
                {
                    $paid_amount = '0';
                    $payment_mode = '';
                    $comments = '';

                }
                elseif($status == 1)
                {
                    $reason = '';
                }

                // $current_time = rand(10,99) .  date('hisdmY');

                $data = array(
                    'paid_amount' => $paid_amount,
                    'completed_date' => date('Y-m-d H:i:s'),
                    'payment_mode' => $payment_mode,
                    'comments' => $comments,
                    'reason' => $reason,
                    'status' => $status,
                    'updated_by' => $id_user
                );
                // echo "<Pre>";print_r($this->input->post());exit();
                
                $id_test_booking = $this->booking_summary_model->editPatientBooking($data,$id);
                
                redirect('/records/bookingSummary/list');
            }

            $data['staffList'] = $this->booking_summary_model->staffListByStatus('1');
            $data['patientBooking'] = $this->booking_summary_model->getPatientBooking($id);
            $data['patientBookingTestDetails'] = $this->booking_summary_model->getPatientBookingTestDetails($id);
            $data['patientBookingSampleDetails'] = $this->booking_summary_model->getPatientBookingSampleDetails($id);
            $data['patient'] = $this->booking_summary_model->getPatient($data['patientBooking']->id_patient);

            // echo "<Pre>"; print_r($data['patient']);exit;

            $this->global['pageTitle'] = 'Campus Management System : Edit Patient';
            $this->loadViews("booking_summary/view", $this->global, $data, NULL);
        }
    }




    function getStateByCountry($id_country)
    {
            $results = $this->booking_summary_model->getStateByCountryId($id_country);

            
                 
            $table="  


            <script type='text/javascript'>
                     $('select').select2();
                 </script> 
                
         ";

            $table.="
            <select name='mailing_state' id='mailing_state' class='form-control'>
                <option value=''>Select</option>
                ";

            for($i=0;$i<count($results);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $results[$i]->id;
            $name = $results[$i]->name;
            $table.="<option value=".$id.">".$name.
                    "</option>";

            }
            $table.="

            </select>";

            echo $table;
            exit;
    }

    function saveFeeDetailData()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $inserted_id = $this->booking_summary_model->saveFeeDetailData($tempData);
        echo "success";exit();
    }

    function saveCoordinatorDetailData()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $inserted_id = $this->booking_summary_model->saveCoordinatorDetailData($tempData);
        echo "success";exit();
    }

    function deleteFeeDetailData($id)
    {
        $deleted = $this->booking_summary_model->deleteFeeDetailData($id);
        echo 'success';exit;
    }

    function deleteCoordinatorDetailData($id)
    {
        $deleted = $this->booking_summary_model->deleteCoordinatorDetailData($id);
        echo 'success';exit;
    }
}
