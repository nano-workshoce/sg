<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Assign_exam_center_to_semester_model extends CI_Model
{
    function assignedExamCenterList()
    {
        $this->db->select('a.*, e.name as examName, in.name as intake, s.name as semester, p.name as program');
        $this->db->from('exam_center_to_semester as a');
        $this->db->join('exam_center as e', 'a.id_exam_center = e.id');
        $this->db->join('intake as in', 'a.id_intake = in.id');
        $this->db->join('programme as p', 'a.id_programme = p.id');
        $this->db->join('semester as s', 'a.id_semester = s.id');
        $this->db->order_by("id", "DESC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();      
         return $result;
    }

    function examCenterListSearch($search)
    {
        // $date = 
        $this->db->select('*');
        $this->db->from('exam_center');
        if (!empty($search))
        {
            $likeCriteria = "(name  LIKE '%" . $search . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("id", "DESC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getAssignedExamCenterList($id)
    {
        $this->db->select('*');
        $this->db->from('exam_center_to_semester');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addExamCenterToSemester($data)
    {
        $this->db->trans_start();
        $this->db->insert('exam_center_to_semester', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editExamCenterToSemester($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('exam_center_to_semester', $data);
        return TRUE;
    }
}