<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>View Exam Registration</h3>
        </div>
         <!-- <h4>Select Student Program & Intake For Course Registration Details</h4> -->

        <form id="form_grade" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Exam Regiastration Details</h4>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Program</label>
                        <input type="text" class="form-control" id="programme" name="programme" value="<?php echo $studentData->programme_code ." - ". $studentData->programme_name?>" readonly="readonly">
                    </div>
                </div>


                <div class="col-sm-4">
                  <div class="form-group">
                  <label>Program</label>
                  <input type="text" class="form-control" id="programme" name="programme" value="<?php echo $studentData->intake_name?>" readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                  <div class="form-group">
                  <label>Exam Location</label>
                  <input type="text" class="form-control" id="location" name="location" value="<?php echo $examRegistration->location?>" readonly="readonly">
                    </div>
                </div>

              
                
            </div>


             <div class="row">

              <div class="col-sm-4">
                  <div class="form-group">
                      <label>Student Name</label>
                      <input type="text" class="form-control" id="programme" name="programme" value="<?php echo $studentData->full_name?>" readonly="readonly">
                  </div>
                </div>

             <div class="col-sm-4">
                    <div class="form-group">
                        <label>Student NRIC</label>
                        <input type="text" class="form-control" id="programme" name="programme" value="<?php echo $studentData->nric?>" readonly="readonly">
                    </div>
                </div>


                <div class="col-sm-4">
                 <div class="form-group">
                  <label>Student Email</label>
                  <input type="text" class="form-control" id="programme" name="programme" value="<?php echo $studentData->email_id?>" readonly="readonly">
                    </div>
                </div>
              </div>

            </div>


           

              </form>


            <!-- <div style="border: 1px solid;border-radius: 12px;">
            <div class="container">
                <div class="row">
                    <div class="col-sm-4">
                        <h4>Student Name : </h4>
                        <h4>Student ID : </h4>
                        <h4>Branch : </h4>
                    </div>
                    <div class="col-sm-4">
                        <h4>Intake : </h4>
                        <h4>Programme : </h4>
                        <h4>Scheme : </h4>
                    </div>
                </div>
            </div>
        </div> -->
        
        


         <br>
        <div class="custom-table">
              <div id="view"></div>
        </div>


        <h4>Courses Registered For Examination</h4>
        

      <div class="form-container">
            <h4 class="form-group-title">Course Registered Detail</h4>


         <div class="custom-table">
          <table class="table" id="list-table">
            <thead>
              <tr>
                <th>Sl. No</th>
                <th>Course Name</th>
                <th>Exam Event</th>
                <th>Exam Center</th>
                <th>Exam Center Address </th>
                <th>Min Credit Hours</th>
                <th>Pre-Requisit</th>
                <th>Status</th>
              </tr>
            </thead>
            <tbody>
              <?php
              if (!empty($courseList))
              {
                $i=1;
                foreach ($courseList as $record) {
              ?>
                  <tr>
                    <td><?php echo $i?></td>
                    <td><?php echo $record->code . " - " . $record->name?></td>
                    <td><?php echo $record->event . " - " . date('d-m-Y', strtotime($record->from_dt)) . " - " . date('g:i A', strtotime($record->from_tm))?></td>
                    <td><?php echo $record->exam_center_name?></td>
                    <td><?php echo $record->address . ", " . $record->city?></td>
                    <td><?php echo $record->min_total_cr_hrs ?></td>
                    <td><?php echo $record->pre_requisite ?></td>
                    <td><?php if( $record->is_bulk_withdraw == '0')
                      {
                        echo "Registered";
                      }
                      else
                      {
                        echo "Withdraw";
                      } 
                      // elseif($record->investment_status == '2')
                      // {
                      //   echo "Withdraw Approved";
                      // }
                      // elseif($record->investment_status == '3')
                      // {
                      //   echo "Reinvested";
                      // } 
                      ?></td>
                   
                  </tr>
              <?php
              $i++;
                }
              }
              ?>
            </tbody>
          </table>
        </div>

    </div>


            <div class="button-block clearfix">
                <div class="bttn-group">
                    <!-- <button type="submit" class="btn btn-primary btn-lg">Save</button> -->
                    <a href="../../../../list" class="btn btn-link">Back</a>
                </div>
            </div>
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script type="text/javascript">
    $('select').select2();
</script>
<script type="text/javascript">


  function getIntakes()
    {

     var tempPR = {};
        tempPR['id_programme'] = $("#id_programme").val();
            $.ajax(
            {
               url: '/registration/courseRegistration/getIntakesForView',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view_intake").html(result);
                    $("#display_intake").show();

                // if (tempPR['id_programme'] != '' && tempPR['id_intake'] == '')
                // {
                    
                // }
                // var ita = $("#invoice_total_amount").val();
                // $("#receipt_amount").val(ita);
               }
            });
    }



function viewData() {

        var id_intake = $("#id_intake").val();
        var id_programme = $("#id_programme").val();
        var id_student = $("#id_student").val();
            $.ajax(
            {
               url: '/registration/courseRegistration/viewData',
                type: 'POST',
               data:
               {
                'id_intake': id_intake,
                'id_programme': id_programme,
                'id_student': id_student
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view").html(result);
               }
            });
        
    }
    
</script>