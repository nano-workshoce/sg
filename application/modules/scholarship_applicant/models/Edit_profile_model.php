<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Edit_profile_model extends CI_Model
{
   function applicantList($applicantList)
    {
        $this->db->select('a.*, p.code as program_code, p.name as program');
        $this->db->from('scholar_applicant as a');
        $this->db->join('programme as p', 'a.id_program = p.id');
        if($applicantList['first_name']) {
            $likeCriteria = "(a.first_name  LIKE '%" . $applicantList['first_name'] . "%')";
            $this->db->where($likeCriteria);
        }
         if($applicantList['last_name']) {
            $likeCriteria = "(a.last_name  LIKE '%" . $applicantList['last_name'] . "%')";
            $this->db->where($likeCriteria);
        }
         if($applicantList['email_id']) {
            $likeCriteria = "(a.email_id  LIKE '%" . $applicantList['email_id'] . "%')";
            $this->db->where($likeCriteria);
        }
         if($applicantList['nric']) {
            $likeCriteria = "(a.nric  LIKE '%" . $applicantList['nric'] . "%')";
            $this->db->where($likeCriteria);
        }
         if($applicantList['id_program']) {
            $likeCriteria = "(a.id_program  LIKE '%" . $applicantList['id_program'] . "%')";
            $this->db->where($likeCriteria);
        }
        if($applicantList['applicant_status']) {
            $likeCriteria = "(a.applicant_status  LIKE '%" . $applicantList['applicant_status'] . "%')";
            $this->db->where($likeCriteria);
        }
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }




    function addExamDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('scholarship_examination_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getExamDetails($id)
    {
        $this->db->select('*');
        $this->db->from('scholarship_examination_details');
        $this->db->where('id_scholar_applicant', $id);
        $query = $this->db->get();
         $result = $query->result();
        return $result;
    }

     function deleteEducationDetails($id)
    {
        // echo "<Pre>";print_r($id);exit();
        $this->db->where('id', $id);
        $this->db->delete('scholarship_examination_details');
         return TRUE;
    }


    function updateFamilyDetails($data)
    {
        $id = $data['id_scholar_applicant'];
        unset($data['id_scholar_applicant']);
        $this->db->where('id', $id);
        $this->db->update('scholar_applicant_family_details', $data);
    }


    function getFamilyDetails($id)
    {
        $this->db->select('*');
        $this->db->from('scholar_applicant_family_details');
        $this->db->where('id_scholar_applicant', $id);
        $query = $this->db->get();
         $result = $query->row();
        return $result;
    }

    function getEmploymentDetails($id)
    {
        $this->db->select('*');
        $this->db->from('scholar_applicant_employment_status');
        $this->db->where('id_scholar_applicant', $id);
        $query = $this->db->get();
         $result = $query->result();
        return $result;
    }


    function addEmploymentDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('scholar_applicant_employment_status', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }
    
    function deleteEmploymentDetails($id)
    {
        // echo "<Pre>";print_r($id);exit();
        $this->db->where('id', $id);
        $this->db->delete('scholar_applicant_employment_status');
         return TRUE;
    }

    function updateStudentData($data)
    {
        $id = $data['id_scholar_applicant'];
        unset($data['id_scholar_applicant']);
        unset($data['nationality_type']);
        $this->db->where('id', $id);
        $this->db->update('scholar_applicant', $data);
        return TRUE;

    }




    

    




    
    function programListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('scholarship_programme');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    
    function nationalityList()
    {
        $this->db->select('*');
        $this->db->from('nationality');
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function programList()
    {
        $this->db->select('*');
        $this->db->from('scholarship_programme');
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    

    function countryList()
    {
        $this->db->select('*');
        $this->db->from('scholarship_country');
        $this->db->where('status', '1');
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function stateList()
    {
        $this->db->select('*');
        $this->db->from('scholarship_state');
        $this->db->where('status', '1');
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function getStudentDetails($id)
    {
        $this->db->select('*');
        $this->db->from('scholar_applicant');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function raceListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('scholarship_race_setup');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }


    function religionListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('scholarship_religion_setup');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }




    // function checkDuplicateStudent($data,$id)
    // {
    //     $this->db->select('id, email_id, full_name');
    //     $this->db->from('student');
    //     $this->db->where('email_id', $data['email_id']);
    //     $this->db->or_where('phone', $data['phone']);
    //     $this->db->or_where('nric', $data['nric']);
    //     $this->db->where('id !=', $id);
    //     $query = $this->db->get();
    //     return $query->row();
    // }


    
}