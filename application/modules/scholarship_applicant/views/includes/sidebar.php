            <div class="sidebar">
                <div class="user-profile clearfix">
                    <a href="/scholarship_applicant/profile" class="user-profile-link">
                        <span>
                            <img src="<?php echo BASE_PATH; ?>assets/img/user_profile.jpg">
                        </span> <?php echo $scholar_applicant_name; ?>
                    </a>
                    <div class="dropdown">
                        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
                            <!-- <li><a href="/sponser/user/profile">Edit Profile</a></li> -->
                            <li><a href="/scholarship_applicant/profile/logout">Logout</a></li>
                        </ul>
                    </div>
                </div>
                <div class="sidebar-nav">
                    <!-- <h4>Profile</h4>
                    <ul>
                        <li><a href="/scholarship_applicant/editProfile/edit">Edit Application</a></li>
                    </ul> -->
                    <h4>Scholarship</h4>
                    <ul>
                        <li><a href="/scholarship_applicant/scholarshipApplication/schemeList">Apply Scholarship</a></li>
                        <li><a href="/scholarship_applicant/scholarshipApplication/list">View Applications</a></li>
                    </ul>
                </div>
            </div>