<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Examination extends BaseController
{
    public function __construct()
    {
        // $test = new BaseController();
        // $test->isStudentLoggedIn();
        parent::__construct();
        $this->load->model('examination_model');
        $this->isStudentLoggedIn();
    }

    function timeTableSlip()
    {
        $id = $this->session->id_student;
        // if ($this->checkAccess('course_registration.list') == 1)
        // {
        //     $this->loadAccessRestricted();
        // }
        // else
        // { 
            // $data['intakeList'] = $this->course_registration_model->intakeList();
            // $data['programList'] = $this->course_registration_model->programList();



                $formData['first_name'] = $this->security->xss_clean($this->input->post('first_name'));
                $formData['email_id'] = $this->security->xss_clean($this->input->post('email_id'));
                $formData['nric'] = $this->security->xss_clean($this->input->post('nric'));
                $formData['id_program'] = $this->security->xss_clean($this->input->post('id_program'));
                $formData['id_intake'] = $this->security->xss_clean($this->input->post('id_intake'));
 

            $data['searchParam'] = $formData;

            $data['studentDetails'] = $this->examination_model->getStudentByStudentId($id);
            // $data['courseRegistrationList'] = $this->course_registration_model->courseRegistrationList($formData);

            // echo "<Pre>";print_r($data['studentDetails']);exit();
            $this->global['pageTitle'] = 'Student Portal : Examination & Slip';
            $this->loadViews("examination/time_table_slip", $this->global, $data, NULL);
        // }
    }

    function latestExamResults()
    {
        $id = $this->session->id_student;

        $data['studentDetails'] = $this->examination_model->getStudentByStudentId($id);
        $data['resultList'] = $this->examination_model->semesterResultList($id);

        // echo "<Pre>";print_r($data);exit();

        $this->global['pageTitle'] = 'Student Portal : Latest Exam Results';
        $this->loadViews("examination/latest_exam_result", $this->global, $data, NULL);
    }

    function previewMarks($id = NULL)
    {
        $id_student = $this->session->id_student;

        if ($id == null)
        {
            redirect('/student/examination/latestExamResults');
        }

        // $data['studentDetails'] = $this->examination_model->getStudentByStudentId($id);
        $data['marksEntry'] = $this->examination_model->getMarksEntry($id);
        $data['marksEntryDetails'] = $this->examination_model->getMarksEntryDetailsByMasterId($id);

        // echo "<Pre>";print_r($data);exit();

        $this->global['pageTitle'] = 'Student Portal : View Marks Entry';
        $this->loadViews("examination/view_student_marks", $this->global, $data, NULL);
    }

    function partialTranscript()
    {
        $id = $this->session->id_student;

                $formData['first_name'] = $this->security->xss_clean($this->input->post('first_name'));
                $formData['email_id'] = $this->security->xss_clean($this->input->post('email_id'));
                $formData['nric'] = $this->security->xss_clean($this->input->post('nric'));
                $formData['id_program'] = $this->security->xss_clean($this->input->post('id_program'));
                $formData['id_intake'] = $this->security->xss_clean($this->input->post('id_intake'));
 

            $data['searchParam'] = $formData;

            $data['studentDetails'] = $this->examination_model->getStudentByStudentId($id);
            // echo "<Pre>";print_r($data['courseRegistrationList']);exit();

            $this->global['pageTitle'] = 'Student Portal : Partial Transcript';
            $this->loadViews("examination/partial_transcript", $this->global, $data, NULL);
    }

    function resitApplication()
    {
        $id = $this->session->id_student;

        $formData['first_name'] = $this->security->xss_clean($this->input->post('first_name'));
        $formData['email_id'] = $this->security->xss_clean($this->input->post('email_id'));
        $formData['nric'] = $this->security->xss_clean($this->input->post('nric'));
        $formData['id_program'] = $this->security->xss_clean($this->input->post('id_program'));
        $formData['id_intake'] = $this->security->xss_clean($this->input->post('id_intake'));

            $data['searchParam'] = $formData;

            $data['studentDetails'] = $this->examination_model->getStudentByStudentId($id);

            // echo "<Pre>";print_r($data['courseRegistrationList']);exit();
            $this->global['pageTitle'] = 'Student Portal : Resit Application';
            $this->loadViews("examination/resit_application", $this->global, $data, NULL);
        // }
    }

    function remarkingApplication()
    {
        $id = $this->session->id_student;

        $data['studentDetails'] = $this->examination_model->getStudentByStudentId($id);
        $data['resultList'] = $this->examination_model->semesterResultList($id);

        // echo "<Pre>";print_r($data['courseRegistrationList']);exit();
        $this->global['pageTitle'] = 'Student Portal : Resit Application';
        $this->loadViews("examination/remarking_application", $this->global, $data, NULL);  
    }

    function reMark($id = NULL)
    {
        $id_student = $this->session->id_student;

        if ($id == null)
        {
            redirect('/student/examination/latestExamResults');
        }

        // $data['studentDetails'] = $this->examination_model->getStudentByStudentId($id);
        $data['marksEntry'] = $this->examination_model->getMarksEntry($id);
        $data['marksEntryDetails'] = $this->examination_model->getMarksEntryDetailsByMasterId($id);

        // echo "<Pre>";print_r($data);exit();

        $this->global['pageTitle'] = 'Student Portal : View Marks Entry';
        $this->loadViews("examination/remarking", $this->global, $data, NULL);
    }
    
    function add()
    {
        $id_student = $this->session->id_student;
        $id_intake = $this->session->id_intake;
        $id_program = $this->session->id_program;

        if($this->input->post())
        {
            $id_course = $this->security->xss_clean($this->input->post('id_course'));

            $data = array(
                'id_course' => $id_course,
                'id_intake' => $id_intake,
                'id_programme' => $id_program,
                'id_student' => $id_student,
            );
                 // echo "<Pre>";print_r($data);exit();
            $insert_id = $this->course_registration_model->addCoureRegistration($data);
            redirect($_SERVER['HTTP_REFERER']);
        }

        $data['courseList'] = $this->course_registration_model->getPerogramLandscape($id_intake,$id_program);
        $data['getStudentData'] = $this->edit_profile_model->getStudentData($id_student);
        $data['courseRegisteredList'] = $this->course_registration_model->getCourseRegisteredList($id_student);
        // echo "<Pre>";print_r($data['courseList']);exit();

        $this->global['pageTitle'] = 'Student Portal : Add Course Registration';
        $this->loadViews("course_registration/add", $this->global, $data, NULL);
    }

     function deleteCourseRegistration($id_course_registration)
    {
        $student_list_data = $this->course_registration_model->deleteCourseRegistration($id_course_registration);
        echo $id_course_registration;
        exit();
    }
}

