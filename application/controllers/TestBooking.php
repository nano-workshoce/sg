<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';


class TestBooking extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('test_booking_model');
    }

    public function index()
    {   
        $uniqueId = rand(0000000000,9999999999);
        $this->session->set_userdata("id_session_patient", md5($uniqueId));
        $this->booking();
    }

    public function booking()
    {
        $formData = $this->input->post();
        $id_session_patient = $this->session->id_session_patient;
        // echo "<Pre>"; print_r($is_session_patient);exit;
        $domain = $this->getDomainName();

        if($formData)
        {
            // echo "<Pre>"; print_r($_POST);exit;
            $id_patient = 0;
            $register_no = '';
            $patient_phone = '';

            $description = $this->security->xss_clean($this->input->post('description'));
            $salutation = $this->security->xss_clean($this->input->post('salutation'));
            $first_name = $this->security->xss_clean($this->input->post('first_name'));
            $last_name = $this->security->xss_clean($this->input->post('last_name'));
            $gender = $this->security->xss_clean($this->input->post('gender'));
            $date_of_birth = $this->security->xss_clean($this->input->post('date_of_birth'));
            $age = $this->security->xss_clean($this->input->post('age'));
            $age_duration = $this->security->xss_clean($this->input->post('age_duration'));
            $phone = $this->security->xss_clean($this->input->post('phone'));
            $alternative_phone = $this->security->xss_clean($this->input->post('alternative_phone'));
            $email = $this->security->xss_clean($this->input->post('email'));
            $mail_address1 = $this->security->xss_clean($this->input->post('mail_address1'));
            $mail_address2 = $this->security->xss_clean($this->input->post('mail_address2'));
            $mailing_city = $this->security->xss_clean($this->input->post('mailing_city'));

            $martial_status = $this->security->xss_clean($this->input->post('martial_status'));
            $mailing_country = $this->security->xss_clean($this->input->post('mailing_country'));
            $mailing_state = $this->security->xss_clean($this->input->post('mailing_state'));
            $mailing_zipcode = $this->security->xss_clean($this->input->post('mailing_zipcode'));
            $present_address_same_as_mailing_address = $this->security->xss_clean($this->input->post('present_address_same_as_mailing_address'));
            $permanent_address1 = $this->security->xss_clean($this->input->post('permanent_address1'));
            $permanent_address2 = $this->security->xss_clean($this->input->post('permanent_address2'));
            $permanent_country = $this->security->xss_clean($this->input->post('permanent_country'));
            $permanent_state = $this->security->xss_clean($this->input->post('permanent_state'));
            $permanent_city = $this->security->xss_clean($this->input->post('permanent_city'));
            $permanent_zipcode = $this->security->xss_clean($this->input->post('permanent_zipcode'));

            $duplicate_check['email'] = $email;
            $duplicate_check['phone'] = $phone;

            if($date_of_birth)
            {
                $date_of_birth = date("Y-m-d", strtotime($date_of_birth));
            }
            else
            {
                $date_of_birth = '';
            }

            $patient = $this->test_booking_model->getPatientByEmailRPhone($duplicate_check);
            if($patient)
            {
                $id_patient = $patient->id;
                $register_no = $patient->register_no;
                $patient_phone = $patient->phone;
            }
            else
            {
                $generated_number = $this->test_booking_model->generatePatientCode();
                $salutationInfo = $this->test_booking_model->getSalutation($salutation);

                $data = array(
                    'salutation' => $salutation,
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'full_name' => $salutationInfo->name . ". " . $first_name." ".$last_name,
                    'phone' => $phone,
                    'alternative_phone' => $alternative_phone,
                    'email' => $email,
                    'password' => md5($phone),
                    'register_no' => $generated_number,
                    'gender' => $gender,
                    'date_of_birth' => $date_of_birth,
                    'age' => $age,
                    'age_duration' => $age_duration,
                    'martial_status' => $martial_status,
                    'present_address_same_as_mailing_address' => $present_address_same_as_mailing_address,
                    'mail_address1' => $mail_address1,
                    'mail_address2' => $mail_address2,
                    'mailing_country' => $mailing_country,
                    'mailing_state' => $mailing_state,
                    'mailing_city' => $mailing_city,
                    'mailing_zipcode' => $mailing_zipcode,
                    'permanent_address1' => $permanent_address1,
                    'permanent_address2' => $permanent_address2,
                    'permanent_country' => $permanent_country,
                    'permanent_state' => $permanent_state,
                    'permanent_city' => $permanent_city,
                    'permanent_zipcode' => $permanent_zipcode,
                    'status' => 1,
                    'created_by' => 0
                );

                // echo "<Pre>"; print_r($data);exit;
                $id_patient = $this->test_booking_model->addNewPatient($data);
                $register_no = $generated_number;
                $patient_phone = $phone;
            }

            if($id_patient > 0)
            {
                $current_time = rand(10,99) .  date('hisdmY');

                $data = array(
                    'id_patient' => $id_patient,
                    'description' => $description,
                    'reference_number' => $current_time,
                    'added_by_patient' => $id_patient,
                    'status' => 0,
                    'created_by' => 0
                );
                
                $id_test_booking = $this->test_booking_model->addNewPatientBooking($data);

                $added = $this->test_booking_model->moveTempToDetails($id_session_patient,$id_test_booking);
            }

            redirect('testBooking/bookingConfirmed/'. $register_no . '/' . $patient_phone . '/' . $current_time);
        }
        else
        {   
            $deleted = $this->test_booking_model->deleteTempBookingPatientDetailsBySessionId($id_session_patient);
        }

        $data['id_session_patient'] = $id_session_patient;
        $data['testList'] = $this->test_booking_model->getTestListByStatus('1');
        $data['salutationList'] = $this->test_booking_model->salutationListByStatus('1');
        // echo "<Pre>"; print_r($data['testList']);exit;

        $this->global['pageTitle'] = 'SG Lab : Book a Lab Test Online';
        $this->loadBookingViews("test_booking",$this->global,$data);
    }

    function bookingConfirmed($patient_register_no = NULL,$patient_phone = NULL,$booking_reference_no = NULL)
    {
        $data['patient_register_no'] = $patient_register_no;
        $data['patient_phone'] = $patient_phone;
        $data['booking_reference_no'] = $booking_reference_no;
        // echo "<Pre>"; print_r($data['testList']);exit;

        $this->global['pageTitle'] = 'SG Lab: Booking Confirmed';
        $this->loadOnlyView("test_booking_confirmed",$data);        
    }

    function getAgeCalculation($dob)
    {
        $date1 = date('Y-m-d', strtotime($dob));
        $date2 = date("Y-m-d");

        $diff = $this->getDatesDifference($date1,$date2);
        if($diff)
        {
            echo $diff;exit;
        }
        else
        {
            echo $date1;
        }

        // $diff=date_diff($dob,$date);

        // $date1=date_create(date('Y-m-d', strtotime($dob)));
        // $date2=date_create($date = date("Y-m-d"));
        // $diff=date_diff($date1,$date2);
        // $diff_days = $diff->format("%R%a days");
        // echo $diff_days;exit;
    }


    function addTempBookingPatientDetails()
    {        
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        // echo "<Pre>"; print_r($tempData);exit();
        $inserted_id = $this->test_booking_model->saveTempBookingPatientsDetail($tempData);

        $data = $this->displayTempData();

        print_r($data);exit();
    }

    function displayTempData()
    {
        $id_session_patient = $this->session->id_session_patient;

        $temp_details = $this->test_booking_model->getTempBookingPatientDetailsBySessionId($id_session_patient);

        // echo "<Pre>";print_r($temp_details);exit;

        if(!empty($temp_details))
        {
            
        $table ="
        <div class='custom-table'>
        <h5>    Patient Details</h5>
        <table  class='table' id='list-table'>
                <thead>
                  <tr>
                    <th>Sl. No</th>
                    <th>Full Name</th>
                    <th>Gender</th>
                    <th>Age</th>
                    <th class='text-center'>Action</th>
                    </tr>
                </thead>";
                    $total_detail = 0;
                    for($i=0;$i<count($temp_details);$i++)
                    {
                        $id = $temp_details[$i]->id;
                        $full_name = $temp_details[$i]->full_name;
                        $age = $temp_details[$i]->age;
                        $gender = $temp_details[$i]->gender;



                        $j=$i+1;

                        $table .= "
                        <tbody>
                        <tr>
                            <td>$j</td>
                            <td>$full_name</td>
                            <td>$gender</td>
                            <td>$age</td>
                            <td class='text-center'>
                                <a class='btn btn-sm btn-edit' onclick='deleteTempBookingPatientDetailsById($id)'>Delete</a>
                            <td>
                        </tr>";
                                // <span onclick='deleteTempData($id)'>Delete</a>
                    }

                     $table .= "
                    </tbody>
                    </table>
        </div>
        <br>";
        }
        else
        {
            $table = "";
        }

        return $table;
    }

    function deleteTempBookingPatientDetailsById($id)
    {
        $deleted = $this->test_booking_model->deleteTempBookingPatientDetailsById($id);
        $data = $this->displayTempData();
        print_r($data);exit();
    }
}

?>