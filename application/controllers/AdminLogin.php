<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class AdminLogin extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('login_model');
    }

    public function index()
    {
        $base_url = $_SERVER['HTTP_HOST'];
        $this->checkLoggedIn();
    }


    function checkLoggedIn()
    {
        $domain = $this->getDomainName();
        $isLoggedIn = $this->session->userdata('isLoggedIn');
        
        if(!isset($isAdminProfileLoggedIn) || $isAdminProfileLoggedIn != TRUE)
        {
            $this->loginMe();
        }
        else
        {
            echo 'Login';exit();
            redirect('/profile/dashboard/index');
        }
    }

    public function loginMe()
    {

        if($this->input->post())
        {
            // echo '<Pre>';print_r($_POST);exit();

            $this->load->library('form_validation');
            
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email|max_length[128]|trim');
            $this->form_validation->set_rules('password', 'Password', 'required|max_length[32]');
            
            if($this->form_validation->run() == FALSE)
            {
                $this->index();
            }
            else
            {

                // echo '<Pre>';print_r($_POST);exit();

                $email = strtolower($this->security->xss_clean($this->input->post('email')));
                $password = $this->input->post('password');
                
                $result = $this->login_model->loginMe($email, md5($password));
                
                // echo '<Pre>';print_r($result);exit();
                
                if(!empty($result))
                {
                    $lastLogin = $this->login_model->lastLoginInfo($result->userId);

                    if($lastLogin)
                    {
                        $last_login = $lastLogin->createdDtm;
                    }
                    else
                    {
                        $last_login = date('Y-m-d h:i:s');
                    }

                    $sessionArray = array('userId'=>$result->userId,                
                                            'role'=>$result->roleId,
                                            'roleText'=>$result->role,
                                            'name'=>$result->name,
                                            'lastLogin'=> $last_login,
                                            'isAdminProfileLoggedIn' => TRUE
                                    );

                    $this->session->set_userdata($sessionArray);

                    unset($sessionArray['userId'], $sessionArray['isAdminProfileLoggedIn'], $sessionArray['lastLogin']);

                    $loginInfo = array("userId"=>$result->userId, "sessionData" => json_encode($sessionArray), "machineIp"=>$_SERVER['REMOTE_ADDR'], "userAgent"=>getBrowserAgent(), "agentString"=>$this->agent->agent_string(), "platform"=>$this->agent->platform());

                    $uniqueId = rand(0000000000,9999999999);
                    $this->session->set_userdata("my_profile_session_id", md5($uniqueId));


                    $this->login_model->lastLogin($loginInfo);

                    // echo 'Logged In';exit();
                    redirect('/profile/dashboard/index');
                }
            }

        }
        
        $this->load->view('login_admin_profile');
    }
}

?>