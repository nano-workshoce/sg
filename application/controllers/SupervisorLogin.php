<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class : Login (LoginController)
 * Login class to control to authenticate user credentials and starts user's session.
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class SupervisorLogin extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('supervisor_login_model');
    }

    /**
     * Index Page for this controller.
     */
    public function index()
    {
        $this->checkSupervisorLoggedIn();
    }
    

    function checkSupervisorLoggedIn()
    {
        $isSupervisorLoggedIn = $this->session->userdata('isSupervisorLoggedIn');
        
        if(!isset($isSupervisorLoggedIn) || $isSupervisorLoggedIn != TRUE)
        {
            $this->supervisorLogin();
        }
        else
        {
            redirect('supervisor/profile');
        }
    }


    public function supervisorLogin()
    {
        if($this->input->post())
        {
            $formData = $this->input->post();
            // echo "<Pre>"; print_r($formData);exit;
            $domain = $this->getDomainName();

            $this->load->library('form_validation');
            
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email|max_length[128]|trim');
            $this->form_validation->set_rules('password', 'Password', 'required|max_length[32]');
            
            if($this->form_validation->run() == FALSE)
            {
                $this->index();
            }
            else
            {
                $email = strtolower($this->security->xss_clean($this->input->post('email')));
                $password = $this->input->post('password');
                
                
                $result = $this->supervisor_login_model->loginSupervisor($email, $password);

            // echo "<Pre>"; print_r($result);exit;

                
                if(!empty($result))
                {
                    // if($result->applicant_status == 'Graduated')
                    // {
                    //     echo "For Graduated Supervisors, Supervisor Portal is No More Active";exit();
                    // }
                    $lastLogin = $this->supervisor_login_model->supervisorLastLoginInfo($result->id);
            // echo "<Pre>"; print_r($lastLogin);exit;

                    if($lastLogin == '')
                    {
                        $supervisor_login = date('Y-m-d h:i:s');
                    }
                    else
                    {
                        $supervisor_login = $lastLogin->created_dt_tm;
                    }

                    $sessionArray = array('id_supervisor'=>$result->id,                    
                                            'supervisor_name'=>$result->full_name,
                                            'email_id'=>$result->email,
                                            'supervisor_last_login'=> $supervisor_login,
                                            'isSupervisorLoggedIn' => TRUE
                                    );
            // echo "<Pre>";print_r($sessionArray);exit();

                    $this->session->set_userdata($sessionArray);

                    unset($sessionArray['id_supervisor'], $sessionArray['isSupervisorLoggedIn'], $sessionArray['supervisor_last_login']);

                    $loginInfo = array("id_supervisor"=>$result->id, "session_data" => json_encode($sessionArray), "machine_ip"=>$_SERVER['REMOTE_ADDR'], "user_agent"=>getBrowserAgent(), "agent_string"=>$this->agent->agent_string(), "platform"=>$this->agent->platform());

                    $uniqueId = rand(0000000000,9999999999);
                    $this->session->set_userdata("my_supervisor_session_id", md5($uniqueId));


                    $this->supervisor_login_model->addSupervisorLastLogin($loginInfo);

                    // echo "Login";exit();
                    // echo md5($uniqueId);exit();
                    redirect('/supervisor/profile');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Email or password mismatch');
                    // $this->index();
                }
            }

        }
        
        $this->load->view('supervisor_login');
    }
}

?>