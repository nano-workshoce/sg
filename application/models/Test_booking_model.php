<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Test_booking_model extends CI_Model
{
    function getTestListByStatus($status)
    {
        $this->db->select('t.*');
        $this->db->from('test as t');
        $this->db->where('t.status', $status);
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function salutationListByStatus($status)
    {
    	$this->db->select('s.*');
        $this->db->from('salutation_setup as s');
        $this->db->where('s.status', $status);
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function saveTempBookingPatientsDetail($data)
    {
        $this->db->trans_start();
        $this->db->insert('temp_test_booking_patients', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getTempBookingPatientDetailsBySessionId($id_session)
    {
        $this->db->select('*');
        $this->db->from('temp_test_booking_patients');
        $this->db->where('id_session', $id_session);
        $query = $this->db->get();
        return $query->result();
    }

    function deleteTempBookingPatientDetailsBySessionId($id_session)
    {
       $this->db->where('id_session', $id_session);
       $this->db->delete('temp_test_booking_patients');
       return TRUE;
    }

    function deleteTempBookingPatientDetailsById($id)
    {
       $this->db->where('id', $id);
       $this->db->delete('temp_test_booking_patients');
       return TRUE;
    }

    function getPatientByEmailRPhone($data)
    {
        $this->db->select('p.*');
        $this->db->from('patient as p');
        // $this->db->join('state as s', 'sp.id_state = s.id');
        // $this->db->join('country as c', 'sp.id_country = c.id');
        if ($data['phone'] != '')
        {
            $this->db->where('p.phone', $data['phone']);
        }
        if ($data['email'] != '')
        {
            $this->db->where('p.email', $data['email']);
        }
        $this->db->order_by("p.id", "ASC");
         $query = $this->db->get();
         $result = $query->row();
         return $result;;
    }

    function addNewPatient($data)
    {
        $this->db->trans_start();
        $this->db->insert('patient', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function generatePatientCode()
    {
        $year = date('y');
        $Year = date('Y');

            $this->db->select('*');
            $this->db->from('patient');
            $this->db->order_by("id", "desc");
            $query = $this->db->get();
            $result = $query->num_rows();

     
            $count= $result + 1;

           $generated_number = "SG" .(sprintf("%'06d", $count)) . $Year;
           // echo "<Pre>";print_r($generated_number);exit();
           return $generated_number;
    }

    function getSalutation($id)
    {
        $this->db->select('a.*');
        $this->db->from('salutation_setup as a');
        $this->db->where('id', $id);
         $query = $this->db->get();
         $result = $query->row();  
         return $result;
    }

    function addNewPatientBooking($data)
    {
        $this->db->trans_start();
        $this->db->insert('test_booking', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function moveTempToDetails($id_session,$id_test_booking)
    {
        $temp_details = $this->getTempBookingPatientDetailsBySessionId($id_session); 

        foreach ($temp_details as $detail)
        {
            unset($detail->id);
            unset($detail->id_session);
            $detail->id_test_booking = $id_test_booking;

            $added = $this->saveBookingPatientsDetail($detail);
        }

        $deleted = $this->deleteTempBookingPatientDetailsBySessionId($id_session);
        return $deleted;
    }

    function saveBookingPatientsDetail($data)
    {
        $this->db->trans_start();
        $this->db->insert('test_booking_patients', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }
}
?>