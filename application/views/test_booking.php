
<div class="py-5 bg-light-v2">
  <div class="container">
   <div class="row align-items-center">
     <div class="col-md-6">
       <h2>Register</h2>
     </div>
     <div class="col-md-6">
      <ol class="breadcrumb justify-content-md-end bg-transparent">  
        <li class="breadcrumb-item">
          <a href="#">Home</a>
        </li> 
        <li class="breadcrumb-item">
          <a href="#"> Booking</a>
        </li>
       
      </ol>
     </div>
   </div>
  </div> 
</div>





<section class="padding-y-10">
  <div class="container">

    <div class="row">

        <div class="col-md-12 order-md-1">
          <h3><b><u>Booking Details</u></b></h3>
          <!-- <h4 class="mb-3">Booking Details</h4> -->


      <form id="form_main" method="POST" action="">


            <div class="row">


              <div class="col-sm-4">
                  <div class="form-group">
                      <label>Mr. | Ms. <span class='error-text'>*</span></label>
                      <select name="salutation" id="salutation" class="form-control" required>
                          <option value="">Select</option>
                          <?php
                          if (!empty($salutationList))
                          {
                              foreach ($salutationList as $record)
                              {?>
                           <option value="<?php echo $record->id;  ?>">
                              <?php echo $record->name;?>
                           </option>
                          <?php
                              }
                          }
                          ?>
                      </select>
                  </div>
              </div>


              <div class="col-sm-4">
                <div class="form-group">
                    <label>First Name <span class='error-text'>*</span></label>
                    <input type="text" class="form-control" id="first_name" name="first_name" placeholder="Parameshwarappa">
                </div>
              </div>


              <div class="col-sm-4">
                <div class="form-group">
                    <label>Last Name </label>
                    <input type="text" class="form-control" id="last_name" placeholder="DK" name="last_name">
                </div>
              </div>

            </div>


            <div class="row">

              <div class="col-sm-4">
                  <div class="form-group">
                      <label>Gender <span class='error-text'>*</span></label>
                      <select name="gender" id="gender" class="form-control">
                          <option value="">Select</option>
                           <option value="Male">MALE</option>
                           <option value="Female">FEMALE</option>
                           <option value="Other">OTHER</option>
                      </select>
                  </div>
              </div>


              <div class="col-sm-4">
                 <div class="form-group">
                    <label>Date Of Birth </label>
                    <input type="date" class="form-control datepicker" id="date_of_birth" name="date_of_birth" autocomplete="off">
                    <!-- onblur="getAge()" -->
                 </div>
              </div>

              
              <!-- <div class="col-sm-4">
                <div class="form-group">
                    <label>Age <span class='error-text'>*</span></label>
                    <input type="text" class="form-control" id="age" name="age" required readonly>
                </div>
              </div> -->

              <div class="col-sm-4">
                  <label>Age <span class='error-text'>*</span></label>
                  <div class="row">
                     <div class="col-sm-6">
                      <input type="number" class="form-control" id="age" name="age">
                     </div>
                     <div class="col-sm-6">
                        <select name="age_duration" id="age_duration" class="form-control">
                           <option value="">Select</option>
                           <option value="Years">Years</option>
                           <option value="Months">Months</option>
                           <option value="Weeks">Weeks</option>
                           <option value="Days">Days</option>
                        </select>
                     </div>
                  </div>
               </div>


            </div>




            <div class="row">

              <div class="col-sm-4">
                <div class="form-group">
                    <label>Phone Number <span class='error-text'>*</span></label>
                    <input type="number" class="form-control" id="phone" placeholder="8888888888" name="phone" required>
                </div>
              </div>



              <div class="col-sm-4">
                <div class="form-group">
                    <label>Alternate Mobile </label>
                    <input type="number" class="form-control" id="alternative_phone" placeholder="8888888888" name="alternative_phone">
                </div>
              </div>


              <div class="col-sm-4">
                <div class="form-group">
                    <label>Email </label>
                    <input type="email" class="form-control" id="email" placeholder="parameshwarappadk@gmail.com" name="email">
                </div>
              </div>

            </div>

            

            <div class="row">

              <div class="col-sm-4">
                <div class="form-group">
                  <label for="mail_address1">Address <span class='error-text'>*</span></label>
                  <input type="text" class="form-control" id="mail_address1" name="mail_address1" placeholder="No. 88, VK Apartmenrts, 3rd Main">
                </div>
              </div>


              <div class="col-sm-4">
                <div class="form-group">
                  <label for="mail_address2">Address Line 2 / Landmark</label>
                  <input type="text" class="form-control" id="mail_address2" name="mail_address2" placeholder="2nd Stage, Basaveshwara Nagara, Bengaluru">
                </div>
              </div>


              <div class="col-sm-4">
                <div class="form-group">
                  <label for="address">Pincode <span class='error-text'>*</span></label>
                  <input type="number" class="form-control" id="mailing_zipcode" name="mailing_zipcode" placeholder="560088">
                </div>
              </div>

            </div>



            <div class="row">
              
              <div class="col-sm-4">
                  <div class="form-group">
                      <label>City <span class='error-text'>*</span></label>
                      <select name="mailing_city" id="mailing_city" class="form-control">
                           <option value="Bengaluru">Bengaluru</option>
                           <!-- <option value="Dhoddabalapura">Dhoddabalapura</option>
                           <option value="Tumakuru">Tumakuru</option> -->
                      </select>
                  </div>
              </div>

              <div class="col-sm-4">
                <div class="form-group">
                    <label>Test Description <span class='error-text'>*</span></label>
                    <input type="text" class="form-control" id="description" name="description">
                </div>
              </div>


            </div>

            <!-- <div class="row">
            
              <div class="col-sm-12">
                  <div class="form-group">
                      <label>Test Description <span class='error-text'>*</span></label>
                     <textarea type="text" class="form-control" id="description" name="description"></textarea>
                  </div>
               </div>

            </div>
 -->
          <h6>Note : </h6>
          <p>1.If Bookings With Non Existed Phone No. / Incorrect Address Are Cancelled Automatically</p>


          <hr class="mb-4">
          <button type="button" onclick="validateMainForm()" class="btn btn-primary btn-lg btn-block">Book Appointment</button>

      </form>



    <form id="form_details" method="post">

        <br>

        <h6>Note : </h6>
        <p>1. Want To Book Test For More Than One Patient's</p>
        <p>2. Enter Patient's Details Below</p>
        <h4><b><u>Patient Booking Details</u></b></h4>


         <div class="row">

          <div class="col-sm-3">
            <div class="form-group">
                <label>Name <span class='error-text'>*</span></label>
                <input type="text" class="form-control" id="full_name" name="full_name" required>
            </div>
          </div>

          <div class="col-sm-3">
              <div class="form-group">
                  <label>Gender <span class='error-text'>*</span></label>
                  <select name="full_gender" id="full_gender" class="form-control" required>
                      <option value="">Select</option>
                       <option value="Male">MALE</option>
                       <option value="Female">FEMALE</option>
                       <option value="Other">OTHER</option>
                  </select>
              </div>
          </div>

          
          <div class="col-sm-3">
            <div class="form-group">
                <label>Age <span class='error-text'>*</span></label>
                <input type="number" class="form-control" id="full_age" name="full_age" required>
            </div>
          </div>


          <div class="col-sm-3">
            <div class="form-group">
              <br>
              <button type="button" onclick="addTempBookingPatientDetails()" class="btn btn-primary">Save</button>  
            </div>
          </div>

        </div>

        <br>

        <div class="row" id="view_temp_details">
        </div>

    </form>



            

        </div>
    </div>
  </div> <!-- END container-->


<footer class="site-footer">
   <div class="footer-bottom bg-black-0_9 py-5 text-center">
    <div class="container">
      <p class="text-white-0_5 mb-0">&copy; 2014. All rights reserved</p>
    </div>
  </div>  <!-- END footer-bottom-->
</footer> 



</section>





<script>

  function getAge()
  {
    var date_of_birth = $("#date_of_birth").val();
    // var date = <?php echo date('Y-m-d'); ?>

    var date1 = Date.parse($("#date_of_birth").val());
    var date2 = Date.parse(<?php echo date('Y-m-d'); ?>);
    // alert(date1);

    // if (date1 < date2)
    // {

        if(date_of_birth != '')
        {
            $.ajax(
            {
               url: '/testBooking/getAgeCalculation/'+date_of_birth,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                // alert(result);
                  $("#age").val(result);
               }
            });
        }
    // }
    // else
    // {
    //   alert('Select Proper Date');
    //   $("#date_of_birth").val('');
    // }
  }

  function validateMainForm()
  {
    if($('#form_main').valid())
    {
      var pincode = $("#mailing_zipcode").val();
      var pincode_length = pincode.length;
      var res = pincode.substring(0, 3);
      // alert(pincode_length);

      if(pincode_length == 6)
      {
        // if(res == 56)
        // {
          $('#form_main').submit();
        // }
        // else
        // {
        //   alert('Not A Valid Pincode For Bengaluru.');
        // }
      }
      else
      {
        alert('Not A Valid Pincode.');
      }
    }
  }

  function addTempBookingPatientDetails()
  {
    if($('#form_details').valid())
    {

    var tempPR = {};
    tempPR['full_name'] = $("#full_name").val();
    tempPR['gender'] = $("#full_gender").val();
    tempPR['age'] = $("#full_age").val();
    tempPR['id_session'] = "<?php echo $id_session_patient;?>";
        $.ajax(
        {
           url: '/testBooking/addTempBookingPatientDetails',
            type: 'POST',
           data:
           {
            tempData: tempPR
           },
           error: function()
           {
            alert('Something is wrong');
           },
           success: function(result)
           {
            // alert(result);
            $("#view_temp_details").html(result);
            // $('#myModal').modal('hide');
           }
        });
    }
  }


  function deleteTempBookingPatientDetailsById(id)
  {
    if(id != '')
    {
      var cnf= confirm('Do you really want to delete?');
      if(cnf==true)
      {
          $.ajax(
          {
             url: '/testBooking/deleteTempBookingPatientDetailsById/'+id,
             type: 'GET',
             error: function()
             {
              alert('Something is wrong');
             },
             success: function(result)
             {
              // alert(result);
                $("#view_temp_details").html(result);
             }
          });
      }
    }
  }



     $(document).ready(function() {
        $("#form_main").validate({
            rules: {
                salutation: {
                    required: true
                },
                first_name: {
                    required: true
                },
                description: {
                    required: true
                },
                phone: {
                    required: true
                },
                gender: {
                    required: true
                },
                mail_address1: {
                    required: true
                },
                age: {
                    required: true
                },
                age_duration: {
                    required: true
                },
                mailing_zipcode: {
                  required: true
                }
            },
            messages: {
                salutation: {
                    required: "<p class='error-text'>Select Salutation</p>",
                },
                first_name: {
                    required: "<p class='error-text'>First Name Required</p>",
                },
                description: {
                    required: "<p class='error-text'>Test Description Required</p>",
                },
                phone: {
                    required: "<p class='error-text'>Phone No. Required</p>",
                },
                gender: {
                    required: "<p class='error-text'>Select Gender</p>",
                },
                mail_address1: {
                    required: "<p class='error-text'>Address Required</p>",
                },
                age: {
                    required: "<p class='error-text'>Age Required</p>",
                },
                age_duration: {
                    required: "<p class='error-text'>Select Age Duration</p>",
                },
                mailing_zipcode: {
                    required: "<p class='error-text'>Pincode Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

     $(document).ready(function() {
        $("#form_details").validate({
            rules: {
                full_name: {
                    required: true
                },
                full_gender: {
                    required: true
                },
                full_age: {
                    required: true
                }
            },
            messages: {
                full_name: {
                    required: "<p class='error-text'>Full Name Required</p>",
                },
                full_gender: {
                    required: "<p class='error-text'>Select Patient Gender</p>",
                },
                full_age: {
                    required: "<p class='error-text'>Age Required ( Approximate )</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


  function checkduplicate()
  {
    var email = $("#email").val();
          $.get("/register/duplicateemail/"+email, function(data, status){
               if(data=='1') {
                alert("Email already exist please login and proceed further");
                 $("#email").val(' ');
               }
           });
  }
</script>

<script src="//cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>

<script type="text/javascript">
// CKEDITOR.replace('description',{
//   width: "100%",
//   height: "300px"

// }); 

</script>
  </body>
</html>