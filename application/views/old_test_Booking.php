<?php
      // echo "<Pre>";print_r($programmeList);exit();
?>
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Scholarship Management System | Applicant Registration</title>
  <link rel="stylesheet" href="<?php echo BASE_PATH; ?>assets/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo BASE_PATH; ?>assets/css/main.css">
  <!-- <link rel="stylesheet" href="<?php echo BASE_PATH; ?>assets/select2/css/select2.css"> -->
  <link rel="stylesheet" href="<?php echo BASE_PATH; ?>assets/select2/css/select2-bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo BASE_PATH; ?>/assets/css/select2.min.css" rel="stylesheet" />
  <link rel="stylesheet" href="<?php echo BASE_PATH; ?>assets/css/feather.css" />
  <link rel="stylesheet" href="<?php echo BASE_PATH; ?>assets/css/main.css" />
</head>

<body>
    
    <h2>
      <?php
      $this->load->helper('form');
      $error = $this->session->flashdata('error');
      if ($error) {
      ?>
        <div class="alert alert-danger alert-dismissable">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
          <?php echo $error; ?>
        </div>
      <?php }
      $success = $this->session->flashdata('success');
      $entered_url = $this->session->flashdata('entered_url');

      if ($success)
      {
      ?>
          <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo $success; ?>
          </div>
      <?php
      }
      ?>
    </h2>

    <br>
    <br>
    <br>
    
    <form id="form_main" method="post">


      <div class="registration-container">

      <h3><b><u>Test Booking Details</u></b></h3>

        <div class="row">


          <div class="col-sm-4">
              <div class="form-group">
                  <label>Mr. | Ms. <span class='error-text'>*</span></label>
                  <select name="salutation" id="salutation" class="form-control">
                      <option value="">Select</option>
                      <?php
                      if (!empty($salutationList))
                      {
                          foreach ($salutationList as $record)
                          {?>
                       <option value="<?php echo $record->id;  ?>">
                          <?php echo $record->name;?>
                       </option>
                      <?php
                          }
                      }
                      ?>
                  </select>
              </div>
          </div>


          <div class="col-sm-4">
            <div class="form-group">
                <label>First Name <span class='error-text'>*</span></label>
                <input type="text" class="form-control" id="first_name" name="first_name" required>
            </div>
          </div>


          <div class="col-sm-4">
            <div class="form-group">
                <label>Last Name </label>
                <input type="text" class="form-control" id="last_name" name="last_name" required>
            </div>
          </div>


        </div>


        <div class="row">

          <div class="col-sm-4">
              <div class="form-group">
                  <label>Gender <span class='error-text'>*</span></label>
                  <select name="salutation" id="salutation" class="form-control">
                      <option value="">Select</option>
                       <option value="Male">MALE</option>
                       <option value="Female">FEMALE</option>
                       <option value="Other">OTHER</option>
                  </select>
              </div>
          </div>


          <div class="col-sm-4">
             <div class="form-group">
                <label>Date Of Birth <span class='error-text'>*</span></label>
                <input type="date" class="form-control datepicker" id="date_of_birth" name="date_of_birth" autocomplete="off" onblur="getAge()" required>
             </div>
          </div>

          
          <div class="col-sm-4">
            <div class="form-group">
                <label>Age <span class='error-text'>*</span></label>
                <input type="text" class="form-control" id="age" name="age" required readonly>
            </div>
          </div>

        </div>

        <div class="row">

          <div class="col-sm-4">
            <div class="form-group">
                <label>Phone Number <span class='error-text'>*</span></label>
                <input type="number" class="form-control" id="phone" name="phone" required>
            </div>
          </div>



          <div class="col-sm-4">
            <div class="form-group">
                <label>Alternate Mobile </label>
                <input type="number" class="form-control" id="alternative_phone" name="alternative_phone">
            </div>
          </div>


          <div class="col-sm-4">
            <div class="form-group">
                <label>Email </label>
                <input type="email" class="form-control" id="email_id" name="email_id">
            </div>
          </div>


        </div>


    <button type="button" onclick="validateMainForm()" class="btn btn-primary btn-block">Book Appointment</button>       

    </form>






    <form id="form_details" method="post">




        <br>

        <h4><b><u>Patient Booking Details</u></b></h4>


         <div class="row">

          <div class="col-sm-3">
            <div class="form-group">
                <label>Name <span class='error-text'>*</span></label>
                <input type="text" class="form-control" id="full_name" name="full_name">
            </div>
          </div>

          <div class="col-sm-3">
              <div class="form-group">
                  <label>Gender <span class='error-text'>*</span></label>
                  <select name="full_gender" id="full_gender" class="form-control">
                      <option value="">Select</option>
                       <option value="Male">MALE</option>
                       <option value="Female">FEMALE</option>
                       <option value="Other">OTHER</option>
                  </select>
              </div>
          </div>

          
          <div class="col-sm-3">
            <div class="form-group">
                <label>Age <span class='error-text'>*</span></label>
                <input type="text" class="form-control" id="full_age" name="full_age">
            </div>
          </div>


          <div class="col-sm-3">
            <div class="form-group">
              <br>
              <button type="button" onclick="addTempBookingPatientDetails()" class="btn btn-primary">Save</button>  
            </div>
          </div>

        </div>

        <br>

        <div class="row" id="view_temp_details">
        </div>

    </form>

    




  </div>




  <!-- </div> -->

  <script src="<?php echo BASE_PATH; ?>assets/js/jquery-1.12.4.min.js"></script>
  <script src="<?php echo BASE_PATH; ?>assets/js/bootstrap.min.js"></script>
</body>
</html>
<script>
  
  $('select').select2();

  $(function(){
    $(".datepicker").datepicker(
    {
        changeYear: true,
        changeMonth: true,
    });
    });

  // function RefreshParent()
  // {
  //   if (window.opener != null && !window.opener.closed)
  //   {
  //       window.opener.location.href = window.opener.location;
  //   }
  // }

  // window.onbeforeunload = RefreshParent();



  // window.onbeforeunload = function()
  // {
  //   return "Are you sure?";
  // };

  // window.onkeydown = function(event)
  // {
  //   if (event.keyCode === 116)
  //   {
  //     window.location.reload();
  //   }
  // };


  // window.onbeforeunload = function ()
  // {
  //   if (someConditionThatIndicatesIShouldConfirm) {
  //       return "If you reload this page, your previous action will be repeated";
  //   }
  //   else
  //   {
  //       //Don't return anything
  //   }
  // }


  function getAge()
  {
    var date_of_birth = $("#date_of_birth").val();
    // var date = <?php echo date('Y-m-d'); ?>

    var date1 = Date.parse($("#date_of_birth").val());
    var date2 = Date.parse(<?php echo date('Y-m-d'); ?>);
    // alert(date1);

    // if (date1 < date2)
    // {

        if(date_of_birth != '')
        {
            $.ajax(
            {
               url: '/testBooking/getAgeCalculation/'+date_of_birth,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                // alert(result);
                  $("#age").val(result);
               }
            });
        }
    // }
    // else
    // {
    //   alert('Select Proper Date');
    //   $("#date_of_birth").val('');
    // }
  }

  function validateMainForm()
  {
    if($('#form_main').valid())
    {
      $('#form_main').submit();
    }
  }

  function addTempBookingPatientDetails()
  {
    if($('#form_details').valid())
    {

    var tempPR = {};
    tempPR['full_name'] = $("#full_name").val();
    tempPR['full_gender'] = $("#full_gender").val();
    tempPR['full_age'] = $("#full_age").val();
    tempPR['id_session'] = "<?php echo $is_session_patient;?>";
        $.ajax(
        {
           url: '/testBooking/addTempBookingPatientDetails',
            type: 'POST',
           data:
           {
            tempData: tempPR
           },
           error: function()
           {
            alert('Something is wrong');
           },
           success: function(result)
           {
            alert(result);
            $("#view_temp_details").html(result);
            // $('#myModal').modal('hide');
           }
        });
    }
  }



     $(document).ready(function() {
        $("#form_main").validate({
            rules: {
                salutation: {
                    required: true
                },
                first_name: {
                    required: true
                },
                last_name: {
                    required: true
                },
                phone: {
                    required: true
                },
                Gender: {
                    required: true
                },
                date_of_birth: {
                    required: true
                }
            },
            messages: {
                salutation: {
                    required: "<p class='error-text'>Select Role</p>",
                },
                first_name: {
                    required: "<p class='error-text'>Name Required</p>",
                },
                last_name: {
                    required: "<p class='error-text'>NRIC Required</p>",
                },
                phone: {
                    required: "<p class='error-text'>Select Effective Date</p>",
                },
                gender: {
                    required: "<p class='error-text'>Select Effective Date</p>",
                },
                date_of_birth: {
                    required: "<p class='error-text'>Select Effective Date</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

     $(document).ready(function() {
        $("#form_details").validate({
            rules: {
                full_name: {
                    required: true
                },
                full_gender: {
                    required: true
                },
                full_age: {
                    required: true
                }
            },
            messages: {
                full_name: {
                    required: "<p class='error-text'>Full Name Required</p>",
                },
                full_gender: {
                    required: "<p class='error-text'>Select Patient Gender</p>",
                },
                full_age: {
                    required: "<p class='error-text'>Age ( If Approximate )Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

  
</script>