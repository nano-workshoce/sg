<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Applicant Portal | Applicant Registration</title>
  <link rel="stylesheet" href="<?php echo BASE_PATH; ?>assets/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo BASE_PATH; ?>assets/css/main.css">
  <link rel="stylesheet" href="<?php echo BASE_PATH; ?>assets/select2/css/select2.css">
  <link rel="stylesheet" href="<?php echo BASE_PATH; ?>assets/select2/css/select2-bootstrap.min.css">
</head>

<body>
  <div class="login-wrapper">
    <div class="container">
      <div class="login-container">
        <div class="text-center">
          <a href="/"><img src="<?php echo BASE_PATH; ?>assets/img/logo-2.png" /></a>     
        </div>        
        <h3 class="login-title">Applicant Registration</h3>
        <div>
          <?php
          $this->load->helper('form');
          $error = $this->session->flashdata('error');
          if ($error) {
          ?>
            <div class="alert alert-danger alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
              <?php echo $error; ?>
            </div>
          <?php }
          $success = $this->session->flashdata('success');
          $entered_url = $this->session->flashdata('entered_url');
          // print_r($success);exit();
    
          if ($success)
          {
          ?>
              <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $success; ?>
              </div>
          <?php
          }
          ?>
        </div>
        <form action="<?php echo base_url(); ?>applicantLogin/applicantRegistration" method="post">
    
                <div class="form-group">
                    <label>Salutation <span class='error-text'>*</span></label>
                    <select name="salutation" id="salutation" class="form-control" required>
                      <option value="">Select</option>
                       <?php
                          if (!empty($salutationList)) {
                              foreach ($salutationList as $record) {
                          ?>
                       <option value="<?php echo $record->id;  ?>">
                          <?php echo $record->name;  ?>
                       </option>
                       <?php
                          }
                          }
                          ?>  
                        <!-- <option value="">Select</option>
                        <option value="Mr">Mr</option>
                        <option value="Miss">Miss</option>
                        <option value="Mrs">Mrs</option> -->
                    </select>
                </div>
    
    
                <div class="form-group">
                    <label>First Name <span class='error-text'>*</span></label>
                    <input type="text" class="form-control" id="first_name" name="first_name" required>
                </div>
    
    
    
                <div class="form-group">
                    <label>Last Name <span class='error-text'>*</span></label>
                    <input type="text" class="form-control" id="last_name" name="last_name" required>
                </div>
    
    
                <div class="form-group">
                    <label>Phone Number <span class='error-text'>*</span></label>
                    <div class="row">
                       <div class="col-sm-4">


                    <select name="country_code" id="country_code" class="form-control" required>
                      <option value="">Select</option>                    
                      <?php
                          if (!empty($countryList))
                          {
                            foreach ($countryList as $record)
                            {
                          ?>
                       <option value="<?php echo $record->phone_code;  ?>">
                          <?php echo $record->phone_code . "  " . $record->name;  ?>
                       </option>
                         <?php
                          }
                        }
                      ?>
                    </select>
                  </div>
                  <div class="col-sm-8">
                    <input type="number" class="form-control" id="phone" name="phone" minlength="10" maxlength="10" required>
                  </div>
                  </div>
                </div>


                 <div class="form-group">
                    <label>ID Type <span class='error-text'>*</span></label>
                     <select name="id_type" id="id_type" class="form-control" required  onchange="getlabel()">
                      <option value="">Select</option>
                     
                         
                    
                        <option value="NRIC">NRIC</option>
                        <option value="PASSPORT">PASSPORT</option>
                    </select>
                </div>
    
                <div class="form-group" id="dividlabel" style="display:none;">
                    <label><span id='labelspanid'></span> <span class='error-text'>*</span></label>
                    <input type="text" class="form-control" id="nric" name="nric" required>
                </div>
    
    
                <div class="form-group">
                    <label>Email <span class='error-text'>*</span></label>
                    <input type="email" class="form-control" id="email_id" name="email_id" required>
                </div>
    
    
                <div class="form-group">
                    <label>Password <span class='error-text'>*</span></label>
                    <input type="password" class="form-control" id="password" name="password" required>
                </div>
    
                <br>
    
            <button type="submit" class="btn btn-primary btn-block">Submit</button>
    
              <!-- <div class="login-links">
                <hr />
                <p>Have an account? <a href="/applicantLogin">Login</a></p>
              </div> -->
    
    
        </form>
      </div>
    </div>
    <div class="helpinfo-block">
        <h4>Have an account? <a href="/applicantLogin">Login</a></h4>
        <h4>For international applicants :</h4>
        <ul>
           <li>Please use your passport number for registration and use your registered email for login.</li>
          <li>Please download and read Guidelines For The Acceptance Of International Students Who Have Not Met The English Language Requirement For Enrolling As A Student In Malaysia. <a href="http://online.usas.edu.my/postgraduate/v2/English_Requiremt_2-21-Dec-2017.pdf" target="_blank">Click Here</a></li>
        </ul>
        <h4>For any enquiries, please contact <a href="mailto:admission.usas@eag.com.my">admission.usas@eag.com.my</a></h4>
    </div>     
  </div>
<script>
    $('select').select2();  

    function getlabel() {
               $("#dividlabel").hide();

      var labelnric = $("#id_type").val();
      if(labelnric!='') {
         $("#dividlabel").show();
      }
      //alert(labelnric);
      $("#labelspanid").html(labelnric);
    }
</script>

  <script src="<?php echo BASE_PATH; ?>assets/js/jquery-1.12.4.min.js"></script>
  <script src="<?php echo BASE_PATH; ?>assets/js/bootstrap.min.js"></script>
</body>
</html>