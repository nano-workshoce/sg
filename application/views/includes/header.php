<!DOCTYPE html>
<html lang="en"> 

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $pageTitle; ?></title>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="<?php echo BASE_PATH; ?>assets/css/bootstrap.min.custom.css"/>
    <link type="text/css" rel="stylesheet" href="<?php echo BASE_PATH; ?>assets/css/style.css" />
    <link rel="stylesheet" href="<?php echo BASE_PATH; ?>assets/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="<?php echo BASE_PATH; ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo BASE_PATH; ?>assets/css/datatable.min.css">
    <link rel="stylesheet" href="<?php echo BASE_PATH; ?>assets/css/bootstrap-multiselect.css">
    <link rel="stylesheet" href="<?php echo BASE_PATH; ?>assets/css/main.css">

</head>
<body>
    <header class="navbar navbar-default navbar-fixed-top main-header">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand main-logo" href="/">Sangameshwara Laboratories</a>
            </div>
            <nav class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right"></ul>
            </nav>
        </div>
    </header>
</body>

     
