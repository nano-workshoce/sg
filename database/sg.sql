-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 18, 2021 at 12:00 PM
-- Server version: 10.2.38-MariaDB-log
-- PHP Version: 7.3.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `camsedu_sg`
--

-- --------------------------------------------------------

--
-- Table structure for table `applicant`
--

CREATE TABLE `applicant` (
  `id` int(20) NOT NULL,
  `id_intake` int(11) DEFAULT NULL,
  `id_program` int(11) DEFAULT NULL,
  `full_name` varchar(580) DEFAULT '',
  `salutation` varchar(120) DEFAULT '',
  `first_name` varchar(120) DEFAULT '',
  `last_name` varchar(120) DEFAULT '',
  `nric` varchar(120) DEFAULT '',
  `passport` varchar(120) DEFAULT '',
  `phone` varchar(120) DEFAULT '',
  `email_id` varchar(120) DEFAULT '',
  `contact_email` varchar(500) DEFAULT '',
  `password` varchar(120) DEFAULT '',
  `passport_expiry_date` varchar(120) DEFAULT '',
  `gender` varchar(120) DEFAULT '',
  `date_of_birth` varchar(120) DEFAULT '',
  `martial_status` varchar(120) DEFAULT '',
  `religion` varchar(120) DEFAULT '',
  `nationality` varchar(120) DEFAULT '',
  `is_hostel` int(20) DEFAULT 0,
  `id_degree_type` int(20) DEFAULT 0,
  `id_race` varchar(120) DEFAULT '',
  `mail_address1` varchar(120) DEFAULT '',
  `mail_address2` varchar(120) DEFAULT '',
  `mailing_country` int(120) DEFAULT NULL,
  `mailing_state` int(120) DEFAULT NULL,
  `mailing_city` varchar(120) DEFAULT '',
  `program_scheme` varchar(200) DEFAULT '',
  `id_program_scheme` int(20) DEFAULT 0 COMMENT 'it''s id_learning_mode after flow change',
  `id_program_has_scheme` int(20) DEFAULT 0 COMMENT 'it''s the actual id_scheme for the program',
  `id_fee_structure` int(20) DEFAULT 0,
  `id_program_landscape` int(20) DEFAULT 0,
  `id_program_requirement` int(20) DEFAULT 0,
  `id_university` int(20) DEFAULT 0,
  `id_branch` int(20) DEFAULT 0,
  `mailing_zipcode` varchar(120) DEFAULT '',
  `present_address_same_as_mailing_address` int(20) DEFAULT 0,
  `permanent_address1` varchar(120) DEFAULT '',
  `permanent_address2` varchar(120) DEFAULT '',
  `permanent_country` int(120) DEFAULT NULL,
  `permanent_state` int(120) DEFAULT NULL,
  `permanent_city` varchar(120) DEFAULT '',
  `permanent_zipcode` varchar(120) DEFAULT '',
  `sibbling_discount` varchar(50) DEFAULT '',
  `employee_discount` varchar(50) DEFAULT '',
  `alumni_discount` varchar(50) DEFAULT '',
  `applicant_status` varchar(50) DEFAULT 'Draft',
  `is_sibbling_discount` int(20) DEFAULT 3,
  `is_employee_discount` int(20) DEFAULT 3,
  `is_alumni_discount` int(2) DEFAULT 3,
  `is_apeal_applied` int(2) DEFAULT 3,
  `is_invoice_generated` int(20) DEFAULT 0,
  `id_apeal_status` int(20) DEFAULT 0,
  `phd_duration` int(20) DEFAULT 0,
  `apel_reject_reason` varchar(2048) DEFAULT '',
  `pathway` varchar(200) DEFAULT '',
  `email_verified` int(2) DEFAULT 0,
  `is_submitted` int(2) DEFAULT 0,
  `submitted_date` datetime DEFAULT NULL,
  `is_updated` int(2) DEFAULT 0,
  `id_program_structure_type` int(20) DEFAULT 0,
  `reason` varchar(2048) DEFAULT '',
  `status` int(20) DEFAULT NULL,
  `accept_date` date DEFAULT NULL,
  `approved_by` int(20) DEFAULT 0,
  `approved_dt_tm` datetime DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp(),
  `id_type` varchar(50) DEFAULT NULL,
  `country_code` varchar(50) DEFAULT NULL,
  `english_year` varchar(20) DEFAULT NULL,
  `english_grade` varchar(20) DEFAULT NULL,
  `id_english_test` int(20) DEFAULT NULL,
  `whatsapp_number` varchar(50) DEFAULT NULL,
  `linked_in` varchar(50) DEFAULT NULL,
  `facebook_id` varchar(50) DEFAULT NULL,
  `twitter_id` varchar(50) DEFAULT NULL,
  `ig_id` varchar(50) DEFAULT NULL,
  `profile_pic` varchar(1024) DEFAULT 'default_profile.jpg',
  `step6_status` int(10) DEFAULT NULL,
  `step5_status` int(10) DEFAULT NULL,
  `step4_status` int(10) DEFAULT NULL,
  `step3_status` int(10) DEFAULT NULL,
  `step2_status` int(10) DEFAULT NULL,
  `step1_status` int(10) DEFAULT NULL,
  `applicant_comments` text DEFAULT NULL,
  `step6_comments` text DEFAULT NULL,
  `step5_comments` text DEFAULT NULL,
  `step4_comments` text DEFAULT NULL,
  `step3_comments` text DEFAULT NULL,
  `step2_comments` text DEFAULT NULL,
  `step1_comments` text DEFAULT NULL,
  `portfolio_result` int(10) DEFAULT NULL,
  `portfolio_grade` varchar(100) DEFAULT NULL,
  `challenged_test_status` int(10) DEFAULT NULL,
  `challenged_test_grade` varchar(200) DEFAULT NULL,
  `interview_result` int(10) DEFAULT NULL,
  `interview_grade` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `applicant`
--

INSERT INTO `applicant` (`id`, `id_intake`, `id_program`, `full_name`, `salutation`, `first_name`, `last_name`, `nric`, `passport`, `phone`, `email_id`, `contact_email`, `password`, `passport_expiry_date`, `gender`, `date_of_birth`, `martial_status`, `religion`, `nationality`, `is_hostel`, `id_degree_type`, `id_race`, `mail_address1`, `mail_address2`, `mailing_country`, `mailing_state`, `mailing_city`, `program_scheme`, `id_program_scheme`, `id_program_has_scheme`, `id_fee_structure`, `id_program_landscape`, `id_program_requirement`, `id_university`, `id_branch`, `mailing_zipcode`, `present_address_same_as_mailing_address`, `permanent_address1`, `permanent_address2`, `permanent_country`, `permanent_state`, `permanent_city`, `permanent_zipcode`, `sibbling_discount`, `employee_discount`, `alumni_discount`, `applicant_status`, `is_sibbling_discount`, `is_employee_discount`, `is_alumni_discount`, `is_apeal_applied`, `is_invoice_generated`, `id_apeal_status`, `phd_duration`, `apel_reject_reason`, `pathway`, `email_verified`, `is_submitted`, `submitted_date`, `is_updated`, `id_program_structure_type`, `reason`, `status`, `accept_date`, `approved_by`, `approved_dt_tm`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`, `id_type`, `country_code`, `english_year`, `english_grade`, `id_english_test`, `whatsapp_number`, `linked_in`, `facebook_id`, `twitter_id`, `ig_id`, `profile_pic`, `step6_status`, `step5_status`, `step4_status`, `step3_status`, `step2_status`, `step1_status`, `applicant_comments`, `step6_comments`, `step5_comments`, `step4_comments`, `step3_comments`, `step2_comments`, `step1_comments`, `portfolio_result`, `portfolio_grade`, `challenged_test_status`, `challenged_test_grade`, `interview_result`, `interview_grade`) VALUES
(116, 3, 10, 'Mr. Developper Testing One', '1', 'Developper', 'Testing One', '80001', '', '80001', 'dt1@eag.com', '', '202cb962ac59075b964b07152d234b70', '', 'Male', '2021-01-16', 'Single', '2', 'Malaysian', 1, 2, '5', 'No 1 , KL ', 'KL', 110, 1, 'KL', 'Online - Part Time', 29, 1, 24, 34, 1, 9, 9, '12341231', 1, ' No 1, KL', 'KL', 110, 1, 'KL', '12312', '', 'Yes', '', 'Approved', 3, 1, 3, 1, 61, 8, 1, '', 'APEL', 1, 1, '2021-01-13 16:45:12', 1, 1, '', NULL, NULL, 1, '2021-01-13 00:00:00', NULL, '2021-01-12 21:42:23', NULL, '2021-01-12 21:42:23', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'default_profile.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'd', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(120, 3, 10, 'Ms. Hazna Ahmad', '3', 'Hazna', 'Ahmad', '710202022222', '', '0126015943', 'hazzna@gmail.com', '', '45b9a75b20ef4a98f9a159143c636fcf', '', 'Female', '1974-01-05', 'Married', '1', '1', 1, 1, '1', '57 JALAN SWEI INTAN 3', 'TAMAN SERI INTAN', 110, 5, 'Ampang', '', 29, 1, 0, 0, 0, 9, 9, '68000', 1, '57 JALAN SWEI INTAN 3', 'TAMAN SERI INTAN', 110, 5, 'Ampang', '68000', '', '', '', 'Draft', 3, 3, 3, 3, 0, 0, 0, '', '', 1, 0, NULL, 1, 1, '', NULL, NULL, 0, NULL, NULL, '2021-01-13 16:54:15', NULL, '2021-01-13 16:54:15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'default_profile.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(122, 3, 10, 'Mr. Testing  one', '1', 'Testing ', 'one', 'BRMV2M1', '', '1234567893', 'test1@gmail.com', '', '245cf079454dc9a3374a7c076de247cc', '', 'Male', '2021-01-15', 'Single', '3', '3', 1, 2, '', 'add one', 'two', 1, 17, 'city', 'Online - Part Time', 29, 3, 25, 34, 0, 9, 9, '12333', 1, 'add one', 'two', 3, 19, 'city', '12333', '', '', '', 'Draft', 3, 3, 3, 3, 0, 0, 0, '', 'DIRECT ENTRY', 1, 1, '2021-01-15 02:20:35', 1, 1, '', NULL, NULL, 0, NULL, NULL, '2021-01-14 12:47:23', NULL, '2021-01-14 12:47:23', 'PASSPORT', '+61', '', '', 1, NULL, NULL, NULL, NULL, NULL, 'default_profile.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(123, 3, 10, 'Mr. Developper Testing Two', '1', 'Developper', 'Testing Two', '80002', '', '8000211111', 'dt2@eag.com', '', '202cb962ac59075b964b07152d234b70', '', 'Male', '2021-01-12', 'Single', '3', '1', 1, 2, '5', 'KL', 'KL', 110, 1, 'KL', 'Online - Full Time', 31, 1, 23, 34, 0, 9, 9, '123', 1, 'KL', 'KL', 110, 1, 'KL', '123', '', 'Yes', '', 'Draft', 3, 1, 3, 3, 62, 0, 0, '', 'DIRECT ENTRY', 1, 1, '2021-01-15 02:24:41', 1, 1, '', NULL, NULL, 1, '2021-01-15 00:00:00', NULL, '2021-01-14 12:51:28', NULL, '2021-01-14 12:51:28', 'NRIC', '+61', '', '', 0, NULL, NULL, NULL, NULL, NULL, 'default_profile.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(124, 3, 10, 'Mr. zolhisam yunus', '1', 'zolhisam', 'yunus', 'A123456', '', '0123995943', 'zolhisay@gmail.com', '', '6e8ca3f0cd944ec7a80f8f3369b6638f', '', 'Male', '1970-01-01', 'Married', '1', '4', 1, 2, '', '57 JALAN SERI INTAN 3', 'TAMAN SERI INTAN', 110, 5, 'Ampang', 'Online - Part Time', 29, 3, 25, 34, 0, 9, 9, '68000', 1, '57 JALAN SERI INTAN 3', 'TAMAN SERI INTAN', 110, 5, 'Ampang', '68000', '', '', '', 'Approved', 3, 3, 3, 3, 67, 0, 1, '', 'DIRECT ENTRY', 1, 1, '2021-01-15 14:41:30', 1, 1, '', NULL, NULL, 1, '2021-01-15 00:00:00', NULL, '2021-01-14 19:56:09', NULL, '2021-01-14 19:56:09', 'PASSPORT', '+61', '', '', 1, NULL, NULL, NULL, NULL, NULL, 'default_profile.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(125, 3, 10, 'Mr. HH 01', '1', 'HH', '01', '789', '', '7894561230', 'hh01@g.com', '', '1b819199cc41378a4da4d4949e5ddbd2', '', 'Male', '2020-01-01', 'Single', '15', '1', 1, 2, '5', 'No. 12/1 A', 'Paradise street', 110, 9, 'Johor Bahru', 'Online - Full Time', 31, 1, 23, 34, 0, 9, 9, '79100', 0, 'No. 12/1 A', 'Paradise street', 110, 9, 'Johor Bahru', '79100', '', '', '', 'Draft', 3, 3, 3, 3, 63, 0, 1, '', 'DIRECT ENTRY', 1, 1, '2021-01-15 11:06:01', 1, 1, '', NULL, NULL, 1, '2021-01-15 00:00:00', NULL, '2021-01-14 21:20:25', NULL, '2021-01-14 21:20:25', 'NRIC', '+61', '', '', 0, NULL, NULL, NULL, NULL, NULL, 'default_profile.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(126, 3, 10, 'Mr. KK 01', '1', 'KK', '01', '123', '', '1234567790', 'kk01@g.com', '', '3f468f63dff832fd26bf14683b095b5d', '', 'Male', '1992-01-01', 'Single', '4', '4', 1, 2, '', 'No. 007', 'Building - Saturn', 110, 9, 'Johor', 'Online - Full Time', 31, 3, 23, 34, 0, 9, 9, '75000', 1, 'No. 007', 'Building - Saturn', 110, 9, 'Johor', '75000', '', '', '', 'Draft', 3, 3, 3, 3, 64, 0, 1, '', 'DIRECT ENTRY', 1, 1, '2021-01-15 12:35:07', 1, 1, '', NULL, NULL, 1, '2021-01-15 00:00:00', NULL, '2021-01-14 22:52:58', NULL, '2021-01-14 22:52:58', 'NRIC', '+61', '', '', 1, NULL, NULL, NULL, NULL, NULL, 'default_profile.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(127, 3, 10, 'Mr. Ezz Ali', '1', 'Ezz', 'Ali', '123123', '', '1139646066', 'ezzittech@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', '', 'Male', '1991-01-01', 'Single', '1', '4', 1, 2, '', 'Jalan Ampang', '', 110, 6, 'Ampang', 'Online - Part Time', 29, 3, 25, 34, 0, 9, 9, '50450', 1, 'Jalan Ampang', '', 110, 6, 'Ampang', '50450', '', '', '', 'Migrated', 3, 3, 3, 3, 66, 0, 1, '', 'DIRECT ENTRY', 1, 1, '2021-01-15 13:40:48', 1, 1, '', NULL, NULL, 1, '2021-01-15 15:07:55', NULL, '2021-01-15 00:03:46', NULL, '2021-01-15 00:03:46', 'PASSPORT', '+61', '', '', 1, NULL, NULL, NULL, NULL, NULL, 'default_profile.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(128, 3, 10, 'Mr. Salam K.', '1', 'Salam', 'K.', '09778866', '', '0182343385', 'eng.abdulsalam@yahoo.com', '', 'e10adc3949ba59abbe56e057f20f883e', '', 'Male', '2000-01-01', 'Married', '1', '1', 1, 2, '1', 'C-7-7, Megan Phileo Promenade Block C, Hampshire Park', '', 110, 6, 'Kuala Lumpur', 'Online - Full Time', 31, 1, 23, 34, 0, 9, 9, '50450', 1, 'C-7-7, Megan Phileo Promenade Block C, Hampshire Park', '', 110, 6, 'Kuala Lumpur', '50450', '', '', '', 'Migrated', 3, 3, 3, 3, 65, 0, 1, '', 'DIRECT ENTRY', 1, 1, '2021-01-15 14:07:40', 1, 1, '', NULL, NULL, 1, '2021-01-15 15:09:47', NULL, '2021-01-15 00:29:11', NULL, '2021-01-15 00:29:11', 'PASSPORT', '+61', '', '', 0, NULL, NULL, NULL, NULL, NULL, 'default_profile.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(129, 3, 10, 'Mr. Developper Testing Four', '1', 'Developper', 'Testing Four', '80004', '', '8000000004', 'dt4@eag.com', '', '202cb962ac59075b964b07152d234b70', '', 'Male', '2002-01-29', '', '3', '1', 1, 2, '3', 'KL', 'KL', 110, 1, 'KL', 'Online - Part Time', 29, 1, 25, 34, 0, 9, 9, '12345', 1, 'KL', 'KL', 110, 1, 'KL', '12345', '', '', '', 'Draft', 3, 3, 3, 3, 0, 0, 1, '', 'DIRECT ENTRY', 1, 1, '2021-01-15 20:51:35', 1, 1, '', NULL, NULL, 0, NULL, NULL, '2021-01-15 07:16:35', NULL, '2021-01-15 07:16:35', 'NRIC', '+61', '', '', 0, NULL, NULL, NULL, NULL, NULL, 'default_profile.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(130, 3, 10, 'Mr. Developper Testing Five', '1', 'Developper', 'Testing Five', '80005', '', '8000000005', 'dt5@eag.com', '', '202cb962ac59075b964b07152d234b70', '', 'Male', '2021-01-13', 'Single', '4', '1', 1, 2, '3', 'KL', 'KL', 110, 1, 'KL', 'Online - Part Time', 29, 1, 25, 34, 0, 9, 9, '12345', 1, 'KL', 'KL', 110, 1, 'KL', '12345', '', '', '', 'Migrated', 3, 3, 3, 3, 68, 0, 1, '', 'DIRECT ENTRY', 1, 1, '2021-01-16 00:14:43', 1, 1, '', NULL, NULL, 1, '2021-01-16 00:00:00', NULL, '2021-01-15 10:38:12', NULL, '2021-01-15 10:38:12', 'NRIC', '+61', '', '', 0, '999999999', 'www.vk88888.com', 'www.vk88888.com', 'www.vk88888.com', 'www.vk88888.com', '75da8db6e1ac110031b2835bac9102fc.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(131, 3, 10, 'Ms. HANIZA ahmad', '3', 'HANIZA', 'ahmad', 'a123789', '', '0193574124', 'hazna@creativebytes.com.my', '', '8a3f87dbc621ec58f8c540429eb5313c', '', 'Female', '1978-01-01', 'Married', '1', '4', 1, 2, '', 'B-0-8 Jalan Pinggiran Cempaka', 'Taman Cempaka', 110, 5, 'Ampang', 'Online - Part Time', 29, 3, 25, 34, 0, 9, 9, '68000', 1, 'B-0-8 Jalan Pinggiran Cempaka', 'Taman Cempaka', 110, 5, 'Ampang', '68000', '', '', '', 'Submitted', 3, 3, 3, 3, 0, 0, 1, '', 'DIRECT ENTRY', 1, 1, '2021-01-16 12:18:30', 1, 1, '', NULL, NULL, 0, NULL, NULL, '2021-01-15 11:13:44', NULL, '2021-01-15 11:13:44', 'PASSPORT', '+61', '2020', '120', 1, '', '', '', '', '', 'd413dcfb92ed1364b8053c779ba146cb.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(132, 3, 10, 'Ms. Ma. Cristina Leyson', '3', 'Ma. Cristina', 'Leyson', 'P1712008A', '', '639158867822', 'kaye.ilcmarketing@gmail.com', '', 'b98ba340154b41f28f5dc65b5122891f', '', 'Female', '1987-09-16', 'Single', '9', '4', 1, 2, '', 'B38 L31 Ph1 Cityhomes Resortville', '', 146, 162, 'Dasmarinas', 'Online - Part Time', 29, 3, 25, 34, 0, 9, 9, '4114', 1, 'B38 L31 Ph1 Cityhomes Resortville', '', 146, 162, 'Dasmarinas', '4114', '', '', '', 'Submitted', 3, 3, 3, 3, 0, 0, 1, '', 'DIRECT ENTRY', 1, 1, '2021-01-17 01:27:58', 1, 1, '', NULL, NULL, 0, NULL, NULL, '2021-01-16 11:46:00', NULL, '2021-01-16 11:46:00', 'PASSPORT', '+61', '', '', 1, '+639951024112', 'https://www.linkedin.com/in/kayeleyson/', 'https://www.facebook.com/kayeleyson12/', 'https://twitter.com/KayeLeyson_', 'https://www.instagram.com/kaye.leyson/', 'default_profile.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(133, 3, 10, 'Mrs. Mohd Tej', '2', 'Mohd', 'Tej', 'NRI', '', '12312331', 'nric@G.COM', '', '45b7a98aa964e7a1cbcfee04a5fcc76b', '', 'Female', '2021-01-17', 'Married', '4', '2', 1, 2, '', 'addres one', 'two', 3, 19, 'city', 'Online - Part Time', 29, 3, 25, 34, 0, 9, 9, '123456', 1, 'addres one', 'two', 3, 19, 'city', '123456', '', '', '', 'Pending documents', 3, 3, 3, 3, 0, 0, 1, '', 'DIRECT ENTRY', 1, 1, '2021-01-17 02:09:39', 1, 1, '', NULL, NULL, 0, NULL, NULL, '2021-01-16 12:36:34', NULL, '2021-01-16 12:36:34', 'NRIC', '+61', '1990', 'A', 1, 'w', 'l', 'f', 't', 'i', 'default_profile.jpg', 1, 1, 2, 1, 1, 1, 'asdf', 'vvvv', 'v', 'Reupload', 'v', 'v', 'c', NULL, NULL, NULL, NULL, NULL, NULL),
(134, 3, 10, 'Mr. Developper Testing Six', '1', 'Developper', 'Testing Six', 'NRIC', '', '8888888806', 'dt6@eag.com', '', '202cb962ac59075b964b07152d234b70', '', 'Male', '2021-01-18', 'Single', '5', '1', 1, 2, '5', 'KL', 'KL', 1, 17, 'KL', 'Online - Part Time', 29, 1, 25, 34, 0, 9, 9, '12345', 1, 'KL', 'KL', 1, 17, 'KL', '12345', '', '', '', 'Migrated', 3, 3, 3, 3, 0, 0, 1, '', 'DIRECT ENTRY', 1, 1, '2021-01-17 21:32:35', 1, 1, '', NULL, NULL, 0, NULL, NULL, '2021-01-17 06:21:54', NULL, '2021-01-17 06:21:54', 'NRIC', '+61', '', '', 0, '999999999', 'www.vk88888.com', 'www.vk88888.com', 'www.vk88888.com', 'www.vk88888.com', 'default_profile.jpg', 1, 1, 1, NULL, 1, 1, 'Approved', 'Verified', 'Verified', 'Ver', NULL, 'Verified', 'Verified', NULL, NULL, NULL, NULL, NULL, NULL),
(135, NULL, NULL, 'Ms. kira asd', '3', 'kira', 'asd', 'nriciiii', '', '234234', 'a@a.com', '', '0cc175b9c0f1b6a831c399e269772661', '', '', '', '', '', '', 0, 0, '', '', '', NULL, NULL, '', '', 0, 0, 0, 0, 0, 0, 0, '', 0, '', '', NULL, NULL, '', '', '', '', '', 'Draft', 3, 3, 3, 3, 0, 0, 0, '', '', 1, 0, NULL, 0, 0, '', NULL, NULL, 0, NULL, NULL, '2021-01-17 09:10:08', NULL, '2021-01-17 09:10:08', 'NRIC', '+61', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'default_profile.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(136, 3, 10, 'Mr. LL 01', '1', 'LL', '01', '123456', '', '123456', 'll01@g.com', '', '5f4dcc3b5aa765d61d8327deb882cf99', '', 'Male', '0202-01-01', '', '5', '1', 1, 2, '5', 'No. 12/1 A', 'Paradise street', 1, 1, 'Johor Bahru', 'Online - Full Time', 31, 1, 23, 34, 0, 9, 9, '79100', 1, 'No. 12/1 A', 'Paradise street', 1, 1, 'Johor Bahru', '79100', '', '', '', 'Approved', 3, 3, 3, 3, 0, 0, 1, '', 'DIRECT ENTRY', 1, 1, '2021-01-18 07:38:55', 1, 1, '', NULL, NULL, 0, NULL, NULL, '2021-01-17 17:57:52', NULL, '2021-01-17 17:57:52', 'NRIC', '+93', '', '', 0, '123456', 'll01', '', '', 'll01', 'b330d8ed3de6fe2afa6639bcdcd25b31.jpg', 1, 1, 1, 1, 0, 0, 'Approved', '', '', '', 'Verified', '', '', 0, '', 0, '', 0, ''),
(137, 3, 10, 'Mr. MM 01', '1', 'MM', '01', 'LM05789', '', '0101010101', 'mm01@g.com', '', '9eb5a09ebc3e0b9cff10e49b7fdbd399', '', 'Male', '1942-01-01', '', '7', '9', 1, 2, '', 'No. 007', 'Building - Saturn', 1, 9, 'Johor', 'Online - Part Time', 29, 3, 25, 34, 0, 9, 9, '75000', 1, 'No. 007', 'Building - Saturn', 1, 9, 'Johor', '75000', '', '', '', 'Migrated', 3, 3, 3, 3, 76, 0, 1, '', 'DIRECT ENTRY', 1, 1, '2021-01-18 09:57:10', 1, 1, '', NULL, NULL, 0, NULL, NULL, '2021-01-17 20:06:26', NULL, '2021-01-17 20:06:26', 'PASSPORT', '+355', '1990', 'A', 1, '', '', '', '', '', '93f27d23afd21ee4f7a5d6a8b8794984.jpg', 1, 1, 1, 1, 1, 0, 'Approved', '', '', '', 'verified', '', '', 0, '', 0, '', 0, ''),
(138, NULL, NULL, 'Ms. NN 01', '3', 'NN', '01', '1347', '', '7896', 'nn01@g.com', '', '4f77d57c7f30adbdc3c3aa9cd94e2bf5', '', 'Female', '2013-04-01', 'Single', '2', '1', 1, 0, '2', '', '', NULL, NULL, '', '', 0, 0, 0, 0, 0, 0, 0, '', 0, '', '', NULL, NULL, '', '', '', '', '', 'Draft', 3, 3, 3, 3, 0, 0, 0, '', '', 1, 0, NULL, 1, 0, '', NULL, NULL, 0, NULL, NULL, '2021-01-17 23:17:19', NULL, '2021-01-17 23:17:19', 'NRIC', '+61', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'default_profile.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(139, 3, 10, 'Ms. PP 01', '3', 'PP', '01', '7198', '', '934789', 'pp01@g.com', '', 'c2cca5c59b1f2e38b24b496c08c9632a', '', 'Female', '1990-01-03', '', '8', '1', 1, 2, '2', 'No. 12/1 A', 'Paradise street', 1, 9, 'Johor Bahru', 'Online - Full Time', 31, 1, 23, 34, 0, 9, 9, '79100', 1, 'No. 12/1 A', 'Paradise street', 1, 9, 'Johor Bahru', '79100', '', '', '', 'Submitted', 3, 3, 3, 3, 0, 0, 1, '', 'DIRECT ENTRY', 1, 1, '2021-01-18 22:56:43', 1, 1, '', NULL, NULL, 0, NULL, NULL, '2021-01-18 09:14:11', NULL, '2021-01-18 09:14:11', 'NRIC', '+92', '', '', 0, '0402344689', '', '', '', '', '0eb157a070a376642ef4b14b8ee24769.jpg', NULL, NULL, 1, NULL, 0, 1, NULL, NULL, NULL, '', NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `applicant_status`
--

CREATE TABLE `applicant_status` (
  `id` int(20) NOT NULL,
  `name` varchar(2048) DEFAULT '',
  `code` varchar(2048) DEFAULT '',
  `name_optional_language` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `applicant_status`
--

INSERT INTO `applicant_status` (`id`, `name`, `code`, `name_optional_language`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'NEW/LEAD', '', '', 0, NULL, '2020-08-18 12:37:45', NULL, '2020-08-18 12:37:45'),
(2, 'SUBMITTED', '', '', 0, NULL, '2020-08-18 12:37:45', NULL, '2020-08-18 12:37:45'),
(3, 'INCOMPLETE', '', '', 0, NULL, '2020-08-18 12:37:45', NULL, '2020-08-18 12:37:45'),
(4, 'NOTQUALIFIED', '', '', 0, NULL, '2020-08-18 12:37:45', NULL, '2020-08-18 12:37:45'),
(5, 'OFFERED', '', '', 0, NULL, '2020-08-18 12:37:45', NULL, '2020-08-18 12:37:45'),
(6, 'OFFERED CONDITIONALLY', '', '', 0, NULL, '2020-08-18 12:37:45', NULL, '2020-08-18 12:37:45'),
(7, 'REGISTERED', '', '', 0, NULL, '2020-08-18 12:37:45', NULL, '2020-08-18 12:37:45'),
(8, 'REGISTERED CONDITIONALLY', '', '', 0, NULL, '2020-08-18 12:37:45', NULL, '2020-08-18 12:37:45'),
(9, 'COMPLETED', '', '', 0, NULL, '2020-08-18 12:37:45', NULL, '2020-08-18 12:37:45'),
(10, 'GRADUATED', '', '', 0, NULL, '2020-08-18 12:37:45', NULL, '2020-08-18 12:37:45'),
(11, 'DESEASED', '', '', 0, NULL, '2020-08-18 12:37:45', NULL, '2020-08-18 12:37:45'),
(12, 'WITHDRAW', '', '', 0, NULL, '2020-08-18 12:37:45', NULL, '2020-08-18 12:37:45'),
(13, 'DISMISSED/SUSPENDED', '', '', 0, NULL, '2020-08-18 12:37:45', NULL, '2020-08-18 12:37:45');

-- --------------------------------------------------------

--
-- Table structure for table `award`
--

CREATE TABLE `award` (
  `id` int(20) NOT NULL,
  `name` varchar(520) DEFAULT '',
  `level` int(20) DEFAULT 0,
  `description` varchar(500) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `id_programme` int(20) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `award`
--

INSERT INTO `award` (`id`, `name`, `level`, `description`, `code`, `id_programme`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Foundation', 1, 'Foundation', 'FDN', NULL, 1, 1, '2020-07-10 23:44:20', NULL, '2020-07-10 23:44:20'),
(2, 'Diploma', 2, 'Diploma', 'DIP', NULL, 1, 1, '2020-08-09 05:03:14', NULL, '2020-08-09 05:03:14'),
(3, 'Degree', 3, 'Degree', 'DEG', NULL, 1, 1, '2020-08-09 05:03:46', NULL, '2020-08-09 05:03:46'),
(4, 'Master', 4, 'Master', 'MAS', NULL, 1, 1, '2020-08-09 05:04:15', NULL, '2020-08-09 05:04:15'),
(5, 'PHD', 5, 'PHD', 'PHD', NULL, 1, 1, '2020-08-09 05:05:13', NULL, '2020-08-09 05:05:13'),
(6, 'Graduate Diploma', 4, 'Graduate Diploma', 'GDIP', NULL, 1, 1, '2020-08-09 05:06:39', NULL, '2020-08-09 05:06:39');

-- --------------------------------------------------------

--
-- Table structure for table `change_status`
--

CREATE TABLE `change_status` (
  `id` int(20) NOT NULL,
  `name` varchar(250) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `change_status`
--

INSERT INTO `change_status` (`id`, `name`, `code`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Inactive', 'IN', 1, 1, '2020-08-17 08:35:33', NULL, '2020-08-17 08:35:33'),
(2, 'Quit', 'CS002', 1, 1, '2020-12-06 21:46:45', NULL, '2020-12-06 21:46:45');

-- --------------------------------------------------------

--
-- Table structure for table `communication_group`
--

CREATE TABLE `communication_group` (
  `id` int(20) NOT NULL,
  `id_template` int(20) DEFAULT 0,
  `name` varchar(1024) DEFAULT '',
  `type` varchar(200) DEFAULT '',
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `reason` varchar(2048) DEFAULT '',
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `communication_group_message`
--

CREATE TABLE `communication_group_message` (
  `id` int(20) NOT NULL,
  `id_template` int(20) DEFAULT 0,
  `name` varchar(1024) DEFAULT '',
  `type` varchar(200) DEFAULT '',
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `reason` varchar(2048) DEFAULT '',
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `communication_group_message`
--

INSERT INTO `communication_group_message` (`id`, `id_template`, `name`, `type`, `status`, `created_by`, `reason`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 'Group ne', 'Student', 1, 1, '', '2020-10-29 11:52:00', NULL, '2020-10-29 11:52:00');

-- --------------------------------------------------------

--
-- Table structure for table `communication_group_message_recepients`
--

CREATE TABLE `communication_group_message_recepients` (
  `id` int(20) NOT NULL,
  `id_group` int(20) DEFAULT 0,
  `id_recepient` int(20) DEFAULT 0,
  `type` varchar(200) DEFAULT '',
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `reason` varchar(2048) DEFAULT '',
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `communication_group_message_recepients`
--

INSERT INTO `communication_group_message_recepients` (`id`, `id_group`, `id_recepient`, `type`, `status`, `created_by`, `reason`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 30, 'Student', 1, 1, '', '2020-10-29 11:52:15', NULL, '2020-10-29 11:52:15');

-- --------------------------------------------------------

--
-- Table structure for table `communication_group_recepients`
--

CREATE TABLE `communication_group_recepients` (
  `id` int(20) NOT NULL,
  `id_group` int(20) DEFAULT 0,
  `id_recepient` int(20) DEFAULT 0,
  `type` varchar(200) DEFAULT '',
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `reason` varchar(2048) DEFAULT '',
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `communication_template`
--

CREATE TABLE `communication_template` (
  `id` int(20) NOT NULL,
  `name` text DEFAULT NULL,
  `subject` text DEFAULT NULL,
  `message` text DEFAULT NULL,
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `communication_template`
--

INSERT INTO `communication_template` (`id`, `name`, `subject`, `message`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Offer Letter', 'Offer Letter', '<p>@studentname<br>\r\n@mail_address1<br>\r\n@mail_address2<br>\r\n@mailing_city<br>\r\n@mailing_zipcode</p>\r\n\r\n<p> </p>\r\n\r\n<p>Dear Sir / Madam,</p>\r\n\r\n<p><strong>OFFER FOR ADMISSION INTO POSTGRADUATE PROGRAMME 2020/2021 SESSION<br>\r\nCongratulations! We are pleased to inform you that the University has decided to offer the following programme:</strong></p>\r\n\r\n<p>PROGRAMME:@program</p>\r\n\r\n<p>FACULTY: @mode_of_program</p>\r\n\r\n<p>DURATION: @duration</p>\r\n\r\n<p>LEARNING MODE : @mode_of_program</p>\r\n\r\n<p>STUDY MODE:@mode_of_study</p>\r\n\r\n<ol>\r\n <li>This offer is subject to the following conditions:\r\n <ol>\r\n  <li> Payable fees on the registration day. Kindly refer to the attachment for more details on the tuition fees and other related fees.</li>\r\n  <li>USAS has the right to cancel this offer immediately; if any of the information provided is not true, inaccurate or does not meet the eligibility requirements set by the University.</li>\r\n  <li>USAS has the right to cancel this offer immediately; if any of the information provided is not true, inaccurate or does not meet the eligibility requirements set by the University.<br>\r\n   </li>\r\n </ol>\r\n </li>\r\n <li>You are kindly requested to read Appendix A for further details concerning the rules and regulations of admission. If you agree to the terms and conditions stated in the appendix, you are confirm  your acceptance at your application portal  not later than @intake via email to admission.usas @eag.com.my</li>\r\n</ol>\r\n\r\n<p>Thank you. Yours truly,<br>\r\n<strong>(MOHD TAJUL SABKI BIN ABDUL LATIB)</strong><br>\r\nDean<br>\r\nAdmission and Record Division Universiti Sultan Azlan Shah</p>\r\n\r\n<p> </p>\r\n\r\n<p> </p>\r\n\r\n<p><strong>FEE(S) STRUCTURE</strong></p>\r\n\r\n<p>@fee_structure<br>\r\n </p>\r\n\r\n<p><strong>APPENDIX A</strong><br>\r\n<br>\r\n<strong>ACCEPTANCE OF OFFER</strong></p>\r\n\r\n<ul>\r\n <li>To accept our offer, you will need to submit the following before the offer deadline</li>\r\n <li>Completed Confirmation of Acceptance and payment of initial fee(s) outlined in this offer.</li>\r\n</ul>\r\n\r\n<p><strong>GENERAL CONDITIONS</strong></p>\r\n\r\n<ul>\r\n <li>In the unlikely event your programme is cancelled or the University is unable to provide the programme for which you applied for, you will either: \r\n <ul>\r\n  <li>Be given a full refund of all fees paid. Refunds shall be processed and paid within 21 days from notification to you of the cancellation of your programme; or</li>\r\n  <li>At your election, in writing, apply for an alternative programme, for which the University shall determine your eligibility and proceed to issue a new offer accordingly</li>\r\n </ul>\r\n </li>\r\n <li>Enrolment, upon your acceptance of the Offer of Admission and payment of the initial fee(s), may with the approval of the University be deferred to the next intake.</li>\r\n <li>Failure to accept this offer and/or to comply with the provisions for acceptance by the offer deadline shall render this offer to be withdrawn.</li>\r\n</ul>\r\n\r\n<p><strong>ABOUT YOUR FEES:</strong></p>\r\n\r\n<ol>\r\n <li>The semester tuition fee quoted is only applicable to the programme commencing in @intake  First Semester 2020/2021 Session (the exact date will be informed to you the soonest by email). In the event of a variation between the fees on the offer letter and the approved Universiti Sultan Azlan Shah&#39;s structure of fees, the approved will prevail. For the future years of your programme, the University reserves the right to adjust the annual tuition fees</li>\r\n</ol>\r\n\r\n<p> </p>\r\n', 1, 1, '2020-08-10 06:37:43', NULL, '2020-08-10 06:37:43'),
(2, 'OFFER LETTER UNDERGRADUATE INTERNATIONAL', 'OFFER LETTER UNDERGRADUATE INTERNATIONAL', '<p>@DATELETTERGENERATED 02 June 2020</p>\r\n\r\n<p> </p>\r\n\r\n<p><strong>@APPLICANTFULLNAME DENISSE ANANDA PRATAMA</strong></p>\r\n\r\n<p>&lt;@mail_address1> JLN.TERAMPIL NO.14 KOMP TNI AL KODAMAR KELAPA GADING BARAT 14240,</p>\r\n\r\n<p>&lt;@mailing_zipcode>14240,</p>\r\n\r\n<p>@MAILINGCITY JAKARTA UTARA,</p>\r\n\r\n<p>@MAILINGSTATE DKI JAKARTA, @MAILINGCOUNTRYINDONESIA</p>\r\n\r\n<p> </p>\r\n\r\n<p>Dear Sir/Madam,</p>\r\n\r\n<p> </p>\r\n\r\n<p><strong>OFFER FOR UNDERGRADUATE ADMISSION.</strong></p>\r\n\r\n<p> </p>\r\n\r\n<p>It is our pleasure to offer you a place in our undergraduate programme.</p>\r\n\r\n<p>The details of the offer are as follows:</p>\r\n\r\n<p>Programme: @program <strong>BACHELOR OF INFORMATION AND COMMUNICATION TECHNOLOGY (HONS) </strong>&lt;@ACCREDITATION CODE> <strong>(R2-DL/481/6/0482) (A9489) (05/26)</strong></p>\r\n\r\n<p>Intake: @intake <strong>May 2020 Semester</strong></p>\r\n\r\n<p>Commencement Date:   @REGISTRATIONDATE <strong>04 May 2020</strong></p>\r\n\r\n<p>School: @FACULTY <strong>SCHOOL OF SCIENCE & TECHNOLOGY</strong></p>\r\n\r\n<p>Duration of Study: <strong>Shall Normally be </strong>@PROGRAMMINIMUMDURATIONINYEARS 4 <strong>years</strong></p>\r\n\r\n<p>Mode of Study: @mode_of_study <strong>Blended</strong></p>\r\n\r\n<p>Programme Structure:  @PROGRAMMETYPE <strong>Coursework</strong></p>\r\n\r\n<p> </p>\r\n\r\n<p>Please note that the above offer is conditional since all students are required to fulfil their English Language Competency Requirement (ELCR). The ELCR requirement must be fulfilled within a period of two years. Failure to comply within two years after registration may result in the withdrawal of this letter of offer and admission into AeU.</p>\r\n\r\n<p> </p>\r\n\r\n<p>For verification and record purposes, you are required to submit a certified true copy of citizenship card or passport, two most recent coloured passport size photographs and your academic certificates and transcripts during registration.</p>\r\n\r\n<p> </p>\r\n\r\n<p>This offer is valid for TWO years (6 consecutive intakes of January, May or September) only. It will automatically lapse after two years and you need to reapply again should you wish to seek admission to any of the programmes of the University.</p>\r\n\r\n<p> </p>\r\n\r\n<p>The University would like to congratulate and welcome you to the programme in your quest towards academic and career advancement. Should you have any further enquiries, kindly contact Admission & Records Department at 1300-300-238 or email to admission@aeu.edu.my.</p>\r\n\r\n<p> </p>\r\n\r\n<p>We wish you every success.</p>\r\n\r\n<p> </p>\r\n\r\n<p>Thank you.</p>\r\n\r\n<p> </p>\r\n\r\n<p>YUSOFF MUSA</p>\r\n\r\n<p>Registrar</p>\r\n\r\n<p>-------------------------------------------------------------------------------------------------------------------------------------------------------------------------.</p>\r\n\r\n<p>                                            *****Computer Generated Document. No Signature Required*****</p>\r\n\r\n<p>                                           ------------------------------------------------------------------------------------------</p>\r\n\r\n<p>Admissions & Records, Asia e University, Wisma Subang Jaya, No 106, Jalan SS15/4, 47500 Subang Jaya, Selangor, Malaysia. Tel No: 1-300-300-238 Fax No: 03-2785 0001 Website: www.aeu.edu.my</p>\r\n', 1, 1, '2020-08-10 07:23:22', NULL, '2020-08-10 07:23:22'),
(3, 'OFFER LETTER UNDERGRADUATE LOCAL', 'OFFER LETTER UNDERGRADUATE LOCAL', '<p>@DATELETTERGENERATED 02 June 2020</p>\r\n\r\n<p> </p>\r\n\r\n<p><strong>@APPLICANTFULLNAME DENISSE ANANDA PRATAMA</strong></p>\r\n\r\n<p>&lt;@mail_address1> JLN.TERAMPIL NO.14 KOMP TNI AL KODAMAR KELAPA GADING BARAT 14240,</p>\r\n\r\n<p>&lt;@mailing_zipcode>14240,</p>\r\n\r\n<p>@MAILINGCITY JAKARTA UTARA,</p>\r\n\r\n<p>@MAILINGSTATE DKI JAKARTA, @MAILINGCOUNTRYINDONESIA</p>\r\n\r\n<p>Dear Sir/Madam,</p>\r\n\r\n<p> </p>\r\n\r\n<p><strong>OFFER FOR UNDERGRADUATE ADMISSION.</strong></p>\r\n\r\n<p> </p>\r\n\r\n<p>It is our pleasure to offer you a place in our undergraduate programme.</p>\r\n\r\n<p>The details of the offer are as follows:</p>\r\n\r\n<p>Programme: @program <strong>BACHELOR OF INFORMATION AND COMMUNICATION TECHNOLOGY (HONS) </strong>&lt;@ACCREDITATION CODE> <strong>(R2-DL/481/6/0482) (A9489) (05/26)</strong></p>\r\n\r\n<p>Intake: @intake <strong>May 2020 Semester</strong></p>\r\n\r\n<p>Commencement Date:   @REGISTRATIONDATE <strong>04 May 2020</strong></p>\r\n\r\n<p>School: @FACULTY <strong>SCHOOL OF SCIENCE & TECHNOLOGY</strong></p>\r\n\r\n<p>Duration of Study: <strong>Shall Normally be </strong>@PROGRAMMINIMUMDURATIONINYEARS 4 <strong>years</strong></p>\r\n\r\n<p>Mode of Study: @mode_of_study <strong>Blended</strong></p>\r\n\r\n<p>Programme Structure:  @PROGRAMMETYPE <strong>Coursework</strong></p>\r\n\r\n<p>For verification and record purposes, you are required to submit a certified true copy of citizenship card or passport, two most recent coloured passport size photographs and your academic certificates and transcripts during registration.</p>\r\n\r\n<p><br>\r\nThis offer is valid for ONE year (3 consecutive intakes of January, May or September) only. It will automatically lapse after one year and you need to reapply again should you wish to seek admission to any of the programmes of the University.</p>\r\n\r\n<p>The University would like to congratulate and welcome you to the programme in your quest towards academic and career advancement. Should you have any further enquiries, kindly contact Admission & Records Department at 1300-300-238 or email to admission@aeu.edu.my.</p>\r\n\r\n<p> </p>\r\n\r\n<p>We wish you every success.</p>\r\n\r\n<p> </p>\r\n\r\n<p>Thank you.</p>\r\n\r\n<p> </p>\r\n\r\n<p>YUSOFF MUSA</p>\r\n\r\n<p>Registrar</p>\r\n\r\n<p>-------------------------------------------------------------------------------------------------------------------------------------------------------------------------.</p>\r\n\r\n<p>                                            *****Computer Generated Document. No Signature Required*****</p>\r\n\r\n<p>                                           ------------------------------------------------------------------------------------------</p>\r\n\r\n<p>Admissions & Records, Asia e University, Wisma Subang Jaya, No 106, Jalan SS15/4, 47500 Subang Jaya, Selangor, Malaysia. Tel No: 1-300-300-238 Fax No: 03-2785 0001 Website: www.aeu.edu.my</p>\r\n', 1, 1, '2020-09-01 16:34:08', NULL, '2020-09-01 16:34:08'),
(4, 'OFFER LETTER POSTGRADUATE LOCAL', 'OFFER LETTER POSTGRADUATE LOCAL', '<p>@DATELETTERGENERATED 02 June 2020</p>\r\n\r\n<p> </p>\r\n\r\n<p><strong>@APPLICANTFULLNAME DENISSE ANANDA PRATAMA</strong></p>\r\n\r\n<p>&lt;@mail_address1> JLN.TERAMPIL NO.14 KOMP TNI AL KODAMAR KELAPA GADING BARAT 14240,</p>\r\n\r\n<p>&lt;@mailing_zipcode>14240,</p>\r\n\r\n<p>@MAILINGCITY JAKARTA UTARA,</p>\r\n\r\n<p>@MAILINGSTATE DKI JAKARTA, @MAILINGCOUNTRYINDONESIA</p>\r\n\r\n<p>Dear Sir/Madam,</p>\r\n\r\n<p> </p>\r\n\r\n<p><strong>OFFER FOR UNDERGRADUATE ADMISSION.</strong></p>\r\n\r\n<p> </p>\r\n\r\n<p>It is our pleasure to offer you a place in our undergraduate programme.</p>\r\n\r\n<p>The details of the offer are as follows:</p>\r\n\r\n<p>Programme: @program <strong>BACHELOR OF INFORMATION AND COMMUNICATION TECHNOLOGY (HONS) </strong>&lt;@ACCREDITATION CODE> <strong>(R2-DL/481/6/0482) (A9489) (05/26)</strong></p>\r\n\r\n<p>Intake: @intake <strong>May 2020 Semester</strong></p>\r\n\r\n<p>Commencement Date:   @REGISTRATIONDATE <strong>04 May 2020</strong></p>\r\n\r\n<p>School: @FACULTY <strong>SCHOOL OF SCIENCE & TECHNOLOGY</strong></p>\r\n\r\n<p>Duration of Study: <strong>Shall Normally be </strong>@PROGRAMMINIMUMDURATIONINYEARS 4 <strong>years</strong></p>\r\n\r\n<p>Mode of Study: @mode_of_study <strong>Blended</strong></p>\r\n\r\n<p>Programme Structure:  @PROGRAMMETYPE <strong>Coursework</strong></p>\r\n\r\n<p>For verification and record purposes, you are required to submit a certified true copy of citizenship card or passport, two most recent coloured passport size photographs and your academic certificates and transcripts during registration.</p>\r\n\r\n<p><br>\r\nThis offer is valid for ONE year (3 consecutive intakes of January, May or September) only. It will automatically lapse after one year and you need to reapply again should you wish to seek admission to any of the programmes of the University.</p>\r\n\r\n<p>The University would like to congratulate and welcome you to the programme in your quest towards academic and career advancement. Should you have any further enquiries, kindly contact Admission & Records Department at 1300-300-238 or email to admission@aeu.edu.my.</p>\r\n\r\n<p> </p>\r\n\r\n<p>We wish you every success.</p>\r\n\r\n<p> </p>\r\n\r\n<p>Thank you.</p>\r\n\r\n<p> </p>\r\n\r\n<p>YUSOFF MUSA</p>\r\n\r\n<p>Registrar</p>\r\n\r\n<p>-------------------------------------------------------------------------------------------------------------------------------------------------------------------------.</p>\r\n\r\n<p>                                            *****Computer Generated Document. No Signature Required*****</p>\r\n\r\n<p>                                           ------------------------------------------------------------------------------------------</p>\r\n\r\n<p>Admissions & Records, Asia e University, Wisma Subang Jaya, No 106, Jalan SS15/4, 47500 Subang Jaya, Selangor, Malaysia. Tel No: 1-300-300-238 Fax No: 03-2785 0001 Website: www.aeu.edu.my</p>\r\n', 1, 1, '2020-09-01 16:34:30', NULL, '2020-09-01 16:34:30'),
(5, 'OFFER LETTER POSTGRADUATE INTERNATIONAL', 'OFFER LETTER POSTGRADUATE INTERNATIONAL', '<p>@DATELETTERGENERATED 02 June 2020</p>\r\n\r\n<p> </p>\r\n\r\n<p><strong>@APPLICANTFULLNAME DENISSE ANANDA PRATAMA</strong></p>\r\n\r\n<p>&lt;@mail_address1> JLN.TERAMPIL NO.14 KOMP TNI AL KODAMAR KELAPA GADING BARAT 14240,</p>\r\n\r\n<p>&lt;@mailing_zipcode>14240,</p>\r\n\r\n<p>@MAILINGCITY JAKARTA UTARA,</p>\r\n\r\n<p>@MAILINGSTATE DKI JAKARTA, @MAILINGCOUNTRYINDONESIA</p>\r\n\r\n<p> </p>\r\n\r\n<p>Dear Sir/Madam,</p>\r\n\r\n<p> </p>\r\n\r\n<p><strong>OFFER FOR UNDERGRADUATE ADMISSION.</strong></p>\r\n\r\n<p> </p>\r\n\r\n<p>It is our pleasure to offer you a place in our undergraduate programme.</p>\r\n\r\n<p>The details of the offer are as follows:</p>\r\n\r\n<p>Programme: @program <strong>BACHELOR OF INFORMATION AND COMMUNICATION TECHNOLOGY (HONS) </strong>&lt;@ACCREDITATION CODE> <strong>(R2-DL/481/6/0482) (A9489) (05/26)</strong></p>\r\n\r\n<p>Intake: @intake <strong>May 2020 Semester</strong></p>\r\n\r\n<p>Commencement Date:   @REGISTRATIONDATE <strong>04 May 2020</strong></p>\r\n\r\n<p>School: @FACULTY <strong>SCHOOL OF SCIENCE & TECHNOLOGY</strong></p>\r\n\r\n<p>Duration of Study: <strong>Shall Normally be </strong>@PROGRAMMINIMUMDURATIONINYEARS 4 <strong>years</strong></p>\r\n\r\n<p>Mode of Study: @mode_of_study <strong>Blended</strong></p>\r\n\r\n<p>Programme Structure:  @PROGRAMMETYPE <strong>Coursework</strong></p>\r\n\r\n<p> </p>\r\n\r\n<p>Please note that the above offer is conditional since all students are required to fulfil their English Language Competency Requirement (ELCR). The ELCR requirement must be fulfilled within a period of one year. You are however allowed to register for both the ELCR and your course subjects simultaneously. Failure to comply within a year after registration may result in the withdrawal of this letter of offer and admission into AeU.</p>\r\n\r\n<p>For verification and record purposes, you are required to submit a certified true copy of citizenship card or passport, two most recent coloured passport size photographs and your academic certificates and transcripts during registration.</p>\r\n\r\n<p>This offer is valid for ONE year (3 consecutive intakes of January, May or September) only. It will automatically lapse after one year and you need to reapply again should you wish to seek admission to any of the programmes of the University.</p>\r\n\r\n<p>The University would like to congratulate and welcome you to the programme in your quest towards academic and career advancement. Should you have any further enquiries, kindly contact Admission & Records Department at 1300-300-238 or email to admission@aeu.edu.my.</p>\r\n\r\n<p>We wish you every success.</p>\r\n\r\n<p> </p>\r\n\r\n<p>Thank you.</p>\r\n\r\n<p> </p>\r\n\r\n<p>YUSOFF MUSA</p>\r\n\r\n<p>Registrar</p>\r\n\r\n<p>-------------------------------------------------------------------------------------------------------------------------------------------------------------------------.</p>\r\n\r\n<p>                                            *****Computer Generated Document. No Signature Required*****</p>\r\n\r\n<p>                                           ------------------------------------------------------------------------------------------</p>\r\n\r\n<p>Admissions & Records, Asia e University, Wisma Subang Jaya, No 106, Jalan SS15/4, 47500 Subang Jaya, Selangor, Malaysia. Tel No: 1-300-300-238 Fax No: 03-2785 0001 Website: www.aeu.edu.my</p>\r\n', 1, 1, '2020-09-01 16:34:52', NULL, '2020-09-01 16:34:52'),
(6, 'Registration Email', 'Registration Email', '<p>Dear @salutation @studentname,</p>\r\n\r\n<p> </p>\r\n\r\n<p>Welcome to East Asia Global Education. We are an Education Provider that develop and offer educational, academic, and professional programmes in collaboration with outstanding universities and training institutions.</p>\r\n\r\n<p>Please login to view and apply for the programmes offered by our partner universities.</p>\r\n\r\n<p> </p>\r\n\r\n<p>Your login details are as follow:</p>\r\n\r\n<p>URL: http://eag-applicant.camsedu.com/applicantLogin</p>\r\n\r\n<p>Should you have any inquiry or assistance you can chat with our Academic Counsellor after your login to the Portal or email: admission.usas@eag.com.my</p>\r\n', 1, 1, '2021-01-17 15:20:33', NULL, '2021-01-17 15:20:33');

-- --------------------------------------------------------

--
-- Table structure for table `communication_template_message`
--

CREATE TABLE `communication_template_message` (
  `id` int(20) NOT NULL,
  `name` text DEFAULT NULL,
  `subject` text DEFAULT NULL,
  `message` text DEFAULT NULL,
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `communication_template_message`
--

INSERT INTO `communication_template_message` (`id`, `name`, `subject`, `message`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Template One', 'Subject One', '<p>Dara</p>\r\n\r\n<p><strong>Data</strong></p>\r\n', 1, 1, '2020-10-29 11:50:56', NULL, '2020-10-29 11:50:56');

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `id` int(20) NOT NULL,
  `name` varchar(120) DEFAULT '',
  `phone_code` varchar(20) DEFAULT NULL,
  `status` int(2) DEFAULT 1,
  `created_by` int(2) DEFAULT 0,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(2) DEFAULT 0,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`id`, `name`, `phone_code`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Malaysia', '+61', 1, 1, '2020-07-10 23:22:08', 0, '2020-07-10 23:22:08'),
(2, 'Afghanistan', '+93', 1, 1, '2020-07-10 23:22:19', 0, '2020-07-10 23:22:19'),
(3, 'Albania', '+355', 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(4, 'Algeria', '+213', 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(5, 'Andorra', '+376', 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(6, 'Angola', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(7, 'Antigua and Barbuda', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(8, 'Argentina', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(9, 'Armenia', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(10, 'Australia', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(11, 'Austria', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(12, 'Azerbaijan', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(13, 'Bahamas', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(14, 'Bahrain', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(15, 'Bangladesh', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(16, 'Barbados', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(17, 'Belarus', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(18, 'Belgium', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(19, 'Belize', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(20, 'Benin', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(21, 'Bhutan', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(22, 'Bolivia', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(23, 'Bosnia and Herzegovina', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(24, 'Botswana', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(25, 'Brazil', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(26, 'Brunei', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(27, 'Bulgaria', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(28, 'Burkina Faso', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(29, 'Burma', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(30, 'Burundi', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(31, 'Cambodia', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(32, 'Cameroon', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(33, 'Canada', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(34, 'Cape Verde', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(35, 'Central African Republic', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(36, 'Chad', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(37, 'Chile', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(38, 'China', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(39, 'Colombia', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(40, 'Comoros', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(41, 'Congo', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(42, 'Cook Islands', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(43, 'Costa Rica', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(44, 'Croatia', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(45, 'Cuba', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(46, 'Cyprus', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(47, 'Czech Republic', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(48, 'Côte d\'Ivoire', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(49, 'Denmark', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(50, 'Djibouti', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(51, 'Dominica', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(52, 'Dominican Republic', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(53, 'East Timor', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(54, 'Ecuador', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(55, 'Egypt', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(56, 'El Salvador', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(57, 'Equatorial Guinea', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(58, 'Eritrea', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(59, 'Estonia', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(60, 'Ethiopia', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(61, 'Fiji', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(62, 'Finland', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(63, 'France', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(64, 'Gabon', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(65, 'Gambia', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(66, 'Georgia', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(67, 'Germany', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(68, 'Ghana', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(69, 'Greece', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(70, 'Grenada', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(71, 'Guatemala', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(72, 'Guinea', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(73, 'Guinea-Bissau', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(74, 'Guyana', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(75, 'Haiti', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(76, 'Honduras', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(77, 'Hungary', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(78, 'Iceland', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(79, 'India', '+91', 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(80, 'Indonesia', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(81, 'Iran', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(82, 'Iraq', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(83, 'Ireland', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(84, 'Israel', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(85, 'Italy', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(86, 'Ivory Coast', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(87, 'Jamaica', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(88, 'Japan', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(89, 'Jordan', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(90, 'Kazakhstan', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(91, 'Kenya', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(92, 'Kiribati', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(93, 'Korea, North', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(94, 'Korea, South', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(95, 'Kosovo', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(96, 'Kuwait', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(97, 'Kyrgyzstan', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(98, 'Laos', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(99, 'Latvia', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(100, 'Lebanon', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(101, 'Lesotho', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(102, 'Liberia', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(103, 'Libya', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(104, 'Liechtenstein', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(105, 'Lithuania', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(106, 'Luxembourg', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(107, 'Macedonia', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(108, 'Madagascar', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(109, 'Malawi', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(110, 'Abkhazia', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(111, 'Maldives', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(112, 'Mali', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(113, 'Malta', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(114, 'Marshall Islands', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(115, 'Mauritania', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(116, 'Mauritius', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(117, 'Mexico', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(118, 'Micronesia', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(119, 'Moldova', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(120, 'Monaco', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(121, 'Mongolia', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(122, 'Montenegro', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(123, 'Morocco', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(124, 'Mozambique', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(125, 'Myanmar / Burma', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(126, 'Nagorno-Karabakh', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(127, 'Namibia', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(128, 'Nauru', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(129, 'Nepal', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(130, 'Netherlands', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(131, 'New Zealand', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(132, 'Nicaragua', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(133, 'Niger', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(134, 'Nigeria', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(135, 'Niue', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(136, 'Northern Cyprus', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(137, 'Norway', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(138, 'Oman', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(139, 'Pakistan', '+92', 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(140, 'Palau', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(141, 'Palestine', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(142, 'Panama', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(143, 'Papua New Guinea', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(144, 'Paraguay', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(145, 'Peru', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(146, 'Philippines', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(147, 'Poland', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(148, 'Portugal', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(149, 'Qatar', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(150, 'Romania', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(151, 'Russia', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(152, 'Rwanda', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(153, 'Sahrawi Arab Democratic Republic', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(154, 'Saint Kitts and Nevis', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(155, 'Saint Lucia', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(156, 'Saint Vincent and the Grenadines', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(157, 'Samoa', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(158, 'San Marino', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(159, 'Saudi Arabia', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(160, 'Senegal', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(161, 'Serbia', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(162, 'Seychelles', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(163, 'Sierra Leone', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(164, 'Singapore', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(165, 'Slovakia', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(166, 'Slovenia', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(167, 'Solomon Islands', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(168, 'Somalia', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(169, 'Somaliland', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(170, 'South Africa', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(171, 'South Ossetia', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(172, 'Spain', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(173, 'Sri Lanka', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(174, 'Sudan', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(175, 'Suriname', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(176, 'Swaziland', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(177, 'Sweden', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(178, 'Switzerland', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(179, 'Syria', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(180, 'São Tomé and Príncipe', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(181, 'Taiwan', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(182, 'Tajikistan', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(183, 'Tanzania', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(184, 'Thailand', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(185, 'Timor-Leste / East Timor', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(186, 'Togo', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(187, 'Tonga', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(188, 'Trinidad and Tobago', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(189, 'Tunisia', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(190, 'Turkey', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(191, 'Turkmenistan', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(192, 'Tuvalu', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(193, 'Uganda', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(194, 'Ukraine', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(195, 'United Arab Emirates', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(196, 'United Kingdom', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(197, 'United States', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(198, 'Uruguay', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(199, 'Uzbekistan', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(200, 'Vanuatu', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(201, 'Vatican City', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(202, 'Venezuela', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(203, 'Vietnam', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(204, 'Yemen', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(205, 'Zambia', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(206, 'Zimbabwe', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05');

-- --------------------------------------------------------

--
-- Table structure for table `currency_rate_setup`
--

CREATE TABLE `currency_rate_setup` (
  `id` int(20) NOT NULL,
  `id_currency` int(20) DEFAULT 0,
  `exchange_rate` varchar(200) DEFAULT '',
  `min_rate` varchar(200) DEFAULT '',
  `max_rate` varchar(200) DEFAULT '',
  `effective_date` datetime DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `currency_setup`
--

CREATE TABLE `currency_setup` (
  `id` int(20) NOT NULL,
  `code` varchar(2048) DEFAULT '',
  `name` varchar(2048) DEFAULT '',
  `name_optional_language` varchar(2048) DEFAULT '',
  `prefix` varchar(200) DEFAULT '',
  `suffix` varchar(200) DEFAULT '',
  `decimal_place` int(20) DEFAULT 0,
  `default` int(2) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `currency_setup`
--

INSERT INTO `currency_setup` (`id`, `code`, `name`, `name_optional_language`, `prefix`, `suffix`, `decimal_place`, `default`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'MYR', 'MYR', 'Ringgit Malaysia', 'MYR', 'MYR', 2, 1, 1, 1, '2020-08-09 01:58:47', NULL, '2020-08-09 01:58:47'),
(2, 'USD', 'USD ', 'US Dollar', 'USD', 'USD', 2, 0, 1, 1, '2020-08-20 09:24:33', NULL, '2020-08-20 09:24:33'),
(3, 'LKR', 'SRI LANKA RUPEE', 'SRI LANKA RUPEE', 'LKR', 'LKR', 2, 0, 1, 1, '2020-09-02 00:44:50', NULL, '2020-09-02 00:44:50');

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE `department` (
  `id` int(20) NOT NULL,
  `name` varchar(200) DEFAULT '',
  `code` varchar(120) DEFAULT '',
  `description` varchar(520) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `department`
--

INSERT INTO `department` (`id`, `name`, `code`, `description`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'School of Management', 'SOM', 'School of Management', 1, NULL, '2020-07-10 23:27:51', NULL, '2020-07-10 23:27:51'),
(2, 'Finance', 'F&A', 'Accounts & Finance', 1, NULL, '2020-11-13 09:22:34', NULL, '2020-11-13 09:22:34'),
(3, 'Collection Boy', 'CB', 'Collection Boy', 1, NULL, '2021-01-23 22:36:54', NULL, '2021-01-23 22:36:54'),
(4, 'dasda', 'dsadas', 'dasda', 1, NULL, '2021-02-11 18:01:06', NULL, '2021-02-11 18:01:06');

-- --------------------------------------------------------

--
-- Table structure for table `department_has_staff`
--

CREATE TABLE `department_has_staff` (
  `id` int(20) NOT NULL,
  `id_staff` int(20) DEFAULT 0,
  `id_department` int(20) DEFAULT 0,
  `role` varchar(1024) DEFAULT '',
  `effective_date` datetime DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `department_has_staff`
--

INSERT INTO `department_has_staff` (`id`, `id_staff`, `id_department`, `role`, `effective_date`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(0, 4, 4, 'DEAN', '2019-09-09 00:00:00', NULL, NULL, '2021-02-11 18:04:59', NULL, '2021-02-11 18:04:59');

-- --------------------------------------------------------

--
-- Table structure for table `documents`
--

CREATE TABLE `documents` (
  `id` int(20) NOT NULL,
  `name` varchar(520) DEFAULT '',
  `description` varchar(500) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `id_programme` int(20) DEFAULT NULL,
  `file_format` varchar(1024) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp(),
  `is_required` int(10) DEFAULT NULL,
  `file_size` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `documents`
--

INSERT INTO `documents` (`id`, `name`, `description`, `code`, `id_programme`, `file_format`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`, `is_required`, `file_size`) VALUES
(1, 'Copy of NRIC or Passport', '', 'DOC_01', NULL, '1,2,3', 1, 1, '2020-07-11 00:16:40', NULL, '2020-07-11 00:16:40', 1, '3'),
(2, 'Passport Size Photo', '', 'DOC_02', NULL, '1,2,3,4', 1, 1, '2020-07-11 00:17:34', NULL, '2020-07-11 00:17:34', 1, '5'),
(3, 'Copy of Academic Transcripts', '', 'DOC_03', NULL, '1,2', 1, 1, '2020-09-13 14:25:11', NULL, '2020-09-13 14:25:11', 1, '3'),
(4, 'Copy of Scroll', '', 'DOC_04', NULL, '4', 1, 1, '2020-11-14 11:45:34', NULL, '2020-11-14 11:45:34', 1, '3');

-- --------------------------------------------------------

--
-- Table structure for table `documents_program`
--

CREATE TABLE `documents_program` (
  `id` int(20) NOT NULL,
  `id_document` int(20) DEFAULT 0,
  `description` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `documents_program`
--

INSERT INTO `documents_program` (`id`, `id_document`, `description`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 'English', 1, 1, '2020-07-11 00:18:03', NULL, '2020-07-11 00:18:03');

-- --------------------------------------------------------

--
-- Table structure for table `documents_program_details`
--

CREATE TABLE `documents_program_details` (
  `id` int(20) NOT NULL,
  `id_documents_program` int(20) DEFAULT 0,
  `id_document` int(20) DEFAULT 0,
  `id_program` int(20) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `documents_program_details`
--

INSERT INTO `documents_program_details` (`id`, `id_documents_program`, `id_document`, `id_program`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(4, 0, 2, 3, NULL, NULL, '2020-08-22 23:44:26', NULL, '2020-08-22 23:44:26'),
(5, 0, 1, 4, NULL, NULL, '2020-09-13 14:24:20', NULL, '2020-09-13 14:24:20'),
(6, 0, 1, 3, NULL, NULL, '2020-09-13 14:24:25', NULL, '2020-09-13 14:24:25'),
(7, 0, 3, 4, NULL, NULL, '2020-09-13 14:25:27', NULL, '2020-09-13 14:25:27'),
(8, 0, 1, 5, NULL, NULL, '2020-10-28 00:33:06', NULL, '2020-10-28 00:33:06'),
(9, 0, 3, 6, NULL, NULL, '2020-11-08 10:04:56', NULL, '2020-11-08 10:04:56'),
(10, 0, 1, 6, NULL, NULL, '2020-11-08 10:05:06', NULL, '2020-11-08 10:05:06'),
(11, 0, 4, 7, NULL, NULL, '2020-11-14 11:47:34', NULL, '2020-11-14 11:47:34'),
(12, 0, 1, 8, NULL, NULL, '2020-12-10 00:28:18', NULL, '2020-12-10 00:28:18'),
(13, 0, 1, 10, NULL, NULL, '2021-01-12 22:08:09', NULL, '2021-01-12 22:08:09'),
(14, 0, 3, 10, NULL, NULL, '2021-01-12 22:08:20', NULL, '2021-01-12 22:08:20'),
(15, 0, 3, 11, NULL, NULL, '2021-01-14 23:59:13', NULL, '2021-01-14 23:59:13'),
(16, 0, 1, 11, NULL, NULL, '2021-01-14 23:59:30', NULL, '2021-01-14 23:59:30'),
(17, 0, 4, 10, NULL, NULL, '2021-01-14 23:59:41', NULL, '2021-01-14 23:59:41'),
(18, 0, 4, 11, NULL, NULL, '2021-01-14 23:59:45', NULL, '2021-01-14 23:59:45'),
(19, 0, 2, 10, NULL, NULL, '2021-01-14 23:59:54', NULL, '2021-01-14 23:59:54'),
(20, 0, 2, 11, NULL, NULL, '2021-01-14 23:59:58', NULL, '2021-01-14 23:59:58');

-- --------------------------------------------------------

--
-- Table structure for table `document_checklist`
--

CREATE TABLE `document_checklist` (
  `id` int(20) NOT NULL,
  `document_name` varchar(520) DEFAULT '',
  `id_category` int(20) DEFAULT NULL,
  `mandetory` varchar(50) DEFAULT '',
  `id_program` int(20) DEFAULT NULL,
  `status` int(20) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `equipment`
--

CREATE TABLE `equipment` (
  `id` int(20) NOT NULL,
  `name` varchar(520) DEFAULT '',
  `name_in_malay` varchar(520) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `equipment`
--

INSERT INTO `equipment` (`id`, `name`, `name_in_malay`, `code`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Laptop', 'Laptop', 'Laptop', 1, 1, '2020-09-21 14:13:37', NULL, '2020-09-21 14:13:37'),
(2, 'Projector', 'Projector', 'Projector', 1, 1, '2020-09-21 14:13:37', NULL, '2020-09-21 14:13:37');

-- --------------------------------------------------------

--
-- Table structure for table `file_type`
--

CREATE TABLE `file_type` (
  `id` int(20) NOT NULL,
  `name` varchar(512) DEFAULT NULL,
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `file_type`
--

INSERT INTO `file_type` (`id`, `name`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'PDF', 1, NULL, '2021-01-17 00:39:12', NULL, NULL),
(2, 'PNG', 1, NULL, '2021-01-17 00:39:12', NULL, NULL),
(3, 'JPEG', 1, NULL, '2021-01-17 00:39:12', NULL, NULL),
(4, 'JPG', 1, NULL, '2021-01-17 00:39:12', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `main_invoice`
--

CREATE TABLE `main_invoice` (
  `id` int(20) NOT NULL,
  `type` varchar(50) DEFAULT '',
  `id_program` int(20) DEFAULT 0,
  `id_intake` int(20) DEFAULT 0,
  `invoice_number` varchar(50) DEFAULT '',
  `type_of_invoice` varchar(50) DEFAULT '',
  `date_time` datetime DEFAULT current_timestamp(),
  `remarks` varchar(520) DEFAULT '',
  `id_application` int(10) DEFAULT NULL,
  `id_sponser` int(20) DEFAULT 0,
  `id_student` int(10) DEFAULT NULL,
  `no_count` int(20) DEFAULT 0,
  `applicant_partner_fee` int(20) DEFAULT 0,
  `currency` varchar(200) DEFAULT '',
  `total_amount` float(20,2) DEFAULT 0.00,
  `invoice_total` float(20,2) DEFAULT 0.00,
  `total_discount` float(20,2) DEFAULT 0.00,
  `balance_amount` float(20,2) DEFAULT 0.00,
  `paid_amount` float(20,2) DEFAULT 0.00,
  `is_migrate_applicant` int(20) DEFAULT 0,
  `reason` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `main_invoice`
--

INSERT INTO `main_invoice` (`id`, `type`, `id_program`, `id_intake`, `invoice_number`, `type_of_invoice`, `date_time`, `remarks`, `id_application`, `id_sponser`, `id_student`, `no_count`, `applicant_partner_fee`, `currency`, `total_amount`, `invoice_total`, `total_discount`, `balance_amount`, `paid_amount`, `is_migrate_applicant`, `reason`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(65, 'Applicant', 10, 3, 'INV000001/2021', '', '2021-01-15 01:29:23', 'Application Registration Fee', 1, 0, 128, 0, 0, '1', 590.00, 590.00, 0.00, 0.00, 590.00, 128, '', 1, 1, '2021-01-15 01:29:23', NULL, '2021-01-15 01:29:23'),
(66, 'Applicant', 10, 3, 'INV000002/2021', '', '2021-01-15 01:30:14', 'Application Registration Fee', 1, 0, 127, 0, 0, '1', 220.00, 220.00, 0.00, 0.00, 220.00, 127, '', 1, 1, '2021-01-15 01:30:14', NULL, '2021-01-15 01:30:14'),
(67, 'Applicant', 10, 3, 'INV000003/2021', '', '2021-01-15 01:33:29', 'Application Registration Fee', 1, 0, 124, 0, 0, '1', 220.00, 220.00, 0.00, 220.00, 0.00, 124, '', 1, 1, '2021-01-15 01:33:29', NULL, '2021-01-15 01:33:29'),
(68, 'Applicant', 10, 3, 'INV000004/2021', '', '2021-01-15 10:55:16', 'Application Registration Fee', 1, 0, 130, 0, 0, '1', 120.00, 120.00, 0.00, 0.00, 120.00, 130, '', 1, 1, '2021-01-15 10:55:16', NULL, '2021-01-15 10:55:16'),
(76, 'Applicant', 10, 3, 'INV000012/2021', '', '2021-01-17 23:11:07', 'OFFER ACCEPTED Fee', 1, 0, 137, 0, 0, '1', 220.00, 220.00, 0.00, 0.00, 220.00, 137, '', 1, 1, '2021-01-17 23:11:07', NULL, '2021-01-17 23:11:07');

-- --------------------------------------------------------

--
-- Table structure for table `main_invoice_details`
--

CREATE TABLE `main_invoice_details` (
  `id` int(20) NOT NULL,
  `id_main_invoice` int(10) DEFAULT NULL,
  `id_fee_item` int(10) DEFAULT NULL,
  `amount` float(20,2) DEFAULT NULL,
  `quantity` int(20) DEFAULT 0,
  `price` int(20) DEFAULT 0,
  `id_reference` int(20) DEFAULT 0,
  `description` varchar(1024) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `main_invoice_details`
--

INSERT INTO `main_invoice_details` (`id`, `id_main_invoice`, `id_fee_item`, `amount`, `quantity`, `price`, `id_reference`, `description`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(96, 52, 6, 12000.00, 1, 12000, 111, 'Application Registration Fee', 1, 1, '2020-12-25 12:00:46', NULL, '2020-12-25 12:00:46'),
(97, 52, 8, 8000.00, 1, 8000, 111, 'Application Registration Fee', 1, 1, '2020-12-25 12:00:46', NULL, '2020-12-25 12:00:46'),
(98, 53, 8, 200.00, 1, 200, 218, 'Application Registration Fee', 1, 1, '2020-12-25 12:02:44', NULL, '2020-12-25 12:02:44'),
(99, 54, 8, 300.00, 1, 300, 223, 'Application Registration Fee', 1, 1, '2020-12-25 12:03:16', NULL, '2020-12-25 12:03:16'),
(100, 55, 12, 1000.00, 1, 1000, 241, 'Student Course Registration Per Semester Trigger Fee', 1, 1, '2020-12-25 12:30:40', NULL, '2020-12-25 12:30:40'),
(101, 55, 7, 6000.00, 1, 6000, 58, 'Student Course Registration Installment Trigger Fee', 1, 1, '2020-12-25 12:30:40', NULL, '2020-12-25 12:30:40'),
(102, 56, 12, 1000.00, 1, 1000, 241, 'Student Course Registration Per Semester Trigger Fee', 1, 1, '2020-12-25 12:32:34', NULL, '2020-12-25 12:32:34'),
(103, 56, 7, 6000.00, 1, 6000, 58, 'Student Course Registration Installment Trigger Fee', 1, 1, '2020-12-25 12:32:34', NULL, '2020-12-25 12:32:34'),
(104, 57, 12, 1000.00, 1, 1000, 241, 'Student Course Registration Per Semester Trigger Fee', 1, 1, '2020-12-25 12:35:29', NULL, '2020-12-25 12:35:29'),
(105, 57, 7, 6000.00, 1, 6000, 58, 'Student Course Registration Installment Trigger Fee', 1, 1, '2020-12-25 12:35:29', NULL, '2020-12-25 12:35:29'),
(106, 58, 12, 1000.00, 1, 1000, 241, 'Student Course Registration Per Semester Trigger Fee', 1, 1, '2020-12-25 12:39:33', NULL, '2020-12-25 12:39:33'),
(107, 58, 7, 6000.00, 1, 6000, 58, 'Student Course Registration Installment Trigger Fee', 1, 1, '2020-12-25 12:39:33', NULL, '2020-12-25 12:39:33'),
(108, 59, 7, 20000.00, 1, 20000, 242, 'Student Semester Promotion (Per Semester) Trigger Fee', 1, 1, '2020-12-25 12:57:50', NULL, '2020-12-25 12:57:50'),
(109, 59, 12, 5000.00, 1, 5000, 59, 'Student Semester Promotion Installment Trigger Fee', 1, 1, '2020-12-25 12:57:50', NULL, '2020-12-25 12:57:50'),
(110, 60, 8, 12.00, 1, 12, 115, 'Application Registration Fee', 1, 1, '2021-01-12 20:12:15', NULL, '2021-01-12 20:12:15'),
(111, 60, 11, 11.00, 1, 11, 115, 'Application Registration Fee', 1, 1, '2021-01-12 20:12:15', NULL, '2021-01-12 20:12:15'),
(112, 60, 12, 2.00, 1, 2, 115, 'Application Registration Fee', 1, 1, '2021-01-12 20:12:15', NULL, '2021-01-12 20:12:15'),
(113, 61, 6, 1900.00, 1, 1900, 116, 'Application Registration Fee', 1, 1, '2021-01-13 03:22:49', NULL, '2021-01-13 03:22:49'),
(114, 62, 8, 590.00, 1, 590, 259, 'OFFER ACCEPTED Fee', 1, 1, '2021-01-14 13:19:58', NULL, '2021-01-14 13:19:58'),
(115, 63, 8, 590.00, 1, 590, 259, 'OFFER ACCEPTED Fee', 1, 1, '2021-01-14 21:53:13', NULL, '2021-01-14 21:53:13'),
(116, 64, 8, 590.00, 1, 590, 263, 'OFFER ACCEPTED Fee', 1, 1, '2021-01-14 23:17:57', NULL, '2021-01-14 23:17:57'),
(117, 65, 8, 590.00, 1, 590, 259, 'OFFER ACCEPTED Fee', 1, 1, '2021-01-15 01:29:23', NULL, '2021-01-15 01:29:23'),
(118, 66, 10, 220.00, 1, 220, 268, 'OFFER ACCEPTED Fee', 1, 1, '2021-01-15 01:30:14', NULL, '2021-01-15 01:30:14'),
(119, 67, 10, 220.00, 1, 220, 268, 'OFFER ACCEPTED Fee', 1, 1, '2021-01-15 01:33:29', NULL, '2021-01-15 01:33:29'),
(120, 68, 12, 120.00, 1, 120, 266, 'OFFER ACCEPTED Fee', 1, 1, '2021-01-15 10:55:16', NULL, '2021-01-15 10:55:16'),
(121, 69, 10, 220.00, 1, 220, 268, 'OFFER ACCEPTED Fee', 1, 1, '2021-01-17 22:11:12', NULL, '2021-01-17 22:11:12'),
(122, 70, 10, 220.00, 1, 220, 268, 'OFFER ACCEPTED Fee', 1, 1, '2021-01-17 22:13:05', NULL, '2021-01-17 22:13:05'),
(123, 71, 10, 220.00, 1, 220, 268, 'OFFER ACCEPTED Fee', 1, 1, '2021-01-17 22:13:10', NULL, '2021-01-17 22:13:10'),
(124, 72, 10, 220.00, 1, 220, 268, 'OFFER ACCEPTED Fee', 1, 1, '2021-01-17 22:14:52', NULL, '2021-01-17 22:14:52'),
(125, 73, 10, 220.00, 1, 220, 268, 'OFFER ACCEPTED Fee', 1, 1, '2021-01-17 22:23:43', NULL, '2021-01-17 22:23:43'),
(126, 74, 10, 220.00, 1, 220, 268, 'OFFER ACCEPTED Fee', 1, 1, '2021-01-17 22:23:49', NULL, '2021-01-17 22:23:49'),
(127, 75, 10, 220.00, 1, 220, 268, 'OFFER ACCEPTED Fee', 1, 1, '2021-01-17 22:24:11', NULL, '2021-01-17 22:24:11'),
(128, 76, 10, 220.00, 1, 220, 268, 'OFFER ACCEPTED Fee', 1, 1, '2021-01-17 23:11:07', NULL, '2021-01-17 23:11:07');

-- --------------------------------------------------------

--
-- Table structure for table `main_invoice_discount_details`
--

CREATE TABLE `main_invoice_discount_details` (
  `id` int(20) NOT NULL,
  `id_main_invoice` int(20) DEFAULT 0,
  `name` varchar(500) DEFAULT '',
  `amount` float(20,2) DEFAULT 0.00,
  `id_student` int(20) DEFAULT 0,
  `id_reference` int(20) DEFAULT 0,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `main_invoice_discount_details`
--

INSERT INTO `main_invoice_discount_details` (`id`, `id_main_invoice`, `name`, `amount`, `id_student`, `id_reference`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(46, 54, 'Employee Discount Applied', 10.00, 113, 2, 1, NULL, '2020-12-25 12:03:16', NULL, '2020-12-25 12:03:16'),
(47, 62, 'Employee Discount Applied', 50.00, 123, 1, 1, NULL, '2021-01-14 13:19:58', NULL, '2021-01-14 13:19:58');

-- --------------------------------------------------------

--
-- Table structure for table `marital_status_setup`
--

CREATE TABLE `marital_status_setup` (
  `id` int(20) NOT NULL,
  `name` varchar(250) DEFAULT '',
  `sequence` int(20) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `marital_status_setup`
--

INSERT INTO `marital_status_setup` (`id`, `name`, `sequence`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Single', 1, 1, NULL, '2020-07-22 01:31:35', NULL, '2020-07-22 01:31:35'),
(2, 'Married', 2, 1, NULL, '2020-07-22 02:29:21', NULL, '2020-07-22 02:29:21'),
(3, 'Divorced', 3, 0, NULL, '2020-07-22 02:29:35', NULL, '2020-07-22 02:29:35'),
(4, 'Widowed', 4, 1, NULL, '2020-07-22 02:29:52', NULL, '2020-07-22 02:29:52');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(20) NOT NULL,
  `menu_name` varchar(200) DEFAULT NULL,
  `module_name` varchar(200) DEFAULT NULL,
  `parent_name` varchar(200) DEFAULT NULL,
  `order` int(20) DEFAULT NULL,
  `controller` varchar(200) DEFAULT NULL,
  `action` varchar(200) DEFAULT NULL,
  `parent_order` int(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`) VALUES
(1, 'User', 'Setup', 'General Setup', 1, 'user', 'list', 1),
(2, 'Role', 'Setup', 'General Setup', 2, 'role', 'list', 1),
(3, 'Permission', 'Setup', 'General Setup', 3, 'permission', 'list', 1),
(4, 'Salutation', 'Setup', 'General Setup', NULL, 'salutation', 'list', 1),
(5, 'Country', 'Setup', 'country', NULL, 'country', 'list', NULL),
(6, 'State', 'Setup', 'state', NULL, 'state', 'list', NULL),
(7, 'Education Level', 'Setup', 'General Setup', 4, 'educationLevel', 'list', 1),
(9, 'Award', 'Setup', 'General Setup', NULL, 'award', 'list', NULL),
(10, 'Race', 'Setup', 'Demographic', 1, 'race', 'list', 2),
(11, 'Religion', 'Setup', 'Demographic', 2, 'religion', 'list', 2),
(12, 'Nationality', 'Setup', 'Demographic', 3, 'nationality', 'list', 2),
(13, 'Test', 'Setup', 'Booking Setup', 1, 'test', 'list', 6),
(14, 'Organisation', 'Setup', 'Organisation Setup', 1, 'organisation', 'edit', 3),
(18, 'Course Major / Minor', 'Setup', NULL, NULL, NULL, 'list', NULL),
(28, 'Landscape Course Type', 'Setup', NULL, NULL, NULL, 'list', NULL),
(33, 'Documents Required', 'Setup', 'Document', 2, 'documents', 'list', 5),
(34, 'Staff Master', 'Setup', 'Staff', 1, 'staff', 'list', 4),
(35, 'Course Withdraw', 'Setup', NULL, NULL, NULL, 'list', NULL),
(36, 'Late Registration', 'Setup', NULL, NULL, NULL, 'list', NULL),
(37, 'Report Students by Intake', 'Setup', NULL, NULL, NULL, 'list', NULL),
(38, 'Report Applicant V/S Lead V/S Student', 'Setup', NULL, NULL, NULL, 'list', NULL),
(39, 'Report Course Wise data', 'Setup', NULL, NULL, NULL, 'list', NULL),
(153, 'Templates', 'Communication', 'Communication', 1, 'template', 'list', 1),
(154, 'Group', 'Communication', 'Communication', 2, 'group', 'list', 1),
(155, 'Group Recepients', 'Communication', 'Communication', 3, 'group', 'recepientList', 1),
(156, 'Messaging Templates', 'Communication', 'Messaging', 1, 'templateMessage', 'list', 2),
(157, 'Messaging Group', 'Communication', 'Messaging', 2, 'groupMessage', 'list', 2),
(158, 'Group Recepients (Messaging)', 'Communication', 'Messaging', 3, 'groupMessage', 'recepientList', 2),
(202, 'File Type', 'Setup', 'Document', 1, 'fileType', 'list', 5),
(203, 'Samples', 'Setup', 'Booking Setup', 3, 'sample', 'list', 6),
(204, 'Package', 'Setup', 'Booking Setup', 2, 'package', 'list', 6),
(205, 'Department', 'Setup', 'Booking Setup', 4, 'department', 'list', 6),
(206, 'Register Patient', 'Patient', 'Patient', 1, 'patient', 'list', 1),
(207, 'Sample Collection Booking', 'Patient', 'Patient', 2, 'patientBooking', 'list', 1),
(208, 'Sample Collection Summary', 'Patient', 'Booking Summary', 3, 'bookingSummary', 'list', 1),
(209, 'Patient Details', 'Records', 'Patient Records', 1, 'patient', 'list', 1),
(210, 'Patient Booking Summary', 'Records', 'Patient Records', 2, 'bookingSummary', 'list', 1);

-- --------------------------------------------------------

--
-- Table structure for table `nationality`
--

CREATE TABLE `nationality` (
  `id` bigint(20) NOT NULL,
  `name` varchar(2048) NOT NULL,
  `code` varchar(2048) NOT NULL,
  `country` varchar(2048) NOT NULL,
  `person` varchar(2048) NOT NULL,
  `status` int(20) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `nationality`
--

INSERT INTO `nationality` (`id`, `name`, `code`, `country`, `person`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Malaysian', '', '', '', 1, NULL, '2021-01-17 09:15:28', NULL, '2021-01-17 09:15:28'),
(8, 'Afghan', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(9, 'Albanian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(10, 'Algerian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(11, 'American', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(12, 'Andorran', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(13, 'Angolan', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(14, 'Anguillan', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(15, 'Argentine', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(16, 'Armenian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(17, 'Australian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(18, 'Austrian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(19, 'Azerbaijani', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(20, 'Bahamian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(21, 'Bahraini', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(22, 'Bangladeshi', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(23, 'Barbadian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(24, 'Belarusian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(25, 'Belgian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(26, 'Belizean', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(27, 'Beninese', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(28, 'Bermudian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(29, 'Bhutanese', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(30, 'Bolivian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(31, 'Botswanan', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(32, 'Brazilian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(33, 'British', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(34, 'British Virgin Islander', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(35, 'Bruneian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(36, 'Bulgarian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(37, 'Burkinan', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(38, 'Burmese', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(39, 'Burundian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(40, 'Cambodian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(41, 'Cameroonian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(42, 'Canadian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(43, 'Cape Verdean', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(44, 'Cayman Islander', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(45, 'Central African', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(46, 'Chadian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(47, 'Chilean', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(48, 'Chinese', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(49, 'Citizen of Antigua and Barbuda', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(50, 'Citizen of Bosnia and Herzegovina', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(51, 'Citizen of Guinea-Bissau', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(52, 'Citizen of Kiribati', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(53, 'Citizen of Seychelles', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(54, 'Citizen of the Dominican Republic', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(55, 'Citizen of Vanuatu', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(56, 'Colombian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(57, 'Comoran', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(58, 'Congolese (Congo)', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(59, 'Congolese (DRC)', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(60, 'Cook Islander', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(61, 'Costa Rican', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(62, 'Croatian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(63, 'Cuban', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(64, 'Cymraes', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(65, 'Cymro', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(66, 'Cypriot', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(67, 'Czech', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(68, 'Danish', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(69, 'Djiboutian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(70, 'Dominican', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(71, 'Dutch', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(72, 'East Timorese', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(73, 'Ecuadorean', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(74, 'Egyptian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(75, 'Emirati', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(76, 'English', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(77, 'Equatorial Guinean', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(78, 'Eritrean', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(79, 'Estonian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(80, 'Ethiopian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(81, 'Faroese', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(82, 'Fijian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(83, 'Filipino', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(84, 'Finnish', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(85, 'French', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(86, 'Gabonese', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(87, 'Gambian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(88, 'Georgian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(89, 'German', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(90, 'Ghanaian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(91, 'Gibraltarian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(92, 'Greek', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(93, 'Greenlandic', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(94, 'Grenadian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(95, 'Guamanian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(96, 'Guatemalan', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(97, 'Guinean', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(98, 'Guyanese', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(99, 'Haitian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(100, 'Honduran', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(101, 'Hong Konger', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(102, 'Hungarian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(103, 'Icelandic', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(104, 'Indian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(105, 'Indonesian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(106, 'Iranian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(107, 'Iraqi', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(108, 'Irish', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(109, 'Israeli', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(110, 'Italian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(111, 'Ivorian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(112, 'Jamaican', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(113, 'Japanese', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(114, 'Jordanian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(115, 'Kazakh', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(116, 'Kenyan', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(117, 'Kittitian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(118, 'Kosovan', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(119, 'Kuwaiti', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(120, 'Kyrgyz', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(121, 'Lao', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(122, 'Latvian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(123, 'Lebanese', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(124, 'Liberian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(125, 'Libyan', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(126, 'Liechtenstein citizen', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(127, 'Lithuanian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(128, 'Luxembourger', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(129, 'Macanese', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(130, 'Macedonian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(131, 'Malagasy', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(132, 'Malawian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(134, 'Maldivian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(135, 'Malian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(136, 'Maltese', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(137, 'Marshallese', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(138, 'Martiniquais', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(139, 'Mauritanian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(140, 'Mauritian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(141, 'Mexican', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(142, 'Micronesian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(143, 'Moldovan', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(144, 'Monegasque', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(145, 'Mongolian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(146, 'Montenegrin', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(147, 'Montserratian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(148, 'Moroccan', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(149, 'Mosotho', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(150, 'Mozambican', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(151, 'Namibian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(152, 'Nauruan', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(153, 'Nepalese', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(154, 'New Zealander', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(155, 'Nicaraguan', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(156, 'Nigerian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(157, 'Nigerien', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(158, 'Niuean', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(159, 'North Korean', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(160, 'Northern Irish', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(161, 'Norwegian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(162, 'Omani', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(163, 'Pakistani', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(164, 'Palauan', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(165, 'Palestinian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(166, 'Panamanian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(167, 'Papua New Guinean', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(168, 'Paraguayan', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(169, 'Peruvian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(170, 'Pitcairn Islander', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(171, 'Polish', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(172, 'Portuguese', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(173, 'Prydeinig', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(174, 'Puerto Rican', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(175, 'Qatari', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(176, 'Romanian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(177, 'Russian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(178, 'Rwandan', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(179, 'Salvadorean', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(180, 'Sammarinese', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(181, 'Samoan', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(182, 'Sao Tomean', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(183, 'Saudi Arabian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(184, 'Scottish', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(185, 'Senegalese', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(186, 'Serbian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(187, 'Sierra Leonean', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(188, 'Singaporean', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(189, 'Slovak', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(190, 'Slovenian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(191, 'Solomon Islander', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(192, 'Somali', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(193, 'South African', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(194, 'South Korean', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(195, 'South Sudanese', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(196, 'Spanish', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(197, 'Sri Lankan', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(198, 'St Helenian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(199, 'St Lucian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(200, 'Stateless', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(201, 'Sudanese', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(202, 'Surinamese', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(203, 'Swazi', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(204, 'Swedish', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(205, 'Swiss', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(206, 'Syrian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(207, 'Taiwanese', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(208, 'Tajik', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(209, 'Tanzanian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(210, 'Thai', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(211, 'Togolese', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(212, 'Tongan', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(213, 'Trinidadian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(214, 'Tristanian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(215, 'Tunisian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(216, 'Turkish', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(217, 'Turkmen', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(218, 'Turks and Caicos Islander', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(219, 'Tuvaluan', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(220, 'Ugandan', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(221, 'Ukrainian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(222, 'Uruguayan', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(223, 'Uzbek', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(224, 'Vatican citizen', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(225, 'Venezuelan', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(226, 'Vietnamese', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(227, 'Vincentian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(228, 'Wallisian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(229, 'Welsh', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(230, 'Yemeni', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(231, 'Zambian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(232, 'Zimbabwean', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35');

-- --------------------------------------------------------

--
-- Table structure for table `online_claim`
--

CREATE TABLE `online_claim` (
  `id` bigint(20) NOT NULL,
  `type` varchar(256) NOT NULL,
  `id_staff` bigint(20) NOT NULL,
  `id_university` bigint(20) NOT NULL,
  `id_branch` bigint(20) NOT NULL,
  `id_programme` bigint(20) NOT NULL,
  `id_semester` bigint(20) NOT NULL,
  `total_amount` float(20,2) DEFAULT 0.00,
  `reason` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `online_claim`
--

INSERT INTO `online_claim` (`id`, `type`, `id_staff`, `id_university`, `id_branch`, `id_programme`, `id_semester`, `total_amount`, `reason`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Academic Facilitator', 2, 1, 3, 3, 1, 0.00, '', 1, 1, '2020-12-12 03:41:48', NULL, '2020-12-12 03:41:48'),
(2, 'Academic Facilitator', 1, 4, 5, 4, 2, 120.00, '', 1, 1, '2020-12-12 03:51:51', NULL, '2020-12-12 03:51:51');

-- --------------------------------------------------------

--
-- Table structure for table `online_claim_details`
--

CREATE TABLE `online_claim_details` (
  `id` bigint(20) NOT NULL,
  `id_online_claim` int(20) NOT NULL,
  `id_course` bigint(20) NOT NULL,
  `id_payment_rate` bigint(20) NOT NULL,
  `hours` bigint(20) NOT NULL,
  `date_time` varchar(2048) DEFAULT '',
  `amount` float(20,2) NOT NULL,
  `status` int(20) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `online_claim_details`
--

INSERT INTO `online_claim_details` (`id`, `id_online_claim`, `id_course`, `id_payment_rate`, `hours`, `date_time`, `amount`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 2, 1, 1, 3, '2020-12-10', 120.00, 1, NULL, '2020-12-12 03:51:49', NULL, '2020-12-12 03:51:49');

-- --------------------------------------------------------

--
-- Table structure for table `organisation`
--

CREATE TABLE `organisation` (
  `id` int(20) NOT NULL,
  `name` varchar(500) DEFAULT '',
  `short_name` varchar(500) DEFAULT '',
  `name_in_malay` varchar(500) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT '',
  `url` varchar(500) DEFAULT '',
  `id_country` int(20) DEFAULT 0,
  `id_registrar` int(20) DEFAULT 0,
  `date_time` datetime DEFAULT current_timestamp(),
  `contact_number` int(20) DEFAULT 0,
  `email` varchar(200) DEFAULT '',
  `address1` varchar(1024) DEFAULT '',
  `address2` varchar(1024) DEFAULT '',
  `id_state` int(20) DEFAULT 0,
  `city` varchar(512) DEFAULT '',
  `zipcode` int(20) DEFAULT 0,
  `image` varchar(1024) DEFAULT '',
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `organisation`
--

INSERT INTO `organisation` (`id`, `name`, `short_name`, `name_in_malay`, `url`, `id_country`, `id_registrar`, `date_time`, `contact_number`, `email`, `address1`, `address2`, `id_state`, `city`, `zipcode`, `image`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Sangameshwara Laboratory (R)', 'SG Labs', 'ಸಂಗಮೇಶ್ವರ ಲ್ಯಾಬೋರೇಟರಿ', 'https://eag.edu.my/', 1, 1, '2020-07-10 00:00:00', 2147483647, 'conta@eag.com', 'KL', '', 1, 'KL', 345465, '867de2430dc8589b38787f7c45936020.png', 1, 1, '2020-07-10 23:25:36', NULL, '2020-07-10 23:25:36');

-- --------------------------------------------------------

--
-- Table structure for table `organisation_comitee`
--

CREATE TABLE `organisation_comitee` (
  `id` int(20) NOT NULL,
  `id_organisation` int(20) DEFAULT 0,
  `role` varchar(50) DEFAULT '',
  `name` varchar(1024) DEFAULT '',
  `nric` varchar(100) DEFAULT '',
  `effective_date` datetime DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `organisation_comitee`
--

INSERT INTO `organisation_comitee` (`id`, `id_organisation`, `role`, `name`, `nric`, `effective_date`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 'Vice Chancellor', 'Emeritus Prof Datuk Dr Hassan Said', '740501026919', '2020-08-01 00:00:00', NULL, NULL, '2020-08-08 23:35:59', NULL, '2020-08-08 23:35:59');

-- --------------------------------------------------------

--
-- Table structure for table `package`
--

CREATE TABLE `package` (
  `id` bigint(20) NOT NULL,
  `name` varchar(2048) DEFAULT '',
  `code` varchar(512) DEFAULT '',
  `amount` float(20,2) DEFAULT 0.00,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `package`
--

INSERT INTO `package` (`id`, `name`, `code`, `amount`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Oackage One', 'P!', 1200.00, 1, NULL, '2021-01-23 00:38:37', NULL, '2021-01-23 00:38:37'),
(2, 'Arogyam B12', 'AB12', 1000.00, 1, NULL, '2021-01-23 22:30:27', NULL, '2021-01-23 22:30:27'),
(3, 'Arogyam D12', 'AD-12', 430.00, 1, NULL, '2021-02-03 11:33:15', NULL, '2021-02-03 11:33:15');

-- --------------------------------------------------------

--
-- Table structure for table `package_has_test`
--

CREATE TABLE `package_has_test` (
  `id` bigint(20) NOT NULL,
  `id_package` int(20) DEFAULT 0,
  `id_test` int(20) DEFAULT 0,
  `amount` float(20,2) DEFAULT 0.00,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `package_has_test`
--

INSERT INTO `package_has_test` (`id`, `id_package`, `id_test`, `amount`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(3, 1, 0, 0.00, 1, NULL, '2021-01-23 15:23:54', NULL, '2021-01-23 15:23:54'),
(4, 1, 0, 0.00, 1, NULL, '2021-01-23 15:24:03', NULL, '2021-01-23 15:24:03'),
(5, 1, 0, 0.00, 1, NULL, '2021-01-23 15:25:20', NULL, '2021-01-23 15:25:20'),
(9, 2, 0, 0.00, 1, NULL, '2021-01-26 11:54:10', NULL, '2021-01-26 11:54:10'),
(11, 2, 2, 410.00, 1, NULL, '2021-01-26 12:18:40', NULL, '2021-01-26 12:18:40'),
(12, 3, 2, 300.00, 1, NULL, '2021-02-03 11:33:45', NULL, '2021-02-03 11:33:45'),
(14, 3, 1, 100.00, 1, NULL, '2021-02-03 11:34:19', NULL, '2021-02-03 11:34:19'),
(15, 2, 4, 400.00, 1, NULL, '2021-02-08 13:23:39', NULL, '2021-02-08 13:23:39'),
(16, 2, 3, 500.00, 1, NULL, '2021-02-08 13:24:11', NULL, '2021-02-08 13:24:11'),
(18, 3, 1, 80.00, 1, NULL, '2021-02-08 13:28:32', NULL, '2021-02-08 13:28:32'),
(19, 2, 5, 80.00, 1, NULL, '2021-02-11 11:26:33', NULL, '2021-02-11 11:26:33');

-- --------------------------------------------------------

--
-- Table structure for table `patient`
--

CREATE TABLE `patient` (
  `id` int(20) NOT NULL,
  `full_name` varchar(580) DEFAULT '',
  `salutation` varchar(120) DEFAULT '',
  `first_name` varchar(120) DEFAULT '',
  `last_name` varchar(120) DEFAULT '',
  `gender` varchar(120) DEFAULT '',
  `register_no` varchar(120) DEFAULT '',
  `passport` varchar(120) DEFAULT '',
  `phone` varchar(120) DEFAULT '',
  `alternative_phone` varchar(256) DEFAULT '',
  `email` varchar(120) DEFAULT '',
  `contact_email` varchar(500) DEFAULT '',
  `password` varchar(120) DEFAULT '',
  `passport_expiry_date` varchar(120) DEFAULT '',
  `date_of_birth` varchar(120) DEFAULT '',
  `age` int(20) DEFAULT 0,
  `age_duration` varchar(256) DEFAULT '',
  `martial_status` varchar(120) DEFAULT '',
  `religion` varchar(120) DEFAULT '',
  `nationality` varchar(120) DEFAULT '',
  `id_race` varchar(120) DEFAULT '',
  `mail_address1` varchar(120) DEFAULT '',
  `mail_address2` varchar(120) DEFAULT '',
  `mailing_country` int(120) DEFAULT NULL,
  `mailing_state` int(120) DEFAULT NULL,
  `mailing_city` varchar(120) DEFAULT '',
  `mailing_zipcode` varchar(120) DEFAULT '',
  `present_address_same_as_mailing_address` int(20) DEFAULT 0,
  `permanent_address1` varchar(120) DEFAULT '',
  `permanent_address2` varchar(120) DEFAULT '',
  `permanent_country` int(120) DEFAULT NULL,
  `permanent_state` int(120) DEFAULT NULL,
  `permanent_city` varchar(120) DEFAULT '',
  `permanent_zipcode` varchar(120) DEFAULT '',
  `reason` varchar(2048) DEFAULT '',
  `status` int(20) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `patient`
--

INSERT INTO `patient` (`id`, `full_name`, `salutation`, `first_name`, `last_name`, `gender`, `register_no`, `passport`, `phone`, `alternative_phone`, `email`, `contact_email`, `password`, `passport_expiry_date`, `date_of_birth`, `age`, `age_duration`, `martial_status`, `religion`, `nationality`, `id_race`, `mail_address1`, `mail_address2`, `mailing_country`, `mailing_state`, `mailing_city`, `mailing_zipcode`, `present_address_same_as_mailing_address`, `permanent_address1`, `permanent_address2`, `permanent_country`, `permanent_state`, `permanent_city`, `permanent_zipcode`, `reason`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Dr. Vinay P', '4', 'Vinay', 'P', 'Male', 'SG0000012021', '', '88888888', '', 'vinaykiranp888@gmail.com', '', '8ddcff3a80f4189ca1c9d4d902c3c909', '', '2021-01-29', 0, '', '', '', '', '', 'KL', 'KL', 1, 0, 'KL', '', 1, '', '', 0, 0, '', '', '', 1, 1, '2021-01-25 19:26:10', NULL, '2021-01-25 19:26:10'),
(2, 'Ir. Vinay P', '6', 'Vinay', 'P', 'Male', 'SG0000022021', '', '6789', '', 'vinaykiranp888@gmail.com', '', '46d045ff5190f6ea93739da6c0aa19bc', '', '2021-01-07', 0, '', '', '', '', '', 'KL', 'KL', 1, 9, 'KL', '123', 1, '', '', 0, 0, '', '', '', 1, NULL, '2021-01-25 19:31:29', NULL, '2021-01-25 19:31:29'),
(3, 'Dr. Pradeepa K', '4', 'Pradeepa', 'K', 'Male', 'SG0000032021', '', '19739350331', '', 'pk@gmail.com', '', 'ad9fdf2ecd388f0d038d5fa3cb85803a', '', '2021-02-18', 0, '', '', '', '', '', 'VINAY KIRAN P', 'KL', 1, 1, 'KL', '', 1, '', '', 0, 0, '', '', '', 1, 1, '2021-02-03 11:36:13', NULL, '2021-02-03 11:36:13'),
(4, 'Mr. Sampath Kumar', '1', 'Sampath', 'Kumar', 'Male', 'SG0000042021', '', '123', '', 'sk@gmail.com', '', '202cb962ac59075b964b07152d234b70', '', '2021-02-17', 0, '', '', '', '', '', 'KL', 'KL', 1, 14, 'KL', '', 1, '', '', 0, 0, '', '', '', 1, 1, '2021-02-08 13:30:16', NULL, '2021-02-08 13:30:16'),
(5, 'Mr. Vinay W', '1', 'Vinay', 'W', 'Male', 'SG0000052021', '', '123123123', '', 'ss@gmail.com', '', 'f5bb0c8de146c67b44babbf4e6584cc0', '', '2021-02-18', 0, '', '', '', '', '', 'KL', 'KL', 1, 1, 'KL', '1234', 1, '', '', 0, 0, '', '', '', 1, 1, '2021-02-11 11:29:22', NULL, '2021-02-11 11:29:22'),
(6, 'Mr. Parameshwarappa DK', '1', 'Parameshwarappa', 'DK', 'Male', 'SG0000062021', '', '9353457845', '', 'parameshwarappa@gmail.com', '', '808971160691a239102a4c7bfa74e40e', '', '1966-12-15', 0, '', '', '', '', '', 'Rajajinagara', 'Landmark', 0, 0, 'Bengaluru', '560088', 0, '', '', 0, 0, '', '', '', 1, 0, '2021-05-10 12:26:58', NULL, '2021-05-10 12:26:58'),
(7, 'Mr. VeeKee P', '1', 'VeeKee', 'P', 'Male', 'SG0000072021', '', '843242342', '', 'vk@gmail.com', '', 'e3e740e894b3bcb5247e52d5e91c4f2a', '', '', 12, 'Years', '', '', '', '', 'KL', 'KL', 0, 0, 'Bengaluru', '560001', 0, '', '', 0, 0, '', '', '', 1, 0, '2021-05-11 05:58:55', NULL, '2021-05-11 05:58:55'),
(8, 'Mr. VIJAYAKUMAR DP', '1', 'VIJAYAKUMAR', 'DP', 'Male', 'SG0000082021', '', '9036239008', '', 'vijayadp17@gmail.com', '', 'f97be7f3ffff20a04a75eae6018718ce', '', '', 36, 'Years', '', '', '', '', '20 6th main', 'Utharalli', 0, 0, 'Bengaluru', '560085', 0, '', '', 0, 0, '', '', '', 1, 0, '2021-05-11 06:40:51', NULL, '2021-05-11 06:40:51');

-- --------------------------------------------------------

--
-- Table structure for table `payment_rate`
--

CREATE TABLE `payment_rate` (
  `id` int(20) NOT NULL,
  `id_award` int(20) DEFAULT 0,
  `learning_mode` varchar(200) DEFAULT '',
  `teaching_component` varchar(200) DEFAULT '',
  `id_currency` int(20) DEFAULT 0,
  `calculation_type` varchar(200) DEFAULT '',
  `amount` float(20,2) DEFAULT 0.00,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment_rate`
--

INSERT INTO `payment_rate` (`id`, `id_award`, `learning_mode`, `teaching_component`, `id_currency`, `calculation_type`, `amount`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 6, 'Full Time', 'Face to face lecture', 1, 'Per hour', 0.00, 1, 1, '2020-12-09 04:41:24', 1, '2020-12-09 04:41:24');

-- --------------------------------------------------------

--
-- Table structure for table `payment_type`
--

CREATE TABLE `payment_type` (
  `id` int(20) NOT NULL,
  `name` varchar(520) DEFAULT '',
  `description` varchar(2048) DEFAULT '',
  `description_optional_language` varchar(2048) DEFAULT '',
  `payment_group` varchar(512) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `name_in_malay` varchar(520) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment_type`
--

INSERT INTO `payment_type` (`id`, `name`, `description`, `description_optional_language`, `payment_group`, `code`, `name_in_malay`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, '', 'Cash Payment', '', 'Cash', 'Cash', '', 1, NULL, '2020-08-20 10:08:10', NULL, '2020-08-20 10:08:10'),
(2, '', 'Bank Checue Transfer', '', 'Checque', 'Bank Transfer', '', 1, NULL, '2020-08-20 10:09:30', NULL, '2020-08-20 10:09:30');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) NOT NULL,
  `id_menu` int(20) DEFAULT 0,
  `module` varchar(512) DEFAULT '',
  `code` varchar(100) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `id_menu`, `module`, `code`, `description`, `status`) VALUES
(7, 3, 'Setup', 'permission.add', 'Add Permission', 1),
(8, 3, 'Setup', 'permission.list', 'View Permission', 1),
(9, 3, 'Setup', 'permission.edit', 'Edit Permission', 1),
(10, 1, 'Setup', 'user.edit', 'Edit User', 1),
(11, 1, 'Setup', 'user.add', 'Add User', 1),
(12, 1, 'Setup', 'user.list', 'View User', 1),
(13, 2, 'Setup', 'role.list', 'View Role', 1),
(14, 2, 'Setup', 'role.add', 'Add Role', 1),
(15, 2, 'Setup', 'role.edit', 'Edit Role', 1),
(70, 4, 'Setup', 'salutation.list', 'Salutation List', 1),
(71, 4, 'Setup', 'salutation.add', 'Salutation Add', 1),
(72, 4, 'Setup', 'salutation.edit', 'Salutation Edit', 1),
(73, 5, 'Setup', 'country.list', 'Country List', 1),
(74, 5, 'Setup', 'country.add', 'Country Add', 1),
(75, 5, 'Setup', 'country.edit', 'Country Edit', 1),
(76, 6, 'Setup', 'state.list', 'State List', 1),
(77, 7, 'Setup', 'education_level.list', 'Education Level List', 1),
(78, 7, 'Setup', 'education_level.add', 'Education Level Add', 1),
(79, 7, 'Setup', 'education_level.edit', 'Education Level Edit', 1),
(80, 8, 'Setup', 'academic_year.list', 'Academic Year List', 1),
(81, 8, 'Setup', 'academic_year.add', 'Academic Year Add', 1),
(82, 8, 'Setup', 'academic_year.edit', 'Academic Year Edit', 1),
(83, 9, 'Setup', 'award.list', 'Award List', 1),
(84, 9, 'Setup', 'award.add', 'Award Add', 1),
(85, 9, 'Setup', 'award.edit', 'Award Edit', 1),
(86, 10, 'Setup', 'race.list', 'Race List', 1),
(87, 10, 'Setup', 'race.add', 'Race Add', 1),
(88, 10, 'Setup', 'race.edit', 'Race Edit', 1),
(89, 11, 'Setup', 'religion.list', 'Religion List', 1),
(90, 11, 'Setup', 'religion.add', 'Religion Add', 1),
(91, 11, 'Setup', 'religion.edit', 'Religion Edit', 1),
(92, 12, 'Setup', 'nationality.list', 'Nationality List', 1),
(93, 12, 'Setup', 'nationality.add', 'Nationality Add', 1),
(94, 12, 'Setup', 'nationality.edit', 'Nationality Edit', 1),
(95, 13, 'Setup', 'department.list', 'Department List', 1),
(96, 13, 'Setup', 'department.add', 'Department Add', 1),
(97, 13, 'Setup', 'department.edit', 'Department Edit', 1),
(98, 14, 'Setup', 'organisation.edit', 'Organisation Edit', 1),
(99, 15, 'Setup', 'partner_category.list', 'Partner Category List', 1),
(100, 15, 'Setup', 'partner_category.add', 'Partner Category Add', 1),
(101, 15, 'Setup', 'partner_category.edit', 'Partner Category Edit', 1),
(102, 16, 'Setup', 'partner_university.list', 'Partner University List', 1),
(103, 16, 'Setup', 'partner_university.add', 'Partner University Add', 1),
(104, 16, 'Setup', 'partner_university.edit', 'Partner University Edit', 1),
(105, 17, 'Setup', 'faculty_program.list', 'Faculty Program List', 1),
(106, 17, 'Setup', 'faculty_program.add', 'Faculty Program Add', 1),
(107, 17, 'Setup', 'faculty_program.edit', 'Faculty Program Edit', 1),
(108, 18, 'Setup', 'course_description.list', 'course Major / Minor List', 1),
(109, 18, 'Setup', 'course_description.add', 'course Major / Minor Add', 1),
(110, 18, 'Setup', 'course_description.edit', 'course Major / Minor Edit', 1),
(111, 19, 'Setup', 'course_type.list', 'Course Type list', 1),
(112, 19, 'Setup', 'course_type.add', 'Course Type Add', 1),
(113, 19, 'Setup', 'course_type.edit', 'Course Type Edit', 1),
(114, 20, 'Setup', 'course_offered,list', 'Course Offered List', 1),
(115, 20, 'Setup', 'course_offered,add', 'Course Offered Add', 1),
(116, 20, 'Setup', 'course_offered,edit', 'Course Offered Edit', 1),
(117, 21, 'Setup', 'course.list', 'Course List', 1),
(118, 21, 'Setup', 'course.add', 'Course Add', 1),
(119, 21, 'Setup', 'course.edit', 'Course Edit', 1),
(120, 22, 'Setup', 'course_master.list', 'Course Requisite List', 1),
(121, 22, 'Setup', 'course_master.add', 'Course Requisite Add', 1),
(122, 22, 'Setup', 'course_master.edit', 'Course Requisite Edit', 1),
(123, 23, 'Setup', 'equivalent_course.list', 'Equivalent Course List', 1),
(124, 23, 'Setup', 'equivalent_course.add', 'Equivalent Course Add', 1),
(125, 23, 'Setup', 'equivalent_course.edit', 'Equivalent Course Edit', 1),
(126, 24, 'Setup', 'scheme.list', 'Scheme List', 1),
(127, 24, 'Setup', 'scheme.add', 'Scheme Add', 1),
(128, 24, 'Setup', 'scheme.edit', 'Scheme Edit', 1),
(129, 25, 'Setup', 'activity.list', 'Activity List', 1),
(130, 25, 'Setup', 'activity.add', 'Activity Add', 1),
(131, 25, 'Setup', 'activity.edit', 'Activity Edit', 1),
(132, 26, 'Setup', 'semester.list', 'Semester List', 1),
(133, 26, 'Setup', 'semester.add', 'Semester Add', 1),
(134, 26, 'Setup', 'semester.edit', 'Semester Edit', 1),
(135, 27, 'Setup', 'intake.list', 'Intake List', 1),
(136, 27, 'Setup', 'intake.add', 'Intake Add', 1),
(137, 27, 'Setup', 'intake.edit', 'Intake Edit', 1),
(138, 28, 'Setup', 'landscape_course_type.list', 'Landscape CourseType List', 1),
(139, 28, 'Setup', 'landscape_course_type.add', 'Landscape CourseType Add', 1),
(140, 28, 'Setup', 'landscape_course_type.edit', 'Landscape CourseType Edit', 1),
(141, 29, 'Setup', 'program_type.list', 'Program Type list', 1),
(142, 29, 'Setup', 'program_type.add', 'Program Type Add', 1),
(143, 29, 'Setup', 'program_type.edit', 'Program Type Edit', 1),
(144, 30, 'Setup', 'programme.list', 'Programme List', 1),
(145, 30, 'Setup', 'programme.add', 'Programme Add', 1),
(146, 30, 'Setup', 'programme.edit', 'Programme Edit', 1),
(147, 31, 'Setup', 'programme_landscape.list', 'Programme Landscape List', 1),
(148, 31, 'Setup', 'programme_landscape.add', 'Programme Landscape Add', 1),
(149, 31, 'Setup', 'programme_landscape.edit', 'Programme Landscape Edit', 1),
(150, 32, 'Setup', 'programme_entry_requirement.list', 'Programme Entry Requirement List', 1),
(151, 32, 'Setup', 'programme_entry_requirement.add', 'Programme Entry Requirement Add', 1),
(152, 32, 'Setup', 'programme_entry_requirement.edit', 'Programme Entry Requirement Edit', 1),
(153, 33, 'Setup', 'documents_program.list', 'Documents Reuired Program List', 1),
(154, 33, 'Setup', 'documents_program.add', 'Documents Reuired Program Add', 1),
(155, 33, 'Setup', 'documents_program.edit', 'Documents Reuired Program Edit', 1),
(156, 34, 'Setup', 'staff.list', 'Staff List', 1),
(157, 34, 'Setup', 'staff.add', 'Staff Add', 1),
(158, 34, 'Setup', 'staff.edit', 'Staff Edit', 1),
(159, 35, 'Setup', 'course_withdraw.list', 'Course Withdraw List', 1),
(160, 35, 'Setup', 'course_withdraw.add', 'Course Withdraw Add', 1),
(161, 35, 'Setup', 'course_withdraw.edit', 'Course Withdraw Edit', 1),
(162, 36, 'Setup', 'late_registration.list', 'Late Registration List', 1),
(163, 36, 'Setup', 'late_registration.add', 'Late Registration Add', 1),
(164, 36, 'Setup', 'late_registration.edit', 'Late Registration Edit', 1),
(165, 37, 'Setup', 'report.student_by_intake', 'Report Student By Intake', 1),
(166, 38, 'Setup', 'report.applicant_lead_student', 'Report Applicant Lead Student', 1),
(167, 39, 'Setup', 'report.course_wise_data', 'Report Course Wise Data', 1),
(168, 40, 'Admission', 'group_setup.list', 'Group Setup List', 1),
(169, 40, 'Admission', 'group_setup.add', 'Group Setup Add', 1),
(170, 40, 'Admission', 'group_setup.edit', 'Group Setup Edit', 1),
(171, 41, 'Admission', 'specialization_setup.list', 'Specialization List', 1),
(172, 41, 'Admission', 'specialization_setup.add', 'Specialization Add', 1),
(173, 41, 'Admission', 'specialization_setup.edit', 'Specialization Edit', 1),
(174, 42, 'Admission', 'applicant.list', 'Applicant List', 1),
(175, 42, 'Admission', 'applicant.view', 'Applicant View', 1),
(176, 43, 'Admission', 'apel_approval.list', 'Apel Approval List', 1),
(177, 43, 'Admission', 'apel_approval.approve', 'Apel Approve', 1),
(178, 44, 'Admission', 'sibbling_discount_approval.list', 'Sibbling Discount Approval List', 1),
(179, 44, 'Admission', 'sibbling_discount_approval.approve', 'Sibbling Discount Approve', 1),
(180, 45, 'Admission', 'employee_discount_approval.list', 'Employee Discount Approval List', 1),
(181, 45, 'Admission', 'employee_discount_approval.approve', 'Employee Discount Approve', 1),
(182, 46, 'Admission', 'alumni_discount_approval.list', 'Alumni Discount Approval List', 1),
(183, 46, 'Admission', 'alumni_discount_approval.approve', 'Alumni Discount Approve', 1),
(184, 47, 'Admission', 'applicant_approval.list', 'Applicant Approval List', 1),
(185, 47, 'Admission', 'applicant_approval.approve', 'Applicant Approve', 1),
(186, 48, 'Registration', 'student_approval.list', 'Student Approval List', 1),
(187, 48, 'Registration', 'student_approval.approve', 'Student Approval Approve', 1),
(188, 49, 'Registration', 'advisor_tagging.add', 'Academic Advisor Tagging Add/ Edit', 1),
(189, 50, 'Registration', 'course_registration.list', 'Course Registration List', 1),
(190, 50, 'Registration', 'course_registration.add', 'Course Registration Add', 1),
(191, 50, 'Registration', 'course_registration.view', 'Course Registration View', 1),
(192, 51, 'Registration', 'bulk_withdraw.list', 'Bulk Withdraw List', 1),
(193, 51, 'Registration', 'bulk_withdraw.add', 'Bulk Withdraw Add', 1),
(194, 51, 'Registration', 'bulk_withdraw.view', 'Bulk Withdraw View', 1),
(195, 52, 'Registration', 'credit_transfer.list', 'Credit Transfer / Excemption Application List', 1),
(196, 52, 'Registration', 'credit_transfer.add', 'Credit Transfer / Excemption Application Add', 1),
(197, 52, 'Registration', 'credit_transfer.view', 'Credit Transfer / Excemption Application View', 1),
(198, 52, 'Registration', 'credit_transfer.approval_list', 'Credit Transfer / Excemption Application Approval List', 1),
(199, 52, 'Registration', 'credit_transfer.approve', 'Credit Transfer / Excemption Application Approve', 1),
(200, 54, 'Registration', 'attendence_setup.list', 'Attendence Setup List', 1),
(201, 54, 'Registration', 'attendence_setup.add', 'Attendence Setup Add', 1),
(202, 54, 'Registration', 'attendence_setup.view', 'Attendence Setup View', 1),
(203, 55, 'Registration', 'report.course_count', 'Report Course Count', 1),
(204, 56, 'Records', 'barring_type.list', ' Barring Type List', 1),
(205, 56, 'Records', 'barring_type.add', ' Barring Type Add', 1),
(206, 56, 'Records', 'barring_type.edit', ' Barring Type Edit', 1),
(207, 57, 'Records', 'barring.list', 'Barring List', 1),
(208, 57, 'Records', 'barring.add', 'Barring Add', 1),
(209, 57, 'Records', 'barring.edit', 'Barring Edit', 1),
(210, 58, 'Records', 'release.list', 'Release List', 1),
(211, 58, 'Records', 'release.add', 'Release Add', 1),
(212, 58, 'Records', 'release.dedit', 'Release Edit', 1),
(213, 59, 'Records', 'student.list', 'Student List', 1),
(214, 59, 'Records', 'student.edit', 'Student Edit', 1),
(215, 60, 'Records', 'student_record.list', 'Student Record List', 1),
(216, 60, 'Records', 'student_record.view', 'Student Record View', 1),
(217, 61, 'Records', 'change_status.list', 'Change Status List', 1),
(218, 61, 'Records', 'change_status.add', 'Change Status Add', 1),
(219, 61, 'Records', 'change_status.edit', 'Change Status Edit', 1),
(220, 62, 'Records', 'apply_change_status.list', 'Apply Change Status List', 1),
(221, 62, 'Records', 'apply_change_status.add', 'Apply Change Status Add', 1),
(222, 62, 'Records', 'apply_change_status.view', 'Apply Change Status View', 1),
(223, 63, 'Records', 'apply_change_status.approval_list', 'Apply Change Status Approval List', 1),
(224, 63, 'Records', 'apply_change_status.approve', 'Apply Change Status Approve', 1),
(225, 64, 'Records', 'visa_details.student_list', 'Visa Details Student List', 1),
(226, 64, 'Records', 'visa_details.add', 'Visa Details Student Add', 1),
(227, 65, 'Records', 'apply_change_programme.list', 'Apply Change Programme List', 1),
(228, 65, 'Records', 'apply_change_programme.add', 'Apply Change Programme Add', 1),
(229, 65, 'Records', 'apply_change_programme.view', 'Apply Change Programme View', 1),
(230, 66, 'Records', 'apply_change_programme.approve', 'Apply Change Programme Approve', 1),
(231, 66, 'Records', 'apply_change_programme.approval_list', 'Apply Change Programme Approval List', 1),
(232, 67, 'Records', 'apply_change_scheme.list', 'Apply Change Scheme List', 1),
(233, 67, 'Records', 'apply_change_scheme.add', 'Apply Change Scheme Add', 1),
(234, 67, 'Records', 'apply_change_scheme.view', 'Apply Change Scheme View', 1),
(235, 68, 'Records', 'apply_change_scheme.approval_list', 'Apply Change Scheme Approval List', 1),
(236, 68, 'Records', 'apply_change_scheme.approve', 'Apply Change Scheme Approve', 1),
(237, 69, 'Records', 'change_branch.list', 'Change Branch List', 1),
(238, 69, 'Records', 'change_branch.add', 'Change Branch Add', 1),
(239, 69, 'Records', 'change_branch.view', 'Change Branch View', 1),
(240, 70, 'Records', 'change_branch.approval_list', 'Change Branch Approval List', 1),
(241, 70, 'Records', 'change_branch.approve', 'Change Branch Approve', 1),
(242, 71, 'Examination', 'exam_configuration.edit', 'Exam Configuration Edit', 1),
(243, 72, 'Examination', 'exam_configuration.edit', 'Exam Configuration Edit', 1),
(244, 73, 'Examination', 'gpa_cgpa.list', 'Gpa Cgpa List', 1),
(245, 73, 'Examination', 'gpa_cgpa.add', 'Gpa Cgpa Add', 1),
(246, 73, 'Examination', 'gpa_cgpa.edit', 'Gpa Cgpa Edit', 1),
(247, 74, 'Examination', 'assesment_type.list', 'Assesment Type List', 1),
(248, 74, 'Examination', 'assesment_type.add', 'Assesment Type Add', 1),
(249, 74, 'Examination', 'assesment_type.edit', 'Assesment Type Edit', 1),
(250, 75, 'Examination', 'grade_setup.list', 'Grade Setup List', 1),
(251, 75, 'Examination', 'grade_setup.add', 'Grade Setup Add', 1),
(252, 75, 'Examination', 'grade_setup.edit', 'Grade Setup Edit', 1),
(253, 76, 'Examination', 'course_grade.list', 'Course Grade List', 1),
(254, 76, 'Examination', 'course_grade.add', 'Course Grade Add', 1),
(255, 76, 'Examination', 'course_grade.edit', 'Course Grade Edit', 1),
(256, 77, 'Examination', 'publish_exam_result.list', 'Publish Exam Result List', 1),
(257, 77, 'Examination', 'publish_exam_result.add', 'Publish Exam Result Add', 1),
(258, 77, 'Examination', 'publish_exam_result.edit', 'Publish Exam Result Edit', 1),
(259, 78, 'Examination', 'location.list', 'Exam Locaction Setup', 1),
(260, 78, 'Examination', 'location.add', 'Exam Locaction Add', 1),
(261, 78, 'Examination', 'location.edit', 'Exam Locaction Edit', 1),
(262, 79, 'Examination', 'exam_center.list', 'Exam Center List', 1),
(263, 79, 'Examination', 'exam_center.add', 'Exam Center Add', 1),
(264, 79, 'Examination', 'exam_center.edit', 'Exam Center Edit', 1),
(265, 80, 'Examination', 'exam_event.list', 'Exam Event List', 1),
(266, 80, 'Examination', 'exam_event.add', 'Exam Event Add', 1),
(267, 80, 'Examination', 'exam_event.edit', 'Exam Event Edit', 1),
(268, 81, 'Examination', 'exam_registration.list', 'Exam Registration List', 1),
(269, 81, 'Examination', 'exam_registration.add', 'Exam Registration Add', 1),
(270, 82, 'Examination', 'exam_registration.tag_student', 'Exam Registration Tag Student', 1),
(271, 82, 'Examination', 'exam_registration.tag_list', 'Exam Registration Tag List', 1),
(272, 83, 'Examination', 'exam_slip.list', 'Release Exam Slip List', 1),
(273, 83, 'Examination', 'exam_slip.add', 'Release Exam Slip Add', 1),
(274, 83, 'Examination', 'exam_slip.view', 'Release Exam Slip View', 1),
(275, 84, 'Examination', 'publish_exam_result_date.list', 'Publish Exam Result Schedule List', 1),
(276, 84, 'Examination', 'publish_exam_result_date.add', 'Publish Exam Result Schedule Add', 1),
(277, 84, 'Examination', 'publish_exam_result_date.view', 'Publish Exam Result Schedule View', 1),
(278, 85, 'Examination', 'publish_assesment_result_date.list', 'Publish Assesment Result Schedule List', 1),
(279, 85, 'Examination', 'publish_assesment_result_date.add', 'Publish Assesment Result Schedule Add', 1),
(280, 85, 'Examination', 'publish_assesment_result_date.view', 'Publish Assesment Result Schedule View', 1),
(281, 86, 'Examination', 'mark_distribution.list', 'Mark Distribution List', 1),
(282, 86, 'Examination', 'mark_distribution.add', 'Mark Distribution Add', 1),
(283, 86, 'Examination', 'mark_distribution.view', 'Mark Distribution View', 1),
(284, 87, 'Examination', 'mark_distribution.approval_list', 'Mark Distribution Approval List', 1),
(285, 87, 'Examination', 'mark_distribution.approve', 'Mark Distribution Approve', 1),
(286, 88, 'Examination', 'student_marks_entry.approval_list', 'Student Marks Entry Approval List', 1),
(287, 88, 'Examination', 'student_marks_entry.approve', 'Student Marks Entry Approve', 1),
(288, 187, 'Examination', 'student_marks_entry.student_list', 'Student Marks Entry List', 1),
(289, 187, 'Examination', 'student_marks_entry.add', 'Student Marks Entry Add', 1),
(290, 89, 'Examination', 'student_marks_entry.summary_list', 'Student Marks Entry Summary List', 1),
(291, 89, 'Examination', 'student_marks_entry.summary_view', 'Student Marks Entry Summary View', 1),
(292, 90, 'Examination', 'remarking_application.entry_list', 'Remarks Entry List', 1),
(293, 90, 'Examination', 'remarking_application.add', 'Add Remarks', 1),
(294, 91, 'Examination', 'remarking_application.approval_list', 'Remark Entry Approval List', 1),
(295, 91, 'Examination', 'remarking_application.approve', 'Remark Entry Approve', 1),
(296, 92, 'Examination', 'remarking_application.summary_list', 'Student Remarks Application Summary List', 1),
(297, 92, 'Examination', 'remarking_application.view', 'Student Remarks Application Summary View', 1),
(298, 93, 'Examination', 'transcript.list', 'Transcript', 1),
(299, 93, 'Examination', 'transcript.view', 'Transcript View', 1),
(300, 94, 'Examination', 'result_slip.list', 'Result Slip', 1),
(301, 94, 'Examination', 'result_slip.view', 'Result Slip View', 1),
(302, 95, 'Graduation', 'award.list', 'Award List', 1),
(303, 95, 'Graduation', 'award.add', 'Award Add', 1),
(304, 95, 'Graduation', 'award.edit', 'Award Edit', 1),
(305, 96, 'Graduation', 'convocation.list', 'Award List', 1),
(306, 97, 'Graduation', 'guest.list', 'Guest List', 1),
(307, 97, 'Graduation', 'guest.add', 'Guest Add', 1),
(308, 97, 'Graduation', 'guest.edit', 'Guest Edit', 1),
(309, 98, 'Graduation', 'graduation.list', 'Graduation List', 1),
(310, 98, 'Graduation', 'graduation.add', 'Graduation Add', 1),
(311, 98, 'Graduation', 'graduation.view', 'Graduation View', 1),
(312, 98, 'Graduation', 'graduation.add_student', 'Graduation Add / Edit Students', 1),
(313, 99, 'Graduation', 'graduation.approval_list', 'Graduation Approval List', 1),
(314, 99, 'Graduation', 'graduation.approve', 'Graduation Approve', 1),
(315, 100, 'Finance', 'currency.list', 'Currency List', 1),
(316, 100, 'Finance', 'currency.add', 'Currency Add', 1),
(317, 100, 'Finance', 'currency.edit', 'Currency Edit', 1),
(318, 101, 'Finance', 'currency_rate_setup.list', 'Currency Rate Setup List', 1),
(319, 101, 'Finance', 'currency_rate_setup.add', 'Currency Rate Setup Add', 1),
(320, 101, 'Finance', 'currency_rate_setup.edit', 'Currency Rate Setup Edit', 1),
(321, 102, 'Finance', 'credit_note_type.list', 'Credit Note Type', 1),
(322, 102, 'Finance', 'credit_note_type.add', 'Credit Note Add', 1),
(323, 102, 'Finance', 'credit_note_type.edit', 'Credit Note Edit', 1),
(324, 103, 'Finance', 'fee_structure_activity.list', 'Fee Structure Activity List', 1),
(325, 103, 'Finance', 'fee_structure_activity.add', 'Fee Structure Activity Add', 1),
(326, 103, 'Finance', 'fee_structure_activity.edit', 'Fee Structure Activity Edit', 1),
(327, 104, 'Finance', 'account_code.list', 'Account Code List', 1),
(328, 104, 'Finance', 'account_code.add', 'Account Code Add', 1),
(329, 104, 'Finance', 'account_code.edit', 'Account Code Edit', 1),
(330, 105, 'Finance', 'bank_registration.list', 'Bank Registration List', 1),
(331, 105, 'Finance', 'bank_registration.add', 'Bank Registration Add', 1),
(332, 105, 'Finance', 'bank_registration.edit', 'Bank Registration Edit', 1),
(333, 106, 'Finance', 'amount_calculation_type.list', 'Amount Calculation Type List', 1),
(334, 106, 'Finance', 'amount_calculation_type.add', 'Amount Calculation Type Add', 1),
(335, 106, 'Finance', 'amount_calculation_type.edit', 'Amount Calculation Type Edit', 1),
(336, 107, 'Finance', 'frequency_mode.list', 'Frequency Mode List', 1),
(337, 107, 'Finance', 'frequency_mode.add', 'Frequency Mode Add', 1),
(338, 107, 'Finance', 'frequency_mode.edit', 'Frequency Mode Edit', 1),
(339, 108, 'Finance', 'payment_type.list', 'Payment Type List', 1),
(340, 108, 'Finance', 'payment_type.add', 'Payment Type Add', 1),
(341, 108, 'Finance', 'payment_type.edit', 'Payment Type Edit', 1),
(342, 109, 'Finance', 'fee_category.list', 'Fee Category List', 1),
(343, 109, 'Finance', 'fee_category.add', 'Fee Category Add', 1),
(344, 109, 'Finance', 'fee_category.edit', 'Fee Category Edit', 1),
(345, 110, 'Finance', 'fee_setup.list', 'Fee Setup List', 1),
(346, 110, 'Finance', 'fee_setup.add', 'Fee Setup Add', 1),
(347, 110, 'Finance', 'fee_setup.edit', 'Fee Setup Edit', 1),
(348, 111, 'Finance', 'fee_structure.programme_list', 'Fee Structure Program List', 1),
(349, 111, 'Finance', 'fee_structure.programme_landscape_list', 'Fee Structure Program Landscape List', 1),
(350, 111, 'Finance', 'fee_structure.add', 'Fee Structure Add', 1),
(351, 112, 'Finance', 'partne_university_fee.list', 'Partner University Fee List', 1),
(352, 112, 'Finance', 'partne_university_fee.add', 'Partner University Fee Add', 1),
(353, 112, 'Finance', 'partne_university_fee.edit', 'Partner University Fee Edit', 1),
(354, 113, 'Finance', 'sibbling_discount.list', 'Sibbling Discount List', 1),
(355, 113, 'Finance', 'sibbling_discount.add', 'Sibbling Discount Add', 1),
(356, 113, 'Finance', 'sibbling_discount.edit', 'Sibbling Discount Edit', 1),
(357, 114, 'Finance', 'employee_discount.list', 'Employee Discount List', 1),
(358, 114, 'Finance', 'employee_discount.add', 'Employee Discount Add', 1),
(359, 114, 'Finance', 'employee_discount.edit', 'Employee Discount Edit', 1),
(360, 115, 'Finance', 'alumni_discount.list', 'Alumni Discount List', 1),
(361, 115, 'Finance', 'alumni_discount.add', 'Alumni Discount Add', 1),
(362, 115, 'Finance', 'alumni_discount.edit', 'Alumni Discount Edit', 1),
(363, 116, 'Finance', 'statement_of_account.list', 'Statement Of Accounts List', 1),
(364, 116, 'Finance', 'statement_of_account.add', 'Statement Of Accounts Add', 1),
(365, 119, 'Finance', 'main_invoice.list', 'Invoice Generation List', 1),
(366, 117, 'Finance', 'main_invoice.add', 'Add Invoice', 1),
(367, 117, 'Finance', 'main_invoice.view', 'View Invoice', 1),
(368, 118, 'Finance', 'main_invoice.cancel_invoice', 'Cancel Wrong Invoice List', 1),
(369, 118, 'Finance', 'main_invoice.cancel', 'Cancel Wrong Invoice Generated', 1),
(370, 119, 'Finance', 'partner_university_invoice.list', 'Partner University Invoice List', 1),
(371, 119, 'Finance', 'partner_university_invoice.add', 'Partner University Invoice Add', 1),
(372, 119, 'Finance', 'partner_university_invoice.view', 'Partner University Invoice View', 1),
(373, 120, 'Finance', 'receipt.list', 'Receipt List', 1),
(374, 120, 'Finance', 'receipt.add', 'Receipt Add', 1),
(375, 120, 'Finance', 'receipt.view', 'Receipt View', 1),
(376, 121, 'Finance', 'receipt.approval_list', 'Receipt Approval List', 1),
(377, 121, 'Finance', 'receipt.approve', 'Receipt Approve', 1),
(378, 122, 'Finance', 'sponser_main_invoice.list', 'Sponsor Invoice List', 1),
(379, 122, 'Finance', 'sponser_main_invoice.add', 'Sponsor Invoice Add', 1),
(380, 122, 'Finance', 'sponser_main_invoice.view', 'Sponsor Invoice View', 1),
(381, 123, 'Finance', 'sponser_main_invoice.cancel_list', 'Sponsor Invoice Cancellation List', 1),
(382, 123, 'Finance', 'sponser_main_invoice.cancel', 'Sponser Invoice Cancellation  ', 1),
(383, 124, 'Finance', 'advance_payment.list', 'Advance Payment List', 1),
(384, 124, 'Finance', 'advance_payment.add', 'Advance Payment Add', 1),
(385, 124, 'Finance', 'advance_payment.view', 'Advance Payment View', 1),
(386, 124, 'Finance', 'advance_payment.approval_list', 'Advance Payment Approval List', 1),
(387, 124, 'Finance', 'advance_payment.approva', 'Advance Payment Approve', 1),
(388, 125, 'Finance', 'credit_note.list', 'Credit Note Entry List', 1),
(389, 125, 'Finance', 'credit_note.add', 'Credit Note Add', 1),
(390, 125, 'Finance', 'credit_note.view', 'Credit Note View', 1),
(391, 125, 'Finance', 'credit_note.approval_list', 'Credit Note Approval List', 1),
(392, 124, 'Finance', 'credit_note.approve', 'Credit Note Approve', 1),
(393, 126, 'Finance', 'debit_note.list', 'Debit Note List', 1),
(394, 126, 'Finance', 'debit_note.add', 'Debit Note Add', 1),
(395, 126, 'Finance', 'debit_note.view', 'Debit Note View', 1),
(396, 126, 'Finance', 'debit_note.approval_list', 'Debit Note Approval List', 1),
(397, 126, 'Finance', 'debit_note.approve', 'Debit Note Approve', 1),
(398, 127, 'Finance', 'refund_entry.list', 'Refund Entry List', 1),
(399, 127, 'Finance', 'refund_entry.add', 'Refund Entry Add', 1),
(400, 127, 'Finance', 'refund_entry.view', 'Refund Entry View', 1),
(401, 127, 'Finance', 'refund_entry.approval_list', 'Refund Entry Approval List', 1),
(402, 127, 'Finance', 'refund_entry.approve', 'Refund Entry Approve', 1),
(403, 128, 'Sponsor', 'sponser.list', 'Sponsor List', 1),
(404, 128, 'Sponsor', 'sponser.add', 'Sponsor Add', 1),
(405, 128, 'Sponsor', 'sponser.edit', 'Sponsor Edit', 1),
(406, 129, 'Sponsor', 'sponser_has_students.sponser_list', 'Sponsor Has Students List', 1),
(407, 129, 'Sponsor', 'sponser_has_students.add', 'Sponsor Has Students Add', 1),
(408, 130, 'Hostel', 'room_type.list', 'Room Type List', 1),
(409, 130, 'Hostel', 'room_type.add', 'Room Type Add', 1),
(410, 130, 'Hostel', 'room_type.edit', 'Room Type Edit', 1),
(411, 131, 'Hostel', 'hostel_inventory.list', 'Hostel Inventory List', 1),
(412, 131, 'Hostel', 'hostel_inventory.add', 'Hostel Inventory Add', 1),
(413, 131, 'Hostel', 'hostel_inventory.edit', 'Hostel Inventory Edit', 1),
(414, 132, 'Hostel', 'hostel_registration.list', 'Hostel Registration List', 1),
(415, 132, 'Hostel', 'hostel_registration.add', 'Hostel Registration Add', 1),
(416, 132, 'Hostel', 'hostel_registration.edit', 'Hostel Registration Edit', 1),
(417, 133, 'Hostel', 'building_setup.list', 'Apartment Setup List', 1),
(418, 133, 'Hostel', 'building_setup.add', 'Apartment Setup Add', 1),
(419, 133, 'Hostel', 'building_setup.edit', 'Apartment Setup Edit', 1),
(420, 134, 'Hostel', 'block_setup.list', 'Unit Setup List', 1),
(421, 134, 'Hostel', 'block_setup.add', 'Unit Setup Add', 1),
(422, 134, 'Hostel', 'block_setup.edit', 'Unit Setup Edit', 1),
(423, 135, 'Hostel', 'room_setup.list', 'Hostel Room Setup List', 1),
(424, 135, 'Hostel', 'room_setup.add', 'Hostel Room Setup Add', 1),
(425, 135, 'Hostel', 'room_setup.edit', 'Hostel Room Setup Edit', 1),
(426, 136, 'Hostel', 'room_allotment.list', 'Room Allotment List', 1),
(427, 136, 'Hostel', 'room_allotment.add', 'Room Allotment Add', 1),
(428, 136, 'Hostel', 'room_allotment.edit', 'Room Allotment Edit', 1),
(429, 137, 'Hostel', 'room_allotment.summary_list', 'Room Allotment Summary List', 1),
(430, 137, 'Hostel', 'room_allotment.summary_view', 'Room Allotment Summary View', 1),
(431, 138, 'Hostel', 'room_vacancy.list', 'Room Vacancy List', 1),
(432, 138, 'Hostel', 'room_vacancy.allot_room', 'Room Vacancy Allot Room', 1),
(433, 138, 'Hostel', 'room_vacancy.view', 'Room Vacancy View', 1),
(434, 139, 'Facility', 'equipment.list', 'Equipments List', 1),
(435, 139, 'Facility', 'equipment.add', 'Equipments Add', 1),
(436, 139, 'Facility', 'equipment.edit', 'Equipments Edit', 1),
(437, 140, 'Facility', 'facility_room_type.list', 'Facility Room Type List', 1),
(438, 140, 'Facility', 'facility_room_type.add', 'Facility Room Type Add', 1),
(439, 140, 'Facility', 'facility_room_type.edit', 'Facility Room Type Edit', 1),
(440, 141, 'Facility', 'facility_building_registration.list', 'Facility Building List', 1),
(441, 141, 'Facility', 'facility_building_registration.add', 'Facility Building Add', 1),
(442, 141, 'Facility', 'facility_building_registration.edit', 'Facility Building Edit', 1),
(443, 142, 'Facility', 'facility_room_setup.list', 'Facility Room Setup List', 1),
(444, 142, 'Facility', 'facility_room_setup.add', 'Facility Room Setup Add', 1),
(445, 142, 'Facility', 'facility_room_setup.edit', 'Facility Room Setup Edit', 1),
(446, 143, 'Facility', 'room_booking.list', 'Apply For Booking List', 1),
(447, 143, 'Facility', 'room_booking.book_room', 'Room Booking Add', 1),
(448, 144, 'Facility', 'room_booking.summary_list', 'Room Booking Summary List', 1),
(449, 144, 'Facility', 'room_booking.view', 'View Room Booking', 1),
(450, 145, 'Internship', 'internship_limit.edit', 'Max. Internship Limit Per Student Edit', 1),
(451, 146, 'Internship', 'company_type.list', 'Internship Company Type List', 1),
(452, 146, 'Internship', 'company_type.add', 'Internship Company Type Add', 1),
(453, 146, 'Internship', 'company_type.edit', 'Internship Company Type Edit', 1),
(454, 147, 'Internship', 'internship_company_registration.list', 'Intrernship Company Registration List', 1),
(455, 147, 'Internship', 'internship_company_registration.add', 'Internship Company Registration Add', 1),
(456, 147, 'Internship', 'internship_company_registration.edit', 'Internship Company Registration Edit', 1),
(457, 148, 'Internship', 'internship_application.list', 'Internship Application List', 1),
(458, 148, 'Internship', 'internship_application.view', 'Internship Application View', 1),
(459, 149, 'Internship', 'internship_application.approval_list', 'Internship Application Approval List', 1),
(460, 149, 'Internship', 'internship_application.approve', 'Internship Application Approve', 1),
(461, 150, 'Internship', 'internship_application.reporting', 'Reporting List', 1),
(462, 150, 'Internship', 'internship_application.reporting_view', 'Reporting View', 1),
(463, 151, 'Internship', 'placement_form.list', 'Placement Form List', 1),
(464, 151, 'Internship', 'placement_form.add', 'Placement Form Add', 1),
(465, 151, 'Internship', 'placement_form.view', 'Placement Form VIew', 1),
(466, 152, 'Alumni', 'allumini.list', 'Allumini List', 1),
(467, 152, 'Alumni', 'allumini.view', 'Alumni View', 1),
(468, 153, 'Communication', 'email_template.list', 'Communication Email Templates List', 1),
(469, 153, 'Communication', 'email_template.add', 'Communication Email Templates Add', 1),
(470, 153, 'Communication', 'email_template.edit', 'Communication Email Templates Edit', 1),
(471, 154, 'Communication', 'email_communication_group.list', 'Communication Email Group List', 1),
(472, 154, 'Communication', 'email_communication_group.add', 'Communication Email Group Add', 1),
(473, 154, 'Communication', 'email_communication_group.edit', 'Communication Email Group Edit', 1),
(474, 155, 'Communication', 'email_communication_group.recepients_list', 'Communication Email Group Recepients List', 1),
(475, 155, 'Communication', 'email_communication_group.add_recepients', 'Communication Email Group Recepients Add', 1),
(476, 156, 'Communication', 'template_message.list', 'Communication Messaging Template List', 1),
(477, 156, 'Communication', 'template_message.add', 'Communication Messaging Template Add', 1),
(478, 156, 'Communication', 'template_message.edit', 'Communication Messaging Template Edit', 1),
(479, 157, 'Communication', 'communication_message_group.list', 'Communication Messaging Group List', 1),
(480, 157, 'Communication', 'communication_message_group.add', 'Communication Messaging Group Add', 1),
(481, 157, 'Communication', 'communication_message_group.edit', 'Communication Messaging Group Edit', 1),
(482, 158, 'Communication', 'communication_message_group.recepients_list', 'Communication Message Group Recepients List', 1),
(483, 155, 'Communication', 'email_communication_group.view', 'Communication Email Group Recepients View', 1),
(484, 158, 'Communication', 'communication_message_group.add_recepients', 'Communication Message Group Recepients Add', 1),
(485, 158, 'Communication', 'communication_message_group.view', 'Communication Message Group Recepients View', 1),
(486, 159, 'IPS', 'topic.list', 'Research Topic List', 1),
(487, 159, 'IPS', 'topic.add', 'Research Topic Add', 1),
(488, 159, 'IPS', 'topic.edit', 'Research Topic Edit', 1),
(489, 160, 'IPS', 'research_status.list', 'Research Status List', 1),
(490, 160, 'IPS', 'research_status.add', 'Research Status Add', 1),
(491, 160, 'IPS', 'research_status.edit', 'Research Status Edit', 1),
(492, 161, 'IPS', 'research_fieldofinterest.list', 'Research Field Of Interest List', 1),
(493, 161, 'IPS', 'research_fieldofinterest.add', 'Research Field Of Interest Add', 1),
(494, 161, 'IPS', 'research_fieldofinterest.edit', 'Research Field Of Interest Edit', 1),
(495, 162, 'IPS', 'research_reason_applying.list', 'Reason For Applying List', 1),
(496, 162, 'IPS', 'research_reason_applying.add', 'Reason For Applying Add', 1),
(497, 162, 'IPS', 'research_reason_applying.edit', 'Reason For Applying Edit', 1),
(498, 163, 'IPS', 'research_category.list', 'Research Category List', 1),
(499, 163, 'IPS', 'research_category.add', 'Research Category Add', 1),
(500, 163, 'IPS', 'research_category.edit', 'Research Category Edit', 1),
(501, 164, 'IPS', 'research_colloquium.list', 'Colloquium Setup List', 1),
(502, 164, 'IPS', 'research_colloquium.add', 'Colloquium Setup Add', 1),
(503, 164, 'IPS', 'research_colloquium.edit', 'Colloquium Setup Edit', 1),
(504, 165, 'IPS', 'research_advisory.list', 'Research Advisory List', 1),
(505, 165, 'IPS', 'research_advisory.add', 'Research Advisory Add', 1),
(506, 165, 'IPS', 'research_advisory.edit', 'Research Advisory Edit', 1),
(507, 166, 'IPS', 'research_readers.list', 'Research Readers List', 1),
(508, 166, 'IPS', 'research_readers.add', 'Research Readers Add', 1),
(509, 166, 'IPS', 'research_readers.edit', 'Research Readers Edit', 1),
(510, 167, 'IPS', 'research_comitee.list', 'Research Proposal Defense Comitee List', 1),
(511, 167, 'IPS', 'research_comitee.add', 'Research Proposal Defense Comitee Add', 1),
(512, 167, 'IPS', 'research_comitee.edit', 'Research Proposal Defense Comitee Edit', 1),
(513, 168, 'IPS', 'research_chapter.list', 'Research Chapter List', 1),
(514, 168, 'IPS', 'research_chapter.add', 'Research Chapter Add', 1),
(515, 168, 'IPS', 'research_chapter.edit', 'Research Chapter Edit', 1),
(516, 169, 'IPS', 'research_deliverables.list', 'Research Deliverables Setup List', 1),
(517, 169, 'IPS', 'research_deliverables.add', 'Research Deliverables Setup Add', 1),
(518, 169, 'IPS', 'research_deliverables.edit', 'Research Deliverables Setup Edit', 1),
(519, 170, 'IPS', 'submitted_deliverables.list', 'Research Submitted Deliverables Application List', 1),
(520, 170, 'IPS', 'submitted_deliverables.add', 'Research Submitted Deliverables Application Add', 1),
(521, 170, 'IPS', 'submitted_deliverables.approve', 'Research Submitted Deliverables Application Approve', 1),
(522, 170, 'IPS', 'submitted_deliverables.view', 'Research Submitted Deliverables Application View', 1),
(523, 171, 'IPS', 'student_duration_tagging.edit', 'Student Duration Tagging Edit', 1),
(524, 172, 'IPS', 'research_stage.list', 'Research Stage List', 1),
(525, 172, 'IPS', 'research_stage.add', 'Research Stage Add', 1),
(526, 172, 'IPS', 'research_stage.edit', 'Research Stage Edit', 1),
(527, 172, 'IPS', 'research_stage.add_semester', 'Research Stage Add Semester', 1),
(528, 173, 'IPS', 'research_mile_stone.list', 'Research Milestone List', 1),
(529, 173, 'IPS', 'research_mile_stone.add', 'Research Mile Stone Add', 1),
(530, 173, 'IPS', 'research_mile_stone.edit', 'Research Mile Stone Edit', 1),
(531, 174, 'IPS', 'mile_stone_semester.list', 'Tag Mile Stone To Semester List', 1),
(532, 174, 'IPS', 'mile_stone_semester.add', 'Tag Mile Stone To Semester Add', 1),
(533, 174, 'IPS', 'mile_stone_semester.edit', 'Tag Mile Stone To Semester Edit', 1),
(534, 175, 'IPS', 'research_stage.overview', 'Research Stages Overwiew', 1),
(535, 176, 'IPS', 'research_supervisor_role.list', 'Research Supervisor Role List', 1),
(536, 176, 'IPS', 'research_supervisor_role.add', 'Research Supervisor Role Add', 1),
(537, 176, 'IPS', 'research_supervisor_role.edit', 'Research Supervisor Role Edit', 1),
(538, 177, 'IPS', 'research_supervisor.list', 'Research Supervisor List', 1),
(539, 177, 'IPS', 'research_supervisor.add', 'Research Supervisor Add', 1),
(540, 177, 'IPS', 'research_supervisor.edit', 'Research Supervisor Edit', 1),
(541, 178, 'IPS', 'research_supervisor_tagging.add', 'Research Supervisor Tagging Add / Edit', 1),
(542, 179, 'IPS', 'research_examiner_role.list', 'Research Examiner Role List', 1),
(543, 179, 'IPS', 'research_examiner_role.add', 'Research Examiner Role Add', 1),
(544, 179, 'IPS', 'research_examiner_role.edit', 'Research Examiner Role Edit', 1),
(545, 180, 'IPS', 'research_examiner.list', 'Research Examiner List', 1),
(546, 180, 'IPS', 'research_examiner.add', 'Research Examiner Add', 1),
(547, 180, 'IPS', 'research_examiner.edit', 'Research Examiner Edit', 1),
(548, 181, 'IPS', 'research_proposal.list', 'Research Proposal List', 1),
(549, 181, 'IPS', 'research_proposal.add', 'Research Proposal Add', 1),
(550, 181, 'IPS', 'research_proposal.view', 'Research Proposal View', 1),
(551, 182, 'IPS', 'research_articleship.list', 'Research Articleship', 1),
(552, 182, 'IPS', 'research_articleship.add', 'Research Articleship Add', 1),
(553, 182, 'IPS', 'research_articleship.view', 'Research Articleship View', 1),
(554, 181, 'IPS', 'research_proposal.edit', 'Research Proposal Edit', 1),
(555, 182, 'IPS', 'research_articleship.edit', 'Research Articleship Edit', 1),
(556, 183, 'IPS', 'research_professionalpracricepaper.list', 'Research Professional Practice Paper List', 1),
(557, 183, 'IPS', 'research_professionalpracricepaper.add', 'Research Professional Practice Paper Add', 1),
(558, 183, 'IPS', 'research_professionalpracricepaper.edit', 'Research Professional Practice Paper Edit', 1),
(559, 183, 'IPS', 'research_professionalpracricepaper.view', 'Research Professional Practice Paper View', 1),
(560, 184, 'IPS', 'research_proposal.approval_list', 'Research Proposal Approval List', 1),
(561, 184, 'IPS', 'research_proposal.approve', 'Research Proposal Approve', 1),
(562, 185, 'IPS', 'research_articleship.approval_list', 'Research Articleship Approval List', 1),
(563, 185, 'IPS', 'research_articleship.approve', 'Research Articleship Approve', 1),
(564, 186, 'IPS', 'research_professionalpracricepaper.approval_list', 'Research Professional Practice Paper Approval List', 1),
(565, 186, 'IPS', 'research_professionalpracricepaper.approve', 'Research Professional Practice Paper Approve', 1),
(566, 188, 'IPS', 'research_supervisor_change_application.list', 'Change Supervisor Applicaton List', 1),
(567, 188, 'IPS', 'research_supervisor_change_application.approve', 'Change Supervisor Application Approve', 1),
(568, 188, 'IPS', 'research_supervisor_change_application.view', 'Change Supervisor Application View', 1),
(569, 189, 'Examination', 'ips_progress.student_list', 'IPS Progress Student List', 1),
(570, 189, 'Examination', 'ips_progress.add', 'IPS Progress Add', 1),
(571, 111, 'Finance', 'fee_structure.copy_fee_structure', 'Copy Existing Fee Structure', 1),
(572, 31, 'Setup', 'programme_landscape.addcourse', 'Programme Landscape Add Course', 1);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `product` varchar(200) NOT NULL,
  `created_by` varchar(200) DEFAULT NULL,
  `cateated_on` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `product`, `created_by`, `cateated_on`) VALUES
(1, 'MI8', 'Kiran', '2021-06-05 15:24:38');

-- --------------------------------------------------------

--
-- Table structure for table `profile_details`
--

CREATE TABLE `profile_details` (
  `id` int(20) NOT NULL,
  `full_name` varchar(580) DEFAULT '',
  `salutation` varchar(120) DEFAULT '',
  `first_name` varchar(120) DEFAULT '',
  `last_name` varchar(120) DEFAULT '',
  `id_type` varchar(120) DEFAULT '',
  `passport_number` varchar(120) DEFAULT '',
  `phone` varchar(120) DEFAULT '',
  `email_id` varchar(120) DEFAULT '',
  `passport_expiry_date` varchar(120) DEFAULT '',
  `gender` varchar(120) DEFAULT '',
  `date_of_birth` varchar(120) DEFAULT '',
  `martial_status` varchar(120) DEFAULT '',
  `religion` varchar(120) DEFAULT '',
  `nationality` varchar(120) DEFAULT '',
  `nationality_type` varchar(120) DEFAULT '',
  `id_race` int(10) DEFAULT NULL,
  `mail_address1` varchar(120) DEFAULT '',
  `mail_address2` varchar(120) DEFAULT '',
  `mailing_country` int(120) DEFAULT NULL,
  `mailing_state` int(120) DEFAULT NULL,
  `mailing_city` varchar(120) DEFAULT '',
  `mailing_zipcode` varchar(120) DEFAULT '',
  `permanent_address1` varchar(120) DEFAULT '',
  `permanent_address2` varchar(120) DEFAULT '',
  `permanent_country` int(120) DEFAULT NULL,
  `permanent_state` int(120) DEFAULT NULL,
  `permanent_city` varchar(120) DEFAULT '',
  `permanent_zipcode` varchar(120) DEFAULT '',
  `nric` varchar(50) DEFAULT '',
  `status` int(20) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp(),
  `id_student` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profile_details`
--

INSERT INTO `profile_details` (`id`, `full_name`, `salutation`, `first_name`, `last_name`, `id_type`, `passport_number`, `phone`, `email_id`, `passport_expiry_date`, `gender`, `date_of_birth`, `martial_status`, `religion`, `nationality`, `nationality_type`, `id_race`, `mail_address1`, `mail_address2`, `mailing_country`, `mailing_state`, `mailing_city`, `mailing_zipcode`, `permanent_address1`, `permanent_address2`, `permanent_country`, `permanent_state`, `permanent_city`, `permanent_zipcode`, `nric`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`, `id_student`) VALUES
(1, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', '', '', NULL, NULL, '2020-07-11 01:20:44', NULL, '2020-07-11 01:20:44', 1),
(2, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', '', '', NULL, NULL, '2020-07-11 06:25:02', NULL, '2020-07-11 06:25:02', 1),
(3, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', '', '', NULL, NULL, '2020-07-11 11:53:53', NULL, '2020-07-11 11:53:53', 2),
(4, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', '', '', NULL, NULL, '2020-07-11 12:55:29', NULL, '2020-07-11 12:55:29', 3),
(5, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', '', '', NULL, NULL, '2020-07-11 13:15:32', NULL, '2020-07-11 13:15:32', 4),
(6, 'Miss. Anjuman Syed', 'Miss', 'Anjuman', 'Syed', '123', '123', '55678', 'as@cms.com', '08/19/2020', 'Female', '2020-07-24', 'Single', 'Hinduism', 'Malaysian', '', 0, 'KL', 'KL', 1, 1, 'KL', 'KL', '123, KL', 'KL', 1, 1, 'KL', '234567', '', NULL, NULL, '2020-08-04 14:28:09', NULL, '2020-08-04 14:28:09', 5),
(7, 'Mr. Nabasha Ali', 'Mr', 'Nabasha', 'Ali', 'Passport', '323213123', '78987856', 'nab@cms.com', '28-08-2020', 'Female', '1990-08-31', 'Married', 'Christianity', 'Malaysian', '', 0, 'Mohhamed Ali Street, ', 'KL', 1, 1, 'KL', '247827', 'Mohhamed Ali Street, ', 'KL', 2, 1, 'KL', '2123123', '', NULL, NULL, '2020-08-10 12:50:24', NULL, '2020-08-10 12:50:24', 6),
(8, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', '', '', NULL, NULL, '2020-08-10 14:27:11', NULL, '2020-08-10 14:27:11', 7),
(9, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', '', '', NULL, NULL, '2020-08-17 12:29:25', NULL, '2020-08-17 12:29:25', 8),
(10, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', '', '', NULL, NULL, '2020-08-19 08:58:41', NULL, '2020-08-19 08:58:41', 9),
(11, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', '', '', NULL, NULL, '2020-08-19 09:40:24', NULL, '2020-08-19 09:40:24', 10),
(12, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', '', '', NULL, NULL, '2020-08-20 05:22:04', NULL, '2020-08-20 05:22:04', 11),
(13, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', '', '', NULL, NULL, '2020-08-20 14:10:25', NULL, '2020-08-20 14:10:25', 12),
(14, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', '', '', NULL, NULL, '2020-08-23 12:41:29', NULL, '2020-08-23 12:41:29', 13),
(15, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', '', '', NULL, NULL, '2020-08-23 13:03:42', NULL, '2020-08-23 13:03:42', 14),
(16, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', '', '', NULL, NULL, '2020-08-24 20:48:16', NULL, '2020-08-24 20:48:16', 15),
(17, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', '', '', NULL, NULL, '2020-08-25 14:11:00', NULL, '2020-08-25 14:11:00', 16),
(18, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', '', '', NULL, NULL, '2020-08-27 13:44:45', NULL, '2020-08-27 13:44:45', 17),
(19, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', '', '', NULL, NULL, '2020-09-12 13:04:21', NULL, '2020-09-12 13:04:21', 23),
(20, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', '', '', NULL, NULL, '2020-09-13 14:35:44', NULL, '2020-09-13 14:35:44', 24),
(21, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', '', '', NULL, NULL, '2020-09-15 05:05:22', NULL, '2020-09-15 05:05:22', 26),
(22, 'DATIN. Ragina Federar', '2', 'Ragina', 'Federar', '', '', '', 'rf@cms.com', '15-10-2020', 'Male', '2020-10-01', 'Single', '2', 'Other', '', 3, 'KL', 'KL', 110, 1, 'KL', '1234', 'KL', 'KL', 110, 1, 'KL', '12345', '1190889', NULL, NULL, '2020-10-08 22:15:00', NULL, '2020-10-08 22:15:00', 27),
(23, 'AG. Ragina Federer', '1', 'Ragina', 'Federer', '', '', '98989', '', '10/01/2020', 'Male', '2002-10-21', 'Single', '1', 'Malaysian', '', 3, '12, Dala Street', 'KL', 110, 1, 'KL', '12345', '12, Dalal Street', 'KL', 110, 1, 'KL', '12111', 'NF888', NULL, NULL, '2020-10-10 01:14:33', NULL, '2020-10-10 01:14:33', 28),
(24, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', '', '', NULL, NULL, '2020-10-10 01:50:41', NULL, '2020-10-10 01:50:41', 29),
(25, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', '', '', NULL, NULL, '2020-10-27 07:28:41', NULL, '2020-10-27 07:28:41', 30),
(26, 'MR. Student One', '23', 'Student', 'One', '', '', '88881111', '', '11/01/2022', 'Male', '2000-11-01', 'Single', '1', 'Malaysian', '', 1, 'Asian Towers, Near Malysian Embassy', 'KL', 110, 1, 'KL', '200001', 'Asian Towers, Near Malysian Embassy', 'KL', 110, 1, 'KL', '200001', 'NR000001', NULL, NULL, '2020-11-08 11:41:01', NULL, '2020-11-08 11:41:01', 31),
(27, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', '', '', NULL, NULL, '2020-11-09 12:13:40', NULL, '2020-11-09 12:13:40', 32),
(28, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', '', '', NULL, NULL, '2020-11-09 12:13:56', NULL, '2020-11-09 12:13:56', 33),
(29, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', '', '', NULL, NULL, '2020-11-09 20:50:13', NULL, '2020-11-09 20:50:13', 34),
(30, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', '', '', NULL, NULL, '2020-11-10 19:57:42', NULL, '2020-11-10 19:57:42', 35),
(31, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', '', '', NULL, NULL, '2020-11-15 10:28:35', NULL, '2020-11-15 10:28:35', 36);

-- --------------------------------------------------------

--
-- Table structure for table `race_setup`
--

CREATE TABLE `race_setup` (
  `id` int(20) NOT NULL,
  `name` varchar(250) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `race_setup`
--

INSERT INTO `race_setup` (`id`, `name`, `code`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Malays', '05', 1, NULL, '2020-07-10 23:24:07', NULL, '2020-07-10 23:24:07'),
(2, 'Chinese', '02', 1, NULL, '2020-07-10 23:24:14', NULL, '2020-07-10 23:24:14'),
(3, 'Indian', '03', 1, NULL, '2020-08-27 17:14:35', NULL, '2020-08-27 17:14:35'),
(4, 'jawa', '04', 1, NULL, '2020-08-27 17:15:24', NULL, '2020-08-27 17:15:24'),
(5, 'Asian', '01', 1, NULL, '2020-11-13 09:06:53', NULL, '2020-11-13 09:06:53');

-- --------------------------------------------------------

--
-- Table structure for table `religion_setup`
--

CREATE TABLE `religion_setup` (
  `id` int(20) NOT NULL,
  `name` varchar(250) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `religion_setup`
--

INSERT INTO `religion_setup` (`id`, `name`, `code`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Islam', 'ISLAM', 1, NULL, '2020-07-10 23:24:44', NULL, '2020-07-10 23:24:44'),
(2, 'Buddhism', 'Buddhism', 1, NULL, '2020-07-10 23:24:56', NULL, '2020-07-10 23:24:56'),
(3, 'Hindu', 'Hindu', 1, NULL, '2020-11-13 09:10:41', NULL, '2020-11-13 09:10:41'),
(4, 'Atheists', 'Atheists', 1, NULL, '2021-01-14 20:04:02', NULL, '2021-01-14 20:04:02'),
(5, 'Agnostics', 'Agnostics', 1, NULL, '2021-01-14 20:05:17', NULL, '2021-01-14 20:05:17'),
(6, 'Bahais', 'Bahais', 1, NULL, '2021-01-14 20:05:17', NULL, '2021-01-14 20:05:17'),
(7, 'Buddhists', 'Buddhists', 1, NULL, '2021-01-14 20:05:17', NULL, '2021-01-14 20:05:17'),
(8, 'Chinese folk-religionists', 'Chinese folk-religionists', 1, NULL, '2021-01-14 20:05:17', NULL, '2021-01-14 20:05:17'),
(9, 'Christians', 'Christians', 1, NULL, '2021-01-14 20:05:17', NULL, '2021-01-14 20:05:17'),
(10, 'Confucianists', 'Confucianists', 1, NULL, '2021-01-14 20:05:17', NULL, '2021-01-14 20:05:17'),
(11, 'Daoists', 'Daoists', 1, NULL, '2021-01-14 20:05:17', NULL, '2021-01-14 20:05:17'),
(12, 'Hindus', 'Hindus', 1, NULL, '2021-01-14 20:05:17', NULL, '2021-01-14 20:05:17'),
(13, 'Jains', 'Jains', 1, NULL, '2021-01-14 20:05:17', NULL, '2021-01-14 20:05:17'),
(14, 'Shintoists', 'Shintoists', 1, NULL, '2021-01-14 20:06:34', NULL, '2021-01-14 20:06:34'),
(15, 'Spiritists', 'Spiritists', 1, NULL, '2021-01-14 20:06:34', NULL, '2021-01-14 20:06:34'),
(16, 'Sikhs', 'Sikhs', 1, NULL, '2021-01-14 20:06:34', NULL, '2021-01-14 20:06:34'),
(17, 'Zoroastrians', 'Zoroastrians', 1, NULL, '2021-01-14 20:06:34', NULL, '2021-01-14 20:06:34');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(20) NOT NULL,
  `role` varchar(250) DEFAULT '',
  `status` int(20) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `role`, `status`) VALUES
(1, 'Administrator', 1),
(13, 'Examination Chief', 1),
(14, 'Permision View', 1),
(15, 'AEU Administrator', 1),
(16, 'AEU Examination Admin', 1),
(17, 'Research Colloquium', 1);

-- --------------------------------------------------------

--
-- Table structure for table `role_permissions`
--

CREATE TABLE `role_permissions` (
  `id` bigint(20) NOT NULL,
  `id_role` bigint(20) DEFAULT NULL,
  `id_permission` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role_permissions`
--

INSERT INTO `role_permissions` (`id`, `id_role`, `id_permission`) VALUES
(84, 12, 5),
(85, 12, 7),
(459, 1, 5),
(460, 1, 6),
(461, 1, 7),
(462, 1, 8),
(463, 1, 9),
(464, 1, 10),
(465, 1, 11),
(466, 1, 12),
(467, 1, 13),
(468, 1, 14),
(469, 1, 15),
(470, 1, 16),
(471, 1, 17),
(472, 1, 18),
(473, 1, 19),
(474, 1, 20),
(475, 1, 21),
(476, 1, 22),
(477, 1, 23),
(478, 1, 24),
(479, 1, 25),
(480, 1, 26),
(481, 1, 27),
(482, 1, 28),
(483, 1, 29),
(484, 1, 30),
(485, 1, 31),
(486, 1, 33),
(487, 1, 34),
(488, 1, 35),
(489, 1, 36),
(490, 1, 37),
(491, 1, 38),
(492, 1, 39),
(493, 1, 40),
(494, 1, 41),
(495, 1, 42),
(496, 1, 43),
(497, 1, 44),
(498, 1, 45),
(499, 1, 46),
(500, 1, 47),
(501, 1, 48),
(502, 1, 49),
(503, 1, 50),
(504, 1, 51),
(505, 1, 52),
(506, 1, 53),
(507, 1, 54),
(508, 1, 55),
(509, 1, 56),
(510, 1, 57),
(511, 1, 58),
(512, 1, 59),
(513, 1, 60),
(514, 1, 61),
(515, 1, 62),
(516, 1, 63),
(517, 1, 64),
(518, 1, 65),
(519, 1, 66),
(520, 1, 67),
(532, 13, 14),
(548, 14, 10),
(549, 14, 11),
(550, 14, 12),
(551, 14, 13),
(552, 14, 14),
(553, 14, 15),
(554, 14, 7),
(555, 14, 8),
(556, 14, 9),
(2047, 16, 10),
(2048, 16, 11),
(2049, 16, 12),
(2050, 16, 13),
(2051, 16, 14),
(2052, 16, 15),
(2053, 16, 242),
(2054, 16, 243),
(2055, 16, 244),
(2056, 16, 245),
(2057, 16, 246),
(2058, 16, 247),
(2059, 16, 248),
(2060, 16, 249),
(2061, 16, 250),
(2062, 16, 251),
(2063, 16, 252),
(2064, 16, 253),
(2065, 16, 254),
(2066, 16, 255),
(2067, 16, 256),
(2068, 16, 257),
(2069, 16, 258),
(2070, 16, 259),
(2071, 16, 260),
(2072, 16, 261),
(2073, 16, 262),
(2074, 16, 263),
(2075, 16, 264),
(2076, 16, 265),
(2077, 16, 266),
(2078, 16, 267),
(2079, 16, 268),
(2080, 16, 269),
(2081, 16, 270),
(2082, 16, 271),
(2083, 16, 272),
(2084, 16, 273),
(2085, 16, 274),
(2086, 16, 275),
(2087, 16, 276),
(2088, 16, 277),
(2089, 16, 278),
(2090, 16, 279),
(2091, 16, 280),
(2092, 16, 281),
(2093, 16, 282),
(2094, 16, 283),
(2095, 16, 284),
(2096, 16, 285),
(2097, 16, 286),
(2098, 16, 287),
(2099, 16, 290),
(2100, 16, 291),
(2101, 16, 292),
(2102, 16, 293),
(2103, 16, 294),
(2104, 16, 295),
(2105, 16, 296),
(2106, 16, 297),
(2107, 16, 298),
(2108, 16, 299),
(2109, 16, 300),
(2110, 16, 301),
(2111, 16, 569),
(2112, 16, 570),
(5649, 15, 10),
(5650, 15, 11),
(5651, 15, 12),
(5652, 15, 13),
(5653, 15, 14),
(5654, 15, 15),
(5655, 15, 7),
(5656, 15, 8),
(5657, 15, 9),
(5658, 15, 70),
(5659, 15, 71),
(5660, 15, 72),
(5661, 15, 73),
(5662, 15, 74),
(5663, 15, 75),
(5664, 15, 76),
(5665, 15, 77),
(5666, 15, 78),
(5667, 15, 79),
(5668, 15, 80),
(5669, 15, 81),
(5670, 15, 82),
(5671, 15, 83),
(5672, 15, 84),
(5673, 15, 85),
(5674, 15, 86),
(5675, 15, 87),
(5676, 15, 88),
(5677, 15, 89),
(5678, 15, 90),
(5679, 15, 91),
(5680, 15, 92),
(5681, 15, 93),
(5682, 15, 94),
(5683, 15, 95),
(5684, 15, 96),
(5685, 15, 97),
(5686, 15, 98),
(5687, 15, 99),
(5688, 15, 100),
(5689, 15, 101),
(5690, 15, 102),
(5691, 15, 103),
(5692, 15, 104),
(5693, 15, 105),
(5694, 15, 106),
(5695, 15, 107),
(5696, 15, 108),
(5697, 15, 109),
(5698, 15, 110),
(5699, 15, 111),
(5700, 15, 112),
(5701, 15, 113),
(5702, 15, 114),
(5703, 15, 115),
(5704, 15, 116),
(5705, 15, 117),
(5706, 15, 118),
(5707, 15, 119),
(5708, 15, 120),
(5709, 15, 121),
(5710, 15, 122),
(5711, 15, 123),
(5712, 15, 124),
(5713, 15, 125),
(5714, 15, 126),
(5715, 15, 127),
(5716, 15, 128),
(5717, 15, 129),
(5718, 15, 130),
(5719, 15, 131),
(5720, 15, 132),
(5721, 15, 133),
(5722, 15, 134),
(5723, 15, 135),
(5724, 15, 136),
(5725, 15, 137),
(5726, 15, 138),
(5727, 15, 139),
(5728, 15, 140),
(5729, 15, 141),
(5730, 15, 142),
(5731, 15, 143),
(5732, 15, 144),
(5733, 15, 145),
(5734, 15, 146),
(5735, 15, 147),
(5736, 15, 148),
(5737, 15, 149),
(5738, 15, 572),
(5739, 15, 150),
(5740, 15, 151),
(5741, 15, 152),
(5742, 15, 153),
(5743, 15, 154),
(5744, 15, 155),
(5745, 15, 156),
(5746, 15, 157),
(5747, 15, 158),
(5748, 15, 159),
(5749, 15, 160),
(5750, 15, 161),
(5751, 15, 162),
(5752, 15, 163),
(5753, 15, 164),
(5754, 15, 165),
(5755, 15, 166),
(5756, 15, 167),
(5757, 15, 168),
(5758, 15, 169),
(5759, 15, 170),
(5760, 15, 171),
(5761, 15, 172),
(5762, 15, 173),
(5763, 15, 174),
(5764, 15, 175),
(5765, 15, 176),
(5766, 15, 177),
(5767, 15, 178),
(5768, 15, 179),
(5769, 15, 180),
(5770, 15, 181),
(5771, 15, 182),
(5772, 15, 183),
(5773, 15, 184),
(5774, 15, 185),
(5775, 15, 186),
(5776, 15, 187),
(5777, 15, 188),
(5778, 15, 189),
(5779, 15, 190),
(5780, 15, 191),
(5781, 15, 192),
(5782, 15, 193),
(5783, 15, 194),
(5784, 15, 195),
(5785, 15, 196),
(5786, 15, 197),
(5787, 15, 198),
(5788, 15, 199),
(5789, 15, 200),
(5790, 15, 201),
(5791, 15, 202),
(5792, 15, 203),
(5793, 15, 204),
(5794, 15, 205),
(5795, 15, 206),
(5796, 15, 207),
(5797, 15, 208),
(5798, 15, 209),
(5799, 15, 210),
(5800, 15, 211),
(5801, 15, 212),
(5802, 15, 213),
(5803, 15, 214),
(5804, 15, 215),
(5805, 15, 216),
(5806, 15, 217),
(5807, 15, 218),
(5808, 15, 219),
(5809, 15, 220),
(5810, 15, 221),
(5811, 15, 222),
(5812, 15, 223),
(5813, 15, 224),
(5814, 15, 225),
(5815, 15, 226),
(5816, 15, 227),
(5817, 15, 228),
(5818, 15, 229),
(5819, 15, 230),
(5820, 15, 231),
(5821, 15, 232),
(5822, 15, 233),
(5823, 15, 234),
(5824, 15, 235),
(5825, 15, 236),
(5826, 15, 237),
(5827, 15, 238),
(5828, 15, 239),
(5829, 15, 240),
(5830, 15, 241),
(5831, 15, 242),
(5832, 15, 243),
(5833, 15, 244),
(5834, 15, 245),
(5835, 15, 246),
(5836, 15, 247),
(5837, 15, 248),
(5838, 15, 249),
(5839, 15, 250),
(5840, 15, 251),
(5841, 15, 252),
(5842, 15, 253),
(5843, 15, 254),
(5844, 15, 255),
(5845, 15, 256),
(5846, 15, 257),
(5847, 15, 258),
(5848, 15, 259),
(5849, 15, 260),
(5850, 15, 261),
(5851, 15, 262),
(5852, 15, 263),
(5853, 15, 264),
(5854, 15, 265),
(5855, 15, 266),
(5856, 15, 267),
(5857, 15, 268),
(5858, 15, 269),
(5859, 15, 270),
(5860, 15, 271),
(5861, 15, 272),
(5862, 15, 273),
(5863, 15, 274),
(5864, 15, 275),
(5865, 15, 276),
(5866, 15, 277),
(5867, 15, 278),
(5868, 15, 279),
(5869, 15, 280),
(5870, 15, 281),
(5871, 15, 282),
(5872, 15, 283),
(5873, 15, 284),
(5874, 15, 285),
(5875, 15, 286),
(5876, 15, 287),
(5877, 15, 290),
(5878, 15, 291),
(5879, 15, 292),
(5880, 15, 293),
(5881, 15, 294),
(5882, 15, 295),
(5883, 15, 296),
(5884, 15, 297),
(5885, 15, 298),
(5886, 15, 299),
(5887, 15, 300),
(5888, 15, 301),
(5889, 15, 302),
(5890, 15, 303),
(5891, 15, 304),
(5892, 15, 305),
(5893, 15, 306),
(5894, 15, 307),
(5895, 15, 308),
(5896, 15, 309),
(5897, 15, 310),
(5898, 15, 311),
(5899, 15, 312),
(5900, 15, 315),
(5901, 15, 316),
(5902, 15, 317),
(5903, 15, 318),
(5904, 15, 319),
(5905, 15, 320),
(5906, 15, 321),
(5907, 15, 322),
(5908, 15, 323),
(5909, 15, 324),
(5910, 15, 325),
(5911, 15, 326),
(5912, 15, 327),
(5913, 15, 328),
(5914, 15, 329),
(5915, 15, 330),
(5916, 15, 331),
(5917, 15, 332),
(5918, 15, 333),
(5919, 15, 334),
(5920, 15, 335),
(5921, 15, 336),
(5922, 15, 337),
(5923, 15, 338),
(5924, 15, 339),
(5925, 15, 340),
(5926, 15, 341),
(5927, 15, 342),
(5928, 15, 343),
(5929, 15, 344),
(5930, 15, 345),
(5931, 15, 346),
(5932, 15, 347),
(5933, 15, 348),
(5934, 15, 349),
(5935, 15, 350),
(5936, 15, 571),
(5937, 15, 351),
(5938, 15, 352),
(5939, 15, 353),
(5940, 15, 354),
(5941, 15, 355),
(5942, 15, 356),
(5943, 15, 357),
(5944, 15, 358),
(5945, 15, 359),
(5946, 15, 360),
(5947, 15, 361),
(5948, 15, 362),
(5949, 15, 363),
(5950, 15, 364),
(5951, 15, 366),
(5952, 15, 367),
(5953, 15, 368),
(5954, 15, 369),
(5955, 15, 365),
(5956, 15, 370),
(5957, 15, 371),
(5958, 15, 372),
(5959, 15, 373),
(5960, 15, 374),
(5961, 15, 375),
(5962, 15, 376),
(5963, 15, 377),
(5964, 15, 378),
(5965, 15, 379),
(5966, 15, 380),
(5967, 15, 381),
(5968, 15, 382),
(5969, 15, 383),
(5970, 15, 384),
(5971, 15, 385),
(5972, 15, 386),
(5973, 15, 387),
(5974, 15, 392),
(5975, 15, 388),
(5976, 15, 389),
(5977, 15, 390),
(5978, 15, 391),
(5979, 15, 393),
(5980, 15, 394),
(5981, 15, 395),
(5982, 15, 396),
(5983, 15, 397),
(5984, 15, 398),
(5985, 15, 399),
(5986, 15, 400),
(5987, 15, 401),
(5988, 15, 402),
(5989, 15, 403),
(5990, 15, 404),
(5991, 15, 405),
(5992, 15, 406),
(5993, 15, 407),
(5994, 15, 408),
(5995, 15, 409),
(5996, 15, 410),
(5997, 15, 411),
(5998, 15, 412),
(5999, 15, 413),
(6000, 15, 414),
(6001, 15, 415),
(6002, 15, 416),
(6003, 15, 417),
(6004, 15, 418),
(6005, 15, 419),
(6006, 15, 420),
(6007, 15, 421),
(6008, 15, 422),
(6009, 15, 423),
(6010, 15, 424),
(6011, 15, 425),
(6012, 15, 426),
(6013, 15, 427),
(6014, 15, 428),
(6015, 15, 429),
(6016, 15, 430),
(6017, 15, 431),
(6018, 15, 432),
(6019, 15, 433),
(6020, 15, 434),
(6021, 15, 435),
(6022, 15, 436),
(6023, 15, 437),
(6024, 15, 438),
(6025, 15, 439),
(6026, 15, 440),
(6027, 15, 441),
(6028, 15, 442),
(6029, 15, 443),
(6030, 15, 444),
(6031, 15, 445),
(6032, 15, 446),
(6033, 15, 447),
(6034, 15, 448),
(6035, 15, 449),
(6036, 15, 450),
(6037, 15, 451),
(6038, 15, 452),
(6039, 15, 453),
(6040, 15, 454),
(6041, 15, 455),
(6042, 15, 456),
(6043, 15, 457),
(6044, 15, 458),
(6045, 15, 459),
(6046, 15, 460),
(6047, 15, 461),
(6048, 15, 462),
(6049, 15, 463),
(6050, 15, 464),
(6051, 15, 465),
(6052, 15, 466),
(6053, 15, 467),
(6054, 15, 468),
(6055, 15, 469),
(6056, 15, 470),
(6057, 15, 471),
(6058, 15, 472),
(6059, 15, 473),
(6060, 15, 474),
(6061, 15, 475),
(6062, 15, 483),
(6063, 15, 476),
(6064, 15, 477),
(6065, 15, 478),
(6066, 15, 479),
(6067, 15, 480),
(6068, 15, 481),
(6069, 15, 482),
(6070, 15, 484),
(6071, 15, 485),
(6072, 15, 489),
(6073, 15, 490),
(6074, 15, 491),
(6075, 15, 492),
(6076, 15, 493),
(6077, 15, 494),
(6078, 15, 495),
(6079, 15, 496),
(6080, 15, 497),
(6081, 15, 498),
(6082, 15, 499),
(6083, 15, 500),
(6084, 15, 501),
(6085, 15, 502),
(6086, 15, 503),
(6087, 15, 504),
(6088, 15, 505),
(6089, 15, 506),
(6090, 15, 507),
(6091, 15, 508),
(6092, 15, 509),
(6093, 15, 510),
(6094, 15, 511),
(6095, 15, 512),
(6096, 15, 513),
(6097, 15, 514),
(6098, 15, 515),
(6099, 15, 516),
(6100, 15, 517),
(6101, 15, 518),
(6102, 15, 519),
(6103, 15, 520),
(6104, 15, 521),
(6105, 15, 522),
(6106, 15, 523),
(6107, 15, 524),
(6108, 15, 525),
(6109, 15, 526),
(6110, 15, 527),
(6111, 15, 528),
(6112, 15, 529),
(6113, 15, 530),
(6114, 15, 531),
(6115, 15, 532),
(6116, 15, 533),
(6117, 15, 534),
(6118, 15, 535),
(6119, 15, 536),
(6120, 15, 537),
(6121, 15, 538),
(6122, 15, 539),
(6123, 15, 540),
(6124, 15, 541),
(6125, 15, 542),
(6126, 15, 543),
(6127, 15, 544),
(6128, 15, 545),
(6129, 15, 546),
(6130, 15, 547),
(6131, 15, 548),
(6132, 15, 549),
(6133, 15, 550),
(6134, 15, 554),
(6135, 15, 551),
(6136, 15, 552),
(6137, 15, 553),
(6138, 15, 555),
(6139, 15, 556),
(6140, 15, 557),
(6141, 15, 558),
(6142, 15, 559),
(6143, 15, 560),
(6144, 15, 561),
(6145, 15, 562),
(6146, 15, 563),
(6147, 15, 564),
(6148, 15, 565),
(6149, 15, 288),
(6150, 15, 289),
(6151, 15, 566),
(6152, 15, 567),
(6153, 15, 568),
(6154, 15, 569),
(6155, 15, 570),
(6168, 17, 10),
(6169, 17, 11),
(6170, 17, 12),
(6171, 17, 13),
(6172, 17, 14),
(6173, 17, 15),
(6174, 17, 7),
(6175, 17, 8),
(6176, 17, 9),
(6177, 17, 486),
(6178, 17, 487),
(6179, 17, 488),
(6180, 17, 501),
(6181, 17, 502),
(6182, 17, 503);

-- --------------------------------------------------------

--
-- Table structure for table `salutation_setup`
--

CREATE TABLE `salutation_setup` (
  `id` int(20) NOT NULL,
  `name` varchar(250) DEFAULT '',
  `sequence` int(20) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `salutation_setup`
--

INSERT INTO `salutation_setup` (`id`, `name`, `sequence`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Mr', 1, 1, NULL, '2020-08-10 03:08:19', NULL, '2020-08-10 03:08:19'),
(2, 'Mrs', 2, 1, NULL, '2020-08-10 03:11:21', NULL, '2020-08-10 03:11:21'),
(3, 'Ms', 3, 1, NULL, '2020-08-10 03:11:21', NULL, '2020-08-10 03:11:21'),
(4, 'Dr', 4, 1, NULL, '2020-08-10 03:11:21', NULL, '2020-08-10 03:11:21'),
(5, 'Dato', 5, 0, NULL, '2020-08-10 03:11:21', NULL, '2020-08-10 03:11:21'),
(6, 'Ir', 6, 0, NULL, '2020-08-10 03:11:21', NULL, '2020-08-10 03:11:21');

-- --------------------------------------------------------

--
-- Table structure for table `sample`
--

CREATE TABLE `sample` (
  `id` bigint(20) NOT NULL,
  `name` varchar(2048) DEFAULT '',
  `code` varchar(512) DEFAULT '',
  `description` varchar(2042) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sample`
--

INSERT INTO `sample` (`id`, `name`, `code`, `description`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Saliva', 'S', ' Saliva   ', 1, NULL, '2021-01-22 13:43:33', NULL, '2021-01-22 13:43:33'),
(2, 'Blood', 'B', '<p>Collection Of Blood Sample </p>\r\n', 1, NULL, '2021-02-03 11:35:09', NULL, '2021-02-03 11:35:09'),
(3, 'Flouride', 'FL', '<p>Flouride<br>\r\n<strong> 2ML</strong></p>\r\n\r\n<p> </p>\r\n', 1, NULL, '2021-02-11 11:28:33', NULL, '2021-02-11 11:28:33');

-- --------------------------------------------------------

--
-- Table structure for table `sample_booking_details`
--

CREATE TABLE `sample_booking_details` (
  `id` bigint(20) NOT NULL,
  `id_test_booking` varchar(2048) DEFAULT '',
  `id_sample` int(20) DEFAULT 0,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sample_booking_details`
--

INSERT INTO `sample_booking_details` (`id`, `id_test_booking`, `id_sample`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(3, NULL, 1, 1, NULL, '2021-01-26 00:35:57', NULL, '2021-01-26 00:35:57'),
(4, '6', 1, 1, NULL, '2021-01-26 00:37:20', NULL, '2021-01-26 00:37:20'),
(5, '7', 1, 1, NULL, '2021-01-26 01:05:40', NULL, '2021-01-26 01:05:40'),
(6, '8', 1, 1, NULL, '2021-01-26 13:50:26', NULL, '2021-01-26 13:50:26'),
(7, '9', 2, 1, NULL, '2021-02-03 11:37:15', NULL, '2021-02-03 11:37:15'),
(8, '9', 1, 1, NULL, '2021-02-03 11:37:23', NULL, '2021-02-03 11:37:23'),
(9, '10', 2, 1, NULL, '2021-02-08 13:31:56', NULL, '2021-02-08 13:31:56'),
(10, '10', 1, 1, NULL, '2021-02-08 13:31:58', NULL, '2021-02-08 13:31:58'),
(11, '11', 3, 1, NULL, '2021-02-11 11:32:18', NULL, '2021-02-11 11:32:18'),
(12, '11', 2, 1, NULL, '2021-02-11 11:32:20', NULL, '2021-02-11 11:32:20');

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

CREATE TABLE `staff` (
  `id` int(20) NOT NULL,
  `salutation` varchar(20) DEFAULT '',
  `name` varchar(520) DEFAULT '',
  `first_name` varchar(2048) DEFAULT '',
  `last_name` varchar(2048) DEFAULT '',
  `ic_no` varchar(50) DEFAULT '',
  `dob` date DEFAULT NULL,
  `mobile_number` varchar(20) DEFAULT '',
  `phone_number` varchar(20) DEFAULT '',
  `id_country` int(20) DEFAULT NULL,
  `id_state` int(20) DEFAULT NULL,
  `zipcode` int(10) DEFAULT NULL,
  `gender` varchar(20) DEFAULT '',
  `staff_id` varchar(50) DEFAULT '',
  `email` varchar(180) DEFAULT '',
  `address` varchar(520) DEFAULT '',
  `address_two` varchar(520) DEFAULT '',
  `job_type` varchar(20) DEFAULT '',
  `academic_type` varchar(20) DEFAULT '',
  `id_department` int(10) DEFAULT NULL,
  `id_faculty_program` int(20) DEFAULT 0 COMMENT 'as similar to department',
  `id_education_level` int(20) DEFAULT 0,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `staff`
--

INSERT INTO `staff` (`id`, `salutation`, `name`, `first_name`, `last_name`, `ic_no`, `dob`, `mobile_number`, `phone_number`, `id_country`, `id_state`, `zipcode`, `gender`, `staff_id`, `email`, `address`, `address_two`, `job_type`, `academic_type`, `id_department`, `id_faculty_program`, `id_education_level`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, '6', 'Ir. VINAY P', 'VINAY', 'P', '', '2019-09-09', '67865467', '0123123123', 1, 25, 12345, 'Male', 'DAS', 'vinaykiranp888@gmail.com', 'KL', 'KL', '0', '', 4, 0, 0, 1, NULL, '2021-02-11 18:12:59', NULL, '2021-02-11 18:12:59'),
(2, '6', 'Ir. VINAY P', 'VINAY', 'P', '', '2019-09-09', '67865467', '0123123123', 1, 25, 12345, 'Male', 'DAS', 'vinaykiranp888@gmail.com', 'KL', 'KL', '0', '', 4, 0, 0, 1, NULL, '2021-02-11 18:13:35', NULL, '2021-02-11 18:13:35'),
(3, '6', 'Ir. VINAY P', 'VINAY', 'P', '', '2019-09-09', '67865467', '0123123123', 1, 25, 12345, 'Male', 'DAS', 'vinaykiranp888@gmail.com', 'KL', 'KL', '0', '', 4, 0, 0, 1, NULL, '2021-02-11 18:13:45', NULL, '2021-02-11 18:13:45');

-- --------------------------------------------------------

--
-- Table structure for table `staff_bank_details`
--

CREATE TABLE `staff_bank_details` (
  `id` int(20) NOT NULL,
  `id_staff` int(20) DEFAULT 0,
  `id_bank` int(20) DEFAULT 0,
  `bank_account_name` varchar(500) DEFAULT '',
  `bank_code` varchar(500) DEFAULT '',
  `bank_account_number` varchar(500) DEFAULT '',
  `bank_address` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `staff_bank_details`
--

INSERT INTO `staff_bank_details` (`id`, `id_staff`, `id_bank`, `bank_account_name`, `bank_code`, `bank_account_number`, `bank_address`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 2, 1, 'Saving', 'CIM-KL', '1009284371783', 'Jalan no 9', 1, 1, '2020-12-09 17:50:17', NULL, '2020-12-09 17:50:17');

-- --------------------------------------------------------

--
-- Table structure for table `staff_change_status_details`
--

CREATE TABLE `staff_change_status_details` (
  `id` int(20) NOT NULL,
  `id_staff` int(20) DEFAULT 0,
  `id_change_status` int(20) DEFAULT 0,
  `from_dt` varchar(200) DEFAULT '0',
  `to_dt` varchar(200) DEFAULT '0',
  `reason` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `staff_change_status_details`
--

INSERT INTO `staff_change_status_details` (`id`, `id_staff`, `id_change_status`, `from_dt`, `to_dt`, `reason`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 2, 1, '0', '0', '', 1, 1, '2020-12-08 08:30:47', NULL, '2020-12-08 08:30:47'),
(2, 2, 5, '0', '0', '', 1, 1, '2020-12-08 08:30:58', NULL, '2020-12-08 08:30:58'),
(3, 3, 7, '2020-12-11', '2020-12-11', 'wertyuytrew', 1, 1, '2020-12-10 20:06:12', NULL, '2020-12-10 20:06:12');

-- --------------------------------------------------------

--
-- Table structure for table `staff_has_course`
--

CREATE TABLE `staff_has_course` (
  `id` int(20) NOT NULL,
  `id_staff` int(10) DEFAULT NULL,
  `id_course` int(10) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `staff_has_course`
--

INSERT INTO `staff_has_course` (`id`, `id_staff`, `id_course`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 1, NULL, NULL, '2020-07-10 23:42:07', NULL, '2020-07-10 23:42:07'),
(2, 1, 3, NULL, NULL, '2020-07-10 23:42:07', NULL, '2020-07-10 23:42:07'),
(3, 1, 0, NULL, NULL, '2020-07-13 09:04:43', NULL, '2020-07-13 09:04:43'),
(4, 1, 0, NULL, NULL, '2020-07-13 09:06:03', NULL, '2020-07-13 09:06:03'),
(5, 1, 0, NULL, NULL, '2020-07-13 09:06:51', NULL, '2020-07-13 09:06:51'),
(6, 1, 0, NULL, NULL, '2020-07-13 09:07:01', NULL, '2020-07-13 09:07:01'),
(7, 1, 0, NULL, NULL, '2020-07-13 10:02:31', NULL, '2020-07-13 10:02:31'),
(8, 1, 0, NULL, NULL, '2020-07-14 22:37:29', NULL, '2020-07-14 22:37:29'),
(9, 2, 0, NULL, NULL, '2020-08-09 19:43:19', NULL, '2020-08-09 19:43:19'),
(10, 2, 0, NULL, NULL, '2020-08-09 23:46:46', NULL, '2020-08-09 23:46:46'),
(11, 2, 0, NULL, NULL, '2020-08-10 00:25:09', NULL, '2020-08-10 00:25:09'),
(12, 1, 0, NULL, NULL, '2020-08-10 00:25:51', NULL, '2020-08-10 00:25:51'),
(13, 2, 0, NULL, NULL, '2020-08-10 01:42:09', NULL, '2020-08-10 01:42:09'),
(14, 2, 0, NULL, NULL, '2020-08-10 06:08:49', NULL, '2020-08-10 06:08:49'),
(15, 2, 0, NULL, NULL, '2020-08-10 06:09:25', NULL, '2020-08-10 06:09:25'),
(17, 3, 10, NULL, NULL, '2020-11-16 04:21:51', NULL, '2020-11-16 04:21:51'),
(18, 2, 2, NULL, NULL, '2020-12-08 06:13:04', NULL, '2020-12-08 06:13:04'),
(19, 4, 6, NULL, NULL, '2020-12-22 22:54:38', NULL, '2020-12-22 22:54:38');

-- --------------------------------------------------------

--
-- Table structure for table `staff_leave_records`
--

CREATE TABLE `staff_leave_records` (
  `id` int(20) NOT NULL,
  `id_staff` int(20) DEFAULT 0,
  `name` varchar(2048) DEFAULT '',
  `from_dt` varchar(200) DEFAULT '',
  `to_dt` varchar(200) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `staff_leave_records`
--

INSERT INTO `staff_leave_records` (`id`, `id_staff`, `name`, `from_dt`, `to_dt`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 2, 'Sabbatical', '2020-12-01', '2020-12-16', 1, 1, '2020-12-08 08:06:50', NULL, '2020-12-08 08:06:50');

-- --------------------------------------------------------

--
-- Table structure for table `staff_status`
--

CREATE TABLE `staff_status` (
  `id` int(20) NOT NULL,
  `code` varchar(2048) DEFAULT '',
  `name` varchar(2048) DEFAULT '',
  `name_optional_language` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `staff_status`
--

INSERT INTO `staff_status` (`id`, `code`, `name`, `name_optional_language`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, '', 'Active', '', 1, 1, '2020-12-08 06:10:37', NULL, '2020-12-08 06:10:37'),
(2, '', 'Inactive', '', 1, 1, '2020-12-08 06:10:37', NULL, '2020-12-08 06:10:37'),
(3, '', 'Terminated', '', 1, 1, '2020-12-08 06:10:37', NULL, '2020-12-08 06:10:37'),
(4, '', 'Suspended', '', 1, 1, '2020-12-08 06:10:37', NULL, '2020-12-08 06:10:37'),
(5, '', 'Deceased', '', 1, 1, '2020-12-08 06:10:37', NULL, '2020-12-08 06:10:37'),
(6, '', 'Quit', '', 1, 1, '2020-12-08 06:10:37', NULL, '2020-12-08 06:10:37'),
(7, '', 'Sabbatical Leave', '', 1, 1, '2020-12-08 06:10:37', NULL, '2020-12-08 06:10:37'),
(8, '', 'Long MC', '', 1, 1, '2020-12-08 06:10:37', NULL, '2020-12-08 06:10:37'),
(9, '', 'Maternity', '', 1, 1, '2020-12-08 06:10:37', NULL, '2020-12-08 06:10:37');

-- --------------------------------------------------------

--
-- Table structure for table `staff_teaching_details`
--

CREATE TABLE `staff_teaching_details` (
  `id` int(20) NOT NULL,
  `id_staff` int(20) DEFAULT 0,
  `id_semester` int(20) DEFAULT 0,
  `id_programme` int(20) DEFAULT 0,
  `id_course` int(20) DEFAULT 0,
  `id_mode_of_study` int(20) DEFAULT 0,
  `id_learning_center` int(20) DEFAULT 0,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `staff_teaching_details`
--

INSERT INTO `staff_teaching_details` (`id`, `id_staff`, `id_semester`, `id_programme`, `id_course`, `id_mode_of_study`, `id_learning_center`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 2, 4, 4, 6, 15, 4, 1, 1, '2020-12-08 08:06:37', NULL, '2020-12-08 08:06:37');

-- --------------------------------------------------------

--
-- Table structure for table `stage_semester`
--

CREATE TABLE `stage_semester` (
  `id` int(20) NOT NULL,
  `id_stage` int(20) DEFAULT 0,
  `name` varchar(2048) DEFAULT '',
  `type` varchar(2048) DEFAULT '',
  `from` int(20) DEFAULT 0,
  `to` int(20) DEFAULT 0,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stage_semester`
--

INSERT INTO `stage_semester` (`id`, `id_stage`, `name`, `type`, `from`, `to`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 'Semester 1 - 3', 'Range', 1, 3, 1, 1, '2020-10-12 21:38:04', NULL, '2020-10-12 21:38:04'),
(6, 2, 'Semester 4 - 7', 'Range', 4, 7, 1, 1, '2020-10-13 23:10:32', NULL, '2020-10-13 23:10:32'),
(7, 3, 'Semester 8 - 11', 'Range', 8, 11, 1, 1, '2020-10-13 23:10:55', NULL, '2020-10-13 23:10:55'),
(8, 4, 'Semester 11 - 12', 'Range', 11, 12, 1, 1, '2020-10-13 23:11:12', NULL, '2020-10-13 23:11:12'),
(9, 5, 'Semester 1 - 2', 'Range', 1, 2, 1, 1, '2020-10-13 23:15:48', NULL, '2020-10-13 23:15:48'),
(10, 6, 'Semester 3 - 5', 'Range', 3, 5, 1, 1, '2020-10-13 23:16:05', NULL, '2020-10-13 23:16:05'),
(11, 7, 'Semester 6 - 7', 'Range', 6, 7, 1, 1, '2020-10-13 23:16:22', NULL, '2020-10-13 23:16:22'),
(12, 8, 'Semester 8 - 9', 'Range', 8, 9, 1, 1, '2020-10-13 23:16:46', NULL, '2020-10-13 23:16:46');

-- --------------------------------------------------------

--
-- Table structure for table `state`
--

CREATE TABLE `state` (
  `id` int(20) NOT NULL,
  `name` varchar(120) DEFAULT '',
  `id_country` int(10) DEFAULT NULL,
  `status` int(20) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT NULL,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `state`
--

INSERT INTO `state` (`id`, `name`, `id_country`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Kedah', 1, 1, NULL, NULL, NULL, '2020-07-10 23:22:53'),
(2, 'Perlis', 1, 1, NULL, NULL, NULL, '2020-07-10 23:23:18'),
(3, 'Penang', 1, 1, NULL, NULL, NULL, '2020-07-10 23:23:41'),
(4, 'Perak', 1, 1, NULL, NULL, NULL, '2020-08-27 13:05:28'),
(5, 'Selangor', 1, 1, NULL, NULL, NULL, '2020-08-27 13:05:39'),
(6, 'Wilayah Persekutuan', 1, 1, NULL, NULL, NULL, '2020-08-27 13:05:58'),
(7, 'Melaka', 1, 1, NULL, NULL, NULL, '2020-08-27 13:06:12'),
(8, 'Negeri Sembilan', 1, 1, NULL, NULL, NULL, '2020-08-27 13:06:30'),
(9, 'Johor', 1, 1, NULL, NULL, NULL, '2020-08-27 13:06:43'),
(10, 'Pahang', 1, 1, NULL, NULL, NULL, '2020-08-27 13:06:53'),
(11, 'Terengganu', 1, 1, NULL, NULL, NULL, '2020-08-27 13:07:08'),
(12, 'Kelantan', 1, 1, NULL, NULL, NULL, '2020-08-27 13:07:20'),
(13, 'Terengganu', 1, 1, NULL, NULL, NULL, '2020-08-27 13:07:32'),
(14, 'Labuan', 1, 1, NULL, NULL, NULL, '2020-08-27 13:07:53'),
(15, 'Province 1', 1, 1, NULL, NULL, NULL, '2020-09-01 19:07:09'),
(16, 'Sri Lanka', 1, 1, NULL, NULL, NULL, '2020-09-01 19:33:57'),
(17, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(18, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(19, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(20, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(21, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(22, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(23, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(24, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(25, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(26, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(27, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(28, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(29, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(30, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(31, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(32, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(33, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(34, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(35, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(36, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(37, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(38, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(39, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(40, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(41, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(42, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(43, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(44, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(45, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(46, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(47, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(48, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(49, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(50, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(51, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(52, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(53, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(54, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(55, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(56, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(57, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(58, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(59, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(60, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(61, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(62, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(63, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(64, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(65, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(66, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(67, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(68, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(69, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(70, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(71, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(72, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(73, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(74, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(75, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(76, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(77, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(78, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(79, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(80, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(81, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(82, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(83, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(84, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(85, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(86, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(87, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(88, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(89, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(90, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(91, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(92, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(93, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(94, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(95, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(96, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(97, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(98, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(99, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(100, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(101, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(102, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(103, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(104, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(105, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(106, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(107, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(108, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(109, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(110, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(111, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(112, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(113, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(114, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(115, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(116, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(117, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(118, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(119, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(120, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(121, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(122, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(123, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(124, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(125, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(126, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(127, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(128, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(129, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(130, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(131, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(132, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(133, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(134, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(135, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(136, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(137, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(138, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(139, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(140, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(141, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(142, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(143, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(144, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(145, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(146, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(147, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(148, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(149, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(150, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(151, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(152, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(153, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(154, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(155, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(156, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(157, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(158, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(159, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(160, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(161, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(162, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(163, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(164, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(165, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(166, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(167, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(168, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(169, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(170, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(171, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(172, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(173, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(174, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(175, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(176, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(177, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(178, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(179, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(180, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(181, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(182, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(183, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(184, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(185, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(186, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(187, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(188, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(189, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(190, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(191, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(192, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(193, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(194, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(195, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(196, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(197, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(198, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(199, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(200, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(201, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(202, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(203, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(204, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(205, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(206, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(207, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(208, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(209, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(210, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(211, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(212, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(213, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(214, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(215, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(216, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(217, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(218, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(219, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(220, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(221, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(222, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42');

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `id` int(20) NOT NULL,
  `whatsapp_number` varchar(50) DEFAULT NULL,
  `linked_in` varchar(50) DEFAULT NULL,
  `facebook_id` varchar(50) DEFAULT NULL,
  `twitter_id` varchar(50) DEFAULT NULL,
  `ig_id` varchar(50) DEFAULT NULL,
  `profile_pic` varchar(1024) DEFAULT 'default_profile.jpg',
  `step6_status` int(10) DEFAULT NULL,
  `step5_status` int(10) DEFAULT NULL,
  `step4_status` int(10) DEFAULT NULL,
  `step3_status` int(10) DEFAULT NULL,
  `step2_status` int(10) DEFAULT NULL,
  `step1_status` int(10) DEFAULT NULL,
  `step6_comments` text DEFAULT NULL,
  `step5_comments` text DEFAULT NULL,
  `step4_comments` text DEFAULT NULL,
  `step3_comments` text DEFAULT NULL,
  `step2_comments` text DEFAULT NULL,
  `step1_comments` text DEFAULT NULL,
  `id_intake` int(11) DEFAULT NULL,
  `id_program` int(11) DEFAULT NULL,
  `full_name` varchar(580) DEFAULT '',
  `salutation` varchar(120) DEFAULT '',
  `first_name` varchar(120) DEFAULT '',
  `last_name` varchar(120) DEFAULT '',
  `matrix_number` varchar(200) DEFAULT '',
  `nric` varchar(120) DEFAULT '',
  `passport` varchar(120) DEFAULT '',
  `phone` varchar(120) DEFAULT '',
  `email_id` varchar(120) DEFAULT '',
  `contact_email` varchar(500) DEFAULT '',
  `password` varchar(120) DEFAULT '',
  `id_type` varchar(50) DEFAULT '',
  `country_code` varchar(50) DEFAULT NULL,
  `passport_expiry_date` varchar(120) DEFAULT '',
  `gender` varchar(120) DEFAULT '',
  `date_of_birth` varchar(120) DEFAULT '',
  `martial_status` varchar(120) DEFAULT '',
  `religion` varchar(120) DEFAULT '',
  `nationality` varchar(120) DEFAULT '',
  `is_hostel` int(20) DEFAULT 0,
  `id_degree_type` int(20) DEFAULT 0,
  `id_race` varchar(120) DEFAULT '',
  `id_advisor` int(20) DEFAULT 0,
  `id_phd_advisor` int(20) DEFAULT 0,
  `id_supervisor` int(20) DEFAULT 0,
  `mail_address1` varchar(120) DEFAULT '',
  `mail_address2` varchar(120) DEFAULT '',
  `mailing_country` int(120) DEFAULT NULL,
  `mailing_state` int(120) DEFAULT NULL,
  `mailing_city` varchar(120) DEFAULT '',
  `program_scheme` varchar(200) DEFAULT '',
  `id_program_scheme` int(20) DEFAULT 0 COMMENT 'it''s id_learning_mode after flow change',
  `id_program_structure_type` int(20) DEFAULT 0,
  `id_program_has_scheme` int(20) DEFAULT 0 COMMENT 'it''s the actual id_scheme for the program',
  `id_fee_structure` int(20) DEFAULT 0,
  `portfolio_result` int(10) DEFAULT NULL,
  `portfolio_grade` varchar(100) DEFAULT NULL,
  `challenged_test_status` int(10) DEFAULT NULL,
  `challenged_test_grade` varchar(200) DEFAULT NULL,
  `interview_result` int(10) DEFAULT NULL,
  `interview_grade` varchar(200) DEFAULT NULL,
  `id_program_landscape` int(20) DEFAULT 0,
  `id_program_requirement` int(20) DEFAULT 0,
  `id_university` int(20) DEFAULT 0,
  `id_branch` int(20) DEFAULT 0,
  `phd_duration` int(20) DEFAULT 0,
  `current_semester` int(20) DEFAULT 0,
  `current_deliverable` varchar(200) DEFAULT '',
  `mailing_zipcode` varchar(120) DEFAULT '',
  `present_address_same_as_mailing_address` int(20) DEFAULT 0,
  `permanent_address1` varchar(120) DEFAULT '',
  `permanent_address2` varchar(120) DEFAULT '',
  `permanent_country` int(120) DEFAULT NULL,
  `permanent_state` int(120) DEFAULT NULL,
  `permanent_city` varchar(120) DEFAULT '',
  `permanent_zipcode` varchar(120) DEFAULT '',
  `sibbling_discount` varchar(50) DEFAULT '',
  `employee_discount` varchar(50) DEFAULT '',
  `alumni_discount` varchar(50) DEFAULT '',
  `applicant_status` varchar(50) DEFAULT 'Draft',
  `passport_number` varchar(50) DEFAULT '',
  `id_applicant` int(20) DEFAULT 0,
  `pathway` varchar(200) DEFAULT '',
  `added_by_partner_university` int(2) DEFAULT 0,
  `status` int(20) DEFAULT NULL,
  `accept_date` date DEFAULT NULL,
  `reason` varchar(2048) DEFAULT '',
  `approved_by` int(20) DEFAULT 0,
  `approved_dt_tm` datetime DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp(),
  `applicant_comments` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`id`, `whatsapp_number`, `linked_in`, `facebook_id`, `twitter_id`, `ig_id`, `profile_pic`, `step6_status`, `step5_status`, `step4_status`, `step3_status`, `step2_status`, `step1_status`, `step6_comments`, `step5_comments`, `step4_comments`, `step3_comments`, `step2_comments`, `step1_comments`, `id_intake`, `id_program`, `full_name`, `salutation`, `first_name`, `last_name`, `matrix_number`, `nric`, `passport`, `phone`, `email_id`, `contact_email`, `password`, `id_type`, `country_code`, `passport_expiry_date`, `gender`, `date_of_birth`, `martial_status`, `religion`, `nationality`, `is_hostel`, `id_degree_type`, `id_race`, `id_advisor`, `id_phd_advisor`, `id_supervisor`, `mail_address1`, `mail_address2`, `mailing_country`, `mailing_state`, `mailing_city`, `program_scheme`, `id_program_scheme`, `id_program_structure_type`, `id_program_has_scheme`, `id_fee_structure`, `portfolio_result`, `portfolio_grade`, `challenged_test_status`, `challenged_test_grade`, `interview_result`, `interview_grade`, `id_program_landscape`, `id_program_requirement`, `id_university`, `id_branch`, `phd_duration`, `current_semester`, `current_deliverable`, `mailing_zipcode`, `present_address_same_as_mailing_address`, `permanent_address1`, `permanent_address2`, `permanent_country`, `permanent_state`, `permanent_city`, `permanent_zipcode`, `sibbling_discount`, `employee_discount`, `alumni_discount`, `applicant_status`, `passport_number`, `id_applicant`, `pathway`, `added_by_partner_university`, `status`, `accept_date`, `reason`, `approved_by`, `approved_dt_tm`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`, `applicant_comments`) VALUES
(74, NULL, NULL, NULL, NULL, NULL, 'default_profile.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 10, 'Mr. Developper Testing One', '1', 'Developper', 'Testing One', '', '80001', '', '80001', 'dt1@eag.com', '', '202cb962ac59075b964b07152d234b70', '', NULL, '', 'Male', '2021-01-16', 'Single', '2', 'Malaysian', 1, 2, '5', 0, 0, 5, 'No 1 , KL ', 'KL', 110, 1, 'KL', 'Online - Part Time', 29, 1, 1, 23, NULL, NULL, NULL, NULL, NULL, NULL, 34, 1, 9, 9, 1, 1, 'Jan-2021', '12341231', 1, ' No 1, KL', 'KL', 110, 1, 'KL', '12312', '', 'Yes', '', 'Approved', '', 116, 'APEL', 0, 1, NULL, '', 0, '2021-01-13 00:00:00', NULL, '2021-01-13 03:24:52', 1, '2021-01-12 21:42:23', NULL),
(75, NULL, NULL, NULL, NULL, NULL, 'default_profile.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 10, 'Mr. Ezz Ali', '1', 'Ezz', 'Ali', '', '123123', '', '1139646066', 'ezzittech@gmail.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'PASSPORT', NULL, '', 'Male', '1991-01-01', 'Single', '1', '4', 1, 2, '', 0, 0, 0, 'Jalan Ampang', '', 110, 6, 'Ampang', 'Online - Part Time', 29, 1, 3, 25, NULL, NULL, NULL, NULL, NULL, NULL, 34, 0, 9, 9, 1, 1, 'Jan-2021', '50450', 1, 'Jalan Ampang', '', 110, 6, 'Ampang', '50450', '', '', '', 'Approved', '', 127, 'DIRECT ENTRY', 0, 1, NULL, '', 0, '2021-01-15 00:00:00', NULL, '2021-01-15 01:37:55', NULL, '2021-01-15 00:03:46', NULL),
(76, NULL, NULL, NULL, NULL, NULL, 'default_profile.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 10, 'Mr. Salam K.', '1', 'Salam', 'K.', '', '09778866', '', '0182343385', 'eng.abdulsalam@yahoo.com', '', 'e10adc3949ba59abbe56e057f20f883e', 'PASSPORT', NULL, '', 'Male', '2000-01-01', 'Married', '1', '1', 1, 2, '1', 0, 0, 0, 'C-7-7, Megan Phileo Promenade Block C, Hampshire Park', '', 110, 6, 'Kuala Lumpur', 'Online - Full Time', 31, 1, 1, 23, NULL, NULL, NULL, NULL, NULL, NULL, 34, 0, 9, 9, 1, 1, 'Jan-2021', '50450', 1, 'C-7-7, Megan Phileo Promenade Block C, Hampshire Park', '', 110, 6, 'Kuala Lumpur', '50450', '', '', '', 'Approved', '', 128, 'DIRECT ENTRY', 0, 1, NULL, '', 0, '2021-01-15 00:00:00', NULL, '2021-01-15 01:39:47', NULL, '2021-01-15 00:29:11', NULL),
(78, '999999999', 'www.vk88888.com', 'www.vk88888.com', 'www.vk88888.com', 'www.vk88888.com', 'default_profile.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 10, 'Mr. Developper Testing Five', '1', 'Developper', 'Testing Five', '', '80005', '', '8000000005', 'dt5@eag.com', '', '202cb962ac59075b964b07152d234b70', 'NRIC', NULL, '', 'Male', '2021-01-13', 'Single', '4', '1', 1, 2, '3', 0, 0, 0, 'KL', 'KL', 110, 1, 'KL', 'Online - Part Time', 29, 1, 1, 25, NULL, NULL, NULL, NULL, NULL, NULL, 34, 0, 9, 9, 1, 1, 'Jan-2021', '12345', 1, 'KL', 'KL', 110, 1, 'KL', '12345', '', '', '', 'Approved', '', 130, 'DIRECT ENTRY', 0, 1, NULL, '', 0, '2021-01-16 00:00:00', NULL, '2021-01-15 10:58:57', NULL, '2021-01-15 10:38:12', NULL),
(79, '', '', '', '', '', '93f27d23afd21ee4f7a5d6a8b8794984.jpg', 1, 1, 1, 1, 1, 0, '', '', '', 'verified', '', '', 3, 10, 'Mr. MM 01', '1', 'MM', '01', '', 'LM05789', '', '0101010101', 'mm01@g.com', '', '9eb5a09ebc3e0b9cff10e49b7fdbd399', 'PASSPORT', NULL, '', 'Male', '1942-01-01', '', '7', '9', 1, 2, '', 0, 0, 0, 'No. 007', 'Building - Saturn', 1, 9, 'Johor', 'Online - Part Time', 29, 1, 3, 25, 0, '', 0, '', 0, '', 34, 0, 9, 9, 1, 1, 'Jan-2021', '75000', 1, 'No. 007', 'Building - Saturn', 1, 9, 'Johor', '75000', '', '', '', 'Approved', '', 137, 'DIRECT ENTRY', 0, 1, NULL, '', 0, NULL, NULL, '2021-01-17 23:23:23', NULL, '2021-01-17 20:06:26', 'Approved');

-- --------------------------------------------------------

--
-- Table structure for table `student_bca`
--

CREATE TABLE `student_bca` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `city` varchar(200) NOT NULL,
  `email` varchar(100) NOT NULL,
  `address` text NOT NULL,
  `aadhar` int(12) NOT NULL,
  `application_date` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_bca`
--

INSERT INTO `student_bca` (`id`, `name`, `city`, `email`, `address`, `aadhar`, `application_date`) VALUES
(7, 'TEsting', 'Tumkur', 'email@a.com', 'asdf', 1234, '2021-06-05 06:03:39'),
(8, 'TEsting2', 'Mysore', '1email@a.com', 'asdf', 12345, '2021-06-05 06:03:39'),
(9, 'q', 'a', 'email@a.com', 'a', 34555, '2021-06-09 06:04:07');

-- --------------------------------------------------------

--
-- Table structure for table `student_deliverable_history`
--

CREATE TABLE `student_deliverable_history` (
  `id` int(20) NOT NULL,
  `id_student` int(10) DEFAULT NULL,
  `old_deliverable_term` varchar(200) DEFAULT '',
  `new_deliverable_term` varchar(200) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT 0,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT 0,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_deliverable_history`
--

INSERT INTO `student_deliverable_history` (`id`, `id_student`, `old_deliverable_term`, `new_deliverable_term`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 28, '', 'Dec-2020', 1, 1, '2020-10-27 06:48:59', 0, '2020-10-27 06:48:59'),
(2, 30, '', 'Dec-2020', 1, 1, '2020-10-27 07:28:41', 0, '2020-10-27 07:28:41'),
(3, 31, '', 'Nov-2020', 1, 1, '2020-11-08 11:41:01', 0, '2020-11-08 11:41:01'),
(4, 32, '', 'Nov-2020', 1, 1, '2020-11-09 12:13:40', 0, '2020-11-09 12:13:40'),
(5, 33, '', 'Nov-2020', 1, 1, '2020-11-09 12:13:56', 0, '2020-11-09 12:13:56'),
(6, 34, '', 'Nov-2020', 1, 1, '2020-11-09 20:50:13', 0, '2020-11-09 20:50:13'),
(7, 35, '', 'Nov-2020', 1, 1, '2020-11-10 19:57:42', 0, '2020-11-10 19:57:42'),
(8, 36, '', 'Nov-2020', 1, 1, '2020-11-15 10:28:35', 0, '2020-11-15 10:28:35'),
(9, 37, '', 'Dec-2020', 1, 1, '2020-12-07 11:29:27', 0, '2020-12-07 11:29:27'),
(10, 38, '', 'Dec-2020', 1, 1, '2020-12-07 11:39:47', 0, '2020-12-07 11:39:47'),
(11, 39, '', 'Dec-2020', 1, 1, '2020-12-08 08:25:25', 0, '2020-12-08 08:25:25'),
(12, 40, '', 'Dec-2020', 1, 1, '2020-12-08 09:05:04', 0, '2020-12-08 09:05:04'),
(13, 41, '', 'Dec-2020', 1, 1, '2020-12-08 09:07:53', 0, '2020-12-08 09:07:53'),
(14, 42, '', 'Dec-2020', 1, 1, '2020-12-09 01:49:19', 0, '2020-12-09 01:49:19'),
(15, 43, '', 'Dec-2020', 1, 1, '2020-12-09 01:49:25', 0, '2020-12-09 01:49:25'),
(16, 44, '', 'Dec-2020', 1, 1, '2020-12-09 21:02:20', 0, '2020-12-09 21:02:20'),
(17, 45, '', 'Dec-2020', 1, 1, '2020-12-10 01:13:23', 0, '2020-12-10 01:13:23'),
(18, 46, '', 'Dec-2020', 1, 1, '2020-12-12 10:51:30', 0, '2020-12-12 10:51:30'),
(19, 47, '', 'Dec-2020', 1, NULL, '2020-12-12 11:07:42', 0, '2020-12-12 11:07:42'),
(20, 48, '', 'Dec-2020', 1, 1, '2020-12-12 11:43:36', 0, '2020-12-12 11:43:36'),
(21, 49, '', 'Dec-2020', 1, 1, '2020-12-12 12:00:50', 0, '2020-12-12 12:00:50'),
(22, 50, '', 'Dec-2020', 1, 1, '2020-12-15 10:20:43', 0, '2020-12-15 10:20:43'),
(23, 51, '', 'Dec-2020', 1, 1, '2020-12-15 10:42:27', 0, '2020-12-15 10:42:27'),
(24, 52, '', 'Dec-2020', 1, NULL, '2020-12-17 02:29:22', 0, '2020-12-17 02:29:22'),
(25, 53, '', 'Dec-2020', 1, NULL, '2020-12-17 02:40:26', 0, '2020-12-17 02:40:26'),
(26, 54, '', 'Dec-2020', 1, 1, '2020-12-18 13:10:29', 0, '2020-12-18 13:10:29'),
(27, 55, '', 'Dec-2020', 1, 1, '2020-12-18 13:20:31', 0, '2020-12-18 13:20:31'),
(28, 56, '', 'Dec-2020', 1, NULL, '2020-12-19 14:36:11', 0, '2020-12-19 14:36:11'),
(29, 57, '', 'Dec-2020', 1, 1, '2020-12-19 14:36:31', 0, '2020-12-19 14:36:31'),
(30, 58, '', 'Dec-2020', 1, 1, '2020-12-20 14:10:33', 0, '2020-12-20 14:10:33'),
(31, 59, '', 'Dec-2020', 1, NULL, '2020-12-20 14:10:54', 0, '2020-12-20 14:10:54'),
(32, 60, '', 'Dec-2020', 1, 1, '2020-12-20 14:54:18', 0, '2020-12-20 14:54:18'),
(33, 61, '', 'Dec-2020', 1, 1, '2020-12-23 00:12:43', 0, '2020-12-23 00:12:43'),
(34, 62, '', 'Dec-2020', 1, 1, '2020-12-23 08:59:02', 0, '2020-12-23 08:59:02'),
(35, 63, '', 'Dec-2020', 1, 1, '2020-12-23 08:59:15', 0, '2020-12-23 08:59:15'),
(36, 64, '', 'Dec-2020', 1, NULL, '2020-12-23 11:59:14', 0, '2020-12-23 11:59:14'),
(37, 65, '', 'Dec-2020', 1, 1, '2020-12-23 11:59:30', 0, '2020-12-23 11:59:30'),
(38, 66, '', 'Dec-2020', 1, 1, '2020-12-23 12:11:51', 0, '2020-12-23 12:11:51'),
(39, 67, '', 'Dec-2020', 1, NULL, '2020-12-23 12:31:11', 0, '2020-12-23 12:31:11'),
(40, 68, '', 'Dec-2020', 1, 1, '2020-12-24 05:07:22', 0, '2020-12-24 05:07:22'),
(41, 69, '', 'Dec-2020', 1, 1, '2020-12-24 08:20:13', 0, '2020-12-24 08:20:13'),
(42, 70, '', 'Dec-2020', 1, 1, '2020-12-24 08:27:51', 0, '2020-12-24 08:27:51'),
(43, 71, '', 'Dec-2020', 1, 1, '2020-12-25 12:04:52', 0, '2020-12-25 12:04:52'),
(44, 72, '', 'Dec-2020', 1, NULL, '2020-12-25 12:07:39', 0, '2020-12-25 12:07:39'),
(45, 73, '', 'Dec-2020', 1, NULL, '2020-12-25 12:10:18', 0, '2020-12-25 12:10:18'),
(46, 74, '', 'Jan-2021', 1, NULL, '2021-01-13 03:24:52', 0, '2021-01-13 03:24:52'),
(47, 75, '', 'Jan-2021', 1, 1, '2021-01-15 01:37:55', 0, '2021-01-15 01:37:55'),
(48, 76, '', 'Jan-2021', 1, 1, '2021-01-15 01:39:47', 0, '2021-01-15 01:39:47'),
(49, 77, '', 'Jan-2021', 1, NULL, '2021-01-15 10:55:56', 0, '2021-01-15 10:55:56'),
(50, 78, '', 'Jan-2021', 1, NULL, '2021-01-15 10:58:57', 0, '2021-01-15 10:58:57'),
(51, 79, '', 'Jan-2021', 1, NULL, '2021-01-17 23:23:23', 0, '2021-01-17 23:23:23');

-- --------------------------------------------------------

--
-- Table structure for table `student_duration_history`
--

CREATE TABLE `student_duration_history` (
  `id` int(20) NOT NULL,
  `id_student` int(10) DEFAULT NULL,
  `old_deliverable` varchar(200) DEFAULT '',
  `new_deliverable` varchar(200) DEFAULT '',
  `old_phd_duration` int(20) DEFAULT 0,
  `new_phd_duration` int(20) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `student_emergency_contact_details`
--

CREATE TABLE `student_emergency_contact_details` (
  `id` int(20) NOT NULL,
  `id_student` int(20) DEFAULT 0,
  `relationship` varchar(10240) DEFAULT '',
  `relative_name` varchar(10240) DEFAULT '',
  `relative_address` varchar(10240) DEFAULT '',
  `relative_mobile` varchar(20) DEFAULT '',
  `relative_home_phone` varchar(20) DEFAULT '',
  `relative_office_phone` varchar(20) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_emergency_contact_details`
--

INSERT INTO `student_emergency_contact_details` (`id`, `id_student`, `relationship`, `relative_name`, `relative_address`, `relative_mobile`, `relative_home_phone`, `relative_office_phone`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 28, 'Sister', 'Nmann', 'KL', '3231', '321313', '232131313', NULL, NULL, '2020-10-10 02:28:15', NULL, '2020-10-10 02:28:15'),
(2, 31, 'Father', 'Khaleel Ahmed', 'KL', '28893211', '2321313131', '123212121', NULL, NULL, '2020-11-08 11:46:50', NULL, '2020-11-08 11:46:50');

-- --------------------------------------------------------

--
-- Table structure for table `student_has_document`
--

CREATE TABLE `student_has_document` (
  `id` int(20) NOT NULL,
  `id_student` int(20) DEFAULT NULL,
  `id_document` varchar(520) DEFAULT '',
  `file_name` varchar(520) DEFAULT '',
  `file` varchar(500) DEFAULT '',
  `reason` varchar(500) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_has_document`
--

INSERT INTO `student_has_document` (`id`, `id_student`, `id_document`, `file_name`, `file`, `reason`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(4, 22, '2', 'road construction.png', '0d07bd613d0a4d15528bd12cdb51279c.png', '', NULL, NULL, '2020-09-12 11:34:49', NULL, '2020-09-12 11:34:49'),
(5, 22, '2', 'Temp Comforts.png', 'eca24c5667c93c292ad462ab9e0ddef8.png', '', NULL, NULL, '2020-09-12 11:35:30', NULL, '2020-09-12 11:35:30'),
(6, 25, '2', 'Screenshot from 2020-03-17 23-54-22.png', 'd1fe6c5e19f9df1d8b769f8a528c6020.png', '', NULL, NULL, '2020-09-15 04:33:19', NULL, '2020-09-15 04:33:19'),
(7, 25, '1', 'Screenshot from 2020-03-17 23-45-27.png', '5968fe81a285d01340f6db22f4c3afae.png', '', NULL, NULL, '2020-09-15 04:33:19', NULL, '2020-09-15 04:33:19');

-- --------------------------------------------------------

--
-- Table structure for table `student_insurance_details`
--

CREATE TABLE `student_insurance_details` (
  `id` int(20) NOT NULL,
  `id_student` int(20) DEFAULT 0,
  `insurance_id_visa` int(20) DEFAULT 0,
  `insurance_insurance_type` varchar(200) DEFAULT '',
  `insurance_reference_no` varchar(200) DEFAULT '',
  `insurance_date_issue` datetime DEFAULT NULL,
  `insurance_expiry_date` datetime DEFAULT NULL,
  `insurance_reminder_type` varchar(200) DEFAULT '',
  `insurance_reminder_months_expiry` int(20) DEFAULT 0,
  `insurance_reminder_template` varchar(200) DEFAULT '0',
  `insurance_reminder_remarks` varchar(2048) DEFAULT '0',
  `insurance_cover_letter` varchar(200) DEFAULT '0',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `student_last_login`
--

CREATE TABLE `student_last_login` (
  `id` bigint(20) NOT NULL,
  `id_student` bigint(20) NOT NULL,
  `session_data` varchar(2048) NOT NULL,
  `machine_ip` varchar(1024) NOT NULL,
  `user_agent` varchar(128) NOT NULL,
  `agent_string` varchar(1024) NOT NULL,
  `platform` varchar(128) NOT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `student_last_login`
--

INSERT INTO `student_last_login` (`id`, `id_student`, `session_data`, `machine_ip`, `user_agent`, `agent_string`, `platform`, `created_dt_tm`) VALUES
(1, 3, '{\"student_name\":\"Mr. Mohd Khairul\",\"email_id\":\"askiran123+3@gmail.com\",\"nric\":\"23424242324\",\"id_intake\":\"1\",\"id_program\":\"1\",\"id_qualification\":\"1\"}', '49.206.7.23', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-07-11 12:56:50'),
(2, 4, '{\"student_name\":\"Mr. Bhasha Zahir\",\"email_id\":\"bz@cms.com\",\"nric\":\"NR33445\",\"id_intake\":\"1\",\"id_program\":\"1\",\"id_qualification\":\"1\"}', '49.206.7.23', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-07-11 13:15:46'),
(3, 4, '{\"student_name\":\"Mr. Bhasha Zahir\",\"email_id\":\"bz@cms.com\",\"nric\":\"NR33445\",\"id_intake\":\"1\",\"id_program\":\"1\",\"id_qualification\":\"1\"}', '49.206.7.23', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-07-19 17:29:29'),
(4, 6, '{\"student_name\":\"Mr. Nabasha Ali\",\"email_id\":\"nab@cms.com\",\"nric\":\"5612345\",\"id_intake\":\"1\",\"id_program\":\"1\",\"id_qualification\":\"1\"}', '49.206.7.23', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-10 17:49:31'),
(5, 6, '{\"student_name\":\"Mr. Nabasha Ali\",\"email_id\":\"nab@cms.com\",\"nric\":\"5612345\",\"id_intake\":\"1\",\"id_program\":\"1\",\"id_qualification\":\"1\"}', '211.25.82.226', 'Chrome 84.0.4147.105', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36 Edg/84.0.522.52', 'Windows 10', '2020-08-10 20:03:00'),
(6, 17, '{\"student_name\":\"AG. Vasanthi Krishnan\",\"email_id\":\"vask@cms.com\",\"nric\":\"78031098778\",\"id_intake\":\"1\",\"id_program\":\"4\",\"id_qualification\":\"1\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-27 19:25:21'),
(7, 17, '{\"student_name\":\"AG. Vasanthi Krishnan\",\"email_id\":\"vask@cms.com\",\"nric\":\"78031098778\",\"id_intake\":\"1\",\"id_program\":\"4\",\"id_qualification\":\"1\"}', '42.190.31.233', 'Chrome 84.0.4147.135', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.135 Safari/537.36', 'Windows 10', '2020-08-27 20:10:26'),
(8, 13, '{\"student_name\":\"DATIN. Testing Two\",\"email_id\":\"t2@cms.com\",\"nric\":\"t2\",\"id_intake\":\"1\",\"id_program\":\"3\",\"id_qualification\":\"1\",\"id_program_scheme\":\"8\"}', '112.79.85.46', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '2020-08-30 14:02:58'),
(9, 16, '{\"student_name\":\"AG. Jury Tise\",\"email_id\":\"jt@cms.com\",\"nric\":\"NTJT88990\",\"id_intake\":\"1\",\"id_program\":\"3\",\"id_qualification\":\"1\",\"id_program_scheme\":\"11\"}', '157.45.63.59', 'Chrome 81.0.4044.111', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.111 Safari/537.36', 'Linux', '2020-09-02 19:20:32'),
(10, 14, '{\"student_name\":\"DATIN. Testing Four\",\"email_id\":\"t4@cms.com\",\"nric\":\"t4\",\"id_intake\":\"1\",\"id_program\":\"3\",\"id_qualification\":\"1\",\"id_program_scheme\":\"8\",\"isStudentAdminLoggedIn\":true}', '157.45.44.99', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '2020-09-08 06:34:32'),
(11, 18, '{\"student_name\":\"DATIN. Osomania Azarul\",\"email_id\":\"oa@cms.com\",\"nric\":\"NR213312\",\"id_intake\":\"1\",\"id_program\":\"3\",\"id_program_landscape\":null,\"id_qualification\":\"1\",\"id_program_scheme\":\"14\",\"isStudentAdminLoggedIn\":true}', '157.49.189.60', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '2020-09-10 14:36:57'),
(12, 18, '{\"student_name\":\"DATIN. Osomania Azarul\",\"email_id\":\"oa@cms.com\",\"nric\":\"NR213312\",\"id_intake\":\"1\",\"id_program\":\"3\",\"id_program_landscape\":\"10\",\"id_qualification\":\"1\",\"id_program_scheme\":\"14\",\"isStudentAdminLoggedIn\":true}', '157.49.189.60', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '2020-09-10 14:40:04'),
(13, 23, '{\"student_name\":\"DATIN. Student Eight\",\"email_id\":\"s8@cms.com\",\"nric\":\"TIQWE123\",\"id_intake\":\"1\",\"id_program\":\"3\",\"id_program_landscape\":\"10\",\"id_qualification\":\"1\",\"id_program_scheme\":\"14\",\"student_education_level\":\"POSTGRADUATE\"}', '157.49.161.86', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '2020-09-12 13:14:25'),
(14, 24, '{\"student_name\":\"DATIN. Sadhullah Beeg\",\"email_id\":\"sb@cms.com\",\"nric\":\"9074567090\",\"id_intake\":\"1\",\"id_program\":\"4\",\"id_program_landscape\":\"18\",\"id_qualification\":\"7\",\"id_program_scheme\":\"15\",\"student_education_level\":\"Master\",\"isStudentAdminLoggedIn\":true}', '42.190.19.126', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Windows 10', '2020-10-07 21:46:14'),
(15, 27, '{\"student_name\":\"DATIN. Ragina Federar\",\"email_id\":\"rf@cms.com\",\"nric\":\"119088\",\"id_intake\":\"1\",\"id_program\":\"3\",\"id_program_landscape\":null,\"id_qualification\":\"7\",\"id_program_scheme\":\"14\",\"student_education_level\":\"Master\"}', '157.49.126.255', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '2020-10-08 22:15:21'),
(16, 27, '{\"student_name\":\"DATIN. Ragina Federar\",\"email_id\":\"rf@cms.com\",\"nric\":\"119088\",\"id_intake\":\"1\",\"id_program\":\"3\",\"id_program_landscape\":null,\"id_qualification\":\"7\",\"id_program_scheme\":\"14\",\"student_education_level\":\"Master\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-10-08 22:32:33'),
(17, 28, '{\"student_name\":\"AG. Ragina Federer\",\"email_id\":\"rf@cms.com\",\"nric\":\"NF888\",\"id_intake\":\"4\",\"id_program\":\"3\",\"id_program_landscape\":\"11\",\"id_qualification\":\"1\",\"id_program_scheme\":\"14\",\"student_education_level\":\"POSTGRADUATE\"}', '157.49.64.231', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '2020-10-17 21:30:38'),
(18, 29, '{\"student_name\":\"DATINDR. Naani Haris\",\"email_id\":\"nh@cms.com\",\"nric\":\"SADJAKKA7878\",\"id_intake\":\"4\",\"id_program\":\"4\",\"id_program_landscape\":\"17\",\"id_qualification\":\"7\",\"id_program_scheme\":\"16\",\"student_education_level\":\"Master\"}', '157.49.75.119', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '2020-10-18 13:07:46'),
(19, 30, '{\"student_name\":\"DATINDR. Khaleel Ahmed\",\"email_id\":\"ah@cms.com\",\"nric\":\"SA6863821\",\"id_intake\":\"4\",\"id_program\":\"3\",\"id_program_landscape\":\"0\",\"id_qualification\":\"1\",\"id_program_scheme\":\"17\",\"student_education_level\":\"POSTGRADUATE\"}', '157.49.185.42', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '2020-10-27 07:32:16'),
(20, 30, '{\"student_name\":\"DATINDR. Khaleel Ahmed\",\"email_id\":\"ah@cms.com\",\"nric\":\"SA6863821\",\"id_intake\":\"4\",\"id_program\":\"3\",\"id_program_landscape\":\"0\",\"id_qualification\":\"1\",\"id_program_scheme\":\"17\",\"student_education_level\":\"POSTGRADUATE\"}', '157.49.94.176', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '2020-11-07 12:41:43'),
(21, 31, '{\"student_name\":\"MR. Student One\",\"email_id\":\"s1@cms.com\",\"nric\":\"NR000001\",\"id_intake\":\"1\",\"id_program\":\"6\",\"id_program_landscape\":\"21\",\"id_qualification\":\"1\",\"id_program_scheme\":\"21\",\"student_education_level\":\"POSTGRADUATE\"}', '117.230.141.80', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '2020-11-08 11:50:46'),
(22, 32, '{\"student_name\":\"MDM. Student  Five\",\"email_id\":\"s5@cms.com\",\"nric\":\"NR05\",\"id_intake\":\"1\",\"id_program\":\"6\",\"id_program_landscape\":\"22\",\"id_qualification\":\"1\",\"id_program_scheme\":\"22\",\"student_education_level\":\"POSTGRADUATE\"}', '117.230.12.209', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '2020-11-09 12:14:33'),
(23, 35, '{\"student_name\":\"AG. aeutesting2 2\",\"email_id\":\"aeutesting2@gmail.com\",\"nric\":\"7373737337377\",\"id_intake\":\"1\",\"id_program\":\"6\",\"id_program_landscape\":\"21\",\"id_qualification\":\"1\",\"id_program_scheme\":\"21\",\"student_education_level\":\"POSTGRADUATE\",\"isStudentAdminLoggedIn\":true}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-11-10 19:58:50'),
(24, 35, '{\"student_name\":\"AG. aeutesting2 2\",\"email_id\":\"aeutesting2@gmail.com\",\"nric\":\"7373737337377\",\"id_intake\":\"1\",\"id_program\":\"6\",\"id_program_landscape\":\"21\",\"id_qualification\":\"1\",\"id_program_scheme\":\"21\",\"student_education_level\":\"POSTGRADUATE\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-11-30 00:13:03'),
(25, 35, '{\"student_name\":\"AG. aeutesting2 2\",\"email_id\":\"aeutesting2@gmail.com\",\"nric\":\"7373737337377\",\"id_intake\":\"1\",\"id_program\":\"6\",\"id_program_landscape\":\"21\",\"id_qualification\":\"1\",\"id_program_scheme\":\"21\",\"student_education_level\":\"POSTGRADUATE\"}', '49.206.4.152', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2020-11-30 03:09:26'),
(26, 35, '{\"student_name\":\"AG. aeutesting2 2\",\"email_id\":\"aeutesting2@gmail.com\",\"nric\":\"7373737337377\",\"id_intake\":\"1\",\"id_program\":\"6\",\"id_program_landscape\":\"21\",\"id_qualification\":\"1\",\"id_program_scheme\":\"21\",\"student_education_level\":\"POSTGRADUATE\"}', '49.206.4.152', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2020-11-30 07:21:03'),
(27, 35, '{\"student_name\":\"AG. aeutesting2 2\",\"email_id\":\"aeutesting2@gmail.com\",\"nric\":\"7373737337377\",\"id_intake\":\"1\",\"id_program\":\"6\",\"id_program_landscape\":\"21\",\"id_qualification\":\"1\",\"id_program_scheme\":\"21\",\"student_education_level\":\"POSTGRADUATE\"}', '42.190.11.118', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Windows 10', '2020-11-30 18:30:22'),
(28, 35, '{\"student_name\":\"AG. aeutesting2 2\",\"email_id\":\"aeutesting2@gmail.com\",\"nric\":\"7373737337377\",\"id_intake\":\"1\",\"id_program\":\"6\",\"id_program_landscape\":\"21\",\"id_qualification\":\"1\",\"id_program_scheme\":\"21\",\"student_education_level\":\"POSTGRADUATE\"}', '42.190.11.118', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Windows 10', '2020-12-02 23:20:23'),
(29, 35, '{\"student_name\":\"AG. aeutesting2 2\",\"email_id\":\"aeutesting2@gmail.com\",\"nric\":\"7373737337377\",\"id_intake\":\"1\",\"id_program\":\"6\",\"id_program_landscape\":\"21\",\"id_qualification\":\"1\",\"id_program_scheme\":\"21\",\"student_education_level\":\"POSTGRADUATE\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-12-07 22:14:14'),
(30, 35, '{\"student_name\":\"AG. aeutesting2 2\",\"email_id\":\"aeutesting2@gmail.com\",\"nric\":\"7373737337377\",\"id_intake\":\"1\",\"id_program\":\"6\",\"id_program_landscape\":\"21\",\"id_qualification\":\"1\",\"id_program_scheme\":\"21\",\"student_education_level\":\"POSTGRADUATE\"}', '42.190.11.118', 'Chrome 87.0.4280.88', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 'Windows 10', '2020-12-07 22:16:51'),
(31, 40, '{\"student_name\":\"HAJI. CC C\",\"email_id\":\"aeudemo0803@gmail.com\",\"nric\":\"0987\",\"id_intake\":\"1\",\"id_program\":\"7\",\"id_program_landscape\":\"24\",\"id_qualification\":\"7\",\"id_program_scheme\":\"24\",\"student_education_level\":\"Master\"}', '49.206.4.152', 'Chrome 87.0.4280.88', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 'Windows 10', '2020-12-08 09:10:10'),
(32, 42, '{\"student_name\":\"MR. Kiran  testing\",\"email_id\":\"11@gmail.com\",\"nric\":\"1111122332111\",\"id_intake\":\"1\",\"id_program\":\"8\",\"id_program_landscape\":\"0\",\"id_qualification\":\"1\",\"id_program_scheme\":\"26\",\"student_education_level\":\"POSTGRADUATE\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-12-09 02:30:10'),
(33, 44, '{\"student_name\":\"DRHAJI. DD D\",\"email_id\":\"aeudemo1001@gmail.com\",\"nric\":\"3456\",\"id_intake\":\"1\",\"id_program\":\"8\",\"id_program_landscape\":\"0\",\"id_qualification\":\"1\",\"id_program_scheme\":\"26\",\"student_education_level\":\"POSTGRADUATE\"}', '49.206.4.152', 'Chrome 87.0.4280.88', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 'Windows 10', '2020-12-09 21:02:42'),
(34, 31, '{\"student_name\":\"MR. Student One\",\"email_id\":\"s1@cms.com\",\"nric\":\"NR000001\",\"id_intake\":\"1\",\"id_program\":\"6\",\"id_program_landscape\":\"21\",\"id_qualification\":\"1\",\"id_program_scheme\":\"21\",\"student_education_level\":\"POSTGRADUATE\"}', '42.105.116.181', 'Chrome 87.0.4280.88', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 'Windows 10', '2020-12-10 22:26:18'),
(35, 45, '{\"student_name\":\"MR. Developper Testing5\",\"email_id\":\"dt5@cms.com\",\"nric\":\"DTNRIC05\",\"id_intake\":\"1\",\"id_program\":\"8\",\"id_program_landscape\":\"0\",\"id_qualification\":\"1\",\"id_program_scheme\":\"26\",\"student_education_level\":\"POSTGRADUATE\"}', '157.45.71.122', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2020-12-11 02:51:46'),
(36, 33, '{\"student_name\":\"AG. Student Four\",\"email_id\":\"s4@cms.com\",\"nric\":\"NR04\",\"id_intake\":\"1\",\"id_program\":\"6\",\"id_program_landscape\":\"21\",\"id_qualification\":\"1\",\"id_program_scheme\":\"21\",\"student_education_level\":\"POSTGRADUATE\"}', '157.45.71.122', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2020-12-11 13:04:55'),
(37, 33, '{\"student_name\":\"AG. Student Four\",\"email_id\":\"s4@cms.com\",\"nric\":\"NR04\",\"id_intake\":\"1\",\"id_program\":\"6\",\"id_program_landscape\":\"21\",\"id_qualification\":\"1\",\"id_program_scheme\":\"21\",\"student_education_level\":\"POSTGRADUATE\"}', '157.45.71.122', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2020-12-11 20:37:07'),
(38, 46, '{\"student_name\":\"MR. Student 6\",\"email_id\":\"s6@cms.com\",\"nric\":\"NRICS8006\",\"id_intake\":\"1\",\"id_program\":\"8\",\"id_program_landscape\":\"0\",\"id_qualification\":\"1\",\"id_program_scheme\":\"26\",\"student_education_level\":\"POSTGRADUATE\"}', '42.105.117.54', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2020-12-12 10:51:51'),
(39, 49, '{\"student_name\":\"DR. Student 8\",\"email_id\":\"s8@cms.com\",\"nric\":\"NRICS8008\",\"id_intake\":\"1\",\"id_program\":\"8\",\"id_program_landscape\":\"0\",\"id_qualification\":\"1\",\"id_program_scheme\":\"26\",\"student_education_level\":\"POSTGRADUATE\"}', '42.105.117.179', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2020-12-12 12:01:27'),
(40, 51, '{\"student_name\":\"MR. Developper Testing 6\",\"email_id\":\"dt6@cms.com\",\"nric\":\"NRICS8006\",\"id_intake\":\"1\",\"id_program\":\"8\",\"id_program_landscape\":\"28\",\"id_qualification\":\"1\",\"id_program_scheme\":\"26\",\"student_education_level\":\"POSTGRADUATE\"}', '157.45.79.144', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2020-12-15 10:43:17'),
(41, 51, '{\"student_name\":\"MR. Developper Testing 6\",\"email_id\":\"dt6@cms.com\",\"nric\":\"NRICS8006\",\"id_intake\":\"1\",\"id_program\":\"8\",\"id_program_landscape\":\"28\",\"id_qualification\":\"1\",\"id_program_scheme\":\"1\",\"student_education_level\":\"POSTGRADUATE\",\"student_semester\":\"1\"}', '157.45.167.117', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2020-12-16 12:03:38'),
(42, 54, '{\"student_name\":\"DR. Developper Testing 7\",\"email_id\":\"dt7@cms.com\",\"nric\":\"NRICS8007\",\"id_intake\":\"1\",\"id_program\":\"8\",\"id_program_landscape\":\"28\",\"id_qualification\":\"1\",\"id_program_scheme\":\"1\",\"student_education_level\":\"POSTGRADUATE\",\"student_semester\":\"1\"}', '42.105.116.144', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2020-12-18 13:12:59'),
(43, 55, '{\"student_name\":\"HAJI. Developper Testing 8\",\"email_id\":\"dt8@cms.com\",\"nric\":\"NRICS8008\",\"id_intake\":\"1\",\"id_program\":\"8\",\"id_program_landscape\":\"28\",\"id_qualification\":\"1\",\"id_program_scheme\":\"1\",\"student_education_level\":\"POSTGRADUATE\",\"student_semester\":\"1\"}', '42.105.116.144', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2020-12-18 13:21:07'),
(44, 62, '{\"student_name\":\"MRS. AA 02\",\"email_id\":\"aa02@gmail.com\",\"nric\":\"2\",\"id_intake\":\"1\",\"id_program\":\"9\",\"id_program_landscape\":\"33\",\"id_qualification\":\"2\",\"id_program_scheme\":\"1\",\"student_education_level\":\"UNDERGRADUATE\",\"student_semester\":\"1\"}', '49.206.4.152', 'Chrome 87.0.4280.88', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 'Windows 10', '2020-12-23 08:59:53'),
(45, 63, '{\"student_name\":\"MRS. AA 02\",\"email_id\":\"aa02@gmail.com\",\"nric\":\"2\",\"id_intake\":\"1\",\"id_program\":\"9\",\"id_program_landscape\":\"33\",\"id_qualification\":\"2\",\"id_program_scheme\":\"1\",\"student_education_level\":\"UNDERGRADUATE\",\"student_semester\":\"1\"}', '157.45.71.122', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2020-12-23 09:15:28'),
(46, 63, '{\"student_name\":\"MRS. AA 02\",\"email_id\":\"aa02@gmail.com\",\"nric\":\"2\",\"id_intake\":\"1\",\"id_program\":\"9\",\"id_program_landscape\":\"33\",\"id_qualification\":\"2\",\"id_program_scheme\":\"1\",\"student_education_level\":\"UNDERGRADUATE\",\"student_semester\":\"1\"}', '117.230.51.163', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2020-12-24 03:13:06'),
(47, 61, '{\"student_name\":\"MR. AA 01\",\"email_id\":\"aa01@gmail.com\",\"nric\":\"01\",\"id_intake\":\"1\",\"id_program\":\"9\",\"id_program_landscape\":\"33\",\"id_qualification\":\"2\",\"id_program_scheme\":\"1\",\"student_education_level\":\"UNDERGRADUATE\",\"student_semester\":\"1\"}', '117.230.154.46', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2020-12-24 03:20:24'),
(48, 63, '{\"student_name\":\"MRS. AA 02\",\"email_id\":\"aa02@gmail.com\",\"nric\":\"2\",\"id_intake\":\"1\",\"id_program\":\"9\",\"id_program_landscape\":\"33\",\"id_qualification\":\"2\",\"id_program_scheme\":\"1\",\"student_education_level\":\"UNDERGRADUATE\",\"student_semester\":\"1\"}', '42.105.125.15', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2020-12-24 08:16:14'),
(49, 64, '{\"student_name\":\"EN. Developper Testing 17\",\"email_id\":\"dt17@cms.com\",\"nric\":\"NRIC8017\",\"id_intake\":\"1\",\"id_program\":\"6\",\"id_program_landscape\":\"29\",\"id_qualification\":\"1\",\"id_program_scheme\":\"2\",\"student_education_level\":\"POSTGRADUATE\",\"student_semester\":\"1\"}', '42.105.125.15', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2020-12-24 08:32:19'),
(50, 71, '{\"student_name\":\"DR. PHD Testing 1\",\"email_id\":\"phdt1@cms.com\",\"nric\":\"NRICS88001\",\"id_intake\":\"1\",\"id_program\":\"6\",\"id_program_landscape\":\"29\",\"id_qualification\":\"1\",\"id_program_scheme\":\"2\",\"student_education_level\":\"POSTGRADUATE\",\"student_semester\":\"1\"}', '157.45.8.126', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2020-12-25 12:06:48'),
(51, 72, '{\"student_name\":\"HE. PHD Testing 2\",\"email_id\":\"phdt2@cms.com\",\"nric\":\"NRICSPHDT8002\",\"id_intake\":\"1\",\"id_program\":\"6\",\"id_program_landscape\":\"29\",\"id_qualification\":\"1\",\"id_program_scheme\":\"2\",\"student_education_level\":\"POSTGRADUATE\",\"student_semester\":\"1\"}', '157.45.8.126', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2020-12-25 12:07:55'),
(52, 73, '{\"student_name\":\"MS. pHD Testing 3\",\"email_id\":\"phdt3@cms.com\",\"nric\":\"NRICSPHD8003\",\"id_intake\":\"1\",\"id_program\":\"6\",\"id_program_landscape\":\"29\",\"id_qualification\":\"1\",\"id_program_scheme\":\"2\",\"student_education_level\":\"POSTGRADUATE\",\"student_semester\":\"1\"}', '157.45.8.126', 'Firefox 84.0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:84.0) Gecko/20100101 Firefox/84.0', 'Linux', '2020-12-25 12:14:44'),
(53, 40, '{\"student_name\":\"HAJI. CC C\",\"email_id\":\"aeudemo0803@gmail.com\",\"nric\":\"0987\",\"id_intake\":\"1\",\"id_program\":\"7\",\"id_program_landscape\":\"24\",\"id_qualification\":\"7\",\"id_program_scheme\":\"1\",\"student_education_level\":\"MASTER\",\"student_semester\":\"1\"}', '49.206.15.129', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2021-01-10 22:54:14'),
(54, 74, '{\"student_name\":\"Mr. Developper Testing One\",\"email_id\":\"dt1@eag.com\",\"nric\":\"80001\",\"id_intake\":\"3\",\"id_program\":\"10\",\"id_program_landscape\":\"34\",\"id_qualification\":\"2\",\"id_program_scheme\":\"1\",\"student_education_level\":\"POSTGRADUATE\",\"student_semester\":\"1\"}', '157.45.180.227', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2021-01-13 03:25:53'),
(55, 74, '{\"student_name\":\"Mr. Developper Testing One\",\"email_id\":\"dt1@eag.com\",\"nric\":\"80001\",\"id_intake\":\"3\",\"id_program\":\"10\",\"id_program_landscape\":\"34\",\"id_qualification\":\"2\",\"id_program_scheme\":\"29\",\"student_education_level\":\"PHD\",\"isStudentAdminLoggedIn\":true}', '42.190.11.118', 'Chrome 87.0.4280.141', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36', 'Windows 10', '2021-01-13 23:26:54'),
(56, 74, '{\"student_name\":\"Mr. Developper Testing One\",\"email_id\":\"dt1@eag.com\",\"nric\":\"80001\",\"id_intake\":\"3\",\"id_program\":\"10\",\"id_program_landscape\":\"34\",\"id_qualification\":\"2\",\"id_program_scheme\":\"29\",\"student_education_level\":\"POSTGRADUATE\",\"isStudentAdminLoggedIn\":true}', '42.190.11.118', 'Chrome 87.0.4280.141', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36', 'Windows 10', '2021-01-14 19:32:48'),
(57, 78, '{\"student_name\":\"Mr. Developper Testing Five\",\"email_id\":\"dt5@eag.com\",\"nric\":\"80005\",\"id_intake\":\"3\",\"id_program\":\"10\",\"id_program_landscape\":\"34\",\"id_qualification\":\"2\",\"id_program_scheme\":\"1\",\"student_education_level\":\"POSTGRADUATE\",\"student_semester\":\"1\"}', '117.230.21.197', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2021-01-15 11:06:43'),
(58, 74, '{\"student_name\":\"Mr. Developper Testing One\",\"email_id\":\"dt1@eag.com\",\"nric\":\"80001\",\"id_intake\":\"3\",\"id_program\":\"10\",\"id_program_landscape\":\"34\",\"id_qualification\":\"2\",\"id_program_scheme\":\"29\",\"student_education_level\":\"POSTGRADUATE\",\"student_profile_pic\":\"default_profile.jpg\",\"isStudentAdminLoggedIn\":true}', '42.190.11.118', 'Chrome 87.0.4280.141', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36', 'Windows 10', '2021-01-17 01:08:33'),
(59, 74, '{\"student_name\":\"Mr. Developper Testing One\",\"email_id\":\"dt1@eag.com\",\"nric\":\"80001\",\"id_intake\":\"3\",\"id_program\":\"10\",\"id_program_landscape\":\"34\",\"id_qualification\":\"2\",\"id_program_scheme\":\"29\",\"student_education_level\":\"POSTGRADUATE\",\"student_profile_pic\":\"default_profile.jpg\",\"isStudentAdminLoggedIn\":true}', '42.190.11.118', 'Chrome 87.0.4280.141', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36', 'Windows 10', '2021-01-17 19:41:14');

-- --------------------------------------------------------

--
-- Table structure for table `student_note`
--

CREATE TABLE `student_note` (
  `id` int(20) NOT NULL,
  `id_student` int(20) DEFAULT 0,
  `note` varchar(10240) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_note`
--

INSERT INTO `student_note` (`id`, `id_student`, `note`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(2, 6, 'Fee Paymet Alert Need To Add', NULL, 1, '2020-08-10 13:49:18', NULL, '2020-08-10 13:49:18'),
(3, 4, 'Fee Paymet Alert Need To Add', NULL, 1, '2020-08-10 13:49:18', NULL, '2020-08-10 13:49:18'),
(4, 7, 'Payment Added & Migration As Student Done', NULL, 1, '2020-08-10 14:28:03', NULL, '2020-08-10 14:28:03');

-- --------------------------------------------------------

--
-- Table structure for table `tax`
--

CREATE TABLE `tax` (
  `id` int(20) NOT NULL,
  `name` varchar(250) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `percentage` varchar(20) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_last_login`
--

CREATE TABLE `tbl_last_login` (
  `id` bigint(20) NOT NULL,
  `userId` bigint(20) NOT NULL,
  `sessionData` varchar(2048) NOT NULL,
  `machineIp` varchar(1024) NOT NULL,
  `userAgent` varchar(128) NOT NULL,
  `agentString` varchar(1024) NOT NULL,
  `platform` varchar(128) NOT NULL,
  `createdDtm` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_last_login`
--

INSERT INTO `tbl_last_login` (`id`, `userId`, `sessionData`, `machineIp`, `userAgent`, `agentString`, `platform`, `createdDtm`) VALUES
(1, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.7.23', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(2, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.7.23', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(3, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.7.23', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(4, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '117.230.180.74', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(5, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.7.23', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(6, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.7.23', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(7, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '112.79.53.220', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(8, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.7.23', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(9, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.7.23', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(10, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '112.79.50.106', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(11, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.7.23', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(12, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '112.79.51.107', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(13, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.7.23', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(14, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '112.79.51.38', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(15, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '112.79.51.38', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(16, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '112.79.50.252', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(17, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.7.23', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(18, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '112.79.50.14', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(19, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '112.79.51.142', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(20, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.7.23', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(21, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '112.79.50.225', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(22, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.7.23', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(23, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '112.79.53.171', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(24, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.7.23', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(25, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '42.190.31.148', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36 Edg/84.0.522.40', 'Windows 10', '0000-00-00 00:00:00'),
(26, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '112.79.52.12', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(27, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '112.79.50.244', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(28, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.7.23', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(29, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.7.23', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(30, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '112.79.51.16', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(31, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.7.23', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(32, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '42.190.15.202', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36 Edg/84.0.522.40', 'Windows 10', '0000-00-00 00:00:00'),
(33, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '112.79.53.197', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(34, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '112.79.55.57', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(35, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '112.79.48.70', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(36, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '42.190.15.202', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36 Edg/84.0.522.44', 'Windows 10', '0000-00-00 00:00:00'),
(37, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '14.98.22.14', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(38, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '42.190.15.202', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(39, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.7.23', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(40, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.7.23', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(41, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.7.23', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(42, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.7.23', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(43, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.45.15.79', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(44, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '117.230.160.152', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(45, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.45.15.79', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(46, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '42.190.15.202', 'Chrome 84.0.4147.105', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36 Edg/84.0.522.52', 'Windows 10', '0000-00-00 00:00:00'),
(47, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '124.123.104.139', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(48, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.7.23', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(49, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '112.79.80.205', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(50, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '112.79.90.175', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(51, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.7.23', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(52, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '42.190.15.202', 'Chrome 84.0.4147.105', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36 Edg/84.0.522.52', 'Windows 10', '0000-00-00 00:00:00'),
(53, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '112.79.88.194', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(54, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '112.79.52.202', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(55, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.7.23', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(56, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.49.169.254', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(57, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '112.79.51.4', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(58, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '124.123.104.139', 'Chrome 84.0.4147.105', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(59, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.49.170.225', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(60, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '42.190.15.202', 'Chrome 84.0.4147.105', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36 Edg/84.0.522.52', 'Windows 10', '0000-00-00 00:00:00'),
(61, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.49.191.89', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(62, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '112.79.49.44', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(63, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '42.190.15.202', 'Chrome 84.0.4147.105', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36 Edg/84.0.522.52', 'Windows 10', '0000-00-00 00:00:00'),
(64, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.7.23', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(65, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.49.133.61', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(66, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.49.133.61', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(67, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '42.190.15.202', 'Chrome 84.0.4147.105', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36 Edg/84.0.522.52', 'Windows 10', '0000-00-00 00:00:00'),
(68, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.7.23', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(69, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.7.23', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(70, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '112.79.51.100', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(71, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '42.190.15.202', 'Chrome 84.0.4147.105', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36 Edg/84.0.522.52', 'Windows 10', '0000-00-00 00:00:00'),
(72, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '42.190.15.202', 'Chrome 84.0.4147.105', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36 Edg/84.0.522.52', 'Windows 10', '0000-00-00 00:00:00'),
(73, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.7.23', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(74, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.49.133.58', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(75, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '112.79.49.45', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(76, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '42.190.56.252', 'Chrome 84.0.4147.105', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36 Edg/84.0.522.52', 'Windows 10', '0000-00-00 00:00:00'),
(77, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.7.23', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(78, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.7.23', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(79, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '42.190.31.233', 'Chrome 84.0.4147.105', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36 Edg/84.0.522.52', 'Windows 10', '0000-00-00 00:00:00'),
(80, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '42.190.31.233', 'Chrome 84.0.4147.105', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36 Edg/84.0.522.52', 'Windows 10', '0000-00-00 00:00:00'),
(81, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.7.23', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(82, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '112.79.80.180', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(83, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '106.197.221.48', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(84, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '112.79.88.170', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(85, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '42.190.31.233', 'Chrome 79.0.3945.136', 'Mozilla/5.0 (Linux; Android 10; SAMSUNG SM-N960F) AppleWebKit/537.36 (KHTML, like Gecko) SamsungBrowser/12.0 Chrome/79.0.3945.136 Mobile Safari/537.36', 'Android', '0000-00-00 00:00:00'),
(86, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '42.190.31.233', 'Chrome 84.0.4147.105', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36 Edg/84.0.522.58', 'Windows 10', '0000-00-00 00:00:00'),
(87, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '106.197.199.72', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(88, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '106.197.199.72', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(89, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '117.230.26.220', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(90, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.49.172.59', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(91, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '42.190.31.233', 'Chrome 84.0.4147.105', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36 Edg/84.0.522.58', 'Windows 10', '0000-00-00 00:00:00'),
(92, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.49.172.59', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(93, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.45.95.195', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(94, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '117.230.149.115', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(95, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Safari 604.1', 'Mozilla/5.0 (iPhone; CPU iPhone OS 13_5 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) CriOS/84.0.4147.71 Mobile/15E148 Safari/604.1', 'iOS', '0000-00-00 00:00:00'),
(96, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Safari 604.1', 'Mozilla/5.0 (iPhone; CPU iPhone OS 13_5 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) CriOS/84.0.4147.71 Mobile/15E148 Safari/604.1', 'iOS', '0000-00-00 00:00:00'),
(97, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '112.79.80.70', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(98, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(99, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(100, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(101, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(102, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '112.79.53.166', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(103, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(104, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(105, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.49.64.114', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(106, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '124.123.106.237', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(107, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '124.123.106.237', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(108, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.49.75.38', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(109, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '124.123.106.237', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(110, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.49.67.94', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(111, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(112, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(113, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.49.67.94', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(114, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(115, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(116, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(117, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.49.74.131', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(118, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(119, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(120, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.49.65.8', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(121, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(122, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.49.72.71', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(123, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(124, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '183.171.71.226', 'Chrome 84.0.4147.125', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(125, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '183.171.71.226', 'Chrome 84.0.4147.125', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(126, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(127, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(128, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '183.171.185.78', 'Chrome 84.0.4147.135', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.135 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(129, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(130, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.49.81.173', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(131, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.49.75.83', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(132, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.49.72.21', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(133, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(134, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.49.72.21', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(135, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(136, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(137, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.45.45.121', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(138, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.45.62.6', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(139, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '42.190.31.233', 'Chrome 84.0.4147.135', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.135 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(140, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(141, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.45.75.202', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(142, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.45.71.199', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(143, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(144, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '42.190.31.233', 'Chrome 84.0.4147.135', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.135 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(145, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.45.69.83', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(146, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(147, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.45.72.147', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(148, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(149, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.45.89.27', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(150, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.49.147.228', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(151, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.45.88.85', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(152, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '42.190.31.233', 'Chrome 84.0.4147.135', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.135 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(153, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(154, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '42.190.31.233', 'Chrome 84.0.4147.135', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.135 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(155, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.49.189.149', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(156, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '42.190.31.233', 'Chrome 84.0.4147.135', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.135 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(157, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(158, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.49.175.138', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(159, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.49.109.47', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(160, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '42.190.31.233', 'Chrome 84.0.4147.135', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.135 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(161, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '42.190.31.233', 'Chrome 84.0.4147.135', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.135 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(162, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(163, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '112.79.85.46', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(164, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '42.190.31.233', 'Chrome 85.0.4183.83', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(165, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.45.50.107', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(166, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '112.79.92.246', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(167, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.49.115.47', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(168, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(169, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.45.43.66', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(170, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '42.190.31.233', 'Chrome 85.0.4183.83', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(171, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '42.190.31.233', 'Chrome 85.0.4183.83', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(172, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.45.39.199', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(173, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Safari 604.1', 'Mozilla/5.0 (iPhone; CPU iPhone OS 13_6 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) CriOS/85.0.4183.72 Mobile/15E148 Safari/604.1', 'iOS', '0000-00-00 00:00:00'),
(174, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '42.190.31.233', 'Chrome 85.0.4183.83', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(175, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(176, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.45.41.100', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(177, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(178, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(179, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00');
INSERT INTO `tbl_last_login` (`id`, `userId`, `sessionData`, `machineIp`, `userAgent`, `agentString`, `platform`, `createdDtm`) VALUES
(180, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.45.56.255', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(181, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '42.190.31.233', 'Chrome 85.0.4183.83', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(182, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(183, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.45.44.44', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(184, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '42.190.31.233', 'Chrome 85.0.4183.83', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(185, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.45.45.28', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(186, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '42.190.31.233', 'Chrome 85.0.4183.83', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(187, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.45.54.157', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(188, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '183.171.96.1', 'Chrome 85.0.4183.83', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(189, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.45.63.59', 'Chrome 81.0.4044.111', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.111 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(190, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '112.79.80.127', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(191, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(192, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(193, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '112.79.92.89', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(194, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '1.39.139.215', 'Safari 604.1', 'Mozilla/5.0 (iPhone; CPU iPhone OS 13_6 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) CriOS/85.0.4183.72 Mobile/15E148 Safari/604.1', 'iOS', '0000-00-00 00:00:00'),
(195, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '175.141.143.249', 'Chrome 85.0.4183.83', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(196, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '112.79.80.122', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(197, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '112.79.52.253', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(198, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.45.44.49', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(199, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '112.79.51.143', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(200, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(201, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.45.60.114', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(202, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(203, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.45.44.99', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(204, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '42.190.31.233', 'Chrome 85.0.4183.83', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(205, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(206, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(207, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.49.174.157', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(208, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(209, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(210, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.49.189.60', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(211, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(212, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(213, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(214, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.49.174.206', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(215, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.49.174.89', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(216, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.49.178.137', 'Chrome 85.0.4183.102', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(217, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '42.190.31.233', 'Chrome 85.0.4183.83', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(218, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.49.176.43', 'Chrome 85.0.4183.102', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(219, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.49.166.214', 'Chrome 85.0.4183.102', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(220, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(221, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '183.171.79.216', 'Chrome 85.0.4183.83', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(222, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(223, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(224, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(225, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '42.190.31.233', 'Chrome 85.0.4183.83', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(226, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(227, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '42.190.31.233', 'Chrome 85.0.4183.102', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(228, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '42.190.31.233', 'Chrome 85.0.4183.102', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(229, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(230, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(231, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(232, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.49.164.228', 'Chrome 85.0.4183.102', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(233, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '42.190.31.233', 'Chrome 85.0.4183.102', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(234, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(235, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '42.190.31.233', 'Chrome 85.0.4183.102', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(236, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '42.190.31.233', 'Chrome 85.0.4183.102', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(237, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(238, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(239, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.49.156.160', 'Chrome 85.0.4183.102', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(240, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(241, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '115.133.17.87', 'Chrome 85.0.4183.102', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(242, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.49.140.116', 'Chrome 85.0.4183.102', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(243, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.49.128.131', 'Chrome 85.0.4183.102', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(244, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(245, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.49.131.37', 'Chrome 85.0.4183.102', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(246, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(247, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.49.177.11', 'Chrome 85.0.4183.102', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(248, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '183.171.84.45', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(249, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(250, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '183.171.70.176', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(251, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(252, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '42.190.19.126', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(253, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '42.190.19.126', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(254, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '42.190.19.126', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(255, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '42.190.19.126', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(256, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.49.126.255', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(257, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(258, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.45.32.248', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(259, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '42.190.19.126', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(260, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(261, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '117.230.153.184', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(262, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '117.230.142.181', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(263, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.49.68.10', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(264, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '42.190.22.135', 'Chrome 86.0.4240.75', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(265, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '1.39.131.83', 'Safari 604.1', 'Mozilla/5.0 (iPhone; CPU iPhone OS 13_7 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.1.2 Mobile/15E148 Safari/604.1', 'iOS', '0000-00-00 00:00:00'),
(266, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '117.230.14.183', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(267, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(268, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.45.56.163', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(269, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.49.87.183', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(270, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '117.230.53.27', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(271, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '112.79.54.223', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(272, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.49.76.186', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(273, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '117.230.188.179', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(274, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Safari 604.1', 'Mozilla/5.0 (iPhone; CPU iPhone OS 13_7 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) CriOS/86.0.4240.93 Mobile/15E148 Safari/604.1', 'iOS', '0000-00-00 00:00:00'),
(275, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.49.64.231', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(276, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.13.223', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(277, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.49.75.119', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(278, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '42.190.22.135', 'Chrome 86.0.4240.75', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(279, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.13.223', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(280, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.13.223', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(281, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.49.75.119', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(282, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(283, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(284, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(285, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(286, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.49.175.188', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(287, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(288, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(289, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(290, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.49.181.60', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(291, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.49.185.42', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(292, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(293, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '42.190.22.135', 'Chrome 86.0.4240.111', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(294, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.49.169.78', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(295, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(296, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(297, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.49.90.103', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(298, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(299, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '117.230.26.123', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(300, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '117.230.37.0', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(301, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '117.230.163.21', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(302, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '42.190.22.135', 'Chrome 86.0.4240.111', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(303, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.45.80.110', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(304, 10, '{\"role\":\"14\",\"roleText\":\"Permision View\",\"name\":\"Permission Assigner\"}', '157.45.80.110', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(305, 10, '{\"role\":\"14\",\"roleText\":\"Permision View\",\"name\":\"Permission Assigner\"}', '157.45.80.110', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(306, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.190.22.135', 'Chrome 86.0.4240.111', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(307, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(308, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.49.99.229', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(309, 10, '{\"role\":\"14\",\"roleText\":\"Permision View\",\"name\":\"Permission Assigner\"}', '157.49.99.229', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(310, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.49.99.229', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(311, 10, '{\"role\":\"14\",\"roleText\":\"Permision View\",\"name\":\"Permission Assigner\"}', '157.49.99.229', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(312, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.190.22.135', 'Chrome 86.0.4240.111', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(313, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(314, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.49.115.133', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(315, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.49.115.133', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(316, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.49.109.213', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(317, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.47.75', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(318, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.105.116.241', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(319, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(320, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.49.94.176', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(321, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.49.94.176', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(322, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(323, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '117.230.52.43', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(324, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.49.84.10', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(325, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(326, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(327, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.49.69.52', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(328, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '117.230.12.209', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(329, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(330, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(331, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.105.119.182', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(332, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.190.22.135', 'Chrome 86.0.4240.183', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(333, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(334, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.105.116.138', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(335, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 86.0.4240.183', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(336, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(337, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 86.0.4240.183', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(338, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(339, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '117.230.128.249', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(340, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(341, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.49.66.199', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(342, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.49.89.60', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(343, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '117.230.181.29', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(344, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.49.89.60', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(345, 11, '{\"role\":\"15\",\"roleText\":\"AEU Administrator\",\"name\":\"Aeu Administrator\"}', '157.45.85.128', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(346, 12, '{\"role\":\"16\",\"roleText\":\"AEU Examination Admin\",\"name\":\"Aeu Examination Admin\"}', '157.45.85.128', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(347, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(348, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 86.0.4240.198', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(349, 11, '{\"role\":\"15\",\"roleText\":\"AEU Administrator\",\"name\":\"Aeu Administrator\"}', '157.45.79.69', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(350, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.12.198', 'Chrome 86.0.4240.198', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(351, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.49.166.215', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(352, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.12.198', 'Chrome 86.0.4240.198', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(353, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.12.198', 'Chrome 86.0.4240.198', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(354, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.12.198', 'Chrome 86.0.4240.198', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(355, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.12.198', 'Chrome 86.0.4240.198', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(356, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.12.198', 'Chrome 86.0.4240.198', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(357, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.12.198', 'Chrome 86.0.4240.198', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(358, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(359, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.190.22.135', 'Chrome 86.0.4240.193', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00');
INSERT INTO `tbl_last_login` (`id`, `userId`, `sessionData`, `machineIp`, `userAgent`, `agentString`, `platform`, `createdDtm`) VALUES
(360, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.49.80.109', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(361, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.105.116.73', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(362, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.105.116.33', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(363, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(364, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(365, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(366, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '117.230.43.103', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(367, 11, '{\"role\":\"15\",\"roleText\":\"AEU Administrator\",\"name\":\"Aeu Administrator\"}', '117.230.43.103', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(368, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '117.230.43.103', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(369, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.190.11.118', 'Chrome 86.0.4240.193', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(370, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(371, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.190.11.118', 'Chrome 86.0.4240.193', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(372, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.190.11.118', 'Chrome 86.0.4240.193', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(373, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.105.120.166', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(374, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.190.11.118', 'Chrome 86.0.4240.193', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(375, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Safari 604.1', 'Mozilla/5.0 (iPhone; CPU iPhone OS 13_7 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) CriOS/86.0.4240.93 Mobile/15E148 Safari/604.1', 'iOS', '0000-00-00 00:00:00'),
(376, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(377, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(378, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.49.94.250', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(379, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(380, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.49.82.40', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(381, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.49.82.40', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(382, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(383, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.49.94.191', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(384, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.49.70.62', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(385, 13, '{\"role\":\"17\",\"roleText\":\"Research Colloquium\",\"name\":\"Colloquium User\"}', '157.49.70.62', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(386, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.49.70.62', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(387, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(388, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(389, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.190.11.118', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(390, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(391, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(392, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.48.196.25', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(393, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(394, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.190.11.118', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(395, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Safari 604.1', 'Mozilla/5.0 (iPhone; CPU iPhone OS 13_7 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.1.2 Mobile/15E148 Safari/604.1', 'iOS', '0000-00-00 00:00:00'),
(396, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(397, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(398, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.190.11.118', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(399, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(400, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(401, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(402, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.190.11.118', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(403, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(404, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(405, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.190.11.118', 'Chrome 83.0.4103.106', 'Mozilla/5.0 (Linux; Android 10; SAMSUNG SM-N960F) AppleWebKit/537.36 (KHTML, like Gecko) SamsungBrowser/13.0 Chrome/83.0.4103.106 Mobile Safari/537.36', 'Android', '0000-00-00 00:00:00'),
(406, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.190.11.118', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(407, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(408, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(409, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(410, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.190.11.118', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(411, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(412, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(413, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-12-03 00:22:08'),
(414, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2020-12-03 02:09:56'),
(415, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-12-03 02:25:25'),
(416, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2020-12-03 09:57:01'),
(417, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-12-03 19:17:23'),
(418, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '117.230.178.148', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2020-12-05 22:02:16'),
(419, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.190.11.118', 'Chrome 87.0.4280.88', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 'Windows 10', '2020-12-06 19:54:04'),
(420, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-12-06 23:53:45'),
(421, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '117.230.130.22', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2020-12-07 07:33:12'),
(422, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-12-07 08:56:43'),
(423, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '117.230.61.17', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2020-12-07 11:18:25'),
(424, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-12-07 18:59:43'),
(425, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.190.11.118', 'Chrome 87.0.4280.88', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 'Windows 10', '2020-12-07 22:21:03'),
(426, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-12-07 23:02:27'),
(427, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '223.186.176.100', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2020-12-07 23:41:33'),
(428, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-12-08 03:04:08'),
(429, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.190.11.118', 'Chrome 87.0.4280.88', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 'Windows 10', '2020-12-08 05:21:42'),
(430, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '223.186.176.100', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2020-12-08 06:11:22'),
(431, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-12-08 08:05:46'),
(432, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 87.0.4280.88', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 'Windows 10', '2020-12-08 08:08:46'),
(433, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '223.186.176.100', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2020-12-08 09:51:58'),
(434, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.190.11.118', 'Chrome 87.0.4280.88', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 'Windows 10', '2020-12-08 16:35:30'),
(435, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-12-08 18:30:17'),
(436, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-12-08 23:37:29'),
(437, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.190.11.118', 'Chrome 87.0.4280.88', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 'Windows 10', '2020-12-09 03:46:25'),
(438, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.105.116.125', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2020-12-09 13:42:13'),
(439, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-12-09 17:48:42'),
(440, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.71.122', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2020-12-09 20:19:13'),
(441, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 87.0.4280.88', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 'Windows 10', '2020-12-09 20:45:59'),
(442, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.71.122', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2020-12-10 00:19:08'),
(443, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-12-10 03:57:34'),
(444, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-12-10 06:21:18'),
(445, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-12-10 18:17:29'),
(446, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.105.116.181', 'Chrome 87.0.4280.88', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 'Windows 10', '2020-12-10 22:24:37'),
(447, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-12-11 00:32:04'),
(448, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.71.122', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2020-12-11 02:50:58'),
(449, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-12-11 03:11:16'),
(450, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.71.122', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2020-12-11 20:34:33'),
(451, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.105.118.168', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2020-12-12 03:41:18'),
(452, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.105.117.54', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2020-12-12 10:43:27'),
(453, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-12-14 04:22:33'),
(454, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.190.11.118', 'Chrome 87.0.4280.88', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 'Windows 10', '2020-12-14 04:30:56'),
(455, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.83.23', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2020-12-14 06:49:08'),
(456, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.83.23', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2020-12-14 07:14:19'),
(457, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.105.117.59', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2020-12-14 12:36:21'),
(458, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-12-14 17:29:28'),
(459, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.190.11.118', 'Chrome 87.0.4280.88', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 'Windows 10', '2020-12-14 19:38:32'),
(460, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.95.115', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2020-12-14 22:05:24'),
(461, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-12-14 23:05:15'),
(462, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.105.118.201', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2020-12-15 02:34:03'),
(463, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.105.125.194', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2020-12-15 05:19:43'),
(464, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-12-15 08:39:27'),
(465, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.79.144', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2020-12-15 09:53:51'),
(466, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.190.11.118', 'Chrome 87.0.4280.88', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 'Windows 10', '2020-12-15 21:35:46'),
(467, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-12-16 09:12:42'),
(468, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.167.117', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2020-12-16 12:03:15'),
(469, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-12-17 02:12:33'),
(470, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 87.0.4280.88', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 'Windows 10', '2020-12-17 02:48:32'),
(471, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.105.116.240', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2020-12-17 02:53:18'),
(472, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '27.59.17.87', 'Chrome 87.0.4280.88', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 'Mac OS X', '2020-12-17 03:02:41'),
(473, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-12-17 04:16:31'),
(474, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.105.118.232', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2020-12-17 12:14:45'),
(475, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-12-17 18:35:35'),
(476, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.105.118.167', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2020-12-17 20:23:12'),
(477, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '27.59.58.198', 'Chrome 87.0.4280.88', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 'Mac OS X', '2020-12-18 00:06:28'),
(478, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '27.59.58.198', 'Chrome 87.0.4280.88', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 'Mac OS X', '2020-12-18 03:47:10'),
(479, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.179.219', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2020-12-18 05:48:47'),
(480, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-12-18 07:47:44'),
(481, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.105.116.144', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2020-12-18 12:57:45'),
(482, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.190.11.118', 'Chrome 87.0.4280.88', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 'Windows 10', '2020-12-18 17:01:01'),
(483, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '117.230.62.170', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2020-12-19 14:20:11'),
(484, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.190.11.118', 'Chrome 87.0.4280.88', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 'Windows 10', '2020-12-19 17:31:55'),
(485, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.105.117.194', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2020-12-20 13:28:17'),
(486, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-12-21 17:31:15'),
(487, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.190.11.118', 'Chrome 87.0.4280.88', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 'Windows 10', '2020-12-21 17:37:17'),
(488, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-12-21 19:47:49'),
(489, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.190.11.118', 'Chrome 87.0.4280.88', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 'Windows 10', '2020-12-21 19:50:31'),
(490, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-12-21 21:32:45'),
(491, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.190.11.118', 'Chrome 87.0.4280.88', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 'Windows 10', '2020-12-21 22:04:01'),
(492, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.190.11.118', 'Chrome 87.0.4280.88', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 'Windows 10', '2020-12-21 22:05:13'),
(493, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '223.186.84.56', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2020-12-22 01:19:28'),
(494, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '117.230.150.237', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2020-12-22 06:28:16'),
(495, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-12-22 20:23:42'),
(496, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-12-22 20:23:56'),
(497, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-12-22 20:36:04'),
(498, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.190.11.118', 'Chrome 87.0.4280.88', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 'Windows 10', '2020-12-22 20:36:15'),
(499, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 87.0.4280.88', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 'Windows 10', '2020-12-22 22:47:17'),
(500, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '117.230.132.177', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2020-12-22 23:15:16'),
(501, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '117.230.130.18', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2020-12-23 02:54:39'),
(502, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.105.120.31', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2020-12-23 08:36:37'),
(503, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 87.0.4280.88', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 'Windows 10', '2020-12-23 08:56:33'),
(504, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.190.11.118', 'Chrome 87.0.4280.88', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 'Windows 10', '2020-12-23 17:09:36'),
(505, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 87.0.4280.88', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 'Windows 10', '2020-12-24 04:26:31'),
(506, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 87.0.4280.88', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 'Windows 10', '2020-12-24 07:53:03'),
(507, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 87.0.4280.88', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 'Windows 10', '2020-12-24 08:25:15'),
(508, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.105.125.15', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2020-12-24 08:35:42'),
(509, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.190.11.118', 'Chrome 87.0.4280.88', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 'Windows 10', '2020-12-25 00:13:25'),
(510, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.105.116.145', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2020-12-25 02:26:18'),
(511, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.8.126', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2020-12-25 11:18:12'),
(512, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.236.152', 'Chrome 87.0.4280.88', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 'Windows 10', '2020-12-26 09:21:00'),
(513, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.190.11.118', 'Chrome 87.0.4280.88', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 'Windows 10', '2020-12-28 20:46:56'),
(514, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.15.200', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-12-30 02:15:27'),
(515, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.15.200', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-12-30 02:37:43'),
(516, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.15.200', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2021-01-05 08:53:58'),
(517, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.15.200', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2021-01-05 09:08:06'),
(518, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '27.59.114.37', 'Chrome 71.0.3578.141', 'Mozilla/5.0 (Linux; U; Android 7.0; en-in; Redmi Note 4 Build/NRD90M) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/71.0.3578.141 Mobile Safari/537.36 XiaoMi/MiuiBrowser/12.2.6-g', 'Android', '2021-01-05 09:08:12'),
(519, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '27.59.114.37', 'Chrome 87.0.4280.88', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 'Mac OS X', '2021-01-05 09:09:58'),
(520, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '27.59.114.37', 'Chrome 87.0.4280.88', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 'Mac OS X', '2021-01-05 09:31:34'),
(521, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '27.59.114.37', 'Chrome 87.0.4280.88', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 'Mac OS X', '2021-01-05 09:43:21'),
(522, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '27.59.114.37', 'Chrome 87.0.4280.88', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 'Mac OS X', '2021-01-05 09:52:04'),
(523, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.15.200', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2021-01-05 09:55:06'),
(524, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '1.39.138.92', 'Chrome 87.0.4280.101', 'Mozilla/5.0 (Linux; Android 7.0; Redmi Note 4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.101 Mobile Safari/537.36', 'Android', '2021-01-05 09:55:22'),
(525, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '27.59.114.37', 'Chrome 87.0.4280.88', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 'Mac OS X', '2021-01-05 09:59:39'),
(526, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '27.59.114.37', 'Chrome 87.0.4280.88', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 'Mac OS X', '2021-01-05 10:01:25'),
(527, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.190.11.118', 'Chrome 83.0.4103.106', 'Mozilla/5.0 (Linux; Android 10; SAMSUNG SM-N960F) AppleWebKit/537.36 (KHTML, like Gecko) SamsungBrowser/13.0 Chrome/83.0.4103.106 Mobile Safari/537.36', 'Android', '2021-01-05 15:25:37'),
(528, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.190.11.118', 'Chrome 87.0.4280.88', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 'Windows 7', '2021-01-05 18:59:27'),
(529, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.15.200', 'Safari 604.1', 'Mozilla/5.0 (iPhone; CPU iPhone OS 14_2 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14.0.1 Mobile/15E148 Safari/604.1', 'iOS', '2021-01-05 19:10:32'),
(530, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.15.200', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2021-01-05 20:15:31'),
(531, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.190.11.118', 'Chrome 87.0.4280.88', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 'Windows 7', '2021-01-05 20:27:32'),
(532, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.15.200', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2021-01-05 20:28:59'),
(533, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.151', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2021-01-09 10:05:27'),
(534, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.15.129', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2021-01-10 22:52:46'),
(535, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.190.11.118', 'Chrome 87.0.4280.141', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36', 'Windows 10', '2021-01-11 20:51:49'),
(536, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.190.11.118', 'Chrome 87.0.4280.141', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36', 'Windows 10', '2021-01-11 23:21:52'),
(537, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.15.129', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2021-01-12 01:36:33'),
(538, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.15.129', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2021-01-12 03:04:52'),
(539, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.15.129', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2021-01-12 03:05:17'),
(540, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.195.187', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2021-01-12 05:07:49'),
(541, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.15.129', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2021-01-12 08:43:06'),
(542, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.15.129', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2021-01-12 08:43:07');
INSERT INTO `tbl_last_login` (`id`, `userId`, `sessionData`, `machineIp`, `userAgent`, `agentString`, `platform`, `createdDtm`) VALUES
(543, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.222.10', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2021-01-12 11:17:50'),
(544, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.15.129', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2021-01-12 19:53:26'),
(545, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.212.92', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2021-01-12 20:30:02'),
(546, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.15.129', 'Chrome 87.0.4280.141', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36', 'Windows 10', '2021-01-12 21:36:59'),
(547, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.190.11.118', 'Chrome 87.0.4280.141', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36', 'Windows 10', '2021-01-12 22:27:03'),
(548, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.178.184', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2021-01-13 01:00:45'),
(549, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.15.129', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2021-01-13 01:40:49'),
(550, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.180.227', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2021-01-13 10:04:48'),
(551, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.190.11.118', 'Chrome 87.0.4280.141', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36', 'Windows 10', '2021-01-13 16:52:41'),
(552, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.190.11.118', 'Chrome 87.0.4280.141', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36', 'Windows 10', '2021-01-13 22:40:09'),
(553, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.81.150', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2021-01-14 06:14:43'),
(554, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.15.129', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2021-01-14 11:24:10'),
(555, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.15.129', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2021-01-14 12:43:23'),
(556, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.190.11.118', 'Chrome 87.0.4280.141', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36', 'Windows 10', '2021-01-14 19:15:54'),
(557, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.15.129', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2021-01-14 19:17:42'),
(558, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.190.11.118', 'Chrome 87.0.4280.141', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36', 'Windows 10', '2021-01-14 19:32:00'),
(559, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.15.129', 'Chrome 87.0.4280.141', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36', 'Windows 10', '2021-01-14 21:01:42'),
(560, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.15.129', 'Chrome 87.0.4280.141', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36', 'Windows 10', '2021-01-14 23:06:57'),
(561, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.86.132', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2021-01-14 23:52:32'),
(562, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.190.11.118', 'Chrome 87.0.4280.141', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36', 'Windows 10', '2021-01-14 23:58:44'),
(563, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.67.119', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2021-01-15 07:09:37'),
(564, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.15.129', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2021-01-15 09:56:31'),
(565, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '117.230.21.197', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2021-01-15 10:54:09'),
(566, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.190.11.118', 'Chrome 87.0.4280.141', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36', 'Windows 10', '2021-01-15 11:29:31'),
(567, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.190.11.118', 'Chrome 87.0.4280.141', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36', 'Windows 10', '2021-01-16 00:35:09'),
(568, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.190.11.118', 'Chrome 87.0.4280.141', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36', 'Windows 10', '2021-01-16 07:49:47'),
(569, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.15.129', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2021-01-16 12:40:36'),
(570, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.15.129', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2021-01-16 21:34:11'),
(571, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.81.244', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2021-01-17 00:40:00'),
(572, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.190.11.118', 'Chrome 87.0.4280.141', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36', 'Windows 10', '2021-01-17 01:08:05'),
(573, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.15.129', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2021-01-17 01:46:48'),
(574, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.190.11.118', 'Chrome 87.0.4280.141', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36', 'Windows 10', '2021-01-17 04:06:19'),
(575, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.78.7', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2021-01-17 04:53:06'),
(576, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.15.129', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2021-01-17 09:10:42'),
(577, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.15.129', 'Chrome 87.0.4280.141', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36', 'Windows 10', '2021-01-17 17:52:52'),
(578, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.190.11.118', 'Chrome 87.0.4280.141', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36', 'Windows 10', '2021-01-17 19:36:13'),
(579, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.15.129', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2021-01-17 22:18:03'),
(580, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.190.11.118', 'Chrome 87.0.4280.141', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36', 'Windows 10', '2021-01-17 22:25:22'),
(581, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.15.129', 'Chrome 87.0.4280.141', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36', 'Windows 10', '2021-01-18 08:48:52'),
(582, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.15.129', 'Chrome 87.0.4280.141', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36', 'Windows 10', '2021-01-18 09:27:44'),
(583, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.91.198', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2021-01-18 09:35:54'),
(584, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '127.0.0.1', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2021-01-19 17:19:28'),
(585, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '127.0.0.1', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2021-01-19 20:28:24'),
(586, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '127.0.0.1', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2021-01-19 20:51:11'),
(587, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '127.0.0.1', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2021-01-20 23:07:38'),
(588, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '127.0.0.1', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2021-01-21 11:39:21'),
(589, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '127.0.0.1', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2021-01-21 22:00:14'),
(590, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '127.0.0.1', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2021-01-21 22:09:24'),
(591, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '127.0.0.1', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2021-01-21 22:47:14'),
(592, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '127.0.0.1', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2021-01-22 11:41:09'),
(593, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '127.0.0.1', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2021-01-22 13:23:39'),
(594, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '127.0.0.1', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2021-01-22 21:31:42'),
(595, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '127.0.0.1', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2021-01-23 00:17:37'),
(596, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '127.0.0.1', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2021-01-23 14:40:46'),
(597, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '127.0.0.1', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2021-01-23 22:18:45'),
(598, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '127.0.0.1', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2021-01-25 15:19:20'),
(599, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '127.0.0.1', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2021-01-25 19:18:27'),
(600, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '127.0.0.1', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2021-01-26 11:41:42'),
(601, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '127.0.0.1', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2021-01-26 20:45:44'),
(602, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '127.0.0.1', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2021-01-27 12:19:48'),
(603, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '127.0.0.1', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2021-01-28 11:58:35'),
(604, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '127.0.0.1', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2021-01-28 15:48:50'),
(605, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '127.0.0.1', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2021-01-29 16:49:22'),
(606, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '127.0.0.1', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2021-01-29 22:55:53'),
(607, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '127.0.0.1', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2021-01-31 10:49:12'),
(608, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '127.0.0.1', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2021-02-03 11:31:06'),
(609, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '127.0.0.1', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2021-02-08 13:19:52'),
(610, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '127.0.0.1', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2021-02-09 18:28:54'),
(611, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '127.0.0.1', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2021-02-11 11:22:42'),
(612, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '127.0.0.1', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2021-02-11 18:00:54'),
(613, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '127.0.0.1', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2021-02-15 19:50:52'),
(614, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '106.217.34.231', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2021-03-01 21:38:49'),
(615, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.184.25', 'Chrome 88.0.4324.181', 'Mozilla/5.0 (Linux; Android 11; POCO X2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.181 Mobile Safari/537.36', 'Android', '2021-03-01 21:49:04'),
(616, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.176.32', 'Chrome 88.0.4324.190', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Safari/537.36', 'Windows 10', '2021-03-02 19:59:50'),
(617, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2021-03-24 12:17:15'),
(618, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.10.161', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2021-04-11 22:53:13'),
(619, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.24.130', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '2021-05-04 01:33:28');

-- --------------------------------------------------------

--
-- Table structure for table `temp_sample_booking_details`
--

CREATE TABLE `temp_sample_booking_details` (
  `id` bigint(20) NOT NULL,
  `id_session` varchar(2048) DEFAULT '',
  `id_sample` int(20) DEFAULT 0,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `temp_sample_booking_details`
--

INSERT INTO `temp_sample_booking_details` (`id`, `id_session`, `id_sample`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(7, 'ca77405fb9985b3129f87529fccd426d', 2, 1, NULL, '2021-02-08 14:00:29', NULL, '2021-02-08 14:00:29'),
(8, 'c3a8ff58bd619126a6b0352ab459d582', 2, 1, NULL, '2021-03-24 12:18:28', NULL, '2021-03-24 12:18:28'),
(9, 'b6bbc4f99070449c4bd7c8a433d9c726', 2, 1, NULL, '2021-04-11 22:53:41', NULL, '2021-04-11 22:53:41');

-- --------------------------------------------------------

--
-- Table structure for table `temp_test_booking_details`
--

CREATE TABLE `temp_test_booking_details` (
  `id` bigint(20) NOT NULL,
  `id_session` varchar(2048) DEFAULT '',
  `booking_type` varchar(2048) DEFAULT '',
  `id_test` int(20) DEFAULT 0,
  `id_package` int(20) DEFAULT 0,
  `amount` float(20,2) DEFAULT 0.00,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `temp_test_booking_details`
--

INSERT INTO `temp_test_booking_details` (`id`, `id_session`, `booking_type`, `id_test`, `id_package`, `amount`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(8, 'ca77405fb9985b3129f87529fccd426d', 'Individual', 3, 0, 500.00, 1, NULL, '2021-02-08 14:00:24', NULL, '2021-02-08 14:00:24'),
(10, 'b6bbc4f99070449c4bd7c8a433d9c726', 'Package', 0, 2, 1000.00, 1, NULL, '2021-04-11 22:53:35', NULL, '2021-04-11 22:53:35');

-- --------------------------------------------------------

--
-- Table structure for table `temp_test_booking_patients`
--

CREATE TABLE `temp_test_booking_patients` (
  `id` bigint(20) NOT NULL,
  `id_session` varchar(512) DEFAULT '',
  `id_patient` int(2) DEFAULT 1,
  `full_name` varchar(512) DEFAULT '',
  `gender` varchar(512) DEFAULT '',
  `age` varchar(512) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` varchar(256) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `temp_test_booking_patients`
--

INSERT INTO `temp_test_booking_patients` (`id`, `id_session`, `id_patient`, `full_name`, `gender`, `age`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(2, 'efb4f00b416cc7277a6cdeb2d7d4a71a', 1, 'Name', 'Male', '23', 1, NULL, '2021-05-10 12:26:50', NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `test`
--

CREATE TABLE `test` (
  `id` bigint(20) NOT NULL,
  `name` varchar(2048) DEFAULT '',
  `code` varchar(512) DEFAULT '',
  `amount` float(20,2) DEFAULT 0.00,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `test`
--

INSERT INTO `test` (`id`, `name`, `code`, `amount`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Caratine Test (BLOOD)', 'CT', 120.00, 1, NULL, '2021-01-22 13:29:52', NULL, '2021-01-22 13:29:52'),
(2, 'Albumin', 'AB', 480.00, 1, NULL, '2021-01-23 22:29:44', NULL, '2021-01-23 22:29:44'),
(3, 'Sugar Glucone', 'SG', 500.00, 1, NULL, '2021-02-03 11:32:44', NULL, '2021-02-03 11:32:44'),
(4, 'LFT', 'LFT', 500.00, 1, NULL, '2021-02-08 13:23:06', NULL, '2021-02-08 13:23:06'),
(5, 'Fasting Blood Sugar', 'FBS', 100.00, 1, NULL, '2021-02-11 11:25:58', NULL, '2021-02-11 11:25:58');

-- --------------------------------------------------------

--
-- Table structure for table `test_booking`
--

CREATE TABLE `test_booking` (
  `id` bigint(20) NOT NULL,
  `reference_number` varchar(256) DEFAULT '',
  `id_patient` int(20) DEFAULT 0,
  `id_staff` int(20) DEFAULT 0,
  `description` varchar(2048) DEFAULT '',
  `time_slot` varchar(200) DEFAULT '',
  `appointment_date` varchar(200) DEFAULT '',
  `completed_date` varchar(512) DEFAULT '',
  `amount` float(20,2) DEFAULT 0.00,
  `paid_amount` float(20,2) DEFAULT 0.00,
  `payment_mode` varchar(512) DEFAULT '',
  `payment_status` int(20) DEFAULT 0,
  `comments` varchar(2048) DEFAULT '',
  `reason` varchar(2048) DEFAULT '',
  `added_by_patient` int(20) DEFAULT 0,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `test_booking`
--

INSERT INTO `test_booking` (`id`, `reference_number`, `id_patient`, `id_staff`, `description`, `time_slot`, `appointment_date`, `completed_date`, `amount`, `paid_amount`, `payment_mode`, `payment_status`, `comments`, `reason`, `added_by_patient`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(7, '3501054926012021', 1, 1, '', '6:00 - 6:30', '01/29/2021', '', 3120.00, 0.00, '', 0, '', '', 0, 0, 1, '2021-01-26 01:05:49', NULL, '2021-01-26 01:05:49'),
(8, '4601512026012021', 1, 4, '', '8:00 - 8:30', '01/30/2021', '2021-01-26', 830.00, 830.00, 'Cash', 0, 'DASDASDa', '', 0, 1, 1, '2021-01-26 13:51:20', 1, '2021-01-26 13:51:20'),
(9, '8411375903022021', 3, 3, '', '7:00 - 7:30', '02/04/2021', '2021-02-03 11:39:18', 900.00, 900.00, 'UPI', 0, 'IADNNDIAF12341231 Done', '', 0, 1, 1, '2021-02-03 11:37:59', 1, '2021-02-03 11:37:59'),
(10, '9401333608022021', 4, 5, '', '7:00 - 7:30', '02/10/2021', '2021-02-08 13:36:30', 930.00, 930.00, 'Cash', 0, 'Avinash Taken', '', 0, 1, 1, '2021-02-08 13:33:36', 1, '2021-02-08 13:33:36'),
(11, '2511345911022021', 5, 5, '', '7:00 - 7:30', '02/12/2021', '2021-02-11 11:37:07', 1100.00, 1100.00, 'UPI', 0, 'Google Pay  421432131414', '', 0, 1, 1, '2021-02-11 11:34:59', 1, '2021-02-11 11:34:59'),
(12, '4812565811052021', 6, 0, '<p>Data</p>\r\n\r\n<p><strong>Ndnbs</strong></p>\r\n', '', '', '', 0.00, 0.00, '', 0, '', '', 6, 0, 0, '2021-05-10 12:26:58', NULL, '2021-05-10 12:26:58'),
(13, '5306285511052021', 7, 0, 'Test Desccription', '', '', '', 0.00, 0.00, '', 0, '', '', 7, 0, 0, '2021-05-11 05:58:55', NULL, '2021-05-11 05:58:55'),
(14, '2907105111052021', 8, 0, 'CBC ESR CRP FERRITINE D-Dimer', '', '', '', 0.00, 0.00, '', 0, '', '', 8, 0, 0, '2021-05-11 06:40:51', NULL, '2021-05-11 06:40:51');

-- --------------------------------------------------------

--
-- Table structure for table `test_booking_details`
--

CREATE TABLE `test_booking_details` (
  `id` bigint(20) NOT NULL,
  `id_test_booking` varchar(2048) DEFAULT '',
  `booking_type` varchar(2048) DEFAULT '',
  `id_test` int(20) DEFAULT 0,
  `id_package` int(20) DEFAULT 0,
  `amount` float(20,2) DEFAULT 0.00,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `test_booking_details`
--

INSERT INTO `test_booking_details` (`id`, `id_test_booking`, `booking_type`, `id_test`, `id_package`, `amount`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(8, '6', 'Package', 0, 2, 3000.00, 1, NULL, '2021-01-26 00:37:13', NULL, '2021-01-26 00:37:13'),
(9, '6', 'Individual', 1, 0, 120.00, 1, NULL, '2021-01-26 00:37:17', NULL, '2021-01-26 00:37:17'),
(10, '7', 'Package', 0, 2, 3000.00, 1, NULL, '2021-01-26 01:05:31', NULL, '2021-01-26 01:05:31'),
(11, '7', 'Individual', 1, 0, 120.00, 1, NULL, '2021-01-26 01:05:36', NULL, '2021-01-26 01:05:36'),
(12, '8', 'Package', 0, 2, 710.00, 1, NULL, '2021-01-26 13:50:19', NULL, '2021-01-26 13:50:19'),
(13, '8', 'Individual', 1, 0, 120.00, 1, NULL, '2021-01-26 13:50:23', NULL, '2021-01-26 13:50:23'),
(14, '9', 'Individual', 3, 0, 500.00, 1, NULL, '2021-02-03 11:36:49', NULL, '2021-02-03 11:36:49'),
(15, '9', 'Package', 0, 3, 400.00, 1, NULL, '2021-02-03 11:36:58', NULL, '2021-02-03 11:36:58'),
(16, '10', 'Package', 0, 3, 430.00, 1, NULL, '2021-02-08 13:31:35', NULL, '2021-02-08 13:31:35'),
(17, '10', 'Individual', 4, 0, 500.00, 1, NULL, '2021-02-08 13:31:42', NULL, '2021-02-08 13:31:42'),
(18, '11', 'Package', 0, 2, 1000.00, 1, NULL, '2021-02-11 11:31:48', NULL, '2021-02-11 11:31:48'),
(19, '11', 'Individual', 5, 0, 100.00, 1, NULL, '2021-02-11 11:32:04', NULL, '2021-02-11 11:32:04');

-- --------------------------------------------------------

--
-- Table structure for table `test_booking_patients`
--

CREATE TABLE `test_booking_patients` (
  `id` bigint(20) NOT NULL,
  `id_test_booking` int(10) DEFAULT 0,
  `id_patient` int(2) DEFAULT 1,
  `full_name` varchar(512) DEFAULT '',
  `gender` varchar(512) DEFAULT '',
  `age` varchar(512) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` varchar(256) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `test_booking_upload_files`
--

CREATE TABLE `test_booking_upload_files` (
  `id` int(20) NOT NULL,
  `id_test_booking` int(20) DEFAULT 0,
  `upload_file` varchar(512) DEFAULT '',
  `comments` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `test_booking_upload_files`
--

INSERT INTO `test_booking_upload_files` (`id`, `id_test_booking`, `upload_file`, `comments`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 8, 'de040f5413059c3b480cca4eda15d232.png', '', 1, 1, '2021-01-26 23:43:41', NULL, '2021-01-26 23:43:41'),
(2, 10, '9606ba0f6d5d6c5459f9a0472c0f478b.jpeg', 'Uploaded Document', 1, 1, '2021-02-08 13:37:10', NULL, '2021-02-08 13:37:10'),
(3, 11, '4a1e2980f4e2f1c27e13ebfde1bad103.jpeg', 'Report Generated', 1, 1, '2021-02-11 11:38:20', NULL, '2021-02-11 11:38:20');

-- --------------------------------------------------------

--
-- Table structure for table `unit`
--

CREATE TABLE `unit` (
  `id` int(20) NOT NULL,
  `name` varchar(250) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `email` varchar(128) NOT NULL COMMENT 'login email',
  `password` varchar(128) NOT NULL COMMENT 'hashed login password',
  `name` varchar(128) DEFAULT NULL COMMENT 'full name of user',
  `mobile` varchar(20) DEFAULT NULL,
  `role_id` tinyint(4) NOT NULL,
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  `created_by` int(11) NOT NULL,
  `created_dt_tm` datetime NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `name`, `mobile`, `role_id`, `is_deleted`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'admin@gmail.com', '$2y$10$YykrXgnhZ563BCiC6L2ee.Kktd/Vj/KncTnTeDZQXCWwAQ1xH4Req', 'Administrator', '9890098901', 1, 0, 0, '2015-07-01 18:56:49', 1, '2020-01-30 01:48:49'),
(10, 'permissions@cms.com', '$2y$10$IGMpNysy12MZbrSP50tYsuJCAIQH/N8f3VaqsafRP9Ip58W9pMU7.', 'Permission Assigner', '479123491', 14, 0, 1, '2020-10-31 11:34:16', 1, '2020-10-31 11:52:07'),
(11, 'aeu@cms.com', '$2y$10$OnVsw.8CIhaASVmdYwM/yuyviFmFEv3wtwkuBdcBamgu0d6EIBm96', 'Aeu Administrator', '88888888', 15, 0, 1, '2020-11-13 02:26:28', NULL, NULL),
(12, 'aeu.exam@cms.com', '$2y$10$hzMxYRcvHWlR/wAjsg/3huBzKx7Adc82ETZ2kz6WKfpokx0mQxce.', 'Aeu Examination Admin', '88889999', 16, 0, 1, '2020-11-13 02:27:06', NULL, NULL),
(13, 'cu@cms.com', '$2y$10$3DbqlCzfYPHaV7UyBD/Yy.qPYEE1/TS8b6CH4FFvRaQ6KDXUFC5Ii', 'Colloquium User', '888888888', 17, 0, 1, '2020-11-25 23:07:33', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `visa_details`
--

CREATE TABLE `visa_details` (
  `id` int(20) NOT NULL,
  `id_student` int(10) DEFAULT NULL,
  `malaysian_visa` varchar(120) DEFAULT '',
  `visa_number` varchar(200) DEFAULT '',
  `visa_expiry_date` varchar(120) DEFAULT '',
  `visa_status` varchar(120) DEFAULT '',
  `status` int(20) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `visa_details`
--

INSERT INTO `visa_details` (`id`, `id_student`, `malaysian_visa`, `visa_number`, `visa_expiry_date`, `visa_status`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, '', '', '', '', NULL, NULL, '2020-07-11 01:20:44', NULL, '2020-07-11 01:20:44'),
(2, 1, '', '', '', '', NULL, NULL, '2020-07-11 06:25:02', NULL, '2020-07-11 06:25:02'),
(3, 2, '', '', '', '', NULL, NULL, '2020-07-11 11:53:53', NULL, '2020-07-11 11:53:53'),
(4, 3, '', '', '', '', NULL, NULL, '2020-07-11 12:55:29', NULL, '2020-07-11 12:55:29'),
(5, 4, '', '', '', '', NULL, NULL, '2020-07-11 13:15:32', NULL, '2020-07-11 13:15:32'),
(7, 5, 'Yes', '128932726', '2020-08-29', 'Active Visa', NULL, NULL, '2020-08-06 07:56:06', NULL, '2020-08-06 07:56:06'),
(8, 5, '', '', '1970-01-01', 'Active Visa', NULL, NULL, '2020-08-06 07:59:36', NULL, '2020-08-06 07:59:36'),
(13, 6, 'Yes', 'w3432432', '2020-08-31', 'Active Visa', NULL, NULL, '2020-08-12 10:06:41', NULL, '2020-08-12 10:06:41'),
(14, 6, '', '', '1970-01-01', 'Active Visa', NULL, NULL, '2020-08-12 10:07:24', NULL, '2020-08-12 10:07:24'),
(15, 13, 'Yes', 'NADSA12313', '2020-08-18', 'Active', NULL, NULL, '2020-08-30 14:18:24', NULL, '2020-08-30 14:18:24'),
(16, 27, 'Yes', '123', '2020-10-04', 'Active', NULL, NULL, '2020-10-09 00:45:59', NULL, '2020-10-09 00:45:59'),
(17, 28, 'Yes', '123231', '2022-10-01', 'Active Visa', NULL, NULL, '2020-10-10 01:55:31', NULL, '2020-10-10 01:55:31'),
(18, 28, 'Yes', '2123DHASJD', '2021-10-17', 'Active Visa', NULL, NULL, '2020-10-10 02:00:27', NULL, '2020-10-10 02:00:27'),
(19, 29, 'Yes', '3213GGA', '2021-10-29', 'Active Visa', NULL, NULL, '2020-10-10 02:01:39', NULL, '2020-10-10 02:01:39'),
(20, 29, 'Yes', 'ASDJKA768', '2022-10-29', 'Active Visa', NULL, NULL, '2020-10-10 02:01:57', NULL, '2020-10-10 02:01:57'),
(21, 31, 'Yes', 'DASDMA123', '2022-11-28', 'Active Visa', NULL, NULL, '2020-11-08 11:47:23', NULL, '2020-11-08 11:47:23'),
(22, 35, 'Yes', 'VISA121312313', '2020-11-26', 'Active Visa', NULL, NULL, '2020-11-29 22:53:06', NULL, '2020-11-29 22:53:06'),
(23, 35, 'Yes', 'VISA', '2022-11-26', 'Active Visa', NULL, NULL, '2020-11-29 22:56:47', NULL, '2020-11-29 22:56:47'),
(24, 33, 'Yes', 'VABD123211', '2019-11-01', 'Active Visa', NULL, NULL, '2020-11-29 23:05:11', NULL, '2020-11-29 23:05:11'),
(25, 33, 'Yes', 'VADAGA122121', '2022-01-31', 'Active Visa', NULL, NULL, '2020-11-29 23:05:31', NULL, '2020-11-29 23:05:31');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `applicant`
--
ALTER TABLE `applicant`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `applicant_status`
--
ALTER TABLE `applicant_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `award`
--
ALTER TABLE `award`
  ADD PRIMARY KEY (`id`),
  ADD KEY `updated_by` (`updated_by`),
  ADD KEY `created_by` (`created_by`);

--
-- Indexes for table `change_status`
--
ALTER TABLE `change_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `communication_group`
--
ALTER TABLE `communication_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `communication_group_message`
--
ALTER TABLE `communication_group_message`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `communication_group_message_recepients`
--
ALTER TABLE `communication_group_message_recepients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `communication_group_recepients`
--
ALTER TABLE `communication_group_recepients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `communication_template`
--
ALTER TABLE `communication_template`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `communication_template_message`
--
ALTER TABLE `communication_template_message`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `currency_rate_setup`
--
ALTER TABLE `currency_rate_setup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `currency_setup`
--
ALTER TABLE `currency_setup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `documents`
--
ALTER TABLE `documents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `documents_program`
--
ALTER TABLE `documents_program`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `documents_program_details`
--
ALTER TABLE `documents_program_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `document_checklist`
--
ALTER TABLE `document_checklist`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `equipment`
--
ALTER TABLE `equipment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `file_type`
--
ALTER TABLE `file_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `main_invoice`
--
ALTER TABLE `main_invoice`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_application` (`id_application`),
  ADD KEY `id_student` (`id_student`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `main_invoice_details`
--
ALTER TABLE `main_invoice_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_main_invoice` (`id_main_invoice`),
  ADD KEY `id_fee_item` (`id_fee_item`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `main_invoice_discount_details`
--
ALTER TABLE `main_invoice_discount_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `marital_status_setup`
--
ALTER TABLE `marital_status_setup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nationality`
--
ALTER TABLE `nationality`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `online_claim`
--
ALTER TABLE `online_claim`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `online_claim_details`
--
ALTER TABLE `online_claim_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `organisation`
--
ALTER TABLE `organisation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `organisation_comitee`
--
ALTER TABLE `organisation_comitee`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `package`
--
ALTER TABLE `package`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `package_has_test`
--
ALTER TABLE `package_has_test`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `patient`
--
ALTER TABLE `patient`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_rate`
--
ALTER TABLE `payment_rate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_type`
--
ALTER TABLE `payment_type`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profile_details`
--
ALTER TABLE `profile_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `race_setup`
--
ALTER TABLE `race_setup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `religion_setup`
--
ALTER TABLE `religion_setup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_permissions`
--
ALTER TABLE `role_permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `salutation_setup`
--
ALTER TABLE `salutation_setup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sample`
--
ALTER TABLE `sample`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sample_booking_details`
--
ALTER TABLE `sample_booking_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `staff`
--
ALTER TABLE `staff`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `staff_bank_details`
--
ALTER TABLE `staff_bank_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `staff_change_status_details`
--
ALTER TABLE `staff_change_status_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `staff_has_course`
--
ALTER TABLE `staff_has_course`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_staff` (`id_staff`),
  ADD KEY `id_course` (`id_course`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `staff_leave_records`
--
ALTER TABLE `staff_leave_records`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `staff_status`
--
ALTER TABLE `staff_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `staff_teaching_details`
--
ALTER TABLE `staff_teaching_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stage_semester`
--
ALTER TABLE `stage_semester`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `state`
--
ALTER TABLE `state`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_bca`
--
ALTER TABLE `student_bca`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `aadhar` (`aadhar`);

--
-- Indexes for table `student_deliverable_history`
--
ALTER TABLE `student_deliverable_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_duration_history`
--
ALTER TABLE `student_duration_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_emergency_contact_details`
--
ALTER TABLE `student_emergency_contact_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_has_document`
--
ALTER TABLE `student_has_document`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_insurance_details`
--
ALTER TABLE `student_insurance_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_last_login`
--
ALTER TABLE `student_last_login`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_note`
--
ALTER TABLE `student_note`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tax`
--
ALTER TABLE `tax`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_last_login`
--
ALTER TABLE `tbl_last_login`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_sample_booking_details`
--
ALTER TABLE `temp_sample_booking_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_test_booking_details`
--
ALTER TABLE `temp_test_booking_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_test_booking_patients`
--
ALTER TABLE `temp_test_booking_patients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `test`
--
ALTER TABLE `test`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `test_booking`
--
ALTER TABLE `test_booking`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `test_booking_details`
--
ALTER TABLE `test_booking_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `test_booking_patients`
--
ALTER TABLE `test_booking_patients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `test_booking_upload_files`
--
ALTER TABLE `test_booking_upload_files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `unit`
--
ALTER TABLE `unit`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `visa_details`
--
ALTER TABLE `visa_details`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `applicant`
--
ALTER TABLE `applicant`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=140;

--
-- AUTO_INCREMENT for table `applicant_status`
--
ALTER TABLE `applicant_status`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `award`
--
ALTER TABLE `award`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `change_status`
--
ALTER TABLE `change_status`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `communication_group`
--
ALTER TABLE `communication_group`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `communication_group_message`
--
ALTER TABLE `communication_group_message`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `communication_group_message_recepients`
--
ALTER TABLE `communication_group_message_recepients`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `communication_group_recepients`
--
ALTER TABLE `communication_group_recepients`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `communication_template`
--
ALTER TABLE `communication_template`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `communication_template_message`
--
ALTER TABLE `communication_template_message`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=207;

--
-- AUTO_INCREMENT for table `currency_rate_setup`
--
ALTER TABLE `currency_rate_setup`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `currency_setup`
--
ALTER TABLE `currency_setup`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `department`
--
ALTER TABLE `department`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `documents`
--
ALTER TABLE `documents`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `documents_program`
--
ALTER TABLE `documents_program`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `documents_program_details`
--
ALTER TABLE `documents_program_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `document_checklist`
--
ALTER TABLE `document_checklist`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `equipment`
--
ALTER TABLE `equipment`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `file_type`
--
ALTER TABLE `file_type`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `main_invoice`
--
ALTER TABLE `main_invoice`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;

--
-- AUTO_INCREMENT for table `main_invoice_details`
--
ALTER TABLE `main_invoice_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=129;

--
-- AUTO_INCREMENT for table `main_invoice_discount_details`
--
ALTER TABLE `main_invoice_discount_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `marital_status_setup`
--
ALTER TABLE `marital_status_setup`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=211;

--
-- AUTO_INCREMENT for table `nationality`
--
ALTER TABLE `nationality`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=233;

--
-- AUTO_INCREMENT for table `online_claim`
--
ALTER TABLE `online_claim`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `online_claim_details`
--
ALTER TABLE `online_claim_details`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `organisation`
--
ALTER TABLE `organisation`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `organisation_comitee`
--
ALTER TABLE `organisation_comitee`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `package`
--
ALTER TABLE `package`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `package_has_test`
--
ALTER TABLE `package_has_test`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `patient`
--
ALTER TABLE `patient`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `payment_rate`
--
ALTER TABLE `payment_rate`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `payment_type`
--
ALTER TABLE `payment_type`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=573;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `profile_details`
--
ALTER TABLE `profile_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `race_setup`
--
ALTER TABLE `race_setup`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `religion_setup`
--
ALTER TABLE `religion_setup`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `role_permissions`
--
ALTER TABLE `role_permissions`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6183;

--
-- AUTO_INCREMENT for table `salutation_setup`
--
ALTER TABLE `salutation_setup`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `sample`
--
ALTER TABLE `sample`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `sample_booking_details`
--
ALTER TABLE `sample_booking_details`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `staff`
--
ALTER TABLE `staff`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `staff_bank_details`
--
ALTER TABLE `staff_bank_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `staff_change_status_details`
--
ALTER TABLE `staff_change_status_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `staff_has_course`
--
ALTER TABLE `staff_has_course`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `staff_leave_records`
--
ALTER TABLE `staff_leave_records`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `staff_status`
--
ALTER TABLE `staff_status`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `staff_teaching_details`
--
ALTER TABLE `staff_teaching_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `stage_semester`
--
ALTER TABLE `stage_semester`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `state`
--
ALTER TABLE `state`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=223;

--
-- AUTO_INCREMENT for table `student`
--
ALTER TABLE `student`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=80;

--
-- AUTO_INCREMENT for table `student_bca`
--
ALTER TABLE `student_bca`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `student_deliverable_history`
--
ALTER TABLE `student_deliverable_history`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT for table `student_duration_history`
--
ALTER TABLE `student_duration_history`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `student_emergency_contact_details`
--
ALTER TABLE `student_emergency_contact_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `student_has_document`
--
ALTER TABLE `student_has_document`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `student_insurance_details`
--
ALTER TABLE `student_insurance_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `student_last_login`
--
ALTER TABLE `student_last_login`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT for table `student_note`
--
ALTER TABLE `student_note`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tax`
--
ALTER TABLE `tax`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_last_login`
--
ALTER TABLE `tbl_last_login`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=620;

--
-- AUTO_INCREMENT for table `temp_sample_booking_details`
--
ALTER TABLE `temp_sample_booking_details`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `temp_test_booking_details`
--
ALTER TABLE `temp_test_booking_details`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `temp_test_booking_patients`
--
ALTER TABLE `temp_test_booking_patients`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `test`
--
ALTER TABLE `test`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `test_booking`
--
ALTER TABLE `test_booking`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `test_booking_details`
--
ALTER TABLE `test_booking_details`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `test_booking_patients`
--
ALTER TABLE `test_booking_patients`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `test_booking_upload_files`
--
ALTER TABLE `test_booking_upload_files`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `unit`
--
ALTER TABLE `unit`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `visa_details`
--
ALTER TABLE `visa_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
