


ALTER TABLE `student`  ADD `country_code` VARCHAR(50) NULL AFTER `id_type`;


ALTER TABLE `fee_structure_master` ADD `description` VARCHAR(2048) NULL DEFAULT '' AFTER `code`;

ALTER TABLE `scholarship_individual_entry_requirement` ADD `entry_type` VARCHAR(50) NULL AFTER `updated_dt_tm`;

UPDATE `scholarship_individual_entry_requirement` SET `entry_type` = 'ENTRY'

ALTER TABLE `applicant` ADD `profile_pic` VARCHAR(1024) NULL DEFAULT 'default_profile.jpg' AFTER `ig_id`;

ALTER TABLE `student` ADD `profile_pic` VARCHAR(1024) NULL DEFAULT 'default_profile.jpg' AFTER `ig_id`;


ALTER TABLE `file_type` CHANGE `file_type_name` `name` VARCHAR(512) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;

ALTER TABLE `file_type` ADD `status` INT(2) NULL DEFAULT '0' AFTER `name`, ADD `created_by` INT(20) NULL AFTER `status`, ADD `created_dt_tm` DATETIME NULL DEFAULT CURRENT_TIMESTAMP AFTER `created_by`, ADD `updated_by` INT(20) NULL AFTER `created_dt_tm`, ADD `updated_dt_tm` DATETIME NULL AFTER `updated_by`;

INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`) VALUES (NULL, 'fileType', 'Setup', 'Document', '1', 'fileType', 'list', '5');

ALTER TABLE `discount_type` ADD `status` INT(2) NULL DEFAULT '0' AFTER `description`, ADD `created_by` INT(20) NULL AFTER `status`, ADD `created_dt_tm` DATETIME NULL DEFAULT CURRENT_TIMESTAMP AFTER `created_by`, ADD `updated_by` INT(20) NULL AFTER `created_dt_tm`, ADD `updated_dt_tm` DATETIME NULL AFTER `updated_by`;

ALTER TABLE `discount` ADD `status` INT(2) NULL DEFAULT '0' AFTER `currency`, ADD `created_by` INT(20) NULL AFTER `status`, ADD `created_dt_tm` DATETIME NULL DEFAULT CURRENT_TIMESTAMP AFTER `created_by`, ADD `updated_by` INT(20) NULL AFTER `created_dt_tm`, ADD `updated_dt_tm` DATETIME NULL AFTER `updated_by`;

CREATE TABLE `program_landscape_learning_mode` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_program_landscape` bigint(20) NOT NULL DEFAULT 0,
  `id_programme` bigint(20) NOT NULL DEFAULT 0,
  `code` varchar(1024) DEFAULT '',
  `name` varchar(2048) DEFAULT '',
  `mode_of_program` varchar(2048) DEFAULT '',
  `mode_of_study` varchar(2048) DEFAULT '',
  `id_program_type` bigint(20) NOT NULL DEFAULT 0,
  `total_semester` bigint(20) NOT NULL DEFAULT 0,
  `min_cr_hrs` bigint(20) NOT NULL DEFAULT 0,
  `max_cr_hrs` bigint(20) NOT NULL DEFAULT 0,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `notification` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `type` varchar(1024) DEFAULT '',
  `name` varchar(2048) DEFAULT '',
  `description` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `event` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `type` varchar(1024) DEFAULT '',
  `name` varchar(2048) DEFAULT '',
  `description` varchar(2048) DEFAULT '',
  `start_date` varchar(2048) DEFAULT '',
  `end_date` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`) VALUES (NULL, 'Notification', 'Communication', 'Alerts', '1', 'notification', 'list', '3');

INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`) VALUES (NULL, 'Events', 'Communication', 'Alerts', '2', 'events', 'list', '3');

CREATE TABLE `test` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(2048) DEFAULT '',
  `code` varchar(512) DEFAULT '',
  `amount` float(20,2) DEFAULT 0,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `sample` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(2048) DEFAULT '',
  `code` varchar(512) DEFAULT '',
  `description` varchar(2042) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `package` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(2048) DEFAULT '',
  `code` varchar(512) DEFAULT '',
  `amount` float(20,2) DEFAULT 0,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




CREATE TABLE `package_has_test` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_package` int(20) DEFAULT 0,
  `id_test` int(20) DEFAULT 0,
  `amount` float(20,2) DEFAULT 0,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE `patient` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `full_name` varchar(580) DEFAULT '',
  `salutation` varchar(120) DEFAULT '',
  `first_name` varchar(120) DEFAULT '',
  `last_name` varchar(120) DEFAULT '',
  `gender` varchar(120) DEFAULT '',
  `register_no` varchar(120) DEFAULT '',
  `passport` varchar(120) DEFAULT '',
  `phone` varchar(120) DEFAULT '',
  `email` varchar(120) DEFAULT '',
  `contact_email` varchar(500) DEFAULT '',
  `password` varchar(120) DEFAULT '',
  `passport_expiry_date` varchar(120) DEFAULT '',
  `date_of_birth` varchar(120) DEFAULT '',
  `martial_status` varchar(120) DEFAULT '',
  `religion` varchar(120) DEFAULT '',
  `nationality` varchar(120) DEFAULT '',
  `id_race` varchar(120) DEFAULT '',
  `mail_address1` varchar(120) DEFAULT '',
  `mail_address2` varchar(120) DEFAULT '',
  `mailing_country` int(120) DEFAULT NULL,
  `mailing_state` int(120) DEFAULT NULL,
  `mailing_city` varchar(120) DEFAULT '',
  `mailing_zipcode` varchar(120) DEFAULT '',
  `present_address_same_as_mailing_address` int(20) DEFAULT 0,
  `permanent_address1` varchar(120) DEFAULT '',
  `permanent_address2` varchar(120) DEFAULT '',
  `permanent_country` int(120) DEFAULT NULL,
  `permanent_state` int(120) DEFAULT NULL,
  `permanent_city` varchar(120) DEFAULT '',
  `permanent_zipcode` varchar(120) DEFAULT '',
  `reason` varchar(2048) DEFAULT '',
  `status` int(20) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


ALTER TABLE `patient` ADD `alternative_phone` VARCHAR(256) NULL DEFAULT '' AFTER `phone`;


CREATE TABLE `temp_test_booking_patients` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_session` varchar(512) DEFAULT '',
  `id_patient` int(2) DEFAULT 1,
  `full_name` varchar(512) DEFAULT '',
  `gender` varchar(512) DEFAULT '',
  `age` varchar(512) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` varchar(256) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




CREATE TABLE `test_booking_patients` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_test_booking` int(10) DEFAULT '0',
  `id_patient` int(2) DEFAULT 1,
  `full_name` varchar(512) DEFAULT '',
  `gender` varchar(512) DEFAULT '',
  `age` varchar(512) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` varchar(256) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `test_booking` ADD `added_by_patient` INT(20) NULL DEFAULT '0' AFTER `reason`;

ALTER TABLE `test_booking` ADD `description` VARCHAR(2048) NULL DEFAULT '' AFTER `id_staff`;

ALTER TABLE `patient` ADD `age` INT(20) NULL DEFAULT '0' AFTER `date_of_birth`, ADD `age_duration` VARCHAR(256) NULL DEFAULT '' AFTER `age`;
------ Update CMS From Here ------



































